﻿1
00:00:00,000 --> 00:00:01,690
嘿，大家好，我叫库尔纳，欢迎您回到我的住宿，再加上
hey what's up guys my name is the

2
00:00:01,889 --> 00:00:03,849
嘿，大家好，我叫库尔纳，欢迎您回到我的住宿，再加上
churner and welcome back to my stay plus

3
00:00:04,049 --> 00:00:06,729
加号系列，我需要理发，所以今天我要谈论所有关于物体的事情
plus series I need a haircut so today

4
00:00:06,929 --> 00:00:08,169
加号系列，我需要理发，所以今天我要谈论所有关于物体的事情
I'm gonna be talking all about objects

5
00:00:08,369 --> 00:00:09,819
我将开始谈论内存以及对象如何
lifetimes I'm gonna kind of start

6
00:00:10,019 --> 00:00:11,649
我将开始谈论内存以及对象如何
talking about memory and how objects

7
00:00:11,849 --> 00:00:14,019
专门针对这些堆栈进行直播，我将制作一个热对决视频
live specifically on these stack I am

8
00:00:14,218 --> 00:00:15,729
专门针对这些堆栈进行直播，我将制作一个热对决视频
going to make a heat versus tack video

9
00:00:15,929 --> 00:00:17,470
我们实际上可以深入地讨论这些东西
in which we can actually talk about this

10
00:00:17,670 --> 00:00:19,179
我们实际上可以深入地讨论这些东西
stuff in depth as well as probably

11
00:00:19,379 --> 00:00:20,890
对性能差异进行基准测试，然后谈论所有
benchmark the difference in terms of

12
00:00:21,089 --> 00:00:22,929
对性能差异进行基准测试，然后谈论所有
performance and just talk about all the

13
00:00:23,129 --> 00:00:25,929
性能指标，以及视频中的所有此类内容，但
performance kind of metrics and and and

14
00:00:26,129 --> 00:00:27,818
性能指标，以及视频中的所有此类内容，但
all that kind of stuff in that video but

15
00:00:28,018 --> 00:00:29,140
今天我们只是要像这是一生的温和介绍
today we're just gonna this is like a

16
00:00:29,339 --> 00:00:32,168
今天我们只是要像这是一生的温和介绍
gentle introduction to what lifetime

17
00:00:32,368 --> 00:00:34,750
一种基于堆栈的变量的方法，所以这有两部分
kind of means for stack-based variables

18
00:00:34,950 --> 00:00:36,339
一种基于堆栈的变量的方法，所以这有两部分
so there's kind of two parts to this

19
00:00:36,539 --> 00:00:38,529
这是第一部分，您必须了解事物如何生存
this this the first part is kind of you

20
00:00:38,729 --> 00:00:41,259
这是第一部分，您必须了解事物如何生存
have to understand how things live on

21
00:00:41,460 --> 00:00:43,329
堆栈，以便您实际上能够编写不会崩溃的代码
the stack in order for you to actually

22
00:00:43,530 --> 00:00:45,428
堆栈，以便您实际上能够编写不会崩溃的代码
be able to write code that doesn't crash

23
00:00:45,628 --> 00:00:48,219
和有效的代码，但是一旦知道了这一点，这个参与者就可以设置下一步
and code that works but also this actor

24
00:00:48,420 --> 00:00:50,018
和有效的代码，但是一旦知道了这一点，这个参与者就可以设置下一步
set the next step once you know how this

25
00:00:50,219 --> 00:00:52,358
我的工作是，我现在知道如何利用这一点并使之做我所需要的
works is that I now know how to leverage

26
00:00:52,558 --> 00:00:54,549
我的工作是，我现在知道如何利用这一点并使之做我所需要的
this and make it kind of do what I

27
00:00:54,750 --> 00:00:55,959
想做并想出一些聪明的方法做事，我们将讨论
wanted to do and come up with clever

28
00:00:56,159 --> 00:00:57,549
想做并想出一些聪明的方法做事，我们将讨论
ways to do things and we'll talk about

29
00:00:57,750 --> 00:00:59,288
好，今天我们来看一些我的意思的例子，但是基本上
well we'll look at some examples of what

30
00:00:59,488 --> 00:01:01,268
好，今天我们来看一些我的意思的例子，但是基本上
I mean by that today but basically we

31
00:01:01,469 --> 00:01:02,559
有了堆栈的概念，如何将两个堆栈很好地融合在一起
have this concept of stacks how two

32
00:01:02,759 --> 00:01:04,778
有了堆栈的概念，如何将两个堆栈很好地融合在一起
stacks well we'll have a web of video in

33
00:01:04,978 --> 00:01:06,219
关于这一点的深度，我知道我一直在说我们将有一个漂亮的视频
depth about that I know that I keep

34
00:01:06,420 --> 00:01:07,778
关于这一点的深度，我知道我一直在说我们将有一个漂亮的视频
saying we'll have a video for pretty

35
00:01:07,978 --> 00:01:09,369
几乎每个主题，但我不想专注于某些主题的细节
much every topic but it's just I don't

36
00:01:09,569 --> 00:01:11,168
几乎每个主题，但我不想专注于某些主题的细节
want to focus on the details of certain

37
00:01:11,368 --> 00:01:12,909
每个视频中的元素，否则这些视频需要45分钟
elements in every video because

38
00:01:13,109 --> 00:01:14,439
每个视频中的元素，否则这些视频需要45分钟
otherwise these videos we've 45 minutes

39
00:01:14,640 --> 00:01:16,209
对已经知道这些东西的人没有帮助，这就是为什么我要尝试
not helpful to people who already know

40
00:01:16,409 --> 00:01:17,859
对已经知道这些东西的人没有帮助，这就是为什么我要尝试
that stuff that's why I'm kind of trying

41
00:01:18,060 --> 00:01:20,558
为了使本系列货币化，可以将堆栈视为数据的最佳方法
to monetize this series the best thing a

42
00:01:20,759 --> 00:01:21,759
为了使本系列货币化，可以将堆栈视为数据的最佳方法
stack can be thought of as a data

43
00:01:21,959 --> 00:01:23,500
在上面堆叠事物的结构假装您有一堆东西
structure in which you stack things on

44
00:01:23,700 --> 00:01:25,238
在上面堆叠事物的结构假装您有一堆东西
top pretend that you've got a stack of

45
00:01:25,438 --> 00:01:27,159
桌上的书，以便您实际访问中间的一本书
books on your desk in order for you to

46
00:01:27,359 --> 00:01:28,750
桌上的书，以便您实际访问中间的一本书
actually access one in the middle you

47
00:01:28,950 --> 00:01:31,058
必须先取消前几本书才能在
would have to take the first few off to

48
00:01:31,259 --> 00:01:33,429
必须先取消前几本书才能在
get to that that book in the middle of

49
00:01:33,629 --> 00:01:34,899
当然，在现实生活中，您可以将其拉出，但这不是堆栈在其中工作的方式
course in real life you could just yank

50
00:01:35,099 --> 00:01:36,878
当然，在现实生活中，您可以将其拉出，但这不是堆栈在其中工作的方式
it out but that's not how stacks work in

51
00:01:37,078 --> 00:01:38,378
编程，所以每次我们进入C ++范围时，我们基本上都会推动我们
programming so every time we enter a

52
00:01:38,578 --> 00:01:41,109
编程，所以每次我们进入C ++范围时，我们基本上都会推动我们
scope in C++ we basically push us push

53
00:01:41,310 --> 00:01:42,939
在这种情况下，其上的堆栈框不一定必须是堆栈框
stack frame on it doesn't necessarily

54
00:01:43,140 --> 00:01:44,918
在这种情况下，其上的堆栈框不一定必须是堆栈框
have to be a stack frame in the case of

55
00:01:45,118 --> 00:01:46,689
我实际上是在推送数据，但您可以将其视为在您的书上添加一本书
I'm actually pushing data on but you can

56
00:01:46,890 --> 00:01:48,488
我实际上是在推送数据，但您可以将其视为在您的书上添加一本书
think of it as adding a book to your

57
00:01:48,688 --> 00:01:50,349
桩，并且您在该范围内声明的任何变量都像编写东西
pile and any variables that you declare

58
00:01:50,549 --> 00:01:52,238
桩，并且您在该范围内声明的任何变量都像编写东西
inside that scope is like writing stuff

59
00:01:52,438 --> 00:01:54,549
进入您的书中，一旦范围结束，您便将书从书架上拿下来
into your book and once that scope ends

60
00:01:54,750 --> 00:01:56,109
进入您的书中，一旦范围结束，您便将书从书架上拿下来
and you take that book off of your pile

61
00:01:56,310 --> 00:01:59,259
的书籍，您在其中声明的每个基于堆栈的变量都正确
of books it's gone right every stack

62
00:01:59,459 --> 00:02:00,939
的书籍，您在其中声明的每个基于堆栈的变量都正确
based variable which you declared in

63
00:02:01,140 --> 00:02:03,159
那本书，你在那本书里面的堆栈上创建的每个对象都是
that book every object that you created

64
00:02:03,359 --> 00:02:05,409
那本书，你在那本书里面的堆栈上创建的每个对象都是
on the stack inside that book they're

65
00:02:05,609 --> 00:02:07,209
现在走了，那既是福也是祸，但是如果你知道自己是什么
gone now and that is both a blessing and

66
00:02:07,409 --> 00:02:08,860
现在走了，那既是福也是祸，但是如果你知道自己是什么
a curse but if you know what you're

67
00:02:09,060 --> 00:02:10,600
那么显然这只是100％的时间的祝福，所以
doing then obviously it's just

68
00:02:10,800 --> 00:02:12,550
那么显然这只是100％的时间的祝福，所以
be a blessing 100% of the time right so

69
00:02:12,750 --> 00:02:14,200
我将向您展示一些有关如何将所有这些完美结合的示例
I'm going to show you some examples of

70
00:02:14,400 --> 00:02:15,790
我将向您展示一些有关如何将所有这些完美结合的示例
exactly how all of this comes together

71
00:02:15,990 --> 00:02:17,110
以及所有这些如何运作，并希望不会让您痛苦地看到
and how all this works and hopefully

72
00:02:17,310 --> 00:02:19,060
以及所有这些如何运作，并希望不会让您痛苦地看到
doesn't make it painfully obvious to you

73
00:02:19,259 --> 00:02:20,530
所以首先让我们谈谈范围范围可以是函数中的任何内容
so first of all let's talk about scopes

74
00:02:20,729 --> 00:02:22,719
所以首先让我们谈谈范围范围可以是函数中的任何内容
a scope can be anything from a function

75
00:02:22,919 --> 00:02:24,880
像这样的范围在这里，某种if语句可以像这样或
scope like what this is right here some

76
00:02:25,080 --> 00:02:27,490
像这样的范围在这里，某种if语句可以像这样或
kind of if statements cope like this or

77
00:02:27,689 --> 00:02:28,960
也可能在for循环或while循环中，或者像我们这样的空Scott
could also be in a for loop or a while

78
00:02:29,159 --> 00:02:32,230
也可能在for循环或while循环中，或者像我们这样的空Scott
loop or just an empty Scott like that we

79
00:02:32,430 --> 00:02:33,760
也有类的范围，这意味着当我声明类似实体的类时
also have scopes for classes meaning

80
00:02:33,960 --> 00:02:35,950
也有类的范围，这意味着当我声明类似实体的类时
that when I declare a class like entity

81
00:02:36,150 --> 00:02:38,110
这是我的首选示例，我这里有某种堆栈初始化变量
it's my go-to example and I have some

82
00:02:38,310 --> 00:02:40,120
这是我的首选示例，我这里有某种堆栈初始化变量
kind of stack initialized variable here

83
00:02:40,319 --> 00:02:41,500
所以没有在堆上分配的东西，那么这个变量也是
so something that isn't allocated on the

84
00:02:41,699 --> 00:02:43,870
所以没有在堆上分配的东西，那么这个变量也是
heap here then this variable is also

85
00:02:44,069 --> 00:02:45,820
在该类范围内意味着当该类死亡时，该变量死亡
inside this class scope meaning that

86
00:02:46,020 --> 00:02:48,280
在该类范围内意味着当该类死亡时，该变量死亡
when this class dies this variable dies

87
00:02:48,479 --> 00:02:50,290
因此，让我们看看实际情况，我将只编写一个非常简单的实体类
so let's see this in action I'm going to

88
00:02:50,490 --> 00:02:51,820
因此，让我们看看实际情况，我将只编写一个非常简单的实体类
just write a very simple entity class

89
00:02:52,020 --> 00:02:53,500
在这里，这不必是一个空的类，绝对可以是任何东西
here this doesn't have to be an empty

90
00:02:53,699 --> 00:02:55,270
在这里，这不必是一个空的类，绝对可以是任何东西
class could be absolutely anything it's

91
00:02:55,469 --> 00:02:56,980
将在这里有一个构造函数，该构造函数将一些内容打印到
going to have a constructor here which

92
00:02:57,180 --> 00:02:58,240
将在这里有一个构造函数，该构造函数将一些内容打印到
is going to print something to the

93
00:02:58,439 --> 00:03:00,610
控制台，例如创建身份，我们还将为此类提供一个
console like create identity and we're

94
00:03:00,810 --> 00:03:01,420
控制台，例如创建身份，我们还将为此类提供一个
also going to give this class a

95
00:03:01,620 --> 00:03:02,590
析构函数我实际上只是将其复制到此处，将代字号添加到
destructor I'm actually just going to

96
00:03:02,789 --> 00:03:04,630
析构函数我实际上只是将其复制到此处，将代字号添加到
copy this add the tilde here to make it

97
00:03:04,830 --> 00:03:06,219
一个析构函数并将其更改为被破坏的实体，因此我们创建了
a destructor and change this to

98
00:03:06,419 --> 00:03:08,320
一个析构函数并将其更改为被破坏的实体，因此我们创建了
destroyed entity so we have created

99
00:03:08,520 --> 00:03:10,240
此范围内的构造函数中的实体和析构函数中的销毁实体
entity in a constructor and destroyed

100
00:03:10,439 --> 00:03:12,400
此范围内的构造函数中的实体和析构函数中的销毁实体
entity in a destructor inside this scope

101
00:03:12,599 --> 00:03:14,410
在这里，我要声明我的实体，而不是在堆上创建它
over here I'm going to declare my entity

102
00:03:14,610 --> 00:03:16,180
在这里，我要声明我的实体，而不是在堆上创建它
instead of creating it on the heap on

103
00:03:16,379 --> 00:03:17,410
通过编写像star这样的代码来保护堆栈上的信用
guard credit on the stack just by

104
00:03:17,610 --> 00:03:19,060
通过编写像star这样的代码来保护堆栈上的信用
writing code like star this is going to

105
00:03:19,259 --> 00:03:21,070
调用默认构造函数，我将在该行上放置一个断点并点击
call the default constructor I'm going

106
00:03:21,270 --> 00:03:22,900
调用默认构造函数，我将在该行上放置一个断点并点击
to put a breakpoint on this line and hit

107
00:03:23,099 --> 00:03:25,060
f5可以运行我的程序，所以一旦我碰到这个断点，我就打开
f5 to run my program okay so once I hit

108
00:03:25,259 --> 00:03:26,590
f5可以运行我的程序，所以一旦我碰到这个断点，我就打开
this breakpoint I'm just going to open

109
00:03:26,789 --> 00:03:27,969
我的控制台在这里，以便我们可以看到如果我按f10前进会发生什么情况
my console here so that we can see

110
00:03:28,169 --> 00:03:30,160
我的控制台在这里，以便我们可以看到如果我按f10前进会发生什么情况
what's happening if I hit f10 to advance

111
00:03:30,360 --> 00:03:32,439
向前一行，您会看到广告素材实体现在已打印到控制台
one line forward you'll see the creative

112
00:03:32,639 --> 00:03:34,240
向前一行，您会看到广告素材实体现在已打印到控制台
entity gets printed to the console now

113
00:03:34,439 --> 00:03:36,610
我们在这个范围的尽头，所以当我击中f10时，我们就在摧毁
we are at the end of this scope here so

114
00:03:36,810 --> 00:03:38,620
我们在这个范围的尽头，所以当我击中f10时，我们就在摧毁
as soon as I hit f10 we are destroying

115
00:03:38,819 --> 00:03:40,509
我们的实体那个实体已经消失了，因为内存已经释放了，应该很漂亮
our entity that entity is gone now that

116
00:03:40,709 --> 00:03:42,610
我们的实体那个实体已经消失了，因为内存已经释放了，应该很漂亮
memory has been freed should be pretty

117
00:03:42,810 --> 00:03:44,950
很明显，如果我要通过将其转换为
obvious if I was to do a heap allocation

118
00:03:45,150 --> 00:03:46,870
很明显，如果我要通过将其转换为
on this by converting this into a

119
00:03:47,069 --> 00:03:49,150
如果我在这里按f5键，则将指针和代码编写为带有可选括号的代码
pointer and writing code like so with

120
00:03:49,349 --> 00:03:51,430
如果我在这里按f5键，则将指针和代码编写为带有可选括号的代码
optional parentheses if I hit f5 here

121
00:03:51,629 --> 00:03:53,469
我将前进一行代码，您可以看到我们什至没有去
I'll have one line of code forward and

122
00:03:53,669 --> 00:03:55,000
我将前进一行代码，您可以看到我们什至没有去
you can see that we didn't even go to

123
00:03:55,199 --> 00:03:56,920
这里的范围，如果我看一下控制台，我们只是说信用实体，仅此而已
the scope here if I look at my console

124
00:03:57,120 --> 00:04:00,460
这里的范围，如果我看一下控制台，我们只是说信用实体，仅此而已
we just say credit entity and that's it

125
00:04:00,659 --> 00:04:01,990
我们已经在这行上，我什至可以执行这行，所以现在我们
we're already on this line I can even

126
00:04:02,189 --> 00:04:03,640
我们已经在这行上，我什至可以执行这行，所以现在我们
execute this line so right now we're

127
00:04:03,840 --> 00:04:05,230
实际上等待输入，您可以看到我们的实体永远不会被摧毁
actually waiting for input and you can

128
00:04:05,430 --> 00:04:07,539
实际上等待输入，您可以看到我们的实体永远不会被摧毁
see our entity never gets destroyed of

129
00:04:07,739 --> 00:04:09,160
当然，当我们执行以下操作时，内存确实会被操作系统清除
course that memory does get cleaned up

130
00:04:09,360 --> 00:04:10,780
当然，当我们执行以下操作时，内存确实会被操作系统清除
by the operating system when our

131
00:04:10,979 --> 00:04:12,340
应用程序立即终止，您应该看到对象的不同
application terminates so straight away

132
00:04:12,539 --> 00:04:14,259
应用程序立即终止，您应该看到对象的不同
you should see the difference in object

133
00:04:14,459 --> 00:04:16,210
基于堆栈的变量和基于热的变量之间的寿命
lifetimes between a stack-based variable

134
00:04:16,410 --> 00:04:17,900
基于堆栈的变量和基于热的变量之间的寿命
and a heat-based variable

135
00:04:18,100 --> 00:04:19,699
一旦超出范围，基于变量的变量将被释放，因此
based variable gets freed gets destroyed

136
00:04:19,899 --> 00:04:21,560
一旦超出范围，基于变量的变量将被释放，因此
as soon as we go out of scope so that's

137
00:04:21,759 --> 00:04:22,970
基本上是今天视频的重点，我只想让您将它带入头脑
basically the point of today's video I

138
00:04:23,170 --> 00:04:24,290
基本上是今天视频的重点，我只想让您将它带入头脑
just want you to get it into your head

139
00:04:24,490 --> 00:04:25,819
如果您在堆栈上声明了一些东西，如果您在
that if you declare something on the

140
00:04:26,019 --> 00:04:26,960
如果您在堆栈上声明了一些东西，如果您在
stack if you create a variable on the

141
00:04:27,160 --> 00:04:29,360
堆栈，当它开始时就不存在了，现在让我们来
stack it's going to cease to exist when

142
00:04:29,560 --> 00:04:31,460
堆栈，当它开始时就不存在了，现在让我们来
it goes out of start now let's take a

143
00:04:31,660 --> 00:04:32,900
了解一些您现在已经不愿做的事情，因为您已经掌握了这些知识
look at some things you might not want

144
00:04:33,100 --> 00:04:34,759
了解一些您现在已经不愿做的事情，因为您已经掌握了这些知识
to do now that you have this knowledge a

145
00:04:34,959 --> 00:04:37,009
很好的例子是我想在函数内部创建一个数组，也许是
great example is I want to create an

146
00:04:37,209 --> 00:04:38,930
很好的例子是我想在函数内部创建一个数组，也许是
array inside a function maybe it's an

147
00:04:39,129 --> 00:04:40,910
整数数组，因此它将返回一个int指针，我将其称为create数组，
integer array so it will return an int

148
00:04:41,110 --> 00:04:43,610
整数数组，因此它将返回一个int指针，我将其称为create数组，
pointer I'll call this create array and

149
00:04:43,810 --> 00:04:46,759
我可能会编写诸如int array这样的代码，所以这将分配数组15
I might write code such as int array so

150
00:04:46,959 --> 00:04:48,439
我可能会编写诸如int array这样的代码，所以这将分配数组15
this is going to allocate the array 15

151
00:04:48,639 --> 00:04:50,329
从这里开始，然后我可以返回团队，这看起来很完美
here to just large and then I can return

152
00:04:50,529 --> 00:04:51,860
从这里开始，然后我可以返回团队，这看起来很完美
a raid now this looks like perfectly

153
00:04:52,060 --> 00:04:53,150
我的意思是明智的卡是创建一个数组，然后返回一个指针
sensible card I mean is creating an

154
00:04:53,350 --> 00:04:55,009
我的意思是明智的卡是创建一个数组，然后返回一个指针
array and then returning a pointer to

155
00:04:55,209 --> 00:04:57,020
该数组似乎合法正确是错误的，如果您以为是完全错误的
that array seems legit right wrong

156
00:04:57,220 --> 00:04:58,670
该数组似乎合法正确是错误的，如果您以为是完全错误的
you're completely wrong if you thought

157
00:04:58,870 --> 00:05:00,170
那是合法的，我真的希望您能通过创建
that was legit I really hope you did

158
00:05:00,370 --> 00:05:01,910
那是合法的，我真的希望您能通过创建
let's take a look at why so by creating

159
00:05:02,110 --> 00:05:04,040
这样的数组我们不会在堆上分配它，我们不会调用new
an array like this we're not allocating

160
00:05:04,240 --> 00:05:05,660
这样的数组我们不会在堆上分配它，我们不会调用new
on it on the heap we're not calling new

161
00:05:05,860 --> 00:05:08,180
或进行某种堆分配，我们只是在堆栈上声明它，
or doing some kind of heap allocation we

162
00:05:08,379 --> 00:05:10,160
或进行某种堆分配，我们只是在堆栈上声明它，
are just declaring it on the stack and

163
00:05:10,360 --> 00:05:11,689
当我们返回指向该堆栈存储器的指针时
when we return a pointer to that is

164
00:05:11,889 --> 00:05:13,370
当我们返回指向该堆栈存储器的指针时
returning a pointer to that stack memory

165
00:05:13,569 --> 00:05:15,650
猜测一旦超出范围，堆栈内存将被清除，因此
guess what that stack memory gets

166
00:05:15,850 --> 00:05:17,900
猜测一旦超出范围，堆栈内存将被清除，因此
cleared as soon as we go out of scope so

167
00:05:18,100 --> 00:05:19,910
如果您编写这样的代码，如果您想编写一个代码，它将失败
if you write code like this it's going

168
00:05:20,110 --> 00:05:21,800
如果您编写这样的代码，如果您想编写一个代码，它将失败
to fail if you would like to write a

169
00:05:22,000 --> 00:05:23,629
这样的功能，您基本上有两个选择，您可以选择
function like this you basically have

170
00:05:23,829 --> 00:05:26,360
这样的功能，您基本上有两个选择，您可以选择
two options you can either get this to

171
00:05:26,560 --> 00:05:27,850
实际在堆上分配数组，从而确保其生存期
actually allocate the array on the heap

172
00:05:28,050 --> 00:05:30,170
实际在堆上分配数组，从而确保其生存期
thus ensuring that its lifetime will

173
00:05:30,370 --> 00:05:31,819
实际上停留在附近，或者您可以询问在此处创建的数据以
actually stay around or you could ask

174
00:05:32,019 --> 00:05:33,350
实际上停留在附近，或者您可以询问在此处创建的数据以
this data that you've created here to

175
00:05:33,550 --> 00:05:34,790
实际上被复制到堆栈中更远的位置，因此它具有
actually be copied to a location that

176
00:05:34,990 --> 00:05:36,590
实际上被复制到堆栈中更远的位置，因此它具有
exists further up the stack so it has an

177
00:05:36,790 --> 00:05:38,509
举例来说，我实际上是在这里用50个整数创建数组，然后
example let's say I actually create my

178
00:05:38,709 --> 00:05:40,610
举例来说，我实际上是在这里用50个整数创建数组，然后
array over here of 50 integers and then

179
00:05:40,810 --> 00:05:41,990
我希望此功劳更多地是一个填充数组
I want this credit ready to be more of a

180
00:05:42,189 --> 00:05:43,699
我希望此功劳更多地是一个填充数组
fill array which would take in that

181
00:05:43,899 --> 00:05:45,800
数组作为参数，实际上在这里要做的任何事情
array as a parameter and actually do

182
00:05:46,000 --> 00:05:47,480
数组作为参数，实际上在这里要做的任何事情
anything that it had to do here of

183
00:05:47,680 --> 00:05:49,310
当然，这个创建数组的示例自从我爱以来就崩溃了
course this example of creating an array

184
00:05:49,509 --> 00:05:50,689
当然，这个创建数组的示例自从我爱以来就崩溃了
kind of falls apart since when I love

185
00:05:50,889 --> 00:05:52,520
在这里创建它，但是我们仍然基本上在操纵它，所以也许
are creating it inside here but we are

186
00:05:52,720 --> 00:05:54,560
在这里创建它，但是我们仍然基本上在操纵它，所以也许
still basically manipulating it so maybe

187
00:05:54,759 --> 00:05:56,420
这可能会像填充数组一样，从这个意义上说，我们只是
this could do something like fill our

188
00:05:56,620 --> 00:05:58,189
这可能会像填充数组一样，从这个意义上说，我们只是
array and in this sense we're just

189
00:05:58,389 --> 00:06:00,110
传递指针，这样就不会取消分配数组创建
passing a pointer so it's not going to

190
00:06:00,310 --> 00:06:02,720
传递指针，这样就不会取消分配数组创建
get de-allocated that array creation is

191
00:06:02,920 --> 00:06:04,490
我经常倾向于说人们总是会创造一个经典的错误
a classic mistake which I actually tend

192
00:06:04,689 --> 00:06:06,319
我经常倾向于说人们总是会创造一个经典的错误
to say all the time people will create a

193
00:06:06,519 --> 00:06:07,879
基于堆栈的变量，并尝试返回指向它的指针
stack based variable and try to return a

194
00:06:08,079 --> 00:06:08,660
基于堆栈的变量，并尝试返回指向它的指针
pointer to it

195
00:06:08,860 --> 00:06:10,879
没有意识到，一旦功能结束，您就超出了范围
no realizing that once that function

196
00:06:11,079 --> 00:06:12,050
没有意识到，一旦功能结束，您就超出了范围
ends and you go out of scope that

197
00:06:12,250 --> 00:06:13,639
变量通过这种基于堆栈的自动破坏来完成
variables done so with this kind of

198
00:06:13,839 --> 00:06:15,350
变量通过这种基于堆栈的自动破坏来完成
automatic destruction of stack based

199
00:06:15,550 --> 00:06:16,910
变量有没有办法让我们有用呢？有没有办法
variables is there a way that we can

200
00:06:17,110 --> 00:06:18,800
变量有没有办法让我们有用呢？有没有办法
kind of make it useful is there a way

201
00:06:19,000 --> 00:06:20,900
我们可以利用它并永久使用它，答案是肯定的
that we can leverage it and use it for

202
00:06:21,100 --> 00:06:22,939
我们可以利用它并永久使用它，答案是肯定的
good and the answer is yes there are

203
00:06:23,139 --> 00:06:24,319
大量有用的方法，这些方法非常有用并且可以实际帮助
plenty of ways in which this is

204
00:06:24,519 --> 00:06:26,028
大量有用的方法，这些方法非常有用并且可以实际帮助
incredibly useful and can actually help

205
00:06:26,228 --> 00:06:26,960
一种自动化的代码，我们可以做的一件事
kind of automate

206
00:06:27,160 --> 00:06:28,400
一种自动化的代码，我们可以做的一件事
code and one thing that we can do with

207
00:06:28,600 --> 00:06:30,560
这实际上是写范围很广的类，例如智能指针
this is actually write scoped kind of

208
00:06:30,759 --> 00:06:32,840
这实际上是写范围很广的类，例如智能指针
classes right such as a smart pointer

209
00:06:33,040 --> 00:06:34,579
像是唯一的指针，它是作用域的指针或类似作用域的锁
like unique pointer which is a scoped

210
00:06:34,779 --> 00:06:36,500
像是唯一的指针，它是作用域的指针或类似作用域的锁
pointer or something like a scoped lock

211
00:06:36,699 --> 00:06:38,480
将来肯定会有很多例子
that there's a lot of examples which we

212
00:06:38,680 --> 00:06:40,129
将来肯定会有很多例子
will definitely get into in the future

213
00:06:40,329 --> 00:06:41,720
但最简单的例子可能是笔触指针
but the simplest example is probably a

214
00:06:41,920 --> 00:06:43,280
但最简单的例子可能是笔触指针
stroked pointer what that basically is

215
00:06:43,480 --> 00:06:45,590
是一个在构造堆上对指针进行包装的类
is a class that's a wrapper over a

216
00:06:45,790 --> 00:06:47,660
是一个在构造堆上对指针进行包装的类
pointer which upon construction heap

217
00:06:47,860 --> 00:06:49,250
分配指针，然后在销毁后删除指针，因此我们
allocates the pointer and then upon

218
00:06:49,449 --> 00:06:51,079
分配指针，然后在销毁后删除指针，因此我们
destruction deletes the pointer so we

219
00:06:51,279 --> 00:06:53,060
可以自动化这个新的和删除的东西，让我们看看我们如何
can kind of automate this new and delete

220
00:06:53,259 --> 00:06:54,650
可以自动化这个新的和删除的东西，让我们看看我们如何
thing let's take a look at how we might

221
00:06:54,850 --> 00:06:56,180
写一个像这样的类，所以这个实体权我想仍然分配在
write a class like that so this entity

222
00:06:56,379 --> 00:06:58,069
写一个像这样的类，所以这个实体权我想仍然分配在
right I want to still allocated on the

223
00:06:58,269 --> 00:06:59,569
我想将其称为new和all但我想自动成为
heap I want to call new and all that

224
00:06:59,769 --> 00:07:01,850
我想将其称为new和all但我想自动成为
however I wanted to automatically be

225
00:07:02,050 --> 00:07:03,740
当超出范围时将其删除我们可以做到的答案是肯定的，所以我们可以
deleted when this goes out of scope can

226
00:07:03,939 --> 00:07:05,389
当超出范围时将其删除我们可以做到的答案是肯定的，所以我们可以
we do that the answer is yes so we could

227
00:07:05,589 --> 00:07:06,379
在标准库中使用称为唯一指针的东西
use something in the standard library

228
00:07:06,579 --> 00:07:07,850
在标准库中使用称为唯一指针的东西
called a unique pointer which is a

229
00:07:08,050 --> 00:07:09,590
作用域指针，但出于本示例的目的，我们将编写自己的指针，以便
scoped pointer but for the purposes of

230
00:07:09,790 --> 00:07:11,000
作用域指针，但出于本示例的目的，我们将编写自己的指针，以便
this example we'll write our own so that

231
00:07:11,199 --> 00:07:12,170
您可以看到它是如何工作的我写了一个名为Scarf 2.0的类
you can see how it works

232
00:07:12,370 --> 00:07:14,300
您可以看到它是如何工作的我写了一个名为Scarf 2.0的类
I write a class called scarf 2.0 I'm

233
00:07:14,500 --> 00:07:15,740
现在要保持它非常简单，只接受猪只接受
going to keep it really simple for now

234
00:07:15,939 --> 00:07:17,090
现在要保持它非常简单，只接受猪只接受
and just hog credit to only accept

235
00:07:17,290 --> 00:07:18,829
实体，所以这将是我们要构造的实际指针
entities so this will be our actual

236
00:07:19,029 --> 00:07:21,230
实体，所以这将是我们要构造的实际指针
pointer upon construction I'm going to

237
00:07:21,430 --> 00:07:23,240
写一个接受我们实体的构造函数，所以这是我的MC点
write a constructor which takes in our

238
00:07:23,439 --> 00:07:25,879
写一个接受我们实体的构造函数，所以这是我的MC点
entity so this is an MC point up I'm

239
00:07:26,079 --> 00:07:27,980
将s分配到这里，一旦销毁，我将调用delete和
going to assign s to here and upon

240
00:07:28,180 --> 00:07:31,189
将s分配到这里，一旦销毁，我将调用delete和
destruction I'm going to call delete and

241
00:07:31,389 --> 00:07:33,620
像这样指向，就是这样，它是基本的作用域指针，所以让我们
point up just like that and that's it

242
00:07:33,819 --> 00:07:36,079
像这样指向，就是这样，它是基本的作用域指针，所以让我们
that is a basic scoped pointer so let's

243
00:07:36,279 --> 00:07:37,040
看一看如何使用它而不是写一个等于new的实体
take a look at how we can use that

244
00:07:37,240 --> 00:07:39,199
看一看如何使用它而不是写一个等于new的实体
instead of writing entity a equals new

245
00:07:39,399 --> 00:07:41,629
我实际上将要使用作用域指针E的实体，这基本上是我们的
entity I'm actually going to ride scoped

246
00:07:41,829 --> 00:07:43,939
我实际上将要使用作用域指针E的实体，这基本上是我们的
pointer E which is basically our

247
00:07:44,139 --> 00:07:46,400
变量名等于新实体，我也可以这样写：
variable name equals new entity I could

248
00:07:46,600 --> 00:07:48,439
变量名等于新实体，我也可以这样写：
have also written this like so with the

249
00:07:48,639 --> 00:07:50,120
默认构造函数，但只是为了使其与之前类似
default constructor but just to keep it

250
00:07:50,319 --> 00:07:52,340
默认构造函数，但只是为了使其与之前类似
similar to what it was before it kind of

251
00:07:52,540 --> 00:07:53,689
看起来一样，这当然是由于隐式转换而起作用的，所以这种
looks the same and of course this works

252
00:07:53,889 --> 00:07:55,759
看起来一样，这当然是由于隐式转换而起作用的，所以这种
due to implicit conversion so this kind

253
00:07:55,959 --> 00:07:57,410
看起来像相同的代码，但是区别在于这实际上
of looks like identical code but the

254
00:07:57,610 --> 00:07:59,540
看起来像相同的代码，但是区别在于这实际上
difference is that this will actually

255
00:07:59,740 --> 00:08:01,280
一旦超出范围就会被破坏，因为范围指针当然
get destroyed once we go out of scope

256
00:08:01,480 --> 00:08:03,379
一旦超出范围就会被破坏，因为范围指针当然
because of course the scoped pointer

257
00:08:03,579 --> 00:08:06,079
类本身将作用域指针对象分配到堆栈上，这意味着
class itself the scoped pointer object

258
00:08:06,279 --> 00:08:08,780
类本身将作用域指针对象分配到堆栈上，这意味着
gets allocated on the stack which means

259
00:08:08,980 --> 00:08:10,639
它被删除，当它被删除时自动等于delete in
that it gets deleted and when it gets

260
00:08:10,839 --> 00:08:12,800
它被删除，当它被删除时自动等于delete in
deleted automatically equals delete in

261
00:08:13,000 --> 00:08:14,900
析构函数，它删除包裹的那个指针，所以如果我们把
the destructor which deletes that that

262
00:08:15,100 --> 00:08:17,540
析构函数，它删除包裹的那个指针，所以如果我们把
pointer that it's wrapping so if we put

263
00:08:17,740 --> 00:08:19,699
在此处设置一个断点并按f5键将向前推进一行，请检查我们的
a breakpoint here and hit f5 will

264
00:08:19,899 --> 00:08:21,770
在此处设置一个断点并按f5键将向前推进一行，请检查我们的
advance one line forward check our

265
00:08:21,970 --> 00:08:23,780
控制台，我们看到可以看到工作就绪的Mantasy，然后再向前一行
console we see we can see the work-ready

266
00:08:23,980 --> 00:08:26,000
控制台，我们看到可以看到工作就绪的Mantasy，然后再向前一行
mantasy and then one line forward again

267
00:08:26,199 --> 00:08:27,680
并检查一下，即使我们使用new来破坏我们的实体
and check this out we destroyed our

268
00:08:27,879 --> 00:08:29,480
并检查一下，即使我们使用new来破坏我们的实体
entity even though we use new to

269
00:08:29,680 --> 00:08:30,350
堆分配，这是第一个很好的例子
heap-allocated

270
00:08:30,550 --> 00:08:32,509
堆分配，这是第一个很好的例子
and that is a great example of first of

271
00:08:32,710 --> 00:08:34,250
我们要做的是一个非常基本的智能指针（称为唯一指针）
all what a very basic smart pointer

272
00:08:34,450 --> 00:08:35,870
我们要做的是一个非常基本的智能指针（称为唯一指针）
called unique pointer does we're going

273
00:08:36,070 --> 00:08:37,519
具有完整的视频，专门用于各种智能指针
to have an entire video dedicated to

274
00:08:37,719 --> 00:08:38,639
具有完整的视频，专门用于各种智能指针
smart pointers various

275
00:08:38,840 --> 00:08:41,039
你知道人们一直像人们一直在抱怨我使用新的
you know people have been like people

276
00:08:41,240 --> 00:08:42,689
你知道人们一直像人们一直在抱怨我使用新的
have been complaining about me using new

277
00:08:42,889 --> 00:08:43,799
而不是使用智能指针和教授不良的C ++，我们将拥有一个真正的
and not using smart pointers and

278
00:08:44,000 --> 00:08:46,349
而不是使用智能指针和教授不良的C ++，我们将拥有一个真正的
teaching bad C++ we're gonna have a real

279
00:08:46,549 --> 00:08:48,240
稍后再聊一点，但对于所有人，大家都在等待聪明
little chat about that later by the way

280
00:08:48,440 --> 00:08:49,439
稍后再聊一点，但对于所有人，大家都在等待聪明
but for all of you waiting for smart

281
00:08:49,639 --> 00:08:50,549
指针，我们肯定会很快就做一个视频，因为
pointers we're definitely gonna do a

282
00:08:50,750 --> 00:08:51,839
指针，我们肯定会很快就做一个视频，因为
video on that very very soon because

283
00:08:52,039 --> 00:08:53,099
它们是语言的重要组成部分，因此这种自动
they're a very important part of the

284
00:08:53,299 --> 00:08:54,389
它们是语言的重要组成部分，因此这种自动
language so this kind of automatic

285
00:08:54,590 --> 00:08:56,099
创建一种自动销毁，我们可以从以下事实中获得
creation an automatic destruction that

286
00:08:56,299 --> 00:08:58,319
创建一种自动销毁，我们可以从以下事实中获得
we can kind of get from the fact that a

287
00:08:58,519 --> 00:09:00,509
基于堆栈的变量超出范围并被破坏实际上是
stack based variable goes out of scope

288
00:09:00,710 --> 00:09:02,219
基于堆栈的变量超出范围并被破坏实际上是
and gets destroyed is actually really

289
00:09:02,419 --> 00:09:04,349
有用，还有很多，仅举几例
useful and there are plenty there are

290
00:09:04,549 --> 00:09:06,120
有用，还有很多，仅举几例
plenty of more examples just to name

291
00:09:06,320 --> 00:09:07,799
另一个在我头顶上方的计时器是一个计时器，让我们说您想要时间
another one off the top of my head a

292
00:09:08,000 --> 00:09:09,719
另一个在我头顶上方的计时器是一个计时器，让我们说您想要时间
timer let's just say you wanted time how

293
00:09:09,919 --> 00:09:11,069
只要您处于基准测试范围之内，或者您可以
long you are inside a scope for

294
00:09:11,269 --> 00:09:12,779
只要您处于基准测试范围之内，或者您可以
benchmarking or something you could

295
00:09:12,980 --> 00:09:14,129
编写一个类，该类在构造对象时启动计时器
write a time a class which starts the

296
00:09:14,330 --> 00:09:15,959
编写一个类，该类在构造对象时启动计时器
timer upon construction of the object

297
00:09:16,159 --> 00:09:17,729
这停止了​​我可能打印结果的时间，或者当对象
and that stops the time I maybe prints

298
00:09:17,929 --> 00:09:20,009
这停止了​​我可能打印结果的时间，或者当对象
the result or whatever when the object

299
00:09:20,210 --> 00:09:22,229
当计时器对象被销毁时，突然间您有了一个自动计时器
when the timer object gets destroyed so

300
00:09:22,429 --> 00:09:23,339
当计时器对象被销毁时，突然间您有了一个自动计时器
suddenly you've got an automatic timer

301
00:09:23,539 --> 00:09:24,779
您只需在函数的开头编写一行代码，
you just write one line of code at the

302
00:09:24,980 --> 00:09:26,669
您只需在函数的开头编写一行代码，
beginning of your function and that

303
00:09:26,870 --> 00:09:28,469
整个示波器现在要定时了，您转而不必打电话
entire scope is going to now be timed

304
00:09:28,669 --> 00:09:29,909
整个示波器现在要定时了，您转而不必打电话
and you turn and you never have to call

305
00:09:30,110 --> 00:09:31,919
时间会停止或手动停止，因为一旦超出范围
time it'll stop or whatever manually

306
00:09:32,120 --> 00:09:32,969
时间会停止或手动停止，因为一旦超出范围
because as soon as it goes out of scope

307
00:09:33,169 --> 00:09:35,159
并自动为您致电，这真的很棒，而且有很多
and calls that for you automatically

308
00:09:35,360 --> 00:09:37,079
并自动为您致电，这真的很棒，而且有很多
it's really amazing and there's so many

309
00:09:37,279 --> 00:09:39,419
用于它的另一个是互斥锁，如果您想锁定某个功能，则
uses for it another one is mutex locking

310
00:09:39,620 --> 00:09:40,769
用于它的另一个是互斥锁，如果您想锁定某个功能，则
if you want to lock a function so the

311
00:09:40,970 --> 00:09:42,329
多个线程无法同时访问它并导致爆炸
multiple threads can't access it at the

312
00:09:42,529 --> 00:09:44,069
多个线程无法同时访问它并导致爆炸
same time and caused an explosion you

313
00:09:44,269 --> 00:09:46,439
可以具有自动作用域锁，该锁在功能锁的开头
can have an automatic scoped lock which

314
00:09:46,639 --> 00:09:48,509
可以具有自动作用域锁，该锁在功能锁的开头
at the beginning of the function locks

315
00:09:48,710 --> 00:09:49,559
它并在功能结束时将其解锁，红军即将来临
it and at the end of the function

316
00:09:49,759 --> 00:09:52,379
它并在功能结束时将其解锁，红军即将来临
unlocks it and the Reds are coming soon

317
00:09:52,580 --> 00:09:54,659
我真的迫不及待想要进入C ++中更复杂，更有趣的内容
I really can't wait to get into the more

318
00:09:54,860 --> 00:09:57,389
我真的迫不及待想要进入C ++中更复杂，更有趣的内容
kind of complicated and fun stuff in C++

319
00:09:57,590 --> 00:09:59,669
所以您不用担心还会有更多视频希望你们喜欢这个视频
so don't you worry many more videos are

320
00:09:59,870 --> 00:10:01,139
所以您不用担心还会有更多视频希望你们喜欢这个视频
coming hope you guys enjoyed this video

321
00:10:01,340 --> 00:10:02,609
如果你按了那个按钮，我想我会一直在那个角落
if you did you hit that like button I

322
00:10:02,809 --> 00:10:03,839
如果你按了那个按钮，我想我会一直在那个角落
think is in that corner I will always

323
00:10:04,039 --> 00:10:04,889
指向那边和所有其他视频，但我认为它在这里，所以现在
pointing over there and all the other

324
00:10:05,090 --> 00:10:06,120
指向那边和所有其他视频，但我认为它在这里，所以现在
videos but I think it's here so now that

325
00:10:06,320 --> 00:10:07,679
您知道它在哪里，您可以单击它，也可以帮助在
you know where it is you can click it

326
00:10:07,879 --> 00:10:09,209
您知道它在哪里，您可以单击它，也可以帮助在
you can also help support this series on

327
00:10:09,409 --> 00:10:11,339
通过去家里的patreon腰带来装饰churner，那里有很多很酷的东西
patreon by going to patreon at home for

328
00:10:11,539 --> 00:10:13,409
通过去家里的patreon腰带来装饰churner，那里有很多很酷的东西
sash the churner there's plenty of cool

329
00:10:13,610 --> 00:10:15,120
您可以访问的奖励，例如我们计划的私人不和谐聊天
rewards that you can get access to such

330
00:10:15,320 --> 00:10:17,159
您可以访问的奖励，例如我们计划的私人不和谐聊天
as a private discord chat where we plan

331
00:10:17,360 --> 00:10:18,750
这些视频，您可以随便问问，也可以问我任何问题
these videos you can request whatever

332
00:10:18,950 --> 00:10:20,339
这些视频，您可以随便问问，也可以问我任何问题
you like you can ask me any questions

333
00:10:20,539 --> 00:10:22,289
您也要在那里，我当然我当然会回答所有顾客
you want there as well and I of course I

334
00:10:22,490 --> 00:10:24,629
您也要在那里，我当然我当然会回答所有顾客
I of course answer all of my patrons

335
00:10:24,830 --> 00:10:26,009
因为我为什么不这样做，但是您通过支持所做的最重要的事情是
because why wouldn't I but the most

336
00:10:26,210 --> 00:10:27,629
因为我为什么不这样做，但是您通过支持所做的最重要的事情是
important thing you do by supporting is

337
00:10:27,830 --> 00:10:29,099
只是确保我可以制作更多这些视频并尽快将它们发布
just making sure that I can make more of

338
00:10:29,299 --> 00:10:31,019
只是确保我可以制作更多这些视频并尽快将它们发布
these videos and get them out as fast as

339
00:10:31,220 --> 00:10:32,699
可能如此，非常感谢我所有的支持者，我将见到你
possible so a huge thank you to all of

340
00:10:32,899 --> 00:10:34,229
可能如此，非常感谢我所有的支持者，我将见到你
my supporters on that I will see you

341
00:10:34,429 --> 00:10:35,609
伙计们，下一次，我想下一次，我们终于要谈论智能指针
guys next time I think next time we're

342
00:10:35,809 --> 00:10:37,019
伙计们，下一次，我想下一次，我们终于要谈论智能指针
gonna finally talk about smart pointers

343
00:10:37,220 --> 00:10:38,990
因为是的，我下次再见
because well

344
00:10:39,190 --> 00:10:42,359
因为是的，我下次再见
yeah I'll see you guys next time goodbye

345
00:10:42,559 --> 00:10:47,559
[音乐]
[Music]

