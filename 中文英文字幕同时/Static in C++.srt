﻿1
00:00:00,000 --> 00:00:03,959
Hey, what's up guys. My name is Van Cherno and welcome back to my C++ series
嘿，大家好。我叫Van Cherno，欢迎回到我的C ++系列

2
00:00:03,959 --> 00:00:08,038
Today just coming at you with a quick video and we're going to talk about what static means in C++
今天，我们为您提供一个快速视频，我们将讨论静态在C ++中的含义

3
00:00:08,039 --> 00:00:11,250
so the static Keyword in C++ actually has two meanings depending on what the
因此，C ++中的静态关键字实际上有两种含义，具体取决于

4
00:00:11,349 --> 00:00:14,189
Context is one of those applies to when you use the static Keyword
上下文是您使用静态关键字时适用的条件之一

5
00:00:14,439 --> 00:00:17,850
Outside of a class or a struct and the other is when you use static
在类或结构之外，另一个是在使用static时

6
00:00:17,980 --> 00:00:24,629
Inside a class or struct. Basically just to cut to the chase, static outside of class means that the linkage of that symbol
在类或结构内。基本上只是为了切入实际，类外部的静态意味着该符号的链接

7
00:00:24,629 --> 00:00:27,329
that you declare to be static is going to be internal meaning.
您声明为静态的将是内部含义。 

8
00:00:27,329 --> 00:00:28,928
It's only going to be visible to that
那只会是可见的

9
00:00:28,928 --> 00:00:33,868
translation unit that you've defined it in. Whereas a static variable inside a class or struct means that
您在其中定义了翻译单元。而在类或结构中的静态变量表示

10
00:00:33,969 --> 00:00:39,239
variable is actually going to share memory with all of the instances of the class meaning that basically
变量实际上将与该类的所有实例共享内存，这基本上意味着

11
00:00:39,609 --> 00:00:45,359
across all instances that you create of that class or struct, there's only going to be one instance of that
在您创建的该类或结构的所有实例中，只会有一个实例

12
00:00:45,549 --> 00:00:49,679
Static variable and a similar thing applies to static methods in a class there is no
静态变量和类似的东西适用于类中的静态方法，没有

13
00:00:50,049 --> 00:00:54,238
Instance of that class being passed into that method. We're going to talk a lot more in depth about what?
该类的实例将传递到该方法中。我们将更深入地讨论什么？ 

14
00:00:54,308 --> 00:00:59,037
Static actually means in a class or struct scope so definitely check out that video
静态实际上意味着在类或结构范围内，因此请务必查看该视频

15
00:00:59,039 --> 00:01:01,829
It'll be a card on the screen and a link in the description below but today
这将是屏幕上的卡片，并且是以下说明中的链接，但今天

16
00:01:01,829 --> 00:01:03,829
We're just going to focus on what static means outside
我们将只关注外部的静态含义

17
00:01:03,878 --> 00:01:09,268
Of that class or struct scope. So let's dive into some code and take a look. So what I'm going to do here in this completely
该类或结构范围的。因此，让我们深入研究一些代码，看看吧。所以我要在这里完全完成

18
00:01:09,269 --> 00:01:12,658
empty C++ file is just define a variable to be static
空的C ++文件只是将变量定义为静态

19
00:01:12,659 --> 00:01:17,129
I'm going to use the s_ convention here to basically indicate that this variable is static
我将在这里使用s_约定来基本上表明该变量是静态的

20
00:01:17,129 --> 00:01:19,588
And I want to just set it equal to five okay cool
我想将其设置为等于5很好

21
00:01:19,590 --> 00:01:22,799
So it looks like any other variable, but it's got the static keyword in front of it
因此它看起来像其他任何变量，但它前面有static关键字

22
00:01:22,799 --> 00:01:24,519
What does that actually mean what it means?
这实际上意味着什么？ 

23
00:01:24,719 --> 00:01:27,778
Is that this variable is only going to be linked?
难道这个变量只会被链接吗？ 

24
00:01:28,118 --> 00:01:29,409
internally inside this
在内部

25
00:01:29,409 --> 00:01:31,438
Translation unit. If you don't know how C++
翻译单位。如果您不知道C ++ 

26
00:01:31,540 --> 00:01:35,790
Compilation or linking works definitely check out my videos on how the c++ compiler works
编译或链接的工作原理肯定可以查看我的视频，了解c ++编译器的工作原理

27
00:01:35,790 --> 00:01:37,949
And how the C++ linker works because you
以及C ++链接器如何工作，因为您

28
00:01:38,170 --> 00:01:40,948
really need to understand what's going on there to understand this.
确实需要了解发生了什么才能了解​​这一点。 

29
00:01:41,078 --> 00:01:46,228
A static variable or function means that when it actually comes time to link those actual functions or variables to
静态变量或函数意味着实际上需要将这些实际函数或变量链接到

30
00:01:46,328 --> 00:01:53,068
Actually define symbols, the linker is not going to look outside of the scope of this translation unit for that symbol definition
实际上定义符号，链接器不会在该翻译单元的范围之外查找该符号定义

31
00:01:53,069 --> 00:01:56,458
Yeah again, this is one of those things that's best explained by examples
是的，这是最好用例子说明的事情之一

32
00:01:56,459 --> 00:02:00,598
Let's take a look. We've clearly created a variable here and set it equal to five what's going to another
让我们来看看。我们显然已经在此处创建了一个变量，并将其设置为等于五，这将导致另一个变量

33
00:02:00,879 --> 00:02:05,518
C++ file into another translation unit yeah, I've just got a blank C++ file with a main function
 C ++文件到另一个翻译单元，是的，我刚得到一个具有主要功能的空白C ++文件

34
00:02:05,530 --> 00:02:10,710
We're going to create a global variable here. I'm going to give it exact same name as the static one
我们将在这里创建一个全局变量。我要给它和静态名称完全一样的名称

35
00:02:10,710 --> 00:02:13,498
I'll set this equal to like ten or something like that
我将其设置为等于十或类似的值

36
00:02:13,629 --> 00:02:19,379
if I try and print this variable you can see that we can compile our code and we're not going to get any problems and
如果我尝试打印此变量，则可以看到我们可以编译代码，并且不会出现任何问题，并且

37
00:02:19,379 --> 00:02:23,698
if I run my code of course we'll get ten printing out to the console however if I go back to my
如果我运行我的代码，我们当然会在控制台上打印十张，但是如果我返回到我的

38
00:02:23,770 --> 00:02:26,549
static dot CPP file and I remove the static Keyword
静态点CPP文件，我删除了静态关键字

39
00:02:26,550 --> 00:02:30,180
And I try and compile my code you'll see that when it gets to the linking stage
我尝试编译我的代码，您会看到它进入链接阶段

40
00:02:30,180 --> 00:02:32,879
we actually get a linking error because this s
我们实际上收到了链接错误，因为

41
00:02:33,189 --> 00:02:35,609
Variable has already been defined in each different
变量已经在每个不同的位置定义

42
00:02:35,710 --> 00:02:41,560
Translation unit so we can't have two global variables with the same name. One of the things I could do to fix this is change
翻译单位，因此我们不能有两个具有相同名称的全局变量。我可以做的一件事就是改变

43
00:02:41,759 --> 00:02:43,759
this variable to actually refer to
该变量实际要引用

44
00:02:44,020 --> 00:02:49,649
this one and the way that I can do that is by getting rid of the assignment here and marking this variable as
这是我可以做到的方法，是摆脱这里的赋值并将该变量标记为

45
00:02:49,840 --> 00:02:53,819
Extern which means that it's going to look for that s variable in an external
 Extern，这意味着它将在外部查找该变量

46
00:02:54,280 --> 00:03:00,058
Translation unit which is called external linkage or external linking. If I run my code now, you can see the value
翻译单元，称为外部链接或外部链接。如果我现在运行代码，则可以看到该值

47
00:03:00,060 --> 00:03:07,349
We get is five because of course it's referencing this s variable. However if I come over here, and I mark this static
我们得到五个是因为它当然是在引用这个变量。但是，如果我来到这里，并且将此标记为静态

48
00:03:07,349 --> 00:03:09,349
It's kind of like declaring a variable
有点像声明一个变量

49
00:03:09,580 --> 00:03:13,859
private in a class no other translation unit is going to see this
私人授课，没有其他翻译单位会看到

50
00:03:14,020 --> 00:03:18,359
s_Variable the Linker will not see this in a global scope which means that if I go back here
 s_Variable链接器不会在全局范围内看到此消息，这意味着如果我回到此处

51
00:03:18,360 --> 00:03:20,620
And I try and compile my code you can see that
我尝试编译我的代码，您会看到

52
00:03:20,819 --> 00:03:27,299
we actually get an unresolved external Sample s_Variable because it cannot find an integer with the name s_Variable anywhere
我们实际上得到了一个未解析的外部Sample s_Variable，因为它无法在任何地方找到名称为s_Variable的整数

53
00:03:27,370 --> 00:03:31,349
Because we've effectively marked our variable as private if we go back here
因为如果我们回到这里，我们已经有效地将变量标记为私有

54
00:03:31,349 --> 00:03:36,628
I'm also going to declare a function to show you guys that it does the same thing if I declare a function here called
我还要声明一个函数，向大家展示，如果我在这里声明一个函数，它会执行相同的操作

55
00:03:36,819 --> 00:03:37,659
function and
功能和

56
00:03:37,659 --> 00:03:38,969
another function over here
这里的另一个功能

57
00:03:38,969 --> 00:03:44,248
Which has the same signature is also going to return nothing is going to be void if I try and compile this, now
具有相同签名的也将返回任何东西，如果我尝试编译的话，它将是无效的，现在

58
00:03:44,250 --> 00:03:48,149
I'm going to get a duplicate symbol error at the linking stage because there are two
在链接阶段，我将收到重复的符号错误，因为有两个

59
00:03:48,430 --> 00:03:50,520
functions if I go over here into static
功能，如果我在这里进入静态

60
00:03:50,520 --> 00:03:52,030
and I mark this one as
我将此标记为

61
00:03:52,030 --> 00:03:57,240
Static then of course when the linker stuff to link all this it's not going to see the static one and thus I'm not going
静态的，当然，当链接器的东西链接所有这些东西时，它不会看到静态的，因此我不会

62
00:03:57,240 --> 00:04:01,780
To get any errors let's try and compile this you can see I don't get the error anymore for the function
要获取任何错误，请尝试进行编译，您可以看到我不再收到该函数的错误

63
00:04:01,979 --> 00:04:03,329
I still get the s_Variable one
我仍然得到s_Variable 

64
00:04:03,330 --> 00:04:07,719
let's just get rid of that and
让我们摆脱它， 

65
00:04:07,719 --> 00:04:10,709
the print as well. If I compile this we get no errors. So that's really all the static means when you use it outside
印刷品。如果我对此进行编译，则不会出错。所以当您在室外使用时，这就是所有静态方法

66
00:04:10,709 --> 00:04:15,419
of a class or a struct in C++. It just means that when you declare that static function
 C ++中的类或结构。这只是意味着当您声明该静态函数时

67
00:04:15,419 --> 00:04:21,088
Or that static variable is only going to be visible to that C++ file that you've declared it in
否则该静态变量将只对您在其中声明了该C ++文件可见

68
00:04:21,279 --> 00:04:26,409
if you want to declare a static variable in a header file and include that header file in two different
如果要在头文件中声明静态变量并将该头文件包含在两个不同的文件中

69
00:04:26,600 --> 00:04:32,379
C++ files and effectively what you've done is what I've kind of done with my example where I've had that same
 C ++文件，实际上您所做的就是我在示例中所做的事

70
00:04:32,509 --> 00:04:34,569
s_Variable declared as a static variable in both
 s_Variable都声明为静态变量

71
00:04:35,180 --> 00:04:38,410
Translation units because of course when you include that header file
翻译单元，因为当您包含该头文件时

72
00:04:38,410 --> 00:04:42,939
It's going to copy all the contents and paste it into that C++ file
复制所有内容并将其粘贴到该C ++文件中

73
00:04:42,939 --> 00:04:43,569
So what you've done is
所以你要做的是

74
00:04:43,569 --> 00:04:48,938
You just created a static variable in two different translation units as to why you would use this think about why you would use
您刚刚在两个不同的翻译单元中创建了一个静态变量，以了解为什么要使用此原因，并考虑为什么要使用

75
00:04:49,069 --> 00:04:52,808
private in a class kind of environment you basically want to use static as
在类环境中是私有的，您基本上想使用static作为

76
00:04:52,970 --> 00:04:59,709
Much as you can if you don't need the variable to be global because as soon as you start declaring things in a global scope
如果不需要变量是全局变量，则可以做很多事情，因为您一开始在全局范围内声明事物

77
00:04:59,769 --> 00:05:01,639
without that static Keyword
没有该静态关键字

78
00:05:01,639 --> 00:05:07,149
You can see that the link is going to pick it up across translation units which means that you've created a variable that is
您会看到该链接将跨翻译单元进行选择，这意味着您已经创建了一个变量

79
00:05:07,430 --> 00:05:11,439
Completely global. If I create a variable with the name variable, and it's in a global setting
完全全球化。如果我使用name变量创建一个变量，并且该变量处于全局设置中

80
00:05:12,110 --> 00:05:13,319
Suddenly
突然

81
00:05:13,519 --> 00:05:17,649
That that name variable is completely global and can be used anywhere
该名称变量是完全全局的，可以在任何地方使用

82
00:05:17,649 --> 00:05:23,738
And that can lead to some pretty bad bugs and that comes down to the whole thing that global variables are bad
这可能会导致一些非常糟糕的错误，并且可以归结为全局变量很糟糕。 

83
00:05:23,740 --> 00:05:27,160
Which I don't particularly agree with I'll probably make a video about that in the future
我特别不同意的是，我将来可能会制作一个视频

84
00:05:27,160 --> 00:05:33,609
But the point is try and mark your functions and your variable static unless you actually need them to be linked across
但是关键是要尝试将函数和变量标记为静态，除非您实际上需要跨它们链接

85
00:05:33,860 --> 00:05:39,610
Translation units. As I mentioned earlier in the video if you want to know more about this in a class or a struct setting click on
翻译单位。正如我在视频中前面提到的，如果您想在类或结构设置中了解更多信息，请单击

86
00:05:39,610 --> 00:05:41,470
The screen You'll be taken to that video
屏幕将带您到该视频

87
00:05:41,470 --> 00:05:43,968
But I hope you guys enjoyed this video if you did please hit that like button
但是我希望你们喜欢这个视频，请点击喜欢的按钮

88
00:05:44,168 --> 00:05:49,568
Let me know what you think in the comments below if you want any kind of clarification you also follow me on Twitter and Instagram
让我知道您在以下评论中的想法，如果您需要任何澄清，也可以在Twitter和Instagram上关注我

89
00:05:49,850 --> 00:05:52,749
Links will be in the description below if you really like this video
如果您确实喜欢此视频，则链接将在下面的描述中

90
00:05:52,750 --> 00:05:57,160
And you want to see a lot more of them you can support me on Patreon.com/TheCherno
如果您想了解更多，可以在Patreon.com/TheCherno上支持我

91
00:05:57,160 --> 00:06:02,769
If you do that you'll also receive early draft of upcoming videos as well as a nice kind of group discussion where you can talk about?
如果这样做，您还将收到即将到来的视频的早期草稿，以及可以在其中谈论的一种很好的小组讨论方式。 

92
00:06:02,959 --> 00:06:07,538
What actually goes into these videos and request topics and all of that fun stuff man?
这些视频实际上是什么内容并要求主题以及所有这些有趣的东西？ 

93
00:06:07,538 --> 00:06:12,538
I will now to focus the whole time with mine great, but anyway, I'll see you guys later. Good. Bye
现在，我将把整个时间都集中在我的上，但是无论如何，我稍后再见。好。再见

