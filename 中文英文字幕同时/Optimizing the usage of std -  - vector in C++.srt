﻿1
00:00:00,000 --> 00:00:01,269
嘿，大家好，我的名字是他们回到我的C ++系列的渠道
hey what's up guys my name is the

2
00:00:01,469 --> 00:00:03,369
嘿，大家好，我的名字是他们回到我的C ++系列的渠道
channel of them back to my C++ series

3
00:00:03,569 --> 00:00:04,929
上一次我们谈论C ++中的动态数组，特别是回到
last time we talked about dynamic arrays

4
00:00:05,129 --> 00:00:06,490
上一次我们谈论C ++中的动态数组，特别是回到
in C++ and specifically bit back to

5
00:00:06,690 --> 00:00:07,988
上课，如果你们还没看过那部影片，一定要先检查一下
class if you guys haven't seen that

6
00:00:08,189 --> 00:00:09,760
上课，如果你们还没看过那部影片，一定要先检查一下
video definitely check that out first

7
00:00:09,960 --> 00:00:11,769
这将涵盖动态数组实际上是什么以及如何
that's going to cover the basics of what

8
00:00:11,968 --> 00:00:13,419
这将涵盖动态数组实际上是什么以及如何
a dynamic array actually is and how to

9
00:00:13,619 --> 00:00:15,609
在C ++中使用向量类，这里的视频将向您展示如何
use the vector class in C++ and this

10
00:00:15,808 --> 00:00:17,620
在C ++中使用向量类，这里的视频将向您展示如何
video here is going to show you how you

11
00:00:17,820 --> 00:00:19,269
可能以更优化的方式使用该向量类
might use that vector class in a more

12
00:00:19,469 --> 00:00:21,460
可能以更优化的方式使用该向量类
optimal way a little bit about how it

13
00:00:21,660 --> 00:00:23,109
正常工作，并且一般来说我们如何编写我们的代码，所以这仅仅是一个
works and just in general how we can

14
00:00:23,309 --> 00:00:24,850
正常工作，并且一般来说我们如何编写我们的代码，所以这仅仅是一个
write our code so that it's just one

15
00:00:25,050 --> 00:00:26,560
更快，这令人兴奋，因为这就像我们的第一种
faster this is kind of exciting because

16
00:00:26,760 --> 00:00:28,120
更快，这令人兴奋，因为这就像我们的第一种
this is like our first kind of

17
00:00:28,320 --> 00:00:30,940
优化视频，在本系列课程中，我们将成为
optimization video and over the course

18
00:00:31,140 --> 00:00:32,529
优化视频，在本系列课程中，我们将成为
of this series we are going to be

19
00:00:32,729 --> 00:00:34,149
谈论更多关于优化的问题，因为它们专门用于公共汽车
talking a lot more about optimization

20
00:00:34,350 --> 00:00:35,589
谈论更多关于优化的问题，因为它们专门用于公共汽车
because they bus bus is specifically a

21
00:00:35,789 --> 00:00:37,719
与优化配合得很好的语言是很多语言
language which plays nicely with

22
00:00:37,920 --> 00:00:39,250
与优化配合得很好的语言是很多语言
optimization is a language that many

23
00:00:39,450 --> 00:00:40,659
人们之所以选择，是因为他们可能需要一些较低级别的优化
people pick due to the fact that they

24
00:00:40,859 --> 00:00:42,729
人们之所以选择，是因为他们可能需要一些较低级别的优化
might need some lower-level optimization

25
00:00:42,929 --> 00:00:44,919
最重要的优化规则之一就是基本了解您的
and one of the most important rules of

26
00:00:45,119 --> 00:00:46,509
最重要的优化规则之一就是基本了解您的
optimization is basically knowing your

27
00:00:46,710 --> 00:00:48,099
环境，我的意思是了解您的环境是如何工作的
environment and what I mean by knowing

28
00:00:48,299 --> 00:00:49,869
环境，我的意思是了解您的环境是如何工作的
your environment is how do things work

29
00:00:50,070 --> 00:00:52,799
我需要做些什么，应该正确做些什么，那就是
and what do I need to do exactly and

30
00:00:53,000 --> 00:00:55,719
我需要做些什么，应该正确做些什么，那就是
what should happen right and that's

31
00:00:55,920 --> 00:00:57,579
这本身就是一个非常复杂的主题，我们将更深入地研究它
that's such a complex topic in itself

32
00:00:57,780 --> 00:00:59,890
这本身就是一个非常复杂的主题，我们将更深入地研究它
that we are going to dive deeper into it

33
00:01:00,090 --> 00:01:01,448
随着系列的进行，但是今天我们要刮擦表面，
as the series goes on but for today

34
00:01:01,649 --> 00:01:03,640
随着系列的进行，但是今天我们要刮擦表面，
we're just gonna scratch the surface and

35
00:01:03,840 --> 00:01:04,748
看一下我们可以用vector的例子具体做什么
take a look at what we can do

36
00:01:04,948 --> 00:01:07,060
看一下我们可以用vector的例子具体做什么
specifically with the example of vector

37
00:01:07,260 --> 00:01:09,189
因此，要优化向量的使用，您确实应该知道向量的工作原理以及
so to optimize your vector usage you

38
00:01:09,390 --> 00:01:11,469
因此，要优化向量的使用，您确实应该知道向量的工作原理以及
really should know how vector works and

39
00:01:11,670 --> 00:01:13,750
您如何可以将其更改为更好地工作，所以基本上
how you can maybe change it to work a

40
00:01:13,950 --> 00:01:15,308
您如何可以将其更改为更好地工作，所以基本上
little bit better so basically the way

41
00:01:15,509 --> 00:01:16,750
标准向量加实际起作用的是您创建一个向量，然后
that the standard vector plus actually

42
00:01:16,950 --> 00:01:19,390
标准向量加实际起作用的是您创建一个向量，然后
works is you create a vector and then

43
00:01:19,590 --> 00:01:21,429
您开始向后推元素，然后向该数组中添加元素
you start pushing back elements and when

44
00:01:21,629 --> 00:01:23,619
您开始向后推元素，然后向该数组中添加元素
you add elements into that array to that

45
00:01:23,819 --> 00:01:26,500
如果向量的容量不大，则向量的大小
vector if the size of the vector if the

46
00:01:26,700 --> 00:01:28,569
如果向量的容量不大，则向量的大小
capacity of the vector is not large

47
00:01:28,769 --> 00:01:30,369
足以包含此新元素，而您基本上想添加
enough to contain this new element that

48
00:01:30,569 --> 00:01:31,689
足以包含此新元素，而您基本上想添加
you're trying to add basically what

49
00:01:31,890 --> 00:01:33,399
需要发生的是向量需要分配至少为
needs to happen is the vector needs to

50
00:01:33,599 --> 00:01:35,799
需要发生的是向量需要分配至少为
allocate new memory that is at least

51
00:01:36,000 --> 00:01:37,988
足够大以容纳您要添加的新元素以及
large enough to hold this new element

52
00:01:38,188 --> 00:01:39,668
足够大以容纳您要添加的新元素以及
you're trying to add as well as the

53
00:01:39,868 --> 00:01:41,980
向量的当前内容，并复制旧位置中的所有内容
current contents of the vector and copy

54
00:01:42,180 --> 00:01:43,988
向量的当前内容，并复制旧位置中的所有内容
everything across from that old location

55
00:01:44,188 --> 00:01:45,939
在内存中放入新位置，然后删除该旧位置
in memory into the new location in

56
00:01:46,140 --> 00:01:47,769
在内存中放入新位置，然后删除该旧位置
memory and then delete that old location

57
00:01:47,969 --> 00:01:49,539
这就是发生的情况，因此当我们尝试将元素推回时，如果
and that's what happens and so when we

58
00:01:49,739 --> 00:01:51,789
这就是发生的情况，因此当我们尝试将元素推回时，如果
try and push back an element if the

59
00:01:51,989 --> 00:01:53,679
容量用完了，它执行了重新分配大小和
capacity runs out it performs that

60
00:01:53,879 --> 00:01:55,929
容量用完了，它执行了重新分配大小和
resizing that reallocation and that

61
00:01:56,129 --> 00:01:59,439
特别是造成缓慢代码的原因之一是，如果我们
specifically is one cause of kind of

62
00:01:59,640 --> 00:02:00,939
特别是造成缓慢代码的原因之一是，如果我们
slow code it's the fact that if we

63
00:02:01,140 --> 00:02:02,859
不断需要重新分配这是一个缓慢的操作，我们需要重新分配
continually need to reallocate that's a

64
00:02:03,060 --> 00:02:04,480
不断需要重新分配这是一个缓慢的操作，我们需要重新分配
slow operation we need to reallocate

65
00:02:04,680 --> 00:02:06,340
当我们需要复制所有现有元素时，我们想要
when we need to copy all of our existing

66
00:02:06,540 --> 00:02:08,050
当我们需要复制所有现有元素时，我们想要
elements that's something we want to

67
00:02:08,250 --> 00:02:09,850
避免，实际上这将是我们的优化策略
avoid and in fact that's going to

68
00:02:10,050 --> 00:02:11,980
避免，实际上这将是我们的优化策略
largely be our optimization strategy

69
00:02:12,180 --> 00:02:13,610
今天复制我们如何避免复制我们的对象，如果
today copying

70
00:02:13,810 --> 00:02:16,789
今天复制我们如何避免复制我们的对象，如果
how can we avoid copying our objects if

71
00:02:16,989 --> 00:02:18,110
我们正在处理向量，特别是处理基于对象的
we're dealing with vectors and

72
00:02:18,310 --> 00:02:19,759
我们正在处理向量，特别是处理基于对象的
specifically dealing with object based

73
00:02:19,959 --> 00:02:21,170
向量，所以我们没有存储更好的指针，实际上是存储向量
vectors so we're not storing a better of

74
00:02:21,370 --> 00:02:22,759
向量，所以我们没有存储更好的指针，实际上是存储向量
pointers were actually storing a vector

75
00:02:22,959 --> 00:02:24,560
现在我说过了解对象的环境是最
of objects now I've said that knowing

76
00:02:24,759 --> 00:02:25,730
现在我说过了解对象的环境是最
your environment is one of the most

77
00:02:25,930 --> 00:02:27,710
优化的重要内容，现在我们正在尝试优化
important things with optimization and

78
00:02:27,909 --> 00:02:29,719
优化的重要内容，现在我们正在尝试优化
right now we're trying to optimize for

79
00:02:29,919 --> 00:02:32,240
复制，因此我们需要知道何时实际发生复制以及为何发生复制
copying so we need to know when copies

80
00:02:32,439 --> 00:02:34,039
复制，因此我们需要知道何时实际发生复制以及为何发生复制
actually happen and why they happen

81
00:02:34,239 --> 00:02:35,719
让我们看一下卡片，然后从上一个视频中找出答案
let's take a look at some card and

82
00:02:35,919 --> 00:02:37,368
让我们看一下卡片，然后从上一个视频中找出答案
figure that out so from the last video I

83
00:02:37,568 --> 00:02:38,990
有了这个顶点类，我刚刚添加了一个构造函数，但是基本上
had this vertex class I just added a

84
00:02:39,189 --> 00:02:40,850
有了这个顶点类，我刚刚添加了一个构造函数，但是基本上
constructor to it but we basically have

85
00:02:41,050 --> 00:02:42,289
这种默认部分很简单，我们有两个顶点
this kind of default part it's pretty

86
00:02:42,489 --> 00:02:44,390
这种默认部分很简单，我们有两个顶点
simple we have our two vertices that

87
00:02:44,590 --> 00:02:46,700
我们什么都不想花钱，现在让我们看一下
we're pushing back nothing too fancy now

88
00:02:46,900 --> 00:02:47,480
我们什么都不想花钱，现在让我们看一下
let's take a look at what actually

89
00:02:47,680 --> 00:02:49,280
发生在幕后，并确定实际上有多少副本
happens behind the scenes and identify

90
00:02:49,479 --> 00:02:51,170
发生在幕后，并确定实际上有多少副本
how many if any copies are actually

91
00:02:51,370 --> 00:02:53,180
当前代码发生这种情况，所以这样做的好方法是添加一个副本
happening with this current code so a

92
00:02:53,379 --> 00:02:54,830
当前代码发生这种情况，所以这样做的好方法是添加一个副本
good way to do that is to add a copy

93
00:02:55,030 --> 00:02:57,050
顶点的构造函数，也许在其中放置一个断点或只是打印
constructor to vertex and maybe either

94
00:02:57,250 --> 00:02:58,939
顶点的构造函数，也许在其中放置一个断点或只是打印
put a breakpoint in there or just print

95
00:02:59,139 --> 00:03:00,800
到控制台的东西，以查看复制构造函数实际何时获得
something to the console to see when

96
00:03:01,000 --> 00:03:02,240
到控制台的东西，以查看复制构造函数实际何时获得
that copy constructor actually gets

97
00:03:02,439 --> 00:03:04,160
叫，所以我要做的就是写一个拷贝构造函数
called so what I'm going to do is just

98
00:03:04,360 --> 00:03:05,780
叫，所以我要做的就是写一个拷贝构造函数
write a copy constructor if you're not

99
00:03:05,979 --> 00:03:07,550
确定复制构造函数的内容或复制在C ++中的实际工作方式，然后
sure what a copy constructor ears or how

100
00:03:07,750 --> 00:03:09,380
确定复制构造函数的内容或复制在C ++中的实际工作方式，然后
copying actually works in C++ then

101
00:03:09,580 --> 00:03:10,520
一定要检查右上角链接的视频，但是我
definitely check out the video that's

102
00:03:10,719 --> 00:03:12,439
一定要检查右上角链接的视频，但是我
linked in the top right corner but I'm

103
00:03:12,639 --> 00:03:14,118
只是要为这个顶点类编写一个非常简单的副本构造函数
just going to write a very very simple

104
00:03:14,318 --> 00:03:15,920
只是要为这个顶点类编写一个非常简单的副本构造函数
copy constructor for this vertex class

105
00:03:16,120 --> 00:03:17,870
现在，就本视频而言，实际上并不需要正确填写
now for the purposes of this video don't

106
00:03:18,069 --> 00:03:18,980
现在，就本视频而言，实际上并不需要正确填写
really need to fill it out properly but

107
00:03:19,180 --> 00:03:20,360
无论如何，我都不是要打印的内容复制到这里到我们的控制台
I don't so anyway what I'm going to do

108
00:03:20,560 --> 00:03:23,180
无论如何，我都不是要打印的内容复制到这里到我们的控制台
is print copied over here to our console

109
00:03:23,379 --> 00:03:24,319
好让我们来看一下，让我们运行这段代码好吧，我们得到了三个
so that we can take a look at that and

110
00:03:24,519 --> 00:03:27,080
好让我们来看一下，让我们运行这段代码好吧，我们得到了三个
let's run this code okay so we get three

111
00:03:27,280 --> 00:03:29,480
复制有趣的东西，实际上我要在这里添加另一个元素
copies interesting I'm actually going to

112
00:03:29,680 --> 00:03:31,670
复制有趣的东西，实际上我要在这里添加另一个元素
add another element here and this time

113
00:03:31,870 --> 00:03:33,110
因为我可以只使用实际的构造函数
since I can I'm just gonna use the

114
00:03:33,310 --> 00:03:34,430
因为我可以只使用实际的构造函数
actual constructor just so that this

115
00:03:34,629 --> 00:03:36,740
代码看起来更容易阅读，所以我将其设为7到9，实际上
code looks a bit easier to read so I'll

116
00:03:36,939 --> 00:03:39,980
代码看起来更容易阅读，所以我将其设为7到9，实际上
make this seven a nine and in fact I'm

117
00:03:40,180 --> 00:03:42,500
还要压缩我现有的构造器以使用这种新的构造函数
going to squish over my existing ones as

118
00:03:42,699 --> 00:03:44,360
还要压缩我现有的构造器以使用这种新的构造函数
well to use this kind of new constructor

119
00:03:44,560 --> 00:03:45,830
我所做的只是原始的默认构造函数
that I've made this is just the original

120
00:03:46,030 --> 00:03:47,420
我所做的只是原始的默认构造函数
default constructor it does exactly the

121
00:03:47,620 --> 00:03:49,310
与之前的代码相同，但我认为可能只是一点点
same thing as the code before but I

122
00:03:49,509 --> 00:03:50,450
与之前的代码相同，但我认为可能只是一点点
think it might just be a little bit

123
00:03:50,650 --> 00:03:51,830
更容易阅读，因为您确切知道发生了什么，所以如果我运行此程序
easier to read because you know exactly

124
00:03:52,030 --> 00:03:53,330
更容易阅读，因为您确切知道发生了什么，所以如果我运行此程序
what's happening okay so if I run this

125
00:03:53,530 --> 00:03:56,270
代码您希望问有多少份，希望有六份，现在我们有六份
code how many copies do expect six we

126
00:03:56,469 --> 00:03:58,849
代码您希望问有多少份，希望有六份，现在我们有六份
have six copies now you might be asking

127
00:03:59,049 --> 00:04:02,030
自己，为什么会这样，为什么C ++在其中复制我的顶点六次
yourself why why is that happening why

128
00:04:02,229 --> 00:04:05,569
自己，为什么会这样，为什么C ++在其中复制我的顶点六次
is C++ copying my vertex six times in

129
00:04:05,769 --> 00:04:07,610
总的说来，这将进一步调试它，我将放置一个
total what's going on to debug this a

130
00:04:07,810 --> 00:04:09,020
总的说来，这将进一步调试它，我将放置一个
little bit further I'm gonna place a

131
00:04:09,219 --> 00:04:10,909
在此行上设置断点，然后再次运行此代码，因此在
breakpoint on this line and run this

132
00:04:11,109 --> 00:04:12,710
在此行上设置断点，然后再次运行此代码，因此在
code again so we have zero copies at the

133
00:04:12,909 --> 00:04:13,789
当然是因为我们见面的那一刻，我们甚至都没有退缩任何东西，让我们
moment of course because we've met we

134
00:04:13,989 --> 00:04:15,050
当然是因为我们见面的那一刻，我们甚至都没有退缩任何东西，让我们
haven't even pushed anything back let's

135
00:04:15,250 --> 00:04:17,810
让我们按f10，所以现在我们将一个元素推回一个顶点，我们有一个
let's hit f10 so now we've pushed one

136
00:04:18,009 --> 00:04:20,240
让我们按f10，所以现在我们将一个元素推回一个顶点，我们有一个
element back one vertex and we have a

137
00:04:20,439 --> 00:04:23,000
正确复制为什么会发生的原因正在发生的原因是
copy right why is that happening the

138
00:04:23,199 --> 00:04:24,259
正确复制为什么会发生的原因正在发生的原因是
reason that's happening is

139
00:04:24,459 --> 00:04:26,270
因为当我们构造顶点时，我们实际上是在
because when we construct our vertex

140
00:04:26,470 --> 00:04:28,278
因为当我们构造顶点时，我们实际上是在
we're actually constructing it in the

141
00:04:28,478 --> 00:04:30,680
主要功能的当前堆栈框架，因此我们将其构建在堆栈中
current stack frame of the main function

142
00:04:30,879 --> 00:04:32,119
主要功能的当前堆栈框架，因此我们将其构建在堆栈中
so we're constructing it on the stack in

143
00:04:32,319 --> 00:04:35,300
主要，然后需要发生的是，我们需要将其放入
main and then what needs to happen is we

144
00:04:35,500 --> 00:04:36,980
主要，然后需要发生的是，我们需要将其放入
need to put it into that into that

145
00:04:37,180 --> 00:04:39,468
向量正确，所以我们需要以某种方式从主类
vector right so we need to somehow get

146
00:04:39,668 --> 00:04:41,749
向量正确，所以我们需要以某种方式从主类
it from that main class from the main

147
00:04:41,949 --> 00:04:43,910
函数而不是从主函数到实际向量的类函数
function rather not class function from

148
00:04:44,110 --> 00:04:46,460
函数而不是从主函数到实际向量的类函数
the main function into the actual vector

149
00:04:46,660 --> 00:04:48,350
向量已分配到内存中，当然，我们这样做的方式是
into the memory the vector has allocated

150
00:04:48,550 --> 00:04:50,059
向量已分配到内存中，当然，我们这样做的方式是
and of course the way that we do that is

151
00:04:50,259 --> 00:04:52,249
我们将其从主函数复制到向量类中，这是我们的第一类
we copy it from the main function into

152
00:04:52,449 --> 00:04:54,110
我们将其从主函数复制到向量类中，这是我们的第一类
the vector class that's our first kind

153
00:04:54,310 --> 00:04:55,460
错误是我们可以优化的第一件事，如果我们可以构建
of mistake that's the first thing we can

154
00:04:55,660 --> 00:04:57,680
错误是我们可以优化的第一件事，如果我们可以构建
optimize what if we could just construct

155
00:04:57,879 --> 00:05:00,139
向量实际在实际内存中位于适当位置的那个顶点
that vertex in place in the actual

156
00:05:00,339 --> 00:05:02,990
向量实际在实际内存中位于适当位置的那个顶点
memory that the vector has actually

157
00:05:03,189 --> 00:05:03,980
为我们分配的优化策略是第一
allocated for us

158
00:05:04,180 --> 00:05:05,569
为我们分配的优化策略是第一
that's optimization strategy number one

159
00:05:05,769 --> 00:05:06,860
我们将在一秒钟内实现，但让我们继续
which we'll implement in a second but

160
00:05:07,060 --> 00:05:07,550
我们将在一秒钟内实现，但让我们继续
let's keep going

161
00:05:07,750 --> 00:05:09,949
现在如果我得到F 10的话，我们会再得到两份，所以如果没有三份
now if I get F 10 over here we get two

162
00:05:10,149 --> 00:05:12,230
现在如果我得到F 10的话，我们会再得到两份，所以如果没有三份
more copies so if not went out three

163
00:05:12,430 --> 00:05:14,480
复制正确，所以我们已经知道发生了什么，我们已经知道其中之一
copies right so we already know what's

164
00:05:14,680 --> 00:05:16,430
复制正确，所以我们已经知道发生了什么，我们已经知道其中之一
happened we already know where one of

165
00:05:16,629 --> 00:05:18,350
副本已经消失了，因为我们在上面构造了这个顶点对象
the copies has gone us because we were

166
00:05:18,550 --> 00:05:20,449
副本已经消失了，因为我们在上面构造了这个顶点对象
constructing this vertex object on

167
00:05:20,649 --> 00:05:22,369
在主函数内部，然后将其推入顶点向量，这会导致
inside the main function then pushing it

168
00:05:22,569 --> 00:05:24,528
在主函数内部，然后将其推入顶点向量，这会导致
into the vertices vector which causes a

169
00:05:24,728 --> 00:05:26,360
复制，但是为什么我们要再获得一份副本，如果发生了什么情况
copy but then why are we getting an

170
00:05:26,560 --> 00:05:28,189
复制，但是为什么我们要再获得一份副本，如果发生了什么情况
additional copy what's going on well if

171
00:05:28,389 --> 00:05:30,079
我们将鼠标悬停在上面，然后看一下实际的顶点矢量
we hover our mouse over this and we take

172
00:05:30,279 --> 00:05:32,360
我们将鼠标悬停在上面，然后看一下实际的顶点矢量
a look at the actual vertices vector we

173
00:05:32,560 --> 00:05:35,240
可以看到容量是两个，这意味着这个向量
can see the capacity is two what that

174
00:05:35,439 --> 00:05:37,449
可以看到容量是两个，这意味着这个向量
means is that this this vector

175
00:05:37,649 --> 00:05:40,370
物理上具有足够的内存来存储两个顶点或两个顶点
physically has enough memory to store

176
00:05:40,569 --> 00:05:42,800
物理上具有足够的内存来存储两个顶点或两个顶点
two vertices or two vertex

177
00:05:43,000 --> 00:05:45,110
对象，所以发生的当然是当我们尝试推动另一个
objects so what happens of course is

178
00:05:45,310 --> 00:05:47,360
对象，所以发生的当然是当我们尝试推动另一个
when we try and push another one on like

179
00:05:47,560 --> 00:05:50,269
第三个，我按了f10，它需要将容量调整为三到两个
this third one and I hit f10 it needs to

180
00:05:50,468 --> 00:05:52,490
第三个，我按了f10，它需要将容量调整为三到两个
resize the capacity to three or two

181
00:05:52,689 --> 00:05:54,110
基本上任何大于两个的东西，以便我们实际上可以有足够的内存
anything higher than two basically so

182
00:05:54,310 --> 00:05:55,490
基本上任何大于两个的东西，以便我们实际上可以有足够的内存
that we can actually have enough memory

183
00:05:55,689 --> 00:05:58,069
推入第三个顶点，这是另一种潜在的优化策略
to push in a third vertex so that's

184
00:05:58,269 --> 00:05:59,809
推入第三个顶点，这是另一种潜在的优化策略
another potential optimization strategy

185
00:06:00,009 --> 00:06:01,790
我们的向量在这里被调整了两次，只是默认情况下它是一个
our vector is being resized twice here

186
00:06:01,990 --> 00:06:04,910
我们的向量在这里被调整了两次，只是默认情况下它是一个
it just it's it's at one by default it

187
00:06:05,110 --> 00:06:06,649
然后当我们拥有第二个元素时移至两个，然后移至三个
then moves to two when we have the

188
00:06:06,848 --> 00:06:08,240
然后当我们拥有第二个元素时移至两个，然后移至三个
second element and then moves to three

189
00:06:08,439 --> 00:06:10,579
如果我们知道我们的环境，那么我们添加第三个元素时
when we add the third element if we know

190
00:06:10,779 --> 00:06:13,790
如果我们知道我们的环境，那么我们添加第三个元素时
our environment if we know that we are

191
00:06:13,990 --> 00:06:17,019
计划推入三个顶点对象为什么我们不告诉向量嘿
planning to push in three vertex objects

192
00:06:17,218 --> 00:06:20,139
计划推入三个顶点对象为什么我们不告诉向量嘿
why don't we just tell the vector hey

193
00:06:20,339 --> 00:06:23,149
将其腾出三个内存，这样您就不必两次调整大小
make it off memory for three so that you

194
00:06:23,348 --> 00:06:25,309
将其腾出三个内存，这样您就不必两次调整大小
don't have to resize yourself twice just

195
00:06:25,509 --> 00:06:26,629
从一开始就让三个内存足够，因为我打算
make enough memory from three from the

196
00:06:26,829 --> 00:06:28,009
从一开始就让三个内存足够，因为我打算
beginning because I plan to actually

197
00:06:28,209 --> 00:06:29,269
引入三个要素即优化策略第二
push in three elements

198
00:06:29,468 --> 00:06:31,129
引入三个要素即优化策略第二
that's optimization strategy number two

199
00:06:31,329 --> 00:06:32,540
我们也会在一分钟内实施，这就是这些
which we'll implement in a minute as

200
00:06:32,740 --> 00:06:35,360
我们也会在一分钟内实施，这就是这些
well so that is the idea of where these

201
00:06:35,560 --> 00:06:36,730
共有六份，让我们快速而轻松地进行
six copies are coming from

202
00:06:36,930 --> 00:06:38,439
共有六份，让我们快速而轻松地进行
so let's do the quick and easy

203
00:06:38,639 --> 00:06:40,960
优化策略，我想让我们告诉向量，嘿，我要你取悦
optimization strategy I guess let's just

204
00:06:41,160 --> 00:06:43,960
优化策略，我想让我们告诉向量，嘿，我要你取悦
tell the vector hey I want you to please

205
00:06:44,160 --> 00:06:45,790
具有三的容量以及我们可以告诉向量的方式
have the capacity of three and the way

206
00:06:45,990 --> 00:06:47,800
具有三的容量以及我们可以告诉向量的方式
that we can tell the vector I want to

207
00:06:48,000 --> 00:06:50,079
具有三个的容量是通过在创建设置顶点后简单完成的
have the capacity of three is by simply

208
00:06:50,279 --> 00:06:52,300
具有三个的容量是通过在创建设置顶点后简单完成的
after we created setting vertices dot

209
00:06:52,500 --> 00:06:55,329
保留三个，您可以看到现在需要容纳一个
Reserve three and you can see that that

210
00:06:55,529 --> 00:06:57,129
保留三个，您可以看到现在需要容纳一个
takes in a capacity now this is

211
00:06:57,329 --> 00:06:59,620
与调整大小不同，或者如果我们要在构造函数中将它们传递给它们三，
different to resize or if we were to

212
00:06:59,819 --> 00:07:01,509
与调整大小不同，或者如果我们要在构造函数中将它们传递给它们三，
pass them three in the constructor if we

213
00:07:01,709 --> 00:07:02,800
实际上尝试将它们传递给构造函数中的三个，该代码甚至无法编译
actually try and pass them three in the

214
00:07:03,000 --> 00:07:04,600
实际上尝试将它们传递给构造函数中的三个，该代码甚至无法编译
constructor this code won't even compile

215
00:07:04,800 --> 00:07:06,670
你可以看到我是否将其移动到顶点，
and you can see if I just move this up

216
00:07:06,870 --> 00:07:08,740
你可以看到我是否将其移动到顶点，
there that we're getting as vertex no

217
00:07:08,939 --> 00:07:09,910
适当的默认构造函数可用，因为这会
appropriate default constructor

218
00:07:10,110 --> 00:07:10,900
适当的默认构造函数可用，因为这会
available because what this will

219
00:07:11,100 --> 00:07:13,360
实际上不只是分配足够的内存来停顿三个或三个顶点
actually do is not just allocate enough

220
00:07:13,560 --> 00:07:15,550
实际上不只是分配足够的内存来停顿三个或三个顶点
memory to stall three vertices or three

221
00:07:15,750 --> 00:07:17,230
顶点对象实际上是要构造三个我们不想要的顶点对象
vertex objects it's actually going to

222
00:07:17,430 --> 00:07:19,180
顶点对象实际上是要构造三个我们不想要的顶点对象
construct three vertex objects we don't

223
00:07:19,379 --> 00:07:20,740
想构造任何对象，但我们只想拥有足够的内存来容纳
want to construct any objects yet we

224
00:07:20,939 --> 00:07:22,540
想构造任何对象，但我们只想拥有足够的内存来容纳
just want to have enough memory to hold

225
00:07:22,740 --> 00:07:24,730
他们就是储备金的职责储备金确保我们实际上有足够的
them that's what Reserve does Reserve

226
00:07:24,930 --> 00:07:25,930
他们就是储备金的职责储备金确保我们实际上有足够的
makes sure that we actually have enough

227
00:07:26,129 --> 00:07:28,180
内存，所以第一步是我们创建此顶点向量，然后保留
memory so the first step is we create

228
00:07:28,379 --> 00:07:30,550
内存，所以第一步是我们创建此顶点向量，然后保留
this vertices vector and then we reserve

229
00:07:30,750 --> 00:07:32,680
三个，然后我们推回元素，让我们看一下我们有多少个副本
three and then we push back our elements

230
00:07:32,879 --> 00:07:34,449
三个，然后我们推回元素，让我们看一下我们有多少个副本
let's take a look at how many copies we

231
00:07:34,649 --> 00:07:35,920
现在通过添加这一行代码来了解这三点
now get with this with the addition of

232
00:07:36,120 --> 00:07:37,749
现在通过添加这一行代码来了解这三点
this one line of code look at that three

233
00:07:37,949 --> 00:07:39,699
副本，而不是六个副本的一半，当然，这种副本
copies instead of six that's half as

234
00:07:39,899 --> 00:07:41,170
副本，而不是六个副本的一半，当然，这种副本
many copies and of course this kind of

235
00:07:41,370 --> 00:07:43,329
如果我们增加十个，将成倍增加，所以我们已经
goes up exponentially if we add ten

236
00:07:43,529 --> 00:07:45,399
如果我们增加十个，将成倍增加，所以我们已经
that's gonna be a lot more copy so we've

237
00:07:45,598 --> 00:07:45,910
保存下来，我们已经保存了很多副本
saved it

238
00:07:46,110 --> 00:07:47,170
保存下来，我们已经保存了很多副本
we've saved a great deal of copies

239
00:07:47,370 --> 00:07:49,240
已经如此，这很棒，但我们可以做得更好
already so this is great but we can do

240
00:07:49,439 --> 00:07:49,629
已经如此，这很棒，但我们可以做得更好
better

241
00:07:49,829 --> 00:07:51,579
我们仍在获取副本，因为此顶点实际上正在构建中
we're still getting a copy because this

242
00:07:51,779 --> 00:07:53,439
我们仍在获取副本，因为此顶点实际上正在构建中
vertex is actually being constructed

243
00:07:53,639 --> 00:07:55,509
在这里的主要功能内，然后复制到实际向量
over here inside the main function and

244
00:07:55,709 --> 00:07:58,240
在这里的主要功能内，然后复制到实际向量
then copied into the actual vector I

245
00:07:58,439 --> 00:08:00,850
只是想用实际的向量和我们可以做的方式来构造它
would like to just construct it in the

246
00:08:01,050 --> 00:08:02,920
只是想用实际的向量和我们可以做的方式来构造它
actual vector and the way that we can do

247
00:08:03,120 --> 00:08:05,560
这是通过使用就地后退而不是后退，因此在这种情况下，
that is by using in-place back instead

248
00:08:05,759 --> 00:08:07,209
这是通过使用就地后退而不是后退，因此在这种情况下，
of push back and so in this case instead

249
00:08:07,408 --> 00:08:09,338
实际传递我们构造的顶点对象的方法，我们只是传递了
of actually passing a vertex object that

250
00:08:09,538 --> 00:08:11,829
实际传递我们构造的顶点对象的方法，我们只是传递了
we've constructed we instead just passed

251
00:08:12,029 --> 00:08:14,020
构造函数的参数列表，基本上告诉向量嘿
the parameter list for the constructor

252
00:08:14,220 --> 00:08:16,389
构造函数的参数列表，基本上告诉向量嘿
which basically tells the vector hey

253
00:08:16,589 --> 00:08:18,910
在我们的位置具有以下参数的构造性顶点对象
constructive vertex objects with the

254
00:08:19,110 --> 00:08:21,040
在我们的位置具有以下参数的构造性顶点对象
following parameters in place in our

255
00:08:21,240 --> 00:08:23,889
实际的向量内存，因此我将针对所有这些内容执行此操作，我们将替换推送
actual vector memory so I'll do that for

256
00:08:24,089 --> 00:08:26,170
实际的向量内存，因此我将针对所有这些内容执行此操作，我们将替换推送
all of these and we'll replace a push

257
00:08:26,370 --> 00:08:27,579
返回em位置回到f5，
back with em place back

258
00:08:27,779 --> 00:08:30,310
返回em位置回到f5，
let's hit f5 look at that clean clean as

259
00:08:30,509 --> 00:08:32,349
一个不错的控制台，但在此代码中没有一个副本
a whistle that is a nice looking console

260
00:08:32,549 --> 00:08:34,328
一个不错的控制台，但在此代码中没有一个副本
out but not a single copy in this code

261
00:08:34,528 --> 00:08:35,679
您是否还看到我们如何仅通过以下操作就可以非常轻松地对其进行优化
anymore do you see how we've just

262
00:08:35,879 --> 00:08:37,809
您是否还看到我们如何仅通过以下操作就可以非常轻松地对其进行优化
optimized this really easily by just

263
00:08:38,009 --> 00:08:40,299
知道它是如何工作的，并知道我们的顶点对象是
knowing how it works and being aware of

264
00:08:40,500 --> 00:08:42,639
知道它是如何工作的，并知道我们的顶点对象是
the fact that our vertex object was

265
00:08:42,839 --> 00:08:44,939
实际上被复制了六次，编写代码甚至都不难，但是
actually getting copied like six times

266
00:08:45,139 --> 00:08:47,109
实际上被复制了六次，编写代码甚至都不难，但是
it's not even hard to write code but

267
00:08:47,309 --> 00:08:47,699
这特别会跑很多
this

268
00:08:47,899 --> 00:08:50,729
这特别会跑很多
specifically is gonna run a lot a lot

269
00:08:50,929 --> 00:08:52,709
速度比我们最初拥有的代码快，甚至不需要很长时间
faster than the code that we originally

270
00:08:52,909 --> 00:08:54,329
速度比我们最初拥有的代码快，甚至不需要很长时间
had and it didn't even take long to

271
00:08:54,529 --> 00:08:56,519
优化它只是要知道了解我们的实际情况
optimize it's just about knowing what's

272
00:08:56,720 --> 00:08:57,599
优化它只是要知道了解我们的实际情况
actually going on knowing our

273
00:08:57,799 --> 00:08:59,579
环境，并知道我们如何使用这些工具实际进行优化
environment and knowing how we can

274
00:08:59,779 --> 00:09:01,229
环境，并知道我们如何使用这些工具实际进行优化
actually optimize using the tools that

275
00:09:01,429 --> 00:09:02,579
无论如何，我们都可以提供，希望大家喜欢这部影片，
we have available anyway I hope you guys

276
00:09:02,779 --> 00:09:03,929
无论如何，我们都可以提供，希望大家喜欢这部影片，
enjoyed this video that's like a first

277
00:09:04,129 --> 00:09:05,339
关于优化的介绍，我喜欢谈论这些东西，所以我们
kind of introduction to optimization I

278
00:09:05,539 --> 00:09:06,929
关于优化的介绍，我喜欢谈论这些东西，所以我们
love talking about this stuff so we are

279
00:09:07,129 --> 00:09:08,250
在这个模糊的球中将更多地谈论这种东西
going to be talking a lot more about

280
00:09:08,450 --> 00:09:09,719
在这个模糊的球中将更多地谈论这种东西
this kind of stuff in this a fuzz ball

281
00:09:09,919 --> 00:09:11,099
系列，如果您喜欢此剧集，请按“赞”按钮
series if you enjoyed this episode

282
00:09:11,299 --> 00:09:12,779
系列，如果您喜欢此剧集，请按“赞”按钮
please hit the like button if you want

283
00:09:12,980 --> 00:09:13,799
观看更多视频，并且您想帮助支持该系列，然后继续
to see more videos and you want to help

284
00:09:14,000 --> 00:09:15,179
观看更多视频，并且您想帮助支持该系列，然后继续
support the series then head on over to

285
00:09:15,379 --> 00:09:17,250
patreon Macomb 4/2 Cherno，您会因支持
patreon Macomb 4/2 Cherno you'll get

286
00:09:17,450 --> 00:09:18,449
patreon Macomb 4/2 Cherno，您会因支持
some cool rewards for supporting the

287
00:09:18,649 --> 00:09:19,919
能够尽早观看视频之类的系列内容有助于规划
series such as being able to see videos

288
00:09:20,120 --> 00:09:21,449
能够尽早观看视频之类的系列内容有助于规划
early contribute to the planning of the

289
00:09:21,649 --> 00:09:23,129
视频和其他一些好处，您可以在该网站上查看，我会看到
videos and a few more perks which you

290
00:09:23,330 --> 00:09:25,049
视频和其他一些好处，您可以在该网站上查看，我会看到
can check out on that site I will see

291
00:09:25,250 --> 00:09:25,740
你们下次再见
you guys next time

292
00:09:25,940 --> 00:09:27,189
你们下次再见
good bye

293
00:09:27,389 --> 00:09:32,389
[音乐]
[Music]

