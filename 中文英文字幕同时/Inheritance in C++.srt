﻿1
00:00:00,000 --> 00:00:02,515
Hey, what's up guys. My name is TheCherno
嘿，大家好。我叫TheCherno 

2
00:00:02,520 --> 00:00:04,400
and welcome back to my c++ series
欢迎回到我的C ++系列

3
00:00:04,400 --> 00:00:05,640
Today, we're going to be continuing  on our
今天，我们将继续我们的

4
00:00:06,219 --> 00:00:08,675
adventure in object-orientated programming and talking about inheritance
面向对象编程的冒险和谈论继承

5
00:00:08,675 --> 00:00:11,505
in c++. So, in object oriented programming
在C ++中。因此，在面向对象的编程中

6
00:00:11,505 --> 00:00:14,515
there's a huge huge huge paradigm, and inheritance between classes
有一个巨大的巨大范式，并且在类之间进行继承

7
00:00:14,515 --> 00:00:16,115
is one of the fundamental
是基础之一

8
00:00:16,445 --> 00:00:17,914
aspects of it and one of the most
它的方面，也是最重要的方面之一

9
00:00:18,114 --> 00:00:21,144
powerful features that we can actually leverage. Inheritance allows us to
我们可以真正利用的强大功能。继承使我们能够

10
00:00:21,144 --> 00:00:24,164
have a hierarchy of classes which relate to each other.
具有相互关联的类的层次结构。 

11
00:00:24,164 --> 00:00:27,254
Or in other words, it allows us to have a base class which contains
换句话说，它允许我们拥有一个包含

12
00:00:27,254 --> 00:00:30,304
common functionality and then it allows us to kind of
通用功能，然后它使我们能够

13
00:00:30,304 --> 00:00:33,414
branch off from that class and create sub-classes from
从该类中分支出来，并从中创建子类

14
00:00:33,414 --> 00:00:35,935
that initial parent class. The primary
最初的父类。首要的

15
00:00:35,935 --> 00:00:38,734
reason why this is so, so, useful is because
之所以如此，如此有用的原因是， 

16
00:00:38,734 --> 00:00:41,899
it helps us avoid code duplication. Code duplication
它有助于我们避免代码重复。代码重复

17
00:00:41,945 --> 00:00:45,024
refers to whether we have to have the exact same code multiple times
指我们是否必须多次拥有完全相同的代码

18
00:00:45,024 --> 00:00:47,964
Doing maybe, slightly, different things
稍微做些不同的事情

19
00:00:47,965 --> 00:00:51,005
and other time just exactly the same thing. Instead of us
而其他时间完全一样。代替我们

20
00:00:51,005 --> 00:00:53,834
repeating ourselves over and over again, we can
一遍又一遍地重复自己，我们可以

21
00:00:54,034 --> 00:00:57,044
put all of our common functionality between classes
在类之间放置我们所有的通用功能

22
00:00:57,045 --> 00:00:59,794
into a parent class. And then simply make sub-classes
进入父类。然后简单地创建子类

23
00:00:59,994 --> 00:01:02,894
from that base class. Which either change the functionality
从那个基类。哪个改变功能

24
00:01:02,895 --> 00:01:05,804
in subtle ways, or introduce entirely new functionality.
以微妙的方式，或引入全新的功能。 

25
00:01:06,004 --> 00:01:09,185
But the idea is, inheritance gives us a way to put all
但想法是，继承为我们提供了一种将所有

26
00:01:09,185 --> 00:01:12,165
that common code between a number of classes into
多个类之间的通用代码

27
00:01:12,165 --> 00:01:15,024
a base class so that we don't have to keep repeating ourselves. Kind of like a template.
基类，这样我们就不必继续重复自己。有点像模板。 

28
00:01:15,224 --> 00:01:18,484
So, let's take a look at how we can actually define that in our source code.
因此，让我们看一下如何在源代码中实际定义它。 

29
00:01:18,484 --> 00:01:21,594
Suppose that I had an entity class and this was going to govern
假设我有一个实体类，这将要进行管理

30
00:01:21,594 --> 00:01:24,635
pretty much every actual entity that I had in my game.
我在游戏中几乎拥有的每个实际实体。 

31
00:01:24,635 --> 00:01:27,704
We have a lot of very very specific entities in our
我们有很多非常非常具体的实体

32
00:01:27,704 --> 00:01:30,524
game. However to some aspect, they will share
游戏。但是，从某些方面来说，他们将分享

33
00:01:30,525 --> 00:01:33,125
functionality. For example, perhaps every entity has
功能。例如，也许每个实体都有

34
00:01:33,125 --> 00:01:36,155
a position inside our game. This might be expressed through
在我们游戏中的位置。这可以通过以下方式表达

35
00:01:36,155 --> 00:01:39,125
two floats, for example. So we'll have float x and float y.
例如，两个浮子。所以我们将有浮点数x和浮点数y。 

36
00:01:39,125 --> 00:01:42,204
Next, we might want to give every entity  the ability to move.
接下来，我们可能要赋予每个实体移动的能力。 

37
00:01:42,204 --> 00:01:43,204
Perhaps via a movement method.
也许通过运动方法。 

38
00:01:43,405 --> 00:01:46,474
So we'll take in an xa and a ya being the amount
因此，我们将xa和ya视为金额

39
00:01:46,474 --> 00:01:49,434
that we want to move by. OK, great, so we have got a base entity class.
我们要经过的地方。好的，太好了，所以我们有了一个基础实体类。 

40
00:01:49,435 --> 00:01:52,635
Which basically says that every single entity we create in our game will have
基本上说，我们在游戏中创建的每个实体都会有

41
00:01:52,635 --> 00:01:55,304
these traits. Let's go ahead and create a new type of
这些特征。让我们继续创建一种新的

42
00:01:55,504 --> 00:01:58,424
entity, for example, player. We'll write our player class,
实体，例如玩家。我们将编写播放器类， 

43
00:01:58,424 --> 00:02:01,444
for now with no concept of inheritance. So basically if we were doing
目前还没有继承的概念。所以基本上如果我们在做

44
00:02:01,444 --> 00:02:04,454
this from scratch and we wanted this player to also have
从头开始，我们希望这个玩家也有

45
00:02:04,454 --> 00:02:07,314
a position, because it is an entity and it makes sense for it to have
职位，因为它是一个实体，因此拥有

46
00:02:07,314 --> 00:02:10,395
a position inside  out game. We will also want to get it to be able to
由内而外的位置。我们还将希望获得它能够

47
00:02:10,585 --> 00:02:13,435
move, so we would need the "move" function carried across.
移动，因此我们需要传递“移动”功能。 

48
00:02:13,435 --> 00:02:16,235
We would basically end up writing  something that looks very
我们基本上会写出看起来很像的东西

49
00:02:16,235 --> 00:02:18,965
similar to this (entity function) Maybe this player class has extra data that we wanted to store
与此类似（实体函数）也许这个玩家类有我们想要存储的额外数据

50
00:02:19,164 --> 00:02:21,875
for example, a name or something like that. We would add this.
例如名称或类似名称。我们将添加它。 

51
00:02:21,875 --> 00:02:24,905
You can say that these are actually different classes. However
您可以说这些实际上是不同的类。然而

52
00:02:24,905 --> 00:02:27,564
there's quiet a lot of code that's just been copy and pasted and that's
有很多安静的代码只是被复制和粘贴的，那就是

53
00:02:27,764 --> 00:02:30,904
this entire section. So what we could do here is use the power
这整个部分。所以我们可以在这里使用电源

54
00:02:30,905 --> 00:02:33,974
of inheritance. We could extend this entity class to
继承。我们可以将此实体类扩展为

55
00:02:33,974 --> 00:02:37,025
create a new type called player and also have it store
创建一个称为播放器的新类型并存储它

56
00:02:37,025 --> 00:02:39,814
new data such as name. As well as provide
新数据，例如名称。以及提供

57
00:02:40,014 --> 00:02:42,674
extra functionality, such as a function
额外功能，例如功能

58
00:02:42,675 --> 00:02:45,555
that doesn't exist in the base class. And there we go. So now let's get
在基类中不存在。然后我们走了。所以现在让我们

59
00:02:45,754 --> 00:02:48,664
player to be a subclass of entity. The way we do that is
玩家成为实体的子类。我们这样做的方式是

60
00:02:48,664 --> 00:02:51,664
we write a colon, after the type deceleration, here,
我们在类型减速后写一个冒号，在这里， 

61
00:02:51,664 --> 00:02:54,674
class player, and then we write "public entity"
类播放器，然后我们写“公共实体” 

62
00:02:54,675 --> 00:02:57,745
OK, so now with this line of code that we've written here, a few things actually
好的，现在我们在这里编写的这行代码实际上是几件事

63
00:02:57,745 --> 00:03:00,715
happened. The player class now not only has the type "player",
发生了现在，玩家类不仅具有“玩家”类型， 

64
00:03:00,715 --> 00:03:03,585
it also has the type "entity". Meaning that it actually
它也具有“实体”类型。意思是实际上

65
00:03:03,585 --> 00:03:06,615
is both of those types. Types in C++
是这两种类型。 C ++中的类型

66
00:03:06,615 --> 00:03:09,465
are quiet a complicated topic because on one hand, they don't
安静是一个复杂的话题，因为一方面，他们没有

67
00:03:09,465 --> 00:03:12,675
really exist. However on the other hand, they kind of do.
确实存在。但是，另一方面，它们确实可以做到。 

68
00:03:12,675 --> 00:03:15,335
Especially if you have certain runtime flags activated.
特别是如果您激活了某些运行时标志。 

69
00:03:15,534 --> 00:03:18,444
We're not really going to get in-depth about how this whole thing works
我们真的不会深入了解整个过程

70
00:03:18,444 --> 00:03:21,495
just yet, but definitely something for the future.
到目前为止，但绝对是未来的事情。 

71
00:03:21,495 --> 00:03:24,694
The other thing that's occurred is that "player" now has
发生的另一件事是“玩家”现在拥有

72
00:03:24,694 --> 00:03:27,974
everything that "entity" has. So all of these class members that we have
 “实体”拥有的一切。所以我们所有这些班级成员

73
00:03:27,974 --> 00:03:30,965
such as x and y here, two floats, there are now
例如这里的x和y，有两个浮点数

74
00:03:30,965 --> 00:03:33,925
also included in here. So, right now we've actually got
也包括在这里。所以，现在我们实际上已经

75
00:03:33,925 --> 00:03:36,685
four floats total in the player class. We've got these two,
玩家类别中总共有四个浮动。我们有两个， 

76
00:03:36,685 --> 00:03:39,715
that are just the copy-and-pasted ones. As well as the original
只是复制粘贴的内容。以及原始的

77
00:03:39,715 --> 00:03:42,715
two so let's definitely get rid of all of that duplicated
两个，所以我们绝对要消除所有重复的内容

78
00:03:42,715 --> 00:03:45,664
code here so that we only have everything that's new.
在这里编写代码，以便我们仅拥有所有新内容。 

79
00:03:45,664 --> 00:03:48,444
Such as the name, which is a const char pointer, and the
例如名称，这是一个const char指针，并且

80
00:03:48,444 --> 00:03:51,474
PrintName function which is kind of our additional functionality example.
 PrintName函数，这是我们的其他功能示例。 

81
00:03:51,474 --> 00:03:54,574
OK, cool, so the player class is looking really really clean now.
好，很酷，所以玩家班级现在看上去真的很干净。 

82
00:03:54,574 --> 00:03:57,634
However it is actually an entity, which means that just looking at this class
但是，它实际上是一个实体，这意味着仅查看此类

83
00:03:57,634 --> 00:04:00,474
doesn't actually tell us the whole story. We have to go up and
实际上并没有告诉我们整个故事。我们必须上去

84
00:04:00,474 --> 00:04:06,884
find  
找

85
00:04:06,884 --> 00:04:09,804
 
找

86
00:04:09,805 --> 00:04:12,685
 
找

87
00:04:12,884 --> 00:04:15,894
 
找

88
00:04:15,895 --> 00:04:18,735
 
找

89
00:04:18,735 --> 00:04:19,735
 
找

90
00:04:20,144 --> 00:04:21,594
 
找

91
00:04:22,055 --> 00:04:25,055
 
找

92
00:04:25,055 --> 00:04:28,125
 
找

93
00:04:28,125 --> 00:04:30,894
 
找

94
00:04:30,894 --> 00:04:33,824
 
找

95
00:04:33,824 --> 00:04:36,834
 
找

96
00:04:36,834 --> 00:04:40,104
 
找

97
00:04:40,105 --> 00:04:43,194
 
找

98
00:04:43,194 --> 00:04:46,045
 
找

99
00:04:46,045 --> 00:04:49,004
 
找

100
00:04:49,004 --> 00:04:51,995
 
找

101
00:04:51,995 --> 00:04:53,175
 
找

102
00:04:53,524 --> 00:04:56,814
 
找

103
00:04:56,814 --> 00:04:57,814
 
找

104
00:04:57,904 --> 00:05:00,234
 
找

105
00:05:00,685 --> 00:05:01,685
 
找

106
00:05:01,725 --> 00:05:04,745
 
找

107
00:05:04,754 --> 00:05:06,625
 
找

108
00:05:06,935 --> 00:05:10,014
 
找

109
00:05:10,014 --> 00:05:13,365
 
找

110
00:05:13,435 --> 00:05:14,735
 
找

111
00:05:15,404 --> 00:05:18,254
 
找

112
00:05:18,254 --> 00:05:21,225
 
找

113
00:05:21,225 --> 00:05:24,134
 
找

114
00:05:24,334 --> 00:05:27,305
 
找

115
00:05:27,305 --> 00:05:30,355
 
找

116
00:05:30,355 --> 00:05:33,355
 
找

117
00:05:33,355 --> 00:05:36,345
 
找

118
00:05:36,345 --> 00:05:39,185
 
找

119
00:05:39,185 --> 00:05:41,954
 
找

120
00:05:42,154 --> 00:05:44,974
 
找

121
00:05:44,975 --> 00:05:47,915
 
找

122
00:05:47,915 --> 00:05:51,235
 
找

123
00:05:51,285 --> 00:05:54,134
 
找

124
00:05:54,334 --> 00:05:57,245
 
找

125
00:05:57,245 --> 00:06:00,285
 
找

126
00:06:00,285 --> 00:06:01,605
 
找

127
00:06:02,014 --> 00:06:05,064
 
找

128
00:06:05,064 --> 00:06:07,254
 
找

129
00:06:07,605 --> 00:06:10,324
 
找

130
00:06:10,524 --> 00:06:13,875
 
找

131
00:06:13,875 --> 00:06:16,884
 
找

132
00:06:16,884 --> 00:06:19,954
 
找

133
00:06:19,954 --> 00:06:23,144
 
找

134
00:06:23,144 --> 00:06:26,234
 
找

135
00:06:26,235 --> 00:06:29,194
 
找

136
00:06:29,394 --> 00:06:32,625
 
找

137
00:06:33,245 --> 00:06:36,615
 
找

138
00:06:36,615 --> 00:06:39,805
 
找

139
00:06:39,805 --> 00:06:42,785
 
找

140
00:06:42,785 --> 00:06:45,355
 
找

141
00:06:45,355 --> 00:06:48,355
 
找

142
00:06:48,355 --> 00:06:50,504
 
找

143
00:06:50,704 --> 00:06:53,664
 
找

144
00:06:53,795 --> 00:06:56,725
 
找

145
00:06:56,725 --> 00:06:59,685
 
找

146
00:06:59,685 --> 00:07:02,535
 
找

147
00:07:02,535 --> 00:07:05,564
 
找

148
00:07:05,564 --> 00:07:08,694
 
找

149
00:07:08,694 --> 00:07:11,694
 
找

150
00:07:11,694 --> 00:07:14,615
 
找

151
00:07:14,615 --> 00:07:17,805
 
找

152
00:07:17,805 --> 00:07:20,745
 
找

153
00:07:20,745 --> 00:07:23,514
 
找

154
00:07:23,714 --> 00:07:26,744
 
找

155
00:07:26,745 --> 00:07:29,514
 
找

156
00:07:29,714 --> 00:07:32,654
 
找

157
00:07:32,654 --> 00:07:35,694
 
找

158
00:07:35,694 --> 00:07:36,694
 
找

159
00:07:37,855 --> 00:07:39,595
 
找

160
00:07:40,324 --> 00:07:45,324
 
找

