﻿1
00:00:00,000 --> 00:00:04,868
hands up guys my name is achernar and welcome back to my c++ series so you're
举起手来，我的名字叫achernar，欢迎回到我的c ++系列，以便您

2
00:00:05,068 --> 00:00:10,089
using linux to develop sigilyph lost code okay cool let's cover how we can
使用linux开发sigilyph丢失的代码好酷，让我们介绍一下如何

3
00:00:10,289 --> 00:00:13,870
set up the tools we need on linux so that we can make c++ programs now of
在linux上设置我们需要的工具，以便我们现在可以制作c ++程序

4
00:00:14,070 --> 00:00:17,710
course there are so many different tools out there so many different ways to
当然有很多不同的工具，有很多不同的方法

5
00:00:17,910 --> 00:00:21,789
write and build your code I'm going to cover one of those ways one that I
编写和构建您的代码，我将介绍其中一种方法， 

6
00:00:21,989 --> 00:00:25,210
personally like I hope you're ready to do some serious setting up I mean you've
像我个人一样，希望您已经准备好进行认真的设置，我是说您已经

7
00:00:25,410 --> 00:00:29,800
chosen Linux so obviously you're going to be prepared to do some work basically
选择了Linux，因此很明显，您将准备好进行一些工作

8
00:00:30,000 --> 00:00:33,579
we're going to use C mag to generate project files for an IDE called code
我们将使用C mag为称为代码的IDE生成项目文件

9
00:00:33,780 --> 00:00:38,018
light code light is a nice lightweight IDE that basically does pretty much
 light代码light是一个不错的轻量级IDE，基本上可以完成很多工作

10
00:00:38,219 --> 00:00:41,500
everything you would expect but isn't too heavy weight and just lets us write
您所期望的一切，但不是太重，让我们来写

11
00:00:41,700 --> 00:00:45,279
our code in a nice way so let's go ahead and set everything up so we've got a
我们的代码以一种很好的方式，所以让我们继续进行所有设置，以便

12
00:00:45,479 --> 00:00:50,768
VirtualBox VM here running Mint 18.1 and I'm using KDE as my GUI so if I go ahead
 VirtualBox VM在这里运行Mint 18.1，并且我使用KDE作为GUI，因此如果继续

13
00:00:50,969 --> 00:00:55,448
and open a terminal we can get started the first thing I like to do is set up a
并打开一个终端，我们可以开始我想做的第一件事是设置一个

14
00:00:55,649 --> 00:00:58,809
directory here called dev which is basically going to hold all of our
这里的目录称为dev，它基本上将容纳我们所有的目录

15
00:00:59,009 --> 00:01:02,619
development stuff we'll make and then basically inside
我们将制作的开发内容，然后基本上在里面

16
00:01:02,820 --> 00:01:05,950
here we'll have a like a folder for each project so I'll make a directory for
在这里，每个项目都有一个类似的文件夹，所以我将为

17
00:01:06,150 --> 00:01:10,899
follow world which is going to be what we're going to do here now we're going
跟随世界，这将是我们将要在这里做的事情

18
00:01:11,099 --> 00:01:13,719
to set up a basic hello world project now in terms of the applications that we
根据我们的应用程序现在建立一个基本的hello world项目

19
00:01:13,920 --> 00:01:17,500
actually have to install we basically need the ones that we really need are a
实际上必须安装，我们基本上需要的是我们真正需要的

20
00:01:17,700 --> 00:01:22,269
compiler C make and code light for the first thing you're going to want to do
编译器C make和代码指示灯为您要做的第一件事

21
00:01:22,469 --> 00:01:26,528
to install that is do sudo apt-get update that's just going to basically
安装是做sudo apt-get update，这将基本上是

22
00:01:26,728 --> 00:01:30,308
update in having your root password that's going to basically update all of
更新您的root密码，这将基本上更新所有

23
00:01:30,509 --> 00:01:34,929
our package packages our package repositories and then once that's done
我们的程序包会打包我们的程序包存储库，然后一旦完成

24
00:01:35,129 --> 00:01:42,378
we can actually install the text as we need so what we need our will do apt-get
我们实际上可以根据需要安装文本，因此我们需要做的就是apt-get 

25
00:01:42,578 --> 00:01:48,439
install I'll excuse them is my terminal text editor and then we'll get your plus
安装，请原谅他们是我的终端文本编辑器，然后我们将为您提供帮助

26
00:01:48,640 --> 00:01:55,308
plus ago profiler will get coded lights as our IDE and we will get a C make and
加上之前的事件探查器将获得编码光作为我们的IDE，我们将获得C品牌和

27
00:01:55,509 --> 00:02:00,259
then just hit answer instead of taking a hundred ninety eight megabytes Y and
然后只需点击答案，而不是占用198 MB 

28
00:02:00,459 --> 00:02:05,179
this will take a while okay so once that is done let's go ahead and clear this so
这将需要一段时间，所以一旦完成，让我们继续进行清除，以便

29
00:02:05,379 --> 00:02:08,868
we've got nothing in our directory right now I like to make a directory called
现在目录中什么也没有，我想建立一个目录

30
00:02:09,068 --> 00:02:12,170
source and that's where I'll actually put all of my source files I'm going to
源，实际上是我要放入所有源文件的地方

31
00:02:12,370 --> 00:02:15,980
just create a file in the source directory called main just a BB and just
只需在源目录中创建一个名为main的文件BB和

32
00:02:16,180 --> 00:02:18,550
now I'm going to write anything into it I'm just going to create the empty file
现在我要写任何东西，我只是要创建一个空文件

33
00:02:18,750 --> 00:02:22,310
and will actually let code light handle that file for us we're going to need to
并实际上将让代码灯为我们处理该文件，我们将需要

34
00:02:22,509 --> 00:02:25,520
set up the C make file and then I also like to set up a build script so that
设置C make文件，然后我也想设置一个构建脚本，以便

35
00:02:25,719 --> 00:02:28,700
we've got an easy way to generate a project so in terms of C may make that
我们有一个简单的方法来生成项目，因此就C而言， 

36
00:02:28,900 --> 00:02:32,030
first I'm just going to use them you can use any text editor you like the file is
首先，我将要使用它们，您可以使用任何您喜欢文件的文本编辑器

37
00:02:32,229 --> 00:02:36,230
going to be called C make list dot txt and it's going to basically just
将被称为C make list dot txt，基本上

38
00:02:36,430 --> 00:02:39,980
instruct they make how we want to actually generate our code like project
指示他们做出我们想要如何实际生成我们的代码（如项目）的方法

39
00:02:40,180 --> 00:02:45,819
file so first of all we'll specify a minimum version of C make that we need
文件，因此首先我们将指定所需的最低版本的C make 

40
00:02:46,019 --> 00:02:52,159
as a good rule of thumb I like to just roll with version 3.5 will then give our
作为一个好的经验法则，我想只使用3.5版，然后将

41
00:02:52,359 --> 00:02:56,990
project a name so I'll just hollow world and then we'll set up some variables
命名一个名字，这样我就将世界变得空洞，然后我们将设置一些变量

42
00:02:57,189 --> 00:03:02,629
eventually so intensive like flags that we want to use for compilation we want
最终如此密集，像我们想要用于编译的标志

43
00:03:02,829 --> 00:03:09,980
to use the existing one so that basically C 50 C makes the xx legs which
使用现有的，这样基本上C 50 C可以使xx腿

44
00:03:10,180 --> 00:03:13,129
are the compilation apply um but in terms of what we actually want to do we
编译是否适用于我们，但就我们实际想要做什么而言

45
00:03:13,329 --> 00:03:17,868
want to add up we want to display all warning errors of course and then
要加起来，我们当然要显示所有警告错误，然后

46
00:03:18,068 --> 00:03:22,670
intensive like which standard library we use you can use people as weapons at
密集的，例如我们使用的标准库，您可以在

47
00:03:22,870 --> 00:03:25,399
once but it doesn't really doesn't really matter for this project since
一次，但是对于这个项目来说并不重要，因为

48
00:03:25,598 --> 00:03:29,810
we'll be just using will be printing hello world but I'm looking over 14
我们将使用它将打印您好世界，但我正在寻找14 

49
00:03:30,009 --> 00:03:34,879
years future profiles a little bit we're creative variable here called source
几年的未来概况我们有点创意，这里称为来源

50
00:03:35,079 --> 00:03:43,780
directory and I'll actually set it to be the project forth directory and then
目录，实际上我将其设置为第四个项目，然后

51
00:03:43,979 --> 00:03:47,618
essentially just a folder called source so the printer source directory is the
本质上只是一个名为source的文件夹，所以打印机的源目录是

52
00:03:47,818 --> 00:03:51,069
directory we're currently in and then inside there I'm just thinking I've just
我们当前所在的目录，然后在其中，我只是想我已经

53
00:03:51,269 --> 00:03:53,349
made a directory called source and that's where all my source files are so
创建了一个名为source的目录，这是我所有源文件所在的目录

54
00:03:53,549 --> 00:03:58,810
that works out pretty well so we've set up these variables right those two lives
效果很好，所以我们在这两个生命中都设置了这些变量

55
00:03:59,009 --> 00:04:03,219
were just variables we've set our flags and we set our source directory now we
只是变量，我们已经设置了标志，现在我们设置了源目录

56
00:04:03,419 --> 00:04:08,530
actually need to somehow tell the compiler which files to compile so again
实际上需要以某种方式告诉编译器要再次编译哪些文件

57
00:04:08,729 --> 00:04:12,399
we're using CMake we can just use a globe for that so basically I'm going to
我们正在使用CMake，我们可以仅使用地球仪，所以基本上

58
00:04:12,598 --> 00:04:17,528
specify that my source files are going to be in that source directory and that
指定我的源文件将在该源目录中，并且

59
00:04:17,728 --> 00:04:20,050
source writer is just as variable that I've made up here right and then
源代码编写器就像我在这里编写的变量一样，然后

60
00:04:20,250 --> 00:04:24,790
basically anything inside that directory that ends in C++ typically I want to
基本上，该目录中以C ++结尾的所有内容

61
00:04:24,990 --> 00:04:27,639
compile now if you had include directories and stuff you would also
现在进行编译（如果您包含目录和内容） 

62
00:04:27,839 --> 00:04:30,939
want to set that up but so now we're not going to do that we'll just add
想要设置，但是现在我们不再要做，我们只需添加

63
00:04:31,139 --> 00:04:32,620
executables this is going to be our target
可执行文件，这将是我们的目标

64
00:04:32,819 --> 00:04:36,639
the HelloWorld is a project name and then which files we want to pop into
 HelloWorld是项目名称，然后是我们要弹出的文件

65
00:04:36,839 --> 00:04:39,939
they are going to be our source files if you had again an include directory or
如果您再次有一个include目录或它们，它们将成为我们的源文件。 

66
00:04:40,139 --> 00:04:42,400
something you would also pop it in here but there we go
你也会在这里弹出它，但是我们去

67
00:04:42,600 --> 00:04:46,870
that's all of our stuff that's that's our entire cmakelists done so let's
这就是我们所有的东西，这就是我们整个cmakelists所做的，所以让我们

68
00:04:47,069 --> 00:04:51,520
write and quit now we'll set up a script that will run CMake for us so I'll call
现在编写并退出，我们将设置一个脚本来为我们运行CMake，因此我将调用

69
00:04:51,720 --> 00:04:55,420
this build door Sh now this isn't really going to build our code it's just going
这个构建门Sh现在这真的不是要构建我们的代码

70
00:04:55,620 --> 00:04:58,300
to generate the project and I'll show you kind of how the collapse in a minute
生成项目，我将向您展示如何在一分钟内崩溃

71
00:04:58,500 --> 00:05:01,629
but basically the first thing we want to do is make sure that Bash is going to be
但基本上，我们要做的第一件事就是确保Bash 

72
00:05:01,829 --> 00:05:09,899
able to get it in the lab so we'll add bin FH as every bash script we'll have
能够在实验室中获得它，因此我们将bin FH添加为我们拥有的每个bash脚本

73
00:05:10,098 --> 00:05:16,028
then we'll do C make dash G so in terms of the target we're going to be using
然后我们将C制成破折号G，这样就可以使用的目标

74
00:05:16,228 --> 00:05:19,980
code light UNIX make file vision or something like ninja if you like
如果您愿意，可以使用UNIX编写代码来实现文件视觉或忍者之类的功能

75
00:05:20,180 --> 00:05:26,259
probably a bit better than this because it's to be a setup and then as far as my
可能比这更好，因为这是一个设置，然后到我

76
00:05:26,459 --> 00:05:29,949
build type goes I'm actually going to set that to debug mostly make the build
构建类型我实际上要设置它来调试主要是使构建

77
00:05:30,149 --> 00:05:33,610
type because generating a debug configuration is
类型，因为生成调试配置是

78
00:05:33,810 --> 00:05:36,338
probably what we want anyway since we'll be debugging a lot and you want to make
也许我们仍然想要什么，因为我们将进行很多调试，而您想

79
00:05:36,538 --> 00:05:41,230
sure the code light has a capital L here and that's it we can write and quit that
确保代码灯在此处具有大写字母L，就是这样，我们可以编写并退出该代码

80
00:05:41,430 --> 00:05:44,620
and that's out it's gonna be our entire script is very very simple so now we're
事实证明，整个脚本非常简单，所以现在

81
00:05:44,819 --> 00:05:48,160
ready to run our script now in order to run a script in Linux you have to market
现在就可以运行我们的脚本，以便在Linux上运行脚本

82
00:05:48,360 --> 00:05:54,009
it as executable so we'll just do chmod plus x and then build Sh so we're just
它作为可执行文件，所以我们将chmod加x，然后构建Sh，所以我们只是

83
00:05:54,209 --> 00:05:56,870
adding the executable flag to the build Sh
将可执行标志添加到构建Sh 

84
00:05:57,069 --> 00:06:01,759
now we can run it and hopefully if you did everything correctly you can see
现在我们可以运行它了，希望您做的一切正确，您可以看到

85
00:06:01,959 --> 00:06:05,060
that it's written some build files for us so if we look at the directory
它为我们写了一些构建文件，所以如果我们查看目录

86
00:06:05,259 --> 00:06:09,500
because it is a number of new files here the ones that we kind of care about our
因为这里有许多新文件我们非常在乎我们的文件

87
00:06:09,699 --> 00:06:12,740
hello world a project hello world up for workspace there are make files as well
 hello world一个项目，hello world用于工作区，还有make文件

88
00:06:12,939 --> 00:06:17,780
but you can see that those kind of the important so now how do we open this in
但是您可以看到这些很重要，所以现在我们如何在

89
00:06:17,980 --> 00:06:21,710
code light well I like to just open it through the terminal so you can just
代码指示灯很好，我想通过终端将其打开，这样您就可以

90
00:06:21,910 --> 00:06:25,790
type in code light and then hello world sort workspace now if you want to open
如果要打开，请输入代码指示灯，然后立即打世界排序工作区

91
00:06:25,990 --> 00:06:28,610
it in the background so you can still use your terminal just add an ampersand
它在后台，因此您仍然可以使用终端，只需添加一个＆号即可

92
00:06:28,810 --> 00:06:33,199
on to the end hit enter and code light should open now you're probably going to
到最后，按回车键，代码灯应该打开了，现在您可能会

93
00:06:33,399 --> 00:06:37,009
want to run through the setup I don't really care to my hair Council as you
想完成整个安装过程，我真的不关心我的头发理事会

94
00:06:37,209 --> 00:06:41,500
can see we've got our project here and our target which makes a bit bigger
可以看到我们在这里有我们的项目，而我们的目标又大了一点

95
00:06:41,699 --> 00:06:45,079
we've got our include directories so this is where all of our includes go
我们有我们的包含目录，所以这是我们所有包含的地方

96
00:06:45,279 --> 00:06:48,980
it's also just going to go in here we'll double click the file to open it I'm
它也要进入这里，我们将双击文件打开它

97
00:06:49,180 --> 00:06:53,000
going to just basically type out a simple hello world application
基本上只输入一个简单的hello world应用程序

98
00:06:53,199 --> 00:07:06,439
so we'll say and we'll do our little standoff yet as well alright save that
所以我们要说，我们会做一些小僵局，好吧，保存一下

99
00:07:06,639 --> 00:07:11,569
you want to build it you can just hit build and build projects outputs down
您想要构建它，您可以点击构建并构建项目输出

100
00:07:11,769 --> 00:07:15,170
here and you can see that everything's fine there are errors zero warnings
在这里，您会看到一切都很好，其中有错误零警告

101
00:07:15,370 --> 00:07:21,020
we're ready to go so go back to terminal and I do an LS you can see we've got a
我们准备出发了，回到航站楼，我做了一个LS，您可以看到我们有一个

102
00:07:21,220 --> 00:07:25,300
holo world file and it's actually executable
 holo world文件，它实际上是可执行文件

103
00:07:25,500 --> 00:07:30,040
right so that's our actual program so let's go ahead and run that Lego hello
对，所以这是我们的实际程序，所以让我们继续运行该乐高打招呼

104
00:07:30,240 --> 00:07:34,870
world we've written our first application if you want to run it inside
如果您想在其中运行，我们已经编写了第一个应用程序

105
00:07:35,069 --> 00:07:38,829
here one thing I like to do is just go to preferences and make it used to
我想做的一件事就是转到首选项并使其习惯

106
00:07:39,029 --> 00:07:44,350
built-in terminal emulator and then you can just do build run we can build it
内置终端模拟器，然后您就可以进行构建运行，我们可以构建它

107
00:07:44,550 --> 00:07:49,120
and execute it and you can see here it is running inside of code light all
并执行它，您可以看到它在代码灯内部全部运行

108
00:07:49,319 --> 00:07:52,509
right that's it Linux is setup we're ready to write some code we're ready to
没错，就是Linux设置好了，我们准备编写一些代码，我们准备

109
00:07:52,709 --> 00:07:55,780
learn some C++ over the next two videos we're going to cover how people Claus
在接下来的两个视频中学习一些C ++，我们将介绍Claus的方式

110
00:07:55,980 --> 00:08:00,280
actually works because that's the key to learning how to write C++ properly until
确实有效，因为这是学习如何正确编写C ++的关键，直到

111
00:08:00,480 --> 00:08:03,790
then you can follow me on Twitter and Instagram and if you really like this
那么您可以在Twitter和Instagram上关注我，如果您真的喜欢这个

112
00:08:03,990 --> 00:08:07,660
series you can support my videos by pledging on patreon.com for clash to
系列影片，您可以在patreon.com上认捐，以支持我的影片

113
00:08:07,860 --> 00:08:12,860
channel until next time goodbye warm
频道直到下次再见温暖

