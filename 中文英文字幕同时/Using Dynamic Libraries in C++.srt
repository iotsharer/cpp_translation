﻿1
00:00:00,000 --> 00:00:02,829
hey what's up guys my name is the chana welcome back to my estate plus plus
嘿，大家好，我叫chana欢迎回到我的庄园，再加上

2
00:00:03,029 --> 00:00:06,729
series so last time we talked about how you can actually use libraries in C++
系列，因此上次我们讨论了如何在C ++中实际使用库

3
00:00:06,929 --> 00:00:10,839
and your C++ project and specifically how you can link them statically but
和您的C ++项目，特别是如何静态链接它们，但是

4
00:00:11,039 --> 00:00:14,349
today we're talking all about dynamic linking what it is how to use it when
今天，我们讨论的都是动态链接，当什么时候如何使用它

5
00:00:14,548 --> 00:00:17,800
you should be using it more that stuff and this time we're going to be linking
您应该更多地使用它，这次我们将进行链接

6
00:00:18,000 --> 00:00:21,940
JFW dynamically so in the static video we linked it statically and now we're
 JFW是动态的，因此在静态视频中，我们将其静态链接，现在我们

7
00:00:22,140 --> 00:00:24,909
gonna link it dynamically and kind of see what what was involved in that and
将其动态链接，并了解其中涉及的内容， 

8
00:00:25,109 --> 00:00:28,749
what the differences are so first of all dynamic linking what what does it mean
区别是什么，那么首先动态链接是什么意思

9
00:00:28,949 --> 00:00:32,739
how is it different than static why is it called dynamic so dynamic linking is
它与静态有何不同？为什么称其为动态，所以动态链接是

10
00:00:32,939 --> 00:00:36,969
linking that happens at runtime so static linking happens at compile time
链接在运行时发生，因此静态链接在编译时发生

11
00:00:37,170 --> 00:00:41,198
when you compile a static library you then link it into either an executable
编译静态库时，您可以将其链接到一个可执行文件中

12
00:00:41,399 --> 00:00:45,549
an application or a dynamic library and that's kind of it you're done you
应用程序或动态库，就这样，您就完成了

13
00:00:45,750 --> 00:00:49,329
literally take the contents of that static library and you put it into black
从字面上看就是那个静态库的内容，然后把它变成黑色

14
00:00:49,530 --> 00:00:53,229
with the rest of the binary data that's actually in your dynamic library or
与动态库中实际存在的其他二进制数据或

15
00:00:53,429 --> 00:00:57,159
inside your executable and because of that there are there are a number of
在您的可执行文件中，因此有很多

16
00:00:57,359 --> 00:01:01,779
optimizations that can happen because the compiler and linker is now fully
由于编译器和链接器现在完全可用，因此可能发生的优化

17
00:01:01,979 --> 00:01:04,659
aware of the code that actually goes into an application when you're linked
知道链接时实际进入应用程序的代码

18
00:01:04,859 --> 00:01:08,200
statically when we talk more about the actual performance differences in what
当我们更多地谈论实际性能差异时，静态地

19
00:01:08,400 --> 00:01:11,079
Ward linking statically versus dynamically actually means in a future
病房静态链接与动态链接实际上意味着未来

20
00:01:11,280 --> 00:01:15,129
video this is just a basic overview but just keep that in mind that static
视频，这只是基本概述，但请记住，静态

21
00:01:15,329 --> 00:01:19,090
linking kind of allows more of that optimization to happen because the
链接类型允许更多的优化发生，因为

22
00:01:19,290 --> 00:01:23,140
compiler and linker kind of is just they can see more of the picture specifically
编译器和链接器的种类只是他们可以具体看到更多图片

23
00:01:23,340 --> 00:01:26,109
the link that can see more of the more of the picture whereas dynamic linking
可以看到更多图片的链接，而动态链接

24
00:01:26,310 --> 00:01:29,769
as I mentioned happens at runtime what that means is that when you actually
正如我提到的那样，它发生在运行时

25
00:01:29,969 --> 00:01:34,539
launch your executable that is when your dynamic link library gets loaded so it's
启动动态链接库加载时的可执行文件，因此

26
00:01:34,739 --> 00:01:38,109
not actually part of the executable when you launch normal executable it gets
当您启动普通可执行文件时，它实际上不是可执行文件的一部分

27
00:01:38,310 --> 00:01:42,399
loaded into memory however if you have a dynamic link library what that means is
加载到内存中，但是，如果您有动态链接库，这意味着

28
00:01:42,599 --> 00:01:46,778
that you actually link another library and external binary file dynamically at
您实际上在以下位置动态链接了另一个库和外部二进制文件： 

29
00:01:46,978 --> 00:01:51,069
runtime so you run your application and then you load an additional file into
运行时，以便您运行应用程序，然后将其他文件加载到

30
00:01:51,269 --> 00:01:54,369
memory now the way that executables work they can actually require certain
内存现在可执行文件的工作方式实际上可能需要

31
00:01:54,569 --> 00:01:58,058
libraries to be present certain dynamic libraries certain external files to be
某些动态库某些外部文件

32
00:01:58,259 --> 00:02:00,518
present before they actually let you run the application
在他们实际允许您运行应用程序之前显示

33
00:02:00,718 --> 00:02:03,819
that's why on Windows for example you might sometimes see when you launch an
因此，例如在Windows上，您有时会在启动

34
00:02:04,019 --> 00:02:06,878
application you might sometimes see our message pop up being like blah blah or
应用程序，您有时可能会看到我们的信息突然弹出，诸如此类

35
00:02:07,078 --> 00:02:11,300
DLL is required or is not found so we can start the program and that's
 DLL是必需的或找不到的，因此我们可以启动该程序， 

36
00:02:11,500 --> 00:02:14,900
one form of dynamic linking that's kind of I like to almost call that kind of
动态链接的一种形式，我喜欢几乎将其称为

37
00:02:15,099 --> 00:02:19,070
50/50 because they're executable is aware of the dynamic link library and it
 50/50，因为它们是可执行文件，因此知道动态链接库及其

38
00:02:19,270 --> 00:02:23,300
actually lists it as a requirement but it is still a separate file a separate
实际上将其列为要求，但它仍然是单独的文件

39
00:02:23,500 --> 00:02:28,370
module that is loaded at runtime you can also load dynamic link libraries
在运行时加载的模块，您还可以加载动态链接库

40
00:02:28,569 --> 00:02:32,270
completely like dynamically so the executable can have nothing to do with
完全像动态一样，因此可执行文件与之无关

41
00:02:32,469 --> 00:02:36,200
it at all you can launch your executable your application it won't even ask you
它完全可以启动您的可执行文件，甚至不问您

42
00:02:36,400 --> 00:02:40,700
to include a certain dynamic library but then inside your executable you can
包含某个动态库，但是在可执行文件中，您可以

43
00:02:40,900 --> 00:02:45,590
actually write code which looks for and maybe loads certain dynamic libraries at
实际编写代码，寻找并加载某些动态库

44
00:02:45,789 --> 00:02:49,550
one time and that obtains function pointers or whatever you need to the
一次，并获得函数指针或您需要的任何内容

45
00:02:49,750 --> 00:02:54,290
stuff that's inside that dynamic library and then uses that dynamic library so we
该动态库中的内容，然后使用该动态库，因此我们

46
00:02:54,490 --> 00:02:57,469
don't have libraries just keep that in mind that's kind of the static dynamic
没有库只是记住这一点，这是静态动态的

47
00:02:57,669 --> 00:03:00,860
version which is basically my application actually requires that this
基本上是我的应用程序的版本实际上要求

48
00:03:01,060 --> 00:03:04,820
dynamic like link library be present and I'm already up I'm already aware of what
动态像链接库一样存在，我已经启动了，我已经知道了什么

49
00:03:05,020 --> 00:03:08,689
functions are in it and what I can use but then there's also I want to
功能在其中，我可以使用，但是接下来我也想

50
00:03:08,889 --> 00:03:11,840
arbitrarily load this dynamic library I don't know what I don't even know what's
任意加载此动态库，我什至不知道我什至不知道是什么

51
00:03:12,039 --> 00:03:15,620
in it but I want to pull out some stuff or I want to do many things with it and
在其中，但我想取出一些东西，或者我想用它做很多事情， 

52
00:03:15,819 --> 00:03:19,610
there are very good uses for both of them we're gonna well we're actually
两者都有很好的用途，我们实际上是

53
00:03:19,810 --> 00:03:23,510
gonna focus on the formal one for today which is I know my application requires
今天要关注的是正式的，我知道我的申请需要

54
00:03:23,710 --> 00:03:26,600
this library but I'm going to link it dynamically set of statically so let's
这个库，但是我将静态地动态链接它，所以让我们

55
00:03:26,800 --> 00:03:30,080
just jump into an example and look at what that looks like for GL of W because
只是跳入一个例子，看看W的GL看起来像什么，因为

56
00:03:30,280 --> 00:03:34,340
gel W actually lets us link either statically or dynamically so a lot so in
 gel W实际上让我们可以静态或动态链接很多，因此

57
00:03:34,539 --> 00:03:38,450
that static so in that static linking video we basically just linked our jail
静态，所以在静态链接视频中，我们基本上只是链接了我们的监狱

58
00:03:38,650 --> 00:03:42,260
it'll be statically we included the header file and we call gob in it if I
我们将静态地包含头文件，如果我将其称为gob 

59
00:03:42,460 --> 00:03:45,290
run my program you'll see that it compiled successfully and it prints one
运行我的程序，您会看到它已成功编译并打印了一个

60
00:03:45,490 --> 00:03:49,760
as our result here now not everything is going to change if you link dynamically
作为我们这里的结果，如果您动态链接，那么一切都不会改变

61
00:03:49,960 --> 00:03:52,700
this include for example remains identical I mean the header file
这包括例如保持不变，我的意思是头文件

62
00:03:52,900 --> 00:03:56,719
supports both static and dynamic linking there are actually differences that need
同时支持静态链接和动态链接

63
00:03:56,919 --> 00:04:00,410
to occur with your declarations for things like functions if you want to
如果您想在函数的声明中出现

64
00:04:00,610 --> 00:04:03,410
link statically versus dynamically and we'll explore that in a minute but G
静态链接还是动态链接，我们将在一分钟内进行探讨，但G 

65
00:04:03,610 --> 00:04:06,950
love W like most libraries actually kind of supports both static and dynamic
像大多数库一样爱W实际上支持静态和动态

66
00:04:07,150 --> 00:04:10,910
linking with that same header file as we'll see in a minute so this doesn't
链接到我们将在稍后看到的相同的头文件，因此不会

67
00:04:11,110 --> 00:04:14,030
change and if I right click on my project you go to properties you'll see
更改，如果我右键单击我的项目，则转到属性，您将看到

68
00:04:14,229 --> 00:04:18,288
that under CC busbars general I have that include part again if you know 100%
如果您知道100％，则在CC汇流排下，我将再次包含该部分

69
00:04:18,488 --> 00:04:22,120
sure about how to set all of this definitely watch my last video on static
确定如何设置所有这些内容，绝对可以观看我的静态静态视频

70
00:04:22,319 --> 00:04:25,569
blinking even if you're only ever interested in dynamic linking you still
闪烁，即使您只对动态链接感兴趣

71
00:04:25,769 --> 00:04:28,600
need to know what static linking is because a lot of times you'll actually
需要知道什么是静态链接，因为很多时候您会

72
00:04:28,800 --> 00:04:32,259
probably want to link statically if you can instead of dynamically so check that
可能希望静态链接，而不是动态链接，因此请检查

73
00:04:32,459 --> 00:04:35,319
out if you're not sure this doesn't change and then if we go into linker
如果您不确定这不会改变，然后进入链接器

74
00:04:35,519 --> 00:04:40,420
input we see that we have GL fw3 Lib so if I open up my folder with GL w now
输入，我们看到我们有GL fw3 Lib，所以如果现在用GL w打开文件夹

75
00:04:40,620 --> 00:04:43,900
just right-click here get open folder and file explorer and then over here in
只需右键单击此处即可打开文件夹和文件资源管理器，然后在此处

76
00:04:44,100 --> 00:04:47,710
my file explorer I'm just going to go back here into dependencies jail of W
我的文件浏览器，我将回到这里，进入W的依赖监狱

77
00:04:47,910 --> 00:04:52,449
and Lib so you can see here that I've got three files jail also p3 don't live
和Lib，这样您就可以在这里看到我有三个文件，而且p3都不存在

78
00:04:52,649 --> 00:04:55,000
which is clearly the one that we're actually currently linking the static
显然，这是我们当前正在链接静态

79
00:04:55,199 --> 00:04:58,150
library let's go ahead and get rid of this one and replaced it with the
库，让我们继续进行下去，并删除它，并用

80
00:04:58,350 --> 00:05:02,500
dynamic version if we go back over here you can see clearly that we have to kind
动态版本，如果我们回到这里，您可以清楚地看到我们必须友善

81
00:05:02,699 --> 00:05:05,710
of dll files well one one of those actually called jail that we three dll
的dll文件好，我们三个dll实际上称为监狱之一

82
00:05:05,910 --> 00:05:09,970
Lib that one is just basically a series of pointers into kind of this dll file
解放了一个基本上只是一系列指向该dll文件种类的指针

83
00:05:10,170 --> 00:05:13,509
so that we don't have to actually retrieve the locations of everything at
这样我们就不必实际获取所有内容的位置

84
00:05:13,709 --> 00:05:17,350
runtime it's very important that these two are compiled at the same time
在运行时，这两者必须同时编译非常重要

85
00:05:17,550 --> 00:05:21,550
because if you try and kind of use a different static library to link with a
因为如果您尝试并使用其他静态库来与

86
00:05:21,750 --> 00:05:26,740
dll at one time you're probably going to get functions mismatching and wrong kind
一次出现dll可能会导致功能不匹配和种类错误

87
00:05:26,939 --> 00:05:29,889
of memory addresses for function pointers and just it's not gonna work
函数指针的内存地址数量，只是不起作用

88
00:05:30,089 --> 00:05:33,610
out basically what I'm saying so this of course is distributed by jail fw so they
基本上我在说什么，所以这当然是由监狱fw分发的，所以他们

89
00:05:33,810 --> 00:05:37,660
were compiled at the same time and they are related directly to each other that
是在同一时间被编译的，它们彼此直接相关， 

90
00:05:37,860 --> 00:05:41,620
you can't separate these two so back over here I'm going to type in jail of
你不能将这两个分开，所以回到这里，我要输入

91
00:05:41,819 --> 00:05:48,250
w3 DLL lib and that's all we have to do from this side now if I hit OK and I try
 w3 DLL lib，这是我们现在需要做的所有事情，如果我单击确定，然后尝试

92
00:05:48,449 --> 00:05:52,180
and build this it's going to work successfully right so you can see that
并构建它，它将成功运行，这样您就可以看到

93
00:05:52,379 --> 00:05:55,030
it's generated my executable file fighters remove this over here I don't
它已经生成了我的可执行文件，战斗机在这里删除了

94
00:05:55,230 --> 00:05:57,490
really cares about the errorless anyway output is where it's at
真正关心无错误的输出是在哪里

95
00:05:57,689 --> 00:06:00,430
I'll move this over here you can see that HelloWorld exe has successfully
我将其移到此处，您可以看到HelloWorld exe已成功

96
00:06:00,629 --> 00:06:03,939
been generated let's try and launch our application now all right and you can
已生成，让我们尝试立即启动我们的应用程序，您可以

97
00:06:04,139 --> 00:06:06,639
see that we get this error message that I talked about earlier the code
看到我们收到了我之前讨论的错误消息

98
00:06:06,839 --> 00:06:11,139
execution cannot proceed because Java b3o tol was not found this is where we
由于找不到Java b3o tol，执行无法继续进行

99
00:06:11,339 --> 00:06:15,520
actually have to show our program this is Jill Toby thread of DLL
实际上必须显示我们的程序，这是DLL的Jill Toby线程

100
00:06:15,720 --> 00:06:19,540
right I've got it over here please Lord and the way that we do that in a simple
是的，我已经把它放在这里了，求主，我们以简单的方式做到这一点

101
00:06:19,740 --> 00:06:23,160
case is just basically placing that dll file in the same location
情况基本上是将dll文件放在相同的位置

102
00:06:23,360 --> 00:06:27,949
as out executable so if I got makea I can copy this dll file go back over here
作为可执行文件，所以如果我得到了makea，我可以复制此dll文件，请回到此处

103
00:06:28,149 --> 00:06:32,489
interdependencies HelloWorld debug and you can say this is the path with my
相互依赖的HelloWorld调试，您可以说这是我的

104
00:06:32,689 --> 00:06:36,269
executable if I paste this in here and I go back to either visual studio I could
如果我将其粘贴到此处，可以返回可执行文件，则可以返回任何一个Visual Studio 

105
00:06:36,468 --> 00:06:39,838
run it from there as well and I try and run my program you can see that it works
从那里也运行它，我尝试运行我的程序，您可以看到它有效

106
00:06:40,038 --> 00:06:44,009
successfully and we actually get one over here all right beautiful if I go
成功的话，如果我去的话，我们真的可以在这里得到美丽

107
00:06:44,209 --> 00:06:47,699
back over here to my actual Windows Explorer I can also double click on
回到这里回到我实际的Windows资源管理器，我也可以双击

108
00:06:47,899 --> 00:06:50,999
polar world and you can see that it runs just fine in here without bedroom studio
极地世界，您可以看到它在没有卧室工作室的情况下运行良好

109
00:06:51,199 --> 00:06:54,569
without any kind of debug is attached as well so that's all there is to it we
没有任何类型的调试也是如此，所以这就是我们的全部

110
00:06:54,769 --> 00:06:58,468
link against the static library and then we actually make sure that we have the
链接到静态库，然后我们实际上确保拥有

111
00:06:58,668 --> 00:07:02,879
DLL in and in an accessible place you can throw out your application actually
 DLL在可访问的位置以及您可以实际访问的地方

112
00:07:03,079 --> 00:07:08,639
set paths to certain libraries like search locations but the root folder of
设置某些库（例如搜索位置）的路径，但路径为

113
00:07:08,839 --> 00:07:11,790
your executable so the folder that actually contains your application is
您的可执行文件，因此实际上包含您的应用程序的文件夹是

114
00:07:11,990 --> 00:07:16,439
just automatically kind of a search path so if you put it into the same folder if
只是自动生成一种搜索路径，因此，如果将其放入同一文件夹中， 

115
00:07:16,639 --> 00:07:20,100
you put your DLL file into the same folder as your executable you'll be fine
您将DLL文件放入与可执行文件相同的文件夹中，就可以了

116
00:07:20,300 --> 00:07:22,980
okay so one more thing that I wanted to mention is if we take a look at this
好吧，我想说的另一件事是，如果我们看看这个

117
00:07:23,180 --> 00:07:26,129
actual header file we can kind of start to see the differences between what
实际的头文件，我们可以开始看一下两者之间的区别

118
00:07:26,329 --> 00:07:29,278
happens during static and dynamic linking specifically if we look at
发生在静态和动态链接期间，特别是如果我们查看

119
00:07:29,478 --> 00:07:32,490
pretty much any function over here you'll see that it defines geo wapi
在这里几乎所有的功能，您都会看到它定义了geo wapi 

120
00:07:32,689 --> 00:07:37,259
before the return type and then the actual function name so if I go to the
返回类型之前，然后是实际的函数名称，所以如果我转到

121
00:07:37,459 --> 00:07:39,838
definition of that and there's a few definitions lost click on the first one
的定义，还有一些定义会丢失，请点击第一个

122
00:07:40,038 --> 00:07:43,709
you can see we have this whole thing here we've got this case for each we're
您可以看到我们在这里拥有全部东西我们每个人都有这种情况

123
00:07:43,908 --> 00:07:48,088
on Windows and we're trying to build the DLL file then it has to actually export
在Windows上，我们尝试构建DLL文件，那么它实际上必须导出

124
00:07:48,288 --> 00:07:50,490
the DLL functions this is actually really important if you build it without
 DLL函数，如果没有

125
00:07:50,689 --> 00:07:55,139
this it's not gonna work then there's also win32 and just gelato BDL which
这是行不通的，然后还有win32和gelato BDL 

126
00:07:55,338 --> 00:07:59,369
means we're calling here shall be as a win32 dll which does deathless bacterial
意味着我们在这里所说的应该是一个不会死细菌的win32 dll 

127
00:07:59,569 --> 00:08:02,819
input and then finally there's also the building the static library and then
输入，最后还有构建静态库，然后

128
00:08:03,019 --> 00:08:06,269
calling a GL of dopiaza static library which is to find the show w api is
调用dopiaza静态库的GL，以查找显示w api是

129
00:08:06,468 --> 00:08:09,179
nothing so that's actually what's being defined right now now here's an
什么都没有，所以实际上这是现在正在定义的内容

130
00:08:09,379 --> 00:08:13,528
interesting question we are still defining Jeff w API as
一个有趣的问题，我们仍然将Jeff w API定义为

131
00:08:13,728 --> 00:08:20,459
nothing even though we're using it as an actual DLL so shouldn't GLW dll be
即使我们将其用作实际的DLL也没有任何作用，所以GLW dll不应

132
00:08:20,658 --> 00:08:24,778
defined if we go back to the main if I go to properties over here and I go into
如果我回到这里的属性，然后进入

133
00:08:24,978 --> 00:08:31,110
my c c++ preprocessor and then I add that progresses the definition gfw dll
我的c c ++预处理程序，然后添加定义gfw dll的进度

134
00:08:31,310 --> 00:08:35,759
is try and compile the code one more time okay seems pretty legit here it is
是尝试再编译一次代码好吗，这似乎很合法

135
00:08:35,960 --> 00:08:40,279
let's hit f5 and it runs and it looks like it runs in exactly the same way so
让我们按f5键，它开始运行，看起来它以完全相同的方式运行，因此

136
00:08:40,480 --> 00:08:45,929
what's going on here why did I not have to define deco spec DLL input why did I
这是怎么回事，为什么我不必定义装饰规范DLL输入，为什么我要

137
00:08:46,129 --> 00:08:50,939
have to just like even nothing works and why is that and that is a question for
必须甚至连什么都不起作用，这是为什么，这是一个问题

138
00:08:51,139 --> 00:08:54,449
you guys you've been asking me for a while to give you kind of challenges and
你们一直在问我一段时间，给您带来一些挑战， 

139
00:08:54,649 --> 00:08:59,009
things to do for homework or whatever is a great example why is this happening
做功课的事情或什么很好的例子说明为什么会发生这种情况

140
00:08:59,210 --> 00:09:03,269
why am I able why do I not have to actually death respect the other import
我为什么能为什么我不必真的去尊重其他人

141
00:09:03,470 --> 00:09:07,139
to be able to link to these functions in my DLL file successfully leave a comment
能够链接到我的DLL文件中的这些功能成功发表评论

142
00:09:07,340 --> 00:09:12,389
below whoever wins this will just get pinned as the comment that's you guys a
低于谁，这将被固定为评论，你们是一个

143
00:09:12,590 --> 00:09:16,799
pretty decent prize I'm alright but yeah just leave leave a comment below and
不错的奖我没事，但是是的，请在下面留下评论， 

144
00:09:17,000 --> 00:09:19,199
we'll see who can figure it out anyway thanks for watching I hope you guys
我们将看看谁能解决这个问题感谢您的收看，希望大家

145
00:09:19,399 --> 00:09:21,659
enjoy this video if you did you can hit that like button any more questions
如果您喜欢的话，请欣赏此视频，再按一下“喜欢”按钮

146
00:09:21,860 --> 00:09:24,929
about dynamic linking about static linking all that stuff leave a comment
关于动态链接关于静态链接所有这些东西发表评论

147
00:09:25,129 --> 00:09:28,109
below as well as hop on my discord that which is the channel to come such
下面，以及我不和谐的地方，那就是这样的渠道

148
00:09:28,309 --> 00:09:30,899
discord there's a bunch of people there talking about pretty much everything to
不和谐那里有一堆人在谈论几乎所有的事情

149
00:09:31,100 --> 00:09:35,219
do with C++ and OpenGL and graphics and programming and just everything it's a
使用C ++和OpenGL以及图形和编程，仅此而已

150
00:09:35,419 --> 00:09:37,829
really good time if you really like this video and you want to help support the
如果您真的很喜欢这部影片，并且想协助支援

151
00:09:38,029 --> 00:09:40,979
series that I do here on YouTube then you can go to patreon.com/scishow Cherno
我在YouTube上做的系列，然后您可以去patreon.com/scishow Cherno 

152
00:09:41,179 --> 00:09:45,329
so guess the pretty cool rewards like getting videos early and lots of other
因此，请猜猜这些很酷的奖励，例如及早获取视频以及其他许多奖励

153
00:09:45,529 --> 00:09:49,859
fun stuff so definitely check that out and I'll see you guys next time good bye
好玩的东西，所以一定要检查一下，下次再见，再见

154
00:09:50,059 --> 00:09:55,059
[Music]
 [音乐] 

