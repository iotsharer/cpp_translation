﻿1
00:00:00,030 --> 00:00:03,309
hey little guys my name is Machado and welcome back to my people of clock
嘿，小伙子们，我的名字叫马查多（Machado），欢迎回到我的钟表人

2
00:00:03,509 --> 00:00:06,280
series this is going to be a really quick episode in which I'm just going to
系列，这将是一个非常快速的情节，其中我将要

3
00:00:06,480 --> 00:00:10,930
based answer the question what is the difference between a struct class in C++
基于答案的问题C ++中的struct类之间有什么区别

4
00:00:11,130 --> 00:00:14,530
the last time we talked about classes in general and we had a bit of a basic
上一次我们一般地讲课时，我们有一些基本的知识

5
00:00:14,730 --> 00:00:17,919
introduction to what classes actually work you guys haven't seen that video
你们上课的实际效果介绍​​还没看过那个视频

6
00:00:18,118 --> 00:00:20,859
check it out there'll be a link to the description below as well as like some
检查一下，下面将有一个链接到下面的描述

7
00:00:21,059 --> 00:00:24,339
kind of weird annotation I think what are they called cards they'll be a card
有点怪异的注解，我想他们叫卡片，它们将成为卡片

8
00:00:24,539 --> 00:00:27,460
somewhere on the screen but basically we have these two terms in table bus but
屏幕上的某个位置，但基本上我们在表总线中有这两个术语，但是

9
00:00:27,660 --> 00:00:33,698
struct which is short for structure or a class and they might appear to be sort
 struct，它是结构或类的缩写，它们似乎是排序的

10
00:00:33,899 --> 00:00:36,608
of similar and a lot of people are a little bit confused about what the
相似的人很多人对

11
00:00:36,808 --> 00:00:40,029
differences are exactly when you should be using a struct when you should be
差异正是您应该使用结构的时间

12
00:00:40,229 --> 00:00:43,570
using a class this video is going to address all that so the difference is
使用一个班级，该视频将解决所有这些问题，因此区别在于

13
00:00:43,770 --> 00:00:49,178
this there is basically non there is one small difference to do with visibility
这基本上是不存在的，与可见性有一个小差异

14
00:00:49,378 --> 00:00:54,788
so last time when I talked about how we had a class having its members private
所以上次当我谈论我们如何让班级的成员私有时

15
00:00:54,988 --> 00:00:59,169
by default which meant that if I was to do something like this however I did not
默认情况下，这意味着如果我要做这样的事情，但是我没有

16
00:00:59,369 --> 00:01:03,279
declare this as public then we would get an error because you can see that it
将此声明为公开，那么我们会收到错误消息，因为您可以看到它

17
00:01:03,479 --> 00:01:07,539
would tell us that the move method in the player class is inaccessible because
会告诉我们播放器类中的move方法不可访问，因为

18
00:01:07,739 --> 00:01:11,469
it's in Marshall's private default meaning only other methods inside this
这是Marshall的私有默认值，仅意味着其中的其他方法

19
00:01:11,670 --> 00:01:15,009
class can actually access that move method which is why we have to write
类实际上可以访问该move方法，这就是我们必须编写的原因

20
00:01:15,209 --> 00:01:19,149
public here if we want to execute this code because of course we're calling
如果要执行此代码，请在此处公开，因为我们当然要调用

21
00:01:19,349 --> 00:01:23,049
player don't move outside of the player class and that's essentially where the
玩家不要移出玩家类别，这实际上是

22
00:01:23,250 --> 00:01:28,450
difference comes in a class is private by default so if you don't specify any
不同之处在于，默认情况下，一个类是私有的，所以如果您不指定任何

23
00:01:28,650 --> 00:01:32,590
kind of visibility modifier like I did here if you don't just get rid of it and
就像我在这里所做的那样，如果您不只是摆脱它，并且

24
00:01:32,790 --> 00:01:37,659
you don't specify anything at all the default is private however was the
您根本不指定任何内容，默认为private，但是默认为

25
00:01:37,859 --> 00:01:42,189
struct the default is public that is the only difference between the structs in
 struct默认为public，是struct之间唯一的区别

26
00:01:42,390 --> 00:01:45,730
the class technically and you can see over here in our code we get an error
从技术上讲，该类可以在我们的代码中看到

27
00:01:45,930 --> 00:01:50,289
because again move is inaccessible however if I change this to safe struct
因为再次无法移动，但是如果我将其更改为安全结构

28
00:01:50,489 --> 00:01:53,769
player that solution that has changed the work class construct that's all I've
播放器改变了我已经完成的工作类构造的解决方案

29
00:01:53,969 --> 00:01:57,429
done this is all fine now and if I actually did want something to be
这样做现在一切都很好，如果我确实确实想要做某事

30
00:01:57,629 --> 00:02:01,359
private I would explicitly have to write private and then of course that takes us
私有我必须明确地写私有，然后这当然需要我们

31
00:02:01,560 --> 00:02:04,959
back to the same error that we got with a class that is the difference it's
回到与我们在类中遇到的相同错误，不同之处在于

32
00:02:05,159 --> 00:02:09,820
incredibly simple that's really all there is to it however instead of just
非常简单，实际上就是全部，而不仅仅是

33
00:02:10,020 --> 00:02:14,118
putting up a two-minute video just mentioning that I want to kind of
放一个两分钟的视频，只是提到我想

34
00:02:14,318 --> 00:02:18,200
talk about how we might be defining differences between those two words
谈论我们如何定义这两个词之间的差异

35
00:02:18,400 --> 00:02:24,140
because like they might not technically have much of a difference however the
因为就像他们在技术上可能没有太大的区别，但是

36
00:02:24,340 --> 00:02:26,960
usage in code is actually going to differ
代码中的用法实际上将有所不同

37
00:02:27,159 --> 00:02:31,400
the only reason struck really even exists in C++ it's because of the
 C ++中真正存在的唯一原因是

38
00:02:31,599 --> 00:02:35,930
backwards compatibility that it wants to maintain with C because C code doesn't
它想与C保持向后兼容性，因为C代码没有

39
00:02:36,129 --> 00:02:40,039
have classes it does have structures though and if we were suddenly to wipe
有课程，但确实有结构，如果我们突然擦去

40
00:02:40,239 --> 00:02:44,570
out this whole struct keyword entirely then we would lose all compatibility
全部删除整个struct关键字，那么我们将失去所有兼容性

41
00:02:44,770 --> 00:02:48,289
because the compilers the superclass compiler wouldn't know what struct was
因为编译器超类编译器不知道结构是什么

42
00:02:48,489 --> 00:02:51,800
of course you could fix that pretty easily by just using a hash to find so
当然，您可以通过使用哈希查找来轻松解决此问题

43
00:02:52,000 --> 00:02:56,719
we could write something like hash define struct class right and then
我们可以写一些类似hash的struct结构类，然后

44
00:02:56,919 --> 00:03:01,160
suddenly what that's going to do is replace all of our struct with classes
突然要做的是用类替换我们所有的结构

45
00:03:01,360 --> 00:03:05,150
so in this case even if code might look pretty pretty simple
所以在这种情况下，即使代码看起来非常简单

46
00:03:05,349 --> 00:03:09,650
I can pilot you consider suddenly we get that same error telling us no can be
我可以引导您考虑一下，突然之间我们遇到了同样的错误，告诉我们不可能

47
00:03:09,849 --> 00:03:13,039
accessed inside a class even though we've used it we use the web structure
即使我们已经使用过它，也可以使用Web结构访问它

48
00:03:13,239 --> 00:03:16,910
because the course is replaced structure with the word class and then maybe yes
因为课程是用class一词代替结构，然后也许是

49
00:03:17,110 --> 00:03:20,780
you can get that kind of compatibility between C code and C bustle code back
您可以获得C代码和C繁忙代码之间的那种兼容性

50
00:03:20,979 --> 00:03:25,460
because ideally you should be able to go into C code and replace the works trucks
因为理想情况下，您应该能够使用C代码并替换工程车

51
00:03:25,659 --> 00:03:28,759
with class and then make it public and then that would be it that would be the
与班级，然后将其公开，然后就是那个

52
00:03:28,959 --> 00:03:31,969
end that there would be no more different in that actual scenario so
最终，在该实际情况中不会有更多不同，因此

53
00:03:32,169 --> 00:03:36,230
then the difference kind of semantically and how people see it come down to more
那么差异在语义上以及人们如何看待差异

54
00:03:36,430 --> 00:03:40,490
or less the usage if there is no difference when do I use trucks purses
如果我使用卡车皮包没有区别，请减少用法

55
00:03:40,689 --> 00:03:45,349
class do I use a struct if I want all of my members to be public and I don't want
如果我希望所有成员都公开并且我不想公开，我是否使用结构

56
00:03:45,549 --> 00:03:49,879
to have to write the web public is it really that trivial and yes by the way
必须写网络公开真的很简单，是的

57
00:03:50,079 --> 00:03:54,319
it is actually that trivial and so because of that people have kind of come
实际上那是微不足道的，所以因为人们有种来

58
00:03:54,519 --> 00:03:58,730
up with their own definitions for what I think astruc should be and what I think
针对我认为的astruc以及我的想法制定自己的定义

59
00:03:58,930 --> 00:04:03,680
a class should be however there is no really like there really is no like
一个班级应该是，但是没有真正的喜欢，没有真正的喜欢

60
00:04:03,879 --> 00:04:08,509
right or wrong answer it's kind of up to your style of programming so this is my
对还是错的答案取决于您的编程风格，所以这是我的

61
00:04:08,709 --> 00:04:12,050
video let's talk a little bit about my style of programming and where I might
视频让我们谈谈我的编程风格以及我可能在哪里

62
00:04:12,250 --> 00:04:17,569
use each type so I like to use struct whenever possible when I'm basically
使用每种类型，因此我喜欢在基本情况下尽可能使用struct 

63
00:04:17,769 --> 00:04:22,218
talking about plain old data or p OD basically where i'm talking about some
基本上是在谈论普通的旧数据或OD， 

64
00:04:22,418 --> 00:04:26,189
kind of structure which just represents variables a bunch of variables and
一种仅代表变量的结构一堆变量

65
00:04:26,389 --> 00:04:29,790
that's really all isn't there for a great example of this might be something
事实并非如此，这可能是一个很好的例子

66
00:04:29,990 --> 00:04:33,809
like a mathematical vector class if I were to come over here and I wanted to
就像数学向量类，如果我要来这里，我想

67
00:04:34,009 --> 00:04:39,119
make some kind of structure which just held two floats together such as a back
制作一种仅将两个浮子固定在一起的结构，例如后背

68
00:04:39,319 --> 00:04:44,699
to I might define it as a struct and just have my x and y floats
我可能将其定义为一个结构，并且只有我的x和y浮点数

69
00:04:44,899 --> 00:04:49,649
why because fundamentally this back to class or struct or whatever you want to
为什么，因为从根本上讲，这可以返回类或结构或您想要的任何内容

70
00:04:49,848 --> 00:04:55,199
call it is that to structure is just a representation of two floats
所谓结构就是两个浮点数的表示

71
00:04:55,399 --> 00:04:59,399
that's what it is at its core it's not supposed to really contain a massive
这就是它的核心所在，不应真正包含大量

72
00:04:59,598 --> 00:05:03,088
amount of functionality like a player class might where it might be having it
像播放器类这样的功能可能在哪里有

73
00:05:03,288 --> 00:05:06,059
might like a player class might have a 3d model it might handle all the
可能像某个玩家课程可能具有3D模型，它可以处理所有

74
00:05:06,259 --> 00:05:10,290
rendering code for that 3d model it might be handling like how the player
渲染该3D模型的代码，其处理方式可能类似于播放器

75
00:05:10,490 --> 00:05:14,460
moves around the map and taking in keyboard input and all of that there's
在地图上移动并输入键盘输入，所有这些

76
00:05:14,660 --> 00:05:19,499
just so much functionality there right whereas what this is it's just two
那里有那么多功能，而这只是两个

77
00:05:19,699 --> 00:05:25,230
variables and we proved them literally just for the reason of making our code
变量，我们从字面上证明了它们只是出于编写代码的原因

78
00:05:25,430 --> 00:05:29,879
easier to use that's all that we've done and of course that's not to say that I'm
更容易使用了，这就是我们所做的全部，当然，这并不是说我在

79
00:05:30,079 --> 00:05:34,139
not going to be adding methods to this I absolutely will I might add a method
不会为此添加方法，我绝对会添加方法

80
00:05:34,339 --> 00:05:38,399
called add which takes in a null evac'd too and then adds it to the current
称为添加，它也接受一个空的evac'd，然后将其添加到当前

81
00:05:38,598 --> 00:05:43,410
vector by doing something like this right but again what I'm doing here is
通过做这样的事情矢量，但我在这里再次做的是

82
00:05:43,610 --> 00:05:48,480
I'm just manipulating these variables I've just added a function that will
我只是操纵这些变量，我刚刚添加了一个函数

83
00:05:48,680 --> 00:05:52,170
manipulate those variables but at the end of the day I'm still just talking
操作这些变量，但是到最后我还是在谈论

84
00:05:52,370 --> 00:05:55,740
about those two variables now of course if you were really to break it down and
当然，如果您真的要分解这两个变量， 

85
00:05:55,939 --> 00:05:59,730
think about it really hard then you could argue that at
想一想真的很难，那么你可以争论

86
00:05:59,930 --> 00:06:03,629
the end of the day even the player class is just manipulating those variables
一天的结束，甚至是玩家职业也只是在操纵那些变量

87
00:06:03,829 --> 00:06:07,769
however there's a bit of a difference in terms of design because we're talking
但是在设计方面有些差异，因为我们正在谈论

88
00:06:07,968 --> 00:06:11,610
about something that is massively massively more complicated the other
关于一些非常复杂的东西

89
00:06:11,810 --> 00:06:18,269
scenario is inheritance I will never use inheritance with structs if if I get to
场景是继承，如果我愿意的话，我将永远不会在结构上使用继承

90
00:06:18,468 --> 00:06:21,600
the point where I'm actually going to have an entire class hierarchy or some
我实际上将拥有整个类层次结构或某些层次结构的点

91
00:06:21,800 --> 00:06:25,620
kind of inheritance hierarchy I'm going to use a class for that because again
一种继承层次结构，我将为此使用一个类，因为再次

92
00:06:25,819 --> 00:06:29,309
inheritance is something that's adding another level of complexity and I just
继承增加了另一层次的复杂性，我只是

93
00:06:29,509 --> 00:06:33,329
want my structures to be structures of data that's it and additionally to that
希望我的结构成为仅此而已的数据结构

94
00:06:33,528 --> 00:06:36,329
if you try and mix those for example you have a class called a
如果您尝试将其混合，例如，您有一个称为

95
00:06:36,529 --> 00:06:40,139
and then a struct called B which inherits from a some compilers will give
然后从一些编译器继承的名为B的结构将给出

96
00:06:40,339 --> 00:06:43,980
you warning telling you that you're inheriting from a class but you're a
您警告说您是从类继承的，但您是

97
00:06:44,180 --> 00:06:47,009
struct and there are some other kind of minor differences but again that is
结构，还有一些其他细微的区别，但同样是

98
00:06:47,209 --> 00:06:50,910
warning secondly your code will still work it's just a bit of a semantical
警告第二，您的代码仍然可以使用，只是语义上的一点

99
00:06:51,110 --> 00:06:54,990
difference so those are kind of my reasons for using instructors in the
差异，这是我在

100
00:06:55,189 --> 00:07:00,509
class if I just want to represent some data in a structure I will use a struct
类，如果我只想表示结构中的某些数据，我将使用结构

101
00:07:00,709 --> 00:07:04,980
but if I've got an entire class filled with functionality like a like a game
但是如果我整个班级都充满了像游戏这样的功能

102
00:07:05,180 --> 00:07:08,759
world or a player or something that additionally might have inheritance and
世界，玩家或其他可能具有继承权的事物， 

103
00:07:08,959 --> 00:07:13,199
all of these all these systems I'm going to use a class and again that's how I
所有这些系统我将使用一个类，这也是我的方式

104
00:07:13,399 --> 00:07:16,560
personally differentiate between those two types that's how a lot of people are
亲自区分这两种类型，这就是很多人

105
00:07:16,759 --> 00:07:21,199
seen in the industry differentiate between those two types but again if
在行业中看到的区分这两种类型，但如果

106
00:07:21,399 --> 00:07:25,560
there is technically no difference apart from that visibility you can use a
除了能见度之外，技术上没有什么区别，您可以使用

107
00:07:25,759 --> 00:07:29,370
struct wherever you can use class that they're going to work the same way
可以在任何可以使用类的地方使用struct，它们将以相同的方式工作

108
00:07:29,569 --> 00:07:32,790
hope you guys enjoyed this video if you just look at that like button of course
希望大家喜欢这个视频，当然，只要看一下类似的按钮

109
00:07:32,990 --> 00:07:36,689
you can follow me as well on Twitter and Instagram links will be in the
您也可以在Twitter上关注我，Instagram链接将位于

110
00:07:36,889 --> 00:07:39,660
description below if you really like these videos and you want to support
如果您真的喜欢这些视频并且想要支持，请在下面进行描述

111
00:07:39,860 --> 00:07:43,680
this series and see more then you can support me on patreon block comport -
这个系列，以及更多内容，您可以在patreon block comport上支持我- 

112
00:07:43,879 --> 00:07:46,889
the shadow and additionally to just supporting me and making sure that I can
阴影，除了支持我并确保我可以

113
00:07:47,089 --> 00:07:50,910
make more videos you'll get access to early draft as well as a discussion
制作更多视频，您将可以使用早期草稿以及讨论

114
00:07:51,110 --> 00:07:53,699
board where we talk about what actually goes into this video so if your
我们讨论该视频的实际内容，因此，如果您的

115
00:07:53,899 --> 00:07:56,280
interests in that check out the link in the description below
有兴趣的人请查看下面描述中的链接

116
00:07:56,480 --> 00:08:00,210
hey Fiona come forth - Jenna and next time we're going to continue on our
嘿，菲奥娜（Fiona）出来-珍娜（Jenna），下一次，我们将继续

117
00:08:00,410 --> 00:08:03,990
journey as to what classes are going to talk about stuff like inheritance and
关于什么类将要讨论诸如继承和继承之类的事情的旅程

118
00:08:04,189 --> 00:08:08,460
look at some examples of existing code to see how you should be structuring
查看现有代码的一些示例，以了解如何进行结构化

119
00:08:08,660 --> 00:08:11,759
your puzzles how you should be writing them and how we can use them in our code
您的难题您应该如何编写它们以及如何在代码中使用它们

120
00:08:11,959 --> 00:08:20,300
I'll see you guys next time goodbye [Music]
下次再见。 

121
00:08:27,029 --> 00:08:32,029
[Music]
 [音乐] 

