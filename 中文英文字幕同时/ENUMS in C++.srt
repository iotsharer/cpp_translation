﻿1
00:00:00,000 --> 00:00:02,948
Hey look guys my name is Jonah and welcome back to my sequel Plus series
嗨，大家好，我叫乔纳（Jonah），欢迎回到我的续集Plus系列

2
00:00:03,149 --> 00:00:06,879
today we're going to be talking about in ohms instead what was so enormous short
今天我们将以欧姆为单位进行讨论

3
00:00:07,080 --> 00:00:11,500
for enumeration and basically all it is is a set of values if you want a more
进行枚举，如果您想要更多，基本上就是一组值

4
00:00:11,699 --> 00:00:17,079
practical definition than all Menem is is a way to give a name to a value so
比所有Menem更实际的定义是给值赋名称的一种方法

5
00:00:17,278 --> 00:00:22,600
instead of having a bunch of integers called ABC we can have an enum which has
而不是有一堆叫做ABC的整数，我们可以有一个枚举

6
00:00:22,800 --> 00:00:26,679
the values ABC which correspond to integers it also helped us define a set
对应于整数的ABC值也帮助我们定义了一个集合

7
00:00:26,879 --> 00:00:31,239
of values so that instead of just having an integer as a type which of course you
值的大小，因此您当然不只是拥有一个整数

8
00:00:31,439 --> 00:00:35,469
could assign any integer to you limit which values can be assigned to what
可以为您分配任何整数，以限制可以将哪些值分配给什么

9
00:00:35,670 --> 00:00:38,858
so without complicating things that's really all that it is it's just a way to
因此，无需复杂化所有事情，这只是一种方法

10
00:00:39,058 --> 00:00:43,268
name values it's very useful for when you want to use integers to represent
名称值，当您要使用整数表示时非常有用

11
00:00:43,469 --> 00:00:47,018
certain states or certain values however you want to give them a name so that
某些状态或值，但是您想给它们起一个名字，以便

12
00:00:47,219 --> 00:00:50,709
your code is more readable at the end of the day an enum is just an integer
在一天结束时，您的代码更具可读性枚举只是一个整数

13
00:00:50,909 --> 00:00:54,279
however in your code it looks a little bit different and it helps to keep your
但是在您的代码中看起来有些不同，这有助于保持您的

14
00:00:54,479 --> 00:00:57,579
credit bit cleaner let's dive in and take a look so suppose that I have three
让我们深入研究一下，假设我有三个

15
00:00:57,780 --> 00:01:02,169
values that I wanted to take care of I had a which I might set to zero B
我想要照顾的值有一个我可能会设为零B的值

16
00:01:02,369 --> 00:01:07,778
which I might have set to 1 and C which I might set to so I have 3 values to
我可能将其设置为1和C可能会设置为，所以我有3个值

17
00:01:07,978 --> 00:01:12,189
take care of and then somewhere in mean I might use my value and I might set it
照顾好然后在某个地方意味着我可以使用我的值，并且可以设置它

18
00:01:12,390 --> 00:01:15,819
equal to one of these 3 later on my code I might have some code to check what the
等于我的代码后面这三个之一，我可能有一些代码来检查

19
00:01:16,019 --> 00:01:20,890
current value is and then perform some sort of action this is fine however it
当前值是，然后执行某种操作，这很好，但是

20
00:01:21,090 --> 00:01:24,308
carries a few issues first of all these ABCs are not grouped
首先涉及一些问题这些ABC未被分组

21
00:01:24,509 --> 00:01:28,628
at all somewhere later in your code you might have a D or you might want to
在代码稍后的所有地方，您可能都有一个D或您可能想要

22
00:01:28,828 --> 00:01:32,649
declare a again and suddenly this is a bit of an issue but essentially the
再次声明这是一个问题，但本质上是

23
00:01:32,849 --> 00:01:35,619
biggest problem here is that these aren't groups at all furthermore they
这里最大的问题是这些根本不是团体

24
00:01:35,819 --> 00:01:39,488
are just integers meaning I can assign something like 5 here and suddenly this
只是整数，这意味着我可以在这里分配5 

25
00:01:39,688 --> 00:01:43,090
doesn't make any sense anymore we want to be able to essentially define a type
不再有意义，我们希望能够本质上定义一个类型

26
00:01:43,290 --> 00:01:47,289
which can only be one of these three and it's also grouped in some kind of
这只能是这三个中的一个，并且也被归类为

27
00:01:47,489 --> 00:01:51,219
fashion which is exactly what items can be useful so instead of this I can have
时尚，正是这些东西可能会有用，所以我可以代替它

28
00:01:51,420 --> 00:01:55,329
my enum which are for example and then I can list off which values I want to have
例如我的枚举，然后我可以列出我想要的值

29
00:01:55,530 --> 00:02:00,369
here such as a B or C instead of having an integer as my type I can now use the
在这里，例如B或C，而不是整数，我现在可以使用

30
00:02:00,569 --> 00:02:03,829
enum name as an actual type so I can write example value
枚举名称为实际类型，因此我可以编写示例值

31
00:02:04,030 --> 00:02:07,340
I'm going to change my integers to be lowercase here just so that I can use my
我将在这里将我的整数更改为小写，以便可以使用

32
00:02:07,540 --> 00:02:10,610
enum values properly and you can see that I can assign my value just like
正确枚举值，您可以看到我可以像分配值一样

33
00:02:10,810 --> 00:02:15,439
this if I try and assign something else I'll get an error because it has to be
如果我尝试分配其他内容，则会出现错误，因为它必须是

34
00:02:15,639 --> 00:02:19,850
one of these three now these are just integers at the end of the day keep that
这三者之一现在只是一天结束时的整数， 

35
00:02:20,050 --> 00:02:24,080
in mind I can still do a check if I if I reverted back to B I can still do a
请记住，如果我恢复使用BI，我仍然可以进行检查

36
00:02:24,280 --> 00:02:29,810
check such as value equals equals 1 because the does carry the value 1 if
检查，例如value equals等于1，因为如果

37
00:02:30,009 --> 00:02:32,780
you hover your mouse over this it will even tell you the B is 1 you can
您将鼠标悬停在它上面，它甚至会告诉您B为1，您可以

38
00:02:32,979 --> 00:02:35,960
actually specify the value of n should be variable 2 well if you don't by
实际指定n的值应该是变量2，如果不这样做的话

39
00:02:36,159 --> 00:02:39,830
default the first one will be 0 and then it will increment one by one successful
默认情况下，第一个将为0，然后成功将其递增1 

40
00:02:40,030 --> 00:02:45,770
value you just set it equal to something so I could have this is 0 this is 256
值，您只需将其设置为等于某个值，所以我可以将其设置为0，即256 

41
00:02:45,969 --> 00:02:49,880
whatever you really want if you start from a number that is not 0 such as 5
如果您从一个非0的数字（例如5）开始，则您真正想要的是什么

42
00:02:50,080 --> 00:02:53,990
and you don't actually specify the rest of these values you can see that this
并且您实际上没有指定其余的值，您可以看到

43
00:02:54,189 --> 00:02:57,860
will be 6 and this will be 7 because it will basically just starts on 5 and then
将是6，这将是7，因为它基本上只会从5开始，然后

44
00:02:58,060 --> 00:03:02,030
keep going up one other thing that you can also do here is specify which type
继续前进另一件事，您也可以在此处指定哪种类型

45
00:03:02,229 --> 00:03:05,840
of integer you want the Xenon to be and you can do so by writing a colon and
想要氙气成为整数，可以通过写一个冒号和

46
00:03:06,039 --> 00:03:09,560
then the data types are for example an unsigned char enum is by default are
那么数据类型是例如一个无符号字符枚举，默认情况下是

47
00:03:09,759 --> 00:03:13,099
32-bit integers however you can see in this case there's no need for us to use
 32位整数，但是您可以在这种情况下看到不需要我们使用

48
00:03:13,299 --> 00:03:17,750
the 32-bit value we could get away with using an 8-bit integer such as a non
我们可以使用8位整数（例如non 

49
00:03:17,949 --> 00:03:21,740
fine char and we'd be just fine and suddenly it's taking way less memory you
好的字符，我们会好起来的，突然间，它占用的内存更少了

50
00:03:21,939 --> 00:03:24,650
won't be able to assign this to something like a float of course because
当然不能将其分配给类似float的东西，因为

51
00:03:24,849 --> 00:03:29,240
the flow instance and integer and it has to be an integer so let's leave this as
流实例和整数，它必须是整数，所以我们将其保留为

52
00:03:29,439 --> 00:03:33,349
a chart for our example so there we go that's basically what I mean arm is just
我们的示例的图表，所以我们基本上就是说手臂就是

53
00:03:33,549 --> 00:03:37,069
a way to give a name to certain values so that you don't have to keep dealing
一种为某些值命名的方法，这样您就不必继续交易

54
00:03:37,269 --> 00:03:40,069
with integers and be all over the place let's take a look at a real world
整数并遍布各处让我们看一下真实世界

55
00:03:40,269 --> 00:03:43,670
example of where you might use any number looking at our log clock that we
您可以在其中使用任何数字查看我们的日志时钟的示例

56
00:03:43,870 --> 00:03:47,030
started writing in the how to write a super clock last video if we jump over
如果我们跳过，就开始写如何编写超级时钟最后一个视频

57
00:03:47,229 --> 00:03:50,719
here to our low class you can see what we actually ended up doing was using
在我们低年级的学生那里，您可以看到我们实际上最终正在使用的是

58
00:03:50,919 --> 00:03:55,370
three different log levels and they were just integers here 0 1 2 hopefully you
三个不同的日志级别，它们只是整数0 1 2希望您

59
00:03:55,569 --> 00:03:59,210
can see it is pretty much a perfect candidate for an enum because we have
可以看到它几乎是枚举的理想候选人，因为我们有

60
00:03:59,409 --> 00:04:03,590
three values which we just use as integers to represent a certain state in
三个值，我们仅将它们用作整数来表示某个状态

61
00:04:03,789 --> 00:04:07,460
this case which log level our log will actually display so right over here
这种情况下，我们的日志实际上将显示在哪个日志级别

62
00:04:07,659 --> 00:04:11,520
inside this low class I'm going to create a level in um I'm
在这个低级的课程中，我将在um中创建一个关卡

63
00:04:11,719 --> 00:04:15,900
to create error warning and info as possible value
创建错误警告和信息作为可能的值

64
00:04:16,100 --> 00:04:19,949
so we're basically mirroring this as a matter of style I actually like to
所以我们基本上是根据我实际喜欢的风格来反映这一点

65
00:04:20,149 --> 00:04:23,819
explicitly write equals zero however that's definitely not required because
显式写等于零，但是绝对不需要，因为

66
00:04:24,019 --> 00:04:28,920
it will start at zero by default again just something I like doing to help the
默认情况下，它将再次从零开始，只是我想帮助

67
00:04:29,120 --> 00:04:33,120
readability of the code I'll get rid of these now instead of having an int as my
代码的可读性现在我将摆脱它们，而不是将int作为我的

68
00:04:33,319 --> 00:04:37,020
log level which I could still have of course I could still find this to infer
我仍然可以拥有的日志级别，当然我仍然可以找到这个推断

69
00:04:37,220 --> 00:04:41,400
and since any number is just an end this would still work however in this case I
而且由于任何数字都只是终点，但是在这种情况下，我仍然可以使用

70
00:04:41,600 --> 00:04:45,449
would be able to set any value to log level any integer value to log level and
能够将任何值设置为日志级别，将任何整数值设置为日志级别，并且

71
00:04:45,649 --> 00:04:49,770
I want to restrict it to just being there 3 so I will actually surface to be
我想将其限制为仅存在于3，所以我实际上会浮出水面

72
00:04:49,970 --> 00:04:53,460
level nodes of this restriction that I've created of it must be do these
我为此创建的此限制级别节点必须执行以下操作

73
00:04:53,660 --> 00:04:58,829
three purely syntactical service kind of compiler and for things you can get
三种纯粹的语法服务类型的编译器，您可以得到

74
00:04:59,029 --> 00:05:03,030
around it really easily it's not something that can't physically be set
围绕它真的很容易，这不是无法物理设置的东西

75
00:05:03,230 --> 00:05:06,960
of course in this case our enum is just a 4 byte integer it's just 4 bytes of
当然，在这种情况下，我们的枚举只是一个4字节的整数，它只是4字节的

76
00:05:07,160 --> 00:05:10,468
memory you can put anything you want into that memory however by setting the
内存，您可以将所需的任何内容放入该内存，但是通过设置

77
00:05:10,668 --> 00:05:14,670
type to level you're restricting the code so to speak to actually only allow
输入要限制代码的级别，这样说实际上只能

78
00:05:14,870 --> 00:05:19,410
levels I'll change my set level to take a new level now when I do these checks
进行这些检查时，我现在将设置的水平更改为新的水平

79
00:05:19,610 --> 00:05:22,829
they will still work just fine I just need to change log level error to just
它们仍然可以正常工作，我只需要将日志级别错误更改为

80
00:05:23,029 --> 00:05:27,960
be error because M log level is this enum type which is an integer
是错误的，因为M日志级别是此枚举类型，它是整数

81
00:05:28,160 --> 00:05:31,590
I can still do comparisons between them which is very very useful to things like
我仍然可以在它们之间进行比较，这对于诸如

82
00:05:31,790 --> 00:05:35,790
this I'll change this to say warning and I'll change this to say info the other
我将其更改为警告，然后将其更改为信息

83
00:05:35,990 --> 00:05:39,480
thing the changes here is the log set level instead of being logged on log
东西这里的更改是日志集级别，而不是登录日志

84
00:05:39,680 --> 00:05:43,079
level error which used to be that integer it now simply becomes the log
级别错误，以前是那个整数，现在变成了对数

85
00:05:43,279 --> 00:05:47,490
error because we have an anon value called error inside our log namespace
错误，因为我们在日志名称空间中有一个名为error的匿名值

86
00:05:47,689 --> 00:05:51,870
notice that this enum level is not a name safe in itself there is something
请注意，此枚举级别本身并不是一个安全的名称， 

87
00:05:52,069 --> 00:05:55,680
called an enum class which we will talk about in another video however just a
称为枚举类，我们将在另一个视频中讨论，但仅仅是

88
00:05:55,879 --> 00:05:59,790
regular email this level isn't really named safe so you can't use it as one
此级别的普通电子邮件并非真正安全，因此您不能将其用作

89
00:05:59,990 --> 00:06:05,340
which means it is error warning and info simply use it inside this log class and
这意味着它是错误警告，信息仅在此日志类中使用它，并且

90
00:06:05,540 --> 00:06:08,759
you can see I can set it to something like warning as well and everything fine
您可以看到我也可以将其设置为警告，并且一切都很好

91
00:06:08,959 --> 00:06:12,420
now you might have noticed error doesn't actually work and it gives us an error
现在您可能已经注意到错误实际上不起作用，这给了我们一个错误

92
00:06:12,620 --> 00:06:15,870
the reason this is happening is because we actually have a function called error
发生这种情况的原因是因为我们实际上有一个称为错误的函数

93
00:06:16,069 --> 00:06:19,310
as well so the same name I sort of deliberately to show you what
以及同样的名字，我故意向您展示

94
00:06:19,509 --> 00:06:23,120
happens if you have a conflicting name now in this case unfortunately that
不幸的是，如果您现在有一个冲突的名称，则会发生这种情况

95
00:06:23,319 --> 00:06:26,600
wouldn't work because when we try and refer to things like Eric here it
是行不通的，因为当我们尝试在此处引用诸如Eric之类的内容时， 

96
00:06:26,800 --> 00:06:29,420
doesn't know we're friends it's not so we definitely can't have something
不知道我们是朋友，所以我们绝对不能拥有某些东西

97
00:06:29,620 --> 00:06:33,500
called a ratio unfortunately if you want to have a function with the same name so
不幸的是，如果您想要一个具有相同名称的函数，则称为比率

98
00:06:33,699 --> 00:06:37,879
now we can just add a level prefix to the enum which by the way is very very
现在我们可以向枚举添加级别前缀，顺便说一下

99
00:06:38,079 --> 00:06:40,730
common practice so that we know exactly what we're referring to
常规做法，以便我们确切地知道我们指的是什么

100
00:06:40,930 --> 00:06:44,060
since this name doesn't really mean much in the scope of things so I'll tell it's
因为这个名称在范围上并没有多大意义，所以我告诉您

101
00:06:44,259 --> 00:06:50,930
equal to level info this two-level error sista level warning and this level info
等于级别信息此二级错误sista级别警告和此级别信息

102
00:06:51,129 --> 00:06:54,889
and now what I don't need this I can get rid of this and just type is equal to
现在我不需要这个了，我可以摆脱它，只是类型等于

103
00:06:55,089 --> 00:06:59,030
level error if I run my code you can see that I would get a result that I expect
级别错误，如果我运行我的代码，您会看到我会得到预期的结果

104
00:06:59,230 --> 00:07:02,480
well in the error is printing of course because we've set our level to be error
当然，错误是打印，因为我们将级别设置为错误

105
00:07:02,680 --> 00:07:06,410
so there you go and I'm just essentially this to make our lives easier and to
所以你去了，而我基本上只是为了让我们的生活更轻松， 

106
00:07:06,610 --> 00:07:10,100
make our code cleaner they are just integers behind the scenes and you can
使我们的代码更整洁，它们只是后台的整数，您可以

107
00:07:10,300 --> 00:07:14,120
use them pretty much in any way like integers there are many many uses for in
几乎以任何方式使用它们，例如整数，在in有很多用途

108
00:07:14,319 --> 00:07:17,270
ohms which we will cover in future videos we'll definitely talk about in
我们将在以后的视频中介绍的欧姆数

109
00:07:17,470 --> 00:07:21,110
our classes as well in another video but essentially whenever you have a set of
我们的课程以及其他视频，但基本上只要您有

110
00:07:21,310 --> 00:07:24,860
values which you want to represent numerically any numbers probably the way
您想用数字表示任何数字的值

111
00:07:25,060 --> 00:07:27,829
to go I'll see you guys next time good bye
下次我再见

112
00:07:28,029 --> 00:07:33,268
[Music]
[音乐]

113
00:07:40,000 --> 00:07:45,000
[Music]
 [音乐] 

