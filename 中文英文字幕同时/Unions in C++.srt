﻿1
00:00:00,000 --> 00:00:01,060
嘿，大家好，我叫肖恩，欢迎回到我的州老板。
hey what's up guys my name is Shawn and

2
00:00:01,260 --> 00:00:02,379
嘿，大家好，我叫肖恩，欢迎回到我的州老板。
welcome back to my state boss boss

3
00:00:02,580 --> 00:00:03,939
系列，所以今天我们将讨论C ++中的联合，因此联合是一个
series so today we're gonna be talking

4
00:00:04,139 --> 00:00:07,750
系列，所以今天我们将讨论C ++中的联合，因此联合是一个
all about unions in C++ so a union is a

5
00:00:07,950 --> 00:00:09,130
有点像struct类型的类类型，只是它只能占据
little bit like a class type like a

6
00:00:09,330 --> 00:00:12,160
有点像struct类型的类类型，只是它只能占据
struct type except it can only occupy

7
00:00:12,359 --> 00:00:14,349
一次只有一个成员的记忆，这意味着如果我们
the memory of like one member at a time

8
00:00:14,548 --> 00:00:17,109
一次只有一个成员的记忆，这意味着如果我们
what that means is that typically if we

9
00:00:17,309 --> 00:00:18,909
有一个结构，我们声明让我们说四个浮点数或其中的某
have a struct and we declare let's just

10
00:00:19,109 --> 00:00:20,800
有一个结构，我们声明让我们说四个浮点数或其中的某
say four floats or something in it that

11
00:00:21,000 --> 00:00:24,669
意味着我们可以有4乘4字节，总共16字节，
means that we can have four times four

12
00:00:24,868 --> 00:00:27,159
意味着我们可以有4乘4字节，总共16字节，
bytes which is a total of 16 bytes and

13
00:00:27,359 --> 00:00:29,140
因为我们有四个成员，所以它占用了多少空间
that's dropped that's how much space it

14
00:00:29,339 --> 00:00:30,550
因为我们有四个成员，所以它占用了多少空间
occupies because we have four members

15
00:00:30,750 --> 00:00:32,619
显然，随着您不断向类或结构中添加更多成员，
and obviously as you keep adding more

16
00:00:32,820 --> 00:00:34,570
显然，随着您不断向类或结构中添加更多成员，
members to a class or a struct the size

17
00:00:34,770 --> 00:00:36,788
不断发展的工会只能有一个成员，所以如果我宣布
keeps growing a union can only have one

18
00:00:36,988 --> 00:00:40,178
不断发展的工会只能有一个成员，所以如果我宣布
member so if I were if I was to declare

19
00:00:40,378 --> 00:00:43,329
像abcd这样的四个float联合的大小仍然是四个字节，当
four floats like a b c d the size of the

20
00:00:43,530 --> 00:00:45,250
像abcd这样的四个float联合的大小仍然是四个字节，当
union would still be four bytes and when

21
00:00:45,450 --> 00:00:47,409
如果尝试更改a或b或c或d的值，我会尝试解决一个类似问题
i try to address either a like if i try

22
00:00:47,609 --> 00:00:49,599
如果尝试更改a或b或c或d的值，我会尝试解决一个类似问题
to change the value of a or b or c or d

23
00:00:49,799 --> 00:00:51,608
它实际上是相同的内存，所以如果我更改a并将其设置为5的值
it would literally be the same memory so

24
00:00:51,808 --> 00:00:54,009
它实际上是相同的内存，所以如果我更改a并将其设置为5的值
if i change a and set it to 5 the value

25
00:00:54,210 --> 00:00:56,198
的d也很好，这就是工会的工作方式，您可以准确地使用它们
of d would also be fine that's how

26
00:00:56,399 --> 00:00:58,689
的d也很好，这就是工会的工作方式，您可以准确地使用它们
unions work and you can use them exactly

27
00:00:58,890 --> 00:01:00,820
就像您可以使用结构或类，就像可以向其中添加静态函数一样
like you can use structs or classes like

28
00:01:01,020 --> 00:01:04,179
就像您可以使用结构或类，就像可以向其中添加静态函数一样
you can add like static functions to

29
00:01:04,379 --> 00:01:05,590
它们，以及正常的功能，以及它们的人和方法，以及所有这些
them and just normal functions and Men

30
00:01:05,790 --> 00:01:06,879
它们，以及正常的功能，以及它们的人和方法，以及所有这些
and methods to them and all of that

31
00:01:07,079 --> 00:01:09,189
东西，但是你不喜欢有虚方法，还有其他一些
stuff however you can't like have

32
00:01:09,390 --> 00:01:11,019
东西，但是你不喜欢有虚方法，还有其他一些
virtual methods and there are some other

33
00:01:11,219 --> 00:01:12,939
限制，但通常人们使用工会非常紧密
restrictions but usually what people use

34
00:01:13,140 --> 00:01:15,640
限制，但通常人们使用工会非常紧密
unions for is very kind of closely

35
00:01:15,840 --> 00:01:17,738
链接到类型规划，这是我们在上一个视频的最后一位所做的，所以如果
linked to type planning which is what we

36
00:01:17,938 --> 00:01:19,808
链接到类型规划，这是我们在上一个视频的最后一位所做的，所以如果
did in last bit in the last video so if

37
00:01:20,009 --> 00:01:20,709
您还没有确定要立即检查
you haven't check that out definitely

38
00:01:20,909 --> 00:01:22,359
您还没有确定要立即检查
check that out right now

39
00:01:22,560 --> 00:01:24,668
但对于您基本上希望能够给两个
but it's really useful for when you want

40
00:01:24,868 --> 00:01:26,738
但对于您基本上希望能够给两个
to basically be able to either give two

41
00:01:26,938 --> 00:01:28,599
同一变量的名称不同，例如，如果我喜欢
different names to the same variable

42
00:01:28,799 --> 00:01:30,878
同一变量的名称不同，例如，如果我喜欢
like for example if I had like a

43
00:01:31,078 --> 00:01:33,668
数学矢量类XYZ avec 3我也可能想像对待它一样
mathematical vector class X Y Z avec 3 I

44
00:01:33,868 --> 00:01:35,859
数学矢量类XYZ avec 3我也可能想像对待它一样
might also want to address it as if it

45
00:01:36,060 --> 00:01:40,329
是一种彩色RGB，正确的XYZ RGB，我如何获得它，以便可以同时使用它
was a color RGB right XYZ RGB how can I

46
00:01:40,530 --> 00:01:42,878
是一种彩色RGB，正确的XYZ RGB，我如何获得它，以便可以同时使用它
get it so that I can use it both ways

47
00:01:43,078 --> 00:01:44,829
基本上，您知道X将与aa对齐，而J将与Y以及无聊对齐。
and basically you know X will align with

48
00:01:45,030 --> 00:01:47,319
基本上，您知道X将与aa对齐，而J将与Y以及无聊对齐。
aa and J will align with dull with Y and

49
00:01:47,519 --> 00:01:49,659
所有这些东西都可以通过使用工会来实现
all that kind of stuff that's that's all

50
00:01:49,859 --> 00:01:52,840
所有这些东西都可以通过使用工会来实现
achievable through the use of a union so

51
00:01:53,040 --> 00:01:54,209
我们通常在这里看一些例子，我会说使用工会
we'll take a look at some examples here

52
00:01:54,409 --> 00:01:56,799
我们通常在这里看一些例子，我会说使用工会
usually I would say that unions are used

53
00:01:57,000 --> 00:01:58,418
匿名表示您给他们起了个名字，您肯定不会喜欢
anonymously which means your give them a

54
00:01:58,618 --> 00:01:59,859
匿名表示您给他们起了个名字，您肯定不会喜欢
name and you definitely don't have like

55
00:02:00,060 --> 00:02:01,649
方法或运动中的任何内容如果不是匿名的，都不能具有
methods or anything in the movement

56
00:02:01,849 --> 00:02:04,349
方法或运动中的任何内容如果不是匿名的，都不能具有
can't have that if they're not anonymous

57
00:02:04,549 --> 00:02:05,969
但是整体工会真的非常有用，我们来看一下
but overall unions are really really

58
00:02:06,170 --> 00:02:07,978
但是整体工会真的非常有用，我们来看一下
useful and we'll just take a look a few

59
00:02:08,179 --> 00:02:09,570
例子，看看我们如何使用它们，所以我已经写了一些代码
examples to see how we can use them so

60
00:02:09,770 --> 00:02:10,890
例子，看看我们如何使用它们，所以我已经写了一些代码
I've got some code already written out

61
00:02:11,090 --> 00:02:12,090
在这里，但是如果我们只看一个基本的
here but we'll get to that in a minute

62
00:02:12,289 --> 00:02:13,618
在这里，但是如果我们只看一个基本的
if we just take a look at a basic

63
00:02:13,818 --> 00:02:16,500
工会的例子让我们说我要写一个
example of a union let's just say let's

64
00:02:16,699 --> 00:02:17,580
工会的例子让我们说我要写一个
just say that I'm gonna write an

65
00:02:17,780 --> 00:02:19,469
匿名联盟在这里，我们将其写为与我们处理
anonymous Union here we write it the

66
00:02:19,669 --> 00:02:20,520
匿名联盟在这里，我们将其写为与我们处理
same way as we kind of deal with a

67
00:02:20,719 --> 00:02:22,410
结构像一个匿名结构，但它被称为联合，然后我可以给它
struct like an anonymous struct but it's

68
00:02:22,610 --> 00:02:25,020
结构像一个匿名结构，但它被称为联合，然后我可以给它
called a union and then I can give it

69
00:02:25,219 --> 00:02:27,689
我们可以说两个不同的变量我先给它一个float a然后再给它int B
two different variables let's just say

70
00:02:27,889 --> 00:02:30,509
我们可以说两个不同的变量我先给它一个float a然后再给它int B
I'll give it a float a and then int B so

71
00:02:30,709 --> 00:02:33,090
这意味着如果这是一个结构，我们只有两个不同
what this means is that if this was a

72
00:02:33,289 --> 00:02:34,230
这意味着如果这是一个结构，我们只有两个不同
struct we just have two different

73
00:02:34,430 --> 00:02:35,850
成员，但是我们这里有两种不同的解决方法
members however what we have here

74
00:02:36,050 --> 00:02:39,180
成员，但是我们这里有两种不同的解决方法
instead is two different ways to address

75
00:02:39,379 --> 00:02:41,580
相同的内存，这意味着如果我要说的话，将其像结构一样放入
the same memory which means that if I

76
00:02:41,780 --> 00:02:43,680
相同的内存，这意味着如果我要说的话，将其像结构一样放入
was to say put this into like a struct

77
00:02:43,879 --> 00:02:46,740
叫我不知道联盟，这可能会使事情变得混乱而希望做
called I don't know Union this might

78
00:02:46,939 --> 00:02:49,950
叫我不知道联盟，这可能会使事情变得混乱而希望做
confuse things hopefully not and do

79
00:02:50,150 --> 00:02:52,289
如果我要创建这个Union类的一个实例，然后
something like this if I was to make an

80
00:02:52,489 --> 00:02:54,240
如果我要创建这个Union类的一个实例，然后
instance of this Union class and then

81
00:02:54,439 --> 00:02:57,569
说你不是22.0应用或类似的东西，如果我继续打印
said you don't a 22.0 app or something

82
00:02:57,769 --> 00:02:59,880
说你不是22.0应用或类似的东西，如果我继续打印
like that if I go ahead and print the

83
00:03:00,080 --> 00:03:03,390
你的价值，你不会像这样
value of youdo a and you don't be like

84
00:03:03,590 --> 00:03:05,120
你的价值，你不会像这样
this

85
00:03:05,319 --> 00:03:07,680
五，然后您会看到我们得到的是两个，然后是一个零七等等
five then you can see that what we get

86
00:03:07,879 --> 00:03:10,020
五，然后您会看到我们得到的是两个，然后是一个零七等等
here is two and then one zero seven blah

87
00:03:10,219 --> 00:03:12,689
等等，因为该值是两个浮点数的生物表示，如果那
blah because this value is the bio

88
00:03:12,889 --> 00:03:15,660
等等，因为该值是两个浮点数的生物表示，如果那
representation of two as a float if that

89
00:03:15,860 --> 00:03:17,909
很有道理，就好像我们拿走了组成浮点数的记忆，然后
makes sense so it's as if we took that

90
00:03:18,109 --> 00:03:20,700
很有道理，就好像我们拿走了组成浮点数的记忆，然后
memory that made up the float and then

91
00:03:20,900 --> 00:03:23,219
只是将其解释为int，所以我们基本上输入了punted，因此
just interpreted it as it was an int so

92
00:03:23,419 --> 00:03:25,050
只是将其解释为int，所以我们基本上输入了punted，因此
we've typed punted basically so that's

93
00:03:25,250 --> 00:03:26,640
您可以使用工会的原因是它们通常仅用于
what you can use unions for that's what

94
00:03:26,840 --> 00:03:28,020
您可以使用工会的原因是它们通常仅用于
they are commonly used towards just a

95
00:03:28,219 --> 00:03:29,759
如果我想尝试将其转换为两个不同变量的方式
way of type honey if I wants to try and

96
00:03:29,959 --> 00:03:31,140
如果我想尝试将其转换为两个不同变量的方式
convert it into two different variable

97
00:03:31,340 --> 00:03:32,370
输入类似的内容，我可以轻松地做到这一点，所以让我们
type something or something like that I

98
00:03:32,569 --> 00:03:34,050
输入类似的内容，我可以轻松地做到这一点，所以让我们
could easily do that so let's take a

99
00:03:34,250 --> 00:03:35,550
更有用的例子我在这里得到的基本上是一个向量
little bit more useful example what I've

100
00:03:35,750 --> 00:03:39,030
更有用的例子我在这里得到的基本上是一个向量
got up here is essentially a vector to

101
00:03:39,229 --> 00:03:41,670
现在有效，我还有一个称为打印向量2的函数
an effective for now I've also got a

102
00:03:41,870 --> 00:03:43,200
现在有效，我还有一个称为打印向量2的函数
function called print vector two which

103
00:03:43,400 --> 00:03:45,689
接受一个向量二并在我现在无法打印两个向量时打印它
takes in a vector two and prints it now

104
00:03:45,889 --> 00:03:48,000
接受一个向量二并在我现在无法打印两个向量时打印它
at the moment I can't print two vector

105
00:03:48,199 --> 00:03:49,500
四，但是您可以看到向量四实际上只是两个向量二或
four however you can see that a vector

106
00:03:49,699 --> 00:03:52,680
四，但是您可以看到向量四实际上只是两个向量二或
four is really just two vector twos or

107
00:03:52,879 --> 00:03:54,480
至少那可能是一种看待它的方式，我的意思是，它有四个浮子
at least that might be one way to look

108
00:03:54,680 --> 00:03:56,159
至少那可能是一种看待它的方式，我的意思是，它有四个浮子
at it I mean it's got four floats this

109
00:03:56,359 --> 00:03:58,080
有两个浮点数，为什么我们不能仅仅将向量4看作两个向量
has two floats why can't we just see

110
00:03:58,280 --> 00:04:01,830
有两个浮点数，为什么我们不能仅仅将向量4看作两个向量
this vector 4 as two vector twos well

111
00:04:02,030 --> 00:04:03,539
如果我们想再次从这件事中得到向量2怎么办
what we could do if we wanted to again

112
00:04:03,739 --> 00:04:05,789
如果我们想再次从这件事中得到向量2怎么办
get the vector 2 out out of this thing

113
00:04:05,989 --> 00:04:08,039
是我们可以做些像您知道的事情吗？我不知道我们会先称呼它
is we could do something like you know

114
00:04:08,239 --> 00:04:10,379
是我们可以做些像您知道的事情吗？我不知道我们会先称呼它
get I don't know we'll call this first

115
00:04:10,579 --> 00:04:12,420
部分a然后是第二部分B因此向量
part a and then the second part B so the

116
00:04:12,620 --> 00:04:12,719
部分a然后是第二部分B因此向量
vector

117
00:04:12,919 --> 00:04:15,090
将会是X＆Y，而他们将是7w，我们可以在这里做类似的事情
to a would be X&Y and they would be 7w

118
00:04:15,289 --> 00:04:17,009
将会是X＆Y，而他们将是7w，我们可以在这里做类似的事情
we could do something like this where we

119
00:04:17,209 --> 00:04:19,379
构建整个效果2或类似的东西，然后给它一些成员
construct a whole Effect 2 or something

120
00:04:19,579 --> 00:04:20,699
构建整个效果2或类似的东西，然后给它一些成员
like that and then give it some members

121
00:04:20,899 --> 00:04:23,009
并返回整个东西，我们可以做类似的事情，但这是
and return that whole thing and we could

122
00:04:23,209 --> 00:04:24,840
并返回整个东西，我们可以做类似的事情，但这是
do something like that however that's

123
00:04:25,040 --> 00:04:26,189
将要创建一个全新的对象，而我们真的不想这样做，我们可以
going to create a whole new object and

124
00:04:26,389 --> 00:04:28,110
将要创建一个全新的对象，而我们真的不想这样做，我们可以
we don't really want to do that we could

125
00:04:28,310 --> 00:04:30,210
不必复制任何内容，我们也会继续输入
also type on our way into this by not

126
00:04:30,410 --> 00:04:31,860
不必复制任何内容，我们也会继续输入
having to copy anything we still will

127
00:04:32,060 --> 00:04:33,389
需要复制一些东西，因为创建了一个全新的类型，但是我们
need to copy something because of the

128
00:04:33,589 --> 00:04:34,829
需要复制一些东西，因为创建了一个全新的类型，但是我们
creating a whole new type but what we

129
00:04:35,029 --> 00:04:36,600
可以做的只是返回对向量的引用，
could do instead is just return a

130
00:04:36,800 --> 00:04:38,819
可以做的只是返回对向量的引用，
reference to a vector which is really

131
00:04:39,019 --> 00:04:41,968
只是X版本的一种类型，所以我们要做的是将
just a type on version of X so the way

132
00:04:42,168 --> 00:04:43,949
只是X版本的一种类型，所以我们要做的是将
that we would do that is we would cast

133
00:04:44,149 --> 00:04:47,810
X的内存地址像这样的向量2，然后取消引用它，所以您
the memory address of X to a vector 2

134
00:04:48,009 --> 00:04:50,968
X的内存地址像这样的向量2，然后取消引用它，所以您
like that and then dereference it so you

135
00:04:51,168 --> 00:04:52,740
正确地做到这一点，这可能是这样做的一种方式，但却是另一种方式
get this right and that could be one way

136
00:04:52,939 --> 00:04:54,540
正确地做到这一点，这可能是这样做的一种方式，但却是另一种方式
of doing that but another way of doing

137
00:04:54,740 --> 00:04:56,759
这是通过使用Union来进行的，看起来可能会好很多。
that is by using a Union and that will

138
00:04:56,959 --> 00:04:58,889
这是通过使用Union来进行的，看起来可能会好很多。
probably look a lot better as well so to

139
00:04:59,089 --> 00:05:00,600
这样做我只是要摆脱这个功能，然后将其包装在
do that I'm just going to get rid of

140
00:05:00,800 --> 00:05:03,060
这样做我只是要摆脱这个功能，然后将其包装在
this function and then wrap this in a

141
00:05:03,259 --> 00:05:05,939
工会，如我们所知，我会放弃这个名称，因为它是
Union so a union as we know and I'll

142
00:05:06,139 --> 00:05:07,500
工会，如我们所知，我会放弃这个名称，因为它是
just get rid of the name because it's

143
00:05:07,699 --> 00:05:09,750
将成为一个匿名联盟，您所知道的联盟只能有一个成员，因此
going to be an anonymous Union a Union

144
00:05:09,949 --> 00:05:12,028
将成为一个匿名联盟，您所知道的联盟只能有一个成员，因此
as you know can only have one member so

145
00:05:12,228 --> 00:05:14,009
我们不能只像这样把浮点数X留在W边，因为那将意味着X
we can't just leave the float X by the W

146
00:05:14,209 --> 00:05:16,199
我们不能只像这样把浮点数X留在W边，因为那将意味着X
like this because that would mean that X

147
00:05:16,399 --> 00:05:18,569
YZ和W在空间上都将占据相同的位置，我们需要做的是
Y Z and W would both occupy that same

148
00:05:18,769 --> 00:05:20,759
YZ和W在空间上都将占据相同的位置，我们需要做的是
for by space what we need to do is

149
00:05:20,959 --> 00:05:22,528
实际上将其包装在这样的匿名结构中，所以我只说结构
actually wrap this in an anonymous

150
00:05:22,728 --> 00:05:25,199
实际上将其包装在这样的匿名结构中，所以我只说结构
struct like this so I'll just say struct

151
00:05:25,399 --> 00:05:28,319
然后像这样把那个浮点XY W放在那里，所以现在这个结构是
and then put that float XY the W in

152
00:05:28,519 --> 00:05:30,930
然后像这样把那个浮点XY W放在那里，所以现在这个结构是
there like that so now this struct is is

153
00:05:31,129 --> 00:05:33,028
联盟希望的一种成员，恰好像
the one kind of member that the Union

154
00:05:33,228 --> 00:05:34,920
联盟希望的一种成员，恰好像
has wishes which happens to be like a

155
00:05:35,120 --> 00:05:36,930
现在按结构划分16个，如果我继续，就什么都没有改变
sixteen by structure now at this point

156
00:05:37,129 --> 00:05:38,879
现在按结构划分16个，如果我继续，就什么都没有改变
nothing's changed right if I go ahead

157
00:05:39,079 --> 00:05:40,490
然后让我们实际创建一个矢量2或更确切地说，我将创建一个矢量4
and let's actually create a vector two

158
00:05:40,689 --> 00:05:43,560
然后让我们实际创建一个矢量2或更确切地说，我将创建一个矢量4
or rather I'll create a vector four I'll

159
00:05:43,759 --> 00:05:45,689
只是称它为向量，我将其设置为等于一二三四，我仍然可以
just call it vector and I'll set it

160
00:05:45,889 --> 00:05:50,160
只是称它为向量，我将其设置为等于一二三四，我仍然可以
equal to one two three four I can still

161
00:05:50,360 --> 00:05:52,800
访问向量点X和其他类似正常的权限，我可以将其设置为两个
access vector dot X and whatever like

162
00:05:53,000 --> 00:05:54,480
访问向量点X和其他类似正常的权限，我可以将其设置为两个
normal right I can set this to like two

163
00:05:54,680 --> 00:05:56,430
或我仍然可以照常访问的任何东西，因为就像我没有给这个一样
or whatever I can still access it as

164
00:05:56,629 --> 00:05:58,860
或我仍然可以照常访问的任何东西，因为就像我没有给这个一样
usual because like I haven't given this

165
00:05:59,060 --> 00:06:00,210
一个名字或任何想法，如果我开始这样做，它将使所有事物成长
a name or any thought that if I start

166
00:06:00,410 --> 00:06:01,889
一个名字或任何想法，如果我开始这样做，它将使所有事物成长
doing that it's gonna grow everything

167
00:06:02,089 --> 00:06:04,259
但是，如果这是一个匿名的事情，那实际上只是一种结构化方式
but if it's an anonymous thing it's just

168
00:06:04,459 --> 00:06:05,670
但是，如果这是一个匿名的事情，那实际上只是一种结构化方式
really just a way to kind of structure

169
00:06:05,870 --> 00:06:07,259
数据并没有真正添加任何东西，但再次带来的好处是
the data it's not really adding anything

170
00:06:07,459 --> 00:06:09,180
数据并没有真正添加任何东西，但再次带来的好处是
but again the benefit here is that it's

171
00:06:09,379 --> 00:06:10,889
将所有这些转变成一个联盟所期望的单一成员，
converting all this into a single member

172
00:06:11,089 --> 00:06:12,329
将所有这些转变成一个联盟所期望的单一成员，
which is what the Union expects and

173
00:06:12,529 --> 00:06:15,000
最后，我将在此处创建另一个结构，因此很明显，这是添加第二个成员
finally I'll make another struct here so

174
00:06:15,199 --> 00:06:16,949
最后，我将在此处创建另一个结构，因此很明显，这是添加第二个成员
obviously this is adding a second member

175
00:06:17,149 --> 00:06:18,540
到联盟，这意味着它将占据与第一个相同的空间
to the Union which means that it's going

176
00:06:18,740 --> 00:06:21,689
到联盟，这意味着它将占据与第一个相同的空间
to occupy the same space as this first

177
00:06:21,889 --> 00:06:24,329
成员，这将由两名博士。二元
member and that's going to consist of

178
00:06:24,529 --> 00:06:26,030
成员，这将由两名博士。二元
two dr. twos

179
00:06:26,230 --> 00:06:29,840
就像那样好，所以现在我有几种方法可以访问内部数据
just like that okay so now I have a few

180
00:06:30,040 --> 00:06:31,400
就像那样好，所以现在我有几种方法可以访问内部数据
ways of accessing this data inside back

181
00:06:31,600 --> 00:06:33,620
最多可以有四个，我可以使用XY ZW，也可以使用a和B，a将是
to four I can in it I can either use X Y

182
00:06:33,819 --> 00:06:36,800
最多可以有四个，我可以使用XY ZW，也可以使用a和B，a将是
Z W or I can use a and B and a will be

183
00:06:37,000 --> 00:06:39,170
与x和Y和B相同的内存将是W中设置的相同内存，所以让我们检查一下
the same memory as x and Y and B will be

184
00:06:39,370 --> 00:06:41,060
与x和Y和B相同的内存将是W中设置的相同内存，所以让我们检查一下
the same memory set in W so let's check

185
00:06:41,259 --> 00:06:43,579
知道如何工作，所以我在这里写下了一二三四
out how that can work so I've got one

186
00:06:43,779 --> 00:06:45,290
知道如何工作，所以我在这里写下了一二三四
two three four written down here what

187
00:06:45,490 --> 00:06:47,090
我要做的是将矢量打印到我们的打印矢量点上，只是为了
I'll do is I'll print vector to our

188
00:06:47,290 --> 00:06:49,850
我要做的是将矢量打印到我们的打印矢量点上，只是为了
print vector dot a okay just to get that

189
00:06:50,050 --> 00:06:52,040
结果出来，然后我将设置矢量点，我们想知道，让我们说一下矢量
result out and then I'll set vector dot

190
00:06:52,240 --> 00:06:53,540
结果出来，然后我将设置矢量点，我们想知道，让我们说一下矢量
we'll want to know let's just say vector

191
00:06:53,740 --> 00:06:54,350
狗的Zed，我将其设置为500，然后
dog's Zed

192
00:06:54,550 --> 00:06:56,840
狗的Zed，我将其设置为500，然后
I'll set equal to 500 and then I'll

193
00:06:57,040 --> 00:06:58,400
在两点以后打印它，但是这次我将上面的事件打印出来可能是一个因素
print it after two but this time I'll

194
00:06:58,600 --> 00:07:00,259
在两点以后打印它，但是这次我将上面的事件打印出来可能是一个因素
print event above be one factor probably

195
00:07:00,459 --> 00:07:03,020
可能为了再次打印两者都有用
might be useful to print both of them

196
00:07:03,220 --> 00:07:06,800
可能为了再次打印两者都有用
both times just for fun again I also

197
00:07:07,000 --> 00:07:08,660
我猜有一点分隔线，所以当我们实际上
have a little bit of a divider I guess

198
00:07:08,860 --> 00:07:10,210
我猜有一点分隔线，所以当我们实际上
so that is clear when we actually

199
00:07:10,410 --> 00:07:13,220
修改了Domino内部的内容，我将单击“完成”，让我们看一下
modified the Domino inside I'll click

200
00:07:13,420 --> 00:07:15,290
修改了Domino内部的内容，我将单击“完成”，让我们看一下
done so let's take a look at this okay

201
00:07:15,490 --> 00:07:16,730
因此，请检查一下是否有我们第一次期望的一二三四
so check that out we have one two three

202
00:07:16,930 --> 00:07:18,259
因此，请检查一下是否有我们第一次期望的一二三四
four as we would expect the first time

203
00:07:18,459 --> 00:07:20,600
然后是504，所以我什至都没碰过
and then one to 504 so I haven't even

204
00:07:20,800 --> 00:07:23,000
然后是504，所以我什至都没碰过
touched the actual be part of that

205
00:07:23,199 --> 00:07:25,670
向量显然我没有说B点X到500我说向量Z点到500
vector obviously I haven't said B dot X

206
00:07:25,870 --> 00:07:28,879
向量显然我没有说B点X到500我说向量Z点到500
to 500 I've said vector dot Z to 500

207
00:07:29,079 --> 00:07:31,460
这实际上是此变量，但此变量对应于B点
which is actually this variable here but

208
00:07:31,660 --> 00:07:34,040
这实际上是此变量，但此变量对应于B点
this variable here corresponds to B dot

209
00:07:34,240 --> 00:07:36,319
X，因为它被占用是因为它占用了相同的内存，所以
X because it's occupy because it's

210
00:07:36,519 --> 00:07:38,270
X，因为它被占用是因为它占用了相同的内存，所以
occupying the same memory okay so

211
00:07:38,470 --> 00:07:39,410
希望这是一个很好的例子，说明工会如何再次发挥作用，他们确实
hopefully that's a pretty good example

212
00:07:39,610 --> 00:07:41,810
希望这是一个很好的例子，说明工会如何再次发挥作用，他们确实
of how unions work again they're really

213
00:07:42,009 --> 00:07:43,220
当您有多个或当您想要做这样的事情时很有用
useful when you want to do stuff like

214
00:07:43,420 --> 00:07:45,590
当您有多个或当您想要做这样的事情时很有用
this when you have multiple or when you

215
00:07:45,790 --> 00:07:47,480
需要多种方式来处理相同的数据，因为它可能在
want multiple ways to address that same

216
00:07:47,680 --> 00:07:49,520
需要多种方式来处理相同的数据，因为它可能在
data because it might be useful in a

217
00:07:49,720 --> 00:07:53,000
您可以再次使用类型计划或诸如此类的多种方法
number of ways again you could use type

218
00:07:53,199 --> 00:07:54,439
您可以再次使用类型计划或诸如此类的多种方法
planning or something like this usually

219
00:07:54,639 --> 00:07:57,319
工会更具可读性，因此可能会出现一些问题
unions are much more readable there can

220
00:07:57,519 --> 00:07:59,389
工会更具可读性，因此可能会出现一些问题
be some problems that come out of this

221
00:07:59,589 --> 00:08:00,680
我们可能会在另一个视频中谈到，但我不喜欢谈论
which we might talk about in another

222
00:08:00,879 --> 00:08:02,720
我们可能会在另一个视频中谈到，但我不喜欢谈论
video but I don't like talking about

223
00:08:02,920 --> 00:08:04,670
之类的东西，因为这是我非常喜欢的一些东西
that kind of stuff because it's some of

224
00:08:04,870 --> 00:08:06,770
之类的东西，因为这是我非常喜欢的一些东西
those I just like very unlikely to

225
00:08:06,970 --> 00:08:08,990
发生，您可能会很好，但无论如何希望这会给您
happen and you'll probably be fine but

226
00:08:09,189 --> 00:08:10,220
发生，您可能会很好，但无论如何希望这会给您
anyway hopefully that will give you the

227
00:08:10,420 --> 00:08:11,449
信心十足，我希望你们喜欢这个视频，如果您知道的话
confidence you need I hope you guys

228
00:08:11,649 --> 00:08:12,710
信心十足，我希望你们喜欢这个视频，如果您知道的话
enjoy this video if you did you know

229
00:08:12,910 --> 00:08:14,949
这是一个类似的按钮，让我知道patreon.com迫使我们分享我们最好的
it's a like button let me know

230
00:08:15,149 --> 00:08:17,480
这是一个类似的按钮，让我知道patreon.com迫使我们分享我们最好的
patreon.com force us to share our best

231
00:08:17,680 --> 00:08:18,949
支持系列节目的方式衷心感谢所有喜爱赞助的人
way to support the series huge thank you

232
00:08:19,149 --> 00:08:20,720
支持系列节目的方式衷心感谢所有喜爱赞助的人
as always to all the lovely patrons who

233
00:08:20,920 --> 00:08:23,120
使这成为可能，让您留下任何关于工会的评论，为什么会这样
made this possible leave any comments

234
00:08:23,319 --> 00:08:26,030
使这成为可能，让您留下任何关于工会的评论，为什么会这样
you have below about like unions why you

235
00:08:26,230 --> 00:08:27,720
使用它们为什么要使用它们，或者为什么下注为什么要使用它们
use them why you want

236
00:08:27,920 --> 00:08:29,850
使用它们为什么要使用它们，或者为什么下注为什么要使用它们
use them or why bet why they've been

237
00:08:30,050 --> 00:08:31,650
对您有用我会在下一个视频中再见
useful to you I'll see you guys in the

238
00:08:31,850 --> 00:08:33,620
对您有用我会在下一个视频中再见
next video goodbye

239
00:08:33,820 --> 00:08:38,820
[音乐]
[Music]

