﻿1
00:00:00,030 --> 00:00:04,750
hey what's up guys my name is a Cherno and welcome back to my c++ series so
嘿，大家好，我叫Cherno，欢迎回到我的c ++系列，所以

2
00:00:04,950 --> 00:00:09,399
today let's get our tools set up so in order for us to write programs in C++ we
今天让我们设置工具，以便让我们用C ++编写程序

3
00:00:09,599 --> 00:00:12,939
need to have a computer and then a set of tools that will let us build Sybil's
需要拥有一台计算机，然后需要一套工具，这将使我们能够构建Sybil的

4
00:00:13,138 --> 00:00:15,939
klas programs these tools of course are going to depend on which operating
 klas程序这些工具当然取决于哪个操作

5
00:00:16,138 --> 00:00:19,539
system you're using so this video is going to cover Windows but if you're
您正在使用的系统，因此该视频将覆盖Windows，但如果您使用的是

6
00:00:19,739 --> 00:00:22,810
using Mac or Linux then just click on the screen and you'll be taken to those
使用Mac或Linux，然后单击屏幕，您将被带到那些

7
00:00:23,010 --> 00:00:27,100
videos because I made them as well alright so Windows excellent choice
视频，因为我制作得很好，因此Windows是绝佳的选择

8
00:00:27,300 --> 00:00:30,760
because Windows is the most widely used operating system in the games industry
因为Windows是游戏行业中使用最广泛的操作系统

9
00:00:30,960 --> 00:00:35,079
now to write C++ code all you really need is just some kind of text editor
现在编写C ++代码，您真正需要的只是某种文本编辑器

10
00:00:35,280 --> 00:00:39,399
like notepad for example will do fine however once you've written that code in
例如记事本等就可以了，但是一旦您在

11
00:00:39,600 --> 00:00:44,259
C++ as a text file we need to take that and pass it through a compiler to
 C ++作为文本文件，我们需要采用它并将其通过编译器传递给

12
00:00:44,460 --> 00:00:48,099
generate some kind of executable binary so that we can run our program so what
生成某种可执行的二进制文件，以便我们可以运行程序，这样

13
00:00:48,299 --> 00:00:52,719
we need at the very minimum is a compiler that will take that text that
我们至少需要一个编译器来接受该文本， 

14
00:00:52,920 --> 00:00:56,079
we've written and convert it into a program however we can do a lot better
我们已经编写并将其转换为程序，但是我们可以做得更好

15
00:00:56,280 --> 00:00:59,108
than that writing surface plus isn't the easiest task and if you're using
而不是最简单的任务，如果您正在使用

16
00:00:59,308 --> 00:01:03,518
something like notepad it's really just gonna not help you at all so what you
记事本之类的东西真的根本帮不了你，所以你

17
00:01:03,719 --> 00:01:06,939
can do instead is actually get a development environment something called
可以做的实际上是获得一个称为

18
00:01:07,140 --> 00:01:10,090
an integrated development environment this is basically a set of tools that
一个集成的开发环境，这基本上是一组工具， 

19
00:01:10,290 --> 00:01:14,200
helps you write and debug your code in our case since we're on Windows we're
在我们的情况下，可以帮助您编写和调试代码，因为我们使用的是Windows 

20
00:01:14,400 --> 00:01:17,500
gonna be using something called Microsoft Visual Studio which is pretty
将会使用一种叫做Microsoft Visual Studio的东西

21
00:01:17,700 --> 00:01:21,189
much the best idea out there now don't get me wrong it's far from perfect but
现在最好的主意是不要误会我，这远非完美，但

22
00:01:21,390 --> 00:01:25,119
it is the best thing that we've got I've used a lot of different IDs and it's by
最好的是，我使用了许多不同的ID， 

23
00:01:25,319 --> 00:01:28,269
far my favorite Visual Studio has plugins that will help you target pretty
到目前为止，我最喜欢的Visual Studio具有可帮助您轻松定位的插件

24
00:01:28,469 --> 00:01:33,308
much any platform like PC mobile consoles everything which is why it's
诸如PC移动游戏机之类的任何平台都可以掌控一切，这就是为什么

25
00:01:33,509 --> 00:01:36,909
the most popular idea in the games industry so without further ado let's go
是游戏行业最流行的想法，因此事不宜迟，我们开始吧

26
00:01:37,109 --> 00:01:40,719
ahead and install that all right so here we have Windows 10 doesn't really matter
继续并正确安装，所以在这里我们使用Windows 10并不重要

27
00:01:40,920 --> 00:01:43,359
what version of Windows you're using we're gonna basically head on over to
您正在使用什么版本的Windows，我们基本上将转向

28
00:01:43,560 --> 00:01:46,778
Visual Studio comm link will be in the description if you don't want to type
如果您不想输入，Visual Studio comm链接将在描述中

29
00:01:46,978 --> 00:01:50,168
that out we're gonna click on download Visual Studio and we're gonna get
那我们要点击下载Visual Studio，我们会得到

30
00:01:50,368 --> 00:01:54,698
community 2017 now Visual Studio community is absolutely free and once
社区2017现在，Visual Studio社区完全免费，而且一次

31
00:01:54,899 --> 00:01:57,579
it's downloaded we're gonna want to install that and installing Visual
它已经下载了，我们将要安装它并安装Visual 

32
00:01:57,780 --> 00:02:02,198
Studio will take a significant amount of time so you might want to come back now
 Studio将花费大量时间，因此您可能现在想回来

33
00:02:02,399 --> 00:02:06,668
this is Visual Studio 2017 which is quite a bit better than 2015 it's a lot
这是Visual Studio 2017，比2015好很多

34
00:02:06,868 --> 00:02:10,210
faster and it has some pretty nice new features and it's also going to be the
速度更快，它具有一些非常不错的新功能，并且还将成为

35
00:02:10,409 --> 00:02:12,830
idea that we use for the entirety of this series
我们将在整个系列中使用的想法

36
00:02:13,030 --> 00:02:15,560
now while this is happening I want to quickly mention that there is actually a
现在，在发生这种情况时，我想快速提及一下，实际上

37
00:02:15,759 --> 00:02:20,390
plug-in called vigil assist which I use just all the time I haven't used Visual
我从未使用过Visual的插件，称为VigilAssist插件

38
00:02:20,590 --> 00:02:24,350
Studio without visual assist for probably the last two years it's amazing
在过去的两年中，没有视觉辅助的工作室令人赞叹

39
00:02:24,550 --> 00:02:28,640
now it does cost money it's like I think it's $99 yeah
现在它确实要花钱，就像我认为是99美元

40
00:02:28,840 --> 00:02:32,180
99 dollars for a personal license but if you do write a lot of code in Visual
个人许可证价格为99美元，但如果您使用Visual编写了大量代码， 

41
00:02:32,379 --> 00:02:35,810
Studio I highly recommend it it's amazing it basically just gives you a
 Studio我强烈推荐它，它给您带来了一个惊喜

42
00:02:36,009 --> 00:02:38,840
lot of features the Visual Studio is missing and I mean they're not paying me
 Visual Studio缺少许多功能，我是说他们没有付钱给我

43
00:02:39,039 --> 00:02:42,890
to say that I genuinely love visual assist so yeah alright so visual studio
说我真的很喜欢视觉辅助，所以是的，所以视觉工作室

44
00:02:43,090 --> 00:02:45,920
taking this time as usual now in terms of what components we want Visual Studio
现在，就我们需要Visual Studio的组件而言，现在像往常一样

45
00:02:46,120 --> 00:02:52,480
2017 has this brand new install that has like a lot of things way more than 2015
 2017年安装了此全新安装，其功能比2015年还要多

46
00:02:52,680 --> 00:02:57,140
the gist of what we really want is the easiest way to go through this is just
我们真正想要的最简单的方法就是

47
00:02:57,340 --> 00:03:00,980
to pick desktop development with C++ that includes pretty much everything you
选择包含几乎所有内容的C ++进行桌面开发

48
00:03:01,180 --> 00:03:04,039
need we don't need Univ Universal Windows platform development that's
需要我们不需要Univ通用Windows平台开发

49
00:03:04,239 --> 00:03:07,550
actually that's something called uwp which is actually something entirely
实际上，这就是所谓的uwp，实际上完全是

50
00:03:07,750 --> 00:03:11,180
different net is something that you probably want
你可能想要不同的网

51
00:03:11,379 --> 00:03:13,910
and still if you're using stuff like say sharp I'm actually going to be using
而且，即使您使用的是诸如Sharp之类的东西，我实际上也会使用

52
00:03:14,110 --> 00:03:17,150
that so I might install that but all you really need to follow along with this
这样，我可能会安装它，但是您真正需要遵循的所有步骤

53
00:03:17,349 --> 00:03:20,719
series is gonna be say plus plus desktop development with say plus Paul's and you
系列会说再加上加上说保罗和你的桌面开发

54
00:03:20,919 --> 00:03:23,810
don't actually need everything that's included in here like this has stuff
实际上不需要这里包含的所有东西

55
00:03:24,009 --> 00:03:30,349
like tables sofas a make an ATL support I'm we don't really need that but the
像桌子沙发一样支持ATL，我不是真的需要，但是

56
00:03:30,549 --> 00:03:33,170
easiest way again to install this is just to click on desktop development
再次安装它的最简单方法是单击桌面开发

57
00:03:33,370 --> 00:03:37,460
with C++ and be done with it so we'll hit install and now it's gonna start
使用C ++并完成它，因此我们将点击install，现在它将开始

58
00:03:37,659 --> 00:03:41,150
downloading and installing everything we need so this will really take a while
下载并安装我们需要的所有内容，因此这确实需要一段时间

59
00:03:41,349 --> 00:03:44,960
all right so now the visual studio is installed let's go ahead and launch it
好的，现在已经安装了Visual Studio，让我们继续并启动它

60
00:03:45,159 --> 00:03:48,650
you can either click launch where you can you know get rid of this and then
您可以单击启动，在那里您可以知道摆脱它，然后

61
00:03:48,849 --> 00:03:54,080
click launch it's up to you so visual studio open you're gonna want to
单击启动由您决定，以便Visual Studio打开，您将要

62
00:03:54,280 --> 00:03:58,849
probably login with your account this is just any Microsoft account ok and here
可能使用您的帐户登录，这只是任何Microsoft帐户，可以在这里

63
00:03:59,049 --> 00:04:03,469
we see the start page so things that I like to get done out of the box I like
我们看到了开始页面，因此我喜欢完成的工作就可以完成

64
00:04:03,669 --> 00:04:07,370
to customize my settings and stuff like that for example under tools options you
自定义我的设置和类似的东西，例如在工具选项下

65
00:04:07,569 --> 00:04:11,990
can go and change to dark mode for example I quite like dark mode there's a
可以转到暗模式，例如，我非常喜欢暗模式， 

66
00:04:12,189 --> 00:04:15,889
color theme you can just go ahead and change that to dark because it's
颜色主题，您可以继续将其更改为深色，因为它是

67
00:04:16,089 --> 00:04:18,980
actually already loaded my settings just from the cloud and everything now I've
实际上已经从云中加载了我的设置，现在我已经

68
00:04:19,180 --> 00:04:22,520
set up visual studio the way that I like to use it including like the syntax
以我喜欢的方式（包括语法）设置Visual Studio 

69
00:04:22,720 --> 00:04:25,379
highlighting theme and everything like if you guys want to use the same
突出显示主题，如果您想使用相同的主题

70
00:04:25,579 --> 00:04:30,420
settings as me you can just go ahead and go to the channel home for such vs the
像我这样的设置，您可以继续并转到频道首页

71
00:04:30,620 --> 00:04:33,900
link will be in the in the description and you'll download this vs settings
链接将在说明中，您将下载此vs设置

72
00:04:34,100 --> 00:04:37,020
file once you've downloaded that file you're gonna want to put it into
文件下载完成后，您要将其放入

73
00:04:37,220 --> 00:04:41,610
documents Visual Studio 2017 settings and then just paste it in there and then
文档Visual Studio 2017设置，然后将其粘贴到其中，然后

74
00:04:41,810 --> 00:04:46,350
back in Visual Studio you can go tools import and export settings import
回到Visual Studio，您可以转到工具导入和导出设置导入

75
00:04:46,550 --> 00:04:50,340
selected environment settings hit next just import new settings don't worry
选定的环境设置点击下一步，只需导入新设置就不用担心

76
00:04:50,540 --> 00:04:54,990
about exporting your old one since it's a fresh install turn OBS is the one you
关于导出旧文件，因为它是全新安装，OBS是您的

77
00:04:55,189 --> 00:05:00,569
want to use and then of course setup any setting or all of them is really what
想使用，然后当然要设置任何设置，否则所有设置实际上就是

78
00:05:00,769 --> 00:05:04,710
you want under general settings it hasn't done everything but I mean the
您想要在常规设置下没有完成所有操作，但是我的意思是

79
00:05:04,910 --> 00:05:08,790
default option is usually pretty good just hit finish and everything should
默认选项通常很好，只需完成即可，一切都应该

80
00:05:08,990 --> 00:05:12,150
work now if you don't have visual assist installed you we might get some warnings
如果您没有安装视觉辅助，现在可以工作，我们可能会收到一些警告

81
00:05:12,350 --> 00:05:16,139
I have an actually install visual assist I might install and then you know rerun
我实际安装了视觉辅助，我可能会安装，然后重新运行

82
00:05:16,339 --> 00:05:19,290
this thing but anyway if you hit close everything should be set up the way that
这个东西，但是无论如何，如果你敲门，一切都应该这样设置

83
00:05:19,490 --> 00:05:22,139
I like it now so let's just make sure everything works by creating a new
我现在喜欢，所以让我们通过创建一个新的来确保一切正常

84
00:05:22,339 --> 00:05:26,490
solution you can actually click on there's a bunch of proto templates I'm
您实际上可以单击的解决方案，我有一堆原型模板

85
00:05:26,689 --> 00:05:30,300
not sure the visuals to 2017 has changed a bit but basically we're gonna go file
不确定2017年的视觉效果是否有所变化，但基本上我们要归档

86
00:05:30,500 --> 00:05:37,770
new project and then under visual C++ under general they'll be a template
新项目，然后在Visual C ++下，它们通常是模板

87
00:05:37,970 --> 00:05:42,480
called empty project we're gonna select that put it any way you like I recommend
称为空项目，我们将选择您喜欢的任何方式放置它

88
00:05:42,680 --> 00:05:48,210
that you put it into C dev I've actually made a folder for it inside C Devin and
您将其放入C开发人员中我实际上已经在C Devin中为其创建了一个文件夹

89
00:05:48,410 --> 00:05:52,319
CPP series the reason I don't like having it here because first of all this
 CPP系列之所以我不喜欢在这里，是因为首先

90
00:05:52,519 --> 00:05:54,990
path is huge second of all you'll see in my case this
在我的案例中，路径是所有第二大功能

91
00:05:55,189 --> 00:06:00,090
path actually has a space in it which will cause problems with some plugins
路径中实际上有一个空格，这将导致某些插件出现问题

92
00:06:00,290 --> 00:06:03,780
for example some components of Nvidia's Android plugins for visual studio
例如Nvidia用于Visual Studio的Android插件的某些组件

93
00:06:03,980 --> 00:06:08,759
actually just won't work if your path has a space in it so that's already bad
实际上，如果您的路径中有空格，那将是行不通的

94
00:06:08,959 --> 00:06:11,129
news also this puts it in your users
新闻也将它放在您的用户中

95
00:06:11,329 --> 00:06:15,720
directory which isn't really necessary so I like to have it just literally see
目录，它不是真正必要的，所以我喜欢从字面上看它

96
00:06:15,920 --> 00:06:19,500
dev and then whatever my project is so in this case EBP series that's actually
开发人员，然后不管我的项目是什么，在这种情况下，实际上是EBP系列

97
00:06:19,699 --> 00:06:23,370
what I'm gonna put it you can give it a name I'll call this hello world we're
我要说的是，你可以给它起个名字，我称这个世界为“你好” 

98
00:06:23,569 --> 00:06:26,759
literally just gonna write a little hello world application that will print
实际上只是要编写一个小的hello world应用程序，它将打印

99
00:06:26,959 --> 00:06:30,509
hello world to the console and test out our entire tool chain
向控制台打招呼，并测试我们的整个工具链

100
00:06:30,709 --> 00:06:33,509
now you'll see there's also something called a solution name Visual Studio has
现在您会看到Visual Studio还有一个叫做解决方案名称的东西

101
00:06:33,709 --> 00:06:37,170
solutions and projects think of a solution as
解决方案和项目将解决方案视为

102
00:06:37,370 --> 00:06:40,410
like a group of projects that are related to each other they can be
就像一组彼此相关的项目

103
00:06:40,610 --> 00:06:44,129
various project types basically a solution is like your workbench and then
基本上，各种项目类型的解决方案就像您的工作台，然后

104
00:06:44,329 --> 00:06:48,360
each project is essentially just a group of files which compiled into some kind
每个项目实质上只是一组文件，这些文件被编译成某种形式

105
00:06:48,560 --> 00:06:51,930
of target binary whether that be a library or an actual executable so that
目标二进制文件的数量，无论是库还是实际的可执行文件，因此

106
00:06:52,129 --> 00:06:56,699
looks pretty good to me I'm just gonna hit OK and we should be taken into our
对我来说看起来不错，我要点击确定，我们应该被带入

107
00:06:56,899 --> 00:07:00,090
project all right now I like to have my solution Explorer on the Left all we're
现在进行所有项目，我想将我的解决方案资源管理器放在左侧

108
00:07:00,290 --> 00:07:04,560
gonna do for now is right click on source files hit add new item under c++
现在要做的是右键单击源文件，在c ++下单击添加新项

109
00:07:04,759 --> 00:07:09,870
file we're just going to call it main dot cpp hit add then I'm going to type
文件，我们将其称为main dot cpp hit add然后我要输入

110
00:07:10,069 --> 00:07:18,718
hash include iostream inter main and then I'm gonna do st st DC out color
哈希包括iostream主要，然后我要做st st DC out color 

111
00:07:18,918 --> 00:07:27,660
world s city and line I'm gonna type in STD cin dot yet and then i'm gonna right
世界城市和线路我要输入STD cin点，然后我就对了

112
00:07:27,860 --> 00:07:32,100
click on my project and hit build we'll have our output window show up and make
点击我的项目并点击构建，我们将显示输出窗口并进行

113
00:07:32,300 --> 00:07:35,310
sure that everything succeeded there you can see that is generated a hello world
确保一切成功在那里您可以看到生成了一个hello world 

114
00:07:35,509 --> 00:07:39,300
dot exe file which is an executable binary for windows and now you can run
点exe文件，它是Windows的可执行二进制文件，现在您可以运行

115
00:07:39,500 --> 00:07:43,528
it either by going to this directory in fact let's let's take a look at this
实际上，通过进入该目录，我们来看一下

116
00:07:43,728 --> 00:07:47,759
directory heylook hello world eh see so we can either run it by just
目录heylook你好世界嗯，所以我们可以只运行它

117
00:07:47,959 --> 00:07:50,968
double-clicking in here and you can see there's our application color world and
双击此处，您可以看到我们的应用程序颜色世界， 

118
00:07:51,168 --> 00:07:55,588
if we press any key then well if we press enter it will terminate all we can
如果我们按任意键，那么如果我们按Enter键，它将终止所有可以

119
00:07:55,788 --> 00:07:58,740
run it by just clicking on local windows debugger which will actually build and
只需点击本地Windows调试器即可运行它， 

120
00:07:58,939 --> 00:08:02,639
run it so there you go and we're actually debugging the code right now so
运行它，然后就可以开始运行了，我们现在实际上正在调试代码，因此

121
00:08:02,839 --> 00:08:05,699
that's it we've got application running we've written our first application in
就是这样，我们已经在运行应用程序，我们已经在中编写了第一个应用程序

122
00:08:05,899 --> 00:08:09,329
c++ and we've verified that our tools work properly if you guys had any
 c ++，我们已经验证了如果您有任何工具，我们的工具可以正常工作

123
00:08:09,529 --> 00:08:11,579
problems with this or something didn't go according to plan
这个问题或某些事情没有按计划进行

124
00:08:11,779 --> 00:08:15,718
leave a comment below and we'll see if we can help you out alright sweet so
在下面发表评论，我们将看看是否可以帮助您，好甜蜜，所以

125
00:08:15,918 --> 00:08:19,079
Visual Studio is installed and we're ready to go and learn some c++ over the
已安装Visual Studio，我们已经准备好通过

126
00:08:19,279 --> 00:08:22,499
next few videos we're gonna learn how c++ actually works knowing how the
接下来的几段视频，我们将了解c ++的实际工作原理， 

127
00:08:22,699 --> 00:08:26,370
applause boss actually works is pretty much the biggest key to writing c++ code
掌声老板的实际工作几乎是编写C ++代码的最大关键

128
00:08:26,569 --> 00:08:29,910
properly make sure you follow me on twitter and instagram and if you really
正确确保您在Twitter和instagram上关注我，如果您真的

129
00:08:30,110 --> 00:08:34,049
like this series you can support it on patreon but until next time goodbye
像这个系列一样，您可以在patreon上支持它，但是直到下次再见

130
00:08:34,250 --> 00:08:39,250
warm
暖

