﻿1
00:00:00,000 --> 00:00:01,360
嘿，大家好，我叫Archana，欢迎回到我的血统
hey what's up guys my name is Archana

2
00:00:01,560 --> 00:00:03,009
嘿，大家好，我叫Archana，欢迎回到我的血统
and welcome back to my figure of blood

3
00:00:03,209 --> 00:00:04,629
今天的理论，我将只谈论图灵，而这种
theory today I'm going to be talking all

4
00:00:04,830 --> 00:00:06,368
今天的理论，我将只谈论图灵，而这种
about Turing little and this kind of

5
00:00:06,569 --> 00:00:08,290
如果你们还没有的话，我们将从上一集谈到的力量继续发展
extends on from strength which we talked

6
00:00:08,490 --> 00:00:10,089
如果你们还没有的话，我们将从上一集谈到的力量继续发展
about last episode if you guys haven't

7
00:00:10,289 --> 00:00:11,890
看到点击卡或下面的描述中的链接就可以了
seen that click on the card or the link

8
00:00:12,089 --> 00:00:13,689
看到点击卡或下面的描述中的链接就可以了
in description below this is just going

9
00:00:13,888 --> 00:00:15,309
可以更深入地了解我们在该视频中看到的内容
to be kind of more of an in-depth look

10
00:00:15,509 --> 00:00:17,199
可以更深入地了解我们在该视频中看到的内容
at what we looked at in that video so

11
00:00:17,399 --> 00:00:19,629
趋势字面量基本上是一个在两个双引号之间的字符
trend literal is basically a there is

12
00:00:19,829 --> 00:00:21,909
趋势字面量基本上是一个在两个双引号之间的字符
characters in between two double quotes

13
00:00:22,109 --> 00:00:24,310
因此，如果我们跳到这里，我可以通过写双引号来定义字符串文字
so if we jump in here I can define a

14
00:00:24,510 --> 00:00:26,470
因此，如果我们跳到这里，我可以通过写双引号来定义字符串文字
string literal by writing double quotes

15
00:00:26,670 --> 00:00:28,480
然后它们之间的东西，例如中国，我们走了
and then something in between them such

16
00:00:28,679 --> 00:00:30,159
然后它们之间的东西，例如中国，我们走了
as China there we go that's a string

17
00:00:30,359 --> 00:00:31,898
现在从字面上看，这实际上变成了什么，取决于许多因素。
literal now what this actually becomes

18
00:00:32,098 --> 00:00:33,909
现在从字面上看，这实际上变成了什么，取决于许多因素。
depend on a number of factors at the

19
00:00:34,109 --> 00:00:36,759
非常基本的情况如果您将鼠标悬停在这上，这实际上是什么
very basic case what this actually is if

20
00:00:36,960 --> 00:00:38,229
非常基本的情况如果您将鼠标悬停在这上，这实际上是什么
you hover your mouse over this is a

21
00:00:38,429 --> 00:00:41,529
相反，尺寸为7的数组现在马上就会出现，您可能会注意到
contra array of size seven now straight

22
00:00:41,729 --> 00:00:43,779
相反，尺寸为7的数组现在马上就会出现，您可能会注意到
away you may notice that there's

23
00:00:43,979 --> 00:00:46,419
其实这里只有六个字符，为什么它是akancha r87
actually only six characters here right

24
00:00:46,619 --> 00:00:49,119
其实这里只有六个字符，为什么它是akancha r87
why is it akancha r87 the reason that

25
00:00:49,320 --> 00:00:50,649
情况是因为实际上在结尾处有一个额外的字符
the case is because there is actually

26
00:00:50,850 --> 00:00:52,809
情况是因为实际上在结尾处有一个额外的字符
one extra character at the very end of

27
00:00:53,009 --> 00:00:54,459
该字符串，它是一个空终止符，如果我们
that string and that is a null

28
00:00:54,659 --> 00:00:56,320
该字符串，它是一个空终止符，如果我们
termination character which if we were

29
00:00:56,520 --> 00:00:57,939
手动编写看起来像是反斜杠零或
to write manually would look like

30
00:00:58,140 --> 00:00:59,559
手动编写看起来像是反斜杠零或
there's a backslash zero or

31
00:00:59,759 --> 00:01:01,448
或者，它可以只是散布一个实际的零，这是
alternatively it can just be spreaded an

32
00:01:01,649 --> 00:01:03,698
或者，它可以只是散布一个实际的零，这是
actual zero and the reason this is

33
00:01:03,899 --> 00:01:06,129
需要的是向字符串的末尾发出信号，试图将其设为零而不是
needed is to signal the end of a string

34
00:01:06,329 --> 00:01:07,778
需要的是向字符串的末尾发出信号，试图将其设为零而不是
what's trying to a zero not the

35
00:01:07,978 --> 00:01:10,238
如果我们写的字符零实际上有不同的字符零
character zero if we write the character

36
00:01:10,438 --> 00:01:12,009
如果我们写的字符零实际上有不同的字符零
zero that actually has a different

37
00:01:12,209 --> 00:01:13,959
如果您写了一个反斜杠零，则总共是数值
numeric value altogether

38
00:01:14,159 --> 00:01:15,819
如果您写了一个反斜杠零，则总共是数值
if you write a backslash zero so that is

39
00:01:16,019 --> 00:01:18,250
空字符是一个实际的数字零，这是早期信号
what that null character is an actual

40
00:01:18,450 --> 00:01:20,649
空字符是一个实际的数字零，这是早期信号
numerical zero that's the early signal

41
00:01:20,849 --> 00:01:22,539
字符串的末尾，因此如果我们要执行类似操作
the end of the string so if we if we

42
00:01:22,739 --> 00:01:24,849
字符串的末尾，因此如果我们要执行类似操作
want to do something like put that back

43
00:01:25,049 --> 00:01:27,278
在字符串中间将斜杠减零，我们实际上会破坏行为
slash zero in the middle of our string

44
00:01:27,478 --> 00:01:29,049
在字符串中间将斜杠减零，我们实际上会破坏行为
we would actually break the behavior of

45
00:01:29,250 --> 00:01:31,929
在很多情况下，此字符串让我们看一下标准c库
this string in many cases let's take a

46
00:01:32,129 --> 00:01:33,609
在很多情况下，此字符串让我们看一下标准c库
look at the standard c library for a bit

47
00:01:33,810 --> 00:01:37,000
我只包含标准的小H，如果我有3个功能
I'll just include standard little H

48
00:01:37,200 --> 00:01:39,668
我只包含标准的小H，如果我有3个功能
which includes some three functions if I

49
00:01:39,868 --> 00:01:41,558
现在将其分配给某物，因为我们将鼠标悬停在此上面，您可以看到
assign this to something now as we hover

50
00:01:41,759 --> 00:01:42,939
现在将其分配给某物，因为我们将鼠标悬停在此上面，您可以看到
our mouse over this you can see that

51
00:01:43,140 --> 00:01:44,649
这是一个集中，所以我可以将其分配给该文化名称
it's a concentrate so I might just

52
00:01:44,849 --> 00:01:48,730
这是一个集中，所以我可以将其分配给该文化名称
assign it to that culture name put it

53
00:01:48,930 --> 00:01:50,528
在这里，如果我在此处设置一个断点，请按f5以便检查内存
over here if I put a breakpoint over

54
00:01:50,728 --> 00:01:52,929
在这里，如果我在此处设置一个断点，请按f5以便检查内存
here hit f5 like I can inspect my memory

55
00:01:53,129 --> 00:01:55,269
将我们放在字符串名称中，即名称，您可以看到我要
put us in the name of the string which

56
00:01:55,469 --> 00:01:57,518
将我们放在字符串名称中，即名称，您可以看到我要
is name you can see that I'm going to

57
00:01:57,718 --> 00:01:59,590
使其具有16列就可以了，所以您可以看到我们切达干酪
make this have like 16 columns okay so

58
00:01:59,790 --> 00:02:00,759
使其具有16列就可以了，所以您可以看到我们切达干酪
you can see that we have cheddar

59
00:02:00,959 --> 00:02:02,109
在这里打印，我们这里有两个成年人，ASCII表示
printing over here we have kind of two

60
00:02:02,310 --> 00:02:03,549
在这里打印，我们这里有两个成年人，ASCII表示
adults here the ASCII representation

61
00:02:03,750 --> 00:02:06,698
现在代表这棵树的长度的两个零
which represents the two zeros now the

62
00:02:06,899 --> 00:02:07,640
现在代表这棵树的长度的两个零
length of this tree

63
00:02:07,840 --> 00:02:09,140
如果您实际计算的字符将是7，因为
if you actually count the character

64
00:02:09,340 --> 00:02:10,610
如果您实际计算的字符将是7，因为
that's going to be 7 because the

65
00:02:10,810 --> 00:02:12,469
反斜杠0是转义字符，表示它仅算作一个字符
backslash 0 is an escaping character

66
00:02:12,669 --> 00:02:13,969
反斜杠0是转义字符，表示它仅算作一个字符
means it just counts as one character

67
00:02:14,169 --> 00:02:15,890
但是将其设置为a是因为我们实际上在以下位置有一个隐式反斜杠0
however this is set to a because we

68
00:02:16,090 --> 00:02:18,140
但是将其设置为a是因为我们实际上在以下位置有一个隐式反斜杠0
actually have an implicit backslash 0 at

69
00:02:18,340 --> 00:02:19,670
最后，我们在这里的最后一个隐式反斜杠0
the very end which we have an implicit

70
00:02:19,870 --> 00:02:21,920
最后，我们在这里的最后一个隐式反斜杠0
backslash 0 at the very end here which

71
00:02:22,120 --> 00:02:23,868
表示字符串的结尾，因此如果我想实际查看字符串是什么
signals the end of our string so if I

72
00:02:24,068 --> 00:02:25,580
表示字符串的结尾，因此如果我想实际查看字符串是什么
want to actually see what my string is

73
00:02:25,780 --> 00:02:28,368
通过运行Stirling的应用程序（这是C函数），基本上可以告诉我
by apps running Stirling which is a C

74
00:02:28,568 --> 00:02:30,200
通过运行Stirling的应用程序（这是C函数），基本上可以告诉我
function which will basically tell me

75
00:02:30,400 --> 00:02:33,140
我在埃尔帕索（El Paso）中的C字符串命名为我的字符串多长时间，我们将看到什么值
how long my C string in El Paso named as

76
00:02:33,340 --> 00:02:34,939
我在埃尔帕索（El Paso）中的C字符串命名为我的字符串多长时间，我们将看到什么值
my string and we'll see what values are

77
00:02:35,139 --> 00:02:36,530
打印，您可以看到我们实际上在此处获得了值2重新打印
printed you can see that we actually get

78
00:02:36,729 --> 00:02:38,300
打印，您可以看到我们实际上在此处获得了值2重新打印
the value 2 reprinting here however

79
00:02:38,500 --> 00:02:40,219
叫做Cherno的地方是lobos和3个字符的原因是
called Cherno is where lobos and 3

80
00:02:40,419 --> 00:02:41,719
叫做Cherno的地方是lobos和3个字符的原因是
characters the reason for that is

81
00:02:41,919 --> 00:02:43,610
因为直到反斜杠0之前它只计数两个字符，因为
because it only counts two characters up

82
00:02:43,810 --> 00:02:46,009
因为直到反斜杠0之前它只计数两个字符，因为
until that backslash 0 because as soon

83
00:02:46,209 --> 00:02:48,649
当它运行到0时，如果我们删除它，似乎在字符串的末尾
as it runs into 0 it seems that's it at

84
00:02:48,848 --> 00:02:50,330
当它运行到0时，如果我们删除它，似乎在字符串的末尾
the end of the string if we remove that

85
00:02:50,530 --> 00:02:52,849
0，然后重新运行此代码，我们当然会得到文本，这是最好的梦
0 and rerun this code of course we'll

86
00:02:53,049 --> 00:02:54,710
0，然后重新运行此代码，我们当然会得到文本，这是最好的梦
get text which is like the best dream

87
00:02:54,909 --> 00:02:56,868
而不是ace，这实际上是数组现在恰好是这样的
and not ace which is actually what the

88
00:02:57,068 --> 00:02:59,030
而不是ace，这实际上是数组现在恰好是这样的
array happens to be right now so at its

89
00:02:59,229 --> 00:03:01,310
核心紧密，这是一个comp char数组，但是我们也可以将其分配给
core tight this is a comp char array

90
00:03:01,509 --> 00:03:03,469
核心紧密，这是一个comp char数组，但是我们也可以将其分配给
however we can also assign it to a

91
00:03:03,669 --> 00:03:04,160
精巧的CompTIA精炼机壳指针
conch-shell

92
00:03:04,360 --> 00:03:06,740
精巧的CompTIA精炼机壳指针
pointer that's totally fine the CompTIA

93
00:03:06,939 --> 00:03:08,660
保证你会操纵力量，所以我将无法做
promises that you would be manipulating

94
00:03:08,860 --> 00:03:10,099
保证你会操纵力量，所以我将无法做
the strength so I won't be able to do

95
00:03:10,299 --> 00:03:13,189
像名字一样的东西等于上升，因为如果我
something like name to equal a rise

96
00:03:13,389 --> 00:03:15,349
像名字一样的东西等于上升，因为如果我
because its marks have comp now if I

97
00:03:15,549 --> 00:03:17,330
删除的补偿这似乎是可能的，而我实际上是不小心
removed comps this appears to be

98
00:03:17,530 --> 00:03:19,219
删除的补偿这似乎是可能的，而我实际上是不小心
possible and I actually accidentally

99
00:03:19,419 --> 00:03:21,618
说这是在先前的字符串视频中，但可能不是很好
said that it was in the previous strings

100
00:03:21,818 --> 00:03:24,830
说这是在先前的字符串视频中，但可能不是很好
video however it's not well it might be

101
00:03:25,030 --> 00:03:26,360
这就是所谓的未定义行为，这基本上意味着C ++
it's something called undefined behavior

102
00:03:26,560 --> 00:03:28,430
这就是所谓的未定义行为，这基本上意味着C ++
which basically means that the C++

103
00:03:28,629 --> 00:03:30,770
标准没有定义在这种情况下应该发生什么
standard doesn't define what should

104
00:03:30,969 --> 00:03:32,300
标准没有定义在这种情况下应该发生什么
happen in this case

105
00:03:32,500 --> 00:03:34,849
因此某些编译器可能会为此生成有效的代码，但您不能依赖于此
so some compilers may generate valid

106
00:03:35,049 --> 00:03:38,420
因此某些编译器可能会为此生成有效的代码，但您不能依赖于此
codes to this but you can't rely on that

107
00:03:38,620 --> 00:03:39,980
所以基本上这是Bend其他编译器甚至不会让您编译
so basically this is Bend other

108
00:03:40,180 --> 00:03:41,719
所以基本上这是Bend其他编译器甚至不会让您编译
compilers won't even let you compiler

109
00:03:41,919 --> 00:03:44,149
msps C代码，这是我使用的Microsoft Visual Studio编译器
code msps C which is Microsoft's visual

110
00:03:44,348 --> 00:03:45,200
msps C代码，这是我使用的Microsoft Visual Studio编译器
studio compiler which is what I'm using

111
00:03:45,400 --> 00:03:47,300
现在编译我们没有任何问题
right now compiled us with no problems

112
00:03:47,500 --> 00:03:47,780
现在编译我们没有任何问题
at all

113
00:03:47,979 --> 00:03:49,550
但是，仅声称我是优秀的编译器，原因是
however compilers that is claiming only

114
00:03:49,750 --> 00:03:51,530
但是，仅声称我是优秀的编译器，原因是
I was good and the reason that this is

115
00:03:51,729 --> 00:03:53,390
未定义且不允许的原因是因为您实际上在这里所做的是
undefined and not allowed is because

116
00:03:53,590 --> 00:03:54,860
未定义且不允许的原因是因为您实际上在这里所做的是
what you've actually done here is you've

117
00:03:55,060 --> 00:03:56,750
采取了指向该字符串文字的存储位置的指针并受到威胁
taken a pointer to the memory location

118
00:03:56,949 --> 00:03:59,030
采取了指向该字符串文字的存储位置的指针并受到威胁
of that string literal and threatened

119
00:03:59,229 --> 00:04:00,890
文字存储在内存的只读部分中，让我们讨论一下
literals are stored in a read-only

120
00:04:01,090 --> 00:04:03,349
文字存储在内存的只读部分中，让我们讨论一下
section of memory let's talk about that

121
00:04:03,549 --> 00:04:04,039
一分钟，让我们回到我们的代码中
for a minute

122
00:04:04,239 --> 00:04:06,020
一分钟，让我们回到我们的代码中
let's jump back into our code I'm going

123
00:04:06,219 --> 00:04:07,640
要在此处打开我的编译器设置，请确保
to open up my compiler settings here

124
00:04:07,840 --> 00:04:08,300
要在此处打开我的编译器设置，请确保
make sure that

125
00:04:08,500 --> 00:04:10,580
在球配置上，我将向人们泛滥并输出文件，并确保
on ball configurations I'll go to people

126
00:04:10,780 --> 00:04:12,920
在球配置上，我将向人们泛滥并输出文件，并确保
flood and output files and make sure

127
00:04:13,120 --> 00:04:14,810
将其设置为在我的汇编器输出中使用源代码进行汇编，我将单击“确定”
that this is set to assembly with source

128
00:04:15,009 --> 00:04:17,150
将其设置为在我的汇编器输出中使用源代码进行汇编，我将单击“确定”
code in my assembler output I'll hit OK

129
00:04:17,350 --> 00:04:18,860
我将切换到此处的发布点只是为了简化组装
and I'll switch to release point over

130
00:04:19,060 --> 00:04:20,689
我将切换到此处的发布点只是为了简化组装
here just to simplify the assembly

131
00:04:20,889 --> 00:04:23,210
输出，接下来我将构建我的项目，我将转到输出目录
output and I will build my project next

132
00:04:23,410 --> 00:04:24,470
输出，接下来我将构建我的项目，我将转到输出目录
I'm going to go to the output directory

133
00:04:24,670 --> 00:04:27,079
为此，我们已经发布了，我们有主要的点ASM，如果您是
to this so we have release and we have

134
00:04:27,279 --> 00:04:29,810
为此，我们已经发布了，我们有主要的点ASM，如果您是
main dot ASM this is Amanda if you be

135
00:04:30,009 --> 00:04:31,550
在那里为主要的SM文件存档，所有驱动程序的个人工作室，我们可以采取
file there for major SM all drivers

136
00:04:31,750 --> 00:04:32,960
在那里为主要的SM文件存档，所有驱动程序的个人工作室，我们可以采取
individual studios that we could take a

137
00:04:33,160 --> 00:04:34,819
查看生成的程序集，在这里您可以看到名为comped的部分
look at be generated assembly go over

138
00:04:35,019 --> 00:04:36,710
查看生成的程序集，在这里您可以看到名为comped的部分
here you can see a section called comped

139
00:04:36,910 --> 00:04:39,590
细分市场，我们在此列出了切尔诺，您在这里看到的实际上是
segments and we have this Cherno listed

140
00:04:39,790 --> 00:04:41,569
细分市场，我们在此列出了切尔诺，您在这里看到的实际上是
here what you see here is actually be

141
00:04:41,769 --> 00:04:43,730
确定了激光，编译器实际上可以推断出该培训师
identified the laser and the compiler

142
00:04:43,930 --> 00:04:45,620
确定了激光，编译器实际上可以推断出该培训师
can actually reason about this trainer

143
00:04:45,819 --> 00:04:47,840
字符串，但数据已设置到特纳，并且手头有一个联系人
string but the data is set over here to

144
00:04:48,040 --> 00:04:50,180
字符串，但数据已设置到特纳，并且手头有一个联系人
Turner and there's a contact in hand so

145
00:04:50,379 --> 00:04:52,520
基本上，如果打开，此趋势将存储在二进制文件的comped部分中
basically this trend is stored in a

146
00:04:52,720 --> 00:04:54,770
基本上，如果打开，此趋势将存储在二进制文件的comped部分中
comped section in our binary if you open

147
00:04:54,970 --> 00:04:56,389
从西南获取的exe文件出现变化时会执行某些操作
up the exe file that you get from the

148
00:04:56,589 --> 00:04:58,009
从西南获取的exe文件出现变化时会执行某些操作
southwest changes occur to do something

149
00:04:58,209 --> 00:04:59,960
多一点有用，所以我将它打印出来，转到组合
a little bit more useful so I'll get it

150
00:05:00,160 --> 00:05:03,050
多一点有用，所以我将它打印出来，转到组合
to print out turn over to the combo that

151
00:05:03,250 --> 00:05:04,790
我们实际上使用此字符串的方式，因此当我们在发布模式下构建该字符串时，
way we're actually using this string so

152
00:05:04,990 --> 00:05:06,500
我们实际上使用此字符串的方式，因此当我们在发布模式下构建该字符串时，
when we build this in release mode the

153
00:05:06,699 --> 00:05:08,629
如果我找到exe文件，编译器现在不会优化我们的Cherno字符串。
compiler won't optimize away our Cherno

154
00:05:08,829 --> 00:05:11,329
如果我找到exe文件，编译器现在不会优化我们的Cherno字符串。
string now if I find the exe file and

155
00:05:11,529 --> 00:05:13,850
在某些东西中打开它，它只是一个十六进制编辑器HFD，您会看到我们
open it in something plushes HFD which

156
00:05:14,050 --> 00:05:15,650
在某些东西中打开它，它只是一个十六进制编辑器HFD，您会看到我们
is just a hex editor you'll see that we

157
00:05:15,850 --> 00:05:17,240
字面上在我们的二进制文件中定义了阴影，这些字符是
literally have shadow defined here

158
00:05:17,439 --> 00:05:19,040
字面上在我们的二进制文件中定义了阴影，这些字符是
inside our binary those characters are

159
00:05:19,240 --> 00:05:20,689
嵌入到我们的二进制文件中，当我们引用时实际上是指
embedded into our binary and when we

160
00:05:20,889 --> 00:05:22,218
嵌入到我们的二进制文件中，当我们引用时实际上是指
reference this is actually referring to

161
00:05:22,418 --> 00:05:24,379
如果您尝试添加，则不允许添加当前数据段
a current data segment that we are not

162
00:05:24,579 --> 00:05:27,079
如果您尝试添加，则不允许添加当前数据段
allowed to add if you do try and add it

163
00:05:27,279 --> 00:05:28,550
即使您尝试添加代码也可以很好地编译它
to like that even though it'll compile

164
00:05:28,750 --> 00:05:30,860
即使您尝试添加代码也可以很好地编译它
just fine if you do try and add a code

165
00:05:31,060 --> 00:05:33,590
像这样在释放模式下，如果我按f5键，即使我们尝试
like this in release mode if I hit f5

166
00:05:33,790 --> 00:05:35,540
像这样在释放模式下，如果我按f5键，即使我们尝试
you'll see that even though we try to

167
00:05:35,740 --> 00:05:38,660
编辑它实际上是行不通的，第三个字符仍然是欧盟，不是现在，如果
edit this it actually didn't work the

168
00:05:38,860 --> 00:05:41,000
编辑它实际上是行不通的，第三个字符仍然是欧盟，不是现在，如果
third character is still EU not a now if

169
00:05:41,199 --> 00:05:42,560
我们试图在调试模式下尽快运行相同的代码
we were to run the same code in debug

170
00:05:42,759 --> 00:05:44,449
我们试图在调试模式下尽快运行相同的代码
mode as soon as we try to actually

171
00:05:44,649 --> 00:05:46,610
执行此代码，我们将引发异常，因为您无法
execute this code we would get an

172
00:05:46,810 --> 00:05:48,650
执行此代码，我们将引发异常，因为您无法
exception thrown because you can't

173
00:05:48,850 --> 00:05:49,939
实际上，如果您确实想这样做，我们将尝试写入只读内存
actually do that we're trying to write

174
00:05:50,139 --> 00:05:52,129
实际上，如果您确实想这样做，我们将尝试写入只读内存
to read-only memory if you did want to

175
00:05:52,329 --> 00:05:54,290
出于某种原因修改它，您可以只需要将类型定义为数组
modify this for some reason you can you

176
00:05:54,490 --> 00:05:56,930
出于某种原因修改它，您可以只需要将类型定义为数组
just need to define the type as an array

177
00:05:57,129 --> 00:05:59,180
而不是指针，现在，如果我们运行此代码，可以按f10键，您可以看到
instead of a pointer and now if we run

178
00:05:59,379 --> 00:06:01,819
而不是指针，现在，如果我们运行此代码，可以按f10键，您可以看到
this code we can hit f10 and you can see

179
00:06:02,019 --> 00:06:03,230
如果我们在这里查看输出，它的工作就很好，我们有一个带
it works just fine if we look at our

180
00:06:03,430 --> 00:06:05,300
如果我们在这里查看输出，它的工作就很好，我们有一个带
output here we have Jenna with an a

181
00:06:05,500 --> 00:06:07,610
请不要像恶魔一样拼写它，所以总结一下，您不能编写代码
please never spell it like that devil so

182
00:06:07,810 --> 00:06:09,710
请不要像恶魔一样拼写它，所以总结一下，您不能编写代码
to sum this up you cannot write code

183
00:06:09,910 --> 00:06:12,170
如果发生未定义的行为，则应采用这种方法，并且您永远不要这样做，其他编译器会这样做
like this if undefined behavior and you

184
00:06:12,370 --> 00:06:13,819
如果发生未定义的行为，则应采用这种方法，并且您永远不要这样做，其他编译器会这样做
should never do it other compilers will

185
00:06:14,019 --> 00:06:14,420
最有可能警告您有关此乌鸦只是丢
most likely

186
00:06:14,620 --> 00:06:16,189
最有可能警告您有关此乌鸦只是丢
warned you about this raven just throw

187
00:06:16,389 --> 00:06:17,629
错误并阻止您编译这样的代码，因为您不应该这样
an error and prevent you can compile a

188
00:06:17,829 --> 00:06:19,069
错误并阻止您编译这样的代码，因为您不应该这样
code like this because you shouldn't be

189
00:06:19,269 --> 00:06:20,660
从赛季开始，在网络研讨会之前就这样做，例如clan will
doing this from season for the webinar

190
00:06:20,860 --> 00:06:23,060
从赛季开始，在网络研讨会之前就这样做，例如clan will
onwards some compile such as clan will

191
00:06:23,259 --> 00:06:25,218
实际上只允许您编译const char指针，如果您想编译一个孩子
actually only let you compile const char

192
00:06:25,418 --> 00:06:27,439
实际上只允许您编译const char指针，如果您想编译一个孩子
pointer if you want to compile a child

193
00:06:27,639 --> 00:06:29,660
像这样的字符串文字的指针，您实际上必须将其强制转换
pointer from a string literal such as

194
00:06:29,860 --> 00:06:31,939
像这样的字符串文字的指针，您实际上必须将其强制转换
this you will actually have to cast it

195
00:06:32,139 --> 00:06:35,060
手动将其放入一个孩子，但是MSP的功能始终是
into a child manually however MSP

196
00:06:35,259 --> 00:06:36,980
手动将其放入一个孩子，但是MSP的功能始终是
features an inter carrot always been to

197
00:06:37,180 --> 00:06:39,528
很好，所以基本上，如果您清除这样的代码，您应该总是
fine so basically if you clear code like

198
00:06:39,728 --> 00:06:40,819
很好，所以基本上，如果您清除这样的代码，您应该总是
this you should really always be

199
00:06:41,019 --> 00:06:43,040
声明已压缩的内容只是提醒自己忽略它，您实际上无法右键单击
declaring a comped just remind yourself

200
00:06:43,240 --> 00:06:44,870
声明已压缩的内容只是提醒自己忽略它，您实际上无法右键单击
ignore it you can't actually right click

201
00:06:45,069 --> 00:06:46,790
就像我们角色中的有趣事实一样，我们当然有char
like this okay similar to fun fact at

202
00:06:46,990 --> 00:06:49,040
就像我们角色中的有趣事实一样，我们当然有char
our characters we have char of course

203
00:06:49,240 --> 00:06:51,290
每个人都按类型分类，但是在40岁以下还有一个称为w的图表
everyone by type however there is also

204
00:06:51,490 --> 00:06:54,650
每个人都按类型分类，但是在40岁以下还有一个称为w的图表
something called w chart under 40 which

205
00:06:54,850 --> 00:06:57,620
被称为白菜，现在让我们非常快速地了解这种类型，
is called a white cabbage now let's just

206
00:06:57,819 --> 00:06:59,509
被称为白菜，现在让我们非常快速地了解这种类型，
go over this types really quickly so we

207
00:06:59,709 --> 00:07:01,189
有一个宽字符指针，我将拉他的名字返回f等于
have a wide character pointer i'll pull

208
00:07:01,389 --> 00:07:03,139
有一个宽字符指针，我将拉他的名字返回f等于
his name to return is f is equal to

209
00:07:03,339 --> 00:07:05,028
中国将得到一个错误，因为它实际上需要在后面加上大写字母L
china will get an error because it

210
00:07:05,228 --> 00:07:07,460
中国将得到一个错误，因为它实际上需要在后面加上大写字母L
actually needs a capital L appended to

211
00:07:07,660 --> 00:07:09,259
它的前面表示下面的字符串文字是由
the front of it this signifies the

212
00:07:09,459 --> 00:07:11,150
它的前面表示下面的字符串文字是由
following string literal is made up of

213
00:07:11,350 --> 00:07:13,218
11的白色字符tipis还引入了许多其他类型，例如
white characters tipis of 11 also

214
00:07:13,418 --> 00:07:15,079
11的白色字符tipis还引入了许多其他类型，例如
introduce a number of other types such

215
00:07:15,279 --> 00:07:18,710
作为16岁以下儿童的下划线团队，您需要再次将其设置为低技术水平
as child 16 underscore team which again

216
00:07:18,910 --> 00:07:21,528
作为16岁以下儿童的下划线团队，您需要再次将其设置为低技术水平
you will need to set equal to a low-tech

217
00:07:21,728 --> 00:07:24,620
您，然后引用您的文字，不要忘记指针，然后我们
you and then in quote your text

218
00:07:24,819 --> 00:07:26,778
您，然后引用您的文字，不要忘记指针，然后我们
don't forget the pointer and then we

219
00:07:26,978 --> 00:07:31,040
也有一个魅力32下划线的名字，为此您和您的拥护者
also have a charm 32 underscore t name

220
00:07:31,240 --> 00:07:34,670
也有一个魅力32下划线的名字，为此您和您的拥护者
for which has an advocate you and your

221
00:07:34,870 --> 00:07:37,100
文本，您也可以定义带有aua前缀的常规控件，像这样
text you can also define the normal

222
00:07:37,300 --> 00:07:40,338
文本，您也可以定义带有aua前缀的常规控件，像这样
control one with a u a prefix like this

223
00:07:40,538 --> 00:07:41,870
如果您真的要强制执行，并且其中有编译器设置
if you really want to enforce that and

224
00:07:42,069 --> 00:07:43,310
如果您真的要强制执行，并且其中有编译器设置
there are compiler setting in which

225
00:07:43,509 --> 00:07:45,949
控制是使用char还是WR，我们可以谈论很多事情
control whether a char or a WR is used

226
00:07:46,149 --> 00:07:47,870
控制是使用char还是WR，我们可以谈论很多事情
there are a lot of things we could talk

227
00:07:48,069 --> 00:07:49,910
再说一次，我不想真的花太长时间了
about again I don't want to really go

228
00:07:50,110 --> 00:07:51,889
再说一次，我不想真的花太长时间了
into this too long I think this brief

229
00:07:52,089 --> 00:07:54,949
但是从本质上讲，字符当然是每个字符一个字节一个孩子16是一个字符
but basically a char is of course a one

230
00:07:55,149 --> 00:07:57,319
但是从本质上讲，字符当然是每个字符一个字节一个孩子16是一个字符
byte per character thing a child 16 is a

231
00:07:57,519 --> 00:07:59,240
每个字符两个字节，每个字符串16位，然后我们有32
two bytes per character at 16 bits a

232
00:07:59,439 --> 00:08:01,850
每个字符两个字节，每个字符串16位，然后我们有32
character string and then we have 32

233
00:08:02,050 --> 00:08:03,620
这是32位字符或每个字符4个字节，这基本上是我要
which is 32 bits character or 4 bytes

234
00:08:03,819 --> 00:08:06,050
这是32位字符或每个字符4个字节，这基本上是我要
per character this is basically me to

235
00:08:06,250 --> 00:08:08,990
坚持使用utf-32，这一点要坚持使用utf-16，然后我们
adhere with with utf-32 this one point

236
00:08:09,189 --> 00:08:10,850
坚持使用utf-32，这一点要坚持使用utf-16，然后我们
to adhere with utf-16 and then we have

237
00:08:11,050 --> 00:08:13,550
utf-8，现在是相反的问题是什么是什么
utf-8 which is contra now the question

238
00:08:13,750 --> 00:08:14,790
utf-8，现在是相反的问题是什么是什么
is what is it what is the

239
00:08:14,990 --> 00:08:17,430
在WHR和应16之间，因为它们似乎都过于偏向鳄梨
between WHR and shall 16 because they

240
00:08:17,629 --> 00:08:19,110
在WHR和应16之间，因为它们似乎都过于偏向鳄梨
appear to both be too biased avocado

241
00:08:19,310 --> 00:08:22,199
萨尔萨（Salsa）现在对他们的角色都说了，但我一直在谈论角色
salsa to both their character now but I

242
00:08:22,399 --> 00:08:23,579
萨尔萨（Salsa）现在对他们的角色都说了，但我一直在谈论角色
keep saying to about the character

243
00:08:23,779 --> 00:08:24,660
但是，实际上取决于编译器来决定是否只是一口气
however that's actually up to the

244
00:08:24,860 --> 00:08:27,150
但是，实际上取决于编译器来决定是否只是一口气
compiler to decide it might be one bite

245
00:08:27,350 --> 00:08:28,710
现在可能是图形中的两个位，可能是4个字节，以提供永不
it might be two bites that might be 4

246
00:08:28,910 --> 00:08:30,780
现在可能是图形中的两个位，可能是4个字节，以提供永不
bytes now in graphic to provide never

247
00:08:30,980 --> 00:08:32,789
看到它被赢得之前通常是2或4，在Windows上是2，在4
seen it to be won by before it's usually

248
00:08:32,990 --> 00:08:35,879
看到它被赢得之前通常是2或4，在Windows上是2，在4
either 2 or 4 it is 2 on Windows and 4

249
00:08:36,080 --> 00:08:38,250
在Linux上，我也希望在Mac上使用，因此如果您
on Linux and I expect Mac as well so it

250
00:08:38,450 --> 00:08:39,269
在Linux上，我也希望在Mac上使用，因此如果您
is a little bit variable if you

251
00:08:39,470 --> 00:08:41,309
绝对希望您可以通过字符串2来处理HR 16，这总是会
definitely want a two by string you can

252
00:08:41,509 --> 00:08:44,279
绝对希望您可以通过字符串2来处理HR 16，这总是会
deal with HR 16 which is always going to

253
00:08:44,480 --> 00:08:46,620
通过思考奇怪的事情准备16比特，将其变成16位或2位
be 16 bits or two by thinking about

254
00:08:46,820 --> 00:08:49,049
通过思考奇怪的事情准备16比特，将其变成16位或2位
weird things to prepare to string such

255
00:08:49,250 --> 00:08:51,539
作为UNL，您实际上也可以参与其中以增强力量，因此有一些东西
as UNL you can actually also attend it

256
00:08:51,740 --> 00:08:53,789
作为UNL，您实际上也可以参与其中以增强力量，因此有一些东西
things to strength so there is something

257
00:08:53,990 --> 00:08:56,759
在表14中称为FTD的字符串下划线文字中，这给了我们
in table 14 called FTD's string

258
00:08:56,960 --> 00:08:59,099
在表14中称为FTD的字符串下划线文字中，这给了我们
underscore literals which give us the

259
00:08:59,299 --> 00:09:00,240
在上一个有关字符串的视频中，为了方便起见，提供了许多功能
number of functions just for convenience

260
00:09:00,440 --> 00:09:02,250
在上一个有关字符串的视频中，为了方便起见，提供了许多功能
in the previous video about strings

261
00:09:02,450 --> 00:09:04,589
我写了这样的代码，我们的CD字符串名称0等于Cherno，我说
I wrote code such as this we have a CD

262
00:09:04,789 --> 00:09:07,740
我写了这样的代码，我们的CD字符串名称0等于Cherno，我说
string name 0 equals Cherno and I said

263
00:09:07,940 --> 00:09:09,899
如果您想将其他字符串附加到该字符串上，那么您实际上
that if you wanted to append some other

264
00:09:10,100 --> 00:09:12,329
如果您想将其他字符串附加到该字符串上，那么您实际上
string onto this one then you actually

265
00:09:12,529 --> 00:09:14,009
无法做到这一点，因为这些当然是字符串文字
couldn't do that because these are

266
00:09:14,210 --> 00:09:15,990
无法做到这一点，因为这些当然是字符串文字
string literal of course which as you

267
00:09:16,190 --> 00:09:18,509
可以看到数组或指针，所以我们不能仅仅将其指向一起
can see are arrays or pointers so we

268
00:09:18,710 --> 00:09:20,279
可以看到数组或指针，所以我们不能仅仅将其指向一起
can't just actually point it together my

269
00:09:20,480 --> 00:09:22,219
解决方案是用一个构造函数将它包围起来，基本上是
solution was to surround it with a

270
00:09:22,419 --> 00:09:24,389
解决方案是用一个构造函数将它包围起来，基本上是
constructor to basically methods of

271
00:09:24,590 --> 00:09:24,839
弦，但是因为他们14岁
string

272
00:09:25,039 --> 00:09:26,909
弦，但是因为他们14岁
however since they were 14 there is

273
00:09:27,110 --> 00:09:28,469
字符串里面的东西实际上是库
something inside the string which were

274
00:09:28,669 --> 00:09:29,879
字符串里面的东西实际上是库
library which actually kind of makes

275
00:09:30,080 --> 00:09:31,799
可能会更容易一点，这取决于您的外观，实际上
that a little bit easier maybe depending

276
00:09:32,000 --> 00:09:32,909
可能会更容易一点，这取决于您的外观，实际上
how you look on it and you can actually

277
00:09:33,110 --> 00:09:35,490
只需将字母添加到字符串的末尾，这就是
just add the letter at to the end of

278
00:09:35,690 --> 00:09:37,769
只需将字母添加到字符串的末尾，这就是
your string and what this does is it's

279
00:09:37,970 --> 00:09:39,389
基本上是一种功能，如果将鼠标永远悬停在G上，您会看到
basically a function and if you hover

280
00:09:39,590 --> 00:09:40,949
基本上是一种功能，如果将鼠标永远悬停在G上，您会看到
your mouse over G forever you can see

281
00:09:41,149 --> 00:09:41,729
这是一个运算符，实际上返回一个
it's an operator

282
00:09:41,929 --> 00:09:43,409
这是一个运算符，实际上返回一个
function that actually returns a

283
00:09:43,610 --> 00:09:47,159
如果您将年龄放在最前面，现在标准字符串与此类似
standard string now similarly to this if

284
00:09:47,360 --> 00:09:49,919
如果您将年龄放在最前面，现在标准字符串与此类似
you were to put you age at the front

285
00:09:50,120 --> 00:09:52,859
定义是否将L放在前面，则得到一个白色字符串，这意味着
that define if you put L at the front

286
00:09:53,059 --> 00:09:54,419
定义是否将L放在前面，则得到一个白色字符串，这意味着
then you get a white string which means

287
00:09:54,620 --> 00:09:57,089
它变成了W字符串，也必须是白色字符串，您可以
that this becomes a W string and it also

288
00:09:57,289 --> 00:09:59,429
它变成了W字符串，也必须是白色字符串，您可以
has to be a white string and you can

289
00:09:59,629 --> 00:10:02,189
也做队列并分配职责以能够做像你这样的事情32力量
also do queue and allocates duty to be

290
00:10:02,389 --> 00:10:04,889
也做队列并分配职责以能够做像你这样的事情32力量
able to do things like you 32 strength

291
00:10:05,090 --> 00:10:07,319
对于各种字符长度，所以对字符串s感到困惑，我们都是
for various character lengths so yeah

292
00:10:07,519 --> 00:10:10,949
对于各种字符长度，所以对字符串s感到困惑，我们都是
confuse about strings s are we all one

293
00:10:11,149 --> 00:10:12,179
我们实际上可以防止该字符串文字的另一件事是字母R，所以
other thing that we can actually prevent

294
00:10:12,379 --> 00:10:14,729
我们实际上可以防止该字符串文字的另一件事是字母R，所以
that string literals is the letter R so

295
00:10:14,929 --> 00:10:17,309
我可以在这里写const char，我将从字母R开始
I can write it const char here and I'll

296
00:10:17,509 --> 00:10:20,009
我可以在这里写const char，我将从字母R开始
start this off with the letter R at the

297
00:10:20,210 --> 00:10:22,379
前面这意味着忽略一个字符
front what this means is to ignore a

298
00:10:22,580 --> 00:10:23,250
前面这意味着忽略一个字符
character

299
00:10:23,450 --> 00:10:25,319
所以在实践中有什么用，实际上我们没有印过
so in practice what it's useful and we

300
00:10:25,519 --> 00:10:26,759
所以在实践中有什么用，实际上我们没有印过
actually haven't have this printed in

301
00:10:26,960 --> 00:10:28,979
这是多行字符串，所以如果我想拥有第1行，第2行
here is multi-line string so if I wanted

302
00:10:29,179 --> 00:10:31,649
这是多行字符串，所以如果我想拥有第1行，第2行
to have something like line 1 line 2

303
00:10:31,850 --> 00:10:35,459
第3行第4行
line 3 line 4 it makes life a little bit

304
00:10:35,659 --> 00:10:37,439
第3行第4行
easier because without this we would

305
00:10:37,639 --> 00:10:39,179
必须做这样的事情，我们实际上会附加所有东西
have to either do something like this

306
00:10:39,379 --> 00:10:40,889
必须做这样的事情，我们实际上会附加所有东西
where we actually append all stuff

307
00:10:41,090 --> 00:10:45,059
在一起，或者我们也可以在尖锐的es等于1行上做，然后简单地写
together or we could also do on sharp es

308
00:10:45,259 --> 00:10:48,000
在一起，或者我们也可以在尖锐的es等于1行上做，然后简单地写
equal line 1 and then just simply write

309
00:10:48,200 --> 00:10:50,969
第2行第3行，您可以看到这些实际上没有加号或其他任何内容
line 2 line 3 you can see that these

310
00:10:51,169 --> 00:10:53,189
第2行第3行，您可以看到这些实际上没有加号或其他任何内容
don't actually have pluses or anything

311
00:10:53,389 --> 00:10:54,659
在它们之间，此外，我们实际上必须将其放回斜线
in between them and additionally we

312
00:10:54,860 --> 00:10:56,279
在它们之间，此外，我们实际上必须将其放回斜线
would actually have to put it back slash

313
00:10:56,480 --> 00:10:58,919
n如果我们希望它们实际上在换行符上，则为
n onto each of these if we wanted them

314
00:10:59,120 --> 00:11:01,169
n如果我们希望它们实际上在换行符上，则为
to actually be on new line so this is

315
00:11:01,370 --> 00:11:02,579
如果您只想写一整段文字，或者也许
very common if you want to actually just

316
00:11:02,779 --> 00:11:04,409
如果您只想写一整段文字，或者也许
write a full paragraph of text or maybe

317
00:11:04,610 --> 00:11:06,959
您的代码中的一些代码为字符串，您希望能够公平地定义它
some code in your code as a string and

318
00:11:07,159 --> 00:11:09,089
您的代码中的一些代码为字符串，您希望能够公平地定义它
you want to be able to define it fairly

319
00:11:09,289 --> 00:11:11,609
容易，这不仅是能够编写代码，还需要做很多工作
easily and this is a lot more work than

320
00:11:11,809 --> 00:11:12,809
容易，这不仅是能够编写代码，还需要做很多工作
just being able to write your code

321
00:11:13,009 --> 00:11:15,719
像这样自由地进行操作，因此R非常有用，毕竟艺术是最后一件事
freely like this so R is a quite useful

322
00:11:15,919 --> 00:11:17,819
像这样自由地进行操作，因此R非常有用，毕竟艺术是最后一件事
and the art after all now the last thing

323
00:11:18,019 --> 00:11:19,559
我想提一提的是关于字符串文字的存储以及如何
that I wanted to mention was again about

324
00:11:19,759 --> 00:11:21,389
我想提一提的是关于字符串文字的存储以及如何
the memory of string literals and how

325
00:11:21,590 --> 00:11:21,959
有效的字符串文字始终存储在
that works

326
00:11:22,159 --> 00:11:23,849
有效的字符串文字始终存储在
string literals are always stored in

327
00:11:24,049 --> 00:11:26,069
只读内存总是因为我们写了像char这样的东西
read-only memory right always just

328
00:11:26,269 --> 00:11:28,049
只读内存总是因为我们写了像char这样的东西
because we write something like char

329
00:11:28,250 --> 00:11:30,269
用数组命名，然后将其设置为零，然后决定更改
name with an array and we set it equal

330
00:11:30,470 --> 00:11:32,699
用数组命名，然后将其设置为零，然后决定更改
to zero and then we decide to change

331
00:11:32,899 --> 00:11:34,919
像我之前所做的一样，如果我将打印输出到控制台
something like I did earlier and will

332
00:11:35,120 --> 00:11:36,419
像我之前所做的一样，如果我将打印输出到控制台
print out out to the console if I

333
00:11:36,620 --> 00:11:38,519
实际看看生成的代码，我将其编译为
actually take a look at the code that

334
00:11:38,720 --> 00:11:40,139
实际看看生成的代码，我将其编译为
this generates I'll compile it in

335
00:11:40,340 --> 00:11:41,699
发布模式，我将在这里打开目录，并将其拖入该汇编文件中
release mode I'll open up my directory

336
00:11:41,899 --> 00:11:44,819
发布模式，我将在这里打开目录，并将其拖入该汇编文件中
here and I'll drag in this assembly file

337
00:11:45,019 --> 00:11:47,189
进入Visual Studio将搜索Chenault，您可以看到该频道
into Visual Studio will search for

338
00:11:47,389 --> 00:11:48,809
进入Visual Studio将搜索Chenault，您可以看到该频道
Chenault and you can see the channel

339
00:11:49,009 --> 00:11:50,819
直到定义了常量，但是我们显然正在对其进行编辑，因此
until defining the constant however

340
00:11:51,019 --> 00:11:52,409
直到定义了常量，但是我们显然正在对其进行编辑，因此
we're obviously editing it so how is

341
00:11:52,610 --> 00:11:55,259
让我们向下滚动好吧，这是我们的功能，让我们来看看
that working let's scroll down ok great

342
00:11:55,460 --> 00:11:56,939
让我们向下滚动好吧，这是我们的功能，让我们来看看
here is our function let's take a look

343
00:11:57,139 --> 00:11:59,039
查看代码，看看发生了什么，我们正在学习内存
at the code and look at what is

344
00:11:59,240 --> 00:12:01,259
查看代码，看看发生了什么，我们正在学习内存
happening we are learning a memory

345
00:12:01,460 --> 00:12:03,299
如果您查看以下内容，则此处的地址就是名称变量的位置
address over here which is the location

346
00:12:03,500 --> 00:12:05,250
如果您查看以下内容，则此处的地址就是名称变量的位置
of the name variable now if you look a

347
00:12:05,450 --> 00:12:07,019
在这里，您会看到我们的名称偏移量为负12
little bit up here you'll see we have

348
00:12:07,220 --> 00:12:09,089
在这里，您会看到我们的名称偏移量为负12
name which has an offset of minus 12 is

349
00:12:09,289 --> 00:12:10,589
基本上在栈上声明的基本变量，这有点
basically variable that is declared on

350
00:12:10,789 --> 00:12:12,240
基本上在栈上声明的基本变量，这有点
the stack again this is getting a little

351
00:12:12,440 --> 00:12:14,189
有点复杂，所以将来我们会提供有关此细节的视频，但是
bit complicated so we'll have videos

352
00:12:14,389 --> 00:12:16,500
有点复杂，所以将来我们会提供有关此细节的视频，但是
about details on this in the future but

353
00:12:16,700 --> 00:12:18,179
现在这是我们的名称变量，基本上此地址正在加载
for now this is our name variable

354
00:12:18,379 --> 00:12:20,099
现在这是我们的名称变量，基本上此地址正在加载
basically this address is being loaded

355
00:12:20,299 --> 00:12:22,259
进入EDX寄存器，然后我们将Cherno
into the EDX register and

356
00:12:22,460 --> 00:12:24,539
进入EDX寄存器，然后我们将Cherno
we are then moving the Cherno this is

357
00:12:24,740 --> 00:12:27,899
该影子字符串文字在我们的只读存储器中的位置
the location of that shadow string

358
00:12:28,100 --> 00:12:29,490
该影子字符串文字在我们的只读存储器中的位置
literal in our read-only memory we're

359
00:12:29,690 --> 00:12:31,259
如果是SIDS，则将其再次放入应用注册，因为
putting that into the a apps register

360
00:12:31,460 --> 00:12:33,449
如果是SIDS，则将其再次放入应用注册，因为
again if it's a SIDS because the

361
00:12:33,649 --> 00:12:34,979
编译器正在尝试进行优化，这是我最大的能力
compiler is trying to do optimizations

362
00:12:35,179 --> 00:12:36,990
编译器正在尝试进行优化，这是我最大的能力
here this is release most of my ability

363
00:12:37,190 --> 00:12:38,490
位水化器代码，然后我们在该名称变量中学习ax
bit hydrators code and then we're

364
00:12:38,690 --> 00:12:40,799
位水化器代码，然后我们在该名称变量中学习ax
learning ax into that name variable so

365
00:12:41,000 --> 00:12:42,750
实际上，我们在这里所做的基本上是，切尔诺细分市场和
what we've actually done here basically

366
00:12:42,950 --> 00:12:45,240
实际上，我们在这里所做的基本上是，切尔诺细分市场和
gotten that that Cherno segment and

367
00:12:45,440 --> 00:12:47,759
我们已经将其复制到该名称变量中，因此我们实际上创建了一个实际的
we've copied it into that name variable

368
00:12:47,960 --> 00:12:48,959
我们已经将其复制到该名称变量中，因此我们实际上创建了一个实际的
so we've actually created an actual

369
00:12:49,159 --> 00:12:51,719
如果我们不编写这段代码，这里的变量就是我们想要做的
variable here before if we don't write

370
00:12:51,919 --> 00:12:53,009
如果我们不编写这段代码，这里的变量就是我们想要做的
this code what we're trying to do is

371
00:12:53,210 --> 00:12:55,649
修改指向该恒定数据段的指针，因此实际上
modify the pointer that points to that

372
00:12:55,850 --> 00:12:57,240
修改指向该恒定数据段的指针，因此实际上
constant data segment so we're actually

373
00:12:57,440 --> 00:12:59,099
尝试将其写入常量数据中是创建另一个变量，然后
trying to write it into the constant

374
00:12:59,299 --> 00:13:01,709
尝试将其写入常量数据中是创建另一个变量，然后
data here is create another variable and

375
00:13:01,909 --> 00:13:03,809
您可以在此行中看到，我们移动了一个数值97，每个该名称变量
you can see on this line here we moved a

376
00:13:04,009 --> 00:13:06,209
您可以在此行中看到，我们移动了一个数值97，每个该名称变量
numeric value 97 each that name variable

377
00:13:06,409 --> 00:13:09,029
增加了偏移量，这就是这行代码在做的事97是
added offset up to that that's what this

378
00:13:09,230 --> 00:13:12,539
增加了偏移量，这就是这行代码在做的事97是
line of code here is doing 97 is the

379
00:13:12,740 --> 00:13:15,209
字符的数字十进制表示形式看一下，就这样迈克尔
numerical decimal representation of the

380
00:13:15,409 --> 00:13:18,000
字符的数字十进制表示形式看一下，就这样迈克尔
character look at a so that's it Michael

381
00:13:18,200 --> 00:13:19,979
非常希望您现在了解孩子指针和所有其他东西的工作原理
pretty deep hope you understand now how

382
00:13:20,179 --> 00:13:22,529
非常希望您现在了解孩子指针和所有其他东西的工作原理
child pointers and all that stuff works

383
00:13:22,730 --> 00:13:24,299
火车鸣笛一般，了解自己非常重要
the train whistle the general it's

384
00:13:24,500 --> 00:13:25,559
火车鸣笛一般，了解自己非常重要
really important to understand itself

385
00:13:25,759 --> 00:13:26,549
因为你可能会在剩下的时间里处理力量
because you're probably going to be

386
00:13:26,750 --> 00:13:27,990
因为你可能会在剩下的时间里处理力量
dealing with strength for the rest of

387
00:13:28,190 --> 00:13:29,699
您的编程生涯一如既往，我希望你们喜欢这个视频
your programming career as always I hope

388
00:13:29,899 --> 00:13:30,959
您的编程生涯一如既往，我希望你们喜欢这个视频
you guys enjoyed this video if you did

389
00:13:31,159 --> 00:13:32,729
您可以按一下“喜欢”按钮，以便我知道您喜欢我的视频，
you could hit that like button just so

390
00:13:32,929 --> 00:13:34,829
您可以按一下“喜欢”按钮，以便我知道您喜欢我的视频，
that I know that you liked my video you

391
00:13:35,029 --> 00:13:36,419
如果您愿意，还可以在patreon上支持该系列节目或恢复频道
can also support this series on patreon

392
00:13:36,620 --> 00:13:38,429
如果您愿意，还可以在patreon上支持该系列节目或恢复频道
or convalesce the channel if you if you

393
00:13:38,629 --> 00:13:40,439
希望您获得一些不错的奖励，例如DMS有助于规划这些
want to you get some cool rewards such

394
00:13:40,639 --> 00:13:42,179
希望您获得一些不错的奖励，例如DMS有助于规划这些
as DMS contribute to planning of these

395
00:13:42,379 --> 00:13:44,729
视频，以及有时尽早获取这些视频，然后全部删除
videos as well as getting these videos

396
00:13:44,929 --> 00:13:46,620
视频，以及有时尽早获取这些视频，然后全部删除
early sometimes and being drops and all

397
00:13:46,820 --> 00:13:48,359
有趣的东西，以后再见。
that fun stuff I'll see you guys later

398
00:13:48,559 --> 00:13:49,309
有趣的东西，以后再见。
good bye

399
00:13:49,509 --> 00:13:52,709
[音乐]哦
[Music]

400
00:13:52,909 --> 00:13:54,969
[音乐]哦
Oh

401
00:13:58,100 --> 00:14:03,100
[音乐]
[Music]

