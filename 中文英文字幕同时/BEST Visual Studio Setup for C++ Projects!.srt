﻿1
00:00:05,628 --> 00:00:07,839
嘿，大家好，我叫埃特诺，欢迎回到另一个视频
hey what's up guys my name is eterno

2
00:00:08,039 --> 00:00:10,169
嘿，大家好，我叫埃特诺，欢迎回到另一个视频
welcome back to another video

3
00:00:10,369 --> 00:00:13,540
Gama coffee意味着我现在可以制作视频，所以今天我想我会只是
Gama coffee means I can make a video now

4
00:00:13,740 --> 00:00:16,089
Gama coffee意味着我现在可以制作视频，所以今天我想我会只是
so today I thought I would kind of just

5
00:00:16,289 --> 00:00:18,190
制作一些视频，这将是一周中的一小部分
make a little video it's going to be a

6
00:00:18,390 --> 00:00:20,409
制作一些视频，这将是一周中的一小部分
small one kind of a midweek thing for

7
00:00:20,609 --> 00:00:22,210
你们好，我只是想谈一些我不觉得的事情
you guys well I just I just wanted to

8
00:00:22,410 --> 00:00:23,499
你们好，我只是想谈一些我不觉得的事情
talk about something that I didn't feel

9
00:00:23,699 --> 00:00:25,989
这么说会花我很长时间
would take that long that being said the

10
00:00:26,189 --> 00:00:27,609
这么说会花我很长时间
amount of videos that I started off

11
00:00:27,809 --> 00:00:29,079
就像这将是一个短片，然后最终被
being like this is going to be a short

12
00:00:29,278 --> 00:00:31,089
就像这将是一个短片，然后最终被
video and then it's ended up being

13
00:00:31,289 --> 00:00:32,378
大约半个小时的事情就像我的意思是
something that's like half an hour long

14
00:00:32,579 --> 00:00:35,108
大约半个小时的事情就像我的意思是
has been like I mean all of them really

15
00:00:35,308 --> 00:00:36,579
但是今天我们要讨论的是我建立自己的发言权加方式的方式
but today we're just going to talk about

16
00:00:36,780 --> 00:00:38,469
但是今天我们要讨论的是我建立自己的发言权加方式的方式
the way that I set up my say plus

17
00:00:38,670 --> 00:00:40,869
这些项目不一定是每个人的最佳设置
projects these aren't necessarily going

18
00:00:41,070 --> 00:00:42,549
这些项目不一定是每个人的最佳设置
to be the best settings for everyone

19
00:00:42,750 --> 00:00:43,089
坦帕不过是我用过的东西
Tampa

20
00:00:43,289 --> 00:00:44,829
坦帕不过是我用过的东西
however it is what I use it's what I've

21
00:00:45,030 --> 00:00:47,469
像是多年以来逐渐使用的学习方法
kind of learned like grown into using

22
00:00:47,670 --> 00:00:49,689
像是多年以来逐渐使用的学习方法
over the years and honestly every single

23
00:00:49,890 --> 00:00:51,640
服务损失项目或类似项目，而我不仅仅是在谈论一个项目
service loss project or like and I'm not

24
00:00:51,840 --> 00:00:53,049
服务损失项目或类似项目，而我不仅仅是在谈论一个项目
just talking about a single project I'm

25
00:00:53,250 --> 00:00:54,969
像整个项目的解决方案那样谈论，所以我每个人
talking about like a whole solution of

26
00:00:55,170 --> 00:00:56,559
像整个项目的解决方案那样谈论，所以我每个人
projects so every single one that I

27
00:00:56,759 --> 00:00:58,570
从现在开始创建，我几乎使用这些设置，所以让我们
create from now on pretty much I use

28
00:00:58,770 --> 00:01:00,099
从现在开始创建，我几乎使用这些设置，所以让我们
these settings so let's just let's just

29
00:01:00,299 --> 00:01:01,599
进入看看，所以我们刚刚打开Visual Studio，我们已经开始了
jump in and take a look so we've just

30
00:01:01,799 --> 00:01:03,399
进入看看，所以我们刚刚打开Visual Studio，我们已经开始了
opened Visual Studio we've got our start

31
00:01:03,600 --> 00:01:05,319
页面在这里什么都没有，我还没有打开任何解决方案，但是我要创建
page here nothing's I haven't opened any

32
00:01:05,519 --> 00:01:07,028
页面在这里什么都没有，我还没有打开任何解决方案，但是我要创建
kind of solution yet I'm going to create

33
00:01:07,228 --> 00:01:08,980
通过点击文件new，然后为
a brand new project and solution here by

34
00:01:09,180 --> 00:01:12,129
通过点击文件new，然后为
hitting file new and then project for

35
00:01:12,329 --> 00:01:13,569
现在，我将进入Visual C ++ general，并创建一个空项目，然后
now I'm just going to go onto Visual C++

36
00:01:13,769 --> 00:01:15,909
现在，我将进入Visual C ++ general，并创建一个空项目，然后
general and create an empty project and

37
00:01:16,109 --> 00:01:17,789
那么就位置而言，我现在称之为新项目
then I'll just call it something like

38
00:01:17,989 --> 00:01:20,619
那么就位置而言，我现在称之为新项目
new project now in terms of the location

39
00:01:20,819 --> 00:01:22,299
我喜欢将我开发的项目存储在种子目录下，而不是
I like to store the projects that I

40
00:01:22,500 --> 00:01:24,159
我喜欢将我开发的项目存储在种子目录下，而不是
develop under the seed directory and not

41
00:01:24,359 --> 00:01:26,319
当我切换计算机或类似的东西时，以这种方式在用户文件夹中
inside the user folder that way when I'm

42
00:01:26,519 --> 00:01:27,640
当我切换计算机或类似的东西时，以这种方式在用户文件夹中
switching computers or anything like

43
00:01:27,840 --> 00:01:29,259
它不会破坏任何公园，实际上它要容易得多
that it's not going to break any parks

44
00:01:29,459 --> 00:01:30,819
它不会破坏任何公园，实际上它要容易得多
and really it's just a lot easier to

45
00:01:31,019 --> 00:01:32,289
将您的开发项目存储在某种中央目录中，所以我
kind of store your development project

46
00:01:32,489 --> 00:01:34,149
将您的开发项目存储在某种中央目录中，所以我
in some kind of central directory so I

47
00:01:34,349 --> 00:01:36,159
喜欢做诸如说flash dev之类的事情，然后也许我们将其称为新项目
like to do something like say flash dev

48
00:01:36,359 --> 00:01:38,109
喜欢做诸如说flash dev之类的事情，然后也许我们将其称为新项目
and then maybe we'll call it new project

49
00:01:38,310 --> 00:01:39,730
或类似的东西，因为我们已经创建了目录解析，它将
or something like that now because we've

50
00:01:39,930 --> 00:01:41,109
或类似的东西，因为我们已经创建了目录解析，它将
got create directory resolution it will

51
00:01:41,310 --> 00:01:42,789
实际上会自动创建该新项目文件夹，因此我需要编写该文件夹
actually automatically create that new

52
00:01:42,989 --> 00:01:44,289
实际上会自动创建该新项目文件夹，因此我需要编写该文件夹
project folder so I need to write that

53
00:01:44,489 --> 00:01:45,759
我点击确定，我们会看到发生了什么
I'll just hit OK and we'll see what

54
00:01:45,959 --> 00:01:48,048
我点击确定，我们会看到发生了什么
happens

55
00:01:49,700 --> 00:01:52,509
我确定这会花点时间，所以一旦我们有了这个新的空项目
I'm sure it's taking a while okay so

56
00:01:52,709 --> 00:01:53,500
我确定这会花点时间，所以一旦我们有了这个新的空项目
once we've got this new empty project

57
00:01:53,700 --> 00:01:55,840
您可以看到我们这里绝对没有文件或类似的文件
you can see we have absolutely no files

58
00:01:56,040 --> 00:01:57,039
您可以看到我们这里绝对没有文件或类似的文件
here or anything like that if I

59
00:01:57,239 --> 00:01:58,959
右键单击实际项目，然后在文件资源管理器中打开文件夹
right-click on the actual project and

60
00:01:59,159 --> 00:02:01,628
右键单击实际项目，然后在文件资源管理器中打开文件夹
hit open folder in File Explorer you'll

61
00:02:01,828 --> 00:02:04,268
查看Visual Studio为我们创建的实际目录结构，那么
see the actual directory structure the

62
00:02:04,468 --> 00:02:05,799
查看Visual Studio为我们创建的实际目录结构，那么
visual studio has created for us so what

63
00:02:06,000 --> 00:02:07,329
我们实际上是在那个dev文件夹中，我们已经有了一个新项目，
we've actually got is inside that dev

64
00:02:07,530 --> 00:02:09,279
我们实际上是在那个dev文件夹中，我们已经有了一个新项目，
folder we've got that new project we've

65
00:02:09,479 --> 00:02:11,140
旁边有一个解决方案文件，您还可以看到
got a solution file right next to there

66
00:02:11,340 --> 00:02:12,530
旁边有一个解决方案文件，您还可以看到
you can see that also what

67
00:02:12,729 --> 00:02:14,570
工作室已经做到了，这称为我们的解决方案与我们的项目完全相同
studio has done it is called our

68
00:02:14,770 --> 00:02:16,759
工作室已经做到了，这称为我们的解决方案与我们的项目完全相同
solution the exact same as our project

69
00:02:16,959 --> 00:02:19,969
因此，如果这只是一个
so that's not necessarily something that

70
00:02:20,169 --> 00:02:21,920
因此，如果这只是一个
you want if this is just going to be a

71
00:02:22,120 --> 00:02:23,660
解决方案中只有一个项目，那么也许是，但是您知道我们是否
solution with a single project in it

72
00:02:23,860 --> 00:02:25,880
解决方案中只有一个项目，那么也许是，但是您知道我们是否
then maybe it is but you know if we're

73
00:02:26,080 --> 00:02:27,380
像游戏之类的东西，那对我们来说是非常罕见的
making like a game or something then

74
00:02:27,580 --> 00:02:29,840
像游戏之类的东西，那对我们来说是非常罕见的
it's very very rare for us to kind of

75
00:02:30,039 --> 00:02:31,789
希望我们的解决方案和项目名称相同，但是无论如何此结构都是
want our solution and project name to be

76
00:02:31,989 --> 00:02:34,039
希望我们的解决方案和项目名称相同，但是无论如何此结构都是
identical but anyway this structure is

77
00:02:34,239 --> 00:02:35,780
其实还不错，我们有一个专用于我们的文件夹
actually not bad

78
00:02:35,979 --> 00:02:38,420
其实还不错，我们有一个专用于我们的文件夹
we've got a folder dedicated for our for

79
00:02:38,620 --> 00:02:40,670
在我们的项目中，我们同时拥有VCF配置文件和过滤器文件
our project we've got both a VCF profile

80
00:02:40,870 --> 00:02:42,500
在我们的项目中，我们同时拥有VCF配置文件和过滤器文件
and a filters file I'm not going to get

81
00:02:42,699 --> 00:02:43,700
现在可能对Visual Studio现在创建的所有不同文件过于深入
too in-depth about all the different

82
00:02:43,900 --> 00:02:45,590
现在可能对Visual Studio现在创建的所有不同文件过于深入
files Visual Studio create now might be

83
00:02:45,789 --> 00:02:46,969
关于另一个视频的想法，但是基本上VCF方法是我们的​​项目文件
an idea for another video but basically

84
00:02:47,169 --> 00:02:49,819
关于另一个视频的想法，但是基本上VCF方法是我们的​​项目文件
the VCF approach is our project file

85
00:02:50,019 --> 00:02:51,920
这只是一个XML文件，然后我们还有一个解决方案文件，它是
which is just an XML file and then we've

86
00:02:52,120 --> 00:02:53,689
这只是一个XML文件，然后我们还有一个解决方案文件，它是
also got a solution file which is it's

87
00:02:53,889 --> 00:02:55,550
唯一一种奇怪的格式是文本文件夹，这是某种形式的
only kind of weird format it's the text

88
00:02:55,750 --> 00:02:57,860
唯一一种奇怪的格式是文本文件夹，这是某种形式的
folder and that's in a certain kind of

89
00:02:58,060 --> 00:03:00,319
目录好吧，酷，我实际上很喜欢这种结构，我也不会更改它
directory okay cool I actually like this

90
00:03:00,519 --> 00:03:01,550
目录好吧，酷，我实际上很喜欢这种结构，我也不会更改它
structure I'm not going to change it too

91
00:03:01,750 --> 00:03:03,530
很多，但是您会注意到，我们在这里得到的是一堆各种文件夹
much but you'll notice that also what we

92
00:03:03,729 --> 00:03:05,719
很多，但是您会注意到，我们在这里得到的是一堆各种文件夹
get here is a bunch of various folders

93
00:03:05,919 --> 00:03:07,580
现在这些不是文件夹，有一些叫做过滤器的东西
now these are not folders right there

94
00:03:07,780 --> 00:03:10,009
现在这些不是文件夹，有一些叫做过滤器的东西
are things called filters filters are

95
00:03:10,209 --> 00:03:12,170
不是文件夹，如果我右键单击此处的新项目并做广告，您会看到
not folders if I right click on new

96
00:03:12,370 --> 00:03:13,969
不是文件夹，如果我右键单击此处的新项目并做广告，您会看到
project here and do ad you'll see

97
00:03:14,169 --> 00:03:16,399
没有新的文件夹新的过滤器，如果我添加一个称为日记或
there's no new folders new filter and if

98
00:03:16,598 --> 00:03:17,810
没有新的文件夹新的过滤器，如果我添加一个称为日记或
I add a filter called journal or

99
00:03:18,009 --> 00:03:19,400
什么都不会改变我们磁盘上的东西，这不是它的方式
something nothing is going to change on

100
00:03:19,599 --> 00:03:20,868
什么都不会改变我们磁盘上的东西，这不是它的方式
our disk that's not the way that it

101
00:03:21,068 --> 00:03:22,759
可以使用此过滤器文件，但确实包含此类虚拟文件夹
works this filters file however does

102
00:03:22,959 --> 00:03:24,740
可以使用此过滤器文件，但确实包含此类虚拟文件夹
contain these kind of virtual folders

103
00:03:24,939 --> 00:03:26,000
我们创建的这些文件非常棒，例如组织您的源代码
that we create so these are fantastic

104
00:03:26,199 --> 00:03:27,950
我们创建的这些文件非常棒，例如组织您的源代码
such as organizing your source code

105
00:03:28,150 --> 00:03:29,810
在磁盘上实际上不存在但实际上存在于其中的文件夹中
within folders that don't actually exist

106
00:03:30,009 --> 00:03:32,480
在磁盘上实际上不存在但实际上存在于其中的文件夹中
on disk but they do exist in this

107
00:03:32,680 --> 00:03:35,180
解决方案资源管理器视图，因此，例如，如果要进入源文件并添加一个
solution Explorer view so if I was to go

108
00:03:35,379 --> 00:03:36,980
解决方案资源管理器视图，因此，例如，如果要进入源文件并添加一个
into source files for example and add a

109
00:03:37,180 --> 00:03:39,439
新项目源文件位于文件夹中，因此，如果我添加的项目名为
new item source files is in a folder so

110
00:03:39,639 --> 00:03:40,730
新项目源文件位于文件夹中，因此，如果我添加的项目名为
if I add an item with something called

111
00:03:40,930 --> 00:03:43,039
主点CPP创建文件后，您会看到所有内容
main dot CPP once it creates the file

112
00:03:43,239 --> 00:03:44,090
主点CPP创建文件后，您会看到所有内容
and everything you'll see that it's

113
00:03:44,289 --> 00:03:45,590
实际上只是在我的项目旁边在这里创建的，就是我
actually just created that right here

114
00:03:45,789 --> 00:03:47,180
实际上只是在我的项目旁边在这里创建的，就是我
next to my project and that's just I

115
00:03:47,379 --> 00:03:49,460
只是那只是一团糟，好吧，看看为什么，所以我喜欢做什么
just that's just that's just messy right

116
00:03:49,659 --> 00:03:51,920
只是那只是一团糟，好吧，看看为什么，所以我喜欢做什么
well look what why so what I like to do

117
00:03:52,120 --> 00:03:53,060
我想创建一个名为source或SRC的文件夹，其中包含我所有的
is I like to create a folder called

118
00:03:53,259 --> 00:03:55,759
我想创建一个名为source或SRC的文件夹，其中包含我所有的
source or SRC which contains all of my

119
00:03:55,959 --> 00:03:56,990
源代码和头文件以及该文件夹中的所有东西
source code and header files and all

120
00:03:57,189 --> 00:03:58,879
源代码和头文件以及该文件夹中的所有东西
that stuff kind of in that folder so

121
00:03:59,079 --> 00:04:00,649
我的项目文件和我可能正在使用的任何其他资源
that my project files and any other

122
00:04:00,848 --> 00:04:02,599
我的项目文件和我可能正在使用的任何其他资源
resources that I might be using in my

123
00:04:02,799 --> 00:04:04,280
项目实际上实际上是分成几个文件夹，
project are actually kind of actually

124
00:04:04,479 --> 00:04:06,110
项目实际上实际上是分成几个文件夹，
separated into folders and that's

125
00:04:06,310 --> 00:04:07,640
Visual Studio不会自动为您设置的内容，因此您可以
something that the visual studio doesn't

126
00:04:07,840 --> 00:04:09,110
Visual Studio不会自动为您设置的内容，因此您可以
set up for you automatically so you kind

127
00:04:09,310 --> 00:04:10,580
必须配合使用，所以如果我们返回Visual Studio，可以单击我们的
of have to roll with it so if we go back

128
00:04:10,780 --> 00:04:12,680
必须配合使用，所以如果我们返回Visual Studio，可以单击我们的
to visual studio we can click on our

129
00:04:12,879 --> 00:04:15,230
项目在这里，一个小按钮将出现在此处，显示是否显示所有文件
project here and a little button will

130
00:04:15,430 --> 00:04:17,300
项目在这里，一个小按钮将出现在此处，显示是否显示所有文件
appear here which says show all files if

131
00:04:17,500 --> 00:04:21,348
我们单击该视图，此视图实际上是您的目录结构，
we click that this view this view is

132
00:04:21,548 --> 00:04:24,649
我们单击该视图，此视图实际上是您的目录结构，
actually your directory structure that

133
00:04:24,848 --> 00:04:25,759
在硬盘上的磁盘上，所以我现在右键单击
is on your disk

134
00:04:25,959 --> 00:04:27,949
在硬盘上的磁盘上，所以我现在右键单击
on your harddrive so I right click now

135
00:04:28,149 --> 00:04:29,749
然后点击添加，您会看到过滤器已替换为新文件夹，我可以
and hit add you'll see the filter is

136
00:04:29,949 --> 00:04:31,610
然后点击添加，您会看到过滤器已替换为新文件夹，我可以
replaced with new folder and I can

137
00:04:31,810 --> 00:04:33,379
实际创建一个名为case的文件夹，如果我将Tab变回文件，我会看到正确的
actually make a folder called case I'll

138
00:04:33,579 --> 00:04:35,990
实际创建一个名为case的文件夹，如果我将Tab变回文件，我会看到正确的
see right if I alt tab back to my file

139
00:04:36,189 --> 00:04:37,819
资源管理器看到我们有一个名为source的文件夹
explorer look at that we've got a folder

140
00:04:38,019 --> 00:04:38,480
资源管理器看到我们有一个名为source的文件夹
called source

141
00:04:38,680 --> 00:04:40,879
我们现在可以将mendosa TP文件移动到该源目录中
let's move this mendosa TP file into

142
00:04:41,079 --> 00:04:42,499
我们现在可以将mendosa TP文件移动到该源目录中
that source directory now I can do so

143
00:04:42,699 --> 00:04:44,059
使用Windows资源管理器，这意味着我将必须手动更新它
using Windows Explorer which would mean

144
00:04:44,259 --> 00:04:45,649
使用Windows资源管理器，这意味着我将必须手动更新它
that I would have to manually update it

145
00:04:45,848 --> 00:04:47,389
在Visual Studio中，或者我可以像这样单击并拖动它，您可以看到
in Visual Studio or I can just click and

146
00:04:47,589 --> 00:04:49,309
在Visual Studio中，或者我可以像这样单击并拖动它，您可以看到
drag it like that and you can see that

147
00:04:49,509 --> 00:04:51,499
实际上是将磁盘上的文件移到该目录中，所以现在Visual Studio是
on disk is actually moved it into that

148
00:04:51,699 --> 00:04:53,480
实际上是将磁盘上的文件移到该目录中，所以现在Visual Studio是
directory okay so now Visual Studio is

149
00:04:53,680 --> 00:04:55,069
实际上表现得更像我想要的，所以这种表演
actually behaving a little bit more like

150
00:04:55,269 --> 00:04:57,199
实际上表现得更像我想要的，所以这种表演
I would like it to so this kind of show

151
00:04:57,399 --> 00:04:59,360
所有文件按钮非常有用，说实话，我主要是在使用
all files button is incredibly useful

152
00:04:59,560 --> 00:05:01,759
所有文件按钮非常有用，说实话，我主要是在使用
and honestly I kind of use that mostly

153
00:05:01,959 --> 00:05:04,069
如果我们返回到过滤器，您会看到main仍在源中
if we go back to our filters you can see

154
00:05:04,269 --> 00:05:05,990
如果我们返回到过滤器，您会看到main仍在源中
that main is still inside the source

155
00:05:06,189 --> 00:05:07,990
文件，这只是一种虚拟组织的方案，我可以
files this is just a virtual

156
00:05:08,189 --> 00:05:11,718
文件，这只是一种虚拟组织的方案，我可以
organization kind of scheme right I can

157
00:05:11,918 --> 00:05:13,129
将其拖动到头文件中，这无关紧要
drag this to header files it doesn't

158
00:05:13,329 --> 00:05:14,990
将其拖动到头文件中，这无关紧要
matter where this is right it could be

159
00:05:15,189 --> 00:05:16,730
只是在没有过滤器的情况下，它就可以在这里删除所有内容
just in no filters which would make it

160
00:05:16,930 --> 00:05:18,499
只是在没有过滤器的情况下，它就可以在这里删除所有内容
here I can just go ahead and delete all

161
00:05:18,699 --> 00:05:21,139
这些实际上并不重要，这些过滤器视图仅是
these in fact it doesn't really matter

162
00:05:21,339 --> 00:05:24,410
这些实际上并不重要，这些过滤器视图仅是
these this filter view is all just it's

163
00:05:24,610 --> 00:05:26,209
所有伪造的权利，一切都只是为了组织事物
all fake right it's all just kind of

164
00:05:26,408 --> 00:05:28,309
所有伪造的权利，一切都只是为了组织事物
there for you to just organize things

165
00:05:28,509 --> 00:05:31,430
成组，它与磁盘上的实际目录结构没有关系，因此如果
into groups it has no no relation to

166
00:05:31,629 --> 00:05:34,370
成组，它与磁盘上的实际目录结构没有关系，因此如果
actual directory structure on disk so if

167
00:05:34,569 --> 00:05:35,899
我回到Cheryl文件模式，那里有我的主要点CPP文件，我可以
I go back to Cheryl files mode there is

168
00:05:36,098 --> 00:05:37,939
我回到Cheryl文件模式，那里有我的主要点CPP文件，我可以
my main dot CPP file and I can do all of

169
00:05:38,139 --> 00:05:39,649
我的代码在这里，让我们继续，只需编写一个快速的hello world程序
my code here let's go ahead and just

170
00:05:39,848 --> 00:05:41,838
我的代码在这里，让我们继续，只需编写一个快速的hello world程序
write a quick hello world program just

171
00:05:42,038 --> 00:05:43,639
现在我们可以构建一些东西，我将对其进行编译，所以我对
something for us to build all right now

172
00:05:43,839 --> 00:05:44,809
现在我们可以构建一些东西，我将对其进行编译，所以我对
I'm going to compile this so I right

173
00:05:45,009 --> 00:05:46,999
单击我的项目构建，您会看到一切都会构建
click on my projects build and you can

174
00:05:47,199 --> 00:05:48,740
单击我的项目构建，您会看到一切都会构建
see that everything will build

175
00:05:48,939 --> 00:05:51,290
成功，所以我们是他的视觉工作室，如果我们看一下
successfully so we're his visual studio

176
00:05:51,490 --> 00:05:53,389
成功，所以我们是他的视觉工作室，如果我们看一下
put our exe file if we look at the

177
00:05:53,589 --> 00:05:55,009
输出，您可以看到它已经消失了，这是一个新项目并调试一个新项目
output you can see it's gone it's a new

178
00:05:55,209 --> 00:05:56,870
输出，您可以看到它已经消失了，这是一个新项目并调试一个新项目
project and debug a new project

179
00:05:57,069 --> 00:05:58,699
exe因此，如果我们打开我们的调试器，则调试为当前选择器配置
Exe so debug being our currently

180
00:05:58,899 --> 00:06:00,740
exe因此，如果我们打开我们的调试器，则调试为当前选择器配置
selector configuration if we open our

181
00:06:00,939 --> 00:06:03,259
再次使用文件资源管理器，我们将其签出，因此实际上您无法
file explorer once again and we check

182
00:06:03,459 --> 00:06:05,838
再次使用文件资源管理器，我们将其签出，因此实际上您无法
this out so it hasn't actually you can

183
00:06:06,038 --> 00:06:07,459
看到这里的调试文件夹与X显然在同一目录中
see that the debug folder here which is

184
00:06:07,658 --> 00:06:08,838
看到这里的调试文件夹与X显然在同一目录中
in the same directory as obviously X

185
00:06:09,038 --> 00:06:11,449
个人资料，但是如果我们打开它，我看不到这里没有新项目或Exe
profile however if we open it I don't

186
00:06:11,649 --> 00:06:13,879
个人资料，但是如果我们打开它，我看不到这里没有新项目或Exe
see no no new project or Exe in here

187
00:06:14,079 --> 00:06:15,528
对，所以这是怎么回事，我们首先注册了更多
right so what's going on here we

188
00:06:15,728 --> 00:06:16,639
对，所以这是怎么回事，我们首先注册了更多
registered in more first it looks like

189
00:06:16,839 --> 00:06:18,559
这只是一个新项目，然后调试，所以不在该项目文件夹中
it's just new project then debug so it's

190
00:06:18,759 --> 00:06:20,660
这只是一个新项目，然后调试，所以不在该项目文件夹中
not in that project folder hah

191
00:06:20,860 --> 00:06:22,550
有趣的让我们回到新项目，然后再次返回，然后进行调试
interesting let's go back to new project

192
00:06:22,750 --> 00:06:24,350
有趣的让我们回到新项目，然后再次返回，然后进行调试
and then go back again and then debug

193
00:06:24,550 --> 00:06:27,559
然后哦，在这里，视觉工作室多么奇怪，所以实际上是什么
and then oh it's in here how strange

194
00:06:27,759 --> 00:06:29,088
然后哦，在这里，视觉工作室多么奇怪，所以实际上是什么
visual studio so what's actually

195
00:06:29,288 --> 00:06:30,949
发生的事情是将我们的中间文件放入了我们的名为debug的文件夹中
happened is it put our intermediate

196
00:06:31,149 --> 00:06:34,069
发生的事情是将我们的中间文件放入了我们的名为debug的文件夹中
files into a folder called debug in our

197
00:06:34,269 --> 00:06:36,439
项目目录和实际的最终可执行二进制文件放入一个名为
project directory and our actual final

198
00:06:36,639 --> 00:06:39,079
项目目录和实际的最终可执行二进制文件放入一个名为
executable binary into a folder called

199
00:06:39,278 --> 00:06:39,500
在我们不知道的解决方案目录中调试
debug

200
00:06:39,699 --> 00:06:41,420
在我们不知道的解决方案目录中调试
in our solution directory I don't know

201
00:06:41,620 --> 00:06:42,710
谁来制作这些东西，真的很烦人，我没有
who makes this stuff up

202
00:06:42,910 --> 00:06:45,710
谁来制作这些东西，真的很烦人，我没有
it's really quite annoying I have not

203
00:06:45,910 --> 00:06:48,949
实际遇到了任何离开这些设置的专业开发人员
actually met any professional developer

204
00:06:49,149 --> 00:06:51,500
实际遇到了任何离开这些设置的专业开发人员
who leaves these settings everyone

205
00:06:51,699 --> 00:06:53,689
改变它们，因为老实说他们很奇怪，现在不是
changes them because they're just

206
00:06:53,889 --> 00:06:55,819
改变它们，因为老实说他们很奇怪，现在不是
honestly they are weird now it's not a

207
00:06:56,019 --> 00:06:57,259
要更改这些，确实是一个大问题，您可以创建模板和类似
really big problem to change these and

208
00:06:57,459 --> 00:06:58,670
要更改这些，确实是一个大问题，您可以创建模板和类似
you can create templates and stuff like

209
00:06:58,870 --> 00:07:00,949
那，但我并没有为决定这样做而过度烘烤微软，我只是
that but I'm not roasting Microsoft too

210
00:07:01,149 --> 00:07:02,780
那，但我并没有为决定这样做而过度烘烤微软，我只是
much for deciding to do this I'm just

211
00:07:02,980 --> 00:07:04,280
说这有点奇怪，特别是对于人们而言
saying that it's a little bit weird and

212
00:07:04,480 --> 00:07:05,780
说这有点奇怪，特别是对于人们而言
especially for people kind of starting

213
00:07:05,980 --> 00:07:07,129
谁可能不知道您可以更改此设置，您实际上可能会
out who might not know that you can

214
00:07:07,329 --> 00:07:09,740
谁可能不知道您可以更改此设置，您实际上可能会
change this you could actually be quite

215
00:07:09,939 --> 00:07:12,079
一旦构建它，就很难找到该死的可执行二进制文件，因此，如果
difficult to find your damn executable

216
00:07:12,279 --> 00:07:15,379
一旦构建它，就很难找到该死的可执行二进制文件，因此，如果
binary once you built it so anyway so if

217
00:07:15,579 --> 00:07:17,750
如果我们右键单击我们的项目，则返回数字工作室
we go back to the digital studio if we

218
00:07:17,949 --> 00:07:19,040
如果我们右键单击我们的项目，则返回数字工作室
right-click on our project and here

219
00:07:19,240 --> 00:07:21,800
属性，您会在我们的活动配置下看到活动
properties you'll see that on the active

220
00:07:22,000 --> 00:07:23,960
属性，您会在我们的活动配置下看到活动
under our active configuration of

221
00:07:24,160 --> 00:07:26,300
平台，我们有一个输出目录，我们有一个中间目录，因此
platform we have an output directory and

222
00:07:26,500 --> 00:07:27,829
平台，我们有一个输出目录，我们有一个中间目录，因此
we have an intermediate directory so

223
00:07:28,029 --> 00:07:28,670
那实际上是一切进行的地方，所以您可以看到一切
that's actually where everything is

224
00:07:28,870 --> 00:07:30,590
那实际上是一切进行的地方，所以您可以看到一切
going so you can see that everything

225
00:07:30,790 --> 00:07:32,000
我们在这里写的方式将相对于项目文件，所以如果
that we write here by the way is going

226
00:07:32,199 --> 00:07:35,270
我们在这里写的方式将相对于项目文件，所以如果
to be relative to the project file so if

227
00:07:35,470 --> 00:07:37,759
我把一些类似中间的东西放在这里相对于那个项目
I put something like intermediate here

228
00:07:37,959 --> 00:07:40,220
我把一些类似中间的东西放在这里相对于那个项目
is going to be relative to that project

229
00:07:40,420 --> 00:07:42,470
文件，因此与我们的项目文件（该VTX项目）位于同一目录中
file so in the same directory as our

230
00:07:42,670 --> 00:07:44,900
文件，因此与我们的项目文件（该VTX项目）位于同一目录中
project file which is this VTX project

231
00:07:45,100 --> 00:07:46,490
文件，在这种情况下，它实际上会创建一个名为中间体的文件夹，并且
file it will actually make a folder

232
00:07:46,689 --> 00:07:47,720
文件，在这种情况下，它实际上会创建一个名为中间体的文件夹，并且
called intermediates in this case and

233
00:07:47,920 --> 00:07:49,520
把它们都放好，所以我想将其更改为现在
put them all there ok so what I like to

234
00:07:49,720 --> 00:07:51,170
把它们都放好，所以我想将其更改为现在
change this to now first of all I like

235
00:07:51,370 --> 00:07:52,730
去所有配置，以便我编辑所有配置，而我们不
to go to all configuration so that I'm

236
00:07:52,930 --> 00:07:54,680
去所有配置，以便我编辑所有配置，而我们不
editing all configurations and we're not

237
00:07:54,879 --> 00:07:56,090
将使任何imma都在这里进行七个互换
going to make any any imma go seven

238
00:07:56,290 --> 00:07:58,280
将使任何imma都在这里进行七个互换
interchanges that are made here even all

239
00:07:58,480 --> 00:08:00,800
平台正常，然后我要对我喜欢的输出目录执行此操作
platforms ok and then what I like to do

240
00:08:01,000 --> 00:08:03,530
平台正常，然后我要对我喜欢的输出目录执行此操作
is this for my output directory I like

241
00:08:03,730 --> 00:08:05,720
将我的输出目录放在我的解决方案目录中
to put my output directory in my

242
00:08:05,920 --> 00:08:09,430
将我的输出目录放在我的解决方案目录中
solution directory slash bin slash

243
00:08:09,629 --> 00:08:12,710
平台，然后进行斜杠配置，然后以斜杠结尾
platform and then slash configuration

244
00:08:12,910 --> 00:08:15,319
平台，然后进行斜杠配置，然后以斜杠结尾
and then end that with a slash as well

245
00:08:15,519 --> 00:08:17,930
好的，所以在我们的例子中，将其放入解决方案目录中
ok so what it's going to do in our case

246
00:08:18,129 --> 00:08:19,879
好的，所以在我们的例子中，将其放入解决方案目录中
is put this into the solution directory

247
00:08:20,079 --> 00:08:22,009
这是整个事情的根源，这意味着什么，也是我的原因
which is the root of this whole thing

248
00:08:22,209 --> 00:08:24,259
这是整个事情的根源，这意味着什么，也是我的原因
what that means and the reason I've

249
00:08:24,459 --> 00:08:25,939
实际选择解决方案目录是因为如果我们有多个项目
actually chosen a solution directory is

250
00:08:26,139 --> 00:08:28,790
实际选择解决方案目录是因为如果我们有多个项目
because if we have multiple projects

251
00:08:28,990 --> 00:08:31,160
正确的例子，例如我们建立dll文件或我们的主程序需要的东西
right for example we build dll files or

252
00:08:31,360 --> 00:08:32,509
正确的例子，例如我们建立dll文件或我们的主程序需要的东西
something that are needed by our main

253
00:08:32,710 --> 00:08:34,579
应用程序，我们希望它们都在同一个文件夹中，我不想
application we want them all to be in

254
00:08:34,779 --> 00:08:36,289
应用程序，我们希望它们都在同一个文件夹中，我不想
the same folder right I don't want to

255
00:08:36,490 --> 00:08:38,329
必须进入每个项目文件夹，然后处理我只想要的所有
have to go into every project folder and

256
00:08:38,529 --> 00:08:40,250
必须进入每个项目文件夹，然后处理我只想要的所有
then deal with that I just want all of

257
00:08:40,450 --> 00:08:42,528
我构建的二进制文件放在一个地方，因为这通常是您想要的典型情况
my built binaries in one place because

258
00:08:42,729 --> 00:08:44,719
我构建的二进制文件放在一个地方，因为这通常是您想要的典型情况
that's usually the typical case you want

259
00:08:44,919 --> 00:08:46,609
它们都在那里，因此它们将位于我的解决方案目录中的一个文件夹中
them all there so they're going to be in

260
00:08:46,809 --> 00:08:48,259
它们都在那里，因此它们将位于我的解决方案目录中的一个文件夹中
my solution directory in a folder called

261
00:08:48,460 --> 00:08:50,539
在适当的平台文件夹中将哪种类型的二进制文件进行bin
bin which types of binaries in the

262
00:08:50,740 --> 00:08:52,519
在适当的平台文件夹中将哪种类型的二进制文件进行bin
appropriate platform folder so in our

263
00:08:52,720 --> 00:08:52,789
情况真的只会是win32或ax 64
case

264
00:08:52,990 --> 00:08:55,609
情况真的只会是win32或ax 64
really it will only be win32 or axe 64

265
00:08:55,809 --> 00:08:57,919
然后在配置中进行调试或释放
and then inside a configuration so

266
00:08:58,120 --> 00:09:00,139
然后在配置中进行调试或释放
either debug or release in our case now

267
00:09:00,339 --> 00:09:01,578
最后一部分将其放入每种配置的单独文件夹中
this last part putting it into a

268
00:09:01,778 --> 00:09:03,069
最后一部分将其放入每种配置的单独文件夹中
separate folder for each configuration

269
00:09:03,269 --> 00:09:05,599
再次不是我一直做的事情，有时我只是想改变
again not something that I do all the

270
00:09:05,799 --> 00:09:08,029
再次不是我一直做的事情，有时我只是想改变
time sometimes I just like to change the

271
00:09:08,230 --> 00:09:10,729
目标名称，以便在调试版本中附加调试信息
target name to maybe have debug appended

272
00:09:10,929 --> 00:09:11,899
目标名称，以便在调试版本中附加调试信息
to it for the debug build and

273
00:09:12,100 --> 00:09:13,819
可能的版本，或者如果发布版本完全不依赖它
potentially release or nothing at all

274
00:09:14,019 --> 00:09:15,469
可能的版本，或者如果发布版本完全不依赖它
depended to it for the release build if

275
00:09:15,669 --> 00:09:18,979
它会根据我如何处理项目而诚实地变化，但在这种情况下，
it varies honestly based on how I how I

276
00:09:19,179 --> 00:09:20,750
它会根据我如何处理项目而诚实地变化，但在这种情况下，
deal with my projects but in this case

277
00:09:20,950 --> 00:09:22,069
我只是要将它们放入一个单独的文件夹中
I'm just going to put them into a

278
00:09:22,269 --> 00:09:23,899
我只是要将它们放入一个单独的文件夹中
separate folder now intermediate

279
00:09:24,100 --> 00:09:25,459
目录将非常相似，我要输入，我要输入
directories are going to be very similar

280
00:09:25,659 --> 00:09:27,740
目录将非常相似，我要输入，我要输入
I'm going to put I'm going to I'm just

281
00:09:27,940 --> 00:09:29,120
要从输出目录复制所有内容，并将其放入中间目录
going to copy everything from the output

282
00:09:29,320 --> 00:09:30,289
要从输出目录复制所有内容，并将其放入中间目录
directory put it into the intermediate

283
00:09:30,490 --> 00:09:33,019
目录唯一的区别是在bin里面我要创建一个文件夹
directory the only difference is that

284
00:09:33,220 --> 00:09:35,479
目录唯一的区别是在bin里面我要创建一个文件夹
inside bin I'm going to make a folder

285
00:09:35,679 --> 00:09:38,929
称为中间体，然后通过配置再次将它们放在平台上
called intermediate and then put them

286
00:09:39,129 --> 00:09:41,089
称为中间体，然后通过配置再次将它们放在平台上
there by platform by configuration again

287
00:09:41,289 --> 00:09:42,828
有时候我把这个中间文件夹放在最后，所以已经
sometimes I put this intermediate folder

288
00:09:43,028 --> 00:09:43,969
有时候我把这个中间文件夹放在最后，所以已经
at the very end so there's already

289
00:09:44,169 --> 00:09:46,099
里面的一切，但是这样可以使您清洁一点
inside everything but this way it's just

290
00:09:46,299 --> 00:09:47,389
里面的一切，但是这样可以使您清洁一点
a little bit cleaner for you to be able

291
00:09:47,589 --> 00:09:49,250
复制保存所需的平台，并且仅包含所有已构建的二进制文件
to copy save the platform that you want

292
00:09:49,450 --> 00:09:51,258
复制保存所需的平台，并且仅包含所有已构建的二进制文件
and only have all the built binary is no

293
00:09:51,458 --> 00:09:52,879
中间文件，所以如果我现在认为还可以的话
intermediate files so if I now see it

294
00:09:53,080 --> 00:09:53,479
中间文件，所以如果我现在认为还可以的话
okay

295
00:09:53,679 --> 00:09:55,639
是的，我们可以清理我们的项目，这应该摆脱以前的很多
right we can clean our project which

296
00:09:55,839 --> 00:09:57,139
是的，我们可以清理我们的项目，这应该摆脱以前的很多
should get rid of a lot of the previous

297
00:09:57,339 --> 00:09:59,120
文件（如果不是），因为它永远无法很好地工作，我喜欢
files if not because it never really

298
00:09:59,320 --> 00:10:00,709
文件（如果不是），因为它永远无法很好地工作，我喜欢
does works out well I like to go into

299
00:10:00,909 --> 00:10:02,419
文件浏览器，然后删除所有我不在乎的文件
the file explorer and just delete all of

300
00:10:02,620 --> 00:10:05,029
文件浏览器，然后删除所有我不在乎的文件
these files that I don't care about so

301
00:10:05,230 --> 00:10:06,620
您可以看到所有调试目录，现在我什至将删除该bin目录
all the debug directory you can see that

302
00:10:06,820 --> 00:10:07,969
您可以看到所有调试目录，现在我什至将删除该bin目录
now I'll even delete this bin directory

303
00:10:08,169 --> 00:10:10,459
您可以看到，现在我们拥有的只是解决方案文件，即我们拥有的项目
you can see that now all we have is the

304
00:10:10,659 --> 00:10:12,258
您可以看到，现在我们拥有的只是解决方案文件，即我们拥有的项目
solution file the project we've got the

305
00:10:12,458 --> 00:10:14,000
项目过滤器和用户文件，基本上就是我们当前的Visual Studio
project filters and a user file which is

306
00:10:14,200 --> 00:10:15,229
项目过滤器和用户文件，基本上就是我们当前的Visual Studio
just basically our current Visual Studio

307
00:10:15,429 --> 00:10:17,389
配置，然后在源代码中，我们得到了很好的维护，干净
configuration and then in source we've

308
00:10:17,589 --> 00:10:18,828
配置，然后在源代码中，我们得到了很好的维护，干净
got maintance is nice and clean I'm

309
00:10:19,028 --> 00:10:20,120
回到Visual Studio，我会建立很好的效果，所以如果我们
going to go back to visual studio and

310
00:10:20,320 --> 00:10:23,209
回到Visual Studio，我会建立很好的效果，所以如果我们
I'm going to build alright cool so if we

311
00:10:23,409 --> 00:10:25,519
回到这里，我们进入bin，我们可以看到我们已经进入中间级，然后是win32，所以
go back here we go to bin we can see

312
00:10:25,720 --> 00:10:28,039
回到这里，我们进入bin，我们可以看到我们已经进入中间级，然后是win32，所以
we've got intermediate and then win32 so

313
00:10:28,240 --> 00:10:30,049
在intermedius合法的win32调试中，这是我们所有的中间文件
inside intermedius legal win32 debug

314
00:10:30,250 --> 00:10:31,459
在intermedius合法的win32调试中，这是我们所有的中间文件
that's all of our intermediate files

315
00:10:31,659 --> 00:10:33,740
例如我们的obj文件，然后如果我们返回，您会看到我们已经进行了win32调试
such as our obj files and then if we go

316
00:10:33,940 --> 00:10:35,990
例如我们的obj文件，然后如果我们返回，您会看到我们已经进行了win32调试
back you can see we've got win32 debug

317
00:10:36,190 --> 00:10:38,359
然后这就是我们的exe文件，我们可以运行它，一切都很好
and then that is our exe file and we can

318
00:10:38,559 --> 00:10:39,679
然后这就是我们的exe文件，我们可以运行它，一切都很好
run that and everything's fine

319
00:10:39,879 --> 00:10:41,389
好吧，一件事也很酷，您可以看到我们有一个双重
all right cool one little thing as well

320
00:10:41,589 --> 00:10:42,289
好吧，一件事也很酷，您可以看到我们有一个双重
you can see that we've got a double

321
00:10:42,490 --> 00:10:44,899
这里的反斜杠是因为我确实忘记了解决方案目录
backslash here because of course I did

322
00:10:45,100 --> 00:10:46,490
这里的反斜杠是因为我确实忘记了解决方案目录
forget that the solution directory

323
00:10:46,690 --> 00:10:48,469
像项目目录或解决方案目录这样的任何东西实际上都有一个
anything that has like project directory

324
00:10:48,669 --> 00:10:50,328
像项目目录或解决方案目录这样的任何东西实际上都有一个
or solution directory actually has a

325
00:10:50,528 --> 00:10:52,279
因为它是目录名，所以已经在其上加了反斜杠，所以只需返回并修复
backslash on it already because it's a

326
00:10:52,480 --> 00:10:54,589
因为它是目录名，所以已经在其上加了反斜杠，所以只需返回并修复
directory name so just go back and fix

327
00:10:54,789 --> 00:10:56,029
这样就可以了，如果您不确定哪个是
that up so that we don't get a double

328
00:10:56,230 --> 00:10:57,799
这样就可以了，如果您不确定哪个是
backslash if you're not sure what one of

329
00:10:58,000 --> 00:11:00,019
这些宏等于或您想看看它是什么，您可以直接执行
these macros is equal to or you would

330
00:11:00,220 --> 00:11:02,719
这些宏等于或您想看看它是什么，您可以直接执行
like to see what it is you can just go

331
00:11:02,919 --> 00:11:04,339
在这里进行编辑，然后我们可以展开该映射器视图，
under edit here

332
00:11:04,539 --> 00:11:06,139
在这里进行编辑，然后我们可以展开该映射器视图，
then we can expand this mappers view and

333
00:11:06,340 --> 00:11:07,879
然后在这里，我们拥有了我们想要的一切，例如该解决方案
then over here we've got everything we

334
00:11:08,080 --> 00:11:09,169
然后在这里，我们拥有了我们想要的一切，例如该解决方案
want to for example that solution

335
00:11:09,370 --> 00:11:11,329
目录我在说什么，您可以看到它是CDF新项目，您可以
directory what I was talking about you

336
00:11:11,529 --> 00:11:13,189
目录我在说什么，您可以看到它是CDF新项目，您可以
can see it's CDF new project and you can

337
00:11:13,389 --> 00:11:14,599
其实看一下delve并加上反斜杠就可以了，所以差不多了
see delve in fact and with a backslash

338
00:11:14,799 --> 00:11:16,279
其实看一下delve并加上反斜杠就可以了，所以差不多了
all right cool so that's pretty much it

339
00:11:16,480 --> 00:11:17,839
我只想说，设置目录非常重要
that's all I just wanted to say setting

340
00:11:18,039 --> 00:11:19,399
我只想说，设置目录非常重要
up your directory is really important

341
00:11:19,600 --> 00:11:21,379
快速的提示，将使您的生活更轻松
really quick tip that will just kind of

342
00:11:21,580 --> 00:11:22,549
快速的提示，将使您的生活更轻松
make your life easier

343
00:11:22,750 --> 00:11:24,709
就组织而言，这些是我一直使用的设置
as far as organization goes these are

344
00:11:24,909 --> 00:11:26,689
就组织而言，这些是我一直使用的设置
the settings that I use all the time we

345
00:11:26,889 --> 00:11:28,159
可能会谈论或多或少的编译器设置和链接设置
might talk about more or less the

346
00:11:28,360 --> 00:11:29,899
可能会谈论或多或少的编译器设置和链接设置
compiler settings and linking settings

347
00:11:30,100 --> 00:11:31,549
而其他所有可能发布的版本可能会在另一个
and all other kind of maybe release

348
00:11:31,750 --> 00:11:33,169
而其他所有可能发布的版本可能会在另一个
might optimization settings in another

349
00:11:33,370 --> 00:11:35,089
视频我只是想向大家展示我如何设置每个新超级游戏的基础
video I just wanted to show you guys the

350
00:11:35,289 --> 00:11:37,309
视频我只是想向大家展示我如何设置每个新超级游戏的基础
basis of how I setup every new super

351
00:11:37,509 --> 00:11:38,809
我喜欢的酱汁项目，你们喜欢这个视频，请点击
sauce project that I do do you guys

352
00:11:39,009 --> 00:11:40,219
我喜欢的酱汁项目，你们喜欢这个视频，请点击
enjoy this video please hit that like

353
00:11:40,419 --> 00:11:41,779
按钮在Twitter和Instagram上关注我，我会看到你们
button follow me on Twitter and

354
00:11:41,980 --> 00:11:47,750
按钮在Twitter和Instagram上关注我，我会看到你们
Instagram and and I will see you guys in

355
00:11:47,950 --> 00:11:50,750
下一个视频再见[音乐]
the next video goodbye

356
00:11:50,950 --> 00:11:55,079
下一个视频再见[音乐]
[Music]

357
00:11:59,470 --> 00:12:04,470
[音乐]
[Music]

