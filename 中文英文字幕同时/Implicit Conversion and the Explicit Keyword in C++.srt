﻿1
00:00:00,000 --> 00:00:01,660
嘿，大家好，我叫切达干酪
hey what's up guys my name is the

2
00:00:01,860 --> 00:00:03,609
嘿，大家好，我叫切达干酪
cheddar waving my arms around welcome

3
00:00:03,810 --> 00:00:05,799
回到我的说加加系列今天，我们将讨论所有关于隐式
back to my say plus plus series today

4
00:00:06,000 --> 00:00:07,899
回到我的说加加系列今天，我们将讨论所有关于隐式
we're going to talk all about implicit

5
00:00:08,099 --> 00:00:10,450
构造C ++隐式转换以及显式关键字
construction C++ implicit conversions as

6
00:00:10,650 --> 00:00:12,370
构造C ++隐式转换以及显式关键字
well as what the explicit keyword

7
00:00:12,570 --> 00:00:15,309
实际上意味着如此隐式的转换，再加上我在说什么
actually means so implicit conversion

8
00:00:15,509 --> 00:00:16,359
实际上意味着如此隐式的转换，再加上我在说什么
plus it concern what am I talking about

9
00:00:16,559 --> 00:00:18,640
隐式意味着无需您明确告诉它该怎么做
implicit means kind of without you

10
00:00:18,839 --> 00:00:20,620
隐式意味着无需您明确告诉它该怎么做
explicitly telling it what to do so it

11
00:00:20,820 --> 00:00:22,749
在这种情况下，这种工作方式意味着什么
kind of automatic that's what that work

12
00:00:22,949 --> 00:00:24,609
在这种情况下，这种工作方式意味着什么
kind of means in this context and what's

13
00:00:24,809 --> 00:00:25,839
实际上允许总线执行编译器允许执行的操作是
the bus bus is actually allowed to do

14
00:00:26,039 --> 00:00:27,280
实际上允许总线执行编译器允许执行的操作是
what the compiler is allowed to do is

15
00:00:27,480 --> 00:00:30,010
对您的代码执行一次隐式转换，因为如果我开始使用
perform one implicit conversion on your

16
00:00:30,210 --> 00:00:31,870
对您的代码执行一次隐式转换，因为如果我开始使用
code because if I kind of start using

17
00:00:32,070 --> 00:00:34,239
一种数据类型与另一种数据类型之间存在转换，并且存在C ++
one data type as another and a

18
00:00:34,439 --> 00:00:36,399
一种数据类型与另一种数据类型之间存在转换，并且存在C ++
conversion between the two exists C++

19
00:00:36,600 --> 00:00:37,809
实际上将隐式执行该转换，而无需强制转换
will actually implicitly perform that

20
00:00:38,009 --> 00:00:40,089
实际上将隐式执行该转换，而无需强制转换
conversion without you having to cast it

21
00:00:40,289 --> 00:00:40,809
或诸如此类的视频或即将投放的人
or anything like that

22
00:00:41,009 --> 00:00:42,640
或诸如此类的视频或即将投放的人
whoever video and casting very soon

23
00:00:42,840 --> 00:00:44,259
强制转换是我们要做的从一种数据类型转换为另一种数据类型的方法，因此这是
casting is what we do to kind of convert

24
00:00:44,460 --> 00:00:46,509
强制转换是我们要做的从一种数据类型转换为另一种数据类型的方法，因此这是
from one data type to another so this is

25
00:00:46,710 --> 00:00:48,428
最好的例子展示，让我们看一下我要在这里写一个类
best shown with an example so let's take

26
00:00:48,628 --> 00:00:50,169
最好的例子展示，让我们看一下我要在这里写一个类
a look I'm going to write a class here

27
00:00:50,369 --> 00:00:52,509
它将被称为实体，它将具有一个私有字符串名称
it's going to be called entity it's

28
00:00:52,710 --> 00:00:56,529
它将被称为实体，它将具有一个私有字符串名称
going to have a private string name you

29
00:00:56,729 --> 00:00:57,640
伙计们知道我做过的练习，然后再增加年龄
guys know the drill I've done this

30
00:00:57,840 --> 00:01:00,219
伙计们知道我做过的练习，然后再增加年龄
before I'm also going to add an age to

31
00:01:00,420 --> 00:01:02,739
这将创建一个NC构造函数，该构造函数采用我编写Const St字符串的名称
this will create an NC constructor which

32
00:01:02,939 --> 00:01:05,769
这将创建一个NC构造函数，该构造函数采用我编写Const St字符串的名称
takes in a name I write Const St string

33
00:01:05,969 --> 00:01:09,579
参考名称巴勒斯坦的名字也要在这里创建另一个构造函数
reference name Palestine name to name

34
00:01:09,780 --> 00:01:11,349
参考名称巴勒斯坦的名字也要在这里创建另一个构造函数
here also create another constructor

35
00:01:11,549 --> 00:01:15,488
这需要一个年龄，我将在此处签名以确认未知年龄。
which takes in an age I'll sign name

36
00:01:15,688 --> 00:01:20,769
这需要一个年龄，我将在此处签名以确认未知年龄。
here to be unknown an age to be age I

37
00:01:20,969 --> 00:01:23,319
当然也应该在此指定年龄，我们将年龄设置为负数
should definitely also assign age over

38
00:01:23,519 --> 00:01:25,988
当然也应该在此指定年龄，我们将年龄设置为负数
here we'll set age to be negative one

39
00:01:26,188 --> 00:01:28,539
这意味着它是无效的，基本上没有提供年龄，好吧，所以我们有
meaning that it's invalid basically no

40
00:01:28,739 --> 00:01:30,789
这意味着它是无效的，基本上没有提供年龄，好吧，所以我们有
age was supplied okay nice so we have

41
00:01:30,989 --> 00:01:32,769
这里的两个构造函数非常简单
two constructors here which are pretty

42
00:01:32,969 --> 00:01:33,399
这里的两个构造函数非常简单
simple

43
00:01:33,599 --> 00:01:34,750
因此，您可能会创建这些对象的通常方式是
so the normal way that you would

44
00:01:34,950 --> 00:01:36,099
因此，您可能会创建这些对象的通常方式是
probably create these objects is by

45
00:01:36,299 --> 00:01:38,890
写一个像实体a这样的东西可能会给你一个像糖和GB这样的名字
writing something like entity a can

46
00:01:39,090 --> 00:01:41,409
写一个像实体a这样的东西可能会给你一个像糖和GB这样的名字
maybe give you a name like sugar and GB

47
00:01:41,609 --> 00:01:43,659
也许在代理商222中给这个一个，实际上我的年龄是多少
maybe giving this one in agents 222

48
00:01:43,859 --> 00:01:44,948
也许在代理商222中给这个一个，实际上我的年龄是多少
that's actually how old I am for those

49
00:01:45,149 --> 00:01:46,000
你们想知道，因为某些原因我经常问这个问题，而你
of you wondering because I get that

50
00:01:46,200 --> 00:01:47,590
你们想知道，因为某些原因我经常问这个问题，而你
question a lot for some reason and you

51
00:01:47,790 --> 00:01:49,238
会花费你一天的时间，不要三思而后行，这看起来很简单
would kind of go about your day and not

52
00:01:49,438 --> 00:01:51,039
会花费你一天的时间，不要三思而后行，这看起来很简单
think twice and this looks pretty simple

53
00:01:51,239 --> 00:01:52,840
也许如果您想使用等号，就可以像这样编写代码
maybe if you want to use equals you

54
00:01:53,040 --> 00:01:56,140
也许如果您想使用等号，就可以像这样编写代码
would write your code like this and it

55
00:01:56,340 --> 00:01:57,640
看起来很正常这是大多数人使用物体的方式以及有多少人
kind of looks normal this is how most

56
00:01:57,840 --> 00:01:59,799
看起来很正常这是大多数人使用物体的方式以及有多少人
people use objects and how much people

57
00:02:00,000 --> 00:02:02,649
实例化对象，但是很多人不知道的事情是
instantiate objects but what you can do

58
00:02:02,849 --> 00:02:04,929
实例化对象，但是很多人不知道的事情是
which a lot of people don't know is just

59
00:02:05,129 --> 00:02:07,750
基本上说n CA等于Turner或NTP是
basically say that n CA is equal to

60
00:02:07,950 --> 00:02:10,750
基本上说n CA等于Turner或NTP是
Turner or NTP is

61
00:02:10,949 --> 00:02:13,960
等于22现在这有点奇怪，因为首先
equal to 22 now this is a little bit

62
00:02:14,159 --> 00:02:15,759
等于22现在这有点奇怪，因为首先
weird because well first of all you

63
00:02:15,959 --> 00:02:17,080
在其他语言（例如Java或C尖锐语言）中无法做到这一点，但仅次于实体
can't do this in other languages like

64
00:02:17,280 --> 00:02:20,110
在其他语言（例如Java或C尖锐语言）中无法做到这一点，但仅次于实体
Java or C sharp but second of all entity

65
00:02:20,310 --> 00:02:21,520
B等于22什么是有趣的整数我的意思是North
B equals 22

66
00:02:21,719 --> 00:02:23,830
B等于22什么是有趣的整数我的意思是North
what is interesting integer I mean North

67
00:02:24,030 --> 00:02:26,500
有字符串，有名字，但我可以指定22这是怎么回事
is got string and it's got a name but I

68
00:02:26,699 --> 00:02:29,259
有字符串，有名字，但我可以指定22这是怎么回事
can assign 22 what's going on here this

69
00:02:29,459 --> 00:02:31,030
称为隐式转换或非法构造，它是隐式转换
is called implicit conversion or illicit

70
00:02:31,229 --> 00:02:32,980
称为隐式转换或非法构造，它是隐式转换
construction it's implicitly converting

71
00:02:33,180 --> 00:02:35,170
那22变成一个公理，用它构造一个实体，因为有一个
that 22 into an axiom constructing an

72
00:02:35,370 --> 00:02:36,939
那22变成一个公理，用它构造一个实体，因为有一个
entity out of it because there's a

73
00:02:37,139 --> 00:02:39,099
构造函数幻想，它接受整数年龄权，并且有一个
constructor fantasy which takes in an

74
00:02:39,299 --> 00:02:41,080
构造函数幻想，它接受整数年龄权，并且有一个
integer age right and there's a

75
00:02:41,280 --> 00:02:42,939
带有名称的构造函数幻想检查另一个示例，其中
constructor fantasy which takes in a

76
00:02:43,139 --> 00:02:45,219
带有名称的构造函数幻想检查另一个示例，其中
name check another example where you

77
00:02:45,419 --> 00:02:47,050
可能会说这也许是您有一个带实体的print st函数，并且
might say this is maybe you have a print

78
00:02:47,250 --> 00:02:49,689
可能会说这也许是您有一个带实体的print st函数，并且
st function which takes in an entity and

79
00:02:49,889 --> 00:02:51,819
然后也许做一些很酷的打印工作，实际上您可以在这里打电话
then maybe does some cool printing stuff

80
00:02:52,019 --> 00:02:54,730
然后也许做一些很酷的打印工作，实际上您可以在这里打电话
what you can actually do here is call

81
00:02:54,930 --> 00:02:58,450
右边有22的函数，看起来很奇怪，因为我们将挂在
that function with 22 right and that

82
00:02:58,650 --> 00:03:00,069
右边有22的函数，看起来很奇怪，因为我们将挂在
looks weird because we'll hang on a

83
00:03:00,269 --> 00:03:00,490
分钟，我们没有过多的打印和
minute

84
00:03:00,689 --> 00:03:02,289
分钟，我们没有过多的打印和
we don't have an overload for print and

85
00:03:02,489 --> 00:03:05,230
T接受一个整数或类似的东西，我们只得到一个
T which takes in an integer or something

86
00:03:05,430 --> 00:03:06,910
T接受一个整数或类似的东西，我们只得到一个
like that we've just got one that takes

87
00:03:07,110 --> 00:03:09,460
在实体中，但请记住，就c ++而言，22可以转换为
in an entity but remember as far as c++

88
00:03:09,659 --> 00:03:12,129
在实体中，但请记住，就c ++而言，22可以转换为
is concerned 22 can be converted into an

89
00:03:12,329 --> 00:03:14,080
实体，因为您可以调用此构造函数，然后突然从22开始
entity because you can call this

90
00:03:14,280 --> 00:03:16,840
实体，因为您可以调用此构造函数，然后突然从22开始
constructor and suddenly from 22 which

91
00:03:17,039 --> 00:03:18,430
是您使实体成为唯一参数，现在可以观察当我尝试
is the only parameter you've made an

92
00:03:18,629 --> 00:03:20,530
是您使实体成为唯一参数，现在可以观察当我尝试
entity now watch what happens when I try

93
00:03:20,729 --> 00:03:22,810
并使用Cherno调用此打印实体函数，您可能会假设它
and call this print entity function with

94
00:03:23,009 --> 00:03:24,099
并使用Cherno调用此打印实体函数，您可能会假设它
Cherno you would kind of assume it to

95
00:03:24,299 --> 00:03:26,140
工作正常，因为这样做不行，也不行，原因不行
work right because this did no it

96
00:03:26,340 --> 00:03:27,789
工作正常，因为这样做不行，也不行，原因不行
doesn't work and the reason it didn't

97
00:03:27,989 --> 00:03:29,530
工作是因为此Churner字符串实际上不是STD字符串，而是
work is because this churner string

98
00:03:29,729 --> 00:03:32,560
工作是因为此Churner字符串实际上不是STD字符串，而是
isn't actually an STD string it is a

99
00:03:32,759 --> 00:03:35,379
char数组，这是一个Const，我们在其中显示了七个字符的数组，并且
char array it's a Const our array of

100
00:03:35,579 --> 00:03:37,330
char数组，这是一个Const，我们在其中显示了七个字符的数组，并且
seven characters we've got shown on and

101
00:03:37,530 --> 00:03:39,129
然后是一个旧的终止符，如果你们不知道字符串如何工作
then an old termination character if you

102
00:03:39,329 --> 00:03:41,170
然后是一个旧的终止符，如果你们不知道字符串如何工作
guys don't know how strings work link up

103
00:03:41,370 --> 00:03:42,819
他们在下面的说明中的链接谢谢所有地方去检查该视频
their link in the description below

104
00:03:43,019 --> 00:03:44,680
他们在下面的说明中的链接谢谢所有地方去检查该视频
thanks everywhere go check that video

105
00:03:44,879 --> 00:03:46,868
出了非常有用的东西，因此为了使它起作用，C ++实际上需要
out a very very useful stuff so in order

106
00:03:47,068 --> 00:03:48,849
出了非常有用的东西，因此为了使它起作用，C ++实际上需要
for this to work C++ would actually have

107
00:03:49,049 --> 00:03:51,879
进行两次转换，一次是从Const RA转换为字符串，然后是一次转换
to do two conversions one from a Const

108
00:03:52,079 --> 00:03:54,610
进行两次转换，一次是从Const RA转换为字符串，然后是一次转换
RA into a string and then one from a

109
00:03:54,810 --> 00:03:56,259
字符串到实体，并且只允许进行一次隐式转换，因此
string to an entity and it's only

110
00:03:56,459 --> 00:03:59,230
字符串到实体，并且只允许进行一次隐式转换，因此
allowed to do one implicit conversion so

111
00:03:59,430 --> 00:04:00,670
为此，我们必须将其包装在这样的构造函数中
for this to work we would have to either

112
00:04:00,870 --> 00:04:04,060
为此，我们必须将其包装在这样的构造函数中
wrap this inside a constructor like this

113
00:04:04,259 --> 00:04:06,310
字符串，然后突然工作正常，或者我们甚至可以将其包装在
the string and suddenly this works fine

114
00:04:06,509 --> 00:04:09,159
字符串，然后突然工作正常，或者我们甚至可以将其包装在
or we could even wrap it just in an

115
00:04:09,359 --> 00:04:10,330
实体，它也会正常工作，因为在这种情况下，
entity and that would work as well

116
00:04:10,530 --> 00:04:11,800
实体，它也会正常工作，因为在这种情况下，
because in this case it would be

117
00:04:12,000 --> 00:04:13,840
将该字符串隐式转换为STD字符串转换为
implicitly converting this string into a

118
00:04:14,039 --> 00:04:15,700
将该字符串隐式转换为STD字符串转换为
string into an STD string into a

119
00:04:15,900 --> 00:04:17,829
标准字符串，然后将其推送到HD构造函数中，
standard string and then that will be

120
00:04:18,029 --> 00:04:19,759
标准字符串，然后将其推送到HD构造函数中，
pushed into the HD constructor and

121
00:04:19,959 --> 00:04:21,619
构造函数可用好酷，令人印象深刻的构造很酷
constructor available okay cool that's

122
00:04:21,819 --> 00:04:22,790
构造函数可用好酷，令人印象深刻的构造很酷
impressive construction pretty cool

123
00:04:22,990 --> 00:04:24,800
东西可以帮助简化很多个人代码，我尽量避免使用
stuff can help simplify your code a lot

124
00:04:25,000 --> 00:04:26,718
东西可以帮助简化很多个人代码，我尽量避免使用
personally I try to avoid it as much as

125
00:04:26,918 --> 00:04:29,869
可能的，但在某些情况下，当您进行分配时可以得到它
possible except for some some cases when

126
00:04:30,069 --> 00:04:31,999
可能的，但在某些情况下，当您进行分配时可以得到它
you're kind of assigning it can get it

127
00:04:32,199 --> 00:04:34,160
可以只是简单的哦，所以您不必将其与构造函数一起包装
can kind of just simply oh so you're not

128
00:04:34,360 --> 00:04:36,170
可以只是简单的哦，所以您不必将其与构造函数一起包装
wrapping it with constructors all the

129
00:04:36,370 --> 00:04:38,838
时间，但总的来说，我不会像这样分配这个实体b22
time but in general I wouldn't be

130
00:04:39,038 --> 00:04:41,889
时间，但总的来说，我不会像这样分配这个实体b22
assigning this entity b22 like this I

131
00:04:42,089 --> 00:04:43,999
个人会写这样的代码，因为我认为
personally would write code like this

132
00:04:44,199 --> 00:04:46,069
个人会写这样的代码，因为我认为
instead because in my opinion it just

133
00:04:46,269 --> 00:04:47,660
看起来更清晰了，现在让我们讨论一下显式关键字是什么
looks a bit more clear now let's talk

134
00:04:47,860 --> 00:04:49,189
看起来更清晰了，现在让我们讨论一下显式关键字是什么
about what the explicit keyword is

135
00:04:49,389 --> 00:04:51,259
因为它与此非常相关，因此显式禁用了简单的类比
because it's very very relevant to this

136
00:04:51,459 --> 00:04:53,960
因为它与此非常相关，因此显式禁用了简单的类比
explicit disables the simplistic analogy

137
00:04:54,160 --> 00:04:55,399
显式关键字是您放在构造函数前面的内容，如果
the explicit keyword is something that

138
00:04:55,598 --> 00:04:57,199
显式关键字是您放在构造函数前面的内容，如果
you put in front of a constructor and if

139
00:04:57,399 --> 00:04:59,569
您编写了一个显式构造函数，这意味着不，不，不，不，隐，
you write an explicit constructor it

140
00:04:59,769 --> 00:05:02,210
您编写了一个显式构造函数，这意味着不，不，不，不，隐，
means that no no no no no implicit

141
00:05:02,410 --> 00:05:04,490
如果要转换此构造函数必须进行显式调用
conversions this constructor must

142
00:05:04,689 --> 00:05:06,468
如果要转换此构造函数必须进行显式调用
explicitly be called if you want to

143
00:05:06,668 --> 00:05:08,360
例如，使用整数构造此实体对象，让我们回到
construct this entity object with an

144
00:05:08,560 --> 00:05:11,689
例如，使用整数构造此实体对象，让我们回到
integer for example so let's go back to

145
00:05:11,889 --> 00:05:13,670
如果我在这里改变了这个int年龄，那么在这里进行这种隐式转换
having this implicit conversion here if

146
00:05:13,870 --> 00:05:16,730
如果我在这里改变了这个int年龄，那么在这里进行这种隐式转换
I go up here and I change this int age

147
00:05:16,930 --> 00:05:18,709
通过显式地将显式构造器显式化，您将看到
constructor to be explicit by sticking

148
00:05:18,908 --> 00:05:20,689
通过显式地将显式构造器显式化，您将看到
explicit out the front you'll see that

149
00:05:20,889 --> 00:05:23,540
这些鸟突然失败了，如果我想这样做，我就不能再这样做了
these birds suddenly fail I cannot do

150
00:05:23,740 --> 00:05:25,819
这些鸟突然失败了，如果我想这样做，我就不能再这样做了
that anymore if I want to do this I need

151
00:05:26,019 --> 00:05:28,129
编写这样的代码或通过以下方式将其显式转换为实体
to either write code like that

152
00:05:28,329 --> 00:05:30,259
编写这样的代码或通过以下方式将其显式转换为实体
or explicitly cast it to an entity by

153
00:05:30,459 --> 00:05:32,088
这样做，我们将讨论传递另一个视频，这将是一个链接
doing this we'll talk about passing in

154
00:05:32,288 --> 00:05:33,350
这样做，我们将讨论传递另一个视频，这将是一个链接
another video it'll be a link up there

155
00:05:33,550 --> 00:05:35,059
在下面的说明中，我们当然可以只称该实体
and in description below as well all we

156
00:05:35,259 --> 00:05:36,588
在下面的说明中，我们当然可以只称该实体
can of course just call that entity

157
00:05:36,788 --> 00:05:38,629
这样的构造函数也可以，所以这些是我们的选择，但我们可以
constructor like this as well okay so

158
00:05:38,829 --> 00:05:41,028
这样的构造函数也可以，所以这些是我们的选择，但我们可以
those are kind of our options but we can

159
00:05:41,228 --> 00:05:43,579
不再只是像这样隐式地调用代码并期望它能工作，这不是
no longer just implicitly call code like

160
00:05:43,779 --> 00:05:44,749
不再只是像这样隐式地调用代码并期望它能工作，这不是
this and expect it to work it's not

161
00:05:44,949 --> 00:05:46,189
将会发生，当然，如果我们为此构造函数编写显式的
going to happen and of course if we

162
00:05:46,389 --> 00:05:49,100
将会发生，当然，如果我们为此构造函数编写显式的
write explicit for this constructor

163
00:05:49,300 --> 00:05:50,540
当然，这需要输入字符串，否则不会失败
which takes in string then of course

164
00:05:50,740 --> 00:05:52,550
当然，这需要输入字符串，否则不会失败
this is going to fail as well not this

165
00:05:52,750 --> 00:05:54,588
一个，只是因为我们当然仍在实际调用实体
one just yet because we are still of

166
00:05:54,788 --> 00:05:56,269
一个，只是因为我们当然仍在实际调用实体
course actually calling the entity

167
00:05:56,468 --> 00:05:58,278
构造函数，但像这样的任何其他隐式失败，那就是
constructor but everything else that is

168
00:05:58,478 --> 00:06:00,439
构造函数，但像这样的任何其他隐式失败，那就是
implicit like this one fails and that is

169
00:06:00,639 --> 00:06:01,850
确实，该显式关键字的唯一功能是在您需要时
really the only function of that

170
00:06:02,050 --> 00:06:03,680
确实，该显式关键字的唯一功能是在您需要时
explicit keyword it's for when you want

171
00:06:03,879 --> 00:06:05,449
您的构造函数被显式调用，而不是允许
your constructors to be explicitly

172
00:06:05,649 --> 00:06:07,278
您的构造函数被显式调用，而不是允许
called instead of allowing the sibyl's

173
00:06:07,478 --> 00:06:09,649
编译器基本上可以将任何整数隐式转换为实体
compiler to implicitly convert any

174
00:06:09,848 --> 00:06:11,899
编译器基本上可以将任何整数隐式转换为实体
integer into entity by just basically

175
00:06:12,098 --> 00:06:13,999
每次尝试进行此操作时都调用此构造函数，这就是
calling this constructor every time you

176
00:06:14,199 --> 00:06:15,649
每次尝试进行此操作时都调用此构造函数，这就是
try and do that okay so that's what

177
00:06:15,848 --> 00:06:17,329
隐含的是显式的，希望这会起到一些作用
implicit is that's what explicit is

178
00:06:17,529 --> 00:06:19,129
隐含的是显式的，希望这会起到一些作用
hopefully this plays some stuff up as

179
00:06:19,329 --> 00:06:20,899
因为当你想使用它的时候，我有时会在诸如数学之类的东西上使用显式的
for when you want to use this I use

180
00:06:21,098 --> 00:06:23,420
因为当你想使用它的时候，我有时会在诸如数学之类的东西上使用显式的
explicit sometimes for things like math

181
00:06:23,620 --> 00:06:25,699
库，如果我真的不想将数字与向量进行比较
libraries if I really don't want to be

182
00:06:25,899 --> 00:06:27,800
库，如果我真的不想将数字与向量进行比较
comparing numbers to vectors

183
00:06:28,000 --> 00:06:29,660
时间，我只想确保我的代码尽可能安全
time and I want to just ensure that my

184
00:06:29,860 --> 00:06:31,189
时间，我只想确保我的代码尽可能安全
code is as safe as possible

185
00:06:31,389 --> 00:06:32,780
老实说，我在写作低级课程时不会经常使用它
honestly I don't find myself using it

186
00:06:32,980 --> 00:06:34,759
老实说，我在写作低级课程时不会经常使用它
too often when you're writing low-level

187
00:06:34,959 --> 00:06:37,189
说唱歌手或类似的东西可能会派上用场，并可能阻止您
rappers or things like that it can come

188
00:06:37,389 --> 00:06:38,660
说唱歌手或类似的东西可能会派上用场，并可能阻止您
in handy and can prevent you from

189
00:06:38,860 --> 00:06:40,639
意外投掷东西并导致性能问题或嗡嗡声
accidentally casting things and causing

190
00:06:40,839 --> 00:06:42,860
意外投掷东西并导致性能问题或嗡嗡声
you the performance issues or buzz will

191
00:06:43,060 --> 00:06:44,840
如果我们碰到将来可能会谈论一些具体的例子
kind of maybe talk about specific

192
00:06:45,040 --> 00:06:46,460
如果我们碰到将来可能会谈论一些具体的例子
examples in the future if we run into

193
00:06:46,660 --> 00:06:48,050
它们，但这是您可能不必担心的那些事情之一
them but it's one of those things that

194
00:06:48,250 --> 00:06:48,920
它们，但这是您可能不必担心的那些事情之一
you probably don't need to worry about

195
00:06:49,120 --> 00:06:50,960
太多的只是要知道这个关键字，它可以为您和大客户做什么
too much just be aware of this keyword

196
00:06:51,160 --> 00:06:52,790
太多的只是要知道这个关键字，它可以为您和大客户做什么
and what it can do for you and the big

197
00:06:52,990 --> 00:06:54,530
我认为从这段视频中获得的最大收获就是意识到
the big take away I think from this

198
00:06:54,730 --> 00:06:56,810
我认为从这段视频中获得的最大收获就是意识到
video is just be aware that implicit

199
00:06:57,009 --> 00:06:58,850
建设存在，希望你们喜欢这个视频，如果你打
construction exists hope you guys enjoy

200
00:06:59,050 --> 00:07:00,439
建设存在，希望你们喜欢这个视频，如果你打
this video if you did you hit that like

201
00:07:00,639 --> 00:07:02,030
按钮，您还可以帮助支持该系列，并通过
button you can also help support this

202
00:07:02,230 --> 00:07:03,860
按钮，您还可以帮助支持该系列，并通过
series and make it even better by going

203
00:07:04,060 --> 00:07:05,990
回家为这样的频道服务，但他们并没有减少对您的支持
to patreon home for such the channel but

204
00:07:06,189 --> 00:07:07,340
回家为这样的频道服务，但他们并没有减少对您的支持
they don't reduce support the series you

205
00:07:07,540 --> 00:07:08,900
确实可以获得一些很酷的奖励，例如能够尽早看到这些出口
do get some pretty cool rewards such as

206
00:07:09,100 --> 00:07:10,370
确实可以获得一些很酷的奖励，例如能够尽早看到这些出口
being able to see these exits early

207
00:07:10,569 --> 00:07:11,900
基本上，只要我完成了对它们的编辑以及对
basically as soon as I'm done editing

208
00:07:12,100 --> 00:07:13,850
基本上，只要我完成了对它们的编辑以及对
them as well as contribute to the

209
00:07:14,050 --> 00:07:15,410
讨论如果将来有这些录像带
discussion of what goes into these

210
00:07:15,610 --> 00:07:17,060
讨论如果将来有这些录像带
videos in the future if there's

211
00:07:17,259 --> 00:07:18,110
该视频中您不了解的内容可以在下面发表评论
something in this video that you didn't

212
00:07:18,310 --> 00:07:19,400
该视频中您不了解的内容可以在下面发表评论
understand you can leave a comment below

213
00:07:19,600 --> 00:07:20,870
我会尽力回答，但我上面也有话语
and I'll try and reply to as many as I

214
00:07:21,069 --> 00:07:22,819
我会尽力回答，但我上面也有话语
can but I also have a discourse above

215
00:07:23,019 --> 00:07:24,829
下面的描述中的链接，您可以在其中谈论这种事情
link in the description below in which

216
00:07:25,029 --> 00:07:26,360
下面的描述中的链接，您可以在其中谈论这种事情
you could talk about this kind of stuff

217
00:07:26,560 --> 00:07:28,430
与少数人一起，我认为700种族中大约有200人
with with a handful of people I think

218
00:07:28,629 --> 00:07:30,199
与少数人一起，我认为700种族中大约有200人
there's like 200 people in the 700 race

219
00:07:30,399 --> 00:07:31,699
绝对很酷，请在下面的说明中加入他们的链接，我将
that's pretty cool definitely join up

220
00:07:31,899 --> 00:07:33,920
绝对很酷，请在下面的说明中加入他们的链接，我将
their link in description below and I'll

221
00:07:34,120 --> 00:07:37,590
下个视频再见，再见
see you guys in the next video goodbye

222
00:07:37,720 --> 00:07:42,250
[音乐]
[Music]

223
00:07:45,220 --> 00:07:50,220
[音乐]
[Music]

