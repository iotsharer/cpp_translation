﻿1
00:00:00,000 --> 00:00:01,420
嘿，大家好，我叫乔纳（Jonah），最后欢迎回到我的马s
hey what's up guys my name is a Jonah

2
00:00:01,620 --> 00:00:02,799
嘿，大家好，我叫乔纳（Jonah），最后欢迎回到我的马s
and welcome back to my stables last

3
00:00:03,000 --> 00:00:04,328
今天的系列有一些不同，希望大家今天喜欢
series got a bit of a different set

4
00:00:04,528 --> 00:00:06,280
今天的系列有一些不同，希望大家今天喜欢
today hope you guys like it today we're

5
00:00:06,480 --> 00:00:07,689
将要讨论的一切在C ++中this关键字是什么
gonna be talking all about what the this

6
00:00:07,889 --> 00:00:11,169
将要讨论的一切在C ++中this关键字是什么
keyword is in C++ did this that's kind

7
00:00:11,369 --> 00:00:12,970
很难说，所以我们有一个关键字和名为this的C ++，仅可访问
of hard to say so we have a keyword and

8
00:00:13,169 --> 00:00:15,220
很难说，所以我们有一个关键字和名为this的C ++，仅可访问
C++ called this which is only accessible

9
00:00:15,419 --> 00:00:17,109
通过成员函数和对我们意味着属于一个函数的函数
to us through a member function and the

10
00:00:17,309 --> 00:00:19,060
通过成员函数和对我们意味着属于一个函数的函数
function meaning a function that belongs

11
00:00:19,260 --> 00:00:20,890
到一个类，这样的方法和方法内部，我们可以引用这个和什么
to a class so a method and inside a

12
00:00:21,089 --> 00:00:22,960
到一个类，这样的方法和方法内部，我们可以引用这个和什么
method we can reference this and what

13
00:00:23,160 --> 00:00:25,239
这是该方法所属的当前对象实例的指针
this is is a pointer to the current

14
00:00:25,439 --> 00:00:27,370
这是该方法所属的当前对象实例的指针
object instance that the method belongs

15
00:00:27,570 --> 00:00:29,379
当然，在C ++中，我们可以编写一个非静态方法，并按顺序
to so of course in C++ we can write a

16
00:00:29,579 --> 00:00:32,439
当然，在C ++中，我们可以编写一个非静态方法，并按顺序
method a non-static method and in order

17
00:00:32,640 --> 00:00:33,909
调用该方法，我们需要先实例化一个对象，然后调用
to call that method we need to first

18
00:00:34,109 --> 00:00:36,729
调用该方法，我们需要先实例化一个对象，然后调用
instantiate an object and then call the

19
00:00:36,929 --> 00:00:38,559
方法，因此必须使用一个有效的对象来调用该方法
method so the method has to be called

20
00:00:38,759 --> 00:00:41,589
方法，因此必须使用一个有效的对象来调用该方法
with a valid object and this the this

21
00:00:41,789 --> 00:00:44,709
关键字是指向该对象的指针，这实际上对于如何
keyword is a pointer to that object this

22
00:00:44,909 --> 00:00:46,149
关键字是指向该对象的指针，这实际上对于如何
is actually very important to how

23
00:00:46,350 --> 00:00:47,858
方法总体上可行，我将要做一个深入的视频，很快就会链接到
methods work in general I'm gonna be

24
00:00:48,058 --> 00:00:50,108
方法总体上可行，我将要做一个深入的视频，很快就会链接到
doing an in-depth videos soon link that

25
00:00:50,308 --> 00:00:53,409
那里将对对象和类的实际工作方式有所帮助
will be help there about how how objects

26
00:00:53,609 --> 00:00:54,998
那里将对对象和类的实际工作方式有所帮助
and classes actually work kind of

27
00:00:55,198 --> 00:00:56,588
今天在内部，我们将介绍这意味着什么以及如何使用它
internally today we're just gonna cover

28
00:00:56,789 --> 00:00:59,018
今天在内部，我们将介绍这意味着什么以及如何使用它
what this means and how we can use it

29
00:00:59,219 --> 00:01:01,119
以及为什么它甚至存在于我们的代码中，我将创建一个类
and why it even exists so over here in

30
00:01:01,320 --> 00:01:02,678
以及为什么它甚至存在于我们的代码中，我将创建一个类
our code I'm going to make a class

31
00:01:02,878 --> 00:01:05,168
我会在X＆Y给它几个公共领域，然后让我
called entity I'll give it a couple of

32
00:01:05,368 --> 00:01:07,959
我会在X＆Y给它几个公共领域，然后让我
public fields here X&Y and then make me

33
00:01:08,159 --> 00:01:10,329
现在也可以立即接受X＆Y的构造函数
a constructor which also takes in X&Y

34
00:01:10,530 --> 00:01:12,129
现在也可以立即接受X＆Y的构造函数
now immediately you probably say that

35
00:01:12,329 --> 00:01:14,409
我想要做的是将这些成员变量的值分配给这些字段
what I want to do is assign these fields

36
00:01:14,609 --> 00:01:15,579
我想要做的是将这些成员变量的值分配给这些字段
these member variables with the values

37
00:01:15,780 --> 00:01:17,918
这些参数当然可以使用并初始化一个列表，然后执行
of these parameters of course I could

38
00:01:18,118 --> 00:01:19,750
这些参数当然可以使用并初始化一个列表，然后执行
use and initialize a list and just do

39
00:01:19,950 --> 00:01:21,278
这个，那将是完全可以的，但是如果我不想
this and that will be totally fine that

40
00:01:21,478 --> 00:01:23,349
这个，那将是完全可以的，但是如果我不想
would work however if I didn't want to

41
00:01:23,549 --> 00:01:24,789
做到这一点，我想在身体上做到这一点，我将面对一个
do that and I wanted to actually do it

42
00:01:24,989 --> 00:01:26,140
做到这一点，我想在身体上做到这一点，我将面对一个
in the body I would be faced with a

43
00:01:26,340 --> 00:01:27,668
有点问题，因为您可能会注意到它们确实
little bit of a problem because as you

44
00:01:27,868 --> 00:01:29,439
有点问题，因为您可能会注意到它们确实
can probably notice they have exactly

45
00:01:29,640 --> 00:01:31,509
相同的名字，所以如果我尝试做x等于x我实际上只是在分配
the same name so if I try and do x

46
00:01:31,709 --> 00:01:33,429
相同的名字，所以如果我尝试做x等于x我实际上只是在分配
equals x I'm actually just assigning

47
00:01:33,629 --> 00:01:36,159
这个X变量具有自己的值的参数，这绝对是在做
this X variable the parameter with its

48
00:01:36,359 --> 00:01:38,109
这个X变量具有自己的值的参数，这绝对是在做
own value which is just doing absolutely

49
00:01:38,310 --> 00:01:39,819
我真正想要做的就是引用属于的X和y
nothing what I really want to do is

50
00:01:40,019 --> 00:01:41,769
我真正想要做的就是引用属于的X和y
reference the X and y that belongs to

51
00:01:41,969 --> 00:01:43,269
该类是实际的类成员，那是磁盘，实际上是什么
the class the actual class members and

52
00:01:43,469 --> 00:01:44,948
该类是实际的类成员，那是磁盘，实际上是什么
that's what the disk a what actually

53
00:01:45,149 --> 00:01:46,659
就像我提到的那样，关键字允许我基本上是指向
allows me to do so as I mentioned the

54
00:01:46,859 --> 00:01:48,189
就像我提到的那样，关键字允许我基本上是指向
keyword is basically a pointer to the

55
00:01:48,390 --> 00:01:49,808
当前实例，为了使这一点更加清楚，我将
current instance so to make this a

56
00:01:50,009 --> 00:01:50,948
当前实例，为了使这一点更加清楚，我将
little bit more clear I'm just gonna

57
00:01:51,149 --> 00:01:53,140
在这里写出实体点，所以我将其命名为a并将其分配给
write it out here entity point so I'll

58
00:01:53,340 --> 00:01:54,459
在这里写出实体点，所以我将其命名为a并将其分配给
just call it a and I'll assign it to

59
00:01:54,659 --> 00:01:56,198
从技术上讲，如果您将鼠标悬停在
this that is the type of this

60
00:01:56,399 --> 00:01:57,849
从技术上讲，如果您将鼠标悬停在
technically if you hover your mouse over

61
00:01:58,049 --> 00:01:59,349
您会看到它实际上是一个NC指针常量，因此您可以将其设置为
you'll see that it's actually an NC

62
00:01:59,549 --> 00:02:01,480
您会看到它实际上是一个NC指针常量，因此您可以将其设置为
pointer constant so you could set it to

63
00:02:01,680 --> 00:02:03,640
但是如果大多数人不愿意，大多数人只是将其分配给
that if you want to however most people

64
00:02:03,840 --> 00:02:05,230
但是如果大多数人不愿意，大多数人只是将其分配给
don't most people just assign it to an

65
00:02:05,430 --> 00:02:07,480
实体指针，因为Const在其右侧的含义是
entity pointer because what Const means

66
00:02:07,680 --> 00:02:09,338
实体指针，因为Const在其右侧的含义是
on the right-hand side of this is that

67
00:02:09,538 --> 00:02:09,949
当我允许将其重新分配给其他东西时，我们
when I'll allow

68
00:02:10,149 --> 00:02:11,959
当我允许将其重新分配给其他东西时，我们
to reassign this to something else so we

69
00:02:12,158 --> 00:02:13,849
不能写例如等于空指针或尝试实际
couldn't write this equals null pointer

70
00:02:14,049 --> 00:02:15,980
不能写例如等于空指针或尝试实际
for example or attempt to actually

71
00:02:16,180 --> 00:02:18,950
将其分配给此处不是恒定的引用，我们将
assign this to a reference here that is

72
00:02:19,150 --> 00:02:20,480
将其分配给此处不是恒定的引用，我们将
non constant this sense we would

73
00:02:20,680 --> 00:02:22,610
如果您需要更多详细信息，实际上必须像这样声明为Const
actually have to declare this as Const

74
00:02:22,810 --> 00:02:24,319
如果您需要更多详细信息，实际上必须像这样声明为Const
like that if you want more details on

75
00:02:24,519 --> 00:02:26,149
康斯特如何运作以及在这种情况下君士坦丁是什么
how Const works and what constantine's

76
00:02:26,348 --> 00:02:27,770
康斯特如何运作以及在这种情况下君士坦丁是什么
in that context I made a video on that

77
00:02:27,969 --> 00:02:29,330
您可以通过单击那里的卡来访问它，因此无论如何回到此
which you can access by clicking the

78
00:02:29,530 --> 00:02:31,520
您可以通过单击那里的卡来访问它，因此无论如何回到此
card up there so anyway back to this see

79
00:02:31,719 --> 00:02:32,868
我做了什么，如果我们现在想实际分配X，我们可以做一个箭头
what I did that if we now want to

80
00:02:33,068 --> 00:02:35,118
我做了什么，如果我们现在想实际分配X，我们可以做一个箭头
actually assign X we can just do a arrow

81
00:02:35,318 --> 00:02:38,539
X，然后分配X或，当然可以使它更简单一些，我们可以
X and just assign X or of course to make

82
00:02:38,739 --> 00:02:40,099
X，然后分配X或，当然可以使它更简单一些，我们可以
this a little bit more simple we can

83
00:02:40,299 --> 00:02:42,800
只是这个箭头x等于x这当然是指针，所以我们需要
just do this arrow x equals x this is a

84
00:02:43,000 --> 00:02:44,149
只是这个箭头x等于x这当然是指针，所以我们需要
pointer of course so we need to

85
00:02:44,348 --> 00:02:45,800
解引用这就是为什么我们需要使用箭头或者我们可以写
dereference it that's why we need to use

86
00:02:46,000 --> 00:02:47,719
解引用这就是为什么我们需要使用箭头或者我们可以写
the arrow alternatively we could write

87
00:02:47,919 --> 00:02:49,700
像这样的代码，但是当然，箭头看起来更干净
code like this but of course it looks a

88
00:02:49,900 --> 00:02:51,050
像这样的代码，但是当然，箭头看起来更干净
lot cleaner with the arrow so most

89
00:02:51,250 --> 00:02:52,909
人们只是这样做，而YI也会这样做，所以现在我们实际上有了
people just do that and with Y I would

90
00:02:53,109 --> 00:02:54,469
人们只是这样做，而YI也会这样做，所以现在我们实际上有了
do the same so now we actually have a

91
00:02:54,669 --> 00:02:55,819
分配这两个变量的方法非常重要，因为我们
way of assigning these two variables

92
00:02:56,019 --> 00:02:57,800
分配这两个变量的方法非常重要，因为我们
which is very important because we we

93
00:02:58,000 --> 00:02:59,450
否则，如果我们要编写一个返回
couldn't have done it otherwise if we

94
00:02:59,650 --> 00:03:00,800
否则，如果我们要编写一个返回
were to write a function which returns

95
00:03:01,000 --> 00:03:02,629
例如，这些变量之一对我们来说对营销人员来说很常见
one of these variables for example it's

96
00:03:02,829 --> 00:03:04,340
例如，这些变量之一对我们来说对营销人员来说很常见
pretty common for us to marketers Const

97
00:03:04,539 --> 00:03:05,539
因为它不会修改此类，所以在Const函数中
because it's not going to be modifying

98
00:03:05,739 --> 00:03:08,239
因为它不会修改此类，所以在Const函数中
this class so in a Const function this

99
00:03:08,438 --> 00:03:10,879
我实际上不等于一个实体，实际上我等于一个常数
isn't actually equal to just an entity's

100
00:03:11,079 --> 00:03:12,679
我实际上不等于一个实体，实际上我等于一个常数
actually equal to a constant I see

101
00:03:12,878 --> 00:03:14,569
因为将Const放在这里当然意味着我们不允许修改
because of course putting Const here

102
00:03:14,769 --> 00:03:16,129
因为将Const放在这里当然意味着我们不允许修改
means that we're not allowed to modify

103
00:03:16,329 --> 00:03:19,610
上课，所以必须是Const，这样我们就无法摆脱做事
the class so this has to be Const so

104
00:03:19,810 --> 00:03:21,289
上课，所以必须是Const，这样我们就无法摆脱做事
that we can't get away with doing stuff

105
00:03:21,489 --> 00:03:24,050
总而言之，如果我们不这样做的话，这是可能的
like this which would have been possible

106
00:03:24,250 --> 00:03:27,289
总而言之，如果我们不这样做的话，这是可能的
if we didn't do that so in summary in a

107
00:03:27,489 --> 00:03:29,480
const函数将鼠标悬停在您将获得的实际类型上
Const function the actual type that

108
00:03:29,680 --> 00:03:30,890
const函数将鼠标悬停在您将获得的实际类型上
you'll get if you hover your mouse over

109
00:03:31,090 --> 00:03:32,330
在这里，您将看到将是一个常量实体，指向一个常量或一个常量
here you'll see is going to be a

110
00:03:32,530 --> 00:03:34,189
在这里，您将看到将是一个常量实体，指向一个常量或一个常量
constant entity point a Const or just a

111
00:03:34,389 --> 00:03:35,719
const实体指针，这实际上是我们关心的部分，如果您
Const entity pointer that's really the

112
00:03:35,919 --> 00:03:37,520
const实体指针，这实际上是我们关心的部分，如果您
part that we care about and then if you

113
00:03:37,719 --> 00:03:39,649
在非消耗内部访问它只是一个实体指针
access it inside a non-consumption is

114
00:03:39,848 --> 00:03:41,149
在非消耗内部访问它只是一个实体指针
just going to be an entity pointer

115
00:03:41,348 --> 00:03:43,730
另一个有用的情况是，如果我们可能想调用一个
another useful case for this is if we

116
00:03:43,930 --> 00:03:45,259
另一个有用的情况是，如果我们可能想调用一个
maybe wanted to call a function that was

117
00:03:45,459 --> 00:03:46,939
在此实体类之外，因此它不是方法，但我们想调用一个
outside of this entity class so it

118
00:03:47,139 --> 00:03:48,529
在此实体类之外，因此它不是方法，但我们想调用一个
wasn't a method but we wanted to call a

119
00:03:48,729 --> 00:03:50,659
该类外部的功能，该类以实体为对象
function outside this class from within

120
00:03:50,859 --> 00:03:52,879
该类外部的功能，该类以实体为对象
this class that took entity as a

121
00:03:53,079 --> 00:03:55,670
参数，例如，也许我们有一个打印实体功能，并且由于
parameter so as an example maybe we had

122
00:03:55,870 --> 00:03:58,039
参数，例如，也许我们有一个打印实体功能，并且由于
a print entity function and because of

123
00:03:58,239 --> 00:03:59,779
这里的声明水，我实际上必须在这里声明
the declaration water here I'm actually

124
00:03:59,979 --> 00:04:01,550
这里的声明水，我实际上必须在这里声明
going to have to declare this up here

125
00:04:01,750 --> 00:04:05,929
然后在这里定义它，所以我们有一个功能可能会很酷
and then define it over here so we have

126
00:04:06,128 --> 00:04:07,969
然后在这里定义它，所以我们有一个功能可能会很酷
a function that might do some cool

127
00:04:08,169 --> 00:04:09,709
印刷某种东西，我们希望能够从内部与实体合作
printing kind of stuff and we want to be

128
00:04:09,908 --> 00:04:11,480
印刷某种东西，我们希望能够从内部与实体合作
able to cooperate to entity from inside

129
00:04:11,680 --> 00:04:13,580
这个类，所以可能像这样，我想在当前实例中传递
this class so it may be like this I want

130
00:04:13,780 --> 00:04:15,618
这个类，所以可能像这样，我想在当前实例中传递
to pass in the current instance of this

131
00:04:15,818 --> 00:04:17,538
实体类到此函数中，我该怎么办呢？
entity class into this function how do I

132
00:04:17,738 --> 00:04:19,610
实体类到此函数中，我该怎么办呢？
do that that's where this comes in I can

133
00:04:19,810 --> 00:04:21,400
只要把这个递给他，那当然会通过当前
just pass him this and that's

134
00:04:21,600 --> 00:04:23,110
只要把这个递给他，那当然会通过当前
going to of course passing the current

135
00:04:23,310 --> 00:04:25,120
我想在这里设置的X＆Y实例
instance with the X&Y that I've got set

136
00:04:25,319 --> 00:04:26,740
我想在这里设置的X＆Y实例
here if I wanted to take this in as a

137
00:04:26,939 --> 00:04:28,660
恒定引用我在这里要做的实际上只是取消引用它
constant reference all I would have to

138
00:04:28,860 --> 00:04:30,879
恒定引用我在这里要做的实际上只是取消引用它
do here is actually just dereference it

139
00:04:31,079 --> 00:04:31,930
我们去了，所以当然会引用这个
and there we go

140
00:04:32,129 --> 00:04:33,400
我们去了，所以当然会引用这个
so of course would be referencing this

141
00:04:33,600 --> 00:04:35,920
在非音乐会的情况下，我们只会像这样获得实体引用，
in the case of a non concert we'll just

142
00:04:36,120 --> 00:04:38,590
在非音乐会的情况下，我们只会像这样获得实体引用，
get an entity reference back like so and

143
00:04:38,790 --> 00:04:40,870
然后使用Const方法，我们会像这样返回一个Const引用，因为这是一个
then a Const method we would get a Const

144
00:04:41,069 --> 00:04:43,150
然后使用Const方法，我们会像这样返回一个Const引用，因为这是一个
reference back like so because this is a

145
00:04:43,350 --> 00:04:44,620
指向当前类的指针，我们还可以做一些非常奇怪的事情，例如
pointer to the current class we can also

146
00:04:44,819 --> 00:04:46,300
指向当前类的指针，我们还可以做一些非常奇怪的事情，例如
do some pretty bizarre things such as

147
00:04:46,500 --> 00:04:48,730
称为删除此代码，我在非常非常几次
called delete this I've seen this code a

148
00:04:48,930 --> 00:04:50,740
称为删除此代码，我在非常非常几次
handful of times in very very

149
00:04:50,939 --> 00:04:52,990
对于特殊情况，我的建议当然是避免这样做
specialized cases my recommendation

150
00:04:53,189 --> 00:04:55,030
对于特殊情况，我的建议当然是避免这样做
would be of course to avoid doing this

151
00:04:55,230 --> 00:04:56,740
因为您要释放成员函数的内存，并且如果您决定
because you're freeing memory from a

152
00:04:56,939 --> 00:04:58,840
因为您要释放成员函数的内存，并且如果您决定
member function and if you decide to

153
00:04:59,040 --> 00:05:01,900
呼叫删除后，您曾经访问过任何成员数据，否则您将爆炸
ever access any member data after you

154
00:05:02,100 --> 00:05:04,120
呼叫删除后，您曾经访问过任何成员数据，否则您将爆炸
call delete this you're going to explode

155
00:05:04,319 --> 00:05:05,530
因为内存已被释放，所以通常不要写代码
because the memory had been freed so

156
00:05:05,730 --> 00:05:07,600
因为内存已被释放，所以通常不要写代码
don't don't don't typically write code

157
00:05:07,800 --> 00:05:09,819
像这样使它成为C ++，有一个地方
like this just make this is C++ there's

158
00:05:10,019 --> 00:05:11,710
像这样使它成为C ++，有一个地方
kind of a there's a place for pretty

159
00:05:11,910 --> 00:05:16,540
几乎所有的东西，但是该死的，也许不，我还是不希望你喜欢
much everything but but darn it maybe no

160
00:05:16,740 --> 00:05:17,800
几乎所有的东西，但是该死的，也许不，我还是不希望你喜欢
I don't anyway I hope you guys enjoyed

161
00:05:18,000 --> 00:05:19,180
这一集我实际上已经为你们中的一些人升级了音频
this episode I've actually upgraded the

162
00:05:19,379 --> 00:05:20,680
这一集我实际上已经为你们中的一些人升级了音频
audio a bit few of you guys for

163
00:05:20,879 --> 00:05:22,540
抱怨房间和东西回声，所以我使用了不同的
complaining about echoing in rooms and

164
00:05:22,740 --> 00:05:23,439
抱怨房间和东西回声，所以我使用了不同的
stuff so I'm using a different

165
00:05:23,639 --> 00:05:25,389
现在希望它听起来更好，让我知道您在麦克风中的想法
microphone now hopefully it sounds

166
00:05:25,589 --> 00:05:26,470
现在希望它听起来更好，让我知道您在麦克风中的想法
better let me know what you think in the

167
00:05:26,670 --> 00:05:28,030
如果您想支持本系列剧本，并且基本上对剧集有所帮助，请在下面发表评论
comments below if you want to support

168
00:05:28,230 --> 00:05:29,829
如果您想支持本系列剧本，并且基本上对剧集有所帮助，请在下面发表评论
this series and basically help episodes

169
00:05:30,029 --> 00:05:31,720
出来更快，质量更好，所有你可以去的patreon
come out quicker and be high-quality and

170
00:05:31,920 --> 00:05:33,129
出来更快，质量更好，所有你可以去的patreon
all that you can go over to patreon

171
00:05:33,329 --> 00:05:35,079
Tacoma 4/2 Cherno，您将获得一些非常酷的奖励，因此只需获得几集
Tacoma 4/2 Cherno you'll get some pretty

172
00:05:35,279 --> 00:05:36,490
Tacoma 4/2 Cherno，您将获得一些非常酷的奖励，因此只需获得几集
cool rewards so just getting episodes

173
00:05:36,689 --> 00:05:39,250
尽早并且能够在不和谐的气氛中以凉爽的方式私下交谈
early and being able to talk privately

174
00:05:39,449 --> 00:05:41,620
尽早并且能够在不和谐的气氛中以凉爽的方式私下交谈
in a cool kind of patreon on the discord

175
00:05:41,819 --> 00:05:42,939
聊天和所有有趣的东西，所以一定要检查一下，因为它
chat and all that fun stuff so

176
00:05:43,139 --> 00:05:44,560
聊天和所有有趣的东西，所以一定要检查一下，因为它
definitely check that out because it

177
00:05:44,759 --> 00:05:46,720
确实有助于支持该系列影片，并且可以帮助我制作更多的视频，
does help support the series and it

178
00:05:46,920 --> 00:05:48,939
确实有助于支持该系列影片，并且可以帮助我制作更多的视频，
helps me make way more videos I will see

179
00:05:49,139 --> 00:05:51,199
你们下次再见[音乐]
you guys next time goodbye

180
00:05:51,399 --> 00:05:56,399
你们下次再见[音乐]
[Music]

