﻿1
00:00:00,000 --> 00:00:03,520
Hey look guys my name is the Chano and welcome back to my paper floss series
嗨，大家好，我叫Chano，欢迎回到我的纸线系列

2
00:00:03,720 --> 00:00:07,419
today I'm going to be talking all about pluses in tip or flood so finally
今天我要谈论的是小费或洪水的加成，所以最后

3
00:00:07,620 --> 00:00:10,149
getting is something called object-oriented programming which is a
得到的东西叫做面向对象的程序设计

4
00:00:10,349 --> 00:00:13,450
very popular way of programming object-oriented programming is really
面向对象编程的一种非常流行的编程方式是

5
00:00:13,650 --> 00:00:18,400
just a style that you can adult as to how you write your code other languages
只是您可以将自己的代码写成其他语言的样式

6
00:00:18,600 --> 00:00:22,568
such as spaceship and Java are primarily object artists languages in fact you
例如太空飞船和Java主要是对象艺术家语言，实际上您

7
00:00:22,768 --> 00:00:26,620
can't really write any other type of code I mean you can if you really really
我真的不能写任何其他类型的代码，我的意思是你可以

8
00:00:26,820 --> 00:00:30,640
try but ultimately those languages are meant to be of your hunted languages
尝试，但最终这些语言应属于您搜寻的语言

9
00:00:30,839 --> 00:00:33,459
however say for plus is something a little bit different because it doesn't
但是说加号有点不同，因为它没有

10
00:00:33,659 --> 00:00:37,299
really import a certain style upon you the language speed for example doesn't
确实会根据您的语言速度导入某种样式，例如

11
00:00:37,500 --> 00:00:40,299
actually support of your drawing programming because in order to have
实际上支持您的绘图程序，因为

12
00:00:40,500 --> 00:00:44,439
object-oriented programming you need to be able to have concepts such as classes
面向对象的程序设计需要具备诸如类之类的概念

13
00:00:44,640 --> 00:00:47,288
and object and that's not something that's available and see however pepper
和对象，那不是可用的东西，但是看到胡椒

14
00:00:47,488 --> 00:00:51,579
sauce does add all of that functionality if you wish to use it and it is almost
酱汁确实添加了所有这些功能，如果您想使用它的话，几乎

15
00:00:51,780 --> 00:00:54,309
always a good idea to use it to some extent so we're gonna be talking about
一定程度上使用它总是一个好主意，所以我们将在谈论

16
00:00:54,509 --> 00:00:58,509
what classes are in this video to put it simply classes are just a way to group
这部影片中的课程简而言之，课程只是分组的一种方式

17
00:00:58,710 --> 00:01:03,518
data and/or functionality together for example think of a game in
数据和/或功能一起考虑例如

18
00:01:03,719 --> 00:01:08,140
a game we might want to have some kind of representation as a player so what
一个游戏，我们可能希望以某种形式表现为玩家，所以

19
00:01:08,340 --> 00:01:11,799
kind of things do we need to actually represent a player we definitely want
我们需要真正代表一个我们确实想要的球员的事情

20
00:01:12,000 --> 00:01:15,579
some kind of data for example the position of the player in our game while
某种数据，例如玩家在我们游戏中的位置

21
00:01:15,780 --> 00:01:19,899
certain attributes of the player might possess such as the speed at which the
玩家可能拥有的某些属性，例如

22
00:01:20,099 --> 00:01:23,289
player moves we might also want to have some kind of 3d model that represents
玩家的动作，我们可能还想拥有某种3D模型来代表

23
00:01:23,489 --> 00:01:27,009
the player on the screen all of this data needs to be stored somewhere we
屏幕上的播放器所有这些数据都需要存储在我们

24
00:01:27,209 --> 00:01:30,640
could go ahead and create variables for all this in fact let's go ahead and take
可以继续为所有这些创建变量，事实上，让我们继续

25
00:01:30,840 --> 00:01:33,759
a look at what that would look like let's say that we want to make a player
看一下我们想要成为一名球员的样子

26
00:01:33,959 --> 00:01:38,709
here in our code we might want to have a position so I'll just say X&Y and then
在我们的代码中，我们可能希望有一个位置，所以我只说X＆Y，然后

27
00:01:38,909 --> 00:01:41,859
potentially a speed let's just have a speed equal to 2 so these are all
可能有一个速度，让我们让速度等于2，所以这些都是

28
00:01:42,060 --> 00:01:45,369
integers for now you can probably start to see that this is getting a little bit
现在，您可能会开始看到整数

29
00:01:45,569 --> 00:01:49,480
messy and in fact because these names are so generic you might want to do
杂乱无章，实际上是因为这些名称太通用了，您可能想做

30
00:01:49,680 --> 00:01:53,619
something like player acts player Y for the X and y coordinate of the player
类似玩家的角色，将玩家Y用作其X和y坐标

31
00:01:53,819 --> 00:01:57,009
maybe play a speed and this really does start to get a little bit messy
也许玩一个速度，这确实开始变得有点混乱

32
00:01:57,209 --> 00:02:00,640
especially if we decide actually we want to players in our game well then
特别是如果我们决定要在游戏中很好地参与比赛，那么

33
00:02:00,840 --> 00:02:04,079
suddenly you're going to have to duplicate this and start doing something
突然之间，您将不得不重复此操作并开始做某事

34
00:02:04,280 --> 00:02:07,739
like plaque there a player x1 instead of this you could of course use an array
像牌匾一样，有一个玩家x1代替它，您当然可以使用数组

35
00:02:07,939 --> 00:02:12,868
but the point is still the same it's just kind of a bunch of variables that
但重点仍然是相同的，只是一堆变量

36
00:02:13,068 --> 00:02:15,840
aren't group together they're just unorganized they're just sitting in our
并没有在一起他们只是没有组织他们只是坐在我们的

37
00:02:16,039 --> 00:02:21,090
code and it's not a good idea another great example of why this is annoying is
代码，这不是一个好主意，另一个令人讨厌的好例子是

38
00:02:21,289 --> 00:02:24,780
because if I want to write a function that moves the player or something like
因为如果我想编写一个可以移动播放器或类似功能的函数

39
00:02:24,979 --> 00:02:29,670
that I suddenly need to be specifying all three parameters as integers so for
我突然需要将所有三个参数都指定为整数，因此

40
00:02:29,870 --> 00:02:33,689
example the x coordinates and Y coordinates at speed all of this becomes
例如，x坐标和Y坐标的速度很快就变成了

41
00:02:33,889 --> 00:02:39,240
so much code and Laura difficult to maintain and follow that it's just it's
如此多的代码和Laura难以维护和遵循，这就是

42
00:02:39,439 --> 00:02:42,330
really really messy so what we can do instead is simplify this by using a
真的很乱，所以我们可以做的是使用

43
00:02:42,530 --> 00:02:46,170
class we can create a class for the player we can call it player which
类，我们可以为玩家创建一个类，我们可以称之为玩家

44
00:02:46,370 --> 00:02:50,580
contains all of that data that we want all the variables kind of in one time so
一次包含所有想要的所有变量的所有数据，因此

45
00:02:50,780 --> 00:02:56,100
instead of this let's create a class called player we do so by just using the
而不是创建一个叫做player的类，我们只需要使用

46
00:02:56,300 --> 00:02:59,670
work class and then giving it a name this has to be a unique name because
工作类，然后给它起一个名字，这个名字必须是唯一的，因为

47
00:02:59,870 --> 00:03:03,600
classes are types we're basically creating a new variable type and then we
类是类型，我们基本上是在创建一个新的变量类型，然后我们

48
00:03:03,800 --> 00:03:07,080
open in close curly brackets as if this was a function however notice that we do
用大括号括起来，好像这是一个函数，但是请注意， 

49
00:03:07,280 --> 00:03:11,759
actually need a semicolon at the end of the closing brace inside here we can
实际上，在这里，在封闭括号的末端需要一个分号

50
00:03:11,959 --> 00:03:15,810
specify all of those variables that we did below for example X and Y for the
指定我们在下面执行的所有这些变量，例如X和Y 

51
00:03:16,009 --> 00:03:20,969
position and then a be variable as well all right great there we go we've
位置，然后是可变的，好吧，好吧，我们去了，我们已经

52
00:03:21,169 --> 00:03:25,379
created a brand new class called player which is essentially its own type so if
创建了一个称为Player的全新类，它本质上是自己的类型，因此如果

53
00:03:25,579 --> 00:03:29,189
we were to start using this player class we can create it as if it was any other
我们将开始使用此播放器类，我们可以像创建其他任何类一样创建它

54
00:03:29,389 --> 00:03:33,840
variable we write the type so player and then we give it any name I'm going to
变量，我们写出类型，这样播放器，然后给它我要使用的任何名称

55
00:03:34,039 --> 00:03:36,509
call this player and that's it we've created a new variable called player
叫这个玩家，就是这样，我们创建了一个新的变量，叫做玩家

56
00:03:36,709 --> 00:03:40,770
which has the type player and of course the definition of that type is over here
有类型播放器，当然，该类型的定义在这里

57
00:03:40,969 --> 00:03:44,430
variables that are made from class types are called objects and a new object
由类类型制成的变量称为对象和新对象

58
00:03:44,629 --> 00:03:48,150
variable is called an instance so what we've done here is we've instantiated a
变量称为实例，所以我们在这里完成的是实例化一个

59
00:03:48,349 --> 00:03:52,409
player object because we've credit a new instance of that player type now if we
播放器对象，因为如果我们现在已经记下该播放器类型的新实例， 

60
00:03:52,609 --> 00:03:56,340
want to set those variables we can simply write player dot and then the
要设置这些变量，我们可以简单地编写玩家点，然后

61
00:03:56,539 --> 00:04:00,689
variable names such as X and then assign it equal to something like 5 if we try
变量名称（例如X），然后尝试将其赋值等于5 

62
00:04:00,889 --> 00:04:03,360
and compile the code right now we're actually going to get an error that
并立即编译代码，实际上我们会得到一个错误， 

63
00:04:03,560 --> 00:04:07,080
tells us that player cannot access private member declared in class player
告诉我们玩家无法访问课堂玩家中声明的私人成员

64
00:04:07,280 --> 00:04:09,719
this is because of something called visibility when you create a new class
这是因为创建新类时称为“可见性” 

65
00:04:09,919 --> 00:04:12,660
you have the option to specify how visible the
您可以选择指定

66
00:04:12,860 --> 00:04:17,069
in that cloth actually is by default a cloth makes everything private which
在那个布中，实际上是默认情况下，布将所有东西都私有了

67
00:04:17,269 --> 00:04:19,920
means that only functions inside that cloth can actually access those
意味着只有布内部的功能才能实际访问这些功能

68
00:04:20,120 --> 00:04:23,970
variables however we want to be able to access these variables from the main
变量，但是我们希望能够从主变量访问这些变量

69
00:04:24,170 --> 00:04:27,540
function so what we actually need to do is come up here and make it public
功能，所以我们真正需要做的就是在这里公开

70
00:04:27,740 --> 00:04:31,710
public means that we're allowed to access these variables outside of this
 public表示我们被允许在此之外访问这些变量

71
00:04:31,910 --> 00:04:35,310
class anywhere in our code really we'll talk a lot more about visibility in a
在我们代码的任何地方都可以上课，实际上，我们将在

72
00:04:35,509 --> 00:04:38,129
future video I don't really want to get into it right now so you can see if we
未来的视频，我现在真的不想参加，您可以看看我们是否

73
00:04:38,329 --> 00:04:41,759
compile our code here then outside compiled successfully okay pretty sweet
在这里编译我们的代码，然后在外部编译成功，好漂亮

74
00:04:41,959 --> 00:04:45,360
so we've achieved our first goal we've managed to drastically clean up the code
因此我们实现了第一个目标，我们已经设法彻底清理了代码

75
00:04:45,560 --> 00:04:48,420
and have all of our variables in one place because really this collection of
并将我们所有的变量放在一个地方，因为实际上

76
00:04:48,620 --> 00:04:51,480
variables represent the player so we've managed to croak it nicely now that
变量代表玩家，所以我们现在已经设法做到了

77
00:04:51,680 --> 00:04:53,490
we've got all this data suppose that we actually want our player
所有这些数据都假设我们实际上想要我们的播放器

78
00:04:53,689 --> 00:04:56,699
to do something for example move somewhere so we need to write a function
做某事，例如移动到某个地方，所以我们需要编写一个函数

79
00:04:56,899 --> 00:05:00,780
which changes the player's x and y variables how can we do that well we
这会改变玩家的x和y变量，我们该怎么做呢？ 

80
00:05:00,980 --> 00:05:02,759
could write it as just a standalone function
可以将其编写为独立功能

81
00:05:02,959 --> 00:05:06,210
so I'll write void I'll call it moved and then I would need to take in the
所以我会写一个空的，我称之为移动，然后我需要将

82
00:05:06,410 --> 00:05:09,660
player that I would like to move and I will have to pass by reference because
我想移动的球员，我将不得不通过引用传递，因为

83
00:05:09,860 --> 00:05:14,069
will actually be modifying the player of U and then I'll also take in X a and Y a
实际上将修改U的玩家，然后我还将输入X a和Y a 

84
00:05:14,269 --> 00:05:18,329
which will be the amount that we move the player by in both x and y then all I
这就是我们在x和y上移动玩家的数量，然后是所有我

85
00:05:18,529 --> 00:05:23,460
need to do is just say player 2 X plus B equals X a and player 2 y + equals y a
需要做的只是说玩家2 X加B等于X a而玩家2 y +等于ya 

86
00:05:23,660 --> 00:05:28,680
we can also use that speed variable that we have there by multiplying our amount
我们也可以通过乘以我们的速度变量

87
00:05:28,879 --> 00:05:32,280
by B speed all right pretty cool and if we wanted to call that we would just
 B速度很好，很酷，如果我们想打电话给我们， 

88
00:05:32,480 --> 00:05:36,960
write move player and then Y how much we want to move to player so maybe 1 minus
先写出移动球员，然后是Y我们想移动多少，所以大概是1减

89
00:05:37,160 --> 00:05:39,329
1 and there we go we've written a function that can move the player
 1然后我们编写了一个可以移动播放器的函数

90
00:05:39,529 --> 00:05:42,718
however we can do a little bit that in this earlier I said that classes can
但是我们可以做一点，在前面的内容中我说过，课程可以

91
00:05:42,918 --> 00:05:46,290
actually contain functionality so what that basically means is that we can move
实际上包含功能，所以这基本上意味着我们可以移动

92
00:05:46,490 --> 00:05:51,180
that move function of ours into the class and functions inside classes are
将我们的函数移到类中，类中的函数是

93
00:05:51,379 --> 00:05:54,870
called methods so I can literally go over here into my code and just move
称为方法，因此我可以从字面上转到我的代码中，然后移动

94
00:05:55,069 --> 00:05:58,710
this a little bit up so that it's inside the player class and there we go now
这一点，所以它在播放器类中，现在我们去

95
00:05:58,910 --> 00:06:01,920
obviously we're inside the player class now so we actually have access to these
显然，我们现在处于玩家类之列，因此我们实际上可以访问这些

96
00:06:02,120 --> 00:06:06,810
variables we don't need to pass a player object in because we're already inside a
变量，我们不需要传递玩家对象，因为我们已经在

97
00:06:07,009 --> 00:06:12,300
player object so if I get rid of this and then all of these the x and y and
播放器对象，所以如果我摆脱了这个对象，那么所有这些x和y以及

98
00:06:12,500 --> 00:06:16,770
speed that we're referring to will be the current object variable if I come
如果我来，我们指的速度将是当前的对象变量

99
00:06:16,970 --> 00:06:21,209
down over here I can change my code now to just say player don't move
在这里，我现在可以更改代码，只是说玩家不要移动

100
00:06:21,408 --> 00:06:24,809
and there we go we've simplified our quote quite a bit every player object is
然后我们将报价简化了很多，每个玩家对象都是

101
00:06:25,009 --> 00:06:28,350
going to have its own move function and when we call move for that specific
将拥有自己的移动功能，当我们为特定的位置调用移动时

102
00:06:28,550 --> 00:06:32,249
player object that is the player object that will move again this isn't really
会再次移动的玩家对象，这不是真的

103
00:06:32,449 --> 00:06:36,179
any different to having the move function outside of the player class all
与在播放器类之外具有移动功能有什么不同

104
00:06:36,379 --> 00:06:39,600
it does is it kind of cleans up our code and makes everything look a little bit
它确实是清理我们的代码并使所有内容看起来有点

105
00:06:39,800 --> 00:06:42,119
nicer and that's a huge bonus when you're dealing with a lot of code
更好，当您处理大量代码时，这是一个巨大的好处

106
00:06:42,319 --> 00:06:45,600
because the more code you have the more complicated it can get and the harder it
因为您拥有的代码越多，获得的代码就越复杂，而且越难

107
00:06:45,800 --> 00:06:49,199
can actually become to maintain that code so having games like this to keep
可以真正维护该代码，因此保留类似的游戏

108
00:06:49,399 --> 00:06:53,009
the code cleaner is actually a very very welcome thing so that's it that is
代码清理器实际上是非常非常受欢迎的事情，仅此而已

109
00:06:53,209 --> 00:06:58,049
essentially the basics of what classes are classes allow us to group variables
本质上讲，什么是类的基础使我们能够对变量进行分组

110
00:06:58,249 --> 00:07:02,160
together into a type and also add functionality to those variables because
一起成为一个类型，还为这些变量添加了功能，因为

111
00:07:02,360 --> 00:07:05,819
if you take a look at this code one more time what we've really done is we've
如果您再看一次这段代码，我们真正要做的就是

112
00:07:06,019 --> 00:07:11,459
just defined three variables the kind of exist in one type and also a function
只是定义了三个变量，一种类型同时存在，还有一个函数

113
00:07:11,658 --> 00:07:14,429
which manipulates those variables that's really all we've
操纵那些实际上就是我们所拥有的变量

114
00:07:14,629 --> 00:07:17,639
done of course that function can do literally anything but the gist of it is
当然，该功能可以执行任何操作，但其要点是

115
00:07:17,838 --> 00:07:21,269
we've got data and functions to manipulate that data and that's really
我们有数据和操作这些数据的功能，这确实是

116
00:07:21,468 --> 00:07:25,319
all the classes now there are so many uses for classes and we'll kind of start
现在所有的类都有很多类用途，我们将开始

117
00:07:25,519 --> 00:07:29,639
getting into them in the future this is also literally just the basics
将来进入它们，这实际上只是基础知识

118
00:07:29,838 --> 00:07:32,968
of what a class is there are so many more things we can do with classes which
关于一个班级，我们可以使用班级做更多的事情

119
00:07:33,168 --> 00:07:36,899
is why the next few videos are going to be taking a look at those things in more
这就是为什么接下来的几个视频将在更多内容中介绍这些内容的原因

120
00:07:37,098 --> 00:07:40,679
detail also I want you to keep in mind that whilst classes are extremely useful
详细，我也希望您记住，尽管课堂非常有用

121
00:07:40,879 --> 00:07:46,139
and can keep your code a lot cleaner they can't do anything that you can't do
并且可以使您的代码更加整洁，他们无法做您无法做的任何事情

122
00:07:46,338 --> 00:07:50,338
without classes buttons don't give you any kind of new functionality that you
没有课程按钮不会为您提供任何新功能

123
00:07:50,538 --> 00:07:54,600
could not have done otherwise anything you can do with classes you can actually
否则可能无法完成，否则您实际上可以对类进行任何操作

124
00:07:54,800 --> 00:07:58,439
do without classes which is why languages such as C is this and I've
不用类，这就是为什么像C这样的语言的原因

125
00:07:58,639 --> 00:08:02,879
perfectly usable languages they don't have classes and yet we can still write
完全可用的语言，它们没有类，但是我们仍然可以编写

126
00:08:03,079 --> 00:08:06,778
code classes are just there to make our life easier a programmer they're
代码类的存在只是为了使我们的程序员更轻松

127
00:08:06,978 --> 00:08:11,968
essentially just syntactic sugar that we can use to organize our code and make it
本质上只是语法糖，我们可以用它来组织我们的代码并将其编写

128
00:08:12,168 --> 00:08:15,239
easier to maintain that's all they are anyway I hope you guys enjoy this video
维护起来就更容易了，我希望大家喜欢这个视频

129
00:08:15,439 --> 00:08:18,088
if you did please hit that like button you can also follow me on Twitter and
如果您点击了“喜欢”按钮，也可以在Twitter上关注我， 

130
00:08:18,288 --> 00:08:21,478
Instagram and of course next time we'll be taking a look at this stuff in more
 Instagram，当然，下一次我们将在更多内容中查看这些内容

131
00:08:21,678 --> 00:08:25,340
detail and seeing just what we can do with it goodbye
细节，看看我们能用它做些什么再见

132
00:08:25,540 --> 00:08:30,778
[Music]
[音乐]

133
00:08:35,090 --> 00:08:40,090
[Music]
 [音乐] 

