﻿1
00:00:00,000 --> 00:00:01,360
嘿，大家好，我叫Churner，欢迎回到我的C ++系列
hey what's up guys my name is the

2
00:00:01,560 --> 00:00:04,030
嘿，大家好，我叫Churner，欢迎回到我的C ++系列
churner welcome back to my c++ series

3
00:00:04,230 --> 00:00:06,069
今天我们将讨论所有有关如何在单个存储中存储任何类型的数据的问题。
today we'll be talking all about how you

4
00:00:06,269 --> 00:00:09,250
今天我们将讨论所有有关如何在单个存储中存储任何类型的数据的问题。
can store any kind of data in a single

5
00:00:09,449 --> 00:00:11,680
C ++中的变量很多人可能在想的很好，是的，您可以做
variable in c++ a lot of you are

6
00:00:11,880 --> 00:00:13,300
C ++中的变量很多人可能在想的很好，是的，您可以做
probably thinking well yeah you can do

7
00:00:13,500 --> 00:00:15,309
带有一个空指针，是的，这是真的，我将有一个关于空的视频
that with a void pointer and yes that's

8
00:00:15,509 --> 00:00:17,409
带有一个空指针，是的，这是真的，我将有一个关于空的视频
true and I'll have a video about void

9
00:00:17,609 --> 00:00:18,909
指针很快就会到来，但这是一个更好的方法
pointers coming soon in the future

10
00:00:19,109 --> 00:00:20,289
指针很快就会到来，但这是一个更好的方法
however this is a much of a better way

11
00:00:20,489 --> 00:00:22,720
这是一种更安全的方式，这是一种多余的17种全新的处理方式
it's a safer way and it's a supercilious

12
00:00:22,920 --> 00:00:25,629
这是一种更安全的方式，这是一种多余的17种全新的处理方式
17 brand new way of doing things I've

13
00:00:25,829 --> 00:00:27,609
最近有关于如何存储不同类型数据的小型系列
had this kind of miniseries lately on

14
00:00:27,809 --> 00:00:29,620
最近有关于如何存储不同类型数据的小型系列
how we can store different types of data

15
00:00:29,820 --> 00:00:31,359
和C ++中的单个变量，有一个叫做S City的可选S City
and single variables in C++ there's

16
00:00:31,559 --> 00:00:33,219
和C ++中的单个变量，有一个叫做S City的可选S City
something called S City optional S City

17
00:00:33,420 --> 00:00:34,538
变体，现在我们将谈论S City，而我已经
variants and now we're going to be

18
00:00:34,738 --> 00:00:36,518
变体，现在我们将谈论S City，而我已经
talking about S City any and I've

19
00:00:36,719 --> 00:00:37,989
已经制作了关于其他两个的视频，所以如果您还没有看过有关
already made videos about the other two

20
00:00:38,189 --> 00:00:39,219
已经制作了关于其他两个的视频，所以如果您还没有看过有关
so if you haven't seen the video about

21
00:00:39,420 --> 00:00:41,739
有关如何存储多个变量的可选数据或视频
optional data or the video about how we

22
00:00:41,939 --> 00:00:43,809
有关如何存储多个变量的可选数据或视频
can store multiple variables multiple

23
00:00:44,009 --> 00:00:45,878
数据类型放在单个变量中，然后查看将要链接的那些视频
data types in a single variable then

24
00:00:46,079 --> 00:00:47,500
数据类型放在单个变量中，然后查看将要链接的那些视频
check out those videos they'll be linked

25
00:00:47,700 --> 00:00:49,448
到那里，所以现在我们有点在这里，到了
up there so now we're kind of here at

26
00:00:49,649 --> 00:00:51,459
到那里，所以现在我们有点在这里，到了
the end we're up to the point where

27
00:00:51,659 --> 00:00:55,689
使用STD可以存储任何内容，我认为这里更大的问题是
using STD any we can store anything and

28
00:00:55,890 --> 00:00:57,608
使用STD可以存储任何内容，我认为这里更大的问题是
I think that the bigger question here is

29
00:00:57,808 --> 00:00:59,829
而不是我如何使用STD，我认为这非常简单，正如我们将在
not how do I use STD any I think that's

30
00:01:00,030 --> 00:01:01,268
而不是我如何使用STD，我认为这非常简单，正如我们将在
pretty straightforward as we'll see in a

31
00:01:01,469 --> 00:01:04,599
一分钟，这就是为什么当我们想要使用它时为什么存在它，为什么我们要使用s
minute it's why does this exist when do

32
00:01:04,799 --> 00:01:06,789
一分钟，这就是为什么当我们想要使用它时为什么存在它，为什么我们要使用s
we want to use it and why would we use s

33
00:01:06,989 --> 00:01:08,469
UV Eddie，而不是类似城市的变体
UV Eddie instead of something like a

34
00:01:08,670 --> 00:01:09,640
UV Eddie，而不是类似城市的变体
city variant

35
00:01:09,840 --> 00:01:11,590
所有今天都不会谈论的精彩事，但首先让我们谈谈
all brilliant things that wouldn't be

36
00:01:11,790 --> 00:01:13,359
所有今天都不会谈论的精彩事，但首先让我们谈谈
talking about today but first let's talk

37
00:01:13,560 --> 00:01:15,308
关于我们今天的视频Skillshare视频的赞助商，我确定你们有
about our sponsor for today's video

38
00:01:15,509 --> 00:01:16,808
关于我们今天的视频Skillshare视频的赞助商，我确定你们有
Skillshare I'm sure you guys have

39
00:01:17,009 --> 00:01:18,969
可能是在拥有在线学习社区之前听说过Skillshare的
probably heard of Skillshare before it's

40
00:01:19,170 --> 00:01:20,379
可能是在拥有在线学习社区之前听说过Skillshare的
an online learning community with

41
00:01:20,579 --> 00:01:22,509
数以千计的课程，涵盖数十种创新和企业家技能，
thousands of classes covering dozens of

42
00:01:22,709 --> 00:01:25,028
数以千计的课程，涵盖数十种创新和企业家技能，
creative and entrepreneurial skills with

43
00:01:25,228 --> 00:01:26,709
无限制地访问所有这些类，以便您可以加入
unlimited access to all of these classes

44
00:01:26,909 --> 00:01:28,418
无限制地访问所有这些类，以便您可以加入
so that you can join the ones that are

45
00:01:28,618 --> 00:01:29,950
最适合您，因为我更像是我真正喜欢的艺术家的工程师
right for you since I'm more of an

46
00:01:30,150 --> 00:01:31,869
最适合您，因为我更像是我真正喜欢的艺术家的工程师
engineer that an artist I really like

47
00:01:32,069 --> 00:01:33,459
Skillshare必须提供的所有创意课程
all of the creative classes that

48
00:01:33,659 --> 00:01:34,959
Skillshare必须提供的所有创意课程
Skillshare has to offer

49
00:01:35,159 --> 00:01:37,149
例如，当我需要制作一个
for example this class is a fantastic

50
00:01:37,349 --> 00:01:38,709
例如，当我需要制作一个
reference for whenever I need to make a

51
00:01:38,909 --> 00:01:41,019
每当需要时，新徽标Skillshare也是我的首选参考之一
new logo Skillshare are also one of my

52
00:01:41,219 --> 00:01:43,119
每当需要时，新徽标Skillshare也是我的首选参考之一
go-to references for anytime I need to

53
00:01:43,319 --> 00:01:44,918
处理网络内容，因为说实话，我想花最少的时间
deal with web stuff because let's be

54
00:01:45,118 --> 00:01:46,929
处理网络内容，因为说实话，我想花最少的时间
honest I want to spend as little time as

55
00:01:47,129 --> 00:01:49,448
通过这些简短的简短视频，我可能会抓住机会
possible hunt at and with these small

56
00:01:49,649 --> 00:01:51,278
通过这些简短的简短视频，我可能会抓住机会
concise videos I can get through this

57
00:01:51,478 --> 00:01:53,890
很快，Scotia将为任何谁提供两个月的免费会员资格
stuff quickly Scotia are offering two

58
00:01:54,090 --> 00:01:56,049
很快，Scotia将为任何谁提供两个月的免费会员资格
months of free membership to anyone who

59
00:01:56,250 --> 00:01:57,399
使用以下说明中的链接免费注册两个月
signs up using the link in the

60
00:01:57,599 --> 00:02:00,909
使用以下说明中的链接免费注册两个月
description below two months free

61
00:02:01,109 --> 00:02:02,918
两个月的会员资格思考两个月内您能学到多少
membership for two months think about

62
00:02:03,118 --> 00:02:05,109
两个月的会员资格思考两个月内您能学到多少
how much you could learn in two months

63
00:02:05,310 --> 00:02:07,238
所以继续检查一下，我要非常感谢您的技能
so go ahead and check that out and I

64
00:02:07,438 --> 00:02:08,500
所以继续检查一下，我要非常感谢您的技能
want to give a huge thank you to skill

65
00:02:08,699 --> 00:02:09,110
大家都知道我是
chef

66
00:02:09,310 --> 00:02:11,030
大家都知道我是
during this video as you guys know I'm

67
00:02:11,229 --> 00:02:12,200
全职从事YouTube这项工作还很新，这家公司很棒
quite new to doing this YouTube thing

68
00:02:12,400 --> 00:02:14,000
全职从事YouTube这项工作还很新，这家公司很棒
full time and it's amazing companies

69
00:02:14,199 --> 00:02:16,910
像这样使之成为可能的人看到我所做的一切
like that who make this possible anyway

70
00:02:17,110 --> 00:02:19,399
像这样使之成为可能的人看到我所做的一切
see what I did there is that any way

71
00:02:19,598 --> 00:02:23,579
因为我们在谈论SVP
because we're talking about SVP any

72
00:02:23,699 --> 00:02:25,819
让我们跳到代码中，所以我们要做的第一件事是
let's just jump into the code so the

73
00:02:26,019 --> 00:02:26,750
让我们跳到代码中，所以我们要做的第一件事是
first thing that we're gonna do is

74
00:02:26,949 --> 00:02:28,640
包括任何类似的头文件，并确保
include the header file which is just

75
00:02:28,840 --> 00:02:30,950
包括任何类似的头文件，并确保
any just like that also make sure that

76
00:02:31,150 --> 00:02:32,960
您当然是用C ++ 17编译此代码，因为这是C ++ 17
you are of course compiling this code

77
00:02:33,159 --> 00:02:36,200
您当然是用C ++ 17编译此代码，因为这是C ++ 17
with C++ 17 because this is a C++ 17

78
00:02:36,400 --> 00:02:39,170
我应该说，它的唯一功能是C ++ 17中的新功能，因为显然如果使用版本
only feature it's new in C++ 17 I should

79
00:02:39,370 --> 00:02:41,630
我应该说，它的唯一功能是C ++ 17中的新功能，因为显然如果使用版本
say because obviously if using a version

80
00:02:41,830 --> 00:02:43,280
比那还要新，除非委员会
newer than that presumably it would

81
00:02:43,479 --> 00:02:45,920
比那还要新，除非委员会
still be there unless the committee

82
00:02:46,120 --> 00:02:47,539
认为这是一个可怕的主意，但仍然相信它可能是
decided that it was a terrible idea

83
00:02:47,739 --> 00:02:50,780
认为这是一个可怕的主意，但仍然相信它可能是
which still convinced it might be and in

84
00:02:50,979 --> 00:02:52,310
那种情况不见了，但让我们假装它将永远存在
that case it's gone but let's just

85
00:02:52,509 --> 00:02:54,020
那种情况不见了，但让我们假装它将永远存在
pretend that it's gonna be here forever

86
00:02:54,219 --> 00:02:55,880
因为使用它可能会如此，您只需将城市写成任何类似
because it might so to use this you

87
00:02:56,080 --> 00:02:58,880
因为使用它可能会如此，您只需将城市写成任何类似
simply write s city any it's much like

88
00:02:59,080 --> 00:03:00,980
变体，因为没有模板参数，因为可以
variant except there are no template

89
00:03:01,180 --> 00:03:03,200
变体，因为没有模板参数，因为可以
arguments because of course it's it can

90
00:03:03,400 --> 00:03:05,599
存储任何类型，然后我们将为变量输入名称，将其称为数据
store any type then we just will type in

91
00:03:05,799 --> 00:03:07,789
存储任何类型，然后我们将为变量输入名称，将其称为数据
a name for our variable call it data you

92
00:03:07,989 --> 00:03:09,920
如果您想从SUV建造东西，可以使用称为SUV的东西
can use something called SUV make any if

93
00:03:10,120 --> 00:03:11,840
如果您想从SUV建造东西，可以使用称为SUV的东西
you want to construct something from

94
00:03:12,039 --> 00:03:13,610
在这里，但我们要做的就是在这里保持空白，然后
here but what we'll do is we'll just

95
00:03:13,810 --> 00:03:15,500
在这里，但我们要做的就是在这里保持空白，然后
kind of keep it empty here and then just

96
00:03:15,699 --> 00:03:17,270
设置数据等于两个，例如，然后我们可以将数据设置为等于
set data equal to like two for example

97
00:03:17,469 --> 00:03:19,520
设置数据等于两个，例如，然后我们可以将数据设置为等于
and then we can set data equal to a

98
00:03:19,719 --> 00:03:23,900
恒星指针，或者我们可以说死亡或等于字符串，在这种情况下，我们将
constant star pointer or we can say died

99
00:03:24,099 --> 00:03:26,060
恒星指针，或者我们可以说死亡或等于字符串，在这种情况下，我们将
or equal to a string in which case we'll

100
00:03:26,259 --> 00:03:27,530
只是写这样的东西，否则我们可以将其设置为等于你们
just write something like this or we

101
00:03:27,729 --> 00:03:28,819
只是写这样的东西，否则我们可以将其设置为等于你们
could set it equal to you guys kind of

102
00:03:29,019 --> 00:03:30,469
得到一点，就是将其设置为等于绝对喜欢任何东西，因为那是
get the point you just set it equal to

103
00:03:30,669 --> 00:03:32,300
得到一点，就是将其设置为等于绝对喜欢任何东西，因为那是
like absolutely anything because that's

104
00:03:32,500 --> 00:03:35,118
如果您想检索数据，那将是什么事情
what it is any thing if you want to

105
00:03:35,318 --> 00:03:36,349
如果您想检索数据，那将是什么事情
retrieve your data you're gonna have to

106
00:03:36,549 --> 00:03:37,819
做更多的工作，您实际上必须知道它是哪种类型，然后
do a little bit more work you'll have to

107
00:03:38,019 --> 00:03:39,800
做更多的工作，您实际上必须知道它是哪种类型，然后
actually know which type it is and then

108
00:03:40,000 --> 00:03:41,689
将其转换为该类型，您可以通过使用s city来执行任何操作
kind of cast it into that type and you

109
00:03:41,889 --> 00:03:43,909
将其转换为该类型，您可以通过使用s city来执行任何操作
can do that by using s city any cost

110
00:03:44,109 --> 00:03:46,460
然后只需输入要转换为STD字符串的类型，然后
then you just put in the type that you

111
00:03:46,659 --> 00:03:49,310
然后只需输入要转换为STD字符串的类型，然后
want to cast to like STD string and then

112
00:03:49,509 --> 00:03:51,950
当然，实际的任何变量都会进入参数中，这将
of course the actual any variable goes

113
00:03:52,150 --> 00:03:54,110
当然，实际的任何变量都会进入参数中，这将
into the parameter here now this will

114
00:03:54,310 --> 00:03:56,240
如果数据不是该类型，则抛出有关任何强制转换异常的异常
throw an exception about any cast

115
00:03:56,439 --> 00:03:58,939
如果数据不是该类型，则抛出有关任何强制转换异常的异常
exception if the data is not of the type

116
00:03:59,139 --> 00:04:00,950
您要尝试插入的内容，请记住这一点，
that you're trying to cast into so just

117
00:04:01,150 --> 00:04:02,900
您要尝试插入的内容，请记住这一点，
keep that in mind there's a course for

118
00:04:03,099 --> 00:04:05,210
返回一个字符串，以便我们可以像这样分配它，所以现在乍一看，这看起来像
return a string so we can assign it like

119
00:04:05,409 --> 00:04:07,849
返回一个字符串，以便我们可以像这样分配它，所以现在乍一看，这看起来像
so now first glance this may seem like

120
00:04:08,049 --> 00:04:10,640
它与STD变体几乎相同，这是因为它很相似
it's pretty much the same as STD variant

121
00:04:10,840 --> 00:04:12,740
它与STD变体几乎相同，这是因为它很相似
and that's because well it's similar

122
00:04:12,939 --> 00:04:15,530
正确，我们可以在此处存储任何类型，而STD变体要求我们列出所有
right we can store any type here whereas

123
00:04:15,729 --> 00:04:17,509
正确，我们可以在此处存储任何类型，而STD变体要求我们列出所有
STD variant requires us to list all of

124
00:04:17,709 --> 00:04:20,389
我们的类型，但看到虽然很多人可能认为这是非常
our types but see whilst a lot of people

125
00:04:20,589 --> 00:04:22,540
我们的类型，但看到虽然很多人可能认为这是非常
might think that s to be very

126
00:04:22,740 --> 00:04:24,189
更糟糕的是，因为您不必在此处列出所有类型的单词
is worse because you have to list all

127
00:04:24,389 --> 00:04:25,270
更糟糕的是，因为您不必在此处列出所有类型的单词
your types words here you don't have to

128
00:04:25,470 --> 00:04:27,819
完全担心类型，这就是为什么SUT变体实际上比
worry about types at all that's kind of

129
00:04:28,019 --> 00:04:29,740
完全担心类型，这就是为什么SUT变体实际上比
why SUT variant is actually better than

130
00:04:29,939 --> 00:04:31,749
SCT几乎所有您需要处理的东西我都得到了
SCT anything pretty much anything you

131
00:04:31,949 --> 00:04:33,218
SCT几乎所有您需要处理的东西我都得到了
need to deal with I'm really getting

132
00:04:33,418 --> 00:04:34,960
SUV变体要求随时都在说这个词
annoyed with saying the word any all the

133
00:04:35,160 --> 00:04:37,420
SUV变体要求随时都在说这个词
time the fact that SUV variant requires

134
00:04:37,620 --> 00:04:39,338
您列出所有这些类型是好的，这就是使其安全的原因
you to list out all of those types is

135
00:04:39,538 --> 00:04:42,040
您列出所有这些类型是好的，这就是使其安全的原因
good that's what makes it type safe for

136
00:04:42,240 --> 00:04:44,050
例如，我们可能会忘记明确地写出一个事实，即这是一个字符串，
example we might forget to explicitly

137
00:04:44,250 --> 00:04:45,699
例如，我们可能会忘记明确地写出一个事实，即这是一个字符串，
write the fact that this is a string and

138
00:04:45,899 --> 00:04:47,139
我们可能会尝试编写一些这样的代码，这看起来非常合理，我们
we might try and write some code like

139
00:04:47,339 --> 00:04:48,879
我们可能会尝试编写一些这样的代码，这看起来非常合理，我们
this this looks perfectly reasonable we

140
00:04:49,079 --> 00:04:50,740
将数据设置为两个，将数据设置为Cherno，它是一个字符串，然后尝试
set data to two we set data to Cherno

141
00:04:50,939 --> 00:04:52,540
将数据设置为两个，将数据设置为Cherno，它是一个字符串，然后尝试
which is a string and then we try and

142
00:04:52,740 --> 00:04:54,430
拿出那个字符串，但这是行不通的，因为这个chat不休
get that string out however that's not

143
00:04:54,629 --> 00:04:57,790
拿出那个字符串，但这是行不通的，因为这个chat不休
going to work because well this chatter

144
00:04:57,990 --> 00:05:00,009
不是一个字符串，是一个常量，我们的指针，如果
is not a string is it it's just a

145
00:05:00,209 --> 00:05:02,949
不是一个字符串，是一个常量，我们的指针，如果
constant our pointer whereas if we were

146
00:05:03,149 --> 00:05:05,379
在这种确切的情况下使用变体，那么我们的代码将类似于
using variant in this exact scenario

147
00:05:05,579 --> 00:05:07,149
在这种确切的情况下使用变体，那么我们的代码将类似于
then our code would look something like

148
00:05:07,348 --> 00:05:09,790
这我们可能有int我们现在可能有STD字符串当然可以了
this we might have int we might have STD

149
00:05:09,990 --> 00:05:11,889
这我们可能有int我们现在可能有STD字符串当然可以了
string now of course this will work

150
00:05:12,089 --> 00:05:13,930
正确地，我们将不得不更改它，以得到可能的获得，因为这将
correctly and we'd have to change this

151
00:05:14,129 --> 00:05:16,959
正确地，我们将不得不更改它，以得到可能的获得，因为这将
to be get maybe get if because this will

152
00:05:17,158 --> 00:05:18,490
隐式转换为字符串，因为它必须是字符串或整数
get implicitly cast into a string cause

153
00:05:18,689 --> 00:05:21,520
隐式转换为字符串，因为它必须是字符串或整数
it has to be either a string or an int

154
00:05:21,720 --> 00:05:23,170
因此，除了以下事实外，变体之间还有任何区别
so are there any differences between

155
00:05:23,370 --> 00:05:25,718
因此，除了以下事实外，变体之间还有任何区别
variant and any apart from the fact that

156
00:05:25,918 --> 00:05:27,370
当然，变体需要您很好地列出类型，这是与
variant of course requires you to list

157
00:05:27,569 --> 00:05:29,829
当然，变体需要您很好地列出类型，这是与
out the types well yes it's to do with

158
00:05:30,029 --> 00:05:32,110
它们如何存储变体只是一种类型安全的联合，这意味着
how they're stored variant is simply a

159
00:05:32,310 --> 00:05:34,509
它们如何存储变体只是一种类型安全的联合，这意味着
type safe Union what that means is that

160
00:05:34,709 --> 00:05:37,959
它将所有数据存储在一个联合体中，如果不是
it stores all of its data in basically a

161
00:05:38,158 --> 00:05:39,579
它将所有数据存储在一个联合体中，如果不是
union and again if you guys are not

162
00:05:39,779 --> 00:05:40,959
熟悉的是，右上角有一个关于工会的视频
familiar with that there is a video in

163
00:05:41,158 --> 00:05:44,019
熟悉的是，右上角有一个关于工会的视频
the top right corner just about unions

164
00:05:44,218 --> 00:05:47,379
有什么好处吗，让我们看一下是否只是进入这个头文件
what does any do well let's take a look

165
00:05:47,579 --> 00:05:50,468
有什么好处吗，让我们看一下是否只是进入这个头文件
if we just go into this header file then

166
00:05:50,668 --> 00:05:52,569
我们可以看到很多人的所有代码问我这是如何工作的
we can see all of the code for any a lot

167
00:05:52,769 --> 00:05:54,610
我们可以看到很多人的所有代码问我这是如何工作的
of people ask me how does this work how

168
00:05:54,810 --> 00:05:56,829
那行得通吗，我的答案是看看
does that work and my answer is just

169
00:05:57,029 --> 00:05:58,209
那行得通吗，我的答案是看看
take a look

170
00:05:58,408 --> 00:06:00,459
STL是sanno模板库，它在头文件和
STL is the sanno template library it's

171
00:06:00,658 --> 00:06:02,620
STL是sanno模板库，它在头文件和
implemented in all in header files and

172
00:06:02,819 --> 00:06:04,360
您已经在计算机上拥有了所有这些源代码的文件，因此
you've got all of those files all that

173
00:06:04,560 --> 00:06:05,769
您已经在计算机上拥有了所有这些源代码的文件，因此
source code on your computer so just

174
00:06:05,968 --> 00:06:07,629
打开它们并尝试阅读它，我知道有时可能会因为
open them up and try and read it I know

175
00:06:07,829 --> 00:06:09,249
打开它们并尝试阅读它，我知道有时可能会因为
it can be hard sometimes because of the

176
00:06:09,449 --> 00:06:11,319
这样写，但是慢慢来，你会惊讶于
way that is written but just take it

177
00:06:11,519 --> 00:06:12,879
这样写，但是慢慢来，你会惊讶于
slowly and you'll be surprised at how

178
00:06:13,079 --> 00:06:14,680
您实际上会了解很多，因此查看此头文件，我们可以开始
much you'll actually understand so

179
00:06:14,879 --> 00:06:16,778
您实际上会了解很多，因此查看此头文件，我们可以开始
looking at this header file we can start

180
00:06:16,978 --> 00:06:18,399
拼凑这实际上是如何工作的
to piece together how this actually

181
00:06:18,598 --> 00:06:19,149
拼凑这实际上是如何工作的
works

182
00:06:19,348 --> 00:06:21,399
我们知道存储似乎是可以很好地存储某些内容的变量，
we know that storage seems to be the

183
00:06:21,598 --> 00:06:23,588
我们知道存储似乎是可以很好地存储某些内容的变量，
variable that stores well something and

184
00:06:23,788 --> 00:06:25,269
您可以看到它实际上是一个联合，让我们看一下
you can see that what it is is actually

185
00:06:25,468 --> 00:06:27,189
您可以看到它实际上是一个联合，让我们看一下
a union let's take a look at what

186
00:06:27,389 --> 00:06:29,379
存储T是一个结构，似乎有一个小存储，一个大存储
storage T is it's a struct which seems

187
00:06:29,579 --> 00:06:31,810
存储T是一个结构，似乎有一个小存储，一个大存储
to have a small storage of big storage

188
00:06:32,009 --> 00:06:33,968
一个统一的联盟，如果我们实际上看看例如什么大存储T
an aligned Union and if we actually take

189
00:06:34,168 --> 00:06:35,920
一个统一的联盟，如果我们实际上看看例如什么大存储T
a look at what big storage T for example

190
00:06:36,120 --> 00:06:38,439
这是一个空指针，周围是一堆填充物和小的存储空间
is it's a void pointer surrounded by a

191
00:06:38,639 --> 00:06:40,870
这是一个空指针，周围是一堆填充物和小的存储空间
bunch of padding and small storage is

192
00:06:41,069 --> 00:06:43,600
实际上只是一个对齐的Union T，我们深入了解这是一个
actually just an aligned Union T which

193
00:06:43,800 --> 00:06:46,749
实际上只是一个对齐的Union T，我们深入了解这是一个
digging in even deeper we realize is an

194
00:06:46,949 --> 00:06:49,718
所有类型的联合，所以它实际上是什么，我
aligned union of all of the types so

195
00:06:49,918 --> 00:06:52,090
所有类型的联合，所以它实际上是什么，我
what s it e an e actually does and I

196
00:06:52,290 --> 00:06:54,278
认为这对于小型类型是相当聪明的，它只是将它们存储为一个联合
think this is quite clever is for small

197
00:06:54,478 --> 00:06:56,770
认为这对于小型类型是相当聪明的，它只是将它们存储为一个联合
types it just stores them as a union

198
00:06:56,970 --> 00:06:58,600
这意味着对于小型字体，其工作方式与
which means that for small types it

199
00:06:58,800 --> 00:07:00,400
这意味着对于小型字体，其工作方式与
works in exactly the same way as a

200
00:07:00,600 --> 00:07:02,860
但是如果您有一个大类型，它实际上会带您进入
variant however if you have a large type

201
00:07:03,060 --> 00:07:04,778
但是如果您有一个大类型，它实际上会带您进入
that's where it actually takes you into

202
00:07:04,978 --> 00:07:07,180
空指针大存储，在这种情况下，它实际上是动态的
that void pointer big storage and in

203
00:07:07,379 --> 00:07:09,069
空指针大存储，在这种情况下，它实际上是动态的
that case it will actually dynamically

204
00:07:09,269 --> 00:07:11,800
现在分配内存动态分配内存不利于
allocate memory now dynamically

205
00:07:12,000 --> 00:07:13,660
现在分配内存动态分配内存不利于
allocating memory is not good for

206
00:07:13,860 --> 00:07:15,699
性能，以总结使用变体或SUV Eddy时的工作原理
performance so to sum up how this works

207
00:07:15,899 --> 00:07:17,980
性能，以总结使用变体或SUV Eddy时的工作原理
if you're using a variant or SUV Eddy

208
00:07:18,180 --> 00:07:20,230
像整数浮点这样的小类型，您可能知道像类的向量
with small types like integers floats

209
00:07:20,430 --> 00:07:22,930
像整数浮点这样的小类型，您可能知道像类的向量
you know maybe like a vector for class

210
00:07:23,129 --> 00:07:24,610
或诸如此类的东西在数学库中，您将要飞行我的意思是他们在
or something like that like in a math

211
00:07:24,810 --> 00:07:26,829
或诸如此类的东西在数学库中，您将要飞行我的意思是他们在
library you'll be flying I mean they're

212
00:07:27,029 --> 00:07:28,900
将以完全相同的方式工作，您可以通过在此处查看
gonna work in exactly the same way and

213
00:07:29,100 --> 00:07:30,430
将以完全相同的方式工作，您可以通过在此处查看
you can see by looking here at the

214
00:07:30,629 --> 00:07:32,528
源代码表明，一直到这里的那个小存储都碰巧
source code that that small storage

215
00:07:32,728 --> 00:07:34,420
源代码表明，一直到这里的那个小存储都碰巧
which is all the way up here happens to

216
00:07:34,620 --> 00:07:36,460
如果我们看一下这个Align Union，特别是有很小的空间
be if we take a look at this align Union

217
00:07:36,660 --> 00:07:38,499
如果我们看一下这个Align Union，特别是有很小的空间
and specifically there's any small space

218
00:07:38,699 --> 00:07:40,540
大小变量，您可以看到现在大约是32个字节
size variable you can see that it is

219
00:07:40,740 --> 00:07:43,120
大小变量，您可以看到现在大约是32个字节
around 32 bytes now this may be

220
00:07:43,319 --> 00:07:45,278
特定于实现的显然我正在使用Visual Studio和MSB C，它是32
implementation specific obviously I'm

221
00:07:45,478 --> 00:07:48,430
特定于实现的显然我正在使用Visual Studio和MSB C，它是32
using visual studio with MSB C it's 32

222
00:07:48,629 --> 00:07:50,588
字节，因此如果您的存储空间超过了该点，则SC DNA将会
bytes so if you have more storage than

223
00:07:50,788 --> 00:07:52,838
字节，因此如果您的存储空间超过了该点，则SC DNA将会
that at that point SC DNA will

224
00:07:53,038 --> 00:07:55,329
动态分配，但是SCV变体不会如此，换句话说，除了
dynamically allocate however SCV variant

225
00:07:55,529 --> 00:07:57,610
动态分配，但是SCV变体不会如此，换句话说，除了
will not so in other words apart from

226
00:07:57,810 --> 00:07:59,889
具有更好的类型安全性和更严格的限制，这是一件好事
being more typesafe and a little bit

227
00:08:00,089 --> 00:08:01,468
具有更好的类型安全性和更严格的限制，这是一件好事
more restrictive which is a good thing

228
00:08:01,668 --> 00:08:03,939
如果您碰巧遇到较大的变化，则每个变体的性能也会更快
every variant is also going to perform

229
00:08:04,139 --> 00:08:06,069
如果您碰巧遇到较大的变化，则每个变体的性能也会更快
faster if you happen to deal with larger

230
00:08:06,269 --> 00:08:08,230
数据，或者您想避免动态内存分配，我可以提供另一个提示
data or you want to avoid dynamic memory

231
00:08:08,430 --> 00:08:09,999
数据，或者您想避免动态内存分配，我可以提供另一个提示
allocation another tip that I can give

232
00:08:10,199 --> 00:08:12,069
您要获得更快的性能，是要确保您不复制数据，以便您可以
you for faster performance is to make

233
00:08:12,269 --> 00:08:13,718
您要获得更快的性能，是要确保您不复制数据，以便您可以
sure that you don't copy data so you can

234
00:08:13,918 --> 00:08:15,310
看到这里带有字符串，我当然要复制它，如果我们回到我们的
see that over here with string I am of

235
00:08:15,509 --> 00:08:17,560
看到这里带有字符串，我当然要复制它，如果我们回到我们的
course copying it if we go back to our

236
00:08:17,759 --> 00:08:19,718
sed您需要做的任何事情以确保返回
sed any way of doing things

237
00:08:19,918 --> 00:08:20,949
sed您需要做的任何事情以确保返回
you need to make sure that you return

238
00:08:21,149 --> 00:08:22,990
通过引用，如果您这样做，您会发现它本身不起作用
this by reference and if you do that

239
00:08:23,189 --> 00:08:25,028
通过引用，如果您这样做，您会发现它本身不起作用
you'll see it doesn't work natively like

240
00:08:25,228 --> 00:08:26,560
因此，这实际上可以与SVG一起使用
this so this is something that actually

241
00:08:26,759 --> 00:08:29,079
因此，这实际上可以与SVG一起使用
would have worked with SVG yet with any

242
00:08:29,279 --> 00:08:30,730
费用并不能确保您确实将引用粘贴到
cost it doesn't make sure that you

243
00:08:30,930 --> 00:08:32,769
费用并不能确保您确实将引用粘贴到
actually stick the reference into the

244
00:08:32,969 --> 00:08:35,049
如果您通过引用平底船返回，则模板参数在这里
template argument here if you punt

245
00:08:35,250 --> 00:08:37,149
如果您通过引用平底船返回，则模板参数在这里
return by reference and that will of

246
00:08:37,349 --> 00:08:38,740
当然，请确保所有内容都进行了优化，并且您可以在
course make sure that everything is

247
00:08:38,940 --> 00:08:40,990
当然，请确保所有内容都进行了优化，并且您可以在
nicely optimized and you can see that in

248
00:08:41,190 --> 00:08:43,120
这种实际情况显然要确保我们在我们的实际使用sed字符串
this actual case obviously make sure

249
00:08:43,320 --> 00:08:45,729
这种实际情况显然要确保我们在我们的实际使用sed字符串
that we actually use sed string in our

250
00:08:45,929 --> 00:08:47,258
控制点在这种情况下，我们实际上不会获得任何内存
control point in this case we won't

251
00:08:47,458 --> 00:08:48,250
控制点在这种情况下，我们实际上不会获得任何内存
actually get any memory

252
00:08:48,450 --> 00:08:50,259
除了可能来自字符串的分配之外，它的分配是
allocations apart from the one that will

253
00:08:50,460 --> 00:08:52,089
除了可能来自字符串的分配之外，它的分配是
probably come up from string and it's

254
00:08:52,289 --> 00:08:53,799
实际上非常容易测试，您可以将其中的一个替换为new
actually very easy to test this you can

255
00:08:54,000 --> 00:08:56,019
实际上非常容易测试，您可以将其中的一个替换为new
just replace operator new with one of

256
00:08:56,220 --> 00:08:58,329
自己写一些简单的代码，这不是一个很好的选择
your own by writing some simple code

257
00:08:58,529 --> 00:09:01,779
自己写一些简单的代码，这不是一个很好的选择
like this this is not a complete good

258
00:09:01,980 --> 00:09:04,629
新运算符的实现，因此请勿在任何生产代码中使用
implementation of the new operator so

259
00:09:04,830 --> 00:09:06,759
新运算符的实现，因此请勿在任何生产代码中使用
don't use this in any production code

260
00:09:06,960 --> 00:09:07,990
显然，但是如果我们在此处插入一个断点，那么我们现在将意识到
obviously but if we stick a breakpoint

261
00:09:08,190 --> 00:09:09,969
显然，但是如果我们在此处插入一个断点，那么我们现在将意识到
in here then we'll now be aware of all

262
00:09:10,169 --> 00:09:12,279
内存分配，如果我按f5键运行该程序，您会看到我们
memory allocations and if I hit f5 to

263
00:09:12,480 --> 00:09:14,319
内存分配，如果我按f5键运行该程序，您会看到我们
run this program you'll see that we do

264
00:09:14,519 --> 00:09:16,029
得到一个内存分配，但是如果我们看看它实际上来自哪里
get a memory allocation but if we take a

265
00:09:16,230 --> 00:09:17,349
得到一个内存分配，但是如果我们看看它实际上来自哪里
look at where it comes from it actually

266
00:09:17,549 --> 00:09:19,870
来自城市字符串，因此您可以在这里查看是否我将调用堆栈
comes from a city string so you can see

267
00:09:20,070 --> 00:09:21,279
来自城市字符串，因此您可以在这里查看是否我将调用堆栈
over here if I make the call stack a

268
00:09:21,480 --> 00:09:22,209
稍大一点，我们有SCV基本字符串，然后
little bit bigger

269
00:09:22,409 --> 00:09:25,149
稍大一点，我们有SCV基本字符串，然后
we have SCV basic string which then goes

270
00:09:25,350 --> 00:09:27,099
进入分配器，如果我回到main，我将逐步执行该代码
into the allocator and if I go back to

271
00:09:27,299 --> 00:09:29,709
进入分配器，如果我回到main，我将逐步执行该代码
main and I just step through that code a

272
00:09:29,909 --> 00:09:31,329
一点点，所以我们回到这里，我们将走出那一步，然后
little bit so we'll go back up here

273
00:09:31,529 --> 00:09:34,089
一点点，所以我们回到这里，我们将走出那一步，然后
we'll just step out of that and then

274
00:09:34,289 --> 00:09:35,709
如果我们稍微看一下调用堆栈，请转到下一行
just go to the next line if we take a

275
00:09:35,909 --> 00:09:37,179
如果我们稍微看一下调用堆栈，请转到下一行
look at this call stack a little bit

276
00:09:37,379 --> 00:09:39,159
实际上，实际上看起来好像是进行分配的任何人
more closely it might actually seem like

277
00:09:39,360 --> 00:09:40,959
实际上，实际上看起来好像是进行分配的任何人
any is the one doing the allocation

278
00:09:41,159 --> 00:09:42,039
因为您可以看到它来自赋值运算符
because you can see it comes from the

279
00:09:42,240 --> 00:09:44,649
因为您可以看到它来自赋值运算符
assignment operator to any but all any

280
00:09:44,850 --> 00:09:46,689
实际上正在做的是在构造中转发所有这些参数
is actually doing is doing a construct

281
00:09:46,889 --> 00:09:48,459
实际上正在做的是在构造中转发所有这些参数
in place forwarding all those arguments

282
00:09:48,659 --> 00:09:50,979
这是您可以看到再次分配的基本字符串，因此f5可以
and it's the basic string you can see

283
00:09:51,179 --> 00:09:53,979
这是您可以看到再次分配的基本字符串，因此f5可以
that is allocating once again so f5 to

284
00:09:54,179 --> 00:09:56,199
继续运行它，我们又有了一个基本的字符串，那就是所有的
continue running this and again we have

285
00:09:56,399 --> 00:09:58,209
继续运行它，我们又有了一个基本的字符串，那就是所有的
a basic string and that is all of the

286
00:09:58,409 --> 00:10:00,069
该程序的分配，因此在这种情况下，suv实际上没有分配
allocations from this program so in that

287
00:10:00,269 --> 00:10:03,069
该程序的分配，因此在这种情况下，suv实际上没有分配
case s-u-v any actually did not allocate

288
00:10:03,269 --> 00:10:05,439
如果我们要开设某种比
anything if however we were to have some

289
00:10:05,639 --> 00:10:08,529
如果我们要开设某种比
kind of class that would have more than

290
00:10:08,730 --> 00:10:10,419
32个字节，因此，例如，如果有两个字符串s0和s1，我将在此处具有一个结构
32 bytes so for example I'll have a

291
00:10:10,620 --> 00:10:14,439
32个字节，因此，例如，如果有两个字符串s0和s1，我将在此处具有一个结构
struct here of two strings s0 and s1 if

292
00:10:14,639 --> 00:10:17,379
我会尝试并在这里进行实际设置，我将只使用自定义类，
I do try and actually set this up over

293
00:10:17,580 --> 00:10:19,779
我会尝试并在这里进行实际设置，我将只使用自定义类，
here I'll just use custom class and we

294
00:10:19,980 --> 00:10:23,169
将删除此任播，如果我跳过了此基本字符串，则如果我有f5
will remove this anycast and if I had f5

295
00:10:23,370 --> 00:10:25,419
将删除此任播，如果我跳过了此基本字符串，则如果我有f5
if I skip past this basic string

296
00:10:25,620 --> 00:10:26,949
分配当然来自该自定义的成员变量
allocation that of course is coming from

297
00:10:27,149 --> 00:10:28,750
分配当然来自该自定义的成员变量
the member variables of that custom

298
00:10:28,950 --> 00:10:30,069
类，然后您将看到我们在这里拥有的是赋值运算符和
class then you'll see that what we have

299
00:10:30,269 --> 00:10:32,589
类，然后您将看到我们在这里拥有的是赋值运算符和
here is that assignment operator and

300
00:10:32,789 --> 00:10:35,740
在安装过程中，您会看到这是一个很大的存储操作，
during the emplacement you can see that

301
00:10:35,940 --> 00:10:38,829
在安装过程中，您会看到这是一个很大的存储操作，
this is a big storage operation which of

302
00:10:39,029 --> 00:10:41,620
当然实际上在这里需要新的知识，所以请注意sed NE将
course does actually call new over here

303
00:10:41,820 --> 00:10:43,569
当然实际上在这里需要新的知识，所以请注意sed NE将
so just be aware that sed NE will

304
00:10:43,769 --> 00:10:45,549
分配是否需要，在这种情况下，如果您要存储的数据
allocate if it needs to and in this case

305
00:10:45,750 --> 00:10:46,870
分配是否需要，在这种情况下，如果您要存储的数据
if the data that you're trying to store

306
00:10:47,070 --> 00:10:49,419
超过32个字节，您可以看到它调用了new并导致了动态内存
is above 32 bytes you can see that it

307
00:10:49,620 --> 00:10:51,370
超过32个字节，您可以看到它调用了new并导致了动态内存
calls new and causes a dynamic memory

308
00:10:51,570 --> 00:10:53,289
分配，所以回到什么时候应该使用sed的问题
allocation so back to the question of

309
00:10:53,490 --> 00:10:56,379
分配，所以回到什么时候应该使用sed的问题
when to use sed any when should it be

310
00:10:56,580 --> 00:10:59,139
使用这是一个棘手的事情，很多人会平淡地告诉你
used this is a tricky one a lot of

311
00:10:59,340 --> 00:11:01,370
使用这是一个棘手的事情，很多人会平淡地告诉你
people will just flat-out tell you that

312
00:11:01,570 --> 00:11:03,889
这只是一点用处，老实说，我倾向于同意
it's just a bit useless and to be honest

313
00:11:04,090 --> 00:11:07,669
这只是一点用处，老实说，我倾向于同意
I'm kind of inclined to agree when well

314
00:11:07,870 --> 00:11:09,799
如果您想存储多个数据，请看SED Annie的好用例是什么
look what is a good use case for SED

315
00:11:10,000 --> 00:11:12,229
如果您想存储多个数据，请看SED Annie的好用例是什么
Annie if you want to store multiple data

316
00:11:12,429 --> 00:11:15,259
单个变量中的类型使用SV变体，基本上是类型安全的
types in a single variable use SV

317
00:11:15,460 --> 00:11:17,209
单个变量中的类型使用SV变体，基本上是类型安全的
variant it's basically a type safe

318
00:11:17,409 --> 00:11:19,459
STD版本，表示您无法将其设置为任何类型
version of STD any meaning that you

319
00:11:19,659 --> 00:11:21,559
STD版本，表示您无法将其设置为任何类型
can't set it to whatever type you please

320
00:11:21,759 --> 00:11:24,500
偶然地，并且它也不会动态分配内存，所以它将
accidentally and also it's not going to

321
00:11:24,700 --> 00:11:26,329
偶然地，并且它也不会动态分配内存，所以它将
dynamically allocate memory so it will

322
00:11:26,529 --> 00:11:28,219
如果您需要能够存储任何数据的要求，则性能会更好
perform better if you have the need the

323
00:11:28,419 --> 00:11:30,439
如果您需要能够存储任何数据的要求，则性能会更好
requirement to be able to store any data

324
00:11:30,639 --> 00:11:34,279
输入单个变量可能会重新考虑您程序的设计，我是说我
type in a single variable maybe rethink

325
00:11:34,480 --> 00:11:36,469
输入单个变量可能会重新考虑您程序的设计，我是说我
the design of your program I mean I

326
00:11:36,669 --> 00:11:40,219
老实说，真的很像是想一个有效的用例，我认为
honestly really like kind of think of a

327
00:11:40,419 --> 00:11:42,049
老实说，真的很像是想一个有效的用例，我认为
valid use case for this I think that

328
00:11:42,250 --> 00:11:44,599
我认为这是您可能永远不应该使用的东西，如果
it's I think that it's something that

329
00:11:44,799 --> 00:11:46,279
我认为这是您可能永远不应该使用的东西，如果
you probably should never use I mean if

330
00:11:46,480 --> 00:11:48,439
您就像一个数据缓冲区，而且您绝对不知道那是什么
you have like a buffer of data and you

331
00:11:48,639 --> 00:11:50,120
您就像一个数据缓冲区，而且您绝对不知道那是什么
have like absolutely no idea what that

332
00:11:50,320 --> 00:11:51,469
外在是你只想要你只想指向它，那很好你
outer is you just want to you just want

333
00:11:51,669 --> 00:11:53,059
外在是你只想要你只想指向它，那很好你
to point it to it then that's fine you

334
00:11:53,259 --> 00:11:55,699
可以为此使用void指针，但这就像完全是一样
can use a void pointer for that but this

335
00:11:55,899 --> 00:11:57,859
可以为此使用void指针，但这就像完全是一样
is like this is something completely

336
00:11:58,059 --> 00:12:00,620
如果你们有什么用例的话，就不一样了
different if you guys have any good use

337
00:12:00,820 --> 00:12:01,849
如果你们有什么用例的话，就不一样了
cases if you guys have used this

338
00:12:02,049 --> 00:12:03,500
整个代码中，您认为它很有用，请在下面添加注释
throughout your code and you think that

339
00:12:03,700 --> 00:12:05,209
整个代码中，您认为它很有用，请在下面添加注释
it's useful please drop a comment below

340
00:12:05,409 --> 00:12:06,620
我很想读一些评论，然后看看，因为就目前而言，我
I'd love to read some of those comments

341
00:12:06,820 --> 00:12:10,129
我很想读一些评论，然后看看，因为就目前而言，我
and see because as it stands right now I

342
00:12:10,330 --> 00:12:12,500
不要认为这与可选或变体一样有用，
don't think this is anywhere near as

343
00:12:12,700 --> 00:12:14,959
不要认为这与可选或变体一样有用，
useful as optional or variant and it's

344
00:12:15,159 --> 00:12:17,990
我觉得很有趣，我想以这样的方式结束本系列，但这就是
kind of funny I guess to end to end the

345
00:12:18,190 --> 00:12:20,240
我觉得很有趣，我想以这样的方式结束本系列，但这就是
series on something like this but that's

346
00:12:20,440 --> 00:12:21,889
只是这样，我只是认为eeny是您应该做的事情
just the way it is I just think that s

347
00:12:22,090 --> 00:12:24,589
只是这样，我只是认为eeny是您应该做的事情
it eeny is something that you should

348
00:12:24,789 --> 00:12:26,539
一定要意识到，但也许不是，您应该使用所有这些
definitely be aware of but maybe not

349
00:12:26,740 --> 00:12:28,159
一定要意识到，但也许不是，您应该使用所有这些
something that you should use all that

350
00:12:28,360 --> 00:12:29,990
无论如何，我希望你们都喜欢这个视频，如果你能做到的话
often anyway I hope you guys enjoyed

351
00:12:30,190 --> 00:12:31,609
无论如何，我希望你们都喜欢这个视频，如果你能做到的话
this video if you did you can hit that

352
00:12:31,809 --> 00:12:33,529
像按钮一样，别忘了查看两个说明中的链接
like button also don't forget to check

353
00:12:33,730 --> 00:12:35,329
像按钮一样，别忘了查看两个说明中的链接
out the link in description for two

354
00:12:35,529 --> 00:12:37,729
个月的免费Skillshare会员资格，如果您还可以在下面发表评论
months of free Skillshare membership you

355
00:12:37,929 --> 00:12:39,589
个月的免费Skillshare会员资格，如果您还可以在下面发表评论
can also drop a comment below if you

356
00:12:39,789 --> 00:12:41,990
希望我涵盖C ++中的特定主题，并将其添加到我不断增长的主题中
want me to cover a specific topic in C++

357
00:12:42,190 --> 00:12:44,240
希望我涵盖C ++中的特定主题，并将其添加到我不断增长的主题中
and I will add it to my ever-growing

358
00:12:44,440 --> 00:12:46,129
列表，下次我会再见，你们再见
list I will see you guys next time

359
00:12:46,330 --> 00:12:48,329
列表，下次我会再见，你们再见
goodbye strong

360
00:12:48,529 --> 00:12:53,529
[音乐]
[Music]

