﻿1
00:00:00,030 --> 00:00:02,799
今天的厨房之一，因为我是为了伤害而工作，所以有时你会知道
one of the kitchen today because I'm

2
00:00:03,000 --> 00:00:08,020
今天的厨房之一，因为我是为了伤害而工作，所以有时你会知道
working from harm so you know sometimes

3
00:00:08,220 --> 00:00:09,580
我只需要在这里嘿，大家好，我叫安娜，欢迎回到我身边
I just have to be here hey what's up

4
00:00:09,779 --> 00:00:11,950
我只需要在这里嘿，大家好，我叫安娜，欢迎回到我身边
guys my name is Anna welcome back to my

5
00:00:12,150 --> 00:00:13,419
今天我们要讨论的最后一个系列是Elle价值观和我们的价值观
staples last series today we're gonna be

6
00:00:13,619 --> 00:00:16,949
今天我们要讨论的最后一个系列是Elle价值观和我们的价值观
talking about Elle values and our values

7
00:00:17,149 --> 00:00:19,179
那是变焦镜头吗？我今天通常不拍摄变焦镜头。Pam
so is it just a zoom lens I don't

8
00:00:19,379 --> 00:00:20,919
那是变焦镜头吗？我今天通常不拍摄变焦镜头。Pam
usually shoot one a zoom lens Pam today

9
00:00:21,118 --> 00:00:24,220
所以我必须要做的是，这将是一系列
so I had to I had to do that this is

10
00:00:24,420 --> 00:00:25,629
所以我必须要做的是，这将是一系列
gonna be the first video in a series of

11
00:00:25,829 --> 00:00:28,120
实质上是要谈论一些更高级订书钉的视频
videos which essentially is gonna be

12
00:00:28,320 --> 00:00:29,890
实质上是要谈论一些更高级订书钉的视频
talking about some more advanced staples

13
00:00:30,089 --> 00:00:31,898
加上包括移动语义在内的功能，我不知道这是否涵盖了移动
plus features including move semantics I

14
00:00:32,098 --> 00:00:33,489
加上包括移动语义在内的功能，我不知道这是否涵盖了移动
don't know if this will just cover move

15
00:00:33,689 --> 00:00:35,679
语义，因为此信息显然还有很多帮助
semantics because there's obviously a

16
00:00:35,880 --> 00:00:37,928
语义，因为此信息显然还有很多帮助
lot more that this information will help

17
00:00:38,128 --> 00:00:39,059
和您一起学习基本上是移动语义，但这是
you with and just learning about

18
00:00:39,259 --> 00:00:41,589
和您一起学习基本上是移动语义，但这是
basically move semantics but it is

19
00:00:41,789 --> 00:00:44,018
如果您不知道什么是l值，那么绝对是一个巨大的先决条件
definitely a huge prerequisite for that

20
00:00:44,219 --> 00:00:46,059
如果您不知道什么是l值，那么绝对是一个巨大的先决条件
if you don't know what an l-value is if

21
00:00:46,259 --> 00:00:47,140
你不知道什么是r值，那么它会有点
you don't know what an r-value is

22
00:00:47,340 --> 00:00:49,119
你不知道什么是r值，那么它会有点
especially then it's gonna be a little

23
00:00:49,320 --> 00:00:50,259
让您有点难以理解更高级的c ++
bit difficult for you to kind of

24
00:00:50,460 --> 00:00:53,320
让您有点难以理解更高级的c ++
understand how the more advanced c++

25
00:00:53,520 --> 00:00:55,448
诸如移动语义之类的功能看起来绝对是其中之一
features such as move semantics look

26
00:00:55,649 --> 00:00:56,948
诸如移动语义之类的功能看起来绝对是其中之一
this is definitely one of those things

27
00:00:57,149 --> 00:00:58,928
很难像我倾向于那样挥舞着我的手来解释
that is very difficult to just explain

28
00:00:59,128 --> 00:01:01,059
很难像我倾向于那样挥舞着我的手来解释
by waving my hands like I tend to do for

29
00:01:01,259 --> 00:01:02,320
我的大部分视频中，我们肯定会跳入一些代码，我将向您展示
most of my video we're definitely gonna

30
00:01:02,520 --> 00:01:03,759
我的大部分视频中，我们肯定会跳入一些代码，我将向您展示
jump into some code I'm gonna show you

31
00:01:03,960 --> 00:01:05,950
伙计们，这些东西是如何工作的，它是什么，更重要的是，为什么你在乎，
guys how this stuff works and what it is

32
00:01:06,150 --> 00:01:08,890
伙计们，这些东西是如何工作的，它是什么，更重要的是，为什么你在乎，
and more importantly why you care but

33
00:01:09,090 --> 00:01:10,629
首先，我要感谢Skillshare赞助这部影片，如果你们有
first I want to thank Skillshare for

34
00:01:10,829 --> 00:01:12,099
首先，我要感谢Skillshare赞助这部影片，如果你们有
sponsoring this video if you guys have

35
00:01:12,299 --> 00:01:14,200
还没有听说过Skillshare，但您显然没有看过我的大部分内容
not heard of Skillshare yet then you

36
00:01:14,400 --> 00:01:15,700
还没有听说过Skillshare，但您显然没有看过我的大部分内容
clearly haven't been watching most of my

37
00:01:15,900 --> 00:01:17,319
视频Skillshare是一个在线学习社区，这里有数百万个创意者
videos Skillshare is an online learning

38
00:01:17,519 --> 00:01:18,819
视频Skillshare是一个在线学习社区，这里有数百万个创意者
community where millions of creatives

39
00:01:19,019 --> 00:01:20,528
全世界好奇的人们聚集在一起，加深了现有的
and curious people all around the world

40
00:01:20,728 --> 00:01:22,840
全世界好奇的人们聚集在一起，加深了现有的
come together to deepen an existing

41
00:01:23,040 --> 00:01:25,359
热情或爱好，或者学习新的东西，这与其他在线方式不同
passion or hobby or learn something new

42
00:01:25,560 --> 00:01:27,250
热情或爱好，或者学习新的东西，这与其他在线方式不同
and unlike other kind of online

43
00:01:27,450 --> 00:01:29,709
社区Skillshare是专门为教您新知识而设计的
communities Skillshare is specifically

44
00:01:29,909 --> 00:01:32,709
社区Skillshare是专门为教您新知识而设计的
curated to teach you something new and

45
00:01:32,909 --> 00:01:34,628
像超级短片一样的视频超级易于理解的超级OnPoint
with videos that are like super short

46
00:01:34,828 --> 00:01:36,939
像超级短片一样的视频超级易于理解的超级OnPoint
super easy to understand super OnPoint

47
00:01:37,140 --> 00:01:39,730
这是一个非常有效的地方，可让您学习新知识，我知道很多
it's a really efficient place for you to

48
00:01:39,930 --> 00:01:41,528
这是一个非常有效的地方，可让您学习新知识，我知道很多
learn something new I know that a lot of

49
00:01:41,728 --> 00:01:43,480
人们现在对世界上正在发生的事情感到焦虑
people are anxious with what's going on

50
00:01:43,680 --> 00:01:44,679
人们现在对世界上正在发生的事情感到焦虑
in the world right now I know that I

51
00:01:44,879 --> 00:01:46,929
肯定是，我认为有创造力和学习一些东西真的很好
definitely am and I think being creative

52
00:01:47,129 --> 00:01:49,659
肯定是，我认为有创造力和学习一些东西真的很好
and learning something is a really good

53
00:01:49,859 --> 00:01:51,519
一种使您的思想摆脱困境，并加深一些您可能会想到的方法
way to kind of put your mind out of that

54
00:01:51,719 --> 00:01:54,340
一种使您的思想摆脱困境，并加深一些您可能会想到的方法
and also deepen something that you might

55
00:01:54,540 --> 00:01:55,899
一直想做的事，您将分享许多精彩的课程，
have always wanted to do you'll share

56
00:01:56,099 --> 00:01:57,819
一直想做的事，您将分享许多精彩的课程，
have so many amazing classes that will

57
00:01:58,019 --> 00:01:59,679
教你很多东西摄影摄影如何开始
teach you just so many things

58
00:01:59,879 --> 00:02:01,840
教你很多东西摄影摄影如何开始
photography videography how to start a

59
00:02:02,040 --> 00:02:03,429
YouTube频道和市场营销，并能成功开展业务
YouTube channel and market that and

60
00:02:03,629 --> 00:02:05,378
YouTube频道和市场营销，并能成功开展业务
start a successful business I mean

61
00:02:05,578 --> 00:02:06,819
说真的，他们甚至还开设了有关如何重新布置家具的课程
seriously they've even got classes on

62
00:02:07,019 --> 00:02:08,229
说真的，他们甚至还开设了有关如何重新布置家具的课程
how you can like rearrange the furniture

63
00:02:08,429 --> 00:02:10,920
就像我之前提到的那样使您的房间变得更好
in your room to make it better

64
00:02:11,120 --> 00:02:12,090
就像我之前提到的那样使您的房间变得更好
as I mentioned before I particularly

65
00:02:12,289 --> 00:02:13,950
喜欢他们的插图课，因为我认为艺术品是
love their illustration classes because

66
00:02:14,150 --> 00:02:15,630
喜欢他们的插图课，因为我认为艺术品是
I think that artwork is something that's

67
00:02:15,830 --> 00:02:18,030
很难很好地教书，我想我真的很喜欢这种方式
really hard to teach well and I guess I

68
00:02:18,229 --> 00:02:19,349
很难很好地教书，我想我真的很喜欢这种方式
just really enjoyed the way that

69
00:02:19,549 --> 00:02:21,660
Skillshare设法传达所有这些想法，每节课10美元
Skillshare manages to communicate all of

70
00:02:21,860 --> 00:02:23,730
Skillshare设法传达所有这些想法，每节课10美元
those ideas across and a lesson $10 a

71
00:02:23,930 --> 00:02:25,709
我认为这是一个非常有用的资产
month for an annual subscription I think

72
00:02:25,908 --> 00:02:28,110
我认为这是一个非常有用的资产
that is a really useful asset to have in

73
00:02:28,310 --> 00:02:29,969
您的学习，信息和技能库肯定足够好
your library of learning and information

74
00:02:30,169 --> 00:02:32,368
您的学习，信息和技能库肯定足够好
and skill sure have been nice enough to

75
00:02:32,568 --> 00:02:34,679
在下面的说明中提供使用我的链接注册的前1000名用户
offer the first 1,000 people who sign up

76
00:02:34,878 --> 00:02:36,450
在下面的说明中提供使用我的链接注册的前1000名用户
using my link in the description below

77
00:02:36,650 --> 00:02:40,080
两个月的免费Skillshare保费，特别是如果您现在在家中
two months of free Skillshare premium so

78
00:02:40,280 --> 00:02:41,670
两个月的免费Skillshare保费，特别是如果您现在在家中
especially if you're sitting at home now

79
00:02:41,870 --> 00:02:44,189
在这两个月的免费时间内，尝试一下，看看有什么新东西可以
in this period two months for free try

80
00:02:44,389 --> 00:02:46,800
在这两个月的免费时间内，尝试一下，看看有什么新东西可以
it out and see what new stuff you could

81
00:02:47,000 --> 00:02:48,810
再次对Skillshare表示感谢，感谢他不仅赞助了我，还赞助了其他人
learn huge thank you again to Skillshare

82
00:02:49,009 --> 00:02:51,300
再次对Skillshare表示感谢，感谢他不仅赞助了我，还赞助了其他人
for sponsoring not just me but other

83
00:02:51,500 --> 00:02:52,950
在这个艰难的时刻，我的YouTube伙伴们，我真的很喜欢与
fellow youtubers in this difficult time

84
00:02:53,150 --> 00:02:54,840
在这个艰难的时刻，我的YouTube伙伴们，我真的很喜欢与
I have to say I really like working with

85
00:02:55,039 --> 00:02:56,849
确实很棒并且Skillshare绝对是其中之一的公司
companies who are actually nice and

86
00:02:57,049 --> 00:02:58,920
确实很棒并且Skillshare绝对是其中之一的公司
Skillshare is definitely one of them

87
00:02:59,120 --> 00:03:00,569
在我们进入一些卡片之前，先看一下L值和我们的值
Before we jump into some card and take a

88
00:03:00,769 --> 00:03:02,219
在我们进入一些卡片之前，先看一下L值和我们的值
look at what L values and our values

89
00:03:02,419 --> 00:03:04,080
实际上我只是想向您大致解释它们是什么，为什么
actually I just kind of want to explain

90
00:03:04,280 --> 00:03:07,020
实际上我只是想向您大致解释它们是什么，为什么
to you roughly what they are and why

91
00:03:07,219 --> 00:03:08,399
重要的是要您认清它们是什么，而我们不是
it's important for you to kind of

92
00:03:08,598 --> 00:03:10,200
重要的是要您认清它们是什么，而我们不是
recognize what they are and we're not

93
00:03:10,400 --> 00:03:11,640
只是要谈论这个视频中的L值和我们的值
just going to be talking about L values

94
00:03:11,840 --> 00:03:12,990
只是要谈论这个视频中的L值和我们的值
and our values in this video we're going

95
00:03:13,189 --> 00:03:15,149
也可以触及l值参考，而我们的参考值是
to also be touching l value references

96
00:03:15,348 --> 00:03:17,490
也可以触及l值参考，而我们的参考值是
and our value of references these are

97
00:03:17,689 --> 00:03:20,640
我猜这是很多样本丢失程序，但是它们
kind of words I guess that a lot of

98
00:03:20,840 --> 00:03:22,110
我猜这是很多样本丢失程序，但是它们
samples loss program is here but they

99
00:03:22,310 --> 00:03:23,759
不太了解，尤其是如果您是非常规语言，您可能只是
don't quite understand and especially if

100
00:03:23,959 --> 00:03:25,649
不太了解，尤其是如果您是非常规语言，您可能只是
you're unusual language you might just

101
00:03:25,848 --> 00:03:28,319
由于出现诸如L值R值之类的单词而导致编译器错误
get compiler error as saying words like

102
00:03:28,519 --> 00:03:31,259
由于出现诸如L值R值之类的单词而导致编译器错误
L value R value must be an L value must

103
00:03:31,459 --> 00:03:33,330
是一个已知常数的可修改R值，您可能会喜欢，为什么
be a modifiable R value known constant

104
00:03:33,530 --> 00:03:35,280
是一个已知常数的可修改R值，您可能会喜欢，为什么
value and you might be like okay why is

105
00:03:35,479 --> 00:03:36,899
为什么编译器给我这种神秘的语言，为什么这种语言不能
why is the compiler giving me such

106
00:03:37,098 --> 00:03:38,789
为什么编译器给我这种神秘的语言，为什么这种语言不能
cryptic language why can't this language

107
00:03:38,989 --> 00:03:40,590
语言更加简单明了，为什么不能更多
be more straightforward language being

108
00:03:40,789 --> 00:03:42,300
语言更加简单明了，为什么不能更多
simple plus why can't it be more

109
00:03:42,500 --> 00:03:43,349
他们为什么必须要拥有所有这些花哨的单词，而这些单词是
straightforward why do they have to have

110
00:03:43,549 --> 00:03:45,149
他们为什么必须要拥有所有这些花哨的单词，而这些单词是
all these fancy words and the words are

111
00:03:45,348 --> 00:03:46,980
如果您了解它们的意思，就只有看中它们的意思
only fancy if you turn on what they mean

112
00:03:47,180 --> 00:03:48,868
如果您了解它们的意思，就只有看中它们的意思
if you know what the words mean they

113
00:03:49,068 --> 00:03:51,030
失去了幻想，他们实际上是有道理的，这个视频是
kind of lose their fanciness and they

114
00:03:51,229 --> 00:03:52,980
失去了幻想，他们实际上是有道理的，这个视频是
actually make sense and this video is

115
00:03:53,180 --> 00:03:54,990
希望能为你们清除很多东西
hopefully gonna clear a lot of that up

116
00:03:55,189 --> 00:03:55,530
希望能为你们清除很多东西
for you guys

117
00:03:55,729 --> 00:03:56,969
现在很多人称L值定位器值为
now a lot of people call L values

118
00:03:57,169 --> 00:03:58,618
现在很多人称L值定位器值为
locator values because they have some

119
00:03:58,818 --> 00:04:00,300
一会儿我们会看到这样的位置，但是有很多不同的方式
kind of location as we'll see in a

120
00:04:00,500 --> 00:04:01,679
一会儿我们会看到这样的位置，但是有很多不同的方式
minute but there are many different ways

121
00:04:01,878 --> 00:04:04,140
来定义L值和我们的值，我不认为您应该
to kind of define L values and our

122
00:04:04,340 --> 00:04:05,550
来定义L值和我们的值，我不认为您应该
values and I don't think that you should

123
00:04:05,750 --> 00:04:08,700
坚持尝试尝试基本上了解字母L的定义
stick to like trying to basically you

124
00:04:08,900 --> 00:04:10,319
坚持尝试尝试基本上了解字母L的定义
know find a definition for the letter L

125
00:04:10,519 --> 00:04:12,270
字母R，您只需要知道L值不是R值就不要尝试
of letter R you just need to know what

126
00:04:12,469 --> 00:04:14,219
字母R，您只需要知道L值不是R值就不要尝试
an L value isn't an R value is don't try

127
00:04:14,419 --> 00:04:15,480
并为自己定义它，因为我觉得那有点
and kind of define it for yourself

128
00:04:15,680 --> 00:04:17,000
并为自己定义它，因为我觉得那有点
because I feel like that's a little

129
00:04:17,199 --> 00:04:18,530
有点陷阱，所以让我们从一个关于L的超简单定义开始
a bit of a trap okay so let's start with

130
00:04:18,730 --> 00:04:20,509
有点陷阱，所以让我们从一个关于L的超简单定义开始
a super simple definition of what an L

131
00:04:20,709 --> 00:04:22,639
值是和R值是什么假设我有一个变量，我们就称它为
value is and what an R value is suppose

132
00:04:22,839 --> 00:04:24,290
值是和R值是什么假设我有一个变量，我们就称它为
I have a variable let's just call it I

133
00:04:24,490 --> 00:04:26,240
将其设置为十，我们对该表达式有两个部分，我们有一个左侧，
will set it to ten we have two parts to

134
00:04:26,439 --> 00:04:27,829
将其设置为十，我们对该表达式有两个部分，我们有一个左侧，
this expression we have a left side and

135
00:04:28,029 --> 00:04:30,170
右边，这也是思考L值的另一种好方法
a right side and this is also another

136
00:04:30,370 --> 00:04:32,000
右边，这也是思考L值的另一种好方法
good way to think about what an L value

137
00:04:32,199 --> 00:04:34,759
R值是L值是很多时候
is in what an R value is an L value is a

138
00:04:34,959 --> 00:04:36,230
R值是L值是很多时候
lot of the times something that is on

139
00:04:36,430 --> 00:04:38,360
等号的左侧，然后是R值是在
the left side of the equal sign and then

140
00:04:38,560 --> 00:04:40,400
等号的左侧，然后是R值是在
an R value is something that is on the

141
00:04:40,600 --> 00:04:42,079
等号的右边现在这并不总是适用，所以我不会
right side of the equal sign now this

142
00:04:42,279 --> 00:04:43,730
等号的右边现在这并不总是适用，所以我不会
does not always apply so I'm not gonna

143
00:04:43,930 --> 00:04:45,650
就像说这只是一个完整的事实，总要这样想，因为
like say this is just a complete fact to

144
00:04:45,850 --> 00:04:47,210
就像说这只是一个完整的事实，总要这样想，因为
always think of it that way because it's

145
00:04:47,410 --> 00:04:49,160
不一定是真的，但是在这种情况下，我们有一个名为I的变量
not necessarily true but in this case it

146
00:04:49,360 --> 00:04:51,530
不一定是真的，但是在这种情况下，我们有一个名为I的变量
is we have a variable called I which is

147
00:04:51,730 --> 00:04:53,780
当然，在内存中有位置的实际变量，然后
of course an actual variable with a

148
00:04:53,980 --> 00:04:56,090
当然，在内存中有位置的实际变量，然后
location in memory and then we have

149
00:04:56,290 --> 00:04:59,000
只是一个值，只是数字文字，只有十，没有存储空间
simply a value just a numeric literal

150
00:04:59,199 --> 00:05:01,550
只是一个值，只是数字文字，只有十，没有存储空间
it's just ten it has no storage it has

151
00:05:01,750 --> 00:05:05,210
没有位置，直到将其分配给L值，这就是我的意思
no location until it is assigned to an L

152
00:05:05,410 --> 00:05:07,550
没有位置，直到将其分配给L值，这就是我的意思
value which is what I is this makes

153
00:05:07,750 --> 00:05:09,259
有道理，因为您不能为R值赋值，否则我不能说
sense because you can't assign something

154
00:05:09,459 --> 00:05:11,720
有道理，因为您不能为R值赋值，否则我不能说
to an R value so otherwise I can't say

155
00:05:11,920 --> 00:05:14,120
10等于我或类似的东西，因为10不是
10 equals I or something like that that

156
00:05:14,319 --> 00:05:16,490
10等于我或类似的东西，因为10不是
would be weird because 10 is not

157
00:05:16,689 --> 00:05:18,439
某个位置，我们无法在十个位置存储数据，但这是一个L
something that has a location we can't

158
00:05:18,639 --> 00:05:21,650
某个位置，我们无法在十个位置存储数据，但这是一个L
store data in ten however this is an L

159
00:05:21,850 --> 00:05:23,180
现在很明显，我们可以制作另一个名为a的变量并将其设置
value now obviously we could just make

160
00:05:23,379 --> 00:05:25,220
现在很明显，我们可以制作另一个名为a的变量并将其设置
another variable called a and set it

161
00:05:25,420 --> 00:05:27,319
在这种情况下等于I，我们将L值设置为等于
equal to I in this case we're setting an

162
00:05:27,519 --> 00:05:29,449
在这种情况下等于I，我们将L值设置为等于
L value equal to something it is also an

163
00:05:29,649 --> 00:05:31,280
L值，这就是为什么我要说整个像等号的右边
L value which is why I was saying that

164
00:05:31,480 --> 00:05:33,500
L值，这就是为什么我要说整个像等号的右边
whole like right side of the equal sign

165
00:05:33,699 --> 00:05:35,720
并不总是很有意义，我们的价值不只是像
doesn't always make sense and our value

166
00:05:35,920 --> 00:05:37,129
并不总是很有意义，我们的价值不只是像
doesn't just have to be a literal like

167
00:05:37,329 --> 00:05:38,840
这也可能是一个函数的结果，也许我们有一个叫做
this it can also be the result of a

168
00:05:39,040 --> 00:05:40,189
这也可能是一个函数的结果，也许我们有一个叫做
function maybe we have a function called

169
00:05:40,389 --> 00:05:42,439
获取返回10的值，然后在这种情况下现在在这里调用此函数
get value which returns 10 and then we

170
00:05:42,639 --> 00:05:44,629
获取返回10的值，然后在这种情况下现在在这里调用此函数
call this function here now in this case

171
00:05:44,829 --> 00:05:47,329
get value返回一个R值，它返回一个临时值，这是临时的，因为
get value returns an R value it returns

172
00:05:47,529 --> 00:05:50,150
get value返回一个R值，它返回一个临时值，这是临时的，因为
a temporary value it's temporary because

173
00:05:50,350 --> 00:05:52,160
即使它确实返回一个int，它也没有位置，也没有存储空间
even though it does return an int it has

174
00:05:52,360 --> 00:05:53,900
即使它确实返回一个int，它也没有位置，也没有存储空间
no location it has no storage

175
00:05:54,100 --> 00:05:56,300
它只是返回值10，但我们在这里所做的是获取R
it's just returning the value 10 but

176
00:05:56,500 --> 00:05:57,860
它只是返回值10，但我们在这里所做的是获取R
what we're doing here is taking that R

177
00:05:58,060 --> 00:06:00,379
将该临时值赋值并将其存储为L值，因为这是
value that temporary value and storing

178
00:06:00,579 --> 00:06:02,720
将该临时值赋值并将其存储为L值，因为这是
it into an L value now because this is

179
00:06:02,920 --> 00:06:04,910
一个R值，如果我们尝试为该R值分配一些东西，那不会
an R value if we try and assign

180
00:06:05,110 --> 00:06:07,550
一个R值，如果我们尝试为该R值分配一些东西，那不会
something to that R value it's not gonna

181
00:06:07,750 --> 00:06:09,379
工作这个表达式是行不通的为什么，因为这是一个R值，你
work this expression is not gonna work

182
00:06:09,579 --> 00:06:12,290
工作这个表达式是行不通的为什么，因为这是一个R值，你
why because this is an R value and you

183
00:06:12,490 --> 00:06:13,910
可以看到我们的编译器正在帮助我们，说表达式必须是一个
can see that our compiler is helping us

184
00:06:14,110 --> 00:06:16,250
可以看到我们的编译器正在帮助我们，说表达式必须是一个
as saying that expression must be a

185
00:06:16,449 --> 00:06:19,189
可修改的L值可修改的含义是必须非常量L值的含义
modifiable L value modifiable meaning it

186
00:06:19,389 --> 00:06:21,560
可修改的L值可修改的含义是必须非常量L值的含义
has to be non Const an L value meaning

187
00:06:21,759 --> 00:06:23,960
它必须是L值，但这是有趣的地方
it has to be an L value however this is

188
00:06:24,160 --> 00:06:25,100
它必须是L值，但这是有趣的地方
where it gets interesting

189
00:06:25,300 --> 00:06:27,920
如果这是要返回一个L值，我们可以通过返回
if this was to return an L value which

190
00:06:28,120 --> 00:06:29,340
如果这是要返回一个L值，我们可以通过返回
we could do by returning

191
00:06:29,540 --> 00:06:31,650
int引用，这称为l值引用，我显然需要
int reference this is called an l-value

192
00:06:31,850 --> 00:06:33,270
int引用，这称为l值引用，我显然需要
reference and I would need to obviously

193
00:06:33,470 --> 00:06:35,189
通过使用像
provide some kind of storage for my

194
00:06:35,389 --> 00:06:37,199
通过使用像
value maybe by using a static int like

195
00:06:37,399 --> 00:06:39,540
如果是这种情况，然后返回它，因为现在是L值是
this and then returning it if this is

196
00:06:39,740 --> 00:06:42,180
如果是这种情况，然后返回它，因为现在是L值是
the case since this is now an L value is

197
00:06:42,379 --> 00:06:44,069
假装我可以分配给它的L值引用，此表达式有效
pretending an L value reference I can

198
00:06:44,269 --> 00:06:46,560
假装我可以分配给它的L值引用，此表达式有效
assign to it and this expression works

199
00:06:46,759 --> 00:06:48,990
很好，这就是L值参考在此基础上扩展的一点，如果我
fine so that's what an L value reference

200
00:06:49,189 --> 00:06:51,240
很好，这就是L值参考在此基础上扩展的一点，如果我
is to expand on this a little bit if I

201
00:06:51,439 --> 00:06:53,400
有一个带有值的函数，也许我们将其称为设置值，我可以调用
had a function that took in a value

202
00:06:53,600 --> 00:06:55,740
有一个带有值的函数，也许我们将其称为设置值，我可以调用
maybe we'll call it set value I can call

203
00:06:55,939 --> 00:06:57,300
我可以通过多种方式使用L值或R值来调用此函数
this function a number of ways I can

204
00:06:57,500 --> 00:06:59,460
我可以通过多种方式使用L值或R值来调用此函数
call it with an L value or an R value

205
00:06:59,660 --> 00:07:01,590
我将设置回十，然后使用该变量调用设置值，因此
I'll set I back to ten and then I'll

206
00:07:01,790 --> 00:07:04,050
我将设置回十，然后使用该变量调用设置值，因此
call set value with that variable so

207
00:07:04,250 --> 00:07:05,819
在这里我用L值调用set值，然后在这里我调用set
here I'm calling set value with an L

208
00:07:06,019 --> 00:07:08,250
在这里我用L值调用set值，然后在这里我调用set
value and then here I'm calling set

209
00:07:08,449 --> 00:07:10,650
带有R值的值，所以这是一个临时值，我保留R值
value with an R value so this is a

210
00:07:10,850 --> 00:07:13,350
带有R值的值，所以这是一个临时值，我保留R值
temporary value and R value I keep

211
00:07:13,550 --> 00:07:14,699
重复我自己，以便你们真的明白了重点，在这种情况下，
repeating myself just so you guys really

212
00:07:14,899 --> 00:07:16,170
重复我自己，以便你们真的明白了重点，在这种情况下，
get the point here so in this case what

213
00:07:16,370 --> 00:07:17,790
当小时值将用于创建L值时，
will happen is this hour value will be

214
00:07:17,990 --> 00:07:19,230
当小时值将用于创建L值时，
used to create an L value when the

215
00:07:19,430 --> 00:07:20,939
函数实际上被调用，现在我们可以立即看到其中之一是
function is actually called now we can

216
00:07:21,139 --> 00:07:23,009
函数实际上被调用，现在我们可以立即看到其中之一是
instantly see which one of these is

217
00:07:23,209 --> 00:07:24,870
暂时的，这不是因为另一个规则是您不能接受
temporary and which is not because

218
00:07:25,069 --> 00:07:27,060
暂时的，这不是因为另一个规则是您不能接受
another rule is that you cannot take an

219
00:07:27,259 --> 00:07:30,660
来自R值的L值参考，因此您只能拥有一个
L value reference from an R value so you

220
00:07:30,860 --> 00:07:32,910
来自R值的L值参考，因此您只能拥有一个
can only have an L value reference of an

221
00:07:33,110 --> 00:07:34,770
L值，如果我在这里贴上“＆”号，我们可以很容易地检查一下，所以现在
L value and we can easily check this if

222
00:07:34,970 --> 00:07:36,629
L值，如果我在这里贴上“＆”号，我们可以很容易地检查一下，所以现在
I stick an ampersand over here so now

223
00:07:36,829 --> 00:07:39,210
我感兴趣的是一个L值参考，您可以看到
I'm taking an interest is an L value

224
00:07:39,410 --> 00:07:40,740
我感兴趣的是一个L值参考，您可以看到
reference you can see that this

225
00:07:40,939 --> 00:07:42,389
立即给我一个错误，它指出引用的初始值
immediately gives me an error it says

226
00:07:42,589 --> 00:07:44,069
立即给我一个错误，它指出引用的初始值
that the initial value of reference to

227
00:07:44,269 --> 00:07:46,410
non Const是L值，现在您可能已经注意到，到目前为止，
non Const be an L value now you've

228
00:07:46,610 --> 00:07:47,850
non Const是L值，现在您可能已经注意到，到目前为止，
probably noticed so far there's a lot of

229
00:07:48,050 --> 00:07:49,980
提到像Const non constant这样的事情怎么办
mention of like Const non constant

230
00:07:50,180 --> 00:07:51,720
提到像Const non constant这样的事情怎么办
what's the deal here well there's

231
00:07:51,920 --> 00:07:54,060
实际上是一个特殊的规则，那就是虽然我没有L值参考
actually a special rule and that is that

232
00:07:54,259 --> 00:07:56,579
实际上是一个特殊的规则，那就是虽然我没有L值参考
whilst I can't have an L value reference

233
00:07:56,779 --> 00:07:58,949
的R值，换句话说，如果它是Const，就不能有这样的值
of an R value so in other words I can't

234
00:07:59,149 --> 00:08:02,250
的R值，换句话说，如果它是Const，就不能有这样的值
have something like this if it's a Const

235
00:08:02,449 --> 00:08:05,069
L值参考我可以这样，如果你是对的，在这里你可以看到这个作品
L value reference I can so if you're

236
00:08:05,269 --> 00:08:07,500
L值参考我可以这样，如果你是对的，在这里你可以看到这个作品
right Const here you can see this works

237
00:08:07,699 --> 00:08:09,870
这只是一个特殊的规则，有点解决方法
this is just a special rule it's kind of

238
00:08:10,069 --> 00:08:11,210
这只是一个特殊的规则，有点解决方法
a little bit of a workaround

239
00:08:11,410 --> 00:08:13,470
实际上，发生的情况是编译器可能会像
realistically what happens is that the

240
00:08:13,670 --> 00:08:15,090
实际上，发生的情况是编译器可能会像
compiler will probably create like a

241
00:08:15,290 --> 00:08:17,129
具有实际存储空间的临时变量，然后将其分配给该变量
temporary variable with your actual

242
00:08:17,329 --> 00:08:19,620
具有实际存储空间的临时变量，然后将其分配给该变量
storage and then assign it to that

243
00:08:19,819 --> 00:08:22,170
像这样的参考，实际上只是避免避免创建一个
reference like that so really it's just

244
00:08:22,370 --> 00:08:24,480
像这样的参考，实际上只是避免避免创建一个
they're not to kind of avoid creating an

245
00:08:24,680 --> 00:08:26,430
L值，而是仅仅支持我们的价值和L值
L value but rather to just kind of

246
00:08:26,629 --> 00:08:29,100
L值，而是仅仅支持我们的价值和L值
support both our values and L values

247
00:08:29,300 --> 00:08:32,339
因为我在这里可以做的就是将此作为我们的const参考，您可以看到
because what I can do here is make this

248
00:08:32,539 --> 00:08:34,199
因为我在这里可以做的就是将此作为我们的const参考，您可以看到
an our const reference and you can see

249
00:08:34,399 --> 00:08:36,929
它接受两种类型的数据，也接受l值和R值
that it accepts both types of data it

250
00:08:37,129 --> 00:08:38,879
它接受两种类型的数据，也接受l值和R值
accepts an l value and an R value

251
00:08:39,080 --> 00:08:42,509
因为Const l值引用实际上可以
because a Const l value reference can in

252
00:08:42,710 --> 00:08:43,208
因为Const l值引用实际上可以
fact

253
00:08:43,408 --> 00:08:45,128
除非他们两个都好，让我们从头开始看这个例子，再来看另一个
cept both of them okay let's scratch

254
00:08:45,328 --> 00:08:46,479
除非他们两个都好，让我们从头开始看这个例子，再来看另一个
this example and take a look at another

255
00:08:46,679 --> 00:08:48,818
这次使用字符串，我将有一个名字，我将其设置为我的名字
one this time using strings I'll have a

256
00:08:49,019 --> 00:08:50,740
这次使用字符串，我将有一个名字，我将其设置为我的名字
first name which I'll set to my first

257
00:08:50,940 --> 00:08:52,719
名称，然后我将拥有一个姓氏，然后将其设置为我的姓氏
name and then I'll have a last name

258
00:08:52,919 --> 00:08:55,269
名称，然后我将拥有一个姓氏，然后将其设置为我的姓氏
which I will set to my last name then

259
00:08:55,470 --> 00:08:56,498
我要做的是创建第三个字符串，这将是我的全名
what I'll do is I'll create a third

260
00:08:56,698 --> 00:08:58,118
我要做的是创建第三个字符串，这将是我的全名
string which is gonna be my full name

261
00:08:58,318 --> 00:09:00,068
然后将其设置为等于名字和姓氏，而忽略空格
and I'll set this equal to first name

262
00:09:00,269 --> 00:09:02,349
然后将其设置为等于名字和姓氏，而忽略空格
plus last name ignoring the space that

263
00:09:02,549 --> 00:09:03,549
我没有在两个字符串之间添加，所以请仔细看一下并尝试
I'm not adding between the two strings

264
00:09:03,750 --> 00:09:05,469
我没有在两个字符串之间添加，所以请仔细看一下并尝试
so take a good look at this and try and

265
00:09:05,669 --> 00:09:07,508
在此确定您认为左值是什么以及我们的值是什么
identify what you think an l-value is

266
00:09:07,708 --> 00:09:10,779
在此确定您认为左值是什么以及我们的值是什么
and what the our values are so in this

267
00:09:10,980 --> 00:09:13,118
情况左边的所有东西都是L值，右边的所有东西
case everything on the left side here is

268
00:09:13,318 --> 00:09:15,219
情况左边的所有东西都是L值，右边的所有东西
an L value and everything on the right

269
00:09:15,419 --> 00:09:17,198
这边不是我们的值，换句话说，这是R值，这是L
side here isn't our value so in other

270
00:09:17,399 --> 00:09:19,479
这边不是我们的值，换句话说，这是R值，这是L
words this is an R value this is an L

271
00:09:19,679 --> 00:09:22,029
值这个表达式在这里是R值，它是一个临时对象，发生的事情是
value this expression here is an R value

272
00:09:22,230 --> 00:09:24,459
值这个表达式在这里是R值，它是一个临时对象，发生的事情是
it's a temporary object what happens is

273
00:09:24,659 --> 00:09:27,159
从这两个构造一个临时字符串，然后将其分配到
a temporary string gets constructed from

274
00:09:27,360 --> 00:09:29,019
从这两个构造一个临时字符串，然后将其分配到
these two and then it gets assigned into

275
00:09:29,220 --> 00:09:32,589
一个L值，因此如果我有一个称为
an L value so with that same kind of

276
00:09:32,789 --> 00:09:34,659
一个L值，因此如果我有一个称为
rule in mind if I had a function called

277
00:09:34,860 --> 00:09:36,938
打印名称，在此处输入了S City字符串名称，然后再说
print name which took in an S City

278
00:09:37,139 --> 00:09:39,729
打印名称，在此处输入了S City字符串名称，然后再说
string name here and then let's just say

279
00:09:39,929 --> 00:09:43,899
我们在这里打印它，如果我尝试使用全名调用此函数打印名称
we print it here then if I try and call

280
00:09:44,100 --> 00:09:46,779
我们在这里打印它，如果我尝试使用全名调用此函数打印名称
this function print name with full name

281
00:09:46,980 --> 00:09:49,179
它会很好地工作，但是如果我尝试用我们拥有的这个表达式来调用它
it's gonna work fine but if I try and

282
00:09:49,379 --> 00:09:51,308
它会很好地工作，但是如果我尝试用我们拥有的这个表达式来调用它
call it with this expression that we had

283
00:09:51,509 --> 00:09:53,589
这是行不通的，因为它是R值，这就是为什么您会看到
here that's not going to work because

284
00:09:53,789 --> 00:09:56,258
这是行不通的，因为它是R值，这就是为什么您会看到
it's an R value which is why you'll see

285
00:09:56,458 --> 00:09:58,568
很多会议都是用C ++编写的，因为您可以看到它们
a lot of conferences being written in

286
00:09:58,769 --> 00:10:00,729
很多会议都是用C ++编写的，因为您可以看到它们
C++ because as you can see they're

287
00:10:00,929 --> 00:10:03,399
与临时R值和实际实数都兼容
compatible with both temporaries R

288
00:10:03,600 --> 00:10:06,818
与临时R值和实际实数都兼容
values and also actual kind of real

289
00:10:07,019 --> 00:10:09,128
现有变量L值，因此我们显然可以检测出是否或
existing variables L values so we

290
00:10:09,328 --> 00:10:11,498
现有变量L值，因此我们显然可以检测出是否或
clearly have a way to detect whether or

291
00:10:11,698 --> 00:10:14,139
实际的L值不是什么，我们可以通过写一个L值来做到这一点
not something is an actual L value we

292
00:10:14,339 --> 00:10:16,299
实际的L值不是什么，我们可以通过写一个L值来做到这一点
can do that by just writing an L value

293
00:10:16,500 --> 00:10:18,519
非常量的引用L值引用只能接受L值
reference that's non Const an L value

294
00:10:18,720 --> 00:10:20,679
非常量的引用L值引用只能接受L值
reference can only accept L values this

295
00:10:20,879 --> 00:10:22,238
不会编译，因为这当然是R值，我们有办法写吗
will not compile because this of course

296
00:10:22,438 --> 00:10:24,308
不会编译，因为这当然是R值，我们有办法写吗
is an R value do we have a way to write

297
00:10:24,509 --> 00:10:26,318
是一个仅接受临时对象的函数，是的，我们需要使用它
a function that only accepts temporary

298
00:10:26,519 --> 00:10:28,868
是一个仅接受临时对象的函数，是的，我们需要使用它
objects yes and for that we need to use

299
00:10:29,068 --> 00:10:30,698
称为r值参考的东西与我们的值参考看起来相同
something called an r-value reference

300
00:10:30,899 --> 00:10:32,409
称为r值参考的东西与我们的值参考看起来相同
and our value reference looks the same

301
00:10:32,610 --> 00:10:34,118
作为L值参考，除了我们使用两个与号代替一个，因此您可以
as an L value reference except we use

302
00:10:34,318 --> 00:10:36,639
作为L值参考，除了我们使用两个与号代替一个，因此您可以
two ampersands instead of one so you can

303
00:10:36,839 --> 00:10:38,019
看看这里发生了什么是切换错误，现在我们不能传递L
see what's happened here is the errors

304
00:10:38,220 --> 00:10:40,599
看看这里发生了什么是切换错误，现在我们不能传递L
of switch now we can't pass in an L

305
00:10:40,799 --> 00:10:43,238
值，但我们可以传入R值，如果我们将鼠标悬停在此处并查看
value but we can pass in an R value and

306
00:10:43,438 --> 00:10:44,649
值，但我们可以传入R值，如果我们将鼠标悬停在此处并查看
if we hover our mouse over here and look

307
00:10:44,850 --> 00:10:46,029
在错误消息中说R值引用不能绑定到L值
at the error message it says an R value

308
00:10:46,230 --> 00:10:48,549
在错误消息中说R值引用不能绑定到L值
reference cannot be bound to an L value

309
00:10:48,750 --> 00:10:50,919
这很有意义，因为这意味着我们可以
so that makes sense this is pretty cool

310
00:10:51,120 --> 00:10:52,299
这很有意义，因为这意味着我们可以
because it means that we can actually

311
00:10:52,500 --> 00:10:54,099
编写此函数的重载，仅接受临时函数
write an overload of this function that

312
00:10:54,299 --> 00:10:56,139
编写此函数的重载，仅接受临时函数
accepts only temporary

313
00:10:56,339 --> 00:10:58,839
Jax和一个接受L值的对象，即使我们使这个Const表示
Jax and one that accepts L values and

314
00:10:59,039 --> 00:11:01,029
Jax和一个接受L值的对象，即使我们使这个Const表示
even if we make this Const meaning that

315
00:11:01,230 --> 00:11:02,378
它也将与我们的价值观兼容，这种超载仍然会
it's also going to be compatible with

316
00:11:02,578 --> 00:11:04,419
它也将与我们的价值观兼容，这种超载仍然会
our values this overload will still be

317
00:11:04,620 --> 00:11:06,639
选择是否存在，所以如果我们在这里看一下代码，我可能会写L
chosen if it exists so if we take a look

318
00:11:06,839 --> 00:11:08,798
选择是否存在，所以如果我们在这里看一下代码，我可能会写L
at our code here I might just write L

319
00:11:08,999 --> 00:11:10,748
值，然后在这里我将做同样的事情，除了如果
value and then I'll do the same thing

320
00:11:10,948 --> 00:11:13,238
值，然后在这里我将做同样的事情，除了如果
over here except I'll write our value if

321
00:11:13,438 --> 00:11:15,188
我们现在将所有这些都调用，而我只是做一点STD，似乎我猜
we now call all of these and I'll just

322
00:11:15,389 --> 00:11:17,078
我们现在将所有这些都调用，而我只是做一点STD，似乎我猜
do a little STD seems I guess that the

323
00:11:17,278 --> 00:11:18,698
控制台没有关闭，您会看到选择的第一个重载是
console doesn't close you can see that

324
00:11:18,899 --> 00:11:20,198
控制台没有关闭，您会看到选择的第一个重载是
the first overload that was chosen was

325
00:11:20,399 --> 00:11:21,878
L值1，然后第二个是R值1，所以这才是真正的
the L value 1 and then the second one

326
00:11:22,078 --> 00:11:24,339
L值1，然后第二个是R值1，所以这才是真正的
was the R value 1 so this is all really

327
00:11:24,539 --> 00:11:26,498
很酷，因为有了我们的价值参考，我们现在有了一种方法来实际检测
cool because with our value references

328
00:11:26,698 --> 00:11:28,599
很酷，因为有了我们的价值参考，我们现在有了一种方法来实际检测
we now have a way to actually detect

329
00:11:28,799 --> 00:11:31,238
临时值并对它们做一些特殊的事情，所以现在的问题
temporary values and do something

330
00:11:31,438 --> 00:11:33,039
临时值并对它们做一些特殊的事情，所以现在的问题
special to them so the question now

331
00:11:33,240 --> 00:11:35,979
可能还可以，但是这有什么用，答案很好，它非常有用
might be well ok but how is this useful

332
00:11:36,179 --> 00:11:38,649
可能还可以，但是这有什么用，答案很好，它非常有用
and the answer is well it's very useful

333
00:11:38,850 --> 00:11:41,078
特别是在移动语义方面，但这将是另一个主题
especially with move semantics but

334
00:11:41,278 --> 00:11:42,368
特别是在移动语义方面，但这将是另一个主题
that's going to be a topic for another

335
00:11:42,568 --> 00:11:44,498
视频基本上，这里的主要优势是优化，如果我们知道
video basically the main advantage here

336
00:11:44,698 --> 00:11:46,628
视频基本上，这里的主要优势是优化，如果我们知道
is to do with optimization if we know

337
00:11:46,828 --> 00:11:49,058
我们要放入一个临时对象，那么我们就不必担心制作
that we're taking in a temporary object

338
00:11:49,259 --> 00:11:51,339
我们要放入一个临时对象，那么我们就不必担心制作
then we don't have to worry about making

339
00:11:51,539 --> 00:11:53,198
确保我们保持它的生命力，并确保我们保持完整无缺，确保我们
sure that we keep it alive and making

340
00:11:53,399 --> 00:11:54,878
确保我们保持它的生命力，并确保我们保持完整无缺，确保我们
sure we keep it intact making sure we

341
00:11:55,078 --> 00:11:57,039
复制它，我们可以简单地窃取可能附加到该资源的资源
copy it we can simply steal the

342
00:11:57,240 --> 00:11:59,378
复制它，我们可以简单地窃取可能附加到该资源的资源
resources that might be attached to that

343
00:11:59,578 --> 00:12:02,019
特定对象并在其他地方使用它们，因为我们知道它是临时的
specific object and use them somewhere

344
00:12:02,220 --> 00:12:04,298
特定对象并在其他地方使用它们，因为我们知道它是临时的
else because we know that it's temporary

345
00:12:04,499 --> 00:12:06,308
它不会存在很长时间，而如果您采用类似
it's not going to exist for very long

346
00:12:06,509 --> 00:12:07,808
它不会存在很长时间，而如果您采用类似
whereas if you take in something like

347
00:12:08,009 --> 00:12:09,339
这就是您的意思，除了费用，您不能真正做到这一点
this then you can't I mean apart from

348
00:12:09,539 --> 00:12:10,959
这就是您的意思，除了费用，您不能真正做到这一点
the fact that it's cost you can't really

349
00:12:11,159 --> 00:12:13,328
窃取此名称字符串中的任何内容，因为它可能在许多
steal anything from this name string

350
00:12:13,528 --> 00:12:15,728
窃取此名称字符串中的任何内容，因为它可能在许多
because it might be used in a number of

351
00:12:15,928 --> 00:12:17,349
功能，而这显然只是暂时的
functions whereas this is clearly

352
00:12:17,549 --> 00:12:20,048
功能，而这显然只是暂时的
something that is temporary only going

353
00:12:20,249 --> 00:12:22,118
与特定的打印名称调用配合使用，这就是功能的所在
to be used with this particular call of

354
00:12:22,318 --> 00:12:24,399
与特定的打印名称调用配合使用，这就是功能的所在
print name and that's where the power

355
00:12:24,600 --> 00:12:26,948
确实来自，所以请记住L值基本上是具有一些
really comes from so remember L values

356
00:12:27,149 --> 00:12:29,438
确实来自，所以请记住L值基本上是具有一些
are basically variables that have some

357
00:12:29,639 --> 00:12:31,599
一种存储形式，它们的值是临时值l值引用
kind of storage back in them our values

358
00:12:31,799 --> 00:12:34,959
一种存储形式，它们的值是临时值l值引用
are temporary values l value references

359
00:12:35,159 --> 00:12:37,058
只能接受l个值，除非它们是Const并且我们的值引用仅取
can only take in l values unless they're

360
00:12:37,259 --> 00:12:40,089
只能接受l个值，除非它们是Const并且我们的值引用仅取
Const and our value references only take

361
00:12:40,289 --> 00:12:42,308
在这些临时的R值中，您知道我有这个视频Syrus在玩
in these temporary R values you know I

362
00:12:42,509 --> 00:12:44,228
在这些临时的R值中，您知道我有这个视频Syrus在玩
have this video Syrus playing with the

363
00:12:44,428 --> 00:12:45,969
这个视频的辣椒粉对你们来说很有意义，因为我说我绝对是一个
paprika for this video makes sense

364
00:12:46,169 --> 00:12:47,618
这个视频的辣椒粉对你们来说很有意义，因为我说我绝对是一个
for you guys as I say I'm definitely one

365
00:12:47,818 --> 00:12:48,998
很多人对此感到非常困惑的主题，希望如此
of those topics that a lot of people are

366
00:12:49,198 --> 00:12:50,558
很多人对此感到非常困惑的主题，希望如此
very confused about so hopefully this

367
00:12:50,759 --> 00:12:52,719
视频帮助我们了解了我的价值观和我们
video helped to kind of clear the air a

368
00:12:52,919 --> 00:12:55,149
视频帮助我们了解了我的价值观和我们
little bit as to what l values and our

369
00:12:55,350 --> 00:12:57,039
价值观是我在本系列文章中取得的进步，尤其是这种进步
values are as I kind of progress with

370
00:12:57,240 --> 00:12:58,448
价值观是我在本系列文章中取得的进步，尤其是这种进步
this series and specifically this kind

371
00:12:58,649 --> 00:13:01,628
总的来说，关于语义的情况将会变得更加清晰
of sum of semantics situation it will

372
00:13:01,828 --> 00:13:03,698
总的来说，关于语义的情况将会变得更加清晰
definitely become a lot more clear as to

373
00:13:03,899 --> 00:13:06,368
为什么知道您是否要处理R值参考以及是否
why it's important to know if you're

374
00:13:06,568 --> 00:13:08,438
为什么知道您是否要处理R值参考以及是否
dealing with an R value reference and if

375
00:13:08,639 --> 00:13:10,389
实际上，您可以从该临时值中窃取资源，因为它是
you can in fact steal the resources

376
00:13:10,590 --> 00:13:12,729
实际上，您可以从该临时值中窃取资源，因为它是
from that temporary value because it is

377
00:13:12,929 --> 00:13:15,370
临时的，确实对优化有很大帮助，它也非常有用
temporary it really does help a lot with

378
00:13:15,570 --> 00:13:17,589
临时的，确实对优化有很大帮助，它也非常有用
optimization and it's also very useful

379
00:13:17,789 --> 00:13:19,569
如果您想知道销售情况，这是非常重要的信息
it's very important information to know

380
00:13:19,769 --> 00:13:21,099
如果您想知道销售情况，这是非常重要的信息
in general if you want to know how sales

381
00:13:21,299 --> 00:13:22,299
类实际上是有效的，并且当您对这样的代码有点杀手时
class actually works and when you're

382
00:13:22,500 --> 00:13:23,500
类实际上是有效的，并且当您对这样的代码有点杀手时
kind of killing with the code like that

383
00:13:23,700 --> 00:13:25,659
因为很多时候您可能正在通过一些代码来查找
because again a lot of the time you

384
00:13:25,860 --> 00:13:26,889
因为很多时候您可能正在通过一些代码来查找
might be looking through some code you

385
00:13:27,090 --> 00:13:28,659
可能会看到那个双＆号和那个R值参考，您可能像我
might see that double ampersand that R

386
00:13:28,860 --> 00:13:30,129
可能会看到那个双＆号和那个R值参考，您可能像我
value reference and you might be like I

387
00:13:30,330 --> 00:13:31,839
不知道这意味着什么或为什么我在乎，但希望你现在也不要
have no idea what this means or why I

388
00:13:32,039 --> 00:13:35,259
不知道这意味着什么或为什么我在乎，但希望你现在也不要
care but hopefully with this you now nor

389
00:13:35,460 --> 00:13:36,789
你们从来没有喜欢过这个视频，请点赞之类的按钮
never have you guys enjoyed this video

390
00:13:36,990 --> 00:13:38,139
你们从来没有喜欢过这个视频，请点赞之类的按钮
if you did please hit that like button

391
00:13:38,340 --> 00:13:39,519
也不要忘了先看看Skillshare，
also don't forget to check out

392
00:13:39,720 --> 00:13:41,649
也不要忘了先看看Skillshare，
Skillshare first thousand people who use

393
00:13:41,850 --> 00:13:43,719
我在描述模糊中的链接可免费获得两个月的技能塑造
my link in description blur get free

394
00:13:43,919 --> 00:13:45,759
我在描述模糊中的链接可免费获得两个月的技能塑造
access to two months of skill shaped

395
00:13:45,960 --> 00:13:47,500
高级，去学习新的东西，下次见。
premium go and learn something new I

396
00:13:47,700 --> 00:13:49,859
高级，去学习新的东西，下次见。
will see you next time goodbye

397
00:13:50,059 --> 00:13:55,059
[音乐]
[Music]

