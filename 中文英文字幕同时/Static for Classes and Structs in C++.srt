﻿1
00:00:00,000 --> 00:00:03,248
hey what about guys my name is the Chano and welcome back to my state plus plus
嘿，伙计们，我叫Chano，欢迎回到我的州，再加上

2
00:00:03,448 --> 00:00:07,930
series alright this is weird I mean like some check hotel just don't ask
系列好吧，这很奇怪，我的意思是像一些检查酒店只是不要问

3
00:00:08,130 --> 00:00:11,470
questions so today we're going to be talking about what static means inside a
问题，所以今天我们要讨论的是静态

4
00:00:11,669 --> 00:00:16,089
class or a struct last time we talked about the static keyword in C++ and what
上一次我们讨论C ++中的static关键字以及什么是类或结构

5
00:00:16,289 --> 00:00:20,199
its meaning was if it was outside of a class or a struct and there's a link to
它的意思是，如果它在类或结构之外，并且有链接到

6
00:00:20,399 --> 00:00:23,260
that video in the description below as well as a card on screen so go ahead and
以下说明中的视频以及屏幕上的卡片，请继续操作， 

7
00:00:23,460 --> 00:00:28,419
watch that but today what a static mean if it is inside a class or extract in
注意，但是今天，如果它在一个类中或在其中提取，它是什么意思

8
00:00:28,618 --> 00:00:32,829
pretty much all object-oriented languages static inside a class means a
一个类中几乎所有静态的面向对象语言都意味着

9
00:00:33,030 --> 00:00:36,128
certain sense if you use it with a variable it means that there is only
从某种意义上说，如果将它与变量一起使用，则意味着只有

10
00:00:36,329 --> 00:00:40,538
going to be one instance of that variable across all instances of the
将成为该变量在所有实例中的一个实例

11
00:00:40,738 --> 00:00:45,489
class so if I make a class called entity and I keep making entity instances I'm
类，所以如果我创建一个称为实体的类，并且继续创建实体实例， 

12
00:00:45,689 --> 00:00:48,788
still only going to have one version of that variable meaning if one of the
仍然只有该变量的一个版本，即如果其中一个

13
00:00:48,988 --> 00:00:53,288
instances changes one of the variable meaning if one of those instances
实例更改变量含义之一，如果这些实例之一

14
00:00:53,488 --> 00:00:57,698
changes my static variable it's going to reflect that change across all instances
更改我的静态变量，它将反映所有实例中的更改

15
00:00:57,899 --> 00:01:00,998
because really there's only one variable even though I've made a whole bunch of
因为即使我做了很多的事情，实际上只有一个变量

16
00:01:01,198 --> 00:01:04,599
class instances and so because of that there's no point of referring to the
类实例，因此没有必要引用

17
00:01:04,799 --> 00:01:09,099
variable through a class instance because there's like there's like a
通过类实例进行变量，因为就像

18
00:01:09,299 --> 00:01:13,299
global instance for that class with static methods you get a similar effect
使用静态方法的该类的全局实例，您将获得类似的效果

19
00:01:13,500 --> 00:01:18,579
where you just you don't have access to the class instance a static method can
在您无法访问类实例的地方，静态方法可以

20
00:01:18,780 --> 00:01:22,719
be called without a class instance and inside a static method you cannot write
在没有类实例的情况下被调用，并且无法在静态方法内编写

21
00:01:22,920 --> 00:01:27,488
code which refers to a class instance since you don't have that class instance
引用类实例的代码，因为您没有该类实例

22
00:01:27,688 --> 00:01:30,730
to refer to let's jump in and take a look at some examples so over here I'm
让我们进入并看一些例子，所以在这里我

23
00:01:30,930 --> 00:01:34,480
going to write a struct called entity and I'm going to give it two integers X
要写一个叫做entity的结构，我要给它两个整数X 

24
00:01:34,680 --> 00:01:39,308
and y note that I'm using a struct here but you could use a class it doesn't
 y请注意，我在这里使用结构，但是您可以使用一个没有

25
00:01:39,509 --> 00:01:42,128
matter the reason I've chosen a struct here is because I want those x and y
不管我在这里选择一个结构的原因是因为我想要那些x和y 

26
00:01:42,328 --> 00:01:46,539
variables to be public and by using a struct they're going to be public by
变量是公共的，通过使用结构，它们将被公共

27
00:01:46,739 --> 00:01:48,939
default I've made a video about the differences between with classes and
默认情况下，我已经制作了一个有关with和

28
00:01:49,140 --> 00:01:52,599
structs as well if you haven't seen that there's a link everywhere check it out
如果您还没有看到到处都有链接，也可以使用结构

29
00:01:52,799 --> 00:01:55,569
so right now we have a pretty straightforward empty class we can
所以现在我们有一个非常简单的空类，我们可以

30
00:01:55,769 --> 00:02:01,000
instantiate it like so set the values to what we want them to be such as 2 & 3
实例化它，因此将值设置为我们想要的值，例如2＆3 

31
00:02:01,200 --> 00:02:04,869
and we're ready to roll if I want to make another instance of that class I
如果我想创建该类的另一个实例，我们就准备好了

32
00:02:05,069 --> 00:02:08,380
can also do that this time I'll initialize it using an initializer so
这次也可以做到这一点，我将使用初始化程序进行初始化，因此

33
00:02:08,580 --> 00:02:12,368
we'll set it to like 5 & 8 for example and also let's give our entity struct
我们将其设置为例如5＆8，还让我们提供实体结构

34
00:02:12,568 --> 00:02:14,679
some kind of function so that we can print what we
某种功能，以便我们可以打印我们

35
00:02:14,878 --> 00:02:20,618
have to the console just using standard C out and there we go so if I come down
只需使用标准C就可以进入控制台，然后就可以了，如果我摔倒了

36
00:02:20,818 --> 00:02:26,770
here and I print E and E one the behavior here is going to be fairly
在这里，我打印E和E，这里的行为将相当

37
00:02:26,969 --> 00:02:30,520
normal and what you would expect so we should get two three and then five a for
正常，您会期望什么，所以我们应该得到两个三个然后五个

38
00:02:30,719 --> 00:02:33,880
our second one and you can see that's what we get however things will change
我们的第二个，您会看到这就是我们得到的，但是情况会发生变化

39
00:02:34,080 --> 00:02:38,490
if I make the variable static so if I come over here and I make these x and y
如果我将变量设为静态，那么如果我来到这里，然后将这些x和y 

40
00:02:38,689 --> 00:02:43,750
static first of all my initializer here will fail because x and y and no longer
首先，我的静态初始值设定项将因为x和y而不再是静态的

41
00:02:43,949 --> 00:02:47,469
class members so we can forget about that and rewrite our code to be this
类成员，这样我们就可以忘记它并重写我们的代码

42
00:02:47,669 --> 00:02:51,969
which again is not strictly written correctly however for an example it will
再次没有严格正确地编写，但是作为示例，它将

43
00:02:52,169 --> 00:02:55,480
work to make sure we're referring to e1 here so you can see we are referring to
确保我们在这里指的是e1，以便您可以看到我们在指的是

44
00:02:55,680 --> 00:03:00,399
two different instances or it appears to be so at least if we run this code we're
两个不同的实例，或者至少是这样，如果我们运行此代码， 

45
00:03:00,598 --> 00:03:05,140
going to get unresolved external symbols because we actually have to define those
将获得未解决的外部符号，因为我们实际上必须定义那些

46
00:03:05,340 --> 00:03:09,879
static variables somewhere and we can do so over here by just writing int entity
静态变量，我们可以在这里通过编写int实体来实现

47
00:03:10,079 --> 00:03:13,000
which is the scope of this static variable then the name of the variable
这是该静态变量的范围，然后是变量的名称

48
00:03:13,199 --> 00:03:17,140
and we don't really need to set them equal to anything if we don't want to we
如果我们不想我们真的不需要将它们设置为等于任何值

49
00:03:17,340 --> 00:03:20,740
can just write this so now they've been defined the Lincolnton linked to the
可以这样写，所以现在已经定义了林肯顿链接到

50
00:03:20,939 --> 00:03:24,550
appropriate variable and we can run our code however if we do so you'll see that
适当的变量，我们可以运行我们的代码，但是如果这样做，您会看到

51
00:03:24,750 --> 00:03:27,700
we actually get five eight five eight printing twice which is a little bit
我们实际上得到了五八五八八次的打印两次，这有点

52
00:03:27,900 --> 00:03:31,300
weird you can see here in our code that it clearly appears that we're setting x
很奇怪，您可以在我们的代码中看到，显然我们正在设置x 

53
00:03:31,500 --> 00:03:34,868
and y to two and three for the first instance but then five and eight to the
和y分别是2和3，然后是5和8 

54
00:03:35,068 --> 00:03:39,069
second one however remember that those x and y variables when we made them static
第二个，但是请记住，当我们将它们设为静态时，这些x和y变量

55
00:03:39,269 --> 00:03:43,599
we made it so that there was only one instance of those two variables across
我们做到了，所以只有这两个变量的一个实例

56
00:03:43,799 --> 00:03:47,650
all instances of our entity classes which means that when I'm changing the
我们实体类的所有实例，这意味着当我更改

57
00:03:47,849 --> 00:03:52,750
second entity's X&Y they're actually the exact same as the first one they're
第二个实体的X＆Y实际上与第一个实体完全相同

58
00:03:52,949 --> 00:03:56,890
pointing to the same memory picture two different entity instances however the x
指向相同的内存图片两个不同的实体实例，但是x 

59
00:03:57,090 --> 00:04:02,530
and y does they're pointing to a shared they're pointing to the same x and y and
 y是否指向共享对象，它们指向相同的x和y， 

60
00:04:02,729 --> 00:04:07,149
as such there's no point in us referring to them like this we can actually just
因此，我们这样指称他们毫无意义，我们实际上可以

61
00:04:07,348 --> 00:04:12,399
refer to them like this as if they were inside the entity scope because really
像这样引用它们，就好像它们在实体范围内一样，因为

62
00:04:12,598 --> 00:04:16,480
that all they are it's like we've made two variables that are inside a
就像我们已经在一个变量中创建了两个变量一样

63
00:04:16,680 --> 00:04:20,408
namespace called entity they don't really belong to the
命名空间，它们实际上并不属于

64
00:04:20,608 --> 00:04:24,040
class they do of course in the sense that they can be private and public and
他们当然会在私人和公共场合

65
00:04:24,240 --> 00:04:28,480
they have like so they are still kind of part of a class and not just a namespace
他们喜欢，所以它们仍然是类的一部分，而不仅仅是命名空间

66
00:04:28,680 --> 00:04:33,850
but for all intents and services they basically are just in a namespace and
但是对于所有的意图和服务，它们基本上都只是在命名空间中

67
00:04:34,050 --> 00:04:37,720
they have nothing to do with any kind of allocation when you create a new class
创建新类时，它们与任何类型的分配无关

68
00:04:37,920 --> 00:04:43,720
instance or anything like that so if we were to rewrite our code correctly you
实例或类似的东西，所以如果我们要正确地重写我们的代码， 

69
00:04:43,920 --> 00:04:47,230
can see this makes a lot more sense now why we're getting 5/8 everywhere because
可以看到现在变得更有意义了，为什么我们到处都是5/8，因为

70
00:04:47,430 --> 00:04:51,699
we are actually modifying the exact same variable this is of course really useful
我们实际上正在修改完全相同的变量，这当然非常有用

71
00:04:51,899 --> 00:04:55,810
when you want to have variables across classes you could of course just create
当您希望跨类拥有变量时，您当然可以创建

72
00:04:56,009 --> 00:04:59,740
a global variable or instead of a global variable it could be like a static
全局变量或代替全局变量，它可能像静态变量

73
00:04:59,939 --> 00:05:03,310
global variable right so it's still internally linked and all that stuff
正确的全局变量，所以它仍然是内部链接的

74
00:05:03,509 --> 00:05:07,389
it's not going to be global across your entire program however doing that would
它不会在整个程序中都具有全局性，但是这样做会

75
00:05:07,589 --> 00:05:10,870
have the same effect of this so why would you do this the answer is
具有相同的效果，所以为什么要这样做，答案是

76
00:05:11,069 --> 00:05:15,850
basically just it makes sense to put it inside entity if you have something if
基本上，如果有的话，将其放入实体是有意义的

77
00:05:16,050 --> 00:05:20,650
you have a piece of information a piece of data that you want to be shared
您有一条信息要共享

78
00:05:20,850 --> 00:05:25,180
across all entity instances or it makes sense to actually store it inside the
跨所有实体实例，或者将其实际存储在

79
00:05:25,379 --> 00:05:29,230
entity class because it's to do with entities for organizational purposes
实体类，因为它与用于组织目的的实体有关

80
00:05:29,430 --> 00:05:33,910
you're much better making a static variable inside this class rather than
您最好在此类内创建一个静态变量，而不是

81
00:05:34,110 --> 00:05:37,990
having some statical global floating around somewhere static methods works in
在某个静态方法的某个地方浮动一些静态全局变量

82
00:05:38,189 --> 00:05:42,220
a similar way if I make this print method ecstatic then it's going to work
以类似的方式，如果我使这种打印方法狂喜，那么它将起作用

83
00:05:42,420 --> 00:05:46,120
fine because you can see it's referring to x and y which are also static
很好，因为您可以看到它指的是x和y，它们也是静态的

84
00:05:46,319 --> 00:05:51,250
variables I no longer have to call it like this I can also call it like so in
变量，我不再需要这样称呼，我也可以这样称呼它

85
00:05:51,449 --> 00:05:53,889
fact this would be the correct way to call it and of course you can also
事实上，这是调用它的正确方法，当然您也可以

86
00:05:54,089 --> 00:05:56,590
notice that it's clearly going to be printing the same thing because we're
注意，因为我们正在

87
00:05:56,790 --> 00:06:01,329
running the same exact method twice and in this particular example we don't even
两次运行相同的确切方法，在这个特定示例中，我们甚至没有

88
00:06:01,529 --> 00:06:05,650
need class instances at all because everything we're doing is static however
根本需要类实例，因为我们所做的一切都是静态的

89
00:06:05,850 --> 00:06:09,819
things would break here if we decided to make x and y non-static would keep our
如果我们决定将x和y设为非静态，事情就会在这里破裂

90
00:06:10,019 --> 00:06:14,079
print method as static because static methods cannot access non static
打印方法为静态，因为静态方法无法访问非静态方法

91
00:06:14,279 --> 00:06:17,680
variables and there's a very very simple reason to that sometimes we get a little
变量，并且有一个非常非常简单的原因，有时我们会得到一些

92
00:06:17,879 --> 00:06:22,449
bit confused as to what static scene can access what a non-static thing it's
关于什么静态场景可以访问什么是非静态的东西有些困惑

93
00:06:22,649 --> 00:06:26,470
really not confusing at all check this out I'm going to bring back my entity
真的一点也不困惑，请检查一下，我将带回我的实体

94
00:06:26,670 --> 00:06:32,199
instances and rewrite my code so that we actually do have a separate X and Y for
实例并重写我的代码，以便我们实际上确实有一个单独的X和Y 

95
00:06:32,399 --> 00:06:36,939
each instance of the entity class I'm going to still leave this print method
实体类的每个实例我都将保留此打印方法

96
00:06:37,139 --> 00:06:39,829
as static I'm going to get rid of this definite
作为静态，我将摆脱这种确定性

97
00:06:40,029 --> 00:06:44,389
laughs static variables and get rid of static in front of X&Y if we were to try
如果我们尝试尝试笑静态变量，并摆脱X＆Y前面的静态

98
00:06:44,589 --> 00:06:47,870
and compile this turn now we're going to get an error and you can see our error
并编译此回合，现在我们将得到一个错误，您可以看到我们的错误

99
00:06:48,069 --> 00:06:52,759
rates illegal reference to non-static member entity X because you cannot
对非静态成员实体X的非法引用进行评级，因为您不能

100
00:06:52,959 --> 00:06:59,480
access this from a static method the reason is that a static method does not
从静态方法访问它，原因是静态方法没有

101
00:06:59,680 --> 00:07:03,230
have a class instance I might go a little bit more into how classes
有一个类实例，我可能会进一步讲解类

102
00:07:03,430 --> 00:07:07,819
actually work in a future video but essentially every method that you write
实际上可以在未来的视频中使用，但实际上是您编写的每种方法

103
00:07:08,019 --> 00:07:12,259
inside a class every non-static method always gets an instance of the current
在类中，每个非静态方法总是获取当前实例

104
00:07:12,459 --> 00:07:15,949
class as a parameter that's how classes actually work behind the scenes there's
类作为参数，它是类在幕后实际工作的方式

105
00:07:16,149 --> 00:07:20,360
no such thing as a class they're just functions with a hidden parameter of
没有像类这样的东西，它们只是具有隐藏参数

106
00:07:20,560 --> 00:07:24,470
sorts a static method does not get at that hidden parameter a static method is
对静态方法进行排序不会在该隐藏参数处获得静态方法

107
00:07:24,670 --> 00:07:28,220
the same as if you wrote a method outside of the class if I were to write
就像您在类之外编写方法一样

108
00:07:28,420 --> 00:07:31,699
a print method outside here suddenly this probably makes a lot more sense as
外面的打印方法突然变得更有意义，因为

109
00:07:31,899 --> 00:07:34,879
to why you can't access X and why because you don't know what they are now
为什么不能访问X以及为什么因为您不知道它们现在是什么

110
00:07:35,079 --> 00:07:38,720
picture that you have this exact same method however an entity actually was
图片您具有完全相同的方法，但是实体实际上是

111
00:07:38,920 --> 00:07:44,150
passed in as a parameter and your code was converted to this suddenly this
作为参数传入，您的代码突然转换为此

112
00:07:44,350 --> 00:07:48,170
works what we've just written here this method that we've just written here is
运作我们刚刚在这里编写的方法，我们刚刚在这里编写的方法是

113
00:07:48,370 --> 00:07:53,689
essentially what a class method that is non-static actually looks like when it's
本质上，非静态的类方法实际上是

114
00:07:53,889 --> 00:07:57,680
compiled and so if we take away this instance which is exactly what we're
编译，所以如果我们拿走这个实例，那正是我们

115
00:07:57,879 --> 00:08:03,620
doing when we add this static keyword to a class method we get errors that's why
当我们将此静态关键字添加到类方法中时，会得到错误，这就是为什么

116
00:08:03,819 --> 00:08:06,920
you're getting errors it doesn't know which entities x and y you want to
您遇到错误，它不知道您想要的实体x和y 

117
00:08:07,120 --> 00:08:10,790
access because you haven't given it a reference to an entity so hopefully that
访问，因为您没有给它提供对实体的引用，因此希望

118
00:08:10,990 --> 00:08:13,790
makes a lot of sense and that clears everything up next time we're going to
很有道理，下次我们将清除所有内容

119
00:08:13,990 --> 00:08:17,840
take a look at integrating this new sound static knowledge into our low
看看如何将这些新的声音静态知识整合到我们的低音中

120
00:08:18,040 --> 00:08:20,660
class that we've been working on so you guys want to see what that looks like
我们一直在上课，所以你们想看看看起来像什么

121
00:08:20,860 --> 00:08:24,710
you can look at the how to write a C++ class video as we go along with this
您可以在此过程中了解如何编写C ++类视频

122
00:08:24,910 --> 00:08:27,860
series we're going to keep adding to that low class and figure out some new
系列，我们将继续添加到低级课程，并找出一些新的

123
00:08:28,060 --> 00:08:31,160
things we can do and kind of keep improving it as we learn new concepts
我们学习新概念时可以做的事情以及不断改进的方法

124
00:08:31,360 --> 00:08:35,629
static is incredibly useful for static data data that doesn't change between
静态对于静态数据在两个数据之间不变时非常有用

125
00:08:35,830 --> 00:08:40,099
class instances but that we actually want to use in our classes thank you
类实例，但我们实际上要在类中使用谢谢

126
00:08:40,299 --> 00:08:43,609
guys for watching as always you can follow me on Twitter and Instagram links
像往常一样观看的家伙，您可以在Twitter和Instagram链接上关注我

127
00:08:43,809 --> 00:08:46,429
in the description below and if you really like the series you can support
在下面的说明中，如果您真的喜欢该系列，则可以支持

128
00:08:46,629 --> 00:08:48,629
on patreon.com for such the Cherno thank you
在patreon.com上感谢Cherno，谢谢

129
00:08:48,830 --> 00:08:51,809
everyone who's supporting this and making this possible love you guys I'll
所有人都支持这一点并使之成为可能的爱你们，我会

130
00:08:52,009 --> 00:09:00,120
see you next time goodbye [Music]
下次再见 

131
00:09:03,090 --> 00:09:08,090
[Music]
 [音乐] 

