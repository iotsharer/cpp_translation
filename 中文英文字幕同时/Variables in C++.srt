﻿1
00:00:00,000 --> 00:00:04,209
hey what's up guys my name is a Cherno welcome to another video so I'm on the
嘿，大家好，我叫Cherno，欢迎您观看其他视频，所以我在

2
00:00:04,410 --> 00:00:08,979
couch this time after a long day of work for I've mixed it up let's talk about
经过漫长的一天的工作，这次是沙发，因为我把它弄混了，让我们来谈谈

3
00:00:09,179 --> 00:00:14,319
variables in step above so when we write a program in C++ we
变量在上面的步骤中，所以当我们用C ++编写程序时

4
00:00:14,519 --> 00:00:19,239
want to be able to use data most of what programming is about is actually using
希望能够使用数据，而大多数编程内容实际上是在使用

5
00:00:19,439 --> 00:00:24,760
data we manipulate data that's what we do so any kind of data that we use in
数据我们处理的数据就是我们要做的，因此我们在其中使用的任何类型的数据

6
00:00:24,960 --> 00:00:28,600
our program that we want to change that we want to modify that we want to read
我们要更改的程序，我们要修改的内容，我们要阅读的内容

7
00:00:28,800 --> 00:00:32,919
and write from we need to store this data in something called a variable so
然后我们需要将这些数据存储在一个称为变量的东西中

8
00:00:33,119 --> 00:00:37,899
variables basically allow us to name a piece of data that we store in memory so
变量基本上使我们能够命名存储在内存中的一段数据，因此

9
00:00:38,100 --> 00:00:41,559
that we can keep using it as an example pretend that you're making a game and
我们可以继续使用它作为示例，假设您正在制作游戏， 

10
00:00:41,759 --> 00:00:45,820
you've got a player in your game and that player character has some kind of
您的游戏中有一名玩家，并且该玩家角色具有某种

11
00:00:46,020 --> 00:00:49,448
position on the map and the player character can move so we need to be able
在地图上的位置，并且玩家角色可以移动，因此我们需要

12
00:00:49,649 --> 00:00:53,140
to store the player's position as some kind of variable in our memory so that
将玩家的位置作为某种变量存储在我们的内存中，以便

13
00:00:53,340 --> 00:00:56,919
when it comes time to draw the player on the screen or interact with the rest of
当需要在屏幕上吸引玩家或与其他玩家互动时

14
00:00:57,119 --> 00:01:01,059
the level we can actually see hey where on earth is the player so we would want
我们实际上可以看到的水平，嘿，玩家到底在哪里，所以我们想要

15
00:01:01,259 --> 00:01:04,179
to store the players position in a variable this is basically one of the
将球员的位置存储在变量中，这基本上是

16
00:01:04,379 --> 00:01:08,918
fundamentals of writing a program in any language we need to be able to play with
用我们需要的任何语言编写程序的基本知识

17
00:01:09,118 --> 00:01:13,359
data and store that data somewhere when we create a variable is going to be
数据并将其存储在创建变量时的某个位置

18
00:01:13,560 --> 00:01:17,769
stored in memory in one of two places the stack or the heap don't worry we're
存储在内存中两个位置之一的堆栈或堆中，不用担心

19
00:01:17,969 --> 00:01:22,149
going to have a lot of videos discussing how memory actually work so if you're
将会有很多视频讨论内存的实际工作原理，因此如果您

20
00:01:22,349 --> 00:01:25,539
looking for a more more of an in-depth kind of explanation that'll definitely
寻找更多的深入解释，这肯定会

21
00:01:25,739 --> 00:01:29,528
come but for now just know that variables do occupy memory that's where
来吧，但现在只知道变量确实占用了内存

22
00:01:29,728 --> 00:01:34,028
we actually store the data in our computer's memory in C++ were given a
我们实际上将数据存储在C ++中的计算机内存中， 

23
00:01:34,228 --> 00:01:38,799
bunch of primitive data types these primitive data types essentially
一堆原始数据类型这些原始数据类型本质上是

24
00:01:39,000 --> 00:01:43,448
form the building blocks of any kind of data we store in our program each data
形成我们存储在程序中的每种数据的构造块

25
00:01:43,649 --> 00:01:47,619
type that Super Plus gives us has a specific purpose whilst it has a
 Super Plus给我们的类型具有特定目的，而它具有

26
00:01:47,819 --> 00:01:52,448
specific purpose you don't actually have to use it for that purpose if
特定目的，如果没有，您实际上不必使用它

27
00:01:52,649 --> 00:01:56,349
interesting because if applause is a very powerful language which means there
有趣的是，如果掌声是一种非常强大的语言，这意味着

28
00:01:56,549 --> 00:02:01,569
are actually very few rules when you actually get down to it so when I
当您真正遵守规则时，实际上很少有规则，所以当我

29
00:02:01,769 --> 00:02:05,679
explain variables I like to say that really the only distinction between the
解释变量我想说的是， 

30
00:02:05,879 --> 00:02:10,479
different variable types you have in Super Plus is the size how much memory
您在Super Plus中拥有的不同变量类型的大小是多少内存

31
00:02:10,679 --> 00:02:13,910
does this variable occupy when it comes down to it
这个变量归结到它时会占用它吗

32
00:02:14,110 --> 00:02:17,600
really the only difference between these primitive data types how big are they
实际上，这些原始数据类型之间的唯一区别是它们有多大

33
00:02:17,800 --> 00:02:20,600
let's go ahead and jump into Visual Studio and take a look at some examples
让我们继续前进，进入Visual Studio，看看一些示例

34
00:02:20,800 --> 00:02:25,490
so we've actually already got a variable type that we're using here int in stands
所以我们实际上已经有了一个变量类型，我们在看台上使用int 

35
00:02:25,689 --> 00:02:30,170
for integer and it lets us store an integer in a given range if we want to
对于整数，如果我们想让它在给定范围内存储整数

36
00:02:30,370 --> 00:02:33,910
declare a brand new variable we can do so by typing the type of the variable
声明一个全新的变量，我们可以通过键入变量的类型来做到这一点

37
00:02:34,110 --> 00:02:39,350
giving it some kind of name for example variable and then giving it a value
给它起某种名字，例如变量，然后给它一个值

38
00:02:39,550 --> 00:02:43,550
now this last part is optional you don't have to give it a value immediately but
现在这最后一部分是可选的，您不必立即为其赋值，但是

39
00:02:43,750 --> 00:02:46,790
for now let's just give it the value eight an integer is the data type that
现在我们只给它取值八，整数是

40
00:02:46,990 --> 00:02:50,600
is traditionally four bytes large the actual size of a data type
传统上，数据类型的实际大小大了四个字节

41
00:02:50,800 --> 00:02:54,350
depends on the compiler so it may be different depending on what compiler
取决于编译器，所以取决于哪个编译器可能有所不同

42
00:02:54,550 --> 00:02:57,740
you're using ultimately it's the compilers choice to tell you how big
您最终使用的是编译器选择来告诉您大小

43
00:02:57,939 --> 00:03:02,000
your data is going to be the Imps data type is meant for storing integers in a
您的数据将是Imps数据类型，旨在将整数存储在

44
00:03:02,199 --> 00:03:07,460
certain range because it's four bytes large we are limited as to what kind of
一定范围，因为它是四个字节大，我们限于哪种

45
00:03:07,659 --> 00:03:10,580
numbers we can store with it specifically this is something called a
我们可以用它存储的数字，这就是所谓的

46
00:03:10,780 --> 00:03:15,200
signed integer that can store a value of around negative two billion to positive
有符号整数，可以存储大约负20亿到正的值

47
00:03:15,400 --> 00:03:19,730
2 billion anything larger or smaller than that is going to require more data
 20亿个大于或小于20亿的数据将需要更多数据

48
00:03:19,930 --> 00:03:24,800
to store than this int actually support so with 4 bytes of data we can store a
存储比此int实际支持的数据多的数据，因此我们可以存储4个字节的数据

49
00:03:25,000 --> 00:03:29,060
value between this range so let's go ahead and try and print out our variable
值在此范围之间，让我们继续尝试并打印出变量

50
00:03:29,259 --> 00:03:32,660
to the console to see what it actually is I'll substitute this hello world with
到控制台，看看它到底是什么，我将用这个问候世界代替

51
00:03:32,860 --> 00:03:37,520
this actual variable this is how we can log a variable to the console at nf5 to
这个实际的变量，这就是我们如何在nf5处将变量记录到控制台以

52
00:03:37,719 --> 00:03:41,810
run our program and you can see it prints out the number 8 awesome we can
运行我们的程序，您会看到它打印出了8个很棒的数字，我们可以

53
00:03:42,009 --> 00:03:45,110
go ahead and modify our variable for example by reassigning it to something
继续修改我们的变量，例如通过将其重新分配给某些变量

54
00:03:45,310 --> 00:03:49,910
else like 20 let's go ahead and print it again and see what happens so we're
像20一样，让我们​​继续进行打印，看看会发生什么，所以我们

55
00:03:50,110 --> 00:03:53,420
printing it once here and then again here so we should get the value 8
在这里打印一次然后在这里再次打印，所以我们应该得到值8 

56
00:03:53,620 --> 00:03:56,930
printing first and then the value 20 and you can see if we run our program that's
首先打印，然后打印值20，您可以看到我们是否运行了以下程序

57
00:03:57,129 --> 00:04:01,550
exactly what we get cool so as I said an inch data type can store a value between
就像我们说的，英寸数据类型可以存储一个介于

58
00:04:01,750 --> 00:04:06,259
negative 2 billion and positive 2 billion so you might be like why is it
负20亿和正20亿，所以您可能会想为什么

59
00:04:06,459 --> 00:04:09,740
negative 2 billion and positive 2 billion it's not exactly 2 billion by
负20亿和正20亿到20亿不完全相同

60
00:04:09,939 --> 00:04:13,280
the way it's like 2 point something billion where are these limits coming
就像2点东西十亿这些限制在哪里

61
00:04:13,479 --> 00:04:17,060
from do they make any sense and the answer is yes they make sense they are
他们从任何意义上说，答案是肯定的，他们是有道理的

62
00:04:17,259 --> 00:04:21,468
directly tied with the size of the variable that is how much data were
与变量的大小直接相关，变量的大小是多少数据

63
00:04:21,668 --> 00:04:26,210
allowed to store in it an integer is 4 bytes with 4 bytes of data we can store
允许在其中存储一个整数，该整数是4字节，我们可以存储4字节的数据

64
00:04:26,410 --> 00:04:29,129
values in that let's break this down a little bit so
值，让我们分解一下

65
00:04:29,329 --> 00:04:34,319
one byte is eight bits of data which means that four bytes is 32 bits of data
 1个字节是8位数据，这意味着4个字节是32位数据

66
00:04:34,519 --> 00:04:39,270
because this variable is signed meaning it can be negative it contains a sign
因为此变量是带符号的，所以可以为负，它包含一个符号

67
00:04:39,470 --> 00:04:43,980
like a negative sign because this variable is find one of those bits one
像负号一样，因为此变量是那些位之一

68
00:04:44,180 --> 00:04:48,629
of those 32 bits has to be for the sign so that we know if it's positive or
这32位中的位必须是正负号，以便我们知道它是否为正或

69
00:04:48,829 --> 00:04:52,920
negative which only leaves 31 bits left for the actual number now a bit can
负数，只剩下31位保留为实际数字，现在一位可以

70
00:04:53,120 --> 00:04:57,389
either be 0 or 1 so there are 2 possible values for 1 bit of data so using a
可以是0或1，因此1位数据有2个可能的值，因此使用

71
00:04:57,589 --> 00:05:01,110
little bit of maths here we can say that we have 31 bits to play with 2 possible
这里可以说一点数学，我们有31位可以玩2种可能

72
00:05:01,310 --> 00:05:05,460
values took bit so what is 2 to the power of 31 if we crack open a
值花了一点，所以如果我们破解一个

73
00:05:05,660 --> 00:05:10,500
calculator here and type in 2 to the power of 31 we will get about 2 billion
在这里输入2并乘以31的幂，我们将得到约20亿

74
00:05:10,699 --> 00:05:15,870
that value there that 2.1 billion that is the maximum number that we can store
那里的价值是21亿，这是我们可以存储的最大数量

75
00:05:16,069 --> 00:05:19,680
with an integer now remember we also have one bit that is reserved for
现在有一个整数，请记住，我们还有一个保留给

76
00:05:19,879 --> 00:05:22,740
whether or not that number is negative so because of that we can store up to
该数字是否为负，因此我们可以存储多达

77
00:05:22,939 --> 00:05:26,370
that number from 0 but also we can go the other way and store all the negative
从0开始的那个数字，但是我们也可以反过来存储所有负数

78
00:05:26,569 --> 00:05:31,170
values down to negative 2 by 1 billion but I don't want negative values I hear
值降低到负2十亿，但我不希望听到负值

79
00:05:31,370 --> 00:05:34,560
you say is there a way to just get rid of that one bit being for the negative
你说有办法摆脱那一点是负面的

80
00:05:34,759 --> 00:05:40,199
sign and just use it as part of my number why yes yes there is that is what
签名并将其用作我的电话号码的一部分为什么是的就是那是什么

81
00:05:40,399 --> 00:05:43,710
we call an unsigned number that means it's the number that does not have a
我们将其称为无符号数字，这意味着该数字没有

82
00:05:43,910 --> 00:05:48,899
sign meaning is always positive in C++ we can make one of those by just typing
在C ++中，符号含义始终为正，我们只需输入以下内容即可

83
00:05:49,098 --> 00:05:53,520
in unsigned in front of our integer so now what we've done is we have 32 bits
在整数前面的unsigned中，所以现在我们要做的是32位

84
00:05:53,720 --> 00:05:58,920
to play with and 2 to the power of 32 of course is double what we have here for
和2和32的幂一起玩当然是我们在这里的两倍

85
00:05:59,120 --> 00:06:03,569
point two nine billion and that's basically what the unsigned keyword does
指向两个九十亿，这基本上就是unsigned关键字的作用

86
00:06:03,769 --> 00:06:08,310
in Sabre slot it lets us define an integer that does not have a sign bit
在Saber插槽中，我们可以定义没有符号位的整数

87
00:06:08,509 --> 00:06:11,520
okay so what other data types do we have available to us what if I don't want to
好的，那么我们还有哪些其他数据类型可供使用，如果我不想

88
00:06:11,720 --> 00:06:15,329
for byte integer what other types are there so as far as integer values goes
对于字节整数，就整数值而言，还有其他类型

89
00:06:15,529 --> 00:06:19,770
we actually have quite a few we've got char which is one byte of data we've got
实际上，我们有很多字符，char是一个字节的数据

90
00:06:19,970 --> 00:06:24,540
short which is two bytes of data we have int which is four bytes of data we have
 short这是我们拥有的两个字节的数据int这是我们拥有的四个字节的数据

91
00:06:24,740 --> 00:06:29,759
long which is also usually four bytes of data depending on the compiler and then
 long，通常也是四个字节的数据，具体取决于编译器，然后

92
00:06:29,959 --> 00:06:33,960
we have long long which is usually eight bytes of data there's also other types
我们很长很长，通常是八个字节的数据，还有其他类型

93
00:06:34,160 --> 00:06:37,050
like long and there are a few different modifications I'm not going to go
就像很久了，我不会去做一些不同的修改

94
00:06:37,250 --> 00:06:41,399
through all of them but basic ones are these five you can also
通过所有这些，但基本的是这五个，您也可以

95
00:06:41,598 --> 00:06:45,660
add on signs to any of these and it will remove that sign bit and let you set a
在其中任何一个上添加符号，它将删除该符号位，并让您设置一个

96
00:06:45,860 --> 00:06:49,769
larger number chart traditionally is also used for storing characters not
传统上，较大的数字图表还用于存储字符

97
00:06:49,968 --> 00:06:53,429
just numbers so above I'm assigning numbers to it like 50 you can also
只是数字，所以上面我给它分配了数字，例如50，你也可以

98
00:06:53,629 --> 00:06:56,669
assign characters to it like a now that's not to say you can't assign
像现在这样分配字符，这并不是说您不能分配

99
00:06:56,869 --> 00:07:00,088
characters to other integers you can because at the end of the day this
可以将字符转换为其他整数，因为在一天结束时

100
00:07:00,288 --> 00:07:04,468
character that I five to this letter a is just a number in fact that number
我给这封信一个五的字符实际上只是一个数字

101
00:07:04,668 --> 00:07:10,109
that numeric value associated with that character the character a is 65 now if
如果与该字符关联的数字值，则字符a现在为65 

102
00:07:10,309 --> 00:07:13,769
numbers are just characters and if characters are just numbers then why
数字只是字符，如果字符只是数字，那为什么

103
00:07:13,968 --> 00:07:18,209
exactly do we have this distinction why do I say that char is specifically used
我们到底有这个区别，为什么我说char是专门使用的

104
00:07:18,408 --> 00:07:22,679
for characters whereas it's really not that is because we often as programmers
字符，但实际上不是因为我们经常作为程序员

105
00:07:22,879 --> 00:07:26,939
make assumptions about certain data types if I pass in a char and call it
如果我传入一个char并将其命名，则对某些数据类型进行假设

106
00:07:27,139 --> 00:07:30,239
something like character I usually expect you to actually assign a
我通常希望您像角色这样的角色实际分配一个

107
00:07:30,439 --> 00:07:33,749
character to it so a good example of this is if you actually try and print
字符，所以一个很好的例子是，如果您实际尝试打印

108
00:07:33,949 --> 00:07:39,538
out a char if I print out this variable a for example and I hit f5 I'm not going
出一个字符，如果我打印出这个变量a例如，我按f5我就不去了

109
00:07:39,738 --> 00:07:43,049
to get the number associated with it I'm going to get the character a printed out
得到与之相关的数字，我将把字符打印出来

110
00:07:43,249 --> 00:07:47,489
so if I replace this with this actual numeric value like 65 I'm also going to
因此，如果我将其替换为65这样的实际数值，我也会

111
00:07:47,689 --> 00:07:52,559
get the value a printed out as you can see over here because C out if I pass in
您可以在此处看到打印出来的值，因为如果我传入，则输出C 

112
00:07:52,759 --> 00:07:57,629
a char into C out is going to treat it like a character not like a number if I
如果我把一个字符放到C中，它将把它当作字符而不是数字对待

113
00:07:57,829 --> 00:08:02,129
change it to be some other type like a short for example and hit f5 you can see
将其更改为其他类型（例如短裤），然后按f5即可看到

114
00:08:02,329 --> 00:08:05,519
that C out no longer treats it like a character it's going to actually print
 C out不再将其视为将要实际打印的字符

115
00:08:05,718 --> 00:08:09,449
out the numeric value and even if I assign a character here it's just really
数值，即使我在这里分配了一个字符， 

116
00:08:09,649 --> 00:08:14,999
assigning the value 65 so if I run this again you can see that we get 65 so the
分配值65，所以如果我再次运行该命令，您会看到我们得到65，因此

117
00:08:15,199 --> 00:08:17,160
reason I'm telling you all this is because I want you to understand that
我告诉你这一切的原因是因为我希望你能理解

118
00:08:17,360 --> 00:08:23,728
data types the usage of data types is just up to the programmer really there
数据类型数据类型的用法仅取决于程序员

119
00:08:23,928 --> 00:08:27,569
are certain conventions that we have in place but there's nothing concrete that
是我们已经制定的某些约定，但没有具体的规定

120
00:08:27,769 --> 00:08:32,368
you have to actually follow there are very little rules and c++ after all so
毕竟，您实际上必须遵循很少的规则和c ++ 

121
00:08:32,568 --> 00:08:35,758
because of that I do want you to realize that the only real difference between
因此，我确实希望您认识到， 

122
00:08:35,958 --> 00:08:40,769
these data types is how much memory will be allocated when you create a variable
这些数据类型是创建变量时将分配多少内存

123
00:08:40,969 --> 00:08:44,128
with that data type so with those integer types aside what
与该数据类型，所以除了那些整数类型什么

124
00:08:44,328 --> 00:08:48,448
if I want to store a decimal value for example five point five how do I do that
如果我想存储一个十进制值，例如五点五，我该怎么做

125
00:08:48,649 --> 00:08:51,629
well for that we have two data types we have float and we
为此，我们有两种数据类型，分别是float和

126
00:08:51,830 --> 00:08:55,319
have double there are also some modifies that you can do like long double we're
拥有双倍，还有一些修改，您可以像长双倍一样

127
00:08:55,519 --> 00:08:59,819
not going to get into those so a float is basically a decimal value that we can
不会进入那些，所以浮点数基本上是一个十进制值，我们可以

128
00:09:00,019 --> 00:09:04,500
store that occupies four bytes of data so let's define a variable here such as
存储占用了四个字节的数据，所以让我们在这里定义一个变量，例如

129
00:09:04,700 --> 00:09:08,039
5.5 how do we do that let's also replace this variable a we're
 5.5我们如何做到这一点，让我们也替换这个变量

130
00:09:08,240 --> 00:09:10,769
printing out our float variable and compile our file
打印出我们的float变量并编译我们的文件

131
00:09:10,970 --> 00:09:15,179
let's hit f5 to run our program and you can see we get 5.5 printed out fantastic
让我们按f5键运行我们的程序，您会看到我们得到5.5出色的打印输出

132
00:09:15,379 --> 00:09:19,500
now you may think that you've defined a float here but you actually haven't
现在您可能会认为您已经在此处定义了一个浮动，但实际上您还没有

133
00:09:19,700 --> 00:09:23,309
you've actually defined a double if we go back to visual studio and we hover
如果我们回到Visual Studio，然后将鼠标悬停，您实际上已经定义了一个double 

134
00:09:23,509 --> 00:09:26,699
our mouse over this value you can see that in brackets it says double as I
我们将鼠标移到该值上，您可以看到括号中的数字是我的两倍

135
00:09:26,899 --> 00:09:30,779
just mentioned we have two different variables that we can use to store
刚才提到，我们可以使用两个不同的变量来存储

136
00:09:30,980 --> 00:09:35,399
decimal numbers float and double so how do we discern between what a double is
十进制数是浮点数还是双精度数，那么我们如何区分双精度数是什么

137
00:09:35,600 --> 00:09:39,899
and what a float is the way we do that is by basically appending an F to our
而浮动的方式是基本上将F附加到

138
00:09:40,100 --> 00:09:44,189
float variables it can be lowercase or uppercase doesn't matter but the point
浮点变量，可以是小写或大写，但要点

139
00:09:44,389 --> 00:09:48,269
is if we have an F you can see that we've actually declared a flirt so
如果我们有一个F，您可以看到我们实际上已经声明了调情，所以

140
00:09:48,470 --> 00:09:52,919
floats are basically four bytes large and doubles are eight bytes large
浮点数基本上是四个字节，双打是八个字节

141
00:09:53,120 --> 00:09:57,509
finally we have one more primitive datatype to play with and that is bull
最后，我们还有一个原始数据类型可以使用

142
00:09:57,710 --> 00:10:03,029
now bull stands for bullying and it can either be true or false if we try and
现在牛市代表欺凌，如果我们尝试

143
00:10:03,230 --> 00:10:06,719
print it to our console and hit f5 you can see that we'll actually get a
将其打印到我们的控制台并按f5键，您会看到我们实际上将获得一个

144
00:10:06,919 --> 00:10:11,549
numeric value one because of course there's no such thing as true or false
数值一，因为当然没有true或false 

145
00:10:11,750 --> 00:10:16,409
those are English words computer deal with numbers so basically zero means
那些是英语单词，计算机处理数字，所以基本上是零

146
00:10:16,610 --> 00:10:22,409
false and anything except zero any other number means true in this case we'll
 false和除零以外的其他任何数字，在这种情况下表示true 

147
00:10:22,610 --> 00:10:26,669
actually get one printing to the console indicating that it is true if we change
实际在控制台上打印了一次，表明如果我们进行更改，这是真的

148
00:10:26,870 --> 00:10:31,349
the default and run our program we will get 0 which means false the bull' data
默认并运行我们的程序，我们将得到0，这意味着错误的公牛数据

149
00:10:31,549 --> 00:10:37,500
type occupies one byte of memory now you might be wondering one byte why the bull
类型占用一个字节的内存，现在您可能想知道一个字节为什么公牛

150
00:10:37,700 --> 00:10:40,799
can either be true or false surely that only takes one bit to
可以肯定是假，只需要一点点

151
00:10:41,000 --> 00:10:45,539
represent and you are correct it does take one bit to represent however when
表示，你是正确的，但是确实需要一点时间来表示

152
00:10:45,740 --> 00:10:49,409
we're dealing with addressing memory that is we to retrieve our ball from
我们正在处理寻址内存，这是我们从中检索球

153
00:10:49,610 --> 00:10:52,979
memory or stored in memory there is no way for us to actually address
内存或存储在内存中，我们无法实际寻址

154
00:10:53,179 --> 00:10:57,329
individual bits we can only address bytes so because of that we can't
单个位，我们只能寻址字节，因此，我们不能

155
00:10:57,529 --> 00:11:01,349
actually create a variable type that is one bit because we'd need to be able to
实际上创建一个变量类型，因为我们需要能够

156
00:11:01,549 --> 00:11:06,599
access it and we can't we can only access by now course one thing you could
访问它，我们现在只能访问一件事

157
00:11:06,799 --> 00:11:10,769
do on the other hand is be really smart about how you store data and store eight
另一方面，在存储数据和存储八个数据方面确实很聪明

158
00:11:10,970 --> 00:11:16,620
all in one bite that's totally okay one bit per bull but you still have that one
一口气，每头牛完全可以咬一口，但是你仍然可以咬一口

159
00:11:16,820 --> 00:11:21,449
byte of allocating memory we'll probably talk about advanced fun tricks like that
分配内存的字节，我们可能会谈论类似的高级有趣技巧

160
00:11:21,649 --> 00:11:26,959
in the future but for now a bull is one byte of memory so with all this talk of
在将来，但就目前而言，公牛是一个字节的内存，因此， 

161
00:11:27,159 --> 00:11:33,629
sizes and bytes and how much everything takes how how do we actually know how
大小和字节以及所有内容需要多少信息我们如何实际知道

162
00:11:33,830 --> 00:11:36,990
big a data type is it is dependent on the compiler after all is there
数据类型很大吗，毕竟它依赖于编译器

163
00:11:37,190 --> 00:11:41,490
somewhere we can check yes yes there is there's an operator we have available to
我们可以检查某处的地方是，那里有我们可以提供的操作员

164
00:11:41,690 --> 00:11:46,079
us and zip applause called size of so if we come over here and we print size of
我们和zip掌声叫作size，所以如果我们来到这里并打印出size 

165
00:11:46,279 --> 00:11:50,429
wool for example we basically just type in the word size of and then either in
例如，羊毛，我们基本上只输入字号，然后输入

166
00:11:50,629 --> 00:11:55,169
brackets or not doesn't really matter although I do prefer to use brackets or
括号与否无关紧要，尽管我更喜欢使用括号或

167
00:11:55,370 --> 00:11:59,519
parentheses I should say we type in our data type header five you can see it
括号我应该说我们在数据类型标题中输入五个，可以看到它

168
00:11:59,720 --> 00:12:04,469
tells us that a bull is one byte if I replace this with int and hit f5 we have
告诉我们，如果我将int替换为int并击中f5，则公牛是一个字节

169
00:12:04,669 --> 00:12:09,569
four and if I do something like double and hit f5 we have eight awesome
四个，如果我做类似double的操作并击中f5，我们将有八个很棒

170
00:12:09,769 --> 00:12:14,309
critical stuff so that's basically all there is to variables or at least the
关键的东西，所以基本上就是变量或至少

171
00:12:14,509 --> 00:12:17,490
primitive types that I've covered there are many different types that you can
我已经介绍过的原始类型可以有很多不同的类型

172
00:12:17,690 --> 00:12:21,209
actually create in C++ and that have already been created for you however
实际上是用C ++创建的，但是已经为您创建了

173
00:12:21,409 --> 00:12:25,349
they're all custom types that are all based on these primitive types these are
它们都是基于这些原始类型的自定义类型，它们是

174
00:12:25,549 --> 00:12:30,059
the building blocks that we use to define and store any kind of data we
我们用来定义和存储任何类型数据的构建块

175
00:12:30,259 --> 00:12:33,990
could possibly create now with any of these primitive data types we also have
可能现在可以使用我们也拥有的任何原始数据类型创建

176
00:12:34,190 --> 00:12:37,949
the ability to turn them into pointers or references pointers can be declared
可以声明将它们转换为指针或引用指针的能力

177
00:12:38,149 --> 00:12:43,979
by writing an asterisk next to your type like this and references by an ampersand
像这样在您的类型旁边写一个星号，并用“＆”号引用

178
00:12:44,179 --> 00:12:49,169
next year type pointers and references are such huge and complicated and vital
明年类型指针和引用是如此巨大，复杂和至关重要

179
00:12:49,370 --> 00:12:52,919
topics that I really want to save them for separate videos so that you guys
我真的想将它们保存为单独的视频的主题，以便你们

180
00:12:53,120 --> 00:12:56,039
understand them properly so for now for this video we're just kind of stick with
正确理解它们，因此对于此视频，我们现在仍然坚持

181
00:12:56,240 --> 00:12:59,219
these primitive types make sure you understand them they're going to be the
这些原始类型确保您了解它们，它们将成为

182
00:12:59,419 --> 00:13:03,359
basis for pretty much every scene you ever write so they're really important
您编写的几乎所有场景的基础，因此它们非常重要

183
00:13:03,559 --> 00:13:06,599
but anyway I hope you guys enjoyed this video if you did please hit that like
但无论如何，我希望你们喜欢这部影片，请按一下

184
00:13:06,799 --> 00:13:11,250
button you can also follow me on Twitter and Instagram and if you really like
按钮，您也可以在Twitter和Instagram上关注我，如果您真的喜欢

185
00:13:11,450 --> 00:13:13,789
this video and you want to be a part of how to
这个视频，你想成为如何

186
00:13:13,990 --> 00:13:17,539
videos get made you want to contribute to the planning of these videos as well
视频使您也想为这些视频的规划做出贡献

187
00:13:17,740 --> 00:13:22,370
as receive early drafts of videos as I'm making them then please support me on
作为我制作视频时收到的视频的早期草稿，然后请继续支持我

188
00:13:22,570 --> 00:13:26,508
patreon link will be in the description of this video your support there is what
 patreon链接将在该视频的描述中，您的支持是

189
00:13:26,708 --> 00:13:29,719
makes these videos possible thanks for watching guys I'll see you next time
使这些视频成为可能感谢收看的家伙下次见

190
00:13:29,919 --> 00:13:32,309
goodbye
再见

191
00:13:33,559 --> 00:13:40,939
[Music]
[音乐]

192
00:13:43,200 --> 00:13:48,200
you
您

