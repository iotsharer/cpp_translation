﻿1
00:00:00,000 --> 00:00:01,660
嘿，大家好，我叫Chan，欢迎回到安全的老板班
hey what's up guys my name is a Chan and

2
00:00:01,860 --> 00:00:02,859
嘿，大家好，我叫Chan，欢迎回到安全的老板班
welcome back to my safe boss class

3
00:00:03,060 --> 00:00:04,629
在今天的系列文章中，我们将讨论C ++中的箭头运算符
series today we're talking all about the

4
00:00:04,830 --> 00:00:07,118
在今天的系列文章中，我们将讨论C ++中的箭头运算符
arrow operator in C++ we're going to

5
00:00:07,318 --> 00:00:08,140
讨论箭头运算符实际上对结构和类的作用
talk about what the arrow operator

6
00:00:08,339 --> 00:00:10,359
讨论箭头运算符实际上对结构和类的作用
actually does for both struct and class

7
00:00:10,558 --> 00:00:12,460
指针以及实现我们自己的箭头运算符以查看其工作原理
pointers as well as implement our own

8
00:00:12,660 --> 00:00:14,710
指针以及实现我们自己的箭头运算符以查看其工作原理
arrow operator to see how it works so

9
00:00:14,910 --> 00:00:16,060
在这里，如果我创建这个，我的基本实体类类型的源代码现在
over here my source code of our basic

10
00:00:16,260 --> 00:00:17,949
在这里，如果我创建这个，我的基本实体类类型的源代码现在
entity class type now if I create this

11
00:00:18,149 --> 00:00:19,960
对象正常，因为我可能希望完整打印此文件，但没有问题，但是
object normally as I probably would like

12
00:00:20,160 --> 00:00:22,089
对象正常，因为我可能希望完整打印此文件，但没有问题，但是
this in full print I have no issues but

13
00:00:22,289 --> 00:00:24,159
如果这个实体对象实际上是一个指针，那么它要么被分配
if this entity object was actually a

14
00:00:24,359 --> 00:00:26,800
如果这个实体对象实际上是一个指针，那么它要么被分配
pointer so either it's it was allocated

15
00:00:27,000 --> 00:00:28,239
在堆上，或者出于某种原因，我只是有一个指向它的指针
on the heap or maybe I just had a

16
00:00:28,439 --> 00:00:30,190
在堆上，或者出于某种原因，我只是有一个指向它的指针
pointer to it for some reason like this

17
00:00:30,390 --> 00:00:32,168
为了调用该打印功能，我实际上不能只使用指针点
in order to call that print function I

18
00:00:32,368 --> 00:00:34,479
为了调用该打印功能，我实际上不能只使用指针点
can't actually just use pointer dot

19
00:00:34,679 --> 00:00:37,000
这样打印，因为这只是一个指针，基本上只是一个数字
print like that because this is just a

20
00:00:37,200 --> 00:00:38,739
这样打印，因为这只是一个指针，基本上只是一个数字
pointer it's basically just a numerical

21
00:00:38,939 --> 00:00:40,209
我不能仅仅将值叫做点打印，我要做的实际上是
value I can't just called dot print on

22
00:00:40,409 --> 00:00:41,829
我不能仅仅将值叫做点打印，我要做的实际上是
it what I have to actually do is

23
00:00:42,030 --> 00:00:43,839
取消引用该指针，可以这样做，所以我只能说实体
dereference that pointer and that can be

24
00:00:44,039 --> 00:00:46,390
取消引用该指针，可以这样做，所以我只能说实体
done like so I can just say entity

25
00:00:46,590 --> 00:00:48,608
例如参考实体，然后在该点前面使用星号
reference entity for example and then

26
00:00:48,808 --> 00:00:50,948
例如参考实体，然后在该点前面使用星号
use the asterisk in front of the point

27
00:00:51,149 --> 00:00:52,959
像这样取消引用，然后用实体和我的
like this to dereference it and then

28
00:00:53,159 --> 00:00:55,209
像这样取消引用，然后用实体和我的
just substitute this with entity and my

29
00:00:55,409 --> 00:00:57,338
卡现在可以避免出现多余的行，我也可以做的就是使用指针
card works now to avoid this extra line

30
00:00:57,539 --> 00:00:59,498
卡现在可以避免出现多余的行，我也可以做的就是使用指针
what I could also do is use my pointer

31
00:00:59,698 --> 00:01:01,419
但是用括号括起来并像这样取消引用它，所以现在我不能只是
but surrounded with parentheses and

32
00:01:01,619 --> 00:01:03,849
但是用括号括起来并像这样取消引用它，所以现在我不能只是
dereference it like so now I can't just

33
00:01:04,049 --> 00:01:05,590
由于运算符优先级而编写这样的代码，实际上它会尝试转到
write code like this because of operator

34
00:01:05,790 --> 00:01:07,269
由于运算符优先级而编写这样的代码，实际上它会尝试转到
precedence it'll actually try and go to

35
00:01:07,469 --> 00:01:09,759
对象或打印，然后取消引用打印结果，但显然不
the object or print and then dereference

36
00:01:09,959 --> 00:01:12,159
对象或打印，然后取消引用打印结果，但显然不
the result of print that's obviously not

37
00:01:12,359 --> 00:01:13,719
要上班，所以您必须先进行解引用，然后
going to work so you have you have to

38
00:01:13,920 --> 00:01:15,278
要上班，所以您必须先进行解引用，然后
actually do the dereferencing first and

39
00:01:15,478 --> 00:01:17,799
然后调用点打印，这没关系，它可以正常工作，但看起来像
then call dot print now this this is

40
00:01:18,000 --> 00:01:20,859
然后调用点打印，这没关系，它可以正常工作，但看起来像
okay and it works fine but it looks a

41
00:01:21,060 --> 00:01:23,140
有点笨拙，所以我们可以做的只是使用箭头运算符
little bit clunky so what we can do

42
00:01:23,340 --> 00:01:24,549
有点笨拙，所以我们可以做的只是使用箭头运算符
instead is just use the arrow operator

43
00:01:24,750 --> 00:01:26,649
而不是取消引用指针然后调用点打印，我们可以替代
instead of dereferencing the pointer and

44
00:01:26,849 --> 00:01:28,869
而不是取消引用指针然后调用点打印，我们可以替代
then calling dot print we can substitute

45
00:01:29,069 --> 00:01:31,269
所有这些都只有一个箭头像这样打印，而这实际上是
all of that with just an arrow to print

46
00:01:31,469 --> 00:01:32,769
所有这些都只有一个箭头像这样打印，而这实际上是
like this and what this actually does is

47
00:01:32,969 --> 00:01:35,649
取消引用该实体指针，使其成为所有实体类型，然后调用
dereference that entity pointer into it

48
00:01:35,849 --> 00:01:37,299
取消引用该实体指针，使其成为所有实体类型，然后调用
just an all entity type and then calls

49
00:01:37,500 --> 00:01:39,099
打印，这几乎就是它的全部，这只是拥有它的捷径
print so that's pretty much all there is

50
00:01:39,299 --> 00:01:41,378
打印，这几乎就是它的全部，这只是拥有它的捷径
to it it's just a shortcut for having it

51
00:01:41,578 --> 00:01:43,149
因为我们不得不手动取消所有事件的引用
for us having to manually dereference

52
00:01:43,349 --> 00:01:44,079
因为我们不得不手动取消所有事件的引用
events around everything with

53
00:01:44,280 --> 00:01:46,299
括号，然后也调用我们的函数或变量，而不是执行
parentheses and then call our function

54
00:01:46,500 --> 00:01:48,519
括号，然后也调用我们的函数或变量，而不是执行
or our variable as well instead of doing

55
00:01:48,719 --> 00:01:49,959
如果我有一些变量，我们也可以只使用箭头最差变量
all that we can just use an arrow worst

56
00:01:50,159 --> 00:01:51,969
如果我有一些变量，我们也可以只使用箭头最差变量
variable as well if I had some variables

57
00:01:52,170 --> 00:01:54,549
在这里，我将公开int X，例如，我也可以知道
over here I'll just make public int X

58
00:01:54,750 --> 00:01:57,189
在这里，我将公开int X，例如，我也可以知道
for example I could also just you know

59
00:01:57,390 --> 00:01:59,049
通过这样的箭头访问X，然后将其设置为等于我想要的
access X through the arrow like this and

60
00:01:59,250 --> 00:02:00,819
通过这样的箭头访问X，然后将其设置为等于我想要的
then set it equal to whatever I wanted

61
00:02:01,019 --> 00:02:02,439
很酷，所以这几乎是箭头的默认用例
to like so okay cool so that's pretty

62
00:02:02,640 --> 00:02:03,939
很酷，所以这几乎是箭头的默认用例
much the default use case for the arrow

63
00:02:04,140 --> 00:02:05,259
运算符可能就是您90％的时间使用它的方式
operator that's probably how you're

64
00:02:05,459 --> 00:02:08,229
运算符可能就是您90％的时间使用它的方式
using it 90% of the time however as an

65
00:02:08,429 --> 00:02:10,270
C ++中的运算符实际上可以重载它，并且
operator in C++ it is actually possible

66
00:02:10,469 --> 00:02:12,368
C ++中的运算符实际上可以重载它，并且
to overload it and

67
00:02:12,568 --> 00:02:14,230
在您自己的自定义类中使用它，我将向您展示一个示例，说明为什么
use it in your own custom classes and

68
00:02:14,430 --> 00:02:15,368
在您自己的自定义类中使用它，我将向您展示一个示例，说明为什么
I'll show you an example of why you

69
00:02:15,568 --> 00:02:17,200
可能想这样做，以及如何在这里做到这一点，所以假设我是
might want to do that and how you can do

70
00:02:17,400 --> 00:02:18,969
可能想这样做，以及如何在这里做到这一点，所以假设我是
that over here so suppose that I was

71
00:02:19,169 --> 00:02:20,590
编写某种智能指针类，例如订阅指向以保持它
writing some kind of smart pointer class

72
00:02:20,789 --> 00:02:23,080
编写某种智能指针类，例如订阅指向以保持它
like subscribe to point up to keep it

73
00:02:23,280 --> 00:02:24,849
简单，当我构造这个时，我只是要有一个实体指针
simple I'm just going to have it have an

74
00:02:25,049 --> 00:02:27,340
简单，当我构造这个时，我只是要有一个实体指针
entity pointer when I construct this

75
00:02:27,539 --> 00:02:28,299
脚本指针，我将实体作为一个
script pointer

76
00:02:28,498 --> 00:02:30,429
脚本指针，我将实体作为一个
I'm going to take in an entity as a

77
00:02:30,628 --> 00:02:32,800
参数，然后将其分配给析构函数中的对象
parameter here and then assign it to my

78
00:02:33,000 --> 00:02:34,840
参数，然后将其分配给析构函数中的对象
object in the destructor I'm going to

79
00:02:35,039 --> 00:02:38,319
呼叫删除实体或删除M ulchhhh，所以现在我有了一个基本范围
call delete entity or delete M ulchhhh

80
00:02:38,519 --> 00:02:39,969
呼叫删除实体或删除M ulchhhh，所以现在我有了一个基本范围
and so now I've got a basically scope to

81
00:02:40,169 --> 00:02:41,230
指向一个类，该类将在退出时自动删除我的实体
point a class that will automatically

82
00:02:41,430 --> 00:02:43,330
指向一个类，该类将在退出时自动删除我的实体
delete my entity when it goes out of

83
00:02:43,530 --> 00:02:45,340
作用域，这样我就可以像源代码指针实体等于新实体一样使用它，并且
scope so I can use it like source code

84
00:02:45,539 --> 00:02:47,920
作用域，这样我就可以像源代码指针实体等于新实体一样使用它，并且
pointer entity equals new entity and

85
00:02:48,120 --> 00:02:49,689
看起来不错，现在我希望能够调用此打印
that looks pretty good now I want to

86
00:02:49,889 --> 00:02:52,118
看起来不错，现在我希望能够调用此打印
actually be able to call this print

87
00:02:52,318 --> 00:02:54,249
函数或访问此X变量，所以我现在该怎么办呢？
function or access this X variable so

88
00:02:54,449 --> 00:02:56,830
函数或访问此X变量，所以我现在该怎么办呢？
how do I do that well right now I can't

89
00:02:57,030 --> 00:02:59,769
真的像我可以使用圆点，但后来我可以公开或
really like I can use dots but then like

90
00:02:59,968 --> 00:03:01,780
真的像我可以使用圆点，但后来我可以公开或
I could make either this public or maybe

91
00:03:01,979 --> 00:03:04,480
我可以拥有一些返回实体指针的东西，而对象却像
I could just have something that returns

92
00:03:04,680 --> 00:03:06,640
我可以拥有一些返回实体指针的东西，而对象却像
an entity pointer like yet object like

93
00:03:06,840 --> 00:03:09,509
这也许会返回我看起来太凌乱的对象，我想
this maybe that will return my object

94
00:03:09,709 --> 00:03:12,700
这也许会返回我看起来太凌乱的对象，我想
that just looks way too messy I want to

95
00:03:12,900 --> 00:03:14,709
能够像堆分配的实体一样使用它，我希望能够使用它
be able to use it like a heap-allocated

96
00:03:14,908 --> 00:03:17,230
能够像堆分配的实体一样使用它，我希望能够使用它
entity right I want to be able to use it

97
00:03:17,430 --> 00:03:19,149
好像我写了这样的代码，这意味着我可以写得很好
as if I'd written code like this which

98
00:03:19,348 --> 00:03:21,129
好像我写了这样的代码，这意味着我可以写得很好
would mean that I could just write well

99
00:03:21,329 --> 00:03:23,349
那，它会很好，我希望能够代替它并拥有它
that and it would work fine I want to be

100
00:03:23,549 --> 00:03:25,090
那，它会很好，我希望能够代替它并拥有它
able to just substitute this and have it

101
00:03:25,289 --> 00:03:27,610
可以以相同的方式很好地使用，您可以在其中重载箭头
kind of be used the same way well that's

102
00:03:27,810 --> 00:03:28,599
可以以相同的方式很好地使用，您可以在其中重载箭头
where you can overload the arrow

103
00:03:28,799 --> 00:03:30,580
运算符使其代替您获取对象，我可以编写实体指针
operator make it do that for you instead

104
00:03:30,780 --> 00:03:32,800
运算符使其代替您获取对象，我可以编写实体指针
of get object I can write entity pointer

105
00:03:33,000 --> 00:03:35,800
没有这样的参数的运算符小时，然后只返回em就可以了
operator hour with no parameters like

106
00:03:36,000 --> 00:03:39,399
没有这样的参数的运算符小时，然后只返回em就可以了
this and then just return em all and you

107
00:03:39,598 --> 00:03:40,959
可以突然看到它可以编译并且可以正常运行
can see suddenly this compiles and will

108
00:03:41,158 --> 00:03:41,890
可以突然看到它可以编译并且可以正常运行
run just fine

109
00:03:42,090 --> 00:03:43,749
如果我按f5键，您可以看到它正在调用我的函数，
if I hit f5 there you go you can see

110
00:03:43,949 --> 00:03:45,099
如果我按f5键，您可以看到它正在调用我的函数，
that it's calling my function and

111
00:03:45,299 --> 00:03:46,929
如果您是Const，现在可以打个招呼，您也可以提供
printing hello now in the case of this

112
00:03:47,128 --> 00:03:49,270
如果您是Const，现在可以打个招呼，您也可以提供
being Const you could also provide a

113
00:03:49,469 --> 00:03:50,890
该运算符的星座，因此我将其复制并粘贴到该运算符中
constellation of this operator so I'll

114
00:03:51,090 --> 00:03:52,659
该运算符的星座，因此我将其复制并粘贴到该运算符中
copy and paste this have a return a

115
00:03:52,859 --> 00:03:54,730
Constanta T和Mark这样的运算符Const现在将返回一个
Constanta T and Mark the operators Const

116
00:03:54,930 --> 00:03:56,200
Constanta T和Mark这样的运算符Const现在将返回一个
like this and that will now return a

117
00:03:56,400 --> 00:03:58,420
const版本，当然我也将此功能标记为constic
Const version of this and of course I've

118
00:03:58,620 --> 00:03:59,830
const版本，当然我也将此功能标记为constic
marked this function as constic it

119
00:04:00,030 --> 00:04:01,719
不是选区，我无法调用该功能，因此该功能必须
wasn't constituency I'm not able to call

120
00:04:01,919 --> 00:04:03,550
不是选区，我无法调用该功能，因此该功能必须
that function so the function has to be

121
00:04:03,750 --> 00:04:05,769
在这里也被标记为Const，一切基本上都像
marked as Const over here as well and

122
00:04:05,968 --> 00:04:07,539
在这里也被标记为Const，一切基本上都像
everything basically works as if this

123
00:04:07,739 --> 00:04:10,838
只是一个Const指针，没有区别，但是现在当然是
was just a Const pointer like that no

124
00:04:11,038 --> 00:04:13,058
只是一个Const指针，没有区别，但是现在当然是
difference but now of course since it is

125
00:04:13,258 --> 00:04:15,338
有作用域的指针，我投票赞成删除此实际对象，这很酷。
a scoped pointer I voted the deletion of

126
00:04:15,538 --> 00:04:18,009
有作用域的指针，我投票赞成删除此实际对象，这很酷。
this actual object pretty cool stuff so

127
00:04:18,209 --> 00:04:19,389
这就是您可以重载箭头运算符以在自己的类中运行的方式
that's how you can overload the arrow

128
00:04:19,589 --> 00:04:21,610
这就是您可以重载箭头运算符以在自己的类中运行的方式
operator to function in your own classes

129
00:04:21,810 --> 00:04:23,019
它非常强大，非常有用，因为您可以看到可以开始
it's very powerful it's very useful

130
00:04:23,218 --> 00:04:24,699
它非常强大，非常有用，因为您可以看到可以开始
because you can see that you can start

131
00:04:24,899 --> 00:04:25,639
在某种程度上定义您的构造和尿液类型
to kind of define your

132
00:04:25,839 --> 00:04:27,020
在某种程度上定义您的构造和尿液类型
constructs and urine types in the

133
00:04:27,220 --> 00:04:29,150
语言和自动化的东西，它看起来像普通的代码，正是
language and automate things and it

134
00:04:29,350 --> 00:04:31,460
语言和自动化的东西，它看起来像普通的代码，正是
looks like normal code which is exactly

135
00:04:31,660 --> 00:04:33,410
我们想要的很多人会认为这有点令人困惑，因为
what we want a lot of people will argue

136
00:04:33,610 --> 00:04:35,449
我们想要的很多人会认为这有点令人困惑，因为
that that's a bit confusing because yeah

137
00:04:35,649 --> 00:04:36,860
它可能看起来像普通的代码，但是不是，我认为如果您使用它
it might look like normal code but it's

138
00:04:37,060 --> 00:04:39,110
它可能看起来像普通的代码，但是不是，我认为如果您使用它
not however I think that if you use it

139
00:04:39,310 --> 00:04:40,460
正确的，如果您对此很明智，那么这实际上非常有用
properly and if you're sensible about it

140
00:04:40,660 --> 00:04:42,379
正确的，如果您对此很明智，那么这实际上非常有用
then this this is actually really useful

141
00:04:42,579 --> 00:04:43,999
可以帮助您保持代码的真正整洁，非常有趣，我将再向您展示
and can help keep your code really clean

142
00:04:44,199 --> 00:04:45,110
可以帮助您保持代码的真正整洁，非常有趣，我将再向您展示
so fun I'm going to show you one more

143
00:04:45,310 --> 00:04:46,579
我们如何实际使用箭头运算符来获取某个偏移量的方式
way how we can actually use the arrow

144
00:04:46,779 --> 00:04:49,009
我们如何实际使用箭头运算符来获取某个偏移量的方式
operator to get the offset of a certain

145
00:04:49,209 --> 00:04:50,930
内存中的成员变量，所以这有点像我的小奖金
member variable in memory so this is

146
00:04:51,129 --> 00:04:52,520
内存中的成员变量，所以这有点像我的小奖金
kind of like a little bonus segment I

147
00:04:52,720 --> 00:04:53,990
猜中此情节，但与箭头操作符土壤有关，我将其放入
guess of this episode but it has to do

148
00:04:54,189 --> 00:04:55,790
猜中此情节，但与箭头操作符土壤有关，我将其放入
with the arrow operator soil I'll put it

149
00:04:55,990 --> 00:04:58,250
假设我们在这里有一个结构，也许叫向量3，我们
in let's just say that we have a struct

150
00:04:58,449 --> 00:05:02,389
假设我们在这里有一个结构，也许叫向量3，我们
here maybe called vector 3 and we

151
00:05:02,589 --> 00:05:03,980
基本上现在在背面前面只有3个浮点XYZ
basically just have a 3 in front of back

152
00:05:04,180 --> 00:05:05,990
基本上现在在背面前面只有3个浮点XYZ
to float X Y Z like this now suppose

153
00:05:06,189 --> 00:05:08,028
我实际上想找出这个Y变量的偏移量是多少
that I actually wanted to find out what

154
00:05:08,228 --> 00:05:10,610
我实际上想找出这个Y变量的偏移量是多少
the offset of this Y variable was in

155
00:05:10,810 --> 00:05:12,319
内存，所以我们知道这个结构是由浮动组成的，因此
memory so we know that this struct is

156
00:05:12,519 --> 00:05:14,240
内存，所以我们知道这个结构是由浮动组成的，因此
structured out of floats of course so

157
00:05:14,439 --> 00:05:16,759
它有浮点数XY，并说每个浮点数都是4个字节，所以X的偏移量将
it's got float X Y and said each float

158
00:05:16,959 --> 00:05:19,100
它有浮点数XY，并说每个浮点数都是4个字节，所以X的偏移量将
is 4 bytes so the offset of X is going

159
00:05:19,300 --> 00:05:20,600
为0，这是结构Y中的第一件事，因为它是4
to be 0 it's the first thing in the

160
00:05:20,800 --> 00:05:23,028
为0，这是结构Y中的第一件事，因为它是4
struct Y is going to be 4 because it's 4

161
00:05:23,228 --> 00:05:24,770
个字节进入结构，然后最终设置将是8个字节，但是
bytes into the struct and then finally

162
00:05:24,970 --> 00:05:27,290
个字节进入结构，然后最终设置将是8个字节，但是
set is going to be 8 bytes but what

163
00:05:27,490 --> 00:05:29,588
发生如果我突然移动这个，那么全班就可以了
happens if I suddenly move this around

164
00:05:29,788 --> 00:05:32,990
发生如果我突然移动这个，那么全班就可以了
then well the class is gonna work the

165
00:05:33,189 --> 00:05:35,629
同样的方法，但是他们要在内存中使用不同的布局，因此
same way but they're going it's going to

166
00:05:35,829 --> 00:05:37,218
同样的方法，但是他们要在内存中使用不同的布局，因此
have a different layout in memory so

167
00:05:37,418 --> 00:05:38,960
也许我想为自己写一些东西，实际上告诉我偏移量
maybe I want to write something for

168
00:05:39,160 --> 00:05:41,240
也许我想为自己写一些东西，实际上告诉我偏移量
myself that actually tells me the offset

169
00:05:41,439 --> 00:05:43,730
每个成员，我可以使用箭头来执行类似操作
of each of these members and I can do

170
00:05:43,930 --> 00:05:44,960
每个成员，我可以使用箭头来执行类似操作
something like that using the arrow

171
00:05:45,160 --> 00:05:47,028
运算符，所以我要执行的操作是访问这些变量，而不是
operator so what I want to do is kind of

172
00:05:47,228 --> 00:05:50,689
运算符，所以我要执行的操作是访问这些变量，而不是
access these variables but instead of

173
00:05:50,889 --> 00:05:52,550
从有效的内存地址（仅从0开始），因此很难解释，但是
from a valid memory address just from 0

174
00:05:52,750 --> 00:05:54,230
从有效的内存地址（仅从0开始），因此很难解释，但是
so this is kind of hard to explain but

175
00:05:54,430 --> 00:05:55,730
如果我愿意，但我会告诉你我的意思是我实际上要写0，然后
if I'll but I'll show you what I mean

176
00:05:55,930 --> 00:05:57,889
如果我愿意，但我会告诉你我的意思是我实际上要写0，然后
I'm literally going to write 0 and then

177
00:05:58,089 --> 00:06:01,430
像这样将其花费到向量3指针中，然后使用箭头访问X
cost this into a vector 3 pointer like

178
00:06:01,629 --> 00:06:06,139
像这样将其花费到向量3指针中，然后使用箭头访问X
so and then use the arrow to access X

179
00:06:06,339 --> 00:06:08,120
这会给我，这会尝试给我一些
and this is going to give me this is

180
00:06:08,319 --> 00:06:09,410
这会给我，这会尝试给我一些
going to try and give me some kind of

181
00:06:09,610 --> 00:06:11,060
一段无效的内存，但是我要做的实际上是占用内存
piece of invalid memory but what I'm

182
00:06:11,259 --> 00:06:13,160
一段无效的内存，但是我要做的实际上是占用内存
going to do is actually take the memory

183
00:06:13,360 --> 00:06:15,410
X的地址，所以我现在基本上正在获取该地址的偏移量
address of that X so now what I'm doing

184
00:06:15,610 --> 00:06:18,230
X的地址，所以我现在基本上正在获取该地址的偏移量
is basically getting the offset of that

185
00:06:18,430 --> 00:06:19,759
X，因为我从0开始，这也可以写为null
X because I'm starting at 0 this could

186
00:06:19,959 --> 00:06:21,259
X，因为我从0开始，这也可以写为null
also be written as null pointed by the

187
00:06:21,459 --> 00:06:23,270
的方式，如果我最终接受它，只是将其花费为一个常规整数，然后写
way and if I finally take that and just

188
00:06:23,470 --> 00:06:25,730
的方式，如果我最终接受它，只是将其花费为一个常规整数，然后写
cost it to a regular integer and write

189
00:06:25,930 --> 00:06:30,030
在这里偏移我会打印
offset over here I'll print that

190
00:06:32,470 --> 00:06:36,088
我会加五，你会看到它给了我零，所以我接下来要做的是
and I'll hit up five you can see it

191
00:06:36,288 --> 00:06:38,338
我会加五，你会看到它给了我零，所以我接下来要做的是
gives me zero so what I'll do next is

192
00:06:38,538 --> 00:06:40,319
我将其更改为Y并查看看起来像什么
I'll change this to be Y and check out

193
00:06:40,519 --> 00:06:42,209
我将其更改为Y并查看看起来像什么
what that looks like for that seems

194
00:06:42,408 --> 00:06:43,829
正确，然后将其更改为Zed，当然值应该为8，
right and then I'll change it to Zed and

195
00:06:44,029 --> 00:06:45,329
正确，然后将其更改为Zed，当然值应该为8，
of course the value should be eight and

196
00:06:45,529 --> 00:06:47,249
您可以看到它就是这样，我们在这里所做的就是使用箭头
you can see that it is so what we've

197
00:06:47,449 --> 00:06:48,360
您可以看到它就是这样，我们在这里所做的就是使用箭头
done here is we've used the arrow

198
00:06:48,560 --> 00:06:50,850
运算符基本上可以获取内存中某个值的偏移量
operator to basically get the offset of

199
00:06:51,050 --> 00:06:52,499
运算符基本上可以获取内存中某个值的偏移量
a certain value in memory

200
00:06:52,699 --> 00:06:54,088
很酷的东西，这对于序列化时非常有用
pretty cool stuff and this is actually

201
00:06:54,288 --> 00:06:55,709
很酷的东西，这对于序列化时非常有用
very useful for when you're serializing

202
00:06:55,908 --> 00:06:57,420
数据变成字节流，您想找出某些偏移量
data into like a stream of bytes and you

203
00:06:57,620 --> 00:06:59,009
数据变成字节流，您想找出某些偏移量
want to figure out offsets of certain

204
00:06:59,209 --> 00:07:01,079
事情，当我们开始时，我们会进入这种令人兴奋的代码
things and we'll kind of get into this

205
00:07:01,279 --> 00:07:03,059
事情，当我们开始时，我们会进入这种令人兴奋的代码
kind of exciting code when we start

206
00:07:03,259 --> 00:07:04,709
做图形程序设计系列和游戏引擎系列等等
doing the graphics programming series

207
00:07:04,908 --> 00:07:06,240
做图形程序设计系列和游戏引擎系列等等
and the game engine series and all that

208
00:07:06,439 --> 00:07:07,679
因为无论如何我都会一直在处理字节流
because we'll be kind of dealing with

209
00:07:07,879 --> 00:07:09,899
因为无论如何我都会一直在处理字节流
streams of bytes all the time anyway I

210
00:07:10,098 --> 00:07:11,189
希望你们喜欢这个情节，如果您按了赞按钮，
hope you guys enjoyed this episode if

211
00:07:11,389 --> 00:07:12,929
希望你们喜欢这个情节，如果您按了赞按钮，
you did and you hit the like button let

212
00:07:13,129 --> 00:07:14,519
我知道您喜欢它，可能会留下任何评论或反馈
me know that you enjoyed it leave any

213
00:07:14,718 --> 00:07:16,050
我知道您喜欢它，可能会留下任何评论或反馈
comments or feedback you might have in

214
00:07:16,250 --> 00:07:17,249
下面的评论部分，如果您真的喜欢这个视频中的这个系列
the comment section below and if you

215
00:07:17,449 --> 00:07:19,230
下面的评论部分，如果您真的喜欢这个视频中的这个系列
really enjoyed this series in this video

216
00:07:19,430 --> 00:07:20,759
那么您可以通过在patreon图标上搜索来支持我在patreon上
then you can support me on patreon by

217
00:07:20,959 --> 00:07:22,050
那么您可以通过在patreon图标上搜索来支持我在patreon上
going to patreon icon for search the

218
00:07:22,250 --> 00:07:23,939
切尔诺（Cherno），您会及早获得视频，并可以进入私人不和谐频道
Cherno you'll get videos early you'll be

219
00:07:24,139 --> 00:07:26,189
切尔诺（Cherno），您会及早获得视频，并可以进入私人不和谐频道
able to be in a private discord channel

220
00:07:26,389 --> 00:07:27,629
我们讨论这些视频的内容以及所有有趣的内容以及
where we talk about what goes into these

221
00:07:27,829 --> 00:07:29,249
我们讨论这些视频的内容以及所有有趣的内容以及
videos and all that fun stuff and of

222
00:07:29,449 --> 00:07:30,149
当然，他们正在帮助支持该系列影片，并确保我们能提供更多
course they're helping to support the

223
00:07:30,348 --> 00:07:31,769
当然，他们正在帮助支持该系列影片，并确保我们能提供更多
series and making sure that we make more

224
00:07:31,968 --> 00:07:34,230
在这些情节中，我会在下一个节目中看到你们
of these episodes I will see you guys in

225
00:07:34,430 --> 00:07:34,740
在这些情节中，我会在下一个节目中看到你们
the next one

226
00:07:34,939 --> 00:07:36,540
再见[音乐]
good bye

227
00:07:36,740 --> 00:07:45,079
再见[音乐]
[Music]

228
00:07:48,720 --> 00:07:53,720
[音乐]
[Music]

