﻿1
00:00:01,060 --> 00:02:00,599
[音乐]嘿，小伙子，我叫詹纳
[Music]

2
00:02:00,799 --> 00:02:05,969
[音乐]嘿，小伙子，我叫詹纳
hey little guys my name is a Jenner this

3
00:02:06,170 --> 00:02:09,939
嗯，太好了，我的意思是拿铁艺术可以花点功夫，但真的很受欢迎
mmm so good I mean latte art could use a

4
00:02:10,139 --> 00:02:13,390
嗯，太好了，我的意思是拿铁艺术可以花点功夫，但真的很受欢迎
bit of work but really hits the spot

5
00:02:13,590 --> 00:02:15,430
欢迎回到我的安全损失损失系列，这与C ++有关
welcome back to my safe loss loss series

6
00:02:15,629 --> 00:02:19,030
欢迎回到我的安全损失损失系列，这与C ++有关
kind of this is kind of a C++ related

7
00:02:19,229 --> 00:02:21,100
视频，但它也是一种非常普通的视频，非常非常有见地
video but it's also a very kind of

8
00:02:21,300 --> 00:02:22,870
视频，但它也是一种非常普通的视频，非常非常有见地
general video and it's very very opinion

9
00:02:23,069 --> 00:02:24,610
基于，实际上我们只是在谈论为什么我个人不
based and really we're just maybe

10
00:02:24,810 --> 00:02:26,380
基于，实际上我们只是在谈论为什么我个人不
talking about why I personally don't

11
00:02:26,580 --> 00:02:29,500
现在使用命名空间STD，这是一个我想说的问题
using namespace STD now this is a

12
00:02:29,699 --> 00:02:32,740
现在使用命名空间STD，这是一个我想说的问题
question that I get asked I want to say

13
00:02:32,939 --> 00:02:36,550
几乎每天都会有人问我，当我滚动浏览我的文字时，
almost daily like people ask me that I

14
00:02:36,750 --> 00:02:37,990
几乎每天都会有人问我，当我滚动浏览我的文字时，
literally as I scroll through my

15
00:02:38,189 --> 00:02:40,840
某视频上某人当天的评论总是问嘿，你知道你
comments for the day someone on some

16
00:02:41,039 --> 00:02:43,330
某视频上某人当天的评论总是问嘿，你知道你
video always asks hey you know you

17
00:02:43,530 --> 00:02:45,520
不能只使用名​​称空间STD，然后您必须输入城市
couldn't just using namespace STD and

18
00:02:45,719 --> 00:02:46,930
不能只使用名​​称空间STD，然后您必须输入城市
then you're gonna have to type in a city

19
00:02:47,129 --> 00:02:50,259
每当是的时候，我确实知道我确实知道这一点，但我不是故意这样做的，
every time yes I do know that I do know

20
00:02:50,459 --> 00:02:53,650
每当是的时候，我确实知道我确实知道这一点，但我不是故意这样做的，
that but I'm not doing it on purpose and

21
00:02:53,849 --> 00:02:55,450
该视频将谈论为什么我不这样做以及为什么你可能
this video is going to be talking about

22
00:02:55,650 --> 00:02:58,360
该视频将谈论为什么我不这样做以及为什么你可能
why I don't do it and why you probably

23
00:02:58,560 --> 00:03:01,689
不应该这样做，或者也许您应该这样做，这绝对是一种意见
shouldn't do it or maybe you should do

24
00:03:01,889 --> 00:03:04,120
不应该这样做，或者也许您应该这样做，这绝对是一种意见
it it's kind of an opinion definitely

25
00:03:04,319 --> 00:03:06,099
在此视频末尾的以下评论中留下您的想法，但让我们
leave your thoughts in the comments

26
00:03:06,299 --> 00:03:08,740
在此视频末尾的以下评论中留下您的想法，但让我们
below at the end of this video but let's

27
00:03:08,939 --> 00:03:10,390
马上进入，实际上首先要讨论使用什么
get right on into this and actually

28
00:03:10,590 --> 00:03:12,039
马上进入，实际上首先要讨论使用什么
first of all talk about what using

29
00:03:12,239 --> 00:03:14,740
实际上，命名空间STD实际上会执行使用命名空间的操作，所以如果我们只是
namespace STD actually does in fact what

30
00:03:14,939 --> 00:03:16,960
实际上，命名空间STD实际上会执行使用命名空间的操作，所以如果我们只是
a using namespace is okay so if we just

31
00:03:17,159 --> 00:03:18,219
打开您在C ++系列上一集中的代码
open up the code that was in the

32
00:03:18,419 --> 00:03:21,280
打开您在C ++系列上一集中的代码
previous episode of the C++ series you

33
00:03:21,479 --> 00:03:23,080
可以看到我在这里有很多实例
can see that I there are a lot of

34
00:03:23,280 --> 00:03:24,910
可以看到我在这里有很多实例
instances here where I use things from

35
00:03:25,110 --> 00:03:26,620
标准库将类似于STD向量
the standard library will that be

36
00:03:26,819 --> 00:03:28,390
标准库将类似于STD向量
something like STD vector

37
00:03:28,590 --> 00:03:32,140
CDC在这里STD CN点YES STD功能有很多东西
CDC out here STD CN dot yeah STD

38
00:03:32,340 --> 00:03:33,580
CDC在这里STD CN点YES STD功能有很多东西
function there's a bunch of things that

39
00:03:33,780 --> 00:03:36,250
我现在使用的是标准库，请注意，每次使用
I'm using from the standard library now

40
00:03:36,449 --> 00:03:37,930
我现在使用的是标准库，请注意，每次使用
notice that every time I use something

41
00:03:38,129 --> 00:03:39,640
来自标准库，例如我最喜欢的函数
from the standard library like this

42
00:03:39,840 --> 00:03:41,680
来自标准库，例如我最喜欢的函数
fondest function I need to actually type

43
00:03:41,879 --> 00:03:45,009
Xtd冒号冒号在我实际键入该功能之前，但是如果我
xtd colon colon out the front before I

44
00:03:45,209 --> 00:03:47,500
Xtd冒号冒号在我实际键入该功能之前，但是如果我
actually type that function however if I

45
00:03:47,699 --> 00:03:50,080
只需使用命名空间STD作为示例添加到此文件的顶部，就像
just add onto the top of this file as an

46
00:03:50,280 --> 00:03:52,990
只需使用命名空间STD作为示例添加到此文件的顶部，就像
example using namespace STD just like

47
00:03:53,189 --> 00:03:55,270
那我实际上就不必做，我可以通过所有这些
that then I don't actually have to do

48
00:03:55,469 --> 00:03:57,039
那我实际上就不必做，我可以通过所有这些
that I can just go through all of this

49
00:03:57,239 --> 00:03:59,980
并删除所有的性病，事实上我只是要做一个很好的替换--冒号
and remove all the STDs in fact I'm just

50
00:04:00,180 --> 00:04:02,379
并删除所有的性病，事实上我只是要做一个很好的替换--冒号
going to do a fine replace as - colon

51
00:04:02,579 --> 00:04:04,060
冒号，该文档中没有任何内容，您可以看到我可以
colon with nothing in this document and

52
00:04:04,259 --> 00:04:06,160
冒号，该文档中没有任何内容，您可以看到我可以
there you go you can see that I can just

53
00:04:06,360 --> 00:04:08,230
像这样编写我的代码，如果我对其进行编译，它将可以正常工作，因此当
write my code like that and if I compile

54
00:04:08,430 --> 00:04:11,500
像这样编写我的代码，如果我对其进行编译，它将可以正常工作，因此当
it it will work and so immediately when

55
00:04:11,699 --> 00:04:13,360
人们看到这就像是啊，但是是的，代码看起来更干净了
people see this they're like ah but

56
00:04:13,560 --> 00:04:15,250
人们看到这就像是啊，但是是的，代码看起来更干净了
yeah the code looks so much cleaner I'm

57
00:04:15,449 --> 00:04:16,810
可以在任何地方使用它，我的意思是您不必在这里使用它，您可以
gonna use this everywhere and I mean you

58
00:04:17,009 --> 00:04:19,750
可以在任何地方使用它，我的意思是您不必在这里使用它，您可以
don't have to use this up here you can

59
00:04:19,949 --> 00:04:21,699
实际上只是在函数内部局部声明，就像在这个主函数中一样
actually just declare it locally inside

60
00:04:21,899 --> 00:04:24,520
实际上只是在函数内部局部声明，就像在这个主函数中一样
a function so like in this main function

61
00:04:24,720 --> 00:04:25,750
我可以在这里声明它，这意味着它仅适用于此范围，您
I can just declare it here which means

62
00:04:25,949 --> 00:04:28,300
我可以在这里声明它，这意味着它仅适用于此范围，您
it just applies to this scope and you

63
00:04:28,500 --> 00:04:30,699
真的可以在任何范围内做到这一点，然后我仍然必须像这样使用sta
really can do it in any scope and then I

64
00:04:30,899 --> 00:04:33,069
真的可以在任何范围内做到这一点，然后我仍然必须像这样使用sta
would still have to use sta like that

65
00:04:33,269 --> 00:04:34,750
在这里，所以您可以将它限制在不同的裙子上，我的意思是，使用
over here so you can confine it to

66
00:04:34,949 --> 00:04:36,430
在这里，所以您可以将它限制在不同的裙子上，我的意思是，使用
different skirts and I mean yeah using

67
00:04:36,629 --> 00:04:38,829
如果您要处理很长的时间，名称空间可能会非常有用
namespace can be incredibly useful if

68
00:04:39,029 --> 00:04:40,270
如果您要处理很长的时间，名称空间可能会非常有用
you're dealing with really long

69
00:04:40,470 --> 00:04:41,740
名称空间，或者您已经为自己声明了自己的名称空间
namespaces or you've got your own

70
00:04:41,939 --> 00:04:43,449
名称空间，或者您已经为自己声明了自己的名称空间
namespaces that you've declared for your

71
00:04:43,649 --> 00:04:45,129
自己的文件，就像您自己的项目一样，其中的所有符号
own files that are in like your own

72
00:04:45,329 --> 00:04:47,110
自己的文件，就像您自己的项目一样，其中的所有符号
projects and all the symbols in that

73
00:04:47,310 --> 00:04:49,689
然后是很长的时间，在某些功能中您可能会发现
then yes that can get very long and in

74
00:04:49,889 --> 00:04:51,250
然后是很长的时间，在某些功能中您可能会发现
certain functions you might find

75
00:04:51,449 --> 00:04:52,900
自己调用符号或访问这些命名空间中的符号
yourself calling symbols or accessing

76
00:04:53,100 --> 00:04:54,520
自己调用符号或访问这些命名空间中的符号
symbols that are in those namespaces

77
00:04:54,720 --> 00:04:56,710
很多时候，所以您可能只想在那里使用命名空间来偷偷摸摸
very often and so you might want to just

78
00:04:56,910 --> 00:04:58,240
很多时候，所以您可能只想在那里使用命名空间来偷偷摸摸
sneak in a little using namespace there

79
00:04:58,439 --> 00:05:00,939
完全没问题，但是我特别不喜欢使用命名空间STD，让我们
that's totally fine but specifically I

80
00:05:01,139 --> 00:05:04,480
完全没问题，但是我特别不喜欢使用命名空间STD，让我们
don't like using namespace STD and let's

81
00:05:04,680 --> 00:05:06,069
看一看为什么，所以首先要遍历这段代码，
take a look at why so first of all

82
00:05:06,269 --> 00:05:08,079
看一看为什么，所以首先要遍历这段代码，
looking through this code again a lot of

83
00:05:08,279 --> 00:05:10,420
人们会说是的，这确实看起来更清洁，我实际上反对
people would argue that yes this does

84
00:05:10,620 --> 00:05:13,900
人们会说是的，这确实看起来更清洁，我实际上反对
look cleaner I actually argue against

85
00:05:14,100 --> 00:05:17,110
那是因为我们可能已经注意到这部影片的开头是
that because we may have noticed at the

86
00:05:17,310 --> 00:05:19,509
那是因为我们可能已经注意到这部影片的开头是
beginning of this video was when I

87
00:05:19,709 --> 00:05:22,600
实际上这里有原始代码，这对我来说非常容易
actually had this original code here it

88
00:05:22,800 --> 00:05:25,120
实际上这里有原始代码，这对我来说非常容易
was incredibly easy for me to point out

89
00:05:25,319 --> 00:05:27,939
我在C ++的标准模板库中使用的库
what I was using from the standard

90
00:05:28,139 --> 00:05:30,490
我在C ++的标准模板库中使用的库
template library from C++ as library

91
00:05:30,689 --> 00:05:32,620
没错，我没有必要三思而后行使用此STD前缀
right I didn't have to really think

92
00:05:32,819 --> 00:05:36,338
没错，我没有必要三思而后行使用此STD前缀
twice anything that had this STD prefix

93
00:05:36,538 --> 00:05:39,759
现在它来自标准库，但是如果我很好地使用命名空间STD，
to it was from the standard library now

94
00:05:39,959 --> 00:05:42,490
现在它来自标准库，但是如果我很好地使用命名空间STD，
though if I use namespace STD well it's

95
00:05:42,689 --> 00:05:44,139
我很难说是不是，我的意思是您只能通过
a little bit difficult for me to tell

96
00:05:44,339 --> 00:05:47,079
我很难说是不是，我的意思是您只能通过
isn't it I mean you can tell just by the

97
00:05:47,279 --> 00:05:49,900
命名约定好，如果不是写得不好，我的意思是我显然想
naming convention fine if is not written

98
00:05:50,100 --> 00:05:52,120
命名约定好，如果不是写得不好，我的意思是我显然想
like a bad I mean clearly I like to

99
00:05:52,319 --> 00:05:54,129
像这样写我的函数，并在Pascal情况下学习C ++喜欢这条蛇
write my functions like this and kind of

100
00:05:54,329 --> 00:05:56,860
像这样写我的函数，并在Pascal情况下学习C ++喜欢这条蛇
Pascal case learn C++ likes this snake

101
00:05:57,060 --> 00:05:59,170
案例格式，因此您仍然可以通过这种方式判断，但是当然
case kind of format so you can still

102
00:05:59,370 --> 00:06:01,480
案例格式，因此您仍然可以通过这种方式判断，但是当然
tell via that but of course if this for

103
00:06:01,680 --> 00:06:03,699
每个人都被这样写在蛇壳里，然后突然
each was written like that

104
00:06:03,899 --> 00:06:07,000
每个人都被这样写在蛇壳里，然后突然
right in a snake case well then suddenly

105
00:06:07,199 --> 00:06:09,278
很容易看出它在我的示例文件中，但是突然发现是否
it's easy to tell that it's in this file

106
00:06:09,478 --> 00:06:12,069
很容易看出它在我的示例文件中，但是突然发现是否
of my example but suddenly find if is

107
00:06:12,269 --> 00:06:15,160
如果每个STD都存在于STD中，则表示STD正常
that is that STDs fine if for each is

108
00:06:15,360 --> 00:06:17,500
如果每个STD都存在于STD中，则表示STD正常
that the one that's in STD is there one

109
00:06:17,699 --> 00:06:21,819
性病我不知道读这段代码对我来说变得越来越难了
STD I don't know reading this code

110
00:06:22,019 --> 00:06:24,550
性病我不知道读这段代码对我来说变得越来越难了
suddenly has gotten harder for me

111
00:06:24,750 --> 00:06:27,009
但是以前如果我只用这个做蛇案，对我来说很明显
whereas before if I just had this for

112
00:06:27,209 --> 00:06:29,860
但是以前如果我只用这个做蛇案，对我来说很明显
snake case it's pretty obvious to me

113
00:06:30,060 --> 00:06:31,809
该4小时不属于标准库，因为显然
that 4-h is not part of the standard

114
00:06:32,009 --> 00:06:33,699
该4小时不属于标准库，因为显然
library because clearly everything that

115
00:06:33,899 --> 00:06:37,240
有STD ::前缀，我的意思是您可能会发布它wave
is has that STD : : prefix and I mean

116
00:06:37,439 --> 00:06:39,639
有STD ::前缀，我的意思是您可能会发布它wave
you might probably publishing it wave

117
00:06:39,839 --> 00:06:41,980
我的意思是，您可能不认为这是您所需要的
this around and I mean you you might not

118
00:06:42,180 --> 00:06:43,749
我的意思是，您可能不认为这是您所需要的
think that this is something that you

119
00:06:43,949 --> 00:06:45,520
可能会碰到很多时候，我的意思是我会多久写一次向量
might run into often I mean how often I

120
00:06:45,720 --> 00:06:47,710
可能会碰到很多时候，我的意思是我会多久写一次向量
going to write something called vector

121
00:06:47,910 --> 00:06:50,259
还是所谓的“查找”（Find），如果我的意思是通过看起来像什么的写
or something called find if I mean

122
00:06:50,459 --> 00:06:52,389
还是所谓的“查找”（Find），如果我的意思是通过看起来像什么的写
through writes that see out like what

123
00:06:52,589 --> 00:06:53,860
函数名称就是这样，我真的要写好了，让我
kind of function name is that come on am

124
00:06:54,060 --> 00:06:55,480
函数名称就是这样，我真的要写好了，让我
I really going to write that well let me

125
00:06:55,680 --> 00:06:57,460
向您展示一个真实的例子，我正在研究核心技术冻伤的助教
show you a real world example I work on

126
00:06:57,660 --> 00:06:59,230
向您展示一个真实的例子，我正在研究核心技术冻伤的助教
core technology a TA in the Frostbite

127
00:06:59,430 --> 00:07:01,028
组织，我们使用称为EA STL的东西，它实际上是在
organization and we use something called

128
00:07:01,228 --> 00:07:03,639
组织，我们使用称为EA STL的东西，它实际上是在
EA STL which is actually open source on

129
00:07:03,839 --> 00:07:05,410
github现在这基本上是标准模板的替代品
github now this is basically a

130
00:07:05,610 --> 00:07:06,939
github现在这基本上是标准模板的替代品
replacement for the standard template

131
00:07:07,139 --> 00:07:08,559
C ++附带的库，称为EA标准模板库
library that comes with C++ and is

132
00:07:08,759 --> 00:07:09,999
C ++附带的库，称为EA标准模板库
called the EA standard template library

133
00:07:10,199 --> 00:07:11,559
如果需要的话，您可以看一下，因为这有点
and you can take a look at this if you

134
00:07:11,759 --> 00:07:13,210
如果需要的话，您可以看一下，因为这有点
want to and since it is a bit of a

135
00:07:13,410 --> 00:07:15,100
替换API，所有内容实际上都意味着要符合标准
replacement the API and everything is

136
00:07:15,300 --> 00:07:17,710
替换API，所有内容实际上都意味着要符合标准
really meant to match what the standard

137
00:07:17,910 --> 00:07:19,660
如果以矢量为例，模板库看起来很抱歉
template library looks like sorry if we

138
00:07:19,860 --> 00:07:22,028
如果以矢量为例，模板库看起来很抱歉
take vector as an example we have this

139
00:07:22,228 --> 00:07:24,160
向量点H文件，如果向下滚动，您可以看到它在EA STL中
vector dot H file and if we scroll down

140
00:07:24,360 --> 00:07:26,110
向量点H文件，如果向下滚动，您可以看到它在EA STL中
you can see it is in the EA STL

141
00:07:26,310 --> 00:07:29,020
名称空间，但是如果我们继续向下滚动，则类名是相同的
namespace however the class name is the

142
00:07:29,220 --> 00:07:30,819
名称空间，但是如果我们继续向下滚动，则类名是相同的
same if we keep scrolling down here you

143
00:07:31,019 --> 00:07:34,088
可以看到我们有类向量，所以如果我在EA编写代码
can see that we have class vector so

144
00:07:34,288 --> 00:07:37,230
可以看到我们有类向量，所以如果我在EA编写代码
then if I'm kind of writing code at EA

145
00:07:37,430 --> 00:07:40,209
我如何知道我是否只用EA编写了矢量
how do I know if find me if I just have

146
00:07:40,408 --> 00:07:42,670
我如何知道我是否只用EA编写了矢量
vector written like that am I using EA

147
00:07:42,870 --> 00:07:46,870
我使用STD向量的STL向量我不知道它最初不是很明显
STL vector am i using STD vector I don't

148
00:07:47,069 --> 00:07:49,838
我使用STD向量的STL向量我不知道它最初不是很明显
know it's not initially very apparent to

149
00:07:50,038 --> 00:07:53,519
我，但我很高兴看到我实际使用的是什么，如果
me but it would be nice for me to see

150
00:07:53,718 --> 00:07:57,278
我，但我很高兴看到我实际使用的是什么，如果
what I'm actually using which is why if

151
00:07:57,478 --> 00:07:58,629
我有使用命名空间，我可能不得不寻找它
I had that using namespace

152
00:07:58,829 --> 00:08:00,309
我有使用命名空间，我可能不得不寻找它
I'd have to probably look for it

153
00:08:00,509 --> 00:08:02,079
在我的代码中的某个地方真正找到它并在我在
somewhere throughout my code to actually

154
00:08:02,279 --> 00:08:06,040
在我的代码中的某个地方真正找到它并在我在
find it and crk right i had somewhere at

155
00:08:06,240 --> 00:08:07,360
该文件的开头使用名称空间STD，所以我想这是从
the beginning of this file are using

156
00:08:07,560 --> 00:08:09,370
该文件的开头使用名称空间STD，所以我想这是从
namespace STD so i guess this is from

157
00:08:09,569 --> 00:08:13,059
最重要的是STD命名空间，如果有人使用
the STD namespace right on top of that

158
00:08:13,259 --> 00:08:15,009
最重要的是STD命名空间，如果有人使用
what if someone puts a little using

159
00:08:15,209 --> 00:08:17,829
某个地方的命名空间EA STL突然之间出现了编译器错误，因为
namespace EA STL somewhere well suddenly

160
00:08:18,029 --> 00:08:20,019
某个地方的命名空间EA STL突然之间出现了编译器错误，因为
we're getting a compiler error because

161
00:08:20,218 --> 00:08:22,028
这是编译器不知道的模糊符号，我们是指STD吗？
that's an ambiguous symbol the compiler

162
00:08:22,228 --> 00:08:23,680
这是编译器不知道的模糊符号，我们是指STD吗？
doesn't know are we referring to the STD

163
00:08:23,879 --> 00:08:26,230
向量，或者我们是指一个小时的人使用ei STL向量来制作
vector or we've are we referring to ei

164
00:08:26,430 --> 00:08:29,528
向量，或者我们是指一个小时的人使用ei STL向量来制作
STL vector on an hour man to make

165
00:08:29,728 --> 00:08:31,389
问题甚至更糟，如果我们回到过去，这只是拥有一个
matters even worse and if we kind of go

166
00:08:31,589 --> 00:08:33,849
问题甚至更糟，如果我们回到过去，这只是拥有一个
back to is just the example of having a

167
00:08:34,049 --> 00:08:35,349
简单的图书馆，我们只说我们有一个
simple library let's just say that we

168
00:08:35,549 --> 00:08:36,189
简单的图书馆，我们只说我们有一个
have an

169
00:08:36,389 --> 00:08:38,468
这里有一个叫做Apple的空间，我们有一个叫做print的功能，我不知道
space called Apple here and we have a

170
00:08:38,668 --> 00:08:41,679
这里有一个叫做Apple的空间，我们有一个叫做print的功能，我不知道
function called print which I don't know

171
00:08:41,879 --> 00:08:43,448
假设它接受一个字符串，然后输入Const STD字符串文本，然后我们看到
let's just say it takes in a string so

172
00:08:43,649 --> 00:08:47,859
假设它接受一个字符串，然后输入Const STD字符串文本，然后我们看到
Const STD string text and then we see

173
00:08:48,059 --> 00:08:50,529
如果我向下滚动到这里，对我来说这似乎很简单
out this text right this seems pretty

174
00:08:50,730 --> 00:08:52,508
如果我向下滚动到这里，对我来说这似乎很简单
simple to me if I scroll down over here

175
00:08:52,708 --> 00:08:54,818
并留出一些空间，我将在此处包括字符串，以便此代码
and make some space I'm just going to

176
00:08:55,019 --> 00:08:56,559
并留出一些空间，我将在此处包括字符串，以便此代码
include string up here so that this code

177
00:08:56,759 --> 00:08:58,120
实际编译，我可以告诉你我的意思，所以我们有一个完美的
actually compiles and I can show you

178
00:08:58,320 --> 00:09:02,019
实际编译，我可以告诉你我的意思，所以我们有一个完美的
what I mean so there we have a perfectly

179
00:09:02,220 --> 00:09:04,120
如果我不使用命名空间就调用它的话，这里有合理的功能
reasonable function over here if I was

180
00:09:04,320 --> 00:09:06,008
如果我不使用命名空间就调用它的话，这里有合理的功能
to call it without using namespace I

181
00:09:06,208 --> 00:09:07,328
不得不调用print让我们实际将其小写以使其看起来
would have to call print let's actually

182
00:09:07,528 --> 00:09:08,799
不得不调用print让我们实际将其小写以使其看起来
make this lowercase so that it looks

183
00:09:09,000 --> 00:09:11,709
更像这里的标准库，所以如果我有这样的苹果打印，我
more like the standard library here so

184
00:09:11,909 --> 00:09:14,198
更像这里的标准库，所以如果我有这样的苹果打印，我
if I have Apple print like that and I

185
00:09:14,399 --> 00:09:16,000
不知道为什么它消失了，反正变得很糟糕，你好，我们去了
don't know why it's gone made that bad

186
00:09:16,200 --> 00:09:17,889
不知道为什么它消失了，反正变得很糟糕，你好，我们去了
bit anyway hello there we go I'm

187
00:09:18,089 --> 00:09:19,870
打印一些相当合理的东西，让我们说我决定
printing something that's pretty

188
00:09:20,070 --> 00:09:21,578
打印一些相当合理的东西，让我们说我决定
reasonable let's just say that I decide

189
00:09:21,778 --> 00:09:23,318
在此处粘贴使用中的名称空间，这样我就不必做类似的事情
to stick a using namespace here so that

190
00:09:23,519 --> 00:09:24,909
在此处粘贴使用中的名称空间，这样我就不必做类似的事情
I don't have to do something like that

191
00:09:25,110 --> 00:09:27,429
这突然变成了打印你好，像这样，现在我们已经介绍了
this suddenly becomes print hello like

192
00:09:27,629 --> 00:09:30,698
这突然变成了打印你好，像这样，现在我们已经介绍了
this now let's say that we've introduced

193
00:09:30,899 --> 00:09:32,740
另一个库，它被称为命名空间Orange right，所以它在
another library and this is called

194
00:09:32,940 --> 00:09:35,378
另一个库，它被称为命名空间Orange right，所以它在
namespace Orange right so it's in

195
00:09:35,578 --> 00:09:37,059
不同的图书馆，橙色图书馆，它也有打印功能，但
different libraries the orange library

196
00:09:37,259 --> 00:09:39,609
不同的图书馆，橙色图书馆，它也有打印功能，但
and it also has a print function but

197
00:09:39,809 --> 00:09:42,039
这需要一个名为text的音乐会指针，现在让我们说
this one takes in a concert pointer

198
00:09:42,240 --> 00:09:45,068
这需要一个名为text的音乐会指针，现在让我们说
called text now let's just say that this

199
00:09:45,269 --> 00:09:46,899
Orange做了一些不同的工作，而不仅仅是打印了这个
Orange does something a little bit

200
00:09:47,100 --> 00:09:48,729
Orange做了一些不同的工作，而不仅仅是打印了这个
different instead of just printing this

201
00:09:48,929 --> 00:09:50,979
通常，这实际上是将其分配给一个字符串，然后
normally what this actually does is

202
00:09:51,179 --> 00:09:54,929
通常，这实际上是将其分配给一个字符串，然后
assigns it into a string and then

203
00:09:55,129 --> 00:10:00,878
反转字符串，然后打印它，因此它实际上是在打印文本
reverses the string and then prints it

204
00:10:01,078 --> 00:10:02,740
反转字符串，然后打印它，因此它实际上是在打印文本
so it's essentially printing out text

205
00:10:02,940 --> 00:10:04,748
但在Reverse中，这有点恶意功能，现在，此打印是真实的
but in Reverse it's a bit of a malicious

206
00:10:04,948 --> 00:10:07,748
但在Reverse中，这有点恶意功能，现在，此打印是真实的
function now this print here's the real

207
00:10:07,948 --> 00:10:10,568
问题，我们是否使用名称空间Apple并且我们也使用名称空间Orange
question if we use a namespace Apple and

208
00:10:10,769 --> 00:10:13,508
问题，我们是否使用名称空间Apple并且我们也使用名称空间Orange
we use namespace Orange as well then

209
00:10:13,708 --> 00:10:17,429
在我们真正开始之前，我之前哪些会好感冒
which ones can I get cold well

210
00:10:17,629 --> 00:10:20,409
在我们真正开始之前，我之前哪些会好感冒
previously before we actually started

211
00:10:20,610 --> 00:10:23,019
使用这个橙色的图书馆，我们有了苹果，我的意思是那很漂亮
using this orange library and we just

212
00:10:23,220 --> 00:10:24,609
使用这个橙色的图书馆，我们有了苹果，我的意思是那很漂亮
had Apple well I mean it was pretty

213
00:10:24,809 --> 00:10:26,498
显而易见，哪一个会变冷，这是这里的一个，如果我们运行程序
obvious which one gets cold it's this

214
00:10:26,698 --> 00:10:28,089
显而易见，哪一个会变冷，这是这里的一个，如果我们运行程序
one over here and if we run our program

215
00:10:28,289 --> 00:10:31,179
让我在这里下去，给豆腐剩下的零钱，除了相同的东西
let me just go down here and give 0 the

216
00:10:31,379 --> 00:10:32,649
让我在这里下去，给豆腐剩下的零钱，除了相同的东西
rest of this curd except for the same

217
00:10:32,850 --> 00:10:34,748
桶，因为我不想运行它，让我们按f5好吧，您好，打印
bucket because I don't want that to run

218
00:10:34,948 --> 00:10:37,179
桶，因为我不想运行它，让我们按f5好吧，您好，打印
let's hit f5 okay hello gets printed

219
00:10:37,379 --> 00:10:39,578
非常简单，我们的函数有效，但是现在我们引入了这个橙色库
pretty simple our function works but now

220
00:10:39,778 --> 00:10:42,849
非常简单，我们的函数有效，但是现在我们引入了这个橙色库
we've introduced this orange library

221
00:10:43,049 --> 00:10:45,248
让我们继续评论一下，这看起来很简单是哪一个
let's go ahead and comment that out this

222
00:10:45,448 --> 00:10:47,139
让我们继续评论一下，这看起来很简单是哪一个
seems pretty simple which one is it

223
00:10:47,339 --> 00:10:49,498
现在要打电话给我们，按f5哦，好吧，你看看它是打印出来的文字
gonna call now let's hit f5 oh well

224
00:10:49,698 --> 00:10:52,279
现在要打电话给我们，按f5哦，好吧，你看看它是打印出来的文字
you look at that it's printed out text

225
00:10:52,480 --> 00:10:56,848
如果我们回顾一下代码，为什么你做得很好？
backwoods why is it done that well if we

226
00:10:57,048 --> 00:10:59,818
如果我们回顾一下代码，为什么你做得很好？
look back to the code this hello is

227
00:11:00,019 --> 00:11:03,328
实际上是Const我们的数组，如果没有的话，实际上不是字符串
actually a Const our array right it's

228
00:11:03,528 --> 00:11:06,298
实际上是Const我们的数组，如果没有的话，实际上不是字符串
not actually a string if we don't have

229
00:11:06,499 --> 00:11:08,399
然后，这橙色的东西当然可以真正地被称为
this orange thing then of course what

230
00:11:08,600 --> 00:11:10,498
然后，这橙色的东西当然可以真正地被称为
this can actually do is something called

231
00:11:10,698 --> 00:11:12,658
如果你们不知道我是什么隐式转换
an implicit conversion if you guys don't

232
00:11:12,859 --> 00:11:14,038
如果你们不知道我是什么隐式转换
know what an implicit conversion is I've

233
00:11:14,239 --> 00:11:15,269
制作了一个视频，链接在那里，所以一定要检查一下
made a video on that which is linked up

234
00:11:15,470 --> 00:11:16,649
制作了一个视频，链接在那里，所以一定要检查一下
there so definitely check that out

235
00:11:16,850 --> 00:11:18,358
但这会执行称为隐式转换的操作，因为我们可以
but this performs something called an

236
00:11:18,558 --> 00:11:19,889
但这会执行称为隐式转换的操作，因为我们可以
implicit conversion because we can

237
00:11:20,089 --> 00:11:22,108
将约束转换为字符串，但是因为我们已经引入了附件
convert a constraint into a string

238
00:11:22,308 --> 00:11:23,878
将约束转换为字符串，但是因为我们已经引入了附件
however since we've introduced an annex

239
00:11:24,078 --> 00:11:26,218
最好的橙色库这种打印功能突然变得更好了
best orange library this print function

240
00:11:26,418 --> 00:11:27,779
最好的橙色库这种打印功能突然变得更好了
suddenly has become a better match

241
00:11:27,980 --> 00:11:30,688
因为这是Const I，使用此打印功能我们不需要任何种类
because this is a Const I and using this

242
00:11:30,889 --> 00:11:33,389
因为这是Const I，使用此打印功能我们不需要任何种类
print function we don't require any kind

243
00:11:33,589 --> 00:11:35,098
的转换不必是隐式转换，但是有
of conversion doesn't have to be an

244
00:11:35,298 --> 00:11:36,389
的转换不必是隐式转换，但是有
implicit conversion however there does

245
00:11:36,589 --> 00:11:38,428
必须进行转换才能达到这一目标，这样我们就不会出现任何错误
have to be a conversion to reach this

246
00:11:38,629 --> 00:11:40,408
必须进行转换才能达到这一目标，这样我们就不会出现任何错误
one so we don't get any kind of errors

247
00:11:40,609 --> 00:11:42,478
我的意思是零警告，您的错误不是那样，而是
on I mean zero warnings your error is

248
00:11:42,678 --> 00:11:44,908
我的意思是零警告，您的错误不是那样，而是
nothing like that but just by

249
00:11:45,109 --> 00:11:46,889
引入这段代码，我们实际上已经开始调用其他函数
introducing this code we've actually

250
00:11:47,089 --> 00:11:49,108
引入这段代码，我们实际上已经开始调用其他函数
started calling a different function

251
00:11:49,308 --> 00:11:51,149
完全不是编译错误，这是突然的运行时错误
completely this isn't a compile error

252
00:11:51,350 --> 00:11:54,118
完全不是编译错误，这是突然的运行时错误
this is a silent runtime error suddenly

253
00:11:54,318 --> 00:11:56,639
我们所有的文字都是倒退的，可能只有九个
all of our text is backwards that's a

254
00:11:56,839 --> 00:11:58,588
我们所有的文字都是倒退的，可能只有九个
nine may imagine that happening whereas

255
00:11:58,788 --> 00:12:01,139
如果我们根本没有使用过名称空间apple或orange，而我们只是没有使用
if we hadn't used namespace apple or

256
00:12:01,339 --> 00:12:03,328
如果我们根本没有使用过名称空间apple或orange，而我们只是没有使用
orange at all and we just didn't using

257
00:12:03,528 --> 00:12:05,698
到处都有名称空间，那么我们的原始代码就会像那样
namespaces everywhere then our original

258
00:12:05,899 --> 00:12:06,959
到处都有名称空间，那么我们的原始代码就会像那样
code would have been like that and

259
00:12:07,159 --> 00:12:08,338
如果我们只是简单地介绍一下，那将永远不会改变
there's no way that that would ever

260
00:12:08,538 --> 00:12:10,588
如果我们只是简单地介绍一下，那将永远不会改变
change if we just simply introduce

261
00:12:10,788 --> 00:12:12,238
像我们用橙色所做的另一个库，它将保持不变，并且
another library like we did with orange

262
00:12:12,438 --> 00:12:13,678
像我们用橙色所做的另一个库，它将保持不变，并且
it would just remain the same and

263
00:12:13,879 --> 00:12:14,698
一切都很好，所以无论如何我都是对为什么
everything's great

264
00:12:14,899 --> 00:12:17,459
一切都很好，所以无论如何我都是对为什么
so anyway those are my thoughts on why

265
00:12:17,659 --> 00:12:20,149
使用命名空间可能非常非常糟糕，这是另一个非常大的事情
using namespace can be very very bad

266
00:12:20,350 --> 00:12:22,588
使用命名空间可能非常非常糟糕，这是另一个非常大的事情
another really big thing that you

267
00:12:22,788 --> 00:12:24,389
实际上应该避免百分之一百的时间在标题中使用空格
actually should avoid a hundred percent

268
00:12:24,589 --> 00:12:27,149
实际上应该避免百分之一百的时间在标题中使用空格
of the time is using a space in a header

269
00:12:27,350 --> 00:12:31,978
文件永远都不会那样做，因为谁知道那会被包含在哪里
file never ever do that okay because who

270
00:12:32,178 --> 00:12:33,748
文件永远都不会那样做，因为谁知道那会被包含在哪里
knows where that's gonna get included

271
00:12:33,948 --> 00:12:35,279
突然间，您正在使用原本没有的空间
and suddenly you're using a space

272
00:12:35,480 --> 00:12:36,748
突然间，您正在使用原本没有的空间
somewhere where you didn't originally

273
00:12:36,948 --> 00:12:42,658
我们打算在某些时候花费数小时来调试编译错误，因为
intend to we have spent hours at certain

274
00:12:42,859 --> 00:12:45,798
我们打算在某些时候花费数小时来调试编译错误，因为
points debugging compile errors because

275
00:12:45,999 --> 00:12:48,688
一些东西声明为using命名空间和头文件，然后我们得到
something declared a using namespace and

276
00:12:48,889 --> 00:12:50,218
一些东西声明为using命名空间和头文件，然后我们得到
a header file and we then then got

277
00:12:50,418 --> 00:12:51,748
包含在其他地方，如果您有任何追踪，可能会很难追踪
included somewhere else it can get

278
00:12:51,948 --> 00:12:54,298
包含在其他地方，如果您有任何追踪，可能会很难追踪
really hard to track if you have any

279
00:12:54,499 --> 00:12:57,088
相当大的项目，因此绝对不要在头文件中进行操作
kind of sizable project so don't ever do

280
00:12:57,288 --> 00:12:58,889
相当大的项目，因此绝对不要在头文件中进行操作
it in header files if you absolutely

281
00:12:59,089 --> 00:13:01,649
无法阻止自己使用通知，我的意思是我再次暗示我做到了
cannot hold yourself back from using

282
00:13:01,850 --> 00:13:04,769
无法阻止自己使用通知，我的意思是我再次暗示我做到了
notice that I mean I cuz again I do do I

283
00:13:04,970 --> 00:13:07,438
我确实使用命名空间，但通常是出于我自己的库东西
I do use using namespace but I usually

284
00:13:07,639 --> 00:13:10,468
我确实使用命名空间，但通常是出于我自己的库东西
do it for my own kind of library stuff

285
00:13:10,668 --> 00:13:13,288
是的，我从不为STD做它，我从不为STL做它，实际上我写了一个
right I don't do it for STD ever I don't

286
00:13:13,489 --> 00:13:16,498
是的，我从不为STD做它，我从不为STL做它，实际上我写了一个
do it for a STL ever I actually write a

287
00:13:16,698 --> 00:13:19,048
STL每一次称为列向量
STL called column vector every single

288
00:13:19,249 --> 00:13:19,498
STL每一次称为列向量
time

289
00:13:19,698 --> 00:13:21,779
永远不要那样做，因为我想知道自己在使用什么，
never do I not do that okay because

290
00:13:21,980 --> 00:13:24,598
永远不要那样做，因为我想知道自己在使用什么，
again I want to know what I'm using and

291
00:13:24,798 --> 00:13:27,418
我也想避免将来可能发生的错误，但是最大的错误是
also I want to avoid possible errors in

292
00:13:27,619 --> 00:13:29,488
我也想避免将来可能发生的错误，但是最大的错误是
the future right but the biggest one is

293
00:13:29,688 --> 00:13:30,659
真的只是看代码，就能看到城市字符串了
really just looking at the code and

294
00:13:30,860 --> 00:13:32,788
真的只是看代码，就能看到城市字符串了
being able to see okay a city string

295
00:13:32,989 --> 00:13:34,649
该字符串类来自标准C ++库，而不是来自某些库
that this string class is coming from

296
00:13:34,850 --> 00:13:36,389
该字符串类来自标准C ++库，而不是来自某些库
the standard C++ library not from some

297
00:13:36,589 --> 00:13:38,639
其他库真的很容易说，很清楚，但是如果您绝对必须使用
other library really easy to say really

298
00:13:38,839 --> 00:13:40,978
其他库真的很容易说，很清楚，但是如果您绝对必须使用
clear but if you absolutely must use

299
00:13:41,178 --> 00:13:42,659
您的命名空间，如果只需要它，可以在较小的范围内使用它
your namespace you use it in a smaller

300
00:13:42,860 --> 00:13:44,639
您的命名空间，如果只需要它，可以在较小的范围内使用它
scope as possible if you just need it

301
00:13:44,839 --> 00:13:46,529
在if语句中使用它在if语句中使用我们不仅需要它
inside an if statement use it inside an

302
00:13:46,730 --> 00:13:47,848
在if语句中使用它在if语句中使用我们不仅需要它
if statement we don't just need it

303
00:13:48,048 --> 00:13:49,258
在一个非常常见的函数中使用它
inside a function which is pretty common

304
00:13:49,458 --> 00:13:51,838
在一个非常常见的函数中使用它
use it just inside that function and of

305
00:13:52,038 --> 00:13:54,988
最大的范围可能是CVP文件，但是Nanba曾经在头文件中这样做吗
the largest scope may be a CVP file but

306
00:13:55,188 --> 00:13:58,428
最大的范围可能是CVP文件，但是Nanba曾经在头文件中这样做吗
nanba ever do you do it in a header file

307
00:13:58,629 --> 00:14:02,548
无论如何，我希望你们喜欢这个视频，让我知道您的想法
ever anyway I hope you guys enjoyed this

308
00:14:02,749 --> 00:14:04,318
无论如何，我希望你们喜欢这个视频，让我知道您的想法
video let me know what your thoughts are

309
00:14:04,519 --> 00:14:06,448
关于使用主空间为什么要使用它为什么不使用它最大的问题是什么
on using main space why do you use it

310
00:14:06,649 --> 00:14:08,128
关于使用主空间为什么要使用它为什么不使用它最大的问题是什么
why do you not use it what's the biggest

311
00:14:08,328 --> 00:14:09,868
由于某人使用命名空间而导致的灾难
catastrophe that's ever happened as a

312
00:14:10,068 --> 00:14:11,908
由于某人使用命名空间而导致的灾难
result of someone using namespace when

313
00:14:12,109 --> 00:14:14,788
他们不应该有的，下次再见。
they shouldn't have and I will see you

314
00:14:14,989 --> 00:14:17,819
他们不应该有的，下次再见。
guys next time goodbye

315
00:14:18,019 --> 00:14:23,019
[音乐]
[Music]

