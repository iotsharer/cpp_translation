﻿1
00:00:00,030 --> 00:00:01,480
嗨，大家好，我叫eterno，欢迎回到我的c ++系列，所以在
hey what's up guys my name is eterno

2
00:00:01,679 --> 00:00:03,939
嗨，大家好，我叫eterno，欢迎回到我的c ++系列，所以在
welcome back to my c++ series so in the

3
00:00:04,139 --> 00:00:05,919
上一个视频，我解释了为什么你们不喜欢我不喜欢使用命名空间STD
last video I explained why I don't like

4
00:00:06,120 --> 00:00:07,988
上一个视频，我解释了为什么你们不喜欢我不喜欢使用命名空间STD
using namespace STD if you guys haven't

5
00:00:08,189 --> 00:00:10,200
检查视频肯定是这样做的，但是我实际上还没有谈论过
checked that video out definitely do so

6
00:00:10,400 --> 00:00:12,939
检查视频肯定是这样做的，但是我实际上还没有谈论过
but I haven't actually talked about what

7
00:00:13,138 --> 00:00:14,830
名称空间在这个简单的专题系列中，因为在该视频中，
namespaces are in this simple spot

8
00:00:15,029 --> 00:00:16,480
名称空间在这个简单的专题系列中，因为在该视频中，
series and because in that video I

9
00:00:16,679 --> 00:00:18,159
特别想谈论标准名称空间以及为什么我不喜欢
specifically wanted to talk about the

10
00:00:18,359 --> 00:00:20,109
特别想谈论标准名称空间以及为什么我不喜欢
standard namespace and why I don't like

11
00:00:20,309 --> 00:00:24,129
使用名称空间STD，但是今天我们将要讨论
using namespace STD but today we're

12
00:00:24,329 --> 00:00:25,179
使用名称空间STD，但是今天我们将要讨论
going to be talking about what

13
00:00:25,379 --> 00:00:27,789
名称空间在C ++中，为什么当我不使用它们时它们会很有用
namespaces are in C++ why they're useful

14
00:00:27,989 --> 00:00:29,589
名称空间在C ++中，为什么当我不使用它们时它们会很有用
when I would when I wouldn't use them

15
00:00:29,789 --> 00:00:31,419
它们为什么存在，以及我们可以用它们做的所有不同的事情
why they exist and all the different

16
00:00:31,618 --> 00:00:32,500
它们为什么存在，以及我们可以用它们做的所有不同的事情
things that we can do with them

17
00:00:32,700 --> 00:00:35,018
因此，如果我们直接跳进去，这就是我们在其中编写的代码
so if we jump right into this this is

18
00:00:35,219 --> 00:00:36,399
因此，如果我们直接跳进去，这就是我们在其中编写的代码
the code that we had written in that

19
00:00:36,600 --> 00:00:38,349
上一段视频中，我们讨论了为什么我不喜欢到处使用命名空间
previous video where we talked about why

20
00:00:38,549 --> 00:00:40,119
上一段视频中，我们讨论了为什么我不喜欢到处使用命名空间
I don't like using namespace everywhere

21
00:00:40,320 --> 00:00:42,070
以及所有这些东西，让我们谈谈我们实际看到的东西
and all that kind of stuff let's talk a

22
00:00:42,270 --> 00:00:43,570
以及所有这些东西，让我们谈谈我们实际看到的东西
little bit about what we actually see on

23
00:00:43,770 --> 00:00:45,489
屏幕，以及如何进一步扩展名称空间的使用以及原因
the screen here and how we can further

24
00:00:45,689 --> 00:00:47,890
屏幕，以及如何进一步扩展名称空间的使用以及原因
extend our usage of namespaces and why

25
00:00:48,090 --> 00:00:49,390
主要空间实际上是不必要的，所以在这里我们基本上
main spaces are actually used

26
00:00:49,590 --> 00:00:52,538
主要空间实际上是不必要的，所以在这里我们基本上
unnecessary so over here we essentially

27
00:00:52,738 --> 00:00:54,878
有这个问题，我们现在必须打印功能，但它们确实有不同之处
have this issue where we have to print

28
00:00:55,079 --> 00:00:56,678
有这个问题，我们现在必须打印功能，但它们确实有不同之处
functions now they do have different

29
00:00:56,878 --> 00:00:58,390
字符串中这些签名之一的签名在const中是这些签名之一的
signatures one of these takes in a

30
00:00:58,590 --> 00:01:00,369
字符串中这些签名之一的签名在const中是这些签名之一的
string one of these takes in a Const are

31
00:01:00,570 --> 00:01:03,070
但是如果我们恢复原样，也许他们两个都接受一个常数R，那么如果我
but if we kind of revert to maybe both

32
00:01:03,270 --> 00:01:04,959
但是如果我们恢复原样，也许他们两个都接受一个常数R，那么如果我
of them taking in a constant R so if I

33
00:01:05,159 --> 00:01:06,730
更改此字符串1以便实际接收音乐会char指针，就像这样
change this string 1 to actually take in

34
00:01:06,930 --> 00:01:09,969
更改此字符串1以便实际接收音乐会char指针，就像这样
a concert char pointer like so we kind

35
00:01:10,170 --> 00:01:13,179
如果我们不使用名称空间，可能会有一个问题，因为这些功能是
of may have an issue if we weren't using

36
00:01:13,379 --> 00:01:15,789
如果我们不使用名称空间，可能会有一个问题，因为这些功能是
namespaces because these functions are

37
00:01:15,989 --> 00:01:17,890
如果我只是在此处注释掉此命名空间Apple代码行，则完全相同
identical if I just kind of comment out

38
00:01:18,090 --> 00:01:21,549
如果我只是在此处注释掉此命名空间Apple代码行，则完全相同
this namespace Apple line of code here

39
00:01:21,750 --> 00:01:23,799
还有橙色的突然间，如果我尝试的话，我们会遇到一些问题
and the orange one as well suddenly we

40
00:01:24,000 --> 00:01:25,269
还有橙色的突然间，如果我尝试的话，我们会遇到一些问题
have a little bit of an issue if I try

41
00:01:25,469 --> 00:01:27,579
如果我尝试通过点击进行编译，我也会修复此错误
and I'll fix up this error as well if I

42
00:01:27,780 --> 00:01:28,959
如果我尝试通过点击进行编译，我也会修复此错误
try and just compile this by hitting

43
00:01:29,159 --> 00:01:32,679
ctrl f7，您可以在此处看到错误消息，因为它告诉我
ctrl f7 you can see over here that I get

44
00:01:32,879 --> 00:01:34,689
ctrl f7，您可以在此处看到错误消息，因为它告诉我
an error because it's telling me that

45
00:01:34,890 --> 00:01:36,399
接受构造函数的此打印函数已经具有主体，因为我
this print function that takes in the

46
00:01:36,599 --> 00:01:39,369
接受构造函数的此打印函数已经具有主体，因为我
constructor already has a body because I

47
00:01:39,569 --> 00:01:42,399
有两个具有相同名称的符号，它们是相同的符号
have two symbols with the same name and

48
00:01:42,599 --> 00:01:43,689
有两个具有相同名称的符号，它们是相同的符号
they're identical symbols

49
00:01:43,890 --> 00:01:45,849
现在，当我说符号时，我在谈论类函数，变量
now when I say symbols I'm talking about

50
00:01:46,049 --> 00:01:49,599
现在，当我说符号时，我在谈论类函数，变量
things like classes functions variables

51
00:01:49,799 --> 00:01:51,759
保持这种事情不变，所以在这种情况下，我们有两个
constant that kind of that kind of thing

52
00:01:51,959 --> 00:01:54,308
保持这种事情不变，所以在这种情况下，我们有两个
right so we have in this case two

53
00:01:54,509 --> 00:01:56,829
称为print的函数接收构造函数并返回void
functions called print which take in

54
00:01:57,030 --> 00:01:58,569
称为print的函数接收构造函数并返回void
constructor and return void they have

55
00:01:58,769 --> 00:02:00,819
相同的签名，所以这两个符号是相同的我们不能有两个
the same signature so these two symbols

56
00:02:01,019 --> 00:02:02,738
相同的签名，所以这两个符号是相同的我们不能有两个
are identical we can't have two

57
00:02:02,938 --> 00:02:04,569
相同的符号，这是链接错误，也可能是编译错误
identical symbols that's a linking error

58
00:02:04,769 --> 00:02:07,058
相同的符号，这是链接错误，也可能是编译错误
and potentially a compilation error in

59
00:02:07,259 --> 00:02:08,259
如果它们实际上在同一个文件中，那么我们该如何解决呢？
there if they're actually in the same

60
00:02:08,459 --> 00:02:11,950
如果它们实际上在同一个文件中，那么我们该如何解决呢？
file so how do we fix that what if we do

61
00:02:12,150 --> 00:02:14,530
想要打印功能，或者如果我们使用另一个具有
want to have to print functions or what

62
00:02:14,729 --> 00:02:16,780
想要打印功能，或者如果我们使用另一个具有
if we use another library that has

63
00:02:16,979 --> 00:02:19,239
已经定义了打印功能，但是我们现在想要拥有自己的打印功能
already defined a print function but we

64
00:02:19,438 --> 00:02:21,849
已经定义了打印功能，但是我们现在想要拥有自己的打印功能
want to have our own print function now

65
00:02:22,049 --> 00:02:23,739
早在C时代，实际上C仍然没有
back in the days of C and in fact that

66
00:02:23,938 --> 00:02:25,840
早在C时代，实际上C仍然没有
still is a thing C doesn't have

67
00:02:26,039 --> 00:02:28,239
命名空间，这就是为什么您会在OpenGL系列中注意到
namespaces which is why if you would

68
00:02:28,438 --> 00:02:30,129
命名空间，这就是为什么您会在OpenGL系列中注意到
have noticed in the OpenGL series as an

69
00:02:30,329 --> 00:02:31,959
例如，我们使用的是称为GL FW的库，这是一种OpenGL窗口
example we're using a library called GL

70
00:02:32,158 --> 00:02:35,050
例如，我们使用的是称为GL FW的库，这是一种OpenGL窗口
FW which is an OpenGL kind of windowing

71
00:02:35,250 --> 00:02:37,030
和上下文库，以及将所有此类内容正确处理，但要点
and context library and in putting all

72
00:02:37,229 --> 00:02:38,679
和上下文库，以及将所有此类内容正确处理，但要点
that kind of stuff right but the point

73
00:02:38,878 --> 00:02:41,319
是jello W是C是C库，它与C和C ++都兼容，因为
is jello W is a C is a C library it's

74
00:02:41,519 --> 00:02:44,050
是jello W是C是C库，它与C和C ++都兼容，因为
compatible with both C and C++ because

75
00:02:44,250 --> 00:02:46,599
这是一个图书馆，他们不能使用名称空间，这就是为什么如果您注意到
it's a library they can't use namespaces

76
00:02:46,799 --> 00:02:48,280
这是一个图书馆，他们不能使用名称空间，这就是为什么如果您注意到
which is why if you notice in that

77
00:02:48,479 --> 00:02:49,868
系列化我们编写的所有与JFW有关的代码
series all of the code that we write

78
00:02:50,068 --> 00:02:51,750
系列化我们编写的所有与JFW有关的代码
that has something to do with JFW

79
00:02:51,949 --> 00:02:54,280
首先从字面上看，每个函数都以GL FW开头，然后像
begins with like literally every

80
00:02:54,479 --> 00:02:56,920
首先从字面上看，每个函数都以GL FW开头，然后像
function starts with GL FW and then like

81
00:02:57,120 --> 00:02:59,170
函数名称，例如其中的gel W或W的jail创建窗口，所有
the function name for example gel W in

82
00:02:59,370 --> 00:03:01,989
函数名称，例如其中的gel W或W的jail创建窗口，所有
it or jail of W create window all of

83
00:03:02,188 --> 00:03:04,149
那个东西用杰夫W字面意思是用opengl放在前面
that stuff has jelf W literally

84
00:03:04,348 --> 00:03:07,090
那个东西用杰夫W字面意思是用opengl放在前面
prepended to the front of it with opengl

85
00:03:07,289 --> 00:03:08,439
我们也有GL，然后是符号名称，就像
it's kind of the same thing as well we

86
00:03:08,639 --> 00:03:11,170
我们也有GL，然后是符号名称，就像
have GL and then the symbol name so like

87
00:03:11,370 --> 00:03:15,069
GL开始GL和GL j以及顶点缓冲区，我们拥有一个应用程序
GL begin GL & GL j and vertex buffers

88
00:03:15,269 --> 00:03:18,039
GL开始GL和GL j以及顶点缓冲区，我们拥有一个应用程序
that kind of thing right we have an app

89
00:03:18,239 --> 00:03:19,990
契约的实际名称或嵌入其中的某种ID
deed the actual name of the library or

90
00:03:20,189 --> 00:03:23,050
契约的实际名称或嵌入其中的某种ID
some kind of ID embedded into the

91
00:03:23,250 --> 00:03:25,179
函数名称，所以在我们的代码中，它等同于我在字面上有
function name so in our code that will

92
00:03:25,378 --> 00:03:26,830
函数名称，所以在我们的代码中，它等同于我在字面上有
be the equivalent of if I had literally

93
00:03:27,030 --> 00:03:29,349
像写苹果打印权和橙色打印一样的东西，现在你可以
written something like Apple print right

94
00:03:29,549 --> 00:03:31,719
像写苹果打印权和橙色打印一样的东西，现在你可以
and orange print and then now you can

95
00:03:31,919 --> 00:03:33,670
看到我确定这两个功能是不同的，因为这就像苹果
see that I okay these two functions are

96
00:03:33,870 --> 00:03:35,289
看到我确定这两个功能是不同的，因为这就像苹果
different because this is like the Apple

97
00:03:35,489 --> 00:03:36,999
印刷版，这是橙色印刷版，实际上就是C
print version this is the orange print

98
00:03:37,199 --> 00:03:39,099
印刷版，这是橙色印刷版，实际上就是C
version and that's actually what C

99
00:03:39,299 --> 00:03:41,709
库之所以这样做，是因为它们不能仅仅让您知道我无法创建函数
libraries tend to do because they can't

100
00:03:41,908 --> 00:03:43,929
库之所以这样做，是因为它们不能仅仅让您知道我无法创建函数
just you know I can't make a function

101
00:03:44,128 --> 00:03:46,569
之所以称为Ahmet，是因为它是如此通用
just called Ahmet because in it is such

102
00:03:46,769 --> 00:03:49,990
之所以称为Ahmet，是因为它是如此通用
a common name what if you know if every

103
00:03:50,189 --> 00:03:51,429
库就是要在其中命名它们的功能，然后您就可以
library was to do it was to just name

104
00:03:51,628 --> 00:03:53,140
库就是要在其中命名它们的功能，然后您就可以
their functions in it then you could

105
00:03:53,340 --> 00:03:54,789
永远不要将这些库一起使用，而不必修改源代码
never use those libraries together

106
00:03:54,989 --> 00:03:56,590
永远不要将这些库一起使用，而不必修改源代码
without having to modify the source code

107
00:03:56,789 --> 00:03:59,379
所以他们通常做的只是假装
so what they do usually is just kind of

108
00:03:59,579 --> 00:03:59,980
所以他们通常做的只是假装
prepend

109
00:04:00,180 --> 00:04:01,899
库的名称，所以J在其中，W之所以起作用，是因为它是一个
the name of library to it so J of W in

110
00:04:02,098 --> 00:04:04,270
库的名称，所以J在其中，W之所以起作用，是因为它是一个
it and that works because well it's a

111
00:04:04,469 --> 00:04:06,550
相当独特的名称，正确代入w，好酷，这是W的G特有的
fairly unique name right delft w in it

112
00:04:06,750 --> 00:04:09,610
相当独特的名称，正确代入w，好酷，这是W的G特有的
okay cool that's specific to that G of W

113
00:04:09,810 --> 00:04:12,520
库现在使用C ++编写，而不是我们要做的所有事情，我们有了命名空间
library now in C++ instead of us having

114
00:04:12,719 --> 00:04:14,860
库现在使用C ++编写，而不是我们要做的所有事情，我们有了命名空间
to do all this we have namespaces to

115
00:04:15,060 --> 00:04:17,288
解决名称空间的主要目的是避免命名冲突
solve that issue the primary purpose of

116
00:04:17,488 --> 00:04:19,959
解决名称空间的主要目的是避免命名冲突
namespaces is to avoid naming conflicts

117
00:04:20,158 --> 00:04:22,509
这就是为什么它们存在以避免命名冲突的原因
that's why they exist to avoid naming

118
00:04:22,709 --> 00:04:24,329
这就是为什么它们存在以避免命名冲突的原因
conflicts okay

119
00:04:24,528 --> 00:04:25,710
就是这样，如果您感到困惑
that's it

120
00:04:25,910 --> 00:04:27,389
就是这样，如果您感到困惑
sort of just if you've getting confused

121
00:04:27,589 --> 00:04:29,100
为什么命名空间存在或为什么被使用是因为命名
by why namespaces exist or why they're

122
00:04:29,300 --> 00:04:31,650
为什么命名空间存在或为什么被使用是因为命名
being used it's because of naming we

123
00:04:31,850 --> 00:04:33,900
希望能够在不同的上下文中调用相同名称的符号，所以如果我们去
want to be able to call symbols the same

124
00:04:34,100 --> 00:04:37,110
希望能够在不同的上下文中调用相同名称的符号，所以如果我们去
name in different contexts so if we go

125
00:04:37,310 --> 00:04:38,819
回到这里进入苹果打印而不是拥有我们可以做的是
back here into Apple print instead of

126
00:04:39,019 --> 00:04:40,500
回到这里进入苹果打印而不是拥有我们可以做的是
having this what we can do is enclose

127
00:04:40,699 --> 00:04:43,439
在这种情况下，命名空间中的此符号是命名空间的名称
this symbol in a namespace in this case

128
00:04:43,639 --> 00:04:45,750
在这种情况下，命名空间中的此符号是命名空间的名称
what we write is namespace the name of

129
00:04:45,949 --> 00:04:48,090
命名空间，然后就像我们将其围绕在其中一样
the namespace and then just as if it was

130
00:04:48,290 --> 00:04:50,189
命名空间，然后就像我们将其围绕在其中一样
any kind of function we surround it with

131
00:04:50,389 --> 00:04:52,199
大括号里面的所有东西都在这些大括号之间
curly braces which everything in

132
00:04:52,399 --> 00:04:53,939
大括号里面的所有东西都在这些大括号之间
everything in between these curly

133
00:04:54,139 --> 00:04:56,370
我个人喜欢写在Apple名称空间中的方括号
brackets is included in that Apple

134
00:04:56,569 --> 00:04:58,770
我个人喜欢写在Apple名称空间中的方括号
namespace now personally I like to write

135
00:04:58,970 --> 00:05:01,800
我的大括号和实际的名称空间声明在同一行
my curly bracket on the same line as the

136
00:05:02,000 --> 00:05:03,449
我的大括号和实际的名称空间声明在同一行
actual namespace declaration I don't

137
00:05:03,649 --> 00:05:05,310
这样做的原因是因为当我们有多个嵌套时
like doing this the reason that happens

138
00:05:05,509 --> 00:05:07,129
这样做的原因是因为当我们有多个嵌套时
is because when we have multiple nested

139
00:05:07,329 --> 00:05:10,230
命名空间有点混乱，所以我想做的是如果我们有Apple和
namespaces it gets a little bit messy so

140
00:05:10,430 --> 00:05:12,270
命名空间有点混乱，所以我想做的是如果我们有Apple和
what I like to do is if we had Apple and

141
00:05:12,470 --> 00:05:14,250
然后也许像函数或某些您知道的我在这里以及
then maybe like functions or something

142
00:05:14,449 --> 00:05:16,560
然后也许像函数或某些您知道的我在这里以及
you know I have this here and also with

143
00:05:16,759 --> 00:05:18,150
结束括号而不是那种用这种方式写的东西，这就是视觉
the end braces instead of kind of

144
00:05:18,350 --> 00:05:19,770
结束括号而不是那种用这种方式写的东西，这就是视觉
writing it this way which is what Visual

145
00:05:19,970 --> 00:05:21,060
Studio会尝试将订单格式化为我喜欢的格式
Studio will try and try and order

146
00:05:21,259 --> 00:05:23,250
Studio会尝试将订单格式化为我喜欢的格式
formatted to I like to just have it on

147
00:05:23,449 --> 00:05:25,319
这条线使我的实际功能缩进了一层而不是
the same line that way my actual

148
00:05:25,519 --> 00:05:27,780
这条线使我的实际功能缩进了一层而不是
functions are one level indented and not

149
00:05:27,980 --> 00:05:30,240
例如2或3或4，或者取决于我们实际嵌套了多少个名称空间
like 2 or 3 or 4 or depending on how

150
00:05:30,439 --> 00:05:32,720
例如2或3或4，或者取决于我们实际嵌套了多少个名称空间
many namespaces we actually have nested

151
00:05:32,920 --> 00:05:34,889
所以这只是我个人的喜好，您不必遵循它，但这就是
so that's just my personal preference

152
00:05:35,089 --> 00:05:36,300
所以这只是我个人的喜好，您不必遵循它，但这就是
you don't have to follow that but that's

153
00:05:36,500 --> 00:05:38,189
为什么我把所有这些东西放在同一行上
why I kind of put all this stuff on the

154
00:05:38,389 --> 00:05:40,590
为什么我把所有这些东西放在同一行上
same line so that being said we now have

155
00:05:40,790 --> 00:05:42,840
有苹果打印功能，我在这里把它抓成橙色并摆脱了
a have an Apple print function I've

156
00:05:43,040 --> 00:05:44,370
有苹果打印功能，我在这里把它抓成橙色并摆脱了
grabbed it here into orange and get rid

157
00:05:44,569 --> 00:05:46,500
这些评论中，我们还有这个橙色打印功能，我可以将其重命名为
of these comments as well we have this

158
00:05:46,699 --> 00:05:48,770
这些评论中，我们还有这个橙色打印功能，我可以将其重命名为
orange print function I can rename it to

159
00:05:48,970 --> 00:05:52,110
只需打印，我们就能拥有完全相同的两个功能
just print so we're able to have two

160
00:05:52,310 --> 00:05:53,550
只需打印，我们就能拥有完全相同的两个功能
functions with exactly the same

161
00:05:53,750 --> 00:05:55,770
在此文件中签名，何时需要调用它，我们需要做的是
signature in this file and when it comes

162
00:05:55,970 --> 00:05:57,660
在此文件中签名，何时需要调用它，我们需要做的是
time to call it what we do is we need to

163
00:05:57,860 --> 00:06:00,240
指定我们需要调用哪种方式，以便打印现在位于苹果或苹果内部
specify which one way we need to call so

164
00:06:00,439 --> 00:06:02,550
指定我们需要调用哪种方式，以便打印现在位于苹果或苹果内部
print is now inside either the apple or

165
00:06:02,750 --> 00:06:04,259
橙色名称空间有两个，所以如果我们要使用Apple的一个
the orange namespace we have two of them

166
00:06:04,459 --> 00:06:06,150
橙色名称空间有两个，所以如果我们要使用Apple的一个
so if we want to use the one from Apple

167
00:06:06,350 --> 00:06:08,639
我们只要输入Apple ::，然后打印出此::本质上就像一个名称空间
we just type Apple : : and then print

168
00:06:08,839 --> 00:06:11,520
我们只要输入Apple ::，然后打印出此::本质上就像一个名称空间
this : : is essentially like a namespace

169
00:06:11,720 --> 00:06:12,300
操作员，所以您要做的是
operator

170
00:06:12,500 --> 00:06:14,009
操作员，所以您要做的是
so what you do is every kind of

171
00:06:14,209 --> 00:06:15,840
向下钻取名称空间时所要使用的名称空间，您只需拨打电话并
namespace that you go as you drill down

172
00:06:16,040 --> 00:06:17,430
向下钻取名称空间时所要使用的名称空间，您只需拨打电话并
into a namespace you just put a call and

173
00:06:17,629 --> 00:06:20,819
：这就是我想输入的名称空间，或者允许您调用
: and that is what I guess enters into

174
00:06:21,019 --> 00:06:22,410
：这就是我想输入的名称空间，或者允许您调用
that namespace or allows you to call

175
00:06:22,610 --> 00:06:24,480
来自那个名称空间的东西，同样的东西也适用于静态函数
things from that namespace the same

176
00:06:24,680 --> 00:06:27,150
来自那个名称空间的东西，同样的东西也适用于静态函数
thing applies to static functions I

177
00:06:27,350 --> 00:06:29,759
类中的猜测或符号，以及类似的方法和类，以及所有这些
guess or symbols in classes and like

178
00:06:29,959 --> 00:06:31,790
类中的猜测或符号，以及类似的方法和类，以及所有这些
methods and classes and all that stuff a

179
00:06:31,990 --> 00:06:34,439
类有点像一个名称空间，它是它自己的名称空间，它是
class is kind of like a namespace it is

180
00:06:34,639 --> 00:06:36,750
类有点像一个名称空间，它是它自己的名称空间，它是
it is a namespace of its own which is

181
00:06:36,949 --> 00:06:38,639
为什么如果我们像内部类一样访问
why if we're accessing like an inner

182
00:06:38,839 --> 00:06:39,569
为什么如果我们像内部类一样访问
class inside

183
00:06:39,769 --> 00:06:43,110
类或枚举，或者您知道像我给的例子一样static
the class or like an enum or you know

184
00:06:43,310 --> 00:06:45,180
类或枚举，或者您知道像我给的例子一样static
like I gave the example of like static

185
00:06:45,379 --> 00:06:47,069
函数甚至是非静态函数，在某些情况下，我们使用：：
functions or even non static functions

186
00:06:47,269 --> 00:06:50,910
函数甚至是非静态函数，在某些情况下，我们使用：：
in some cases we use : : so that's what

187
00:06:51,110 --> 00:06:52,770
：：一种方式，因此使用此双冒号获取打印功能
the : : kind of means so this double

188
00:06:52,970 --> 00:06:55,139
：：一种方式，因此使用此双冒号获取打印功能
colon is used to get the print function

189
00:06:55,339 --> 00:06:57,240
来自苹果，如果我们要运行它，我们将获得全息打印
from the apple and if we were to run

190
00:06:57,439 --> 00:06:58,649
来自苹果，如果我们要运行它，我们将获得全息打印
this we would just get holo printing

191
00:06:58,848 --> 00:07:00,869
通常，如果我想给橙色打电话，显然我现在输入橙色
normally if I want to call the orange

192
00:07:01,069 --> 00:07:03,059
通常，如果我想给橙色打电话，显然我现在输入橙色
one obviously I type in orange now that

193
00:07:03,259 --> 00:07:04,528
我们在上一集中讨论的整个使用名称空间的事情是
whole using namespace thing that we

194
00:07:04,728 --> 00:07:06,119
我们在上一集中讨论的整个使用名称空间的事情是
talked about in the last episode was

195
00:07:06,319 --> 00:07:08,369
只是不必写这种苹果或橙子前缀的方式
just a way to not have to write this

196
00:07:08,569 --> 00:07:11,100
只是不必写这种苹果或橙子前缀的方式
kind of apple or orange kind of prefix

197
00:07:11,300 --> 00:07:13,170
因此，不必执行此操作，我们可以说实际上是在使用
so instead of having to do this what we

198
00:07:13,370 --> 00:07:15,329
因此，不必执行此操作，我们可以说实际上是在使用
can say is that actually we're using

199
00:07:15,529 --> 00:07:17,610
苹果命名空间，这意味着基本上从
namespace Apple which means that

200
00:07:17,810 --> 00:07:19,528
苹果命名空间，这意味着基本上从
basically import everything from the

201
00:07:19,728 --> 00:07:21,600
苹果的命名空间，就好像我从未将其指定为命名空间一样，在此
namespace Apple as if I had never

202
00:07:21,800 --> 00:07:24,088
苹果的命名空间，就好像我从未将其指定为命名空间一样，在此
specified it as a namespace so in this

203
00:07:24,288 --> 00:07:26,338
如果需要，可以使用Apple的保护套打印，可以将其更改为橙色，可以
case print will be used from Apple if I

204
00:07:26,538 --> 00:07:28,588
如果需要，可以使用Apple的保护套打印，可以将其更改为橙色，可以
want I can change it to orange I can

205
00:07:28,788 --> 00:07:31,410
还说如果我们在这里有多个事物，例如我们有印刷品，也许还有印刷品
also say if we have multiple things in

206
00:07:31,610 --> 00:07:33,718
还说如果我们在这里有多个事物，例如我们有印刷品，也许还有印刷品
here like we have print and maybe print

207
00:07:33,918 --> 00:07:37,110
再打印一次O打印，或者如果我有多种功能，则可能不
out an O print again or something if I

208
00:07:37,310 --> 00:07:38,790
再打印一次O打印，或者如果我有多种功能，则可能不
have multiple functions maybe I don't

209
00:07:38,990 --> 00:07:41,459
想要再次使用橙色打印，我只是想把它当作苹果扔掉
want to use print again from orange I

210
00:07:41,658 --> 00:07:44,490
想要再次使用橙色打印，我只是想把它当作苹果扔掉
just want to throw that as an apple

211
00:07:44,689 --> 00:07:46,740
我们去使用命名空间苹果吧，也许我不想再打印一次
let's go to using namespace apple maybe

212
00:07:46,939 --> 00:07:48,778
我们去使用命名空间苹果吧，也许我不想再打印一次
I don't want print again when I put this

213
00:07:48,978 --> 00:07:50,790
简单的名称空间，我不想再次使用打印，我仍然想指定
easy name space I don't want to use

214
00:07:50,990 --> 00:07:52,230
简单的名称空间，我不想再次使用打印，我仍然想指定
print again I still want to specify

215
00:07:52,430 --> 00:07:54,870
也许我想再打印一次，我只希望打印很重要，所以要这样做
maybe which print again I want I just

216
00:07:55,069 --> 00:07:57,449
也许我想再打印一次，我只希望打印很重要，所以要这样做
want print to be important so to do that

217
00:07:57,649 --> 00:08:00,689
我们可以使用Apple编写，然后将函数名称打印出来，这意味着
we can just write using Apple and then

218
00:08:00,889 --> 00:08:03,059
我们可以使用Apple编写，然后将函数名称打印出来，这意味着
the function name print okay that means

219
00:08:03,259 --> 00:08:04,980
我们将专门从Apple名称空间中获取此信息，但对于
that we're going to take this from the

220
00:08:05,180 --> 00:08:07,170
我们将专门从Apple名称空间中获取此信息，但对于
Apple namespace specifically but for

221
00:08:07,370 --> 00:08:08,819
我们仍然必须指定类似的名称空间，您也可以创建
this we'd still have to specify that

222
00:08:09,019 --> 00:08:11,040
我们仍然必须指定类似的名称空间，您也可以创建
namespace like that you can also create

223
00:08:11,240 --> 00:08:14,129
只需输入您知道等于Apple的名称空间即可获得名称空间的别名
aliases for namespaces by just typing in

224
00:08:14,329 --> 00:08:16,920
只需输入您知道等于Apple的名称空间即可获得名称空间的别名
namespace you know a equals Apple or

225
00:08:17,120 --> 00:08:18,569
这样的事情意味着我可以输入类似的打印内容，而不是
something like that that means I can

226
00:08:18,769 --> 00:08:20,730
这样的事情意味着我可以输入类似的打印内容，而不是
type in a print like that instead of

227
00:08:20,930 --> 00:08:22,769
不得不拼出苹果，这对于嵌套时非常有用
having to spell out Apple that's very

228
00:08:22,968 --> 00:08:24,829
不得不拼出苹果，这对于嵌套时非常有用
useful for when you have nested

229
00:08:25,029 --> 00:08:27,329
命名空间，因此我们也可以嵌套和命名空间，如您先前所见
namespaces so we can also nest and

230
00:08:27,529 --> 00:08:29,249
命名空间，因此我们也可以嵌套和命名空间，如您先前所见
namespaces as you probably saw earlier

231
00:08:29,449 --> 00:08:31,050
因此我们可以拥有Apple，也许我们包括所有有关功能的信息，
so we can have Apple and maybe we

232
00:08:31,250 --> 00:08:32,878
因此我们可以拥有Apple，也许我们包括所有有关功能的信息，
include all about function and we can

233
00:08:33,078 --> 00:08:34,378
由于某种原因，关闭功能障碍名称空间内的所有函数
close all about functions inside the

234
00:08:34,578 --> 00:08:36,120
由于某种原因，关闭功能障碍名称空间内的所有函数
dysfunctions namespace for some reason

235
00:08:36,320 --> 00:08:38,549
你可以问我为什么只是一个例子，所以我们现在拥有名称空间苹果
you can ask me why just an example so we

236
00:08:38,750 --> 00:08:40,019
你可以问我为什么只是一个例子，所以我们现在拥有名称空间苹果
have namespace apple we have nowadays

237
00:08:40,219 --> 00:08:42,389
功能，然后不必写我们就可以知道Apple功能，例如
functions and then instead of us having

238
00:08:42,589 --> 00:08:44,849
功能，然后不必写我们就可以知道Apple功能，例如
to write you know Apple functions like

239
00:08:45,049 --> 00:08:48,199
那或者你知道使用命名空间
that or you know something like using

240
00:08:48,399 --> 00:08:49,939
那或者你知道使用命名空间
using namespace

241
00:08:50,139 --> 00:08:51,569
像这样的苹果功能我们也可以顺便
Apple function

242
00:08:51,769 --> 00:08:54,719
像这样的苹果功能我们也可以顺便
like this we could also by the way for

243
00:08:54,919 --> 00:08:56,758
在此示例中，您也可以将其拆分，以便像使用Access Apple一样
this example you can also split this up

244
00:08:56,958 --> 00:08:59,129
在此示例中，您也可以将其拆分，以便像使用Access Apple一样
so you can be like using access apple

245
00:08:59,330 --> 00:09:01,529
并使用命名空间函数，这将使我们不必指定
and using namespace functions which

246
00:09:01,730 --> 00:09:04,529
并使用命名空间函数，这将使我们不必指定
would allow us to not have to specify

247
00:09:04,730 --> 00:09:06,000
任何用于打印的名称空间，因此我们不必这样做或在其中
any namespace for print at all

248
00:09:06,200 --> 00:09:08,370
任何用于打印的名称空间，因此我们不必这样做或在其中
so instead of us having to do this or in

249
00:09:08,570 --> 00:09:11,399
另一个例子，我们也可以说名称空间a等于Apple函数
the other example that we can also just

250
00:09:11,600 --> 00:09:14,969
另一个例子，我们也可以说名称空间a等于Apple函数
say namespace a equals Apple functions

251
00:09:15,169 --> 00:09:17,069
这意味着我可以只键入打印内容，就好像它不是一个名称空间一样，
which means that I can just type in a

252
00:09:17,269 --> 00:09:19,589
这意味着我可以只键入打印内容，就好像它不是一个名称空间一样，
print as if it wasn't one namespace and

253
00:09:19,789 --> 00:09:22,679
我可以在那得到结果，所以这是一个非常基本的城镇示例
I would get that result over there okay

254
00:09:22,879 --> 00:09:25,349
我可以在那得到结果，所以这是一个非常基本的城镇示例
so that's a pretty basic example of town

255
00:09:25,549 --> 00:09:28,500
命名空间有效，我不确定是否还有其他需要讨论的地方
namespaces work I'm not sure if there's

256
00:09:28,700 --> 00:09:30,839
命名空间有效，我不确定是否还有其他需要讨论的地方
really anything else that I need to talk

257
00:09:31,039 --> 00:09:33,659
诸如此类的事情显然仅限于实际范围
about obviously things like things like

258
00:09:33,860 --> 00:09:36,899
诸如此类的事情显然仅限于实际范围
this are confined to the actual scope so

259
00:09:37,100 --> 00:09:39,599
换句话说，此名称空间a等于Apple函数仅存在于此名称空间中
in other words a this namespace a equals

260
00:09:39,799 --> 00:09:41,549
换句话说，此名称空间a等于Apple函数仅存在于此名称空间中
Apple functions only exists within this

261
00:09:41,750 --> 00:09:43,198
主要功能我无法访问外部
main function I can't access to the

262
00:09:43,399 --> 00:09:44,309
主要功能我无法访问外部
outside

263
00:09:44,509 --> 00:09:46,740
我的建议是，当您执行类似名称空间等的操作时
my recommendation is when you do things

264
00:09:46,940 --> 00:09:48,750
我的建议是，当您执行类似名称空间等的操作时
like this like namespace a equals blah

265
00:09:48,950 --> 00:09:51,959
等等，或者使用命名空间，无论我的建议是什么，您都尝试将其限制为
blah or using namespace whatever my

266
00:09:52,159 --> 00:09:53,909
等等，或者使用命名空间，无论我的建议是什么，您都尝试将其限制为
recommendation is you try and confine it

267
00:09:54,110 --> 00:09:56,128
如果仅在if语句中需要它，则将其范围缩小
to a smaller scope as possible if you

268
00:09:56,328 --> 00:09:58,169
如果仅在if语句中需要它，则将其范围缩小
just need it inside an if statement for

269
00:09:58,370 --> 00:10:00,959
例如，您知道对，您可以在if语句if中编写声明
example you know right you write your

270
00:10:01,159 --> 00:10:03,209
例如，您知道对，您可以在if语句if中编写声明
declaration inside that if statement if

271
00:10:03,409 --> 00:10:04,979
您只需要在函数内部添加它，然后在函数内部编写，然后
you just need it inside a function then

272
00:10:05,179 --> 00:10:06,689
您只需要在函数内部添加它，然后在函数内部编写，然后
writing inside the function and then as

273
00:10:06,889 --> 00:10:09,359
不得已时将最后结果写在顶层
a last result as a last resort write it

274
00:10:09,559 --> 00:10:10,859
不得已时将最后结果写在顶层
in there like kind of in a top-level

275
00:10:11,059 --> 00:10:13,349
文件，但不要尝试，就像您知道的那样在顶级处声明它
file but don't try and like just you

276
00:10:13,549 --> 00:10:15,419
文件，但不要尝试，就像您知道的那样在顶级处声明它
know declare it in at the top level at

277
00:10:15,620 --> 00:10:18,089
始终或在头文件中肯定永远不会在头文件中这样做的原因
all times or in header files certainly

278
00:10:18,289 --> 00:10:20,819
始终或在头文件中肯定永远不会在头文件中这样做的原因
never do it in header files the reason

279
00:10:21,019 --> 00:10:22,529
我们不想这样做是因为当我们得知您是否开始使用
we don't want to do that is because when

280
00:10:22,730 --> 00:10:24,779
我们不想这样做是因为当我们得知您是否开始使用
we get you know if we start using

281
00:10:24,980 --> 00:10:27,899
就像您可以想象的那样，到处都是命名空间
namespace everywhere then as you can

282
00:10:28,100 --> 00:10:31,349
就像您可以想象的那样，到处都是命名空间
probably imagine that kind of undoes all

283
00:10:31,549 --> 00:10:33,059
名称空间通过避免命名冲突来完成的有用工作，因为
of the helpful work that namespaces do

284
00:10:33,259 --> 00:10:35,128
名称空间通过避免命名冲突来完成的有用工作，因为
by avoiding naming conflicts because if

285
00:10:35,328 --> 00:10:36,569
我们正在使用它，如果我们在命名空间中使用我们的函数，那有什么意义
we're using it what's the point of

286
00:10:36,769 --> 00:10:39,089
我们正在使用它，如果我们在命名空间中使用我们的函数，那有什么意义
having our functions in namespaces if we

287
00:10:39,289 --> 00:10:41,250
如果我们无论如何都使用命名空间，则必须导入所有内容，这意味着
have to if we if we use namespace anyway

288
00:10:41,450 --> 00:10:43,769
如果我们无论如何都使用命名空间，则必须导入所有内容，这意味着
right it imports everything which means

289
00:10:43,970 --> 00:10:45,240
我们可能不使用名称空间，我们可以回到
that we might as well not be using

290
00:10:45,440 --> 00:10:47,008
我们可能不使用名称空间，我们可以回到
namespaces and we can get back to the

291
00:10:47,208 --> 00:10:48,990
库之间或我们与之间的命名冲突问题
problem of having naming conflicts

292
00:10:49,190 --> 00:10:51,000
库之间或我们与之间的命名冲突问题
between libraries or between our and

293
00:10:51,200 --> 00:10:53,309
代码，这只是要考虑的几件事，我在下面留下了一个链接
code so that's just a few things to

294
00:10:53,509 --> 00:10:55,620
代码，这只是要考虑的几件事，我在下面留下了一个链接
think about I've left a link below to

295
00:10:55,820 --> 00:10:58,109
有关命名空间的cpp参考页，其中包含一些您可以使用的示例
the cpp reference page on namespaces

296
00:10:58,309 --> 00:11:00,628
有关命名空间的cpp参考页，其中包含一些您可以使用的示例
this has a few examples of what you can

297
00:11:00,828 --> 00:11:02,519
实际上是这样做的，还有所谓的内联名称空间
actually do so there's also something

298
00:11:02,720 --> 00:11:03,959
实际上是这样做的，还有所谓的内联名称空间
called an inline namespace

299
00:11:04,159 --> 00:11:05,818
实际上只是使所有内容都可用于父命名空间
really just kind of makes everything

300
00:11:06,019 --> 00:11:08,399
实际上只是使所有内容都可用于父命名空间
available to the parent namespace we

301
00:11:08,600 --> 00:11:10,678
自从丢失以来，它还具有实际指定名称空间的能力17
also have since it was lost 17 the

302
00:11:10,879 --> 00:11:12,448
自从丢失以来，它还具有实际指定名称空间的能力17
ability to actually specify namespaces

303
00:11:12,649 --> 00:11:14,818
有点像这样，在这种情况下，当我有此功能，而不必
kind of like this so in this case when I

304
00:11:15,019 --> 00:11:16,438
有点像这样，在这种情况下，当我有此功能，而不必
had this functions instead of having to

305
00:11:16,639 --> 00:11:18,359
做命名空间苹果然后命名空间功能我实际上可以写
do namespace Apple then namespace

306
00:11:18,559 --> 00:11:20,729
做命名空间苹果然后命名空间功能我实际上可以写
functions I can actually just write

307
00:11:20,929 --> 00:11:23,339
像这样的命名空间功能并非所有人都支持
namespace functions like that be a way

308
00:11:23,539 --> 00:11:24,479
像这样的命名空间功能并非所有人都支持
that that's not supported by all

309
00:11:24,679 --> 00:11:27,719
当然是编译器，这是一种新的C ++，我建议您阅读
compilers of course and it is a new kind

310
00:11:27,919 --> 00:11:30,448
当然是编译器，这是一种新的C ++，我建议您阅读
of C++ thing I recommend you read

311
00:11:30,649 --> 00:11:31,469
如果您想知道可以做的所有事情，请访问此页面
through this page if you want to know

312
00:11:31,669 --> 00:11:32,549
如果您想知道可以做的所有事情，请访问此页面
everything that you can do with

313
00:11:32,750 --> 00:11:34,078
我已经大致涵盖了大多数有用的名称空间，
namespaces I've pretty much covered most

314
00:11:34,278 --> 00:11:36,149
我已经大致涵盖了大多数有用的名称空间，
of the useful things obviously the

315
00:11:36,350 --> 00:11:39,029
我们使用名称空间的原因是-如果我们要创建代码库
reason why we use namespaces is kind of

316
00:11:39,230 --> 00:11:41,969
我们使用名称空间的原因是-如果我们要创建代码库
- if we are creating a library of code

317
00:11:42,169 --> 00:11:44,248
或者，如果我们只是拥有我们的项目，我们想将其放在命名空间的后面，以便我们
or if we just have our project we want

318
00:11:44,448 --> 00:11:45,988
或者，如果我们只是拥有我们的项目，我们想将其放在命名空间的后面，以便我们
to put it behind a namespace so that we

319
00:11:46,188 --> 00:11:47,938
这样就不会有任何命名冲突了，我可以自由创建任何我可以使用的函数
don't get any naming conflicts that way

320
00:11:48,139 --> 00:11:50,279
这样就不会有任何命名冲突了，我可以自由创建任何我可以使用的函数
I'm free to create any function that I

321
00:11:50,480 --> 00:11:52,349
真的希望C ++标准库中的所有内容都落后于该标准
really want everything in the C++

322
00:11:52,549 --> 00:11:54,328
真的希望C ++标准库中的所有内容都落后于该标准
standard library is behind the standard

323
00:11:54,528 --> 00:11:56,938
STD名称空间，因此显然您不会像这样出现命名冲突
STD namespace so obviously you're not

324
00:11:57,139 --> 00:11:58,589
STD名称空间，因此显然您不会像这样出现命名冲突
going to get naming conflicts like that

325
00:11:58,789 --> 00:12:02,008
但是，如果您使用任何种类的C代码，则没有名称空间之类的东西，这意味着
however if you use any kind of C code no

326
00:12:02,208 --> 00:12:03,779
但是，如果您使用任何种类的C代码，则没有名称空间之类的东西，这意味着
such thing as namespaces which means

327
00:12:03,980 --> 00:12:05,938
当您使用C代码时，您可能会面临命名冲突的风险，因此
that when you do use C code you are

328
00:12:06,139 --> 00:12:08,399
当您使用C代码时，您可能会面临命名冲突的风险，因此
potentially risking a naming conflict so

329
00:12:08,600 --> 00:12:11,519
即使在您的C ++项目中，我当然仍然会使用名称空间，因为如果您
even in your C++ project I would still

330
00:12:11,720 --> 00:12:14,998
即使在您的C ++项目中，我当然仍然会使用名称空间，因为如果您
use a namespace of course because if you

331
00:12:15,198 --> 00:12:17,488
使用任何C代码，如果具有相同的代码，那将是立即冲突
use any C code then that's going to be

332
00:12:17,688 --> 00:12:19,859
使用任何C代码，如果具有相同的代码，那将是立即冲突
an immediate conflict if it has the same

333
00:12:20,059 --> 00:12:21,659
函数名称以及您实际上是在声明名称空间离开
function name as well you're actually

334
00:12:21,860 --> 00:12:24,118
函数名称以及您实际上是在声明名称空间离开
declaring anyway that's namespaces leave

335
00:12:24,318 --> 00:12:26,039
您在下面的想法让您对为什么使用名称空间，为什么不使用命名空间的想法
your thoughts below leave your thoughts

336
00:12:26,240 --> 00:12:27,929
您在下面的想法让您对为什么使用名称空间，为什么不使用命名空间的想法
on why you use namespaces why you don't

337
00:12:28,129 --> 00:12:29,308
就像在主要空间使用您将不会使用的所有空间
like using main spaces where you would

338
00:12:29,509 --> 00:12:30,389
就像在主要空间使用您将不会使用的所有空间
where you wouldn't all that kind of

339
00:12:30,589 --> 00:12:31,919
我认为很明显，在编写代码时，您应该
stuff I think it's pretty clear that

340
00:12:32,120 --> 00:12:33,808
我认为很明显，在编写代码时，您应该
when you are writing code you should be

341
00:12:34,009 --> 00:12:36,209
如果是任何严肃的项目，请在名称空间后面编写它
writing it behind a namespace if it's

342
00:12:36,409 --> 00:12:39,839
如果是任何严肃的项目，请在名称空间后面编写它
any kind of serious project if you guys

343
00:12:40,039 --> 00:12:41,219
喜欢这个视频，请点击“赞”按钮，您可以帮助支持该系列
enjoyed this video please hit the like

344
00:12:41,419 --> 00:12:42,508
喜欢这个视频，请点击“赞”按钮，您可以帮助支持该系列
button you can help support the series

345
00:12:42,708 --> 00:12:44,519
前往patreon Ocampo斜杠，确定您将可以使用
by going over to patreon Ocampo slash

346
00:12:44,720 --> 00:12:46,469
前往patreon Ocampo斜杠，确定您将可以使用
that sure know you'll get access to

347
00:12:46,669 --> 00:12:49,738
早期的视频和许多其他很棒的奖励，非常感谢所有
videos early and many other awesome

348
00:12:49,938 --> 00:12:52,409
早期的视频和许多其他很棒的奖励，非常感谢所有
rewards thank you so much to all of the

349
00:12:52,610 --> 00:12:53,849
可爱的顾客帮助将这个系列带入了现实，它不会在这里
lovely patrons that helped bring this

350
00:12:54,049 --> 00:12:55,258
可爱的顾客帮助将这个系列带入了现实，它不会在这里
series to life it wouldn't be here

351
00:12:55,458 --> 00:12:57,419
没有你，我下次见
without you I will see you guys next

352
00:12:57,620 --> 00:12:59,819
没有你，我下次见
time goodbye

353
00:13:00,019 --> 00:13:14,089
[音乐]你
[Music]

354
00:13:14,289 --> 00:13:19,289
[音乐]你
you

