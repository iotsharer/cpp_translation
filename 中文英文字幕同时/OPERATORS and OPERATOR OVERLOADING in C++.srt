﻿1
00:00:00,000 --> 00:00:01,360
嘿，大家好，我叫chana，欢迎回到我的发言加号
hey what's up guys my name is a chana

2
00:00:01,560 --> 00:00:02,799
嘿，大家好，我叫chana，欢迎回到我的发言加号
and welcome back to my say plus plus

3
00:00:03,000 --> 00:00:04,209
今天的系列文章我们将讨论C ++中的运算符和运算符
series today we're gonna be talking all

4
00:00:04,410 --> 00:00:06,428
今天的系列文章我们将讨论C ++中的运算符和运算符
about operators in C++ and operator

5
00:00:06,628 --> 00:00:08,589
C ++中的重载，所以首先是什么运算符
overloading in C++ so first of all

6
00:00:08,789 --> 00:00:11,050
C ++中的重载，所以首先是什么运算符
what's an operator well an operator is

7
00:00:11,250 --> 00:00:13,210
基本上是我们通常使用的某种符号，而不是函数
basically some kind of symbol that we

8
00:00:13,410 --> 00:00:15,130
基本上是我们通常使用的某种符号，而不是函数
use usually instead of a function to

9
00:00:15,330 --> 00:00:16,929
执行一些操作，所以我不仅在谈论数学运算符
perform something so I'm not just

10
00:00:17,129 --> 00:00:18,370
执行一些操作，所以我不仅在谈论数学运算符
talking about mathematical operators

11
00:00:18,570 --> 00:00:20,589
我们有加减减法等知识
that we have such as plus minus divide

12
00:00:20,789 --> 00:00:22,629
我们有加减减法等知识
you know multiply all that kind of stuff

13
00:00:22,829 --> 00:00:24,068
但是我们还有其他一些我们经常使用的运算符
but we've also got other operators that

14
00:00:24,268 --> 00:00:25,390
但是我们还有其他一些我们经常使用的运算符
we use very commonly and we've actually

15
00:00:25,589 --> 00:00:27,579
已经使用了很多已经包括取消引用之类的东西
been using quite a few already these

16
00:00:27,778 --> 00:00:28,929
已经使用了很多已经包括取消引用之类的东西
include things like the dereference

17
00:00:29,129 --> 00:00:31,780
运算符箭头运算符加等于，我们有一个＆运算符
operator the arrow operator plus equals

18
00:00:31,980 --> 00:00:34,029
运算符箭头运算符加等于，我们有一个＆运算符
there's the ampersand operator that we

19
00:00:34,229 --> 00:00:35,858
用于内存地址，我们也一直在使用左移运算符
use for memory addresses we've also been

20
00:00:36,058 --> 00:00:37,538
用于内存地址，我们也一直在使用左移运算符
using the bit shift left operator which

21
00:00:37,738 --> 00:00:39,759
是在左尖括号中，用于将材料打印到我们的设备中
is to left angular brackets for printing

22
00:00:39,960 --> 00:00:41,439
是在左尖括号中，用于将材料打印到我们的设备中
out stuff into our set out into our

23
00:00:41,640 --> 00:00:42,820
控制台，然后我们有其他您可能不会想到的运算符
console and then we've got other

24
00:00:43,020 --> 00:00:44,288
控制台，然后我们有其他您可能不会想到的运算符
operators which you probably don't think

25
00:00:44,488 --> 00:00:46,419
作为运算符，例如new和delete，它们实际上是
of as operators at all such as new and

26
00:00:46,619 --> 00:00:48,009
作为运算符，例如new和delete，它们实际上是
delete they're actually they're also

27
00:00:48,210 --> 00:00:49,838
运算符，然后我们有完全不同的运算符
operators and then we have entirely

28
00:00:50,039 --> 00:00:52,149
运算符，然后我们有完全不同的运算符
different operators which are weird such

29
00:00:52,350 --> 00:00:54,939
作为逗号运算符，是的，逗号也可以是运算符
as the comma operator yes the comma can

30
00:00:55,140 --> 00:00:56,649
作为逗号运算符，是的，逗号也可以是运算符
also be an operator as well as the

31
00:00:56,850 --> 00:00:58,599
括号是正确的-像方括号-括号也是
parentheses yeah that's right - like

32
00:00:58,799 --> 00:01:01,268
括号是正确的-像方括号-括号也是
brackets - parentheses that's also an

33
00:01:01,469 --> 00:01:02,858
我不会列出C ++中所有可用的运算符
operator I'm not gonna list off all of

34
00:01:03,058 --> 00:01:04,509
我不会列出C ++中所有可用的运算符
the operators we have available in C++ I

35
00:01:04,709 --> 00:01:06,579
将制作有关其中一些视频的特定视频，因为他们认为
will make videos specific videos about

36
00:01:06,780 --> 00:01:09,009
将制作有关其中一些视频的特定视频，因为他们认为
some of them because they think they

37
00:01:09,209 --> 00:01:10,569
需要一些我谈论，但是如果您想查看C ++运算符的完整列表
need some talk me about but if you want

38
00:01:10,769 --> 00:01:12,278
需要一些我谈论，但是如果您想查看C ++运算符的完整列表
to see the full list of C++ operators

39
00:01:12,478 --> 00:01:13,988
的存在，那么我在下面的描述中链接了一个参考，因此
that exist then I've linked a reference

40
00:01:14,188 --> 00:01:15,549
的存在，那么我在下面的描述中链接了一个参考，因此
in the in the description below so

41
00:01:15,750 --> 00:01:17,649
操作员或学习者，这意味着什么？
operator or the learning what does that

42
00:01:17,849 --> 00:01:19,778
操作员或学习者，这意味着什么？
mean well the time overloading in this

43
00:01:19,978 --> 00:01:21,700
感觉只是意味着给或赋予新的含义或为其添加参数
sense just means kind of giving a new

44
00:01:21,900 --> 00:01:24,808
感觉只是意味着给或赋予新的含义或为其添加参数
meaning to or adding parameters to or

45
00:01:25,009 --> 00:01:27,609
本质上是在运算符重载的情况下创建的
creating essentially in the case of

46
00:01:27,810 --> 00:01:29,289
本质上是在运算符重载的情况下创建的
operator overloading you're allowed to

47
00:01:29,489 --> 00:01:32,019
定义或更改程序中操作员的行为
define or change the behavior of an

48
00:01:32,219 --> 00:01:34,209
定义或更改程序中操作员的行为
operator in your program this is a very

49
00:01:34,409 --> 00:01:36,429
Java等语言不支持的非常有用的功能
very useful feature that isn't supported

50
00:01:36,629 --> 00:01:38,079
Java等语言不支持的非常有用的功能
in languages such as Java it is

51
00:01:38,280 --> 00:01:40,238
通常由C锋利的语言部分支持
partially supported in languages such as

52
00:01:40,438 --> 00:01:43,119
通常由C锋利的语言部分支持
C sharp usually the good parts of it are

53
00:01:43,319 --> 00:01:44,799
支持，但如果我认为C可以完全控制我们，那就是C
supported but C if I suppose kind of

54
00:01:45,000 --> 00:01:46,980
支持，但如果我认为C可以完全控制我们，那就是C
gives us full control and that's kind of

55
00:01:47,180 --> 00:01:49,778
这是一件好事，但也可能是一件坏事，这仅仅是
it's kind of a good thing but it can

56
00:01:49,978 --> 00:01:51,730
这是一件好事，但也可能是一件坏事，这仅仅是
also be a bad thing and that's just the

57
00:01:51,930 --> 00:01:53,890
C ++的情况下，它为您提供了很多控制权，但是却可能导致许多不良后果
case with C++ it gives you so much

58
00:01:54,090 --> 00:01:56,018
C ++的情况下，它为您提供了很多控制权，但是却可能导致许多不良后果
control but it can lead to so many bad

59
00:01:56,218 --> 00:01:57,579
程序和使用该语言的人
programs and people heading on the

60
00:01:57,780 --> 00:01:59,069
程序和使用该语言的人
language I might actually

61
00:01:59,269 --> 00:02:00,448
一段谈论所有这些的视频，因为我知道很多人
a video just talking about all this

62
00:02:00,649 --> 00:02:02,369
一段谈论所有这些的视频，因为我知道很多人
because I know that a lot of people are

63
00:02:02,569 --> 00:02:04,259
对此感兴趣，但归根结底，运算符只是他们所要使用的功能
interested in that but at the end of the

64
00:02:04,459 --> 00:02:06,689
对此感兴趣，但归根结底，运算符只是他们所要使用的功能
day operators are just functions they're

65
00:02:06,890 --> 00:02:08,429
只是功能而不是提供您的功能和名称（例如广告）
just functions instead of giving your

66
00:02:08,628 --> 00:02:10,920
只是功能而不是提供您的功能和名称（例如广告）
function and name such as ad you can

67
00:02:11,120 --> 00:02:13,410
给它一个操作员，例如plus和很多真正有助于清洁的箱子
give it an operator such as plus and a

68
00:02:13,610 --> 00:02:15,330
给它一个操作员，例如plus和很多真正有助于清洁的箱子
lot of cases that really helps to clean

69
00:02:15,530 --> 00:02:16,679
在streamliner中添加代码，它看起来会更好，而且
up your code in streamliner

70
00:02:16,878 --> 00:02:18,480
在streamliner中添加代码，它看起来会更好，而且
it'll just it just looks better and it's

71
00:02:18,680 --> 00:02:20,789
更容易阅读，但是在其他情况下，如果您使用＆运算符
easier to read however in other cases if

72
00:02:20,989 --> 00:02:23,610
更容易阅读，但是在其他情况下，如果您使用＆运算符
you're using the ampersand operator to

73
00:02:23,810 --> 00:02:27,539
将变量推入数据集您正在做什么，看您如何提升
push a variable into a data set what are

74
00:02:27,739 --> 00:02:29,300
将变量推入数据集您正在做什么，看您如何提升
you doing looking at you boost

75
00:02:29,500 --> 00:02:31,590
真正在您使用运算符重载的一天结束时进行序列化
serialization really at the end of the

76
00:02:31,789 --> 00:02:33,240
真正在您使用运算符重载的一天结束时进行序列化
day your use of operator overloading

77
00:02:33,439 --> 00:02:35,819
应该是最小的，只有在合理的情况下
should be rather minimal and only in

78
00:02:36,019 --> 00:02:38,009
应该是最小的，只有在合理的情况下
cases where it makes perfect sense if

79
00:02:38,209 --> 00:02:40,349
人们需要转到您的运算符的定义或您的定义
people need to go to the definition of

80
00:02:40,549 --> 00:02:42,390
人们需要转到您的运算符的定义或您的定义
your operator or the definition of your

81
00:02:42,590 --> 00:02:44,969
类或结构或其他任何东西来查看其实际功能，那么您已经
class or struct or whatever to see what

82
00:02:45,169 --> 00:02:47,789
类或结构或其他任何东西来查看其实际功能，那么您已经
it actually does then then you've

83
00:02:47,989 --> 00:02:49,560
例如在定义数学课时可能失败，并且您需要
probably failed for example when

84
00:02:49,759 --> 00:02:51,030
例如在定义数学课时可能失败，并且您需要
defining a maths class and you need to

85
00:02:51,229 --> 00:02:52,800
将两个数学对象加在一起而不是重载plus运算符
add two mathematical objects together

86
00:02:53,000 --> 00:02:55,200
将两个数学对象加在一起而不是重载plus运算符
than overloading the plus operator makes

87
00:02:55,400 --> 00:02:56,819
完美的意义，因为您可以按字面意义编写类似加号B的代码，
perfect sense because you can literally

88
00:02:57,019 --> 00:02:59,099
完美的意义，因为您可以按字面意义编写类似加号B的代码，
write code like a plus B and it will

89
00:02:59,299 --> 00:03:00,810
工作，实际上，让我们看一些例子，我将对两个例子下注
work in fact let's take a look at some

90
00:03:01,009 --> 00:03:03,118
工作，实际上，让我们看一些例子，我将对两个例子下注
examples I'm going to write a bet to two

91
00:03:03,318 --> 00:03:04,950
因此，基本上，这将是更好的选择，我决定
struts so basically what this is going

92
00:03:05,150 --> 00:03:07,439
因此，基本上，这将是更好的选择，我决定
to be is it better to it I've decided to

93
00:03:07,639 --> 00:03:09,090
使其成为一个结构，因为这些字段将是公共的，它将具有
make it a struct because the fields are

94
00:03:09,289 --> 00:03:10,950
使其成为一个结构，因为这些字段将是公共的，它将具有
going to be public it's going to have

95
00:03:11,150 --> 00:03:12,689
两个浮点数和X不，为什么所有的向量都是2，是两个分量
two floats and X no why that's all the

96
00:03:12,889 --> 00:03:13,860
两个浮点数和X不，为什么所有的向量都是2，是两个分量
vector two is it's a two component

97
00:03:14,060 --> 00:03:15,990
向量我将为此快速定义一个构造函数
vector I'm going to define a constructor

98
00:03:16,189 --> 00:03:18,868
向量我将为此快速定义一个构造函数
quickly for this which just takes in an

99
00:03:19,068 --> 00:03:21,840
X和Y，现在就像我的意思是，我们实际上可以开始创建这些
X and a Y and now it's like I mean we

100
00:03:22,039 --> 00:03:23,069
X和Y，现在就像我的意思是，我们实际上可以开始创建这些
can actually start to create these

101
00:03:23,269 --> 00:03:24,960
因此，我们只想说我想存储一个位置或速度
things so let's just say that I want to

102
00:03:25,159 --> 00:03:27,300
因此，我们只想说我想存储一个位置或速度
store a position and maybe a speed as

103
00:03:27,500 --> 00:03:29,879
好吧，太好了，我有两个向量，现在我想添加它们
well okay great so I've got two vectors

104
00:03:30,079 --> 00:03:31,110
好吧，太好了，我有两个向量，现在我想添加它们
and now I would like to add them

105
00:03:31,310 --> 00:03:33,360
一起拖延结果在这里说，所以我来解决这个问题
together and stall the results say over

106
00:03:33,560 --> 00:03:35,610
一起拖延结果在这里说，所以我来解决这个问题
here so I come to the problem of what do

107
00:03:35,810 --> 00:03:37,319
我写的语言很好，没有操作符重载，或者您使用的是C ++
I write well in a language without

108
00:03:37,519 --> 00:03:39,539
我写的语言很好，没有操作符重载，或者您使用的是C ++
operator overloading or if you're in C++

109
00:03:39,739 --> 00:03:40,950
但您不想使用运算符重载，您可能会写
but you don't want to use operator

110
00:03:41,150 --> 00:03:42,780
但您不想使用运算符重载，您可能会写
overloading and you would probably write

111
00:03:42,979 --> 00:03:44,849
像位置点之类的东西会增加速度，所以如果我走到这里，我们将快速定义
something like position dot add a speed

112
00:03:45,049 --> 00:03:46,649
像位置点之类的东西会增加速度，所以如果我走到这里，我们将快速定义
so if I go up here we'll quickly define

113
00:03:46,848 --> 00:03:47,969
add函数将返回一个全新的vector2，我们将其称为
the add function this is going to return

114
00:03:48,169 --> 00:03:50,009
add函数将返回一个全新的vector2，我们将其称为
a brand new vector2 we'll call the

115
00:03:50,209 --> 00:03:51,899
函数添加它将要接收一个现有向量，两个将通过
function add it's going to take in an

116
00:03:52,098 --> 00:03:53,520
函数添加它将要接收一个现有向量，两个将通过
existing vector two will pass this by

117
00:03:53,719 --> 00:03:56,069
避免复制的常量将函数标记为常量
constants to avoid copying well mark the

118
00:03:56,269 --> 00:03:57,960
避免复制的常量将函数标记为常量
function as constants it's not going to

119
00:03:58,159 --> 00:03:59,460
修改这个类，它将创建一个新的向量二，其结果
modify this class it's just going to

120
00:03:59,659 --> 00:04:01,110
修改这个类，它将创建一个新的向量二，其结果
create a new vector two with the result

121
00:04:01,310 --> 00:04:03,450
我们将其返回两个x加其他点X
and we'll just return it back to two x

122
00:04:03,650 --> 00:04:04,969
我们将其返回两个x加其他点X
plus other dot X

123
00:04:05,169 --> 00:04:07,430
以及为什么加上其他狗，为什么好看似乎很简单，我们已经完成了这项工作
and why plus other dog why okay seems

124
00:04:07,629 --> 00:04:08,900
以及为什么加上其他狗，为什么好看似乎很简单，我们已经完成了这项工作
pretty simple we've got this working

125
00:04:09,099 --> 00:04:12,110
在这里看起来相对还好，但是如果我们想通过某些方式来修改速度怎么办
here it looks relatively okay but what

126
00:04:12,310 --> 00:04:14,719
在这里看起来相对还好，但是如果我们想通过某些方式来修改速度怎么办
if we wanted to modify our speed by some

127
00:04:14,919 --> 00:04:16,520
一种修饰符，我们可能已经加电，将我们的速度更改为
kind of modifier we might have had a

128
00:04:16,720 --> 00:04:18,770
一种修饰符，我们可能已经加电，将我们的速度更改为
powerup that changes our speed to be

129
00:04:18,970 --> 00:04:21,230
快一点，也许快10％或类似的速度
slightly faster maybe 10% faster or

130
00:04:21,430 --> 00:04:22,100
快一点，也许快10％或类似的速度
something like that

131
00:04:22,300 --> 00:04:23,509
突然之间，我们希望能够做一些事情，例如速度倍增
suddenly we want to be able to do

132
00:04:23,709 --> 00:04:26,389
突然之间，我们希望能够做一些事情，例如速度倍增
something like speed times tower up so

133
00:04:26,589 --> 00:04:28,460
在当前设置下，这意味着我们必须编写速度倍增的代码
with this current set that means that we

134
00:04:28,660 --> 00:04:30,889
在当前设置下，这意味着我们必须编写速度倍增的代码
have to write code like speed multiplied

135
00:04:31,089 --> 00:04:33,199
加电，我们必须添加乘法功能，所以我将要复制并
power up we have to add the multiply

136
00:04:33,399 --> 00:04:34,670
加电，我们必须添加乘法功能，所以我将要复制并
function so I'm just going to copy and

137
00:04:34,870 --> 00:04:36,740
粘贴此添加函数，将其更改为相同的乘数，然后更改
paste this add function change it to

138
00:04:36,939 --> 00:04:38,509
粘贴此添加函数，将其更改为相同的乘数，然后更改
same multiplier and just change the

139
00:04:38,709 --> 00:04:41,329
运算符在这里成为乘法运算符好吧，现在很酷
operators here to be the multiplication

140
00:04:41,529 --> 00:04:44,750
运算符在这里成为乘法运算符好吧，现在很酷
operator okay cool now this is where it

141
00:04:44,949 --> 00:04:46,759
开始看起来有点难以阅读，但不幸的是使用了诸如
starts to look a little bit hard to read

142
00:04:46,959 --> 00:04:49,009
开始看起来有点难以阅读，但不幸的是使用了诸如
and unfortunately in language such as

143
00:04:49,209 --> 00:04:52,250
Java确实是您唯一的选择，但在C ++中我们有运算符重载
Java this really is your only choice but

144
00:04:52,449 --> 00:04:53,930
Java确实是您唯一的选择，但在C ++中我们有运算符重载
in C++ we have operator overloading

145
00:04:54,129 --> 00:04:55,100
这意味着我们可以利用这些运算符并实际定义
which means that we can take advantage

146
00:04:55,300 --> 00:04:57,290
这意味着我们可以利用这些运算符并实际定义
of those operators and actually define

147
00:04:57,490 --> 00:04:59,120
我们自己去处理构造的向量，所以有可能代替编写
our own to deal with the vector to

148
00:04:59,319 --> 00:05:01,218
我们自己去处理构造的向量，所以有可能代替编写
struct so potentially instead of writing

149
00:05:01,418 --> 00:05:03,710
这样的事情我们可以将其转换为仅使用数学运算符
something like this we can convert this

150
00:05:03,910 --> 00:05:05,750
这样的事情我们可以将其转换为仅使用数学运算符
to just use the mathematical operators

151
00:05:05,949 --> 00:05:07,400
因此我们通常会与操作员一起执行位置加速度乘以功率之类的操作
so we'll do something like position plus

152
00:05:07,600 --> 00:05:11,000
因此我们通常会与操作员一起执行位置加速度乘以功率之类的操作
speed times power often with operator

153
00:05:11,199 --> 00:05:12,860
在这里发挥作用的优先级，应该在此之前得到评估，因此
precedence playing a role here this

154
00:05:13,060 --> 00:05:15,079
在这里发挥作用的优先级，应该在此之前得到评估，因此
should get evaluated before this so

155
00:05:15,279 --> 00:05:16,759
让我们在这里弹出并定义真正的运算符乘法运算符
let's pop up here and define true

156
00:05:16,959 --> 00:05:18,560
让我们在这里弹出并定义真正的运算符乘法运算符
operators the multiplication operator

157
00:05:18,759 --> 00:05:20,329
只是一个星号，这个加法运算符是加号
which is just an asterisk and this

158
00:05:20,529 --> 00:05:21,770
只是一个星号，这个加法运算符是加号
addition operator which is the plus sign

159
00:05:21,970 --> 00:05:23,360
所以我们定义这些的方式就像其他任何函数一样，我们编写return
so the way that we define these is like

160
00:05:23,560 --> 00:05:25,189
所以我们定义这些的方式就像其他任何函数一样，我们编写return
any other function we write the return

161
00:05:25,389 --> 00:05:26,990
结果，当然返回类型将完全相同
result which of course is going to be

162
00:05:27,189 --> 00:05:28,879
结果，当然返回类型将完全相同
the exact same the return type is going

163
00:05:29,079 --> 00:05:30,560
与这些功能相同，因此对于我们的案例，我们正在编写
to be the same as it is for these

164
00:05:30,759 --> 00:05:31,910
与这些功能相同，因此对于我们的案例，我们正在编写
functions so for our case we're writing

165
00:05:32,110 --> 00:05:34,100
在这里，让我们移动这个PS，我们可以将它们分组
out over here let's move this our PS

166
00:05:34,300 --> 00:05:35,689
在这里，让我们移动这个PS，我们可以将它们分组
that we can kind of group them instead

167
00:05:35,889 --> 00:05:37,340
在函数名称中，我们写单词operator，然后是operator，
of the function name we write the word

168
00:05:37,540 --> 00:05:39,439
在函数名称中，我们写单词operator，然后是operator，
operator followed by the operator which

169
00:05:39,639 --> 00:05:42,379
是加号，然后我们打开括号并输入所需的参数
is plus we then open up our parentheses

170
00:05:42,579 --> 00:05:43,939
是加号，然后我们打开括号并输入所需的参数
and type in the parameter that we want

171
00:05:44,139 --> 00:05:45,439
所以实际上您可以看到它完全一样，然后我们在最后写出费用
so really you can see it's exactly the

172
00:05:45,639 --> 00:05:47,210
所以实际上您可以看到它完全一样，然后我们在最后写出费用
same and then we write cost at the end

173
00:05:47,410 --> 00:05:48,410
之所以这样，是因为像其他任何不修改此类的函数一样
of this because like any other function

174
00:05:48,610 --> 00:05:49,730
之所以这样，是因为像其他任何不修改此类的函数一样
that doesn't modify a class like this

175
00:05:49,930 --> 00:05:51,680
添加函数，我们仍然希望它成为Const，然后我们可以做的是
add function we still we still want it

176
00:05:51,879 --> 00:05:53,360
添加函数，我们仍然希望它成为Const，然后我们可以做的是
to be Const and then what we can do is

177
00:05:53,560 --> 00:05:55,160
只是返回相同的结果，否则在这种情况下
just return the same result or in this

178
00:05:55,360 --> 00:05:56,900
只是返回相同的结果，否则在这种情况下
case it would be much simpler to just

179
00:05:57,100 --> 00:05:59,150
返回dhaba，就是这样，如果我们来的话，我们已经创建了plus运算符
return a dhaba and that's it we've

180
00:05:59,350 --> 00:06:00,680
返回dhaba，就是这样，如果我们来的话，我们已经创建了plus运算符
created the plus operator if we come

181
00:06:00,879 --> 00:06:02,389
回到这里，我们只是注释掉这段代码的其余部分，您可以看到
back here and we're just comment out the

182
00:06:02,589 --> 00:06:04,009
回到这里，我们只是注释掉这段代码的其余部分，您可以看到
rest of this code you can see that this

183
00:06:04,209 --> 00:06:05,960
代码很好，我不会忘记我们可以对其进行编译，并且它将编译并
code is fine I don't forget we can

184
00:06:06,160 --> 00:06:07,310
代码很好，我不会忘记我们可以对其进行编译，并且它将编译并
compile this and it will compile and

185
00:06:07,509 --> 00:06:09,588
现在一切都很棒，因为事情就像其他功能一样
everything is great now since things are

186
00:06:09,788 --> 00:06:10,809
现在一切都很棒，因为事情就像其他功能一样
like any other function

187
00:06:11,009 --> 00:06:12,040
我基本上可以做到这一点的相反，而不是这个运算符调用
I could have basically done the reverse

188
00:06:12,240 --> 00:06:13,959
我基本上可以做到这一点的相反，而不是这个运算符调用
of this instead of this operator calling

189
00:06:14,158 --> 00:06:15,939
我可以使add函数调用plus运算符和一个add函数
the add function I could make the add

190
00:06:16,139 --> 00:06:17,949
我可以使add函数调用plus运算符和一个add函数
function call the plus operator and a

191
00:06:18,149 --> 00:06:19,028
实际上很多人对此一无所知，因为语法看起来有点
lot of people actually don't know about

192
00:06:19,228 --> 00:06:20,528
实际上很多人对此一无所知，因为语法看起来有点
this because the syntax looks a bit

193
00:06:20,728 --> 00:06:22,299
很奇怪，您不会经常看到这种情况，但是您所能做的就是改变
weird and you don't see this very often

194
00:06:22,499 --> 00:06:24,399
很奇怪，您不会经常看到这种情况，但是您所能做的就是改变
but what you could do is just change

195
00:06:24,598 --> 00:06:28,299
这基本上就是那个，然后在这里添加，我们将要么返回
this to be basically that and then over

196
00:06:28,499 --> 00:06:30,278
这基本上就是那个，然后在这里添加，我们将要么返回
here in add we're going to either return

197
00:06:30,478 --> 00:06:33,459
而且，尽管这是一种简单的情况，我想我们正在转换
and this plus although which is kind of

198
00:06:33,658 --> 00:06:35,139
而且，尽管这是一种简单的情况，我想我们正在转换
the easy case I guess we're converting

199
00:06:35,338 --> 00:06:37,059
我们真的需要为此制作视频，因为this关键字是
this we really need to make a video on

200
00:06:37,259 --> 00:06:39,759
我们真的需要为此制作视频，因为this关键字是
this because the this keyword is is

201
00:06:39,959 --> 00:06:41,410
非常特别，我们需要谈论的是那里会有一张双卡
quite special and we need to talk about

202
00:06:41,610 --> 00:06:43,929
非常特别，我们需要谈论的是那里会有一张双卡
that there'll be a double card up there

203
00:06:44,129 --> 00:06:45,129
如果已经引用了，是否已经完成，因为这是
if that's already done whether you're

204
00:06:45,329 --> 00:06:46,239
如果已经引用了，是否已经完成，因为这是
referencing it because this is

205
00:06:46,439 --> 00:06:48,069
在这种情况下，本来就是一个点或常量指针
originally a point or a constant pointer

206
00:06:48,269 --> 00:06:49,569
在这种情况下，本来就是一个点或常量指针
in this case whether you're referencing

207
00:06:49,769 --> 00:06:51,399
它只是将其变成一个法线向量，然后我们将其添加
it to just turn this into a normal

208
00:06:51,598 --> 00:06:52,540
它只是将其变成一个法线向量，然后我们将其添加
vector to and then we're just adding it

209
00:06:52,740 --> 00:06:54,399
与我们在一起，但这是很普通的人写这样的代码
with our but that's pretty common people

210
00:06:54,598 --> 00:06:55,239
与我们在一起，但这是很普通的人写这样的代码
write code like that

211
00:06:55,439 --> 00:06:57,249
很多很多，但是我们能做的就是解决这个运算符，再加上像
a lot but what we can do is just address

212
00:06:57,449 --> 00:06:59,439
很多很多，但是我们能做的就是解决这个运算符，再加上像
this operator plus like a function by

213
00:06:59,639 --> 00:07:01,778
只是写返回运算符加，然后其他再次看起来有点奇怪
just writing return operator plus and

214
00:07:01,978 --> 00:07:04,119
只是写返回运算符加，然后其他再次看起来有点奇怪
then other again it looks a bit weird

215
00:07:04,319 --> 00:07:06,129
但是完全可以编译并且完全可以工作
but totally going to compile and it

216
00:07:06,329 --> 00:07:06,879
但是完全可以编译并且完全可以工作
totally works

217
00:07:07,079 --> 00:07:09,609
我本人只是去做样式，我喜欢返回这样的代码
me personally do to just go style I like

218
00:07:09,809 --> 00:07:12,629
我本人只是去做样式，我喜欢返回这样的代码
to return the code like this instead

219
00:07:12,829 --> 00:07:14,980
好的，很酷，要完成此操作，我们将添加乘法运算符，然后将其复制
okay cool so to complete this we'll just

220
00:07:15,180 --> 00:07:17,079
好的，很酷，要完成此操作，我们将添加乘法运算符，然后将其复制
add our multiply operator I'll copy this

221
00:07:17,278 --> 00:07:19,869
运算符加上它在这里并将其更改为运算符x或运算符
operator plus pass it over here and

222
00:07:20,069 --> 00:07:23,019
运算符加上它在这里并将其更改为运算符x或运算符
change this to be operator x or operator

223
00:07:23,218 --> 00:07:24,670
乘数，如果我们取消注释，则将其更改为现在乘
multiplier and we'll change this to say

224
00:07:24,870 --> 00:07:27,459
乘数，如果我们取消注释，则将其更改为现在乘
multiply now if we uncomment this then

225
00:07:27,658 --> 00:07:29,350
您可以看到我们这里有这段代码，我认为这看起来很多
you can see that we have this code here

226
00:07:29,550 --> 00:07:31,689
您可以看到我们这里有这段代码，我认为这看起来很多
which in my opinion looks much much much

227
00:07:31,889 --> 00:07:33,309
比这更好，它在很大的意义上还可以，所以就像我说的那样
better than this and it makes a lot more

228
00:07:33,509 --> 00:07:35,920
比这更好，它在很大的意义上还可以，所以就像我说的那样
sense okay great so as I said I'm not

229
00:07:36,120 --> 00:07:36,699
将要经历每个操作员，因为这将需要一整天
going to go through every single

230
00:07:36,899 --> 00:07:38,230
将要经历每个操作员，因为这将需要一整天
operator because that would take all day

231
00:07:38,430 --> 00:07:40,329
可能没什么用，我以后会制作关于运营商的视频
and it's probably not that useful I will

232
00:07:40,528 --> 00:07:42,459
可能没什么用，我以后会制作关于运营商的视频
make future videos on operators as we

233
00:07:42,658 --> 00:07:44,889
使用它们，或者按我认为合适的方式使用，但是我会再向您展示一个运算符，
use them or as I see fit however I will

234
00:07:45,088 --> 00:07:46,149
使用它们，或者按我认为合适的方式使用，但是我会再向您展示一个运算符，
show you one more operator and that's

235
00:07:46,348 --> 00:07:47,980
我们与SC BC一起使用的左移运算符现在看起来很惊讶
the left shift kind of operator that we

236
00:07:48,180 --> 00:07:50,559
我们与SC BC一起使用的左移运算符现在看起来很惊讶
use with SC BC out so surprised look now

237
00:07:50,759 --> 00:07:52,269
我们有这个向量二，我们想将其实际打印到控制台
that we've got this vector two we want

238
00:07:52,468 --> 00:07:53,679
我们有这个向量二，我们想将其实际打印到控制台
to actually print it to the console

239
00:07:53,879 --> 00:07:56,079
您可能已经注意到的esidisi房子有这种转移
esidisi house as you probably would have

240
00:07:56,278 --> 00:07:58,449
您可能已经注意到的esidisi房子有这种转移
noticed has this kind of shift left

241
00:07:58,649 --> 00:08:01,600
在左侧带有各种类型的运算符，我们有CR类和
operator which takes in various types at

242
00:08:01,800 --> 00:08:03,459
在左侧带有各种类型的运算符，我们有CR类和
the left side we've got the CR class and

243
00:08:03,658 --> 00:08:05,290
在右侧，我们具有某种数据类型，因此我们将输入并得出两个
of the right side we've got some kind of

244
00:08:05,490 --> 00:08:07,299
在右侧，我们具有某种数据类型，因此我们将输入并得出两个
data types so we'll type and result two

245
00:08:07,499 --> 00:08:09,670
这是那个向量，然后我们现在就可以做到这一点
which is that vector and then we'll

246
00:08:09,870 --> 00:08:11,829
这是那个向量，然后我们现在就可以做到这一点
we were just able to do this now we

247
00:08:12,029 --> 00:08:13,600
无法执行此操作，因为此操作符没有重载
can't do this because there is no

248
00:08:13,800 --> 00:08:15,490
无法执行此操作，因为此操作符没有重载
overload for this operator which takes

249
00:08:15,689 --> 00:08:17,710
在输出流中，这是CRT，然后是实际矢量，但是我们可以
in an output stream which is what CRTs

250
00:08:17,910 --> 00:08:20,800
在输出流中，这是CRT，然后是实际矢量，但是我们可以
and then an actual vector too but we can

251
00:08:21,000 --> 00:08:22,840
在向量之外将其添加到类中，因为与该类无关
add that so outside of the vector to a

252
00:08:23,040 --> 00:08:24,069
在向量之外将其添加到类中，因为与该类无关
class because there's nothing to do with

253
00:08:24,269 --> 00:08:25,509
向量，我们只是将其添加以了解我们将要做的是编写STD或
vector we're just adding this to see out

254
00:08:25,709 --> 00:08:28,720
向量，我们只是将其添加以了解我们将要做的是编写STD或
what we're going to do is write STD or a

255
00:08:28,920 --> 00:08:30,310
流代表参考输出流，所以这是原始流
stream which stands for output streams

256
00:08:30,509 --> 00:08:31,810
流代表参考输出流，所以这是原始流
of reference so this is the original

257
00:08:32,009 --> 00:08:34,179
这种操作符的定义，我将在这里重载，
kind of definition of this operator that

258
00:08:34,379 --> 00:08:35,559
这种操作符的定义，我将在这里重载，
I'm overloading here we're going to

259
00:08:35,759 --> 00:08:38,740
写操作符在这里向左移，我们将上课，因为
write operator left shift inside here

260
00:08:38,940 --> 00:08:41,319
写操作符在这里向左移，我们将上课，因为
we're going to take in the class because

261
00:08:41,519 --> 00:08:42,879
您会看到这是类之外的定义，因此我们仍然需要引用
you can see this is a definition outside

262
00:08:43,080 --> 00:08:44,469
您会看到这是类之外的定义，因此我们仍然需要引用
of class so we still need a reference to

263
00:08:44,669 --> 00:08:46,089
现有的流，在这种情况下将是C out，然后我要
the existing stream which in this case

264
00:08:46,289 --> 00:08:47,979
现有的流，在这种情况下将是C out，然后我要
is going to be C out and then I'm going

265
00:08:48,179 --> 00:08:50,229
在这里通过引用传递我的成本向量，我要说流和
to pass my cost vector by reference here

266
00:08:50,429 --> 00:08:52,599
在这里通过引用传递我的成本向量，我要说流和
over here I'm going to say stream and

267
00:08:52,799 --> 00:08:54,519
然后基本上转移我要打印的任何内容，因此在这种情况下，字母点X
then shifts basically whatever I want to

268
00:08:54,720 --> 00:08:56,829
然后基本上转移我要打印的任何内容，因此在这种情况下，字母点X
print so in this case alpha dot X the

269
00:08:57,029 --> 00:08:58,689
流已经知道如何打印调情，所以我们不需要超载
stream already knows how to print a

270
00:08:58,889 --> 00:09:00,309
流已经知道如何打印调情，所以我们不需要超载
flirt so we don't need to overload the

271
00:09:00,509 --> 00:09:01,629
调情之类的东西，可能会有逗号然后是鸟
flirt or anything like that

272
00:09:01,830 --> 00:09:03,429
调情之类的东西，可能会有逗号然后是鸟
and there may be a comma and then a bird

273
00:09:03,629 --> 00:09:05,169
或Y，并确定您的费用正确
or Y and make sure that you've cost

274
00:09:05,370 --> 00:09:05,589
或Y，并确定您的费用正确
right

275
00:09:05,789 --> 00:09:08,589
哦，在这里流，我会像我一样串起来，好吧，这就是它的样子
oh stream here and I'll string like like

276
00:09:08,789 --> 00:09:10,479
哦，在这里流，我会像我一样串起来，好吧，这就是它的样子
me okay so this is what it looks like

277
00:09:10,679 --> 00:09:12,309
最后我们需要返回对流的引用
and then finally we need to return a

278
00:09:12,509 --> 00:09:13,839
最后我们需要返回对流的引用
reference to the stream which is just

279
00:09:14,039 --> 00:09:16,149
这个流，这样我们就可以得到返回的流，就是这样，您认为这
this stream so we can just get returned

280
00:09:16,350 --> 00:09:18,159
这个流，这样我们就可以得到返回的流，就是这样，您认为这
stream and that's it you consider this

281
00:09:18,360 --> 00:09:20,709
代码现在可以编译，如果我运行此代码，我们将得到四点五五逗号
code now compiles and if I run this code

282
00:09:20,909 --> 00:09:22,689
代码现在可以编译，如果我运行此代码，我们将得到四点五五逗号
we'll get four point five five comma

283
00:09:22,889 --> 00:09:24,429
五点六五印刷当然是正确的答案
five point six five printing which of

284
00:09:24,629 --> 00:09:25,779
五点六五印刷当然是正确的答案
course is the right answer

285
00:09:25,980 --> 00:09:27,339
现在我们来看一些我们可以重载的运算符
so there we go that's a look at a few of

286
00:09:27,539 --> 00:09:28,539
现在我们来看一些我们可以重载的运算符
the operators that we can overload in

287
00:09:28,740 --> 00:09:30,370
C ++和我们只是在普通房间中工作，但它们只是功能
C++ and our just operates work in

288
00:09:30,570 --> 00:09:32,439
C ++和我们只是在普通房间中工作，但它们只是功能
general room but they're just functions

289
00:09:32,639 --> 00:09:34,389
并记住只是因为您可以重载运算符并编写代码
and remember just because you can

290
00:09:34,590 --> 00:09:36,250
并记住只是因为您可以重载运算符并编写代码
overload an operator and make your code

291
00:09:36,450 --> 00:09:39,339
看起来很奇怪，因为它会使人们更难以阅读
look weird don't because it's going to

292
00:09:39,539 --> 00:09:40,509
看起来很奇怪，因为它会使人们更难以阅读
make it harder for people to read is

293
00:09:40,710 --> 00:09:42,069
可能会惹恼你自己，这只是糟糕的代码而已
probably going to annoy you yourself and

294
00:09:42,269 --> 00:09:45,519
可能会惹恼你自己，这只是糟糕的代码而已
it's just it's just bad just bad code

295
00:09:45,720 --> 00:09:47,349
样式不要这样做，让我们转移操作符，我们只是重载了
style don't do it this let's shift

296
00:09:47,549 --> 00:09:48,969
样式不要这样做，让我们转移操作符，我们只是重载了
operator that we overloaded just kind of

297
00:09:49,169 --> 00:09:51,639
就像您通常用以下语言覆盖的两个字符串函数一样
like the two string function that you

298
00:09:51,840 --> 00:09:53,259
就像您通常用以下语言覆盖的两个字符串函数一样
commonly override in languages such as

299
00:09:53,460 --> 00:09:55,479
Java或C Sharp，所以这是C ++的一大优点
Java or C sharp and so that's one of the

300
00:09:55,679 --> 00:09:57,309
Java或C Sharp，所以这是C ++的一大优点
great things about C++ you can kind of

301
00:09:57,509 --> 00:09:59,169
有运算符而不是函数c-sharp的另一个很好的例子
have operators instead of functions

302
00:09:59,370 --> 00:10:01,149
有运算符而不是函数c-sharp的另一个很好的例子
another great example which c-sharp does

303
00:10:01,350 --> 00:10:03,459
支持但Java不是Java中的equals equals运算符
support but Java doesn't is the equals

304
00:10:03,659 --> 00:10:05,199
支持但Java不是Java中的equals equals运算符
equals operator in Java you have to

305
00:10:05,399 --> 00:10:07,120
如果您想比较每个类，请为每个类写一个equals替代
write an equals override for every class

306
00:10:07,320 --> 00:10:08,709
如果您想比较每个类，请为每个类写一个equals替代
if you want to be able to compare it and

307
00:10:08,909 --> 00:10:11,049
然后稍后您必须在所有位置写均等
then later on you have to write equals

308
00:10:11,250 --> 00:10:12,179
然后稍后您必须在所有位置写均等
everywhere so

309
00:10:12,379 --> 00:10:14,399
比较结果一个结果-我必须将其写入结果1等于结果2或
compare result one result - I'd have to

310
00:10:14,600 --> 00:10:16,829
比较结果一个结果-我必须将其写入结果1等于结果2或
write it for result 1 equals result 2 or

311
00:10:17,029 --> 00:10:18,809
类似的东西，或者如果不相等，我会写
something like that or maybe if it

312
00:10:19,009 --> 00:10:20,729
类似的东西，或者如果不相等，我会写
doesn't equal I would write that and

313
00:10:20,929 --> 00:10:21,839
看起来有点凌乱，所以我们还可以选择
that just looks a little messy so

314
00:10:22,039 --> 00:10:23,729
看起来有点凌乱，所以我们还可以选择
instead of that we also have the option

315
00:10:23,929 --> 00:10:25,649
写B等于等于运算符，这是我要告诉你的最后一个运算符
of writing B equals equals operator

316
00:10:25,850 --> 00:10:26,789
写B等于等于运算符，这是我要告诉你的最后一个运算符
that's the final one that I'll show you

317
00:10:26,990 --> 00:10:28,349
今天的人们，因为我想展示它向右流动，因为那是一个
guys for today because I I wanted to

318
00:10:28,549 --> 00:10:30,029
今天的人们，因为我想展示它向右流动，因为那是一个
show it flow right pool because that's a

319
00:10:30,230 --> 00:10:31,439
返回的已解决成本可以返回true或false
returned resolved cost can return true

320
00:10:31,639 --> 00:10:31,889
返回的已解决成本可以返回true或false
or false

321
00:10:32,090 --> 00:10:34,620
运算符等于等于const引用其他const，因为它只是
operator equals equals constitu

322
00:10:34,820 --> 00:10:37,319
运算符等于等于const引用其他const，因为它只是
reference other const because it's just

323
00:10:37,519 --> 00:10:38,639
一个比较，我们将不会修改此类，我们只是
a comparison we're not going to be

324
00:10:38,840 --> 00:10:40,409
一个比较，我们将不会修改此类，我们只是
modifying this class and we'll just

325
00:10:40,610 --> 00:10:44,789
返回x等于x和y等于其他del y，所以我们基本上
return x equals equals x and y equals

326
00:10:44,990 --> 00:10:46,259
返回x等于x和y等于其他del y，所以我们基本上
equals other del y so we're basically

327
00:10:46,460 --> 00:10:48,179
检查一下这条浮标是否与在这里向下爬行的完全一样
checking to see if this floats are

328
00:10:48,379 --> 00:10:50,429
检查一下这条浮标是否与在这里向下爬行的完全一样
exactly the same crawling down here we

329
00:10:50,629 --> 00:10:52,319
可以更改此代码以说类似结果等于等于的结果
can change this code to say something

330
00:10:52,519 --> 00:10:55,049
可以更改此代码以说类似结果等于等于的结果
like result equals equals resolved to i

331
00:10:55,250 --> 00:10:57,240
可以看到作品有很大的支持者，最后，如果我们想要一个不等于
can see that works have a big fan and

332
00:10:57,440 --> 00:10:59,639
可以看到作品有很大的支持者，最后，如果我们想要一个不等于
finally if we wanted a not equals

333
00:10:59,840 --> 00:11:01,949
运算符，我们通过复制此代码将其更改为不等于，然后调用
operator we with a copy this code change

334
00:11:02,149 --> 00:11:04,349
运算符，我们通过复制此代码将其更改为不等于，然后调用
this to not equals and then just call

335
00:11:04,549 --> 00:11:06,329
基本上是等号的对立面，所以我们称此等号等于其他，但
basically the opposite of equals so

336
00:11:06,529 --> 00:11:09,179
基本上是等号的对立面，所以我们称此等号等于其他，但
we'll call this equals equals other but

337
00:11:09,379 --> 00:11:10,949
我们将其放在括号中，并在前面加上感叹号以进行反转
we'll put this in parenthesis with an

338
00:11:11,149 --> 00:11:13,199
我们将其放在括号中，并在前面加上感叹号以进行反转
exclamation mark at the front to reverse

339
00:11:13,399 --> 00:11:16,169
结果，或者我向您展示的那种语法
the result of that alternatively with

340
00:11:16,370 --> 00:11:17,549
结果，或者我向您展示的那种语法
the kind of syntax that I showed you

341
00:11:17,750 --> 00:11:19,469
早些时候，您可以编写类似return操作符等于other和
earlier you could write something like

342
00:11:19,669 --> 00:11:22,229
早些时候，您可以编写类似return操作符等于other和
return operator equals equals other and

343
00:11:22,429 --> 00:11:24,149
然后将结果反转并返回看起来很奇怪的结果
then kind of reverse that result and

344
00:11:24,350 --> 00:11:25,769
然后将结果反转并返回看起来很奇怪的结果
return that that looks weird

345
00:11:25,970 --> 00:11:27,809
如果您以这种方式这样做，那就不要这样做
don't do it that way if you are doing it

346
00:11:28,009 --> 00:11:29,159
如果您以这种方式这样做，那就不要这样做
this way which you probably should

347
00:11:29,360 --> 00:11:31,559
假装很好，或者只是拥有一个equals函数并调用
pretend this well maybe alternatively

348
00:11:31,759 --> 00:11:33,779
假装很好，或者只是拥有一个equals函数并调用
just have an equals function and call

349
00:11:33,980 --> 00:11:35,250
填充并返回相反或类似的东西，你再次拥有这些
pad and return the reverse of that or

350
00:11:35,450 --> 00:11:37,259
填充并返回相反或类似的东西，你再次拥有这些
something like that again you have these

351
00:11:37,460 --> 00:11:38,879
奥普拉的意思是有些人在创建我喜欢的图书馆时不喜欢使用它们
oprah's that some people prefer not to

352
00:11:39,080 --> 00:11:41,789
奥普拉的意思是有些人在创建我喜欢的图书馆时不喜欢使用它们
use them when creating a library I like

353
00:11:41,990 --> 00:11:43,439
增加出生，所以我仍然会像在这里一样
to add birth so I would still have a

354
00:11:43,639 --> 00:11:45,959
增加出生，所以我仍然会像在这里一样
like like I did over here I would still

355
00:11:46,159 --> 00:11:48,269
有这个添加功能，但我也有一个运算符，所以你基本上有
have this add function but I would also

356
00:11:48,470 --> 00:11:50,609
有这个添加功能，但我也有一个运算符，所以你基本上有
have an operator so you've basically got

357
00:11:50,809 --> 00:11:51,870
您的农民竭尽所能，使任何使用我们的API选择的人
your peasant your best for giving anyone

358
00:11:52,070 --> 00:11:53,819
您的农民竭尽所能，使任何使用我们的API选择的人
using our API choices as to what they

359
00:11:54,019 --> 00:11:55,559
希望希望大家喜欢这个视频，如果您能做到的话
want to use hope you guys enjoy this

360
00:11:55,759 --> 00:11:57,059
希望希望大家喜欢这个视频，如果您能做到的话
video if you did you can hit that like

361
00:11:57,259 --> 00:11:58,620
按钮，您可以通过出门支持该系列
button and you can support this series

362
00:11:58,820 --> 00:11:59,399
按钮，您可以通过出门支持该系列
by heading out

363
00:11:59,600 --> 00:12:02,399
我在这样的椅子上回家，听说有人早点录制视频
I am home for such the chair I hear that

364
00:12:02,600 --> 00:12:04,109
我在这样的椅子上回家，听说有人早点录制视频
people they are getting videos early

365
00:12:04,309 --> 00:12:06,118
就像每一天都很酷，所以如果您想跳下去，
like every day which is pretty cool so

366
00:12:06,318 --> 00:12:07,349
就像每一天都很酷，所以如果您想跳下去，
if you wanted if you want to jump on

367
00:12:07,549 --> 00:12:09,029
这趟火车比任何人都早，可以早点看视频
this train early earlier than anyone

368
00:12:09,230 --> 00:12:10,740
这趟火车比任何人都早，可以早点看视频
else and get to see the videos early as

369
00:12:10,940 --> 00:12:11,609
我完成编辑后尽快
soon as possible

370
00:12:11,809 --> 00:12:13,198
我完成编辑后尽快
as soon as I'm done editing them they go

371
00:12:13,399 --> 00:12:15,149
你们要不要坐那辆火车，然后在描述中链接
up on patreon do you guys want to get on

372
00:12:15,350 --> 00:12:16,799
你们要不要坐那辆火车，然后在描述中链接
that train then link in the description

373
00:12:17,000 --> 00:12:18,568
在下面，您还可以确保我有更多时间制作这些视频
below you're also ensuring that I have

374
00:12:18,769 --> 00:12:20,758
在下面，您还可以确保我有更多时间制作这些视频
more time to make these videos which

375
00:12:20,958 --> 00:12:23,008
产生了更多的视频，这对整个家庭都很有趣，接下来我会再见到你们
results in more videos so fun for the

376
00:12:23,208 --> 00:12:24,899
产生了更多的视频，这对整个家庭都很有趣，接下来我会再见到你们
whole family I will see you guys next

377
00:12:25,100 --> 00:12:25,349
时间再见
time

378
00:12:25,549 --> 00:12:27,579
时间再见
good bye

379
00:12:27,779 --> 00:12:32,779
[音乐]
[Music]

