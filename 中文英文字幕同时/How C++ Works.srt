﻿1
00:00:00,030 --> 00:00:04,000
hey what's up guys my name is the Cherno welcome to another episode of my people
嘿，大家好，我叫切尔诺，欢迎您再见我的一族

2
00:00:04,200 --> 00:00:07,750
are plus theory so today we're going to learn all about house people as plus
是加理论，所以今天我们将学习所有关于房屋的人

3
00:00:07,950 --> 00:00:11,319
works we're going to kind of try and keep it simple for now but we're going
作品，我们现在将尝试使其保持简单，但我们将

4
00:00:11,519 --> 00:00:16,600
to learn about how we go from the source file the actual text file to an actual
了解我们如何将源文件从实际文本文件转换为实际文件

5
00:00:16,800 --> 00:00:20,949
executable binary or program that we can run the basic workflow of writing a
可执行的二进制文件或程序，我们可以运行编写程序的基本工作流程

6
00:00:21,149 --> 00:00:25,359
simple sub program is you have a series of source files which you write actual
简单的子程序是您拥有一系列实际编写的源文件

7
00:00:25,559 --> 00:00:29,978
text ins and then you pass it through a compiler which compiles it into some
文本ins，然后将其传递给编译器，该编译器将其编译为一些

8
00:00:30,178 --> 00:00:33,788
kind of binary now that binary can be some sort of library or it can be an
现在二进制可以是某种库，也可以是

9
00:00:33,988 --> 00:00:36,939
actual executable program today we're going to talk specifically about
今天的实际可执行程序，我们将专门讨论

10
00:00:37,140 --> 00:00:41,529
executable programs or executable binary so let's hop on over to visual studio
可执行程序或可执行二进制文件，让我们跳到Visual Studio 

11
00:00:41,729 --> 00:00:45,518
and check it out ok so here we have our hello world application that we wrote in
然后检查一下，好了，所以这里有我们编写的hello world应用程序

12
00:00:45,719 --> 00:00:49,088
the previous video when we learn how to set up C++ on Windows it's a pretty
上一个视频，当我们学习如何在Windows上设置C ++时， 

13
00:00:49,289 --> 00:00:52,599
basic program but there are quite a number of things going on here first of
基本程序，但首先要做很多事情

14
00:00:52,799 --> 00:00:55,899
all we have this include iostream statement now this is something called a
我们拥有的所有include iostream语句现在都称为

15
00:00:56,100 --> 00:00:59,378
preprocessor statement anything that begins with a hash is a preprocessor
预处理程序语句以散列开头的任何内容均为预处理程序

16
00:00:59,579 --> 00:01:02,559
statement the first thing that a compiler does when it receives a source
声明编译器在收到源代码时要做的第一件事

17
00:01:02,759 --> 00:01:06,730
file is it pre processes all of your preprocessor statement that's why they
文件是预处理所有预处理程序语句的原因，这就是它们

18
00:01:06,930 --> 00:01:09,759
called preprocessor statements because they happen just before the actual
之所以称为预处理程序语句，是因为它们发生在实际的

19
00:01:09,959 --> 00:01:14,168
compilation in this case is something called include what include will do is
在这种情况下，编译称为“包含”，包含将执行的操作是

20
00:01:14,368 --> 00:01:18,668
find a file so in this case we're looking for a file called iostream take
找到一个文件，因此在这种情况下，我们正在寻找一个名为iostream take的文件

21
00:01:18,868 --> 00:01:22,808
all of the contents of that file and just paste it into this current file
该文件的所有内容，然后将其粘贴到当前文件中

22
00:01:23,009 --> 00:01:26,289
these files that you include are typically called header files and we'll
您包含的这些文件通常称为头文件，我们将

23
00:01:26,489 --> 00:01:29,558
discuss them more in depth as the series goes on the reason we are including
随着系列的进行，我们将更深入地讨论它们， 

24
00:01:29,759 --> 00:01:33,308
something called iostream is because we need a declaration for a function called
所谓的iostream是因为我们需要一个名为的函数的声明

25
00:01:33,509 --> 00:01:37,418
C out which lets us scream stuff to our console next we have this main function
 C让我们在控制台中大声疾呼，接下来我们有了这个主要功能

26
00:01:37,618 --> 00:01:41,140
now the main function is very important because every simplest program has
现在主要功能非常重要，因为每个最简单的程序都有

27
00:01:41,340 --> 00:01:44,769
something like this the main function is called the entry point it's the entry
像这样的主要功能称为入口点，它是入口

28
00:01:44,969 --> 00:01:48,640
point for our application that means that when we run our application our
应用程序的关键点，这意味着当我们运行应用程序时， 

29
00:01:48,840 --> 00:01:52,390
computer starts executing code that begins in this function as the program
计算机开始执行以该功能开头的程序代码

30
00:01:52,590 --> 00:01:56,349
is running our computer will execute the lines of code that we type in order of
正在运行我们的计算机将执行我们按以下顺序键入的代码行

31
00:01:56,549 --> 00:01:59,198
course there are certain things that can break or change the order of execution
当然，某些事情会破坏或改变执行顺序

32
00:01:59,399 --> 00:02:03,250
and those are primarily called control flow statements or calls to other
这些主要称为控制流语句或对其他程序的调用

33
00:02:03,450 --> 00:02:08,350
functions but the gist is that our code gets executed line by line so the first
功能，但要点是我们的代码逐行执行，因此第一个

34
00:02:08,550 --> 00:02:12,280
thing that will get executed in our application will be this hello world the
在我们的应用程序中将执行的事情将是这个世界

35
00:02:12,479 --> 00:02:15,030
hour and then the next thing is this the end
小时，然后下一件事就是结束

36
00:02:15,229 --> 00:02:18,750
gate function and then since that's all that we've got inside our main function
门功能，然后既然这就是我们主要功能中的全部内容

37
00:02:18,949 --> 00:02:22,320
our program will terminate now for those of you who are familiar with functions
我们的程序将针对那些熟悉功能的人而终止

38
00:02:22,520 --> 00:02:26,100
you might notice that the return types of main is actually int however we're
您可能会注意到main的返回类型实际上是int，但是我们

39
00:02:26,300 --> 00:02:29,640
not returning an integer that's because the main function is actually a special
不返回整数，这是因为main函数实际上是一个特殊的

40
00:02:29,840 --> 00:02:33,900
case you don't have to return any kind of value from the main function if you
如果您不需要从主函数返回任何类型的值

41
00:02:34,099 --> 00:02:37,200
don't return anything it will assume that your returning is zero this only
不要返回任何东西，它会假设您的返回值为零

42
00:02:37,400 --> 00:02:40,110
applies to the main function there is a special case all right so let's talk a
适用于main函数，有一种特殊情况，所以让我们来谈谈

43
00:02:40,310 --> 00:02:43,439
little bit more about what this is actually doing this kind of syntax might
关于这实际上在做什么这种语法可能更多

44
00:02:43,639 --> 00:02:47,009
look strange for someone who's new to C++ and it is a little bit unfortunate
对于刚接触C ++的人来说看起来很奇怪，这有点不幸

45
00:02:47,209 --> 00:02:49,500
that it's actually written this way because it doesn't make too much sense
它实际上是这样写的，因为它没有太大意义

46
00:02:49,699 --> 00:02:53,520
when you first look at it but basically these left angular brackets which look
当您第一次查看它时，基本上这些左尖括号

47
00:02:53,719 --> 00:02:57,600
kind of like a bit shift left operator are actually just an overloaded operator
有点像左移运算符实际上只是一个重载运算符

48
00:02:57,800 --> 00:03:01,860
so you need to think of them as a function now I know they look like an
所以现在您需要将它们视为功能，我知道它们看起来像

49
00:03:02,060 --> 00:03:06,719
operator but here's the thing operators are just functions so in this case this
运算符，但这里的东西只是运算符，因此在这种情况下

50
00:03:06,919 --> 00:03:11,100
would actually be the same thing as if it was something like the out of print
实际上就像是绝版一样

51
00:03:11,300 --> 00:03:14,969
and then hello world is our parameter and then maybe we would follow it on
然后你好世界是我们的参数，然后也许我们会继续

52
00:03:15,169 --> 00:03:21,660
along with another print that's all it is you have to think of these operators
加上另一张印刷品，您只需要考虑这些运算符

53
00:03:21,860 --> 00:03:24,810
as functions and if you think of them that way then it makes a little bit more
作为功​​能，如果您以这种方式考虑它们，那么它将使功能更多一些

54
00:03:25,009 --> 00:03:28,980
sense so what we're actually doing here is we're pushing this hollow world
感觉，所以我们在这里实际要做的是推动这个空心世界

55
00:03:29,180 --> 00:03:33,240
string into this CR which basically causes it to get printed to the console
字符串放入此CR，这基本上会导致将其打印到控制台

56
00:03:33,439 --> 00:03:36,810
and then we're pushing an end line this end line basically just tells our
然后我们推一个终点线，这个终点线基本上只是告诉我们

57
00:03:37,009 --> 00:03:40,740
console to advance to the next line the scenes of get function in our case will
控制台前进到下一行，在本例中，get函数的场景将

58
00:03:40,939 --> 00:03:44,160
basically just wait until we press ENTER before advancing to our next line of
基本上就是等到我们按ENTER键，再前进到下一行

59
00:03:44,360 --> 00:03:48,030
code which is nothing so basically what I'm saying is our programs execution
基本上没什么是我要说的代码是我们的程序执行

60
00:03:48,229 --> 00:03:52,170
will pause on this line until we press ENTER because this function is just
将在此行暂停，直到我们按Enter，因为此功能只是

61
00:03:52,370 --> 00:03:55,710
going to wait for us to press ENTER and then we advance the next line which is
将等待我们按ENTER键，然后前进下一行

62
00:03:55,909 --> 00:03:59,280
nothing which means that we actually return zero meaning our program executed
什么也没有，这意味着我们实际上返回零，这意味着我们的程序已执行

63
00:03:59,479 --> 00:04:03,840
successfully and that's it that is our entire program okay so that's our source
成功，那就是我们整个程序还可以，所以这就是我们的来源

64
00:04:04,039 --> 00:04:08,670
file we've actually got a file called main dot CPP which is a source file how
文件我们实际上有一个名为main dot CPP的文件，它是源文件

65
00:04:08,870 --> 00:04:13,830
do we get from this text to an actual executable binary file so basically we
我们从文本中得到一个实际的可执行二进制文件，所以基本上

66
00:04:14,030 --> 00:04:18,240
go through a few stages first we have this include iostream this is something
首先经历几个阶段，我们包括iostream这是

67
00:04:18,439 --> 00:04:22,588
called a preprocessor statement so this preprocessor statement gets evaluated
称为预处理程序语句，因此该预处理程序语句将得到评估

68
00:04:22,788 --> 00:04:26,449
before we compile the file in this case what it does is it include
在这种情况下，在我们编译文件之前，它包括

69
00:04:26,649 --> 00:04:31,129
all of the content of the iostream file into this file and I mean literally it
 iostream文件的所有内容都放入该文件，我的意思是

70
00:04:31,329 --> 00:04:37,009
just it copies and pastes that file into this file and again as I said we will
只是将其复制并粘贴到该文件中，然后再次按我说的那样

71
00:04:37,209 --> 00:04:40,819
talk about how to files in more depth in the future so don't worry too much if
谈论将来如何更深入地归档，因此不要担心太多

72
00:04:41,019 --> 00:04:45,559
you don't understand this so now all you need to know is we include this file so
您不了解这一点，所以现在您需要知道的是我们包含此文件，因此

73
00:04:45,759 --> 00:04:50,088
we can use to see out and see in functions once our preprocessor
我们可以使用预处理器来查看和查看函数

74
00:04:50,288 --> 00:04:54,650
statements have been evaluated our file gets compiled this is a stage where our
语句已被评估，我们的文件被编译，这是我们的阶段

75
00:04:54,850 --> 00:05:01,160
compiler transforms all of this C++ code into actual machine code there are
编译器将所有这些C ++代码转换为实际的机器代码， 

76
00:05:01,360 --> 00:05:04,338
several important settings that determine how this actually happens so
决定实际情况的几个重要设置

77
00:05:04,538 --> 00:05:07,310
let's take a brief look at them in visual studio we have these two
让我们在Visual Studio中简要看一下它们，我们有这两个

78
00:05:07,509 --> 00:05:12,528
important drop-down menus up here once called a solution configuration and once
重要的下拉菜单在这里曾经被称为解决方案配置， 

79
00:05:12,728 --> 00:05:16,160
called a solution platform by default it will probably be set to debug and then
默认情况下称为解决方案平台，它可能会设置为调试，然后

80
00:05:16,360 --> 00:05:20,480
either x86 or win32 they're actually the same if we drop down the debug you'll
 x86或win32，如果我们删除调试，它们实际上是相同的

81
00:05:20,680 --> 00:05:24,050
see that we've got two options debug and release these two options are defaults
看到我们有两个选项debug和release这两个选项是默认选项

82
00:05:24,250 --> 00:05:27,800
for any new project in Visual Studio and then under solution platform you'll see
对于Visual Studio中的任何新项目，然后在解决方案平台下，您将看到

83
00:05:28,000 --> 00:05:32,588
we've got x64 and x86 as our two options again these are just default a
我们再次将x64和x86作为两个选项，它们只是默认设置

84
00:05:32,788 --> 00:05:37,129
configuration is simply a set of rules which applies to the building of a
配置只是一组规则，适用于构建

85
00:05:37,329 --> 00:05:42,259
project whereas the solution platform is what platform we're targeting with our
项目，而解决方案平台就是我们要针对的平台

86
00:05:42,459 --> 00:05:47,180
current compilation so a good examples that will be x86 is targeting windows
当前的编译版本，因此将x86作为一个很好的例子是针对Windows 

87
00:05:47,379 --> 00:05:51,860
32-bit which means that we will generate a 32-bit application for Windows for
 32位，这意味着我们将为Windows生成一个32位应用程序，用于

88
00:05:52,060 --> 00:05:54,499
more complicated projects where you might be targeting different platforms
更复杂的项目，您可能会针对不同的平台

89
00:05:54,699 --> 00:05:59,329
you might have Android as a platform in that drop-down and then if you wanted to
您可以在该下拉菜单中使用Android作为平台，然后，如果您想

90
00:05:59,529 --> 00:06:03,230
build and deploy and debug on Android you would change your platform to be
在Android上构建，部署和调试，您将平台更改为

91
00:06:03,430 --> 00:06:07,459
Android and the solution configuration is a set of rules that defines the
 Android和解决方案配置是一组规则，用于定义

92
00:06:07,658 --> 00:06:10,579
compilation for that platform so let's take a look at some of the rules that we
该平台的编译，因此让我们看一下一些规则

93
00:06:10,779 --> 00:06:14,990
can change in our project let's right-click on it and hit properties so
可以更改我们的项目，让我们右键单击它并点击属性

94
00:06:15,189 --> 00:06:18,410
here we have Visual Studio as property pages these define the rules that are
在这里，我们将Visual Studio作为属性页，这些属性页定义了

95
00:06:18,610 --> 00:06:22,490
used to build them in configurations and platforms the first thing that you need
用于在配置和平台中构建它们的第一件事

96
00:06:22,689 --> 00:06:28,999
to notice is this configuration and platform area make sure that your
注意，此配置和平台区域确保您的

97
00:06:29,199 --> 00:06:33,468
configuration and your platform is set to the one that you actually want to
配置并将您的平台设置为您实际想要的平台

98
00:06:33,668 --> 00:06:37,430
modify for some reason sometimes it might be set to release however you're
出于某些原因进行修改，有时您可能会将其设置为释放

99
00:06:37,629 --> 00:06:40,509
clearly building debug which means that none of these changes will
清楚地进行调试，这意味着这些更改都不会

100
00:06:40,709 --> 00:06:43,449
like to your current configuration and if you missed that you might be
喜欢您当前的配置，如果错过了，您可能会

101
00:06:43,649 --> 00:06:46,720
wondering why nothing's working I've had that happen a few times it's kind of
想知道为什么我什么都没做过，那是几次

102
00:06:46,920 --> 00:06:49,660
annoying so we've got our debug configuration you can see we've got wind
烦人，所以我们有了调试配置，您可以看到我们有风

103
00:06:49,860 --> 00:06:54,280
32 now wind 32 is exactly the same as x86 okay they are the same they've got
 32现在wind 32与x86完全相同，好吧，它们与x86相同

104
00:06:54,480 --> 00:06:56,650
different names for some reasons but they are the same here we've got some
由于某些原因，名称不同，但在这里它们是相同的，我们有一些

105
00:06:56,850 --> 00:07:01,240
general information about our SDK version output directory or intermediate
有关我们SDK版本输出目录或中间目录的一般信息

106
00:07:01,439 --> 00:07:04,360
directory and stuff like that the important thing to note here is that our
目录和类似的东西这里要注意的重要是

107
00:07:04,560 --> 00:07:08,920
configuration type is set to application if we wanted it to be a library we're
配置类型设置为应用程序，如果我们希望它成为一个库

108
00:07:09,120 --> 00:07:11,680
good we could change it here but it's basically the binary there's a
好，我们可以在这里更改它，但是基本上是二进制文件，有一个

109
00:07:11,879 --> 00:07:15,759
compatible output since we're going for an executable binary we'll leave it set
兼容的输出，因为我们要使用可执行的二进制文件，所以我们将其设置为

110
00:07:15,959 --> 00:07:20,680
to application Exe the compiler settings are located under CC + block we've got
到应用程序Exe的编译器设置位于CC +块下

111
00:07:20,879 --> 00:07:24,759
important settings here such as include directories optimization settings that
重要设置，例如包含目录优化设置

112
00:07:24,959 --> 00:07:30,430
we may want to use cogeneration settings preprocessor definitions and a whole lot
我们可能要使用热电联产设置的预处理程序定义和很多其他功能

113
00:07:30,629 --> 00:07:34,000
of stuff that we're not even going to touch anytime soon the default little
我们甚至很快都不会碰到的东西

114
00:07:34,199 --> 00:07:37,689
studio configuration is actually pretty good so we don't really have to do
工作室配置实际上非常好，因此我们不必做

115
00:07:37,889 --> 00:07:41,740
anything but these are the rules that govern how our files will get compiled
除了这些，这些都是控制我们的文件如何编译的规则

116
00:07:41,939 --> 00:07:44,889
you can see the difference between the debug and the released configuration
您可以看到调试和已发布配置之间的区别

117
00:07:45,089 --> 00:07:48,250
pretty well if you go into the optimization tab under optimization if I
如果您进入“优化”下的“优化”标签， 

118
00:07:48,449 --> 00:07:51,970
change this to release you'll see that the optimization is set to maximize B
更改此版本以释放，您将看到优化设置为最大化B 

119
00:07:52,170 --> 00:07:56,110
whereas in debug it's set to disabled that's a great example of why I debug
而在调试中将其设置为禁用，这就是为什么我调试的一个很好的例子

120
00:07:56,310 --> 00:08:00,220
mode by default is slower a lot slower than release mode because optimization
默认情况下，发布模式比发布模式要慢很多，因为优化

121
00:08:00,420 --> 00:08:04,329
is turned off but of course turning off optimization will help us to debug our
已关闭，但当然关闭优化将有助于我们调试我们的

122
00:08:04,529 --> 00:08:07,210
code as we will later discover if you want to learn more about how the
代码，我们稍后会发现您是否想进一步了解

123
00:08:07,410 --> 00:08:11,620
compiler works I've made a dedicated video in depth covering exactly how all
编译器的工作原理我专门制作了一段专门的视频，深入介绍了所有

124
00:08:11,819 --> 00:08:14,829
of that happens so go check that out if you're interested the link will be in
发生这种情况，所以请检查一下，如果您有兴趣，链接将在

125
00:08:15,029 --> 00:08:19,870
the description below each CPP file in our project gets compiled header files
我们项目中每个CPP文件下面的描述都将获取已编译的头文件

126
00:08:20,069 --> 00:08:25,569
do not get compiled at all just CPP file remember header files get included via a
根本不编译只是CPP文件，记住头文件通过

127
00:08:25,769 --> 00:08:30,460
preprocessor statement called include into a CPP file and that's when they get
在CPP文件中调用包括include的预处理程序语句

128
00:08:30,660 --> 00:08:34,269
compiled so we've got a bunch of CPP files that we've compiled and they
编译，所以我们得到了一堆CPP文件，它们

129
00:08:34,470 --> 00:08:38,349
actually get compiled individually every CPP file will get compiled into
实际上每个CPP文件都会单独编译

130
00:08:38,549 --> 00:08:41,979
something called an object file the extension for that using visual studios
所谓的目标文件，是使用Visual Studio的扩展名

131
00:08:42,179 --> 00:08:46,899
compiler is obj once we have all of those individual obj files which are the
一旦我们拥有所有这些单独的obj文件（即

132
00:08:47,100 --> 00:08:50,919
result of compiling our CPP files we need some way to stitch them together
编译CPP文件的结果，我们需要某种方式将它们缝合在一起

133
00:08:51,120 --> 00:08:54,349
into one an exe file and that's where our friend
到一个exe文件中，这就是我们的朋友

134
00:08:54,549 --> 00:08:57,949
the linker comes in you can see the linker settings under this linker tab
链接器进入后，您可以在此链接器选项卡下看到链接器设置

135
00:08:58,149 --> 00:09:02,929
but basically what the linker does is it takes all of those obj files and it
但基本上，链接器的作用是获取所有这些obj文件，然后

136
00:09:03,129 --> 00:09:07,909
glues them together so the linkers job is to take all of our obj files and
将它们粘合在一起，以便链接器工作是获取我们所有的obj文件并

137
00:09:08,110 --> 00:09:12,709
stitch them together into one exe file of course the way that it does that is
当然将它们缝合到一个exe文件中

138
00:09:12,909 --> 00:09:17,209
actually kind of complicated so I've made a specific video covering that go
实际上有点复杂，所以我制作了一个专门的视频

139
00:09:17,409 --> 00:09:20,419
ahead and check that out the link will be in the description below so let's
并检查链接是否在下面的描述中，所以让我们

140
00:09:20,620 --> 00:09:23,509
take a look at this in action the first thing I want to do is actually
看看实际情况，我要做的第一件事实际上是

141
00:09:23,710 --> 00:09:27,859
just compile this CPP file in Visual Studio you can compile files
只需在Visual Studio中编译此CPP文件，就可以编译文件

142
00:09:28,059 --> 00:09:32,359
individually by hitting ctrl f7 you can see our output here shows that we're
分别按ctrl f7键，您可以看到此处的输出表明我们

143
00:09:32,559 --> 00:09:36,049
actually building this main del CPP file and that it succeeded if you don't want
实际上构建了这个主要的del CPP文件，并且如果您不想的话，它会成功

144
00:09:36,250 --> 00:09:40,009
to hit ctrl f7 you can actually bring up this compile button you can do so by
要按ctrl f7，实际上可以调出此编译按钮，方法是： 

145
00:09:40,210 --> 00:09:43,729
right-clicking here and clicking on build and then going out or remove
右键单击此处并单击构建，然后退出或删除

146
00:09:43,929 --> 00:09:50,479
buttons customize and then adding a command on the build called compile so
按钮进行自定义，然后在构建中添加一个名为compile的命令

147
00:09:50,679 --> 00:09:53,959
if we hit that button you can see that we get our file compiling if we were to
如果我们点击该按钮，则可以看到我们可以编译文件了。 

148
00:09:54,159 --> 00:09:57,679
make some kind of syntax error here for example I'm forgetting a semicolon if I
例如，在此处犯某种语法错误，如果我忘记了分号， 

149
00:09:57,879 --> 00:10:00,439
compile that file you'll see that we get an error
编译该文件，您会看到我们得到一个错误

150
00:10:00,639 --> 00:10:03,949
now Visual Studio presents us with errors in many different ways one of
现在，Visual Studio以多种方式向我们展示了错误之一

151
00:10:04,149 --> 00:10:08,629
which is this error list and another way is inside this output window I'm going
这是此错误列表，另一种方式是在我要输出的窗口中

152
00:10:08,830 --> 00:10:14,120
to tell you guys right now this error list is mostly garbage it might appear
告诉大家，这个错误列表主要是垃圾，它可能会出现

153
00:10:14,320 --> 00:10:18,529
to be readable for really small things like this but you never ever want to
对像这样的很小的东西可读，但是您永远都不想

154
00:10:18,730 --> 00:10:21,949
rely on it a lot of times it's actually missing information the weight of the
很多时候依靠它实际上是在丢失信息的重量

155
00:10:22,149 --> 00:10:26,839
error lists work is it basically passes our output window looking for the word
错误列表的工作原理基本上是通过我们的输出窗口寻找单词

156
00:10:27,039 --> 00:10:31,639
error and then grabs information from there that it can find and puts it into
错误，然后从那里获取可以找到的信息并将其放入

157
00:10:31,840 --> 00:10:35,359
this error lit so it's it's a good overview you want to use it like an
该错误点亮，这是一个很好的概述，您想像

158
00:10:35,559 --> 00:10:39,469
overview but if you want more details if you want all the information about the
概述，但是如果您需要更多详细信息，则需要有关

159
00:10:39,669 --> 00:10:43,189
error that you've just had look at the output window so for the rest of this
您刚刚在输出窗口中看到的错误，因此对于其余部分

160
00:10:43,389 --> 00:10:47,120
series I am actually going to be looking at this output window for error messages
我实际上将在此输出窗口中查看错误消息系列

161
00:10:47,320 --> 00:10:50,990
to get used to that you can see that we've got an arrow here it says intact
习惯了，您可以看到我们这里有一个完整的箭头

162
00:10:51,190 --> 00:10:55,609
arrow missing semicolon before a curly bracket it tells you which line number
大括号前缺少箭头的分号，它告诉您哪个行号

163
00:10:55,809 --> 00:10:59,659
the error is on if you double click on this actual line you will be taken to
如果您双击该实际行，则会出现错误，您将被带到

164
00:10:59,860 --> 00:11:03,819
where the error is in your source code so let's go ahead and fix that
错误在您的源代码中所在的位置，所以让我们继续进行修复

165
00:11:04,019 --> 00:11:08,139
by adding a semicolon and then hitting ctrl f7 or compile to build this one
通过添加分号，然后按ctrl f7或编译以构建此分号

166
00:11:08,339 --> 00:11:13,389
file so we've compiled a file when you compile files individually no linking
文件，因此当您分别编译文件而没有链接时，我们已经编译了文件

167
00:11:13,589 --> 00:11:17,049
happens obviously you're just compiling a single file so the linker is an
显然，您只是在编译一个文件，所以链接器是一个

168
00:11:17,250 --> 00:11:21,189
invoked apple let's go ahead and check out what the compiler actually generated
被调用的苹果让我们继续检查编译器实际生成了什么

169
00:11:21,389 --> 00:11:24,370
if we right click on our hollow world project you'll see an open folder in
如果我们右键单击空心世界项目，您将在其中看到一个打开的文件夹

170
00:11:24,570 --> 00:11:28,000
File Explorer button this will open up our File Explorer
 File Explorer按钮，这将打开我们的File Explorer 

171
00:11:28,200 --> 00:11:31,809
by default visuals view will output our build files into this debug folder and
默认情况下，visuals视图会将我们的构建文件输出到该调试文件夹中， 

172
00:11:32,009 --> 00:11:36,639
you can see if we go in there you can see a mean obj file this is the object
您可以看到我们是否进入那里，您可以看到一个平均的obj文件，这是对象

173
00:11:36,839 --> 00:11:40,209
file that our compiler has generated again you will have one of these for
我们的编译器再次生成的文件，您将拥有其中之一

174
00:11:40,409 --> 00:11:45,399
every single simple file in your project if we are back to visual studio and we
项目中的每个简单文件（如果我们返回Visual Studio，而我们

175
00:11:45,600 --> 00:11:49,120
build the actual project so I'm doing more than just building one file here
构建实际项目，所以我要做的不仅仅是在这里构建一个文件

176
00:11:49,320 --> 00:11:53,859
I'm actually building the entire project you can see that we actually get that
我实际上正在构建整个项目，您可以看到我们实际上得到了

177
00:11:54,059 --> 00:11:57,669
exe file and again if we go back to our file explorer that will actually be in
 exe文件，如果再次返回文件浏览器，该文件浏览器实际上将位于

178
00:11:57,870 --> 00:12:01,479
the directory of your solution and then in the debug folder
解决方案的目录，然后在debug文件夹中

179
00:12:01,679 --> 00:12:04,959
I know visual Studios default paths are a little bit weird I usually like to
我知道visual Studios的默认路径通常很奇怪

180
00:12:05,159 --> 00:12:07,779
change them but I'm trying to not to complicate things here and there's our
更改它们，但我尝试不使此处的内容复杂化， 

181
00:12:07,980 --> 00:12:10,779
hello world exe file which we can run and it prints
你好我们可以运行的世界exe文件，并打印

182
00:12:10,980 --> 00:12:16,120
the text hello world so that's a pretty simple overview but what happens when we
文字问候世界，这是一个非常简单的概述，但是当我们

183
00:12:16,320 --> 00:12:20,529
have multiple people plus files let's take a look at a simple example so
有多个人和文件，让我们看一个简单的例子

184
00:12:20,730 --> 00:12:23,500
suppose that we've got our hollow world printing the pecans all here but I don't
假设我们已经在整个空心世界中印刷了山核桃，但我没有

185
00:12:23,700 --> 00:12:27,549
want to use the see out function I want to use my own logging function and then
想使用seeout功能我想使用自己的日志功能，然后

186
00:12:27,750 --> 00:12:30,909
maybe that will wrap this see our function so let's create a function
也许将其包装起来看看我们的功能，所以让我们创建一个功能

187
00:12:31,110 --> 00:12:40,109
called log which will take in a C string called message and print that message
称为日志，它将接收一个名为message的C字符串并打印该消息

188
00:12:40,309 --> 00:12:43,809
choose a contour simple enough now don't worry if you're
选择足够简单的轮廓，现在不用担心

189
00:12:44,009 --> 00:12:48,490
not sure what a Const char pointer is we're going to talk about strings in
不确定什么是Const char指针，我们将讨论字符串

190
00:12:48,690 --> 00:12:52,299
another video for now all you have to know that a Const our pointer is
现在，您需要知道的另一个视频是Const我们的指针是

191
00:12:52,500 --> 00:12:56,620
basically just a type that can hold a string of text so now we can rewrite our
基本上只是一个可以容纳文本字符串的类型，所以现在我们可以重写我们的

192
00:12:56,820 --> 00:13:00,279
code so that instead of calling C out and then printing hello world we call
代码，这样我们就不必调用C，而是打印问候世界了

193
00:13:00,480 --> 00:13:05,799
this log function and then pass in hollow world as a parameter we can go
这个日志函数，然后传入空心世界作为参数，我们可以

194
00:13:06,000 --> 00:13:08,589
ahead and hit the local windows debugger button here just to make sure that it
并点击此处的本地Windows调试器按钮以确保它

195
00:13:08,789 --> 00:13:12,939
still works as you can see it does fantastic we've written our first
如您所见，它仍然可以正常运行，我们已经写了第一本

196
00:13:13,139 --> 00:13:16,789
function that was easy so now let's take that function and what it
很简单的功能，所以现在让我们使用该功能及其功能

197
00:13:16,990 --> 00:13:20,269
into a different file because I don't want to have this main does typically
放入另一个文件，因为我通常不想让这个主要

198
00:13:20,470 --> 00:13:22,729
files flooded with all of my code I want to
我的所有代码淹没了所有文件

199
00:13:22,929 --> 00:13:26,569
separate my code into multiple files to keep things nice and clean and organized
将我的代码分成多个文件，以保持环境整洁有序

200
00:13:26,769 --> 00:13:30,500
we'll make a new file under source files like going a right-click add new item
我们将在源文件下创建一个新文件，例如右键单击添加新项

201
00:13:30,700 --> 00:13:37,219
we'll make a CPP file we'll call it log build CPP and we'll click Add so what
我们将创建一个CPP文件，我们将其称为“日志构建CPP”，然后单击“添加”， 

202
00:13:37,419 --> 00:13:41,779
I'm going to do here is I'm going to go back to main and I'm going to cut this
我要在这里做的是，我要回到主要位置，然后将其切掉

203
00:13:41,980 --> 00:13:47,389
log function and paste it into here so now we've got a function inside our
记录功能并将其粘贴到此处，所以现在我们在内部有了一个功能

204
00:13:47,589 --> 00:13:51,169
logos with a file called log let's try and compile just this file okay check
带有名为log的文件的徽标，让我们尝试仅编译该文件，然后检查

205
00:13:51,370 --> 00:13:54,319
this out we get a bunch of errors and if we look at our output window you'll see
这会导致很多错误，如果我们查看输出窗口，您会看到

206
00:13:54,519 --> 00:13:58,129
that the out is not a member of STD basically is telling us that it has no
外面不是性病的成员，这基本上是在告诉我们

207
00:13:58,330 --> 00:14:03,589
idea what CL is the reason is because we haven't included a declaration for the
知道CL的原因是因为我们没有为

208
00:14:03,789 --> 00:14:09,078
out every kind of symbol in saqqaq lot needs some kind of declaration the out
退出saqqaq中的每种符号都需要某种声明

209
00:14:09,278 --> 00:14:14,209
is defined inside a file that we included in main dot cpp and that file
在我们包含在主点cpp中的文件中定义，并且该文件

210
00:14:14,409 --> 00:14:19,069
of course was iostream let's go ahead and grab iostream and put it at the top
当然是iostream，让我们继续前进，抓住iostream并将其放在顶部

211
00:14:19,269 --> 00:14:23,689
of this file so that we include iostream by doing so we include a declaration for
该文件的内容，这样我们就包括了iostream，我们为

212
00:14:23,889 --> 00:14:27,979
this to be our function let's go ahead and compile this file once again you can
这就是我们的功能，让我们继续编译该文件，您可以

213
00:14:28,179 --> 00:14:30,919
see now it succeeds great so back in main I want to call
现在看到它成功非常好，所以回到主我想打电话

214
00:14:31,120 --> 00:14:36,109
this log function can I do that let's hit ctrl f7 no I can't because log is
这个日志功能可以做到这一点，让我们按ctrl f7，否，因为日志是

215
00:14:36,309 --> 00:14:40,878
not found we also get a complaint about the end but we already know that that's
没找到，我们也收到有关结束的投诉，但我们已经知道那是

216
00:14:41,078 --> 00:14:44,990
because we removed the i/os trim included and we have no idea what bian
因为我们删除了随附的I / OS修剪，但我们不知道该怎么办

217
00:14:45,190 --> 00:14:51,049
is we can restore that include and our problem should be fixed however you can
是我们可以还原包含的内容，还是应该解决我们的问题，但是您可以

218
00:14:51,250 --> 00:14:55,219
see that log is still not found so what's going on here
看到仍然找不到日志，所以这里发生了什么

219
00:14:55,419 --> 00:15:01,490
we've moved a function from one file into the other and we are compiling each
我们已经将函数从一个文件移到了另一个文件中，并且我们正在对其进行编译

220
00:15:01,690 --> 00:15:06,258
file separately for the minute so this main dot CPP file has no idea that
该文件单独存储一分钟，因此该主点CPP文件不知道

221
00:15:06,458 --> 00:15:10,099
there's a function called log somewhere and since it doesn't recognize what log
在某处有一个名为log的函数，因为它无法识别什么日志

222
00:15:10,299 --> 00:15:14,959
is it gives us the compile error we can fix this by providing something called a
它是否给我们带来了编译错误，我们可以通过提供一个称为a的东西来解决此问题

223
00:15:15,159 --> 00:15:20,000
declaration a declaration is exactly what it sounds like we're declaring that
声明声明的确切含义就是我们在声明

224
00:15:20,200 --> 00:15:24,469
something called log exists now this is almost like a promise because we can
现在有一种叫做log的东西，这几乎就像一个承诺，因为我们可以

225
00:15:24,669 --> 00:15:29,329
just say hey compiler there's a function called log however
只是说嘿编译器有一个叫做log的函数

226
00:15:29,529 --> 00:15:32,209
the compiler will just believe us and that's the great thing about the
编译器只会相信我们，这是关于

227
00:15:32,409 --> 00:15:36,229
compiler it'll be like oh yeah cool I totally trust you because the compiler
编译器就像哦，是的，很酷，我完全信任您，因为编译器

228
00:15:36,429 --> 00:15:40,939
doesn't care about resolving where that log function actually is defined we have
不在乎解析该日志函数的实际定义位置

229
00:15:41,139 --> 00:15:45,649
two different words here declarations and definitions declarations are just a
这里的两个不同的词声明和定义声明只是一个

230
00:15:45,850 --> 00:15:51,289
statement which say hey this symbol this function exists and then a definition is
声明说嘿这个符号这个功能存在，然后定义是

231
00:15:51,490 --> 00:15:56,240
something that says this is what this function is this is the body need for
东西说这是这个功能是什么，这是身体需要

232
00:15:56,440 --> 00:16:00,679
this function so let's go ahead and write a declaration for our log function
这个函数，让我们继续为我们的log函数编写一个声明

233
00:16:00,879 --> 00:16:05,209
a declaration looks very similar to an actual definition this is something
声明看起来与实际定义非常相似

234
00:16:05,409 --> 00:16:08,719
called a definition because you can see that not only have we declared something
之所以称为定义，是因为您可以看到不仅我们声明了一些东西

235
00:16:08,919 --> 00:16:12,259
with the name logger function with the name log we've also given it a body
使用名称记录器功能和名称记录，我们还为其指定了一个主体

236
00:16:12,460 --> 00:16:16,309
which actually contains what code will run when we call this function so back
当我们调用此函数时，其中实际上包含将运行的代码，因此返回

237
00:16:16,509 --> 00:16:20,240
in main let's write a declaration and a declaration looks very similar to a
首先，让我们写一个声明，声明看起来非常类似于

238
00:16:20,440 --> 00:16:25,789
definition however what it doesn't have is the actual body so you can see I can
定义，但是它没有的是实际主体，所以您可以看到我可以

239
00:16:25,990 --> 00:16:30,109
just put a semicolon at the end of this and that is the end of that in fact you
只需在此末尾添加一个分号，实际上这就是该末尾

240
00:16:30,309 --> 00:16:33,289
don't even have to specify the name of the parameter because it doesn't matter
甚至不必指定参数名称，因为这无关紧要

241
00:16:33,490 --> 00:16:37,519
you can just write that and the rule of thumb though I do like to specify the
您可以只写一个，但我喜欢指定经验法则

242
00:16:37,720 --> 00:16:42,500
name because it makes more sense so let's compile this file now tallip check
名称，因为它更有意义，所以让我们现在编译此文件，进行tallip检查

243
00:16:42,700 --> 00:16:45,889
this out the compiler totally bought it so you might be wondering at this stage
这是编译器完全购买的，因此您可能想知道现阶段

244
00:16:46,089 --> 00:16:51,289
what will hey how did the compiler know that we actually have a log function in
嘿，编译器如何知道我们实际上有一个log函数呢？ 

245
00:16:51,490 --> 00:16:55,399
another file if we're just compiling this one file and the answer is it
如果我们只是在编译另一个文件，另一个文件就是答案

246
00:16:55,600 --> 00:17:01,250
doesn't it just it trusts us so then your second question should be how does
不只是它信任我们，所以您的第二个问题应该是

247
00:17:01,450 --> 00:17:06,919
it actually run the right code that is where the linker comes in when we build
它实际上运行正确的代码，这是我们构建链接器时所在的位置

248
00:17:07,119 --> 00:17:11,239
our entire project not just this one file but if I actually right click and
我们整个项目不仅是这个文件，而且如果我实际上右键单击并

249
00:17:11,439 --> 00:17:15,319
hit build once our files have been compiled the linker will actually find
一旦我们的文件被编译，点击构建链接器实际上会找到

250
00:17:15,519 --> 00:17:19,489
the definition of that log function and wire it up to the log function that we
该日志函数的定义并将其连接到我们的日志函数

251
00:17:19,689 --> 00:17:24,529
call here in main dot CPP if it can't find that definition that's when we get
如果找不到该定义，请在主点CPP中调用此处

252
00:17:24,730 --> 00:17:27,239
a linker error now linking errors or something
链接器错误现在链接错误或其他内容

253
00:17:27,439 --> 00:17:32,338
look very scary and a lot of people get scared so let's go ahead and look at an
看起来很吓人，很多人都害怕，所以让我们继续看一下

254
00:17:32,538 --> 00:17:36,568
example of that so right now if I just run my program you'll see that it still
这样的例子现在，如果我只是运行我的程序，你会发现它仍然

255
00:17:36,769 --> 00:17:40,078
prints out the text hello world and everything runs successfully however
打印出文本hello world，但是一切都成功运行

256
00:17:40,278 --> 00:17:44,219
let's remove this or at least change it a little bit for example I'll change
让我们删除它或至少对其稍作更改，例如，我将更改

257
00:17:44,419 --> 00:17:49,259
this to logger save the file go back here to main Datsyuk B I'll try and
这是记录程序以保存文件，然后回到这里回到主要的Datsyuk B，我将尝试并

258
00:17:49,460 --> 00:17:52,709
compile this file by itself you can see that we get no problems whatsoever
单独编译该文件，您可以看到我们没有任何问题

259
00:17:52,909 --> 00:17:56,938
however let's right-click on this hello world and hit field and check this out
但是，让我们右键单击这个hello world并点击field并检查一下

260
00:17:57,138 --> 00:18:00,959
we get a pretty scary looking error message which also and unfortunately
我们收到了一个非常可怕的错误消息，不幸的是

261
00:18:01,159 --> 00:18:03,869
looks scary in our output window now the reason this
现在在我们的输出窗口中看起来很恐怖，原因是

262
00:18:04,069 --> 00:18:06,358
looks those two carries because it's actually got some extra information
看起来这两个携带，因为它实际上有一些额外的信息

263
00:18:06,558 --> 00:18:10,469
about our functions ignition for example it's got the pulling convention here as
关于我们的功能点火，例如，这里有拉式约定

264
00:18:10,669 --> 00:18:14,338
well as an actual ID but basically what it's telling you is that you have an
以及实际的ID，但基本上它告诉您的是

265
00:18:14,538 --> 00:18:19,318
unresolved external symbol called log which returns which has this return
尚未解决的外部符号，称为log，它返回并返回

266
00:18:19,519 --> 00:18:24,419
value and these parameters and your referencing this function inside main an
值和这些参数，以及您在内部引用该函数

267
00:18:24,619 --> 00:18:28,198
unresolved external symbol means that the linker was unable to resolve a
未解析的外部符号表示链接器无法解析

268
00:18:28,398 --> 00:18:32,219
symbol remembered linkers job is to resolve symbols it has to wire up
符号记住链接器的工作是解决它必须连接的符号

269
00:18:32,419 --> 00:18:37,438
functions and it couldn't find what to wire log to because we don't have a
功能，它找不到连接日志的内容，因为我们没有

270
00:18:37,638 --> 00:18:41,459
function called log that's actually defined that has a body so the way that
实际上定义的称为log的函数具有主体，因此

271
00:18:41,659 --> 00:18:46,438
we can fix this is by fixing our function we need to provide a definition
我们可以通过修复函数来解决此问题，我们需要提供一个定义

272
00:18:46,638 --> 00:18:51,209
for this log function in other words we have to provide a body for this log
换句话说，我们必须为此日志提供一个主体

273
00:18:51,409 --> 00:18:54,959
function doesn't have to be inside this file it can be inside main but it has to
函数不必在此文件中，它可以在main文件中，但它必须

274
00:18:55,159 --> 00:18:59,068
be somewhere and if we compile this you'll see that we don't get any errors
在某个地方，如果我们编译它，您会看到我们没有任何错误

275
00:18:59,269 --> 00:19:02,698
if we go back to our file explorer and look at what we've got inside our
如果我们回到文件浏览器并查看我们内部的内容

276
00:19:02,898 --> 00:19:06,899
intermediate folder here you'll see that we've got two obj files because the
在中间文件夹中，您会看到我们有两个obj文件，因为

277
00:19:07,099 --> 00:19:11,578
compiler generates an object file for each of our CBP files the linker will
编译器为链接器将为我们的每个CBP文件生成一个目标文件

278
00:19:11,778 --> 00:19:15,299
then take them and pitch them together into an exe file so in our lovely
然后把它们放到一个exe文件中，这样在我们可爱的

279
00:19:15,500 --> 00:19:20,339
example we have our log definition inside this log obj file
例如，我们在此日志obj文件中有日志定义

280
00:19:20,539 --> 00:19:25,230
and our main function inside our main door obj 12 and so link up will
和我们的主要功能在我们的主要对象obj 12内，因此链接起来

281
00:19:25,430 --> 00:19:31,430
basically take that log definition from log and put it into a common binary
基本上从日志中获取该日志定义，并将其放入通用二进制文件中

282
00:19:31,630 --> 00:19:36,509
which is our hello world XP file which contains a definition for both main and
这是我们的hello world XP文件，其中包含main和

283
00:19:36,710 --> 00:19:42,000
log and that is a basic overview of how C++ works again I highly encourage you
日志，这是C ++再次工作的基本概述，我强烈建议您

284
00:19:42,200 --> 00:19:46,109
to check out the in-depth videos about how compiling and linking works because
观看有关编译和链接如何工作的深入视频，因为

285
00:19:46,309 --> 00:19:49,710
they're going to be way more information than this video this video was just made
他们将比这部影片刚制作的影片提供更多资讯

286
00:19:49,910 --> 00:19:53,669
to show you kind of an overview of the pipeline of how you go from source files
向您展示如何使用源文件的管道的概述

287
00:19:53,869 --> 00:19:57,809
to an actual binary as always don't forget to follow me on Twitter and
转换为实际的二进制文件，不要忘记在Twitter上关注我， 

288
00:19:58,009 --> 00:20:00,960
Instagram if you really enjoyed this video and you want to see more you can
 Instagram，如果您真的喜欢这个视频，并且想看更多，可以

289
00:20:01,160 --> 00:20:03,690
support me on patreon I'll see you guys next time
在patreon上支持我下次见

290
00:20:03,890 --> 00:20:08,890
goodbye war
再见战争

