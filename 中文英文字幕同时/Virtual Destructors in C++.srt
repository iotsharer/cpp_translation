﻿1
00:00:00,000 --> 00:00:01,449
嗨，大家好，我叫詹娜（Jenna），欢迎回到我的兄弟姐妹老板系列，所以今天
Hey look guys my name is Jenna welcome

2
00:00:01,649 --> 00:00:03,488
嗨，大家好，我叫詹娜（Jenna），欢迎回到我的兄弟姐妹老板系列，所以今天
back to my siblings boss series so today

3
00:00:03,689 --> 00:00:04,809
如果您不确定，我们在谈论C ++中的虚拟析构函数
we're talking all about virtual

4
00:00:05,009 --> 00:00:07,030
如果您不确定，我们在谈论C ++中的虚拟析构函数
destructors in C++ if you're not sure

5
00:00:07,230 --> 00:00:08,439
破坏者是什么，一定要先查看该视频，然后再看
what a destructor is and definitely

6
00:00:08,638 --> 00:00:10,630
破坏者是什么，一定要先查看该视频，然后再看
check out that video first and also you

7
00:00:10,830 --> 00:00:11,830
不知道什么是血管功能，那么还有那个视频
don't know what a vessel function is

8
00:00:12,029 --> 00:00:13,240
不知道什么是血管功能，那么还有那个视频
then there's also that video that you

9
00:00:13,439 --> 00:00:14,439
应该检查一下，因为您可以想像的虚拟析构函数是
should probably check out because a

10
00:00:14,638 --> 00:00:16,359
应该检查一下，因为您可以想像的虚拟析构函数是
virtual destructor as you can imagine is

11
00:00:16,559 --> 00:00:18,310
这两种东西的结合现在这很简单
kind of the combination of both of those

12
00:00:18,510 --> 00:00:20,530
这两种东西的结合现在这很简单
things now this is kind of simple this

13
00:00:20,730 --> 00:00:21,850
视频可能不应该花太长时间，我们将深入探讨
video probably shouldn't take too long

14
00:00:22,050 --> 00:00:23,260
视频可能不应该花太长时间，我们将深入探讨
and we're just going to dive into an

15
00:00:23,460 --> 00:00:25,239
例子，立即看一看，但实际上虚拟破坏者非常
example and take a look immediately but

16
00:00:25,439 --> 00:00:27,129
例子，立即看一看，但实际上虚拟破坏者非常
essentially virtual disruptors are very

17
00:00:27,329 --> 00:00:28,568
对于您处理多态性非常重要，也就是说，如果我
very important for when you're dealing

18
00:00:28,768 --> 00:00:30,879
对于您处理多态性非常重要，也就是说，如果我
with polymorphism so in other words if I

19
00:00:31,079 --> 00:00:33,159
有一系列的子类和所有继承（如果你们没有）
have a series of subclasses and all that

20
00:00:33,359 --> 00:00:35,198
有一系列的子类和所有继承（如果你们没有）
inheritance if you guys haven't you guys

21
00:00:35,399 --> 00:00:36,788
不知道什么是继承不是简单的运动一定要查看该视频
don't know what inheritance isn't simple

22
00:00:36,988 --> 00:00:37,989
不知道什么是继承不是简单的运动一定要查看该视频
sports definitely check out that video

23
00:00:38,189 --> 00:00:41,439
同样，但如果您有一个像a这样的班级，那么从a派生的班级婴儿
as well but if you have a class like a

24
00:00:41,640 --> 00:00:44,018
同样，但如果您有一个像a这样的班级，那么从a派生的班级婴儿
and then a class babies derive from a

25
00:00:44,219 --> 00:00:46,659
并且您实际上想将B类称为A类，但实际上是B类
and you want to actually reference Class

26
00:00:46,859 --> 00:00:49,390
并且您实际上想将B类称为A类，但实际上是B类
B as Class A but it's actually Class B

27
00:00:49,590 --> 00:00:52,209
然后您决定删除a或以某种方式删除它，然后
and then you decide to delete a or it

28
00:00:52,409 --> 00:00:55,419
然后您决定删除a或以某种方式删除它，然后
gets deleted by some kind of way then

29
00:00:55,619 --> 00:00:58,239
您还希望B的析构函数不仅运行a，而且本质上就是
you also want the destructor of B to run

30
00:00:58,439 --> 00:01:00,070
您还希望B的析构函数不仅运行a，而且本质上就是
not just a and that is essentially what

31
00:01:00,270 --> 00:01:01,989
虚拟破坏者就是这样，这就是他们所促进的，以便
virtual disruptors are and that's what

32
00:01:02,189 --> 00:01:04,418
虚拟破坏者就是这样，这就是他们所促进的，以便
that's what they facilitate so that

33
00:01:04,618 --> 00:01:05,799
在您的脑海中对您可能毫无意义，因为这很难
probably made no sense to you in your

34
00:01:06,000 --> 00:01:06,730
在您的脑海中对您可能毫无意义，因为这很难
head because it's kind of hard to

35
00:01:06,930 --> 00:01:08,049
直观地了解如何直接跳入一些代码，然后看看
visualize how it's going to jump into

36
00:01:08,250 --> 00:01:10,238
直观地了解如何直接跳入一些代码，然后看看
some code straight away and and just see

37
00:01:10,438 --> 00:01:11,799
这样就行了，所以我在这里要做的是制作两个班级，一个班级
this in action okay so all I'm going to

38
00:01:12,000 --> 00:01:14,140
这样就行了，所以我在这里要做的是制作两个班级，一个班级
do here is make two classes one is going

39
00:01:14,340 --> 00:01:16,000
被称为基础，因为这将成为我们的基础课程
to be called base because this is going

40
00:01:16,200 --> 00:01:18,189
被称为基础，因为这将成为我们的基础课程
to be our base class I'm just gonna make

41
00:01:18,390 --> 00:01:20,769
一个公共构造函数，我将只打印出该构造函数，以便我们
a public constructor and I'll just print

42
00:01:20,969 --> 00:01:23,799
一个公共构造函数，我将只打印出该构造函数，以便我们
out that constructed just so that we

43
00:01:24,000 --> 00:01:27,009
知道该类被构造并且该构造函数被调用
know that the class gets constructed and

44
00:01:27,209 --> 00:01:28,539
知道该类被构造并且该构造函数被调用
and that the constructor gets called

45
00:01:28,739 --> 00:01:31,539
然后我还要写一个析构函数，我会说被破坏了，或者实际上
then I also write a destructor

46
00:01:31,739 --> 00:01:34,778
然后我还要写一个析构函数，我会说被破坏了，或者实际上
and I'll say destructed okay or actually

47
00:01:34,978 --> 00:01:36,069
我会叫这个构造函数和析构函数好吧，我想这样做
I'll just call this constructor and

48
00:01:36,269 --> 00:01:38,679
我会叫这个构造函数和析构函数好吧，我想这样做
destructor okay cool I want to do the

49
00:01:38,879 --> 00:01:40,329
对于另一堂课完全一样，我将其复制并粘贴，但我会打电话给
exact same thing for another class I'll

50
00:01:40,530 --> 00:01:42,128
对于另一堂课完全一样，我将其复制并粘贴，但我会打电话给
just copy and paste it but I'll call

51
00:01:42,328 --> 00:01:43,869
这个派生的，它实际上是从基础派生的，所以我们派生了
this derived and it's actually going to

52
00:01:44,069 --> 00:01:46,119
这个派生的，它实际上是从基础派生的，所以我们派生了
be derived from base so we have derived

53
00:01:46,319 --> 00:01:49,390
作为基类的子类，然后在这里我将做完全相同的事情
as a subclass of the base class and then

54
00:01:49,590 --> 00:01:51,369
作为基类的子类，然后在这里我将做完全相同的事情
over here I'm gonna do the exact same

55
00:01:51,569 --> 00:01:53,679
的东西，实际上我只是要写那是一个如此基础的东西
thing and in fact I'm just going to

56
00:01:53,879 --> 00:01:55,689
的东西，实际上我只是要写那是一个如此基础的东西
write which one was constructed so base

57
00:01:55,890 --> 00:01:58,539
基于构造函数的析构函数，然后还派生构造函数并派生
constructor based destructor and then

58
00:01:58,739 --> 00:02:02,679
基于构造函数的析构函数，然后还派生构造函数并派生
also derived constructor and derived

59
00:02:02,879 --> 00:02:05,409
析构函数好吧，让我们在这里考虑两种情况，我想做的第一件事
destructor okay so let's consider two

60
00:02:05,609 --> 00:02:07,539
析构函数好吧，让我们在这里考虑两种情况，我想做的第一件事
scenarios here the first thing I want to

61
00:02:07,739 --> 00:02:08,860
做的是创建我要的基类的实例
do is create

62
00:02:09,060 --> 00:02:10,390
做的是创建我要的基类的实例
an instance of the base class I'm gonna

63
00:02:10,590 --> 00:02:11,890
把它分配在臀部上，这样我们就可以将自己的创作变得清晰明了，
allocate this on the hip so that we can

64
00:02:12,090 --> 00:02:14,469
把它分配在臀部上，这样我们就可以将自己的创作变得清晰明了，
be explicit with our creation and

65
00:02:14,669 --> 00:02:16,689
删除阵营，所以基础基础等于新基础，我将写精英基础，
deletion camp so base base equals new

66
00:02:16,889 --> 00:02:19,810
删除阵营，所以基础基础等于新基础，我将写精英基础，
base and I'll write the elite base and

67
00:02:20,009 --> 00:02:21,610
现在，我就像在这两者之间像一个小的分隔线一样打印
now I'll just print like a little

68
00:02:21,810 --> 00:02:25,780
现在，我就像在这两者之间像一个小的分隔线一样打印
divider between the two like that and I

69
00:02:25,979 --> 00:02:30,780
就像这样，也派生派生等于新派生
also do derived derived equals new

70
00:02:30,979 --> 00:02:34,868
就像这样，也派生派生等于新派生
derived just like that oops right

71
00:02:35,068 --> 00:02:37,270
他们会叫它，我们也会删除掉
they'll call it and we'll delete dropped

72
00:02:37,469 --> 00:02:37,750
他们会叫它，我们也会删除掉
as well

73
00:02:37,949 --> 00:02:39,610
让我们按f5并在此处查看此当前设置会发生什么，以便您可以看到
let's hit f5 and see what happens with

74
00:02:39,810 --> 00:02:41,289
让我们按f5并在此处查看此当前设置会发生什么，以便您可以看到
this current setup here so you can see

75
00:02:41,489 --> 00:02:43,509
真的没有什么不同，我的意思是我没有标记任何东西
nothing really is different I mean I

76
00:02:43,709 --> 00:02:44,950
真的没有什么不同，我的意思是我没有标记任何东西
haven't marked anything anything is

77
00:02:45,150 --> 00:02:47,230
完全是虚拟的，但我有一个基础，我们得到的权利也属于这种类型
virtual at all but I have a base and a

78
00:02:47,430 --> 00:02:50,439
完全是虚拟的，但我有一个基础，我们得到的权利也属于这种类型
right we're derived is also of the type

79
00:02:50,639 --> 00:02:52,660
base，因为它是基类的子类，所以0 5好的，如您在这里看到的
base because it's a subclass of the base

80
00:02:52,860 --> 00:02:55,750
base，因为它是基类的子类，所以0 5好的，如您在这里看到的
class so 0 5 ok so as you can see here I

81
00:02:55,949 --> 00:02:57,160
猜猜您可能会期望什么会在我们创建时在这里发生
guess what you probably would expect

82
00:02:57,360 --> 00:02:59,530
猜猜您可能会期望什么会在我们创建时在这里发生
would happen happen here when we created

83
00:02:59,729 --> 00:03:01,630
基类，它仅称为删除后删除的基结构
the base class it only called the base

84
00:03:01,830 --> 00:03:03,460
基类，它仅称为删除后删除的基结构
construction destroyed after we deleted

85
00:03:03,659 --> 00:03:04,539
它正是您所期望的4以及它的派生
it which is exactly what you would

86
00:03:04,739 --> 00:03:06,580
它正是您所期望的4以及它的派生
expect and 4 derived as well what it

87
00:03:06,780 --> 00:03:09,580
似乎已经完成了，它首先被称为基本构造函数，然后被派生
seemed to have done was it first called

88
00:03:09,780 --> 00:03:11,050
似乎已经完成了，它首先被称为基本构造函数，然后被派生
the base constructor then the derived

89
00:03:11,250 --> 00:03:13,000
构造函数，然后将其删除后，它首先调用派生的析构函数
constructor and then after we deleted it

90
00:03:13,199 --> 00:03:14,920
构造函数，然后将其删除后，它首先调用派生的析构函数
it called the derived destructor first

91
00:03:15,120 --> 00:03:16,240
然后是基本析构函数，因此在这里一切正常
and then the base destructor so

92
00:03:16,439 --> 00:03:18,430
然后是基本析构函数，因此在这里一切正常
everything is working correctly here as

93
00:03:18,629 --> 00:03:20,140
我们现在期望在需要虚拟析构函数时会出现
we would expect now the problem of

94
00:03:20,340 --> 00:03:22,120
我们现在期望在需要虚拟析构函数时会出现
needing a virtual destructor arises when

95
00:03:22,319 --> 00:03:23,950
我们有这种情况，所以让我们再复制一次，但是这次
we have this scenario so let's just copy

96
00:03:24,150 --> 00:03:25,840
我们有这种情况，所以让我们再复制一次，但是这次
this one more time but this time what

97
00:03:26,039 --> 00:03:28,390
我会做的是，实际上我会像多态类型一样
I'll do is I'll actually have like a

98
00:03:28,590 --> 00:03:30,400
我会做的是，实际上我会像多态类型一样
polymorphic kind of type here so else

99
00:03:30,599 --> 00:03:32,020
叫做poly基本上我们在这里要做的就是创建一个派生
called poly basically what we're doing

100
00:03:32,219 --> 00:03:33,490
叫做poly基本上我们在这里要做的就是创建一个派生
here is we're creating a derived

101
00:03:33,689 --> 00:03:35,650
实例，但是我们将其分配给这样的基本类型，因此我们正在处理
instance however we're assigning it to a

102
00:03:35,849 --> 00:03:38,379
实例，但是我们将其分配给这样的基本类型，因此我们正在处理
base type like this so we are treating

103
00:03:38,579 --> 00:03:41,259
这种多边形对象好像是这里的基本指针，但实际上是
this kind of poly object as if it was a

104
00:03:41,459 --> 00:03:43,719
这种多边形对象好像是这里的基本指针，但实际上是
base pointer here but it's actually a

105
00:03:43,919 --> 00:03:47,800
指向派生派生类型的指针，因此在这种情况下，如果我们运行此代码并查看
pointer to a derived derived type so in

106
00:03:48,000 --> 00:03:49,810
指向派生派生类型的指针，因此在这种情况下，如果我们运行此代码并查看
this case if we run this code and see

107
00:03:50,009 --> 00:03:52,150
会发生什么，您可以看到我们在这里有第三种情况
what happens you can see that we have

108
00:03:52,349 --> 00:03:53,980
会发生什么，您可以看到我们在这里有第三种情况
this third scenario here where the base

109
00:03:54,180 --> 00:03:55,509
构造函数和派生构造函数会正确调用，就像我们在调整时一样
constructor and derived constructor gets

110
00:03:55,709 --> 00:03:57,670
构造函数和派生构造函数会正确调用，就像我们在调整时一样
called correctly just as we adjust as we

111
00:03:57,870 --> 00:03:59,409
可以从第二个示例中获得期望，但是当需要删除它时
would expect from that second example

112
00:03:59,609 --> 00:04:01,750
可以从第二个示例中获得期望，但是当需要删除它时
however when it comes time to delete it

113
00:04:01,949 --> 00:04:04,118
只是基本析构函数被称为，我们缺少的派生析构函数
just the base destructor is called not

114
00:04:04,318 --> 00:04:05,710
只是基本析构函数被称为，我们缺少的派生析构函数
the derived destructor we're missing

115
00:04:05,909 --> 00:04:07,990
这一点非常重要，因为这可能导致内存泄漏，
this here and that's very important

116
00:04:08,189 --> 00:04:09,670
这一点非常重要，因为这可能导致内存泄漏，
because that could cause memory leak and

117
00:04:09,870 --> 00:04:11,319
一会儿，我将告诉您，但是您可以看到具体发生的情况
I'll show you how in a minute but you

118
00:04:11,519 --> 00:04:12,759
一会儿，我将告诉您，但是您可以看到具体发生的情况
can see that what specifically happening

119
00:04:12,959 --> 00:04:15,400
这是当我们去删除多边形时，它实际上不知道
here is when we go to delete poly it

120
00:04:15,599 --> 00:04:17,649
这是当我们去删除多边形时，它实际上不知道
doesn't know that actually this

121
00:04:17,848 --> 00:04:20,620
毁了我打电话说我可能还有另一个析构函数
destructed I'm calling my potentially

122
00:04:20,819 --> 00:04:22,900
毁了我打电话说我可能还有另一个析构函数
have another destructor

123
00:04:23,100 --> 00:04:25,090
因为它没有被标记为虚拟的，这意味着说老板老板实际上并没有
because it's not marked as virtual which

124
00:04:25,290 --> 00:04:26,530
因为它没有被标记为虚拟的，这意味着说老板老板实际上并没有
means that say boss boss hasn't actually

125
00:04:26,730 --> 00:04:29,290
知道我可以，这可能有点像方法和某种覆盖
know that I okay this there might be

126
00:04:29,490 --> 00:04:31,480
知道我可以，这可能有点像方法和某种覆盖
like a method and an overwrite some kind

127
00:04:31,680 --> 00:04:34,389
另一类中的重写方法的说明进一步深入了层次结构
of overridden method in another kind of

128
00:04:34,589 --> 00:04:36,490
另一类中的重写方法的说明进一步深入了层次结构
class further dive down the hierarchy

129
00:04:36,689 --> 00:04:38,710
因为使用方法当然，如果我们将其标记出来，我们只是使用普通方法
because with methods of course we're

130
00:04:38,910 --> 00:04:40,060
因为使用方法当然，如果我们将其标记出来，我们只是使用普通方法
just with a normal method if we mark it

131
00:04:40,259 --> 00:04:42,069
作为虚拟的，它具有被覆盖的能力，这意味着它必须完成
as virtual it has the ability to be

132
00:04:42,269 --> 00:04:43,509
作为虚拟的，它具有被覆盖的能力，这意味着它必须完成
overridden which means it has to finish

133
00:04:43,709 --> 00:04:45,189
在V桌子上，所有需要工作的东西，以及会分散注意力的东西，
at the V table and all that has to work

134
00:04:45,389 --> 00:04:47,110
在V桌子上，所有需要工作的东西，以及会分散注意力的东西，
and be set up with distractions it's a

135
00:04:47,310 --> 00:04:48,639
有点不同，因为您不会覆盖虚拟析构函数
bit different because a virtual

136
00:04:48,839 --> 00:04:51,490
有点不同，因为您不会覆盖虚拟析构函数
destructor you're not overriding the

137
00:04:51,689 --> 00:04:53,259
析构函数，您只是添加了一种析构函数，也就是说，如果
destructor you're just adding a

138
00:04:53,459 --> 00:04:55,449
析构函数，您只是添加了一种析构函数，也就是说，如果
destructor kind of so in other words if

139
00:04:55,649 --> 00:04:57,759
我将这个破坏性的基准更改为虚拟基准，实际上
I change the base this base destructive

140
00:04:57,959 --> 00:04:59,590
我将这个破坏性的基准更改为虚拟基准，实际上
to be virtual it's actually going to

141
00:04:59,790 --> 00:05:01,300
都调用它会最终调用派生的析构函数，然后继续调用
call both it's gonna end up calling that

142
00:05:01,500 --> 00:05:03,340
都调用它会最终调用派生的析构函数，然后继续调用
derived destructor first and then go up

143
00:05:03,540 --> 00:05:05,110
并称为基础层，我们将在一分钟内看到它，因此
the hierarchy and also call the base one

144
00:05:05,310 --> 00:05:07,629
并称为基础层，我们将在一分钟内看到它，因此
we'll see that in a minute so to do this

145
00:05:07,829 --> 00:05:09,850
我们需要做的就是简单地标记一下，或者首先让我们谈谈
what we need to do is simply just mark

146
00:05:10,050 --> 00:05:12,400
我们需要做的就是简单地标记一下，或者首先让我们谈谈
this or actually first of all let's talk

147
00:05:12,600 --> 00:05:14,079
关于为什么我们甚至需要很好地调用派生的析构函数的问题
about the problem why do we even need to

148
00:05:14,279 --> 00:05:15,939
关于为什么我们甚至需要很好地调用派生的析构函数的问题
call the derived destructor well

149
00:05:16,139 --> 00:05:18,340
考虑这个例子，也许我们在这里有一个成员，例如一个int数组或
consider this example maybe we had a

150
00:05:18,540 --> 00:05:20,530
考虑这个例子，也许我们在这里有一个成员，例如一个int数组或
member here like an int array or

151
00:05:20,730 --> 00:05:21,850
我们实际上在构造函数中的堆上分配了一些东西
something that was allocated on the heap

152
00:05:22,050 --> 00:05:24,370
我们实际上在构造函数中的堆上分配了一些东西
here in the constructor we actually

153
00:05:24,569 --> 00:05:26,710
像这样分配它，然后在析构函数中我们决定我们需要
allocate it like this maybe and then in

154
00:05:26,910 --> 00:05:28,180
像这样分配它，然后在析构函数中我们决定我们需要
the destructor we decide that we need to

155
00:05:28,379 --> 00:05:30,730
在这种情况下，如果我们使用当前代码仅按f5键，则立即删除该数组
delete that array now in this scenario

156
00:05:30,930 --> 00:05:33,340
在这种情况下，如果我们使用当前代码仅按f5键，则立即删除该数组
if we just hit f5 with this current code

157
00:05:33,540 --> 00:05:34,990
这里发生的实际情况是因为您可以看到它没有在呼叫
what's actually happening over here

158
00:05:35,189 --> 00:05:36,730
这里发生的实际情况是因为您可以看到它没有在呼叫
because you can see it's not calling

159
00:05:36,930 --> 00:05:39,009
那派生的析构函数，但是它当然是在调用派生的构造函数
that derived destructor but it is of

160
00:05:39,209 --> 00:05:40,960
那派生的析构函数，但是它当然是在调用派生的构造函数
course calling the derived constructor

161
00:05:41,160 --> 00:05:42,550
显然正在发生的事情是我们将所有这些内存分配了20个字节
what's obviously happening is that we're

162
00:05:42,750 --> 00:05:44,259
显然正在发生的事情是我们将所有这些内存分配了20个字节
allocating all of this memory 20 bytes

163
00:05:44,459 --> 00:05:46,030
在这里，然后我们实际上从未调用过这行代码，因为
here and then we're never actually

164
00:05:46,230 --> 00:05:47,980
在这里，然后我们实际上从未调用过这行代码，因为
calling this line of code because the

165
00:05:48,180 --> 00:05:49,629
析构函数未调用，因此我们永远不会删除该堆分配的数组
destructors not called so we're never

166
00:05:49,829 --> 00:05:51,850
析构函数未调用，因此我们永远不会删除该堆分配的数组
deleting that heap allocated array which

167
00:05:52,050 --> 00:05:54,310
意味着我们有内存泄漏，那么我们该如何解决这个问题呢？
means that we have a memory leak so how

168
00:05:54,509 --> 00:05:55,960
意味着我们有内存泄漏，那么我们该如何解决这个问题呢？
do we go about solving this well all we

169
00:05:56,160 --> 00:05:58,000
真正需要做的只是将这个基本析构函数标记为虚拟，这意味着
really need to do here is just mark this

170
00:05:58,199 --> 00:06:00,670
真正需要做的只是将这个基本析构函数标记为虚拟，这意味着
base destructor as virtual which means

171
00:06:00,870 --> 00:06:02,800
基本上有可能将该类扩展为
that there is basically the possibility

172
00:06:03,000 --> 00:06:05,079
基本上有可能将该类扩展为
for this class to be extended to be

173
00:06:05,279 --> 00:06:08,079
子类化，我们可能包括一个也需要被调用的析构函数
subclassed and we might be including a

174
00:06:08,279 --> 00:06:10,000
子类化，我们可能包括一个也需要被调用的析构函数
destructor that also needs to get called

175
00:06:10,199 --> 00:06:12,280
这基本上告诉我们我们嘿，如果需要，您需要致电派生的破坏者
this basically tell us above us hey you

176
00:06:12,480 --> 00:06:14,680
这基本上告诉我们我们嘿，如果需要，您需要致电派生的破坏者
need to call the derived disruptors if

177
00:06:14,879 --> 00:06:16,990
他们在场，所以现在让我们按f5键，看看我们可以得到什么，很酷！
they're present so now let's hit f5 and

178
00:06:17,189 --> 00:06:18,819
他们在场，所以现在让我们按f5键，看看我们可以得到什么，很酷！
see what we get ok cool check this out

179
00:06:19,019 --> 00:06:20,680
因此，我们现在具有与第二个示例完全相同的行为，即
so we have the exact same behavior now

180
00:06:20,879 --> 00:06:22,600
因此，我们现在具有与第二个示例完全相同的行为，即
as we did in the second example that

181
00:06:22,800 --> 00:06:24,100
派生的结构首先被调用，然后银行的析构函数被调用
derived a structure is called first and

182
00:06:24,300 --> 00:06:25,720
派生的结构首先被调用，然后银行的析构函数被调用
then the bank's destructor is called

183
00:06:25,920 --> 00:06:27,280
像这样，这意味着该数组确实被清理了，
like this which means that this array

184
00:06:27,480 --> 00:06:29,259
像这样，这意味着该数组确实被清理了，
does indeed get cleaned up and

185
00:06:29,459 --> 00:06:31,900
即使我们确实将其视为多态类型
everyone's happy even if we do treat it

186
00:06:32,100 --> 00:06:33,579
即使我们确实将其视为多态类型
kind of as the polymorphic type in a

187
00:06:33,779 --> 00:06:34,160
感觉到解决该问题，或者我们将其视为该基础
sense that

188
00:06:34,360 --> 00:06:36,739
感觉到解决该问题，或者我们将其视为该基础
address it or we treat it like that base

189
00:06:36,939 --> 00:06:39,110
无论如何，我希望大家都能喜欢这类型的视频
class type anyway I hope you guys enjoy

190
00:06:39,310 --> 00:06:41,300
无论如何，我希望大家都能喜欢这类型的视频
this video very simple bit of a quick

191
00:06:41,500 --> 00:06:44,239
当您在课堂上写作时，提示非常重要
tip really important whenever you're

192
00:06:44,439 --> 00:06:46,699
当您在课堂上写作时，提示非常重要
writing whenever you're on in class that

193
00:06:46,899 --> 00:06:48,829
您基本上会在扩展或者可能是子类
you will be extending or that might be

194
00:06:49,029 --> 00:06:50,119
您基本上会在扩展或者可能是子类
subclass whenever you're basically

195
00:06:50,319 --> 00:06:51,980
允许将一个类子类化，您需要200％声明您的析构函数为
permitting a class to be subclassed

196
00:06:52,180 --> 00:06:55,819
允许将一个类子类化，您需要200％声明您的析构函数为
you need 200% declare your destructor as

197
00:06:56,019 --> 00:06:58,550
虚拟的，否则没人能够安全地扩展该课程
virtual otherwise no one's going to be

198
00:06:58,750 --> 00:07:00,619
虚拟的，否则没人能够安全地扩展该课程
able to safely extend that class

199
00:07:00,819 --> 00:07:02,660
包括自己在内，因为如果这样做，就无法使用析构函数
including yourself because if you do

200
00:07:02,860 --> 00:07:04,670
包括自己在内，因为如果这样做，就无法使用析构函数
that you can't use the destructor

201
00:07:04,870 --> 00:07:06,829
因为如果您正在最好地治疗该班级，它将永远不会被调用
because it will never get called if

202
00:07:07,029 --> 00:07:08,899
因为如果您正在最好地治疗该班级，它将永远不会被调用
you're treating that class by its best

203
00:07:09,098 --> 00:07:10,369
类型，也可能是您将其传递给函数的情况，
type which might be the case that you're

204
00:07:10,569 --> 00:07:12,319
类型，也可能是您将其传递给函数的情况，
passing it into a function as well and

205
00:07:12,519 --> 00:07:14,540
也许那个函数只把它当作基本指针，然后将其删除
maybe that function only takes it as

206
00:07:14,740 --> 00:07:16,610
也许那个函数只把它当作基本指针，然后将其删除
like a base pointer and then deletes it

207
00:07:16,810 --> 00:07:18,499
还是做了这件事，无论如何都有例子，所以一定要确保
or does whatever it does this is have

208
00:07:18,699 --> 00:07:20,088
还是做了这件事，无论如何都有例子，所以一定要确保
example anyway so definitely make sure

209
00:07:20,288 --> 00:07:21,920
如果您允许该子程序，则您声明析构函数是虚拟的
that you are declaring the destructors

210
00:07:22,120 --> 00:07:24,050
如果您允许该子程序，则您声明析构函数是虚拟的
was virtual if you're allowing that sub

211
00:07:24,250 --> 00:07:26,269
如果您喜欢这部影片，实际上会进行分类，请按“赞”按钮
classing to actually happen if you liked

212
00:07:26,468 --> 00:07:27,619
如果您喜欢这部影片，实际上会进行分类，请按“赞”按钮
this video please hit the like button

213
00:07:27,819 --> 00:07:28,790
您也可以参加热情洋溢的会议来帮助支持该系列-中国
you can also help support the series by

214
00:07:28,990 --> 00:07:30,290
您也可以参加热情洋溢的会议来帮助支持该系列-中国
going passionate conference - the China

215
00:07:30,490 --> 00:07:31,819
一如既往，非常感谢我所有可爱的支持者，本系列不会
a huge thank you as always to all my

216
00:07:32,019 --> 00:07:33,379
一如既往，非常感谢我所有可爱的支持者，本系列不会
lovely supporters this series wouldn't

217
00:07:33,579 --> 00:07:35,929
没你们在这儿我下次见
be here without you guys I will see you

218
00:07:36,129 --> 00:07:38,420
没你们在这儿我下次见
next time goodbye

219
00:07:38,620 --> 00:07:43,620
[音乐]
[Music]

