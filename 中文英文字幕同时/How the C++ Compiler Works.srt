﻿1
00:00:00,030 --> 00:00:03,009
hey little guys my name is eterno and welcome back to my sleepless blood
嘿，小伙子们，我叫eterno，欢迎回到我不眠之夜

2
00:00:03,209 --> 00:00:06,309
series so today we're going to learn all about how the people applause compiler
系列，所以今天我们将学习人们如何赞扬编译器的所有知识

3
00:00:06,509 --> 00:00:10,390
works so let's take a step back and think about this for a minute what is
起作用，所以让我们退后一步，仔细考虑一下这是什么

4
00:00:10,589 --> 00:00:13,960
the big picture here what is a simple source compiler actually responsible for
这里的大图是一个简单的源代码编译器实际上负责什么

5
00:00:14,160 --> 00:00:19,210
so we write our C++ code as text if that's all it is it's just a text file
因此，如果仅此而已，我们就将C ++代码编写为文本，这仅仅是一个文本文件

6
00:00:19,410 --> 00:00:23,169
and then we need some way to transform that text into an actual application
然后我们需要某种方式将文本转换为实际的应用程序

7
00:00:23,368 --> 00:00:27,730
that our computer can run in going from that text form to an actual executable
我们的计算机可以从该文本形式运行到实际的可执行文件

8
00:00:27,929 --> 00:00:32,168
binary we basically have two main operations that need to happen one of
二进制文件，我们基本上有两个主要的操作需要发生

9
00:00:32,368 --> 00:00:35,858
them is called compiling and one of them is called linking in this video we're
他们被称为编译，其中一个被称为链接

10
00:00:36,058 --> 00:00:39,518
just going to talk about compiling I've actually made another video specifically
只是谈论编译，我实际上专门制作了另一个视频

11
00:00:39,719 --> 00:00:42,788
covering linking so you might want to check that out as well the link will be
涵盖了链接，因此您可能还想检查一下链接是否正确

12
00:00:42,988 --> 00:00:46,059
in the description below so with that being said the only thing that the C++
在下面的描述中，这样说来就是C ++唯一的东西

13
00:00:46,259 --> 00:00:50,108
compiler actually needs to do is to take our text files and convert them into an
编译器实际上需要做的是获取我们的文本文件并将其转换为

14
00:00:50,308 --> 00:00:53,979
intermediate format called an object file those object files can then be
称为目标文件的中间格式，然后这些目标文件可以是

15
00:00:54,179 --> 00:00:56,979
passed onto the linker and the link could do all of its linking things but
传递给链接器，链接可以完成其所有链接操作，但

16
00:00:57,179 --> 00:00:59,829
anyway we're talking about compiling here the compiler actually does several
无论如何，我们在这里谈论的是编译器实际上做了几个

17
00:01:00,030 --> 00:01:04,090
things when it produces these object files firstly it needs to pre-process
首先生成这些目标文件时需要进行预处理

18
00:01:04,290 --> 00:01:08,619
our code which means that any preprocessor statements get evaluated
我们的代码，这意味着所有预处理器语句都会被求值

19
00:01:08,819 --> 00:01:12,159
then and there once I've heard is include processed we move on to more or
然后，一旦我听说包含处理已完成，我们将继续处理更多或

20
00:01:12,359 --> 00:01:17,079
less tokenizing and pausing and basically sorting out this english c++
更少的分词和暂停，基本上可以解决这个英语c ++ 

21
00:01:17,280 --> 00:01:21,519
language into a format that the compiler can actually understand and reason with
语言转换成编译器可以实际理解和推理的格式

22
00:01:21,719 --> 00:01:24,640
this basically results in something called an abstract syntax tree being
这基本上导致了所谓的抽象语法树

23
00:01:24,840 --> 00:01:28,269
created which is basically a representation of our code but as an
创建的基本上代表了我们的代码，但作为

24
00:01:28,469 --> 00:01:32,528
abstract syntax tree the compilers job at the end of the day is to convert all
最终，编译器的抽象语法树是将所有

25
00:01:32,728 --> 00:01:38,469
of our code into either constant data or instructions once the compiler has
一旦编译器将我们的代码转换为常量数据或指令

26
00:01:38,670 --> 00:01:43,090
created this abstract syntax tree it can begin actually generating code now this
创建了这个抽象语法树，它现在可以开始实际生成代码了

27
00:01:43,290 --> 00:01:48,009
code is going to be the actual machine code that our CPU will execute we also
代码将是我们的CPU将执行的实际机器代码

28
00:01:48,209 --> 00:01:51,789
wind up with various other data such as a place to store all of our constant
完成各种其他数据，例如存储所有常量的位置

29
00:01:51,989 --> 00:01:56,079
variables and that's essentially all the compiler does it's not crazy complicated
变量，基本上就是编译器所做的一切，这并不复杂

30
00:01:56,280 --> 00:02:01,359
it of course dogs get very complicated as your actual code complexity grows but
当然，随着您实际代码复杂度的提高，狗也会变得非常复杂，但是

31
00:02:01,560 --> 00:02:04,628
that is the gist of it that's all that it does we're going to go ahead and jump
这就是它的要旨，我们将继续前进并跳跃

32
00:02:04,828 --> 00:02:08,439
into this and take a look at what each stage actually does so that you guys can
看看这个，看看每个阶段实际做什么，以便你们可以

33
00:02:08,639 --> 00:02:12,789
see how it all works so let's go ok so here we've got a simple hello world
看看一切如何运作，让我们开始吧，所以这里有一个简单的世界

34
00:02:12,989 --> 00:02:14,700
application you might remember this from the house
应用程序，您可能会在家里记得这个

35
00:02:14,900 --> 00:02:18,270
tablecloth Works video that I recently made we basically just got this main
我最近制作的桌布作品视频基本上基本上就是

36
00:02:18,469 --> 00:02:21,780
function which called vlog which is actually defined inside this log does
实际上在此日志中定义的名为vlog的函数

37
00:02:21,979 --> 00:02:25,230
typically file and it simply just prints our message to the screen and we wind up
通常是文件，它只是将我们的消息打印到屏幕上，然后结束

38
00:02:25,430 --> 00:02:28,740
with a simple application which says hello world if we pop over into our
一个简单的应用程序，它会向您问好世界，如果我们跳进我们的

39
00:02:28,939 --> 00:02:31,920
output directory to debug here you can see that it's generated a hello world
输出目录进行调试，您可以看到它生成了一个hello world 

40
00:02:32,120 --> 00:02:36,149
exe file and then back in the project directory in debug its generated a means
 exe文件，然后在调试中回到项目目录中就产生了一种手段

41
00:02:36,348 --> 00:02:40,500
or obj and load obj file so what the compiler has done is as generated object
或obj并加载obj文件，因此编译器所做的是作为生成的对象

42
00:02:40,699 --> 00:02:45,118
files for each of our C++ files for each of our translation units now every CPP
现在每个CPP的每个翻译单元的每个C ++文件的文件

43
00:02:45,318 --> 00:02:49,230
file that our project contains that we actually tell the compiler hey compile
项目包含的文件，实际上告诉编译器嘿

44
00:02:49,430 --> 00:02:53,879
this CPP file every single one of those files will result in an object file
该CPP文件中的每个文件都会产生一个目标文件

45
00:02:54,079 --> 00:02:58,319
these CPP files are things called translation units essentially you have
这些CPP文件实际上就是所谓的翻译单元

46
00:02:58,519 --> 00:03:02,250
to realize that these both love doesn't care about files files are not something
意识到这两者的爱并不关心文件文件不是什么

47
00:03:02,449 --> 00:03:07,289
that exists in C++ for example in Java your class name has to be tied to your
在C ++中存在，例如在Java中，您的类名必须与您的

48
00:03:07,489 --> 00:03:10,379
file name and your folder hierarchy has to be tied to your package and there's
文件名和文件夹层次结构必须绑定到您的软件包，并且

49
00:03:10,579 --> 00:03:15,060
all this going on because Java expects certain files to exist in C++ that is
这一切都是因为Java期望某些文件存在于C ++中，即

50
00:03:15,259 --> 00:03:19,770
not the case there is no such thing as a file a file is just a way to feed the
并非没有文件之类的东西只是文件的一种方式

51
00:03:19,969 --> 00:03:23,460
compiler with source code you're responsible for telling the compiler
带有源代码的编译器，您负责告诉编译器

52
00:03:23,659 --> 00:03:27,569
what kind of file type this is and how the compiler should treat that now of
这是哪种文件类型，以及编译器现在应该如何处理

53
00:03:27,769 --> 00:03:31,740
course if you create a file with the extension CPP the compiler is going to
当然，如果您使用扩展名CPP创建文件，则编译器将

54
00:03:31,939 --> 00:03:36,300
treat that as a C++ file similarly if I make a file with the extension dot C or
如果我使用扩展名C或

55
00:03:36,500 --> 00:03:40,439
dot H the compiler is going to treat the dot C file like a C file and not a
 dot H编译器将把dot C文件视为C文件，而不是

56
00:03:40,639 --> 00:03:44,159
people spot file and it's going to treat the dot H file like a header file these
人们发现文件，并将点H文件像头文件一样对待

57
00:03:44,359 --> 00:03:47,249
are basically just default conventions that are in place you can override any
基本上只是默认约定，您可以覆盖任何

58
00:03:47,449 --> 00:03:50,610
of them and that's just how the compiler will deal with it if you don't tell it
它们，这就是如果您不告诉编译器将如何处理它们的方法

59
00:03:50,810 --> 00:03:53,939
how to deal with it I could go around making sure no files and telling the
如何处理我可以确保没有文件并告诉

60
00:03:54,139 --> 00:03:56,849
compiler to compile that and that would be totally fine as long as I tell the
编译器来编译，只要我告诉

61
00:03:57,049 --> 00:04:01,950
compiler hey this file is a C++ file please compile it like a C++ file so
编译器，此文件是C ++文件，请像C ++文件一样进行编译，因此

62
00:04:02,150 --> 00:04:07,530
just remember files have no meaning ok remember that important so that being
只要记住文件没有意义就可以了，记住这一点很重要

63
00:04:07,729 --> 00:04:11,129
said every C++ file that we feed into the compiler and we tell it this is a
说我们送入编译器的每个C ++文件，我们告诉它这是一个

64
00:04:11,329 --> 00:04:15,060
C++ file please compile it it will compile it as a translation unit and a
 C ++文件，请对其进行编译，它将编译为翻译单元和

65
00:04:15,259 --> 00:04:19,230
translation unit will result in an object file it's actually quite common
翻译单元将生成一个目标文件，它实际上很常见

66
00:04:19,430 --> 00:04:24,389
to sometimes include CPP files in other CPP files and create basically one big
有时将CPP文件包括在其他CPP文件中，并创建一个

67
00:04:24,589 --> 00:04:27,990
CPP file with a lot of Isles in it if you do something like
如果您执行类似以下操作，则CPP文件中会包含很多小岛

68
00:04:28,189 --> 00:04:31,410
that and then you only compile the one CPP file you're going to basically
然后，您只需要编译一个CPP文件即可

69
00:04:31,610 --> 00:04:35,790
result in one translation unit and that's one object file so that's why
结果是一个翻译单元，那是一个目标文件，所以这就是为什么

70
00:04:35,990 --> 00:04:39,449
there's that terminology split between water translation unit is and what does
在水转换单元和什么之间有术语分裂

71
00:04:39,649 --> 00:04:43,290
it BP file actually is because typically file doesn't necessarily have to equal a
 BP文件实际上是因为通常文件不一定必须等于

72
00:04:43,490 --> 00:04:47,129
translation unit however if you just make a project with individual CPP files
但是，如果仅使用单个CPP文件制作项目，则需要翻译单位

73
00:04:47,329 --> 00:04:51,030
and you never include them in each other then yet every security file will be a
并且您永远不会将它们相互包含在一起，那么每个安全文件都会是

74
00:04:51,230 --> 00:04:55,259
translation unit and every CPP file will generate an object file now these are
转换单元和每个CPP文件将生成一个目标文件，现在这些是

75
00:04:55,459 --> 00:04:58,350
actually pretty big you can see this one's 30 kilobytes in this one 46
其实很大，您可以在这46个中看到这个30 KB 

76
00:04:58,550 --> 00:05:02,160
kilobytes the reason to that is because we're including iOS stream and that has
千字节的原因是因为我们包括了iOS流，并且

77
00:05:02,360 --> 00:05:05,370
a lot of self in it so that's why they're so big and because of that
很多自我，所以这就是为什么他们这么大，因为

78
00:05:05,569 --> 00:05:08,189
they're actually pretty complicated so before we dive in and take a look at
它们实际上非常复杂，所以在我们深入研究之前

79
00:05:08,389 --> 00:05:11,970
what's actually in the file let's create something a little bit more simple I'm
文件中实际包含的内容让我们创建一些更简单的内容

80
00:05:12,170 --> 00:05:15,960
going to right click on full file hit add new item it is going to be a C++
要右键单击完整文件，请单击添加新项目，这将是C ++ 

81
00:05:16,160 --> 00:05:20,310
file I'm going to call it math fill favorite eat and hit add over here I'm
我将其称为数学填充最喜欢的食物，然后点击添加

82
00:05:20,509 --> 00:05:23,400
just going to write a very basic multiply function which multiplies two
只是要编写一个非常基本的乘法函数，将两个函数相乘

83
00:05:23,600 --> 00:05:26,699
numbers together I'm not going to include any files in here or anything
数字一起我不会在这里包括任何文件或任何东西

84
00:05:26,899 --> 00:05:29,310
I'm just going to write a very simple function it's going to return an integer
我将要编写一个非常简单的函数，它将返回一个整数

85
00:05:29,509 --> 00:05:33,718
it's going to be called multiply it's going to take two parameters int a and
这将被称为乘法，它将接受两个参数int a和

86
00:05:33,918 --> 00:05:39,689
int B it's then going to create a result variable which stores the result of a
 int B然后将创建一个结果变量，该变量存储a的结果

87
00:05:39,889 --> 00:05:43,920
times B and then we're going to return that result variable nice and simple
乘以B，然后我们将返回结果变量尼斯和简单

88
00:05:44,120 --> 00:05:46,139
that's it let's hit control seven to build that
就是这样，让我们​​按控制七来构建它

89
00:05:46,339 --> 00:05:49,770
file you can see over here that it's built it successfully I'm actually going
文件，您可以在这里看到它已成功构建，我实际上正在

90
00:05:49,970 --> 00:05:52,800
to just resize littlez to do a little bit just to make it easier so now you
只是调整littlez的大小来做一些只是为了使其更容易，所以现在您

91
00:05:53,000 --> 00:05:56,310
can see the output window a bit better if we look back into our output
如果我们回顾一下输出，可以更好地看到输出窗口

92
00:05:56,509 --> 00:05:59,939
directory you can see that we've got this mouse or obj file now and it's four
目录中，您可以看到我们现在有了这个鼠标或obj文件，它是四个

93
00:06:00,139 --> 00:06:03,750
kilobyte before we take a look at what exactly is in that object file let's
千字节，然后我们看一下该目标文件中到底是什么

94
00:06:03,949 --> 00:06:06,410
talk about the first stage of compilation which I mentioned earlier
谈论我之前提到的编译的第一阶段

95
00:06:06,610 --> 00:06:10,020
pre-processing during the pre-processing stage the compiler will basically just
在预处理阶段，编译器基本上只会

96
00:06:10,220 --> 00:06:13,949
go through all of our pre-processing statements and evaluate them the ones
仔细阅读我们所有的预处理语句，然后对它们进行评估

97
00:06:14,149 --> 00:06:18,840
that we commonly use are include define F and F def there are also pragma
我们常用的包括定义F和F def也有杂注

98
00:06:19,040 --> 00:06:21,420
statements which tell the compiler exactly what to do but we'll talk about
告诉编译器确切操作的语句，但我们将在后面讨论

99
00:06:21,620 --> 00:06:24,870
them in other video so let's take a look at one of the most common preprocessor
他们在其他视频中，所以让我们看一下最常见的预处理器之一

100
00:06:25,069 --> 00:06:29,009
statements that we have hash include how does that work so hash include' is
我们具有哈希的语句包括工作原理，因此“哈希包含”为

101
00:06:29,209 --> 00:06:33,000
actually really simple you basically specify which file you want to include
实际上非常简单，您基本上可以指定要包含的文件

102
00:06:33,199 --> 00:06:37,860
and then the preprocessor will open that file read all of its contents and just
然后预处理器将打开该文件并读取其所有内容，然后

103
00:06:38,060 --> 00:06:40,020
paste it into the file where you bro your
将其粘贴到文件中

104
00:06:40,220 --> 00:06:43,650
statement and that's it it's really really simple and I'm about to prove
陈述，就是这样，真的非常简单，我将证明

105
00:06:43,850 --> 00:06:47,100
that so that's over here I'm just going to make a header file I'm gonna right
这样就在这里，我要制作一个头文件，我会正确的

106
00:06:47,300 --> 00:06:50,579
click on the header file to get add new item is going to be a header file and
单击头文件以获取添加新项将是头文件，然后

107
00:06:50,779 --> 00:06:55,560
I'm going to call it and brace and then click Add ok we're going to wipe out
我将其命名为大括号，然后单击“添加”，确定，我们将清除掉

108
00:06:55,759 --> 00:06:59,730
whatever was in this file and I'm just going to type in a closing curly brace
文件中的内容，我只需要在右花括号中输入

109
00:06:59,930 --> 00:07:04,800
that is it that's our entire file so now back in master CPP you can see that we
这就是我们的整个文件，所以现在回到主CPP，您可以看到我们

110
00:07:05,000 --> 00:07:08,430
have reasonably written a closing curly bracket here for our multiply function
在这里为我们的乘法功能合理地写了一个大括号

111
00:07:08,629 --> 00:07:11,699
let's go ahead and wipe that out if we compile our file now by hitting ctrl f7
如果我们现在通过按ctrl f7来编译文件，让我们继续删除它

112
00:07:11,899 --> 00:07:17,009
you can see that the compiler complains about the less brave being unmatched at
您会看到编译器抱怨在

113
00:07:17,209 --> 00:07:20,040
the end of the file so instead of fixing this like a normal person and just
文件的末尾，所以与其像普通人一样修复它，而不仅仅是

114
00:07:20,240 --> 00:07:24,300
adding in our ending braze let's go ahead and include our end brace header
添加我们的末尾钎焊，让我们继续，并包括我们的末尾钎焊头

115
00:07:24,500 --> 00:07:31,110
file so I'll type in hash includes and brace and there we go let's hit ctrl f7
文件，所以我将输入哈希包含和花括号，然后我们按Ctrl F7 

116
00:07:31,310 --> 00:07:35,670
to compile that and look it compiles successfully of course it did because
编译它并看起来它编译成功当然是因为

117
00:07:35,870 --> 00:07:40,439
all the compiler did was open this and break file copy whatever was in here and
编译器所做的所有事情就是打开此文件，并中断文件副本，无论此处和

118
00:07:40,639 --> 00:07:46,829
then just paste it into here and that is it okay header files solved you should
然后将其粘贴到此处，就可以了，头文件已解决，您应该

119
00:07:47,029 --> 00:07:50,129
now know exactly how they work and how you can use them there's actually a way
现在确切地知道了它们的工作方式以及如何使用它们，实际上是有一种方法

120
00:07:50,329 --> 00:07:53,819
we can tell the compiler to output a file which contains the result of all of
我们可以告诉编译器输出一个文件，其中包含所有

121
00:07:54,019 --> 00:07:57,718
these preprocessor evaluations that have happened if we bring back our include
如果我们带回include，将会发生这些预处理器评估

122
00:07:57,918 --> 00:08:01,528
end brace and then right click on our hello world project and hit properties
结束大括号，然后右键单击我们的hello world项目并点击属性

123
00:08:01,728 --> 00:08:06,718
under C C++ and then preprocessor I'm going to set the pre-processed to a file
在C C ++下，然后使用预处理器，我将把预处理的文件设置为

124
00:08:06,918 --> 00:08:10,800
to yes make sure that you're editing your current configuration and platform
是的，请确保您正在编辑当前的配置和平台

125
00:08:11,000 --> 00:08:14,310
so that these settings actually apply let's hit OK and then we're going to
为了使这些设置真正适用，让我们点击OK，然后我们将

126
00:08:14,509 --> 00:08:18,449
just hit control f7 to builders again if we bring up our output directory you'll
如果我们调出输出目录，只需再次将f7控件发送给构建者

127
00:08:18,649 --> 00:08:23,040
see this new dot I file which is our pre-processed CC plus off code let's
看到这个新的点我文件，这是我们的预处理CC加上关闭代码，让我们

128
00:08:23,240 --> 00:08:26,310
open this in a text editor so that we can look at it okay so here you can see
在文本编辑器中打开它，以便我们可以正常查看，在这里您可以看到

129
00:08:26,509 --> 00:08:29,520
what the preprocessor has actually generated you can see that our source
预处理程序实际上生成了什么，您可以看到我们的源

130
00:08:29,720 --> 00:08:34,409
code had this include end brace and yet the preprocessor code has just inserted
代码中包含了大括号，但预处理器代码刚刚插入

131
00:08:34,610 --> 00:08:38,429
our end brace that was in that dot H file that we've included pretty simple
我们在该点H文件中包含的末尾括号非常简单

132
00:08:38,629 --> 00:08:41,620
stuff let's add some more preprocessor statements and
我们添加一些预处理器语句， 

133
00:08:41,820 --> 00:08:46,449
it does so back in our file I'm going to restore our end brace because I'm
这样做确实会回到我们的文件中，因为我

134
00:08:46,649 --> 00:08:49,449
getting tired of looking at that include I'm then going to come up here and
厌倦了看那包括我然后要来这里， 

135
00:08:49,649 --> 00:08:55,089
define something I'm going to define the word integer to be in now don't ask me
定义一些东西，现在我要定义整数这个词，不要问我

136
00:08:55,289 --> 00:08:58,839
why I would ever do this is just an example the design proposals a statement
为什么我会这样做，这只是设计建议的一个示例

137
00:08:59,039 --> 00:09:02,409
will basically just do a search for this word and replace it with whatever
基本上只会搜索这个词并将其替换为

138
00:09:02,610 --> 00:09:06,699
follows so let's replace our end here with the word integer so that we
接下来，让我们在此处用整数一词替换结尾，以便我们

139
00:09:06,899 --> 00:09:12,159
actually return the integer we can also do the same here let's head of ctrl f7
实际上返回整数，我们也可以在此处执行相同操作，让我们按ctrl f7 

140
00:09:12,360 --> 00:09:16,569
and if we look back at our file you can see what's happened it just looks normal
如果我们回头看一下文件，您会发现发生了什么事情看起来很正常

141
00:09:16,769 --> 00:09:20,439
into result if we were to do something stupid here like write the word Cherno
如果我们要在这里做一些愚蠢的事情，例如写下Cherno这个词

142
00:09:20,639 --> 00:09:23,829
and mention ctrl f7 if we go back to our file you can see it
并提到ctrl f7，如果我们返回文件，您可以看到它

143
00:09:24,029 --> 00:09:27,519
now there's channel multiply and channel result pretty cool stuff let's play
现在有频道乘法和频道结果非常棒的东西，让我们玩

144
00:09:27,720 --> 00:09:33,279
around with this a little bit more let's bring back our int we'll get rid of this
再加上一点点，让我们回到我们的诠释，我们将摆脱这一点

145
00:09:33,480 --> 00:09:37,449
define and instead what I'm going to do is actually just use something called if
定义，而实际上，我要使用的是所谓的if 

146
00:09:37,649 --> 00:09:42,819
the if preprocessor statement can let us include or exclude code based on a given
 if预处理器语句可以让我们基于给定的代码包含或排除代码

147
00:09:43,019 --> 00:09:46,659
condition so over here I'm just going to write f1 which in other words means true
条件，所以在这里我只想写f1，换句话说就是true 

148
00:09:46,860 --> 00:09:51,279
and then just write an end s at the end of this function I'll hit ctrl f7 we'll
然后在此函数的末尾写一个end，我将按ctrl f7 

149
00:09:51,480 --> 00:09:54,429
go back to our preparators file and you can see that it looks exactly like it
回到我们的preparators文件，您可以看到它看起来像它

150
00:09:54,629 --> 00:09:58,269
does here without this thing if I go back here and I switch this off by
如果我回到这里，然后通过关闭此功能，则不会

151
00:09:58,470 --> 00:10:02,679
writing in 0 visual studio will fade out our code to show that it's disabled by
在0 Visual Studio中编写代码会淡出我们的代码，以表明已被禁用

152
00:10:02,879 --> 00:10:06,339
here ctrl f7 and take a look at its file we have no code so it's another great
在这里ctrl f7，看看它的文件，我们没有代码，这是另一个很棒的地方

153
00:10:06,539 --> 00:10:10,149
example of a preprocessor statement all right one more we'll look at include
预处理器语句的示例好了，我们再来看一下include 

154
00:10:10,350 --> 00:10:15,519
let's get rid of our s0 and then I'm going to write include iostream
让我们摆脱我们的s0然后我要写include iostream 

155
00:10:15,720 --> 00:10:20,769
the massive massive iostream let's take control f7 let's look back a bit and
大量的iostream让我们控制f7让我们回顾一下

156
00:10:20,970 --> 00:10:29,500
well take a look at this we have in here 50,000 623 lines and there's our
好吧，我们在这里有50,000 623行， 

157
00:10:29,700 --> 00:10:32,949
function at the very bottom and then look this is all that include iostream
功能最底部，然后看这就是包含iostream的所有内容

158
00:10:33,149 --> 00:10:36,909
has done now of course iostream also includes other files so it's kind of
现在已经完成了，当然iostream还包括其他文件，因此

159
00:10:37,110 --> 00:10:41,559
like rolling a snowball down a hill you can now hopefully see why those objects
就像在山上滚雪球一样，现在您可以希望看到为什么这些物体

160
00:10:41,759 --> 00:10:44,889
class was so big because they included iostream and that is a lot of code
类之所以这么大是因为它们包括了iostream，并且这是很多代码

161
00:10:45,090 --> 00:10:49,179
alright great so that's the preprocessor once that stage has done we can move on
很好，这是预处理器，一旦完成该阶段，我们就可以继续

162
00:10:49,379 --> 00:10:52,419
to actually compiling our simple plus code into
实际将我们的简单加号代码编译成

163
00:10:52,620 --> 00:10:55,809
chain code if we go back to our project here I'm going to get rid of the include
链代码，如果我们回到这里的项目，我将摆脱包含

164
00:10:56,009 --> 00:10:59,079
because we don't need it and I'm just going to hit ctrl f7 you should now see
因为我们不需要它，而我只是要按ctrl f7，现在您应该看到

165
00:10:59,279 --> 00:11:02,289
in our progressive file that we're back to normal and in fact I'm actually going
在我们的渐进文件中，我们已经恢复正常，实际上我正在

166
00:11:02,490 --> 00:11:06,399
to go into hi world hit properties and then disable that preprocessor to a file
进入世界流行的属性，然后将该预处理器禁用到文件

167
00:11:06,600 --> 00:11:09,879
if you actually read what presses to a file does you'll see that it actually
如果您实际读取了按文件的内容，您会看到它实际上是

168
00:11:10,080 --> 00:11:13,359
does not produce an obj file so we need to disable it so that we can actually
不会产生obj文件，因此我们需要禁用它，以便我们可以实际

169
00:11:13,559 --> 00:11:18,309
build our project I've said okay and then we'll hit ctrl f7 to build our 50p
建立我们的项目，我说好了，然后我们按ctrl f7建立50p 

170
00:11:18,509 --> 00:11:22,329
file you'll see that we should now get a mastered obj file which is actually up
文件，您将看到我们现在应该获得一个已掌握的obj文件， 

171
00:11:22,529 --> 00:11:26,649
to date so let's take a look at what actually inside our obj file if we open
到目前为止，让我们看一下如果我们打开obj文件中的实际内容

172
00:11:26,850 --> 00:11:29,229
this file with the text editor you'll see that it's binary
使用文本编辑器查看该文件，您将看到它是二进制文件

173
00:11:29,429 --> 00:11:32,589
which doesn't really help up too much but part of what is actually inside here
并没有真正帮助太多，但实际上部分在这里

174
00:11:32,789 --> 00:11:37,059
is the machine code that our CPU will run when we call this multiply function
是我们调用此乘法函数时CPU将运行的机器代码

175
00:11:37,259 --> 00:11:41,319
so because this is just binary and completely unreadable let's convert it
所以，因为这只是二进制文件，而且完全不可读，所以我们将其转换为

176
00:11:41,519 --> 00:11:44,829
into a form that might actually be more readable by our there are several ways
变成一种实际上可能更容易被我们理解的形式

177
00:11:45,029 --> 00:11:47,500
we could do this but I'm just going to use visual studio I'll right click on
我们可以做到这一点，但是我将使用Visual Studio，我将右键单击

178
00:11:47,700 --> 00:11:52,000
hello world and hit properties under C C++ and then output file I'm going to
您好世界，并在C C ++下命中属性，然后输出文件

179
00:11:52,200 --> 00:11:55,929
set assembler output to be set to assembly only listing and then I'm going
将汇编程序输出设置为仅汇编列表，然后我要

180
00:11:56,129 --> 00:12:00,459
to hit OK and we're going to hit ctrl f7 inside our output directory you should
点击确定，我们将在输出目录中点击ctrl f7 

181
00:12:00,659 --> 00:12:05,019
see a math dot ASM file let's go ahead and open that with a text editor okay so
看到一个数学点ASM文件，让我们继续并使用文本编辑器打开它，这样

182
00:12:05,220 --> 00:12:08,500
this is basically a readable result of what that objects file actually contain
这基本上是对象文件实际包含的内容的可读结果

183
00:12:08,700 --> 00:12:12,039
if we go down over here you'll see that we actually have this function called
如果我们走到这里，您会看到我们实际上有一个称为

184
00:12:12,240 --> 00:12:15,879
multiply and then we have a bunch of assembly instructions these are the
乘，然后我们有一堆汇编指令，这些是

185
00:12:16,080 --> 00:12:19,179
actual instructions that our CPU will execute when we were on the function I'm
当我们使用函数时，CPU将执行的实际指令

186
00:12:19,379 --> 00:12:22,959
not going to go into huge detail about all the dissembler code now I might save
现在我可能不会保存所有反汇编程序代码的详细信息

187
00:12:23,159 --> 00:12:25,839
that for another video but if we take a look over here you'll see that our
对于另一个视频，但是如果我们在这里看一看，您会发现我们的

188
00:12:26,039 --> 00:12:29,559
multiplication operation actually happens here basically we load the a
乘法运算实际上发生在这里，基本上我们加载一个

189
00:12:29,759 --> 00:12:34,149
variable into our EAX register and then we perform an I mul instruction which is
变量放入我们的EAX寄存器，然后执行一条I mul指令

190
00:12:34,350 --> 00:12:38,349
a multiplication instruction on the B variable and that a variable we're then
 B变量的乘法指令，然后我们就是一个变量

191
00:12:38,549 --> 00:12:41,919
storing the result of that in a variable called result and moving it back into
将结果存储在名为result的变量中，然后将其移回

192
00:12:42,120 --> 00:12:45,609
EAX to return it the reason this kind of double move happens because I actually
 EAX将其退回是因为我实际上发生了这种双重举动的原因

193
00:12:45,809 --> 00:12:50,019
made a variable called result and then returned it instead of just returning a
制作了一个名为result的变量，然后将其返回，而不是仅仅返回一个

194
00:12:50,220 --> 00:12:55,839
times B that's why we get this moving EAX into result and then moving result
乘以B，这就是为什么我们将移动EAX转换为结果然后移动结果的原因

195
00:12:56,039 --> 00:13:00,399
into EAX which is completely redundant this is another great example of why if
到完全多余的EAX中，这是为什么

196
00:13:00,600 --> 00:13:03,159
you set your compiler not to optimize you're going to find out with slow code
您将编译器设置为不进行优化，以发现缓慢的代码

197
00:13:03,360 --> 00:13:05,639
because it's doing extra stuff like this for no reason if I
因为我无缘无故地在做这样的事情

198
00:13:05,840 --> 00:13:09,449
go back to my code and I actually get rid of that result variable by just
回到我的代码，实际上我只是摆脱了那个结果变量

199
00:13:09,649 --> 00:13:13,379
returning a times B and then compile this you'll see the assembly looks
返回时间B，然后编译它，您将看到程序集的外观

200
00:13:13,580 --> 00:13:17,609
slightly different because we're just doing a mole on B and E ax and then
稍有不同，因为我们只是在B和E轴上进行磨牙，然后

201
00:13:17,809 --> 00:13:20,039
that's it the ax is actually going to contain our
这就是斧头实际上将包含我们的

202
00:13:20,240 --> 00:13:23,849
return value now all this may look like a lot of code and that's because we're
返回值，现在所有这些可能看起来像很多代码，这是因为我们

203
00:13:24,049 --> 00:13:27,149
actually compiling in debug which doesn't do any optimization that and
实际上在调试中进行编译，而调试并没有做任何优化， 

204
00:13:27,350 --> 00:13:31,139
does extra things to make sure that our code is as diverse as possible and as
要做一些额外的事情来确保我们的代码尽可能地多样化

205
00:13:31,340 --> 00:13:35,459
easy to debug as possible if we go back into our project and right click here
如果我们返回到项目并右键单击此处，则易于调试

206
00:13:35,659 --> 00:13:40,169
hit properties I'm going to go over here into optimization under the debug
命中属性我将在调试下转到这里进行优化

207
00:13:40,370 --> 00:13:44,069
configuration let's select maximize speed if you try
配置让我们选择最大化速度（如果尝试） 

208
00:13:44,269 --> 00:13:47,309
and compile this now it'll actually give you an error because you'll see that OH
并立即编译它，实际上会给您一个错误，因为您会看到

209
00:13:47,509 --> 00:13:51,479
- and RTC is actually incompatible so we'll have to go back over here into
 -而且RTC实际上是不兼容的，所以我们必须回到这里

210
00:13:51,679 --> 00:13:56,189
code generation and make sure that our basic runtime checks are set to default
代码生成，并确保将我们的基本运行时检查设置为默认值

211
00:13:56,389 --> 00:13:59,279
which basically won't perform runtime checks this is basically just code that
基本上不会执行运行时检查，这基本上只是

212
00:13:59,480 --> 00:14:01,349
the compiler will insert to help us with debugging
编译器将插入以帮助我们进行调试

213
00:14:01,549 --> 00:14:05,519
let's take control of seven and look at that assembly file again wow that looks
让我们控制七个并再次查看该程序集文件

214
00:14:05,720 --> 00:14:09,449
a lot smaller we've basically just got our variables being loaded into a
小得多，我们基本上只是将变量加载到

215
00:14:09,649 --> 00:14:13,740
register and then the multiplication and then that's it pretty simple stuff you
注册，然后乘法，那就是非常简单的东西

216
00:14:13,940 --> 00:14:16,559
should now have a basic idea of what the compiler actually does when you tell it
现在，您应该基本了解编译器在告诉它时的实际作用

217
00:14:16,759 --> 00:14:20,309
to optimize it optimizes this is a pretty simple example so let's take a
优化它优化这是一个非常简单的示例，让我们来看一个

218
00:14:20,509 --> 00:14:22,769
look at something a bit more advanced we'll take a look at a slightly
看一些更高级的东西，我们会稍微看一下

219
00:14:22,970 --> 00:14:26,789
different example in which case we don't actually take anything in here but I
不同的例子，在这种情况下，我们实际上什么都不装，但是我

220
00:14:26,990 --> 00:14:31,229
decide to do something like five times - we'll save that file I'll go into my
决定执行五次操作-我们将保存该文件，然后将其放入

221
00:14:31,429 --> 00:14:36,089
properties and make sure that I disable optimization so let's hit control f7 and
属性，并确保我禁用了优化功能，所以让我们点击控件f7和

222
00:14:36,289 --> 00:14:39,599
take a look at our file you can see that what it's done is actually really simple
看一下我们的文件，您会发现它的完成实际上非常简单

223
00:14:39,799 --> 00:14:44,250
it's simply moved ten into our EAX register which is a register that will
只需将其移到我们的EAX寄存器中的十位， 

224
00:14:44,450 --> 00:14:47,609
actually store our return value in so if we take a look at our code again it's
实际存储我们的返回值，所以如果我们再次看一下代码， 

225
00:14:47,809 --> 00:14:53,279
basically just simplified our five times two to be ten because of course has no
基本上只是将我们的五乘二简化为十，因为当然没有

226
00:14:53,480 --> 00:14:57,689
need to do something like five times to two constant values at runtime there's
需要在运行时做五次到两个常数的操作

227
00:14:57,889 --> 00:15:01,169
something called constant folding where anything that is constant that can be
称为恒定折叠的东西，其中任何恒定的东西都可以是

228
00:15:01,370 --> 00:15:05,069
worked out at compile time is let's make things more interesting by involving
在编译时得出的结论是，通过使

229
00:15:05,269 --> 00:15:09,379
another function so for example I'm actually going to write a log function
另一个函数，例如，我实际上要编写一个日志函数

230
00:15:09,580 --> 00:15:14,250
which is going to log a certain message of course I don't actually want to make
这当然会记录某些我实际上不想发出的消息

231
00:15:14,450 --> 00:15:15,969
it log anything because that would mean I
它记录任何东西，因为那意味着我

232
00:15:16,169 --> 00:15:19,389
have to include iostream which will drastically complicate this so I'm is
必须包括iostream，这将使这个问题大大复杂化，所以我是

233
00:15:19,589 --> 00:15:22,870
going to get it to return that message that it received over here and multiply
将其返回以返回在此接收到的消息并相乘

234
00:15:23,070 --> 00:15:27,279
I'm going to call log with the word multiplier I want to change this back to
我要用乘数一词来调用log，我想把它改回

235
00:15:27,480 --> 00:15:35,169
be a and B and watertown a times B let's take control of seven all right so let's
是A，B和watertown是B，让我们控制七个吧，所以

236
00:15:35,370 --> 00:15:38,229
take a look at what our compiler has generated if we scroll down a bit you'll
看一下我们的编译器生成的内容，如果我们向下滚动一点，您会

237
00:15:38,429 --> 00:15:41,979
see that we've got this log function which doesn't really do much but this
看到我们有这个日志功能，它实际上并没有做很多，但是

238
00:15:42,179 --> 00:15:45,698
actually will just return our message you can see that it's moving our message
实际上只会返回我们的消息，您可以看到它正在移动我们的消息

239
00:15:45,899 --> 00:15:49,659
pointer into EAX which is our return register as we've established so this is
指向EAX的指针，这是我们已经建立的返回寄存器，所以这是

240
00:15:49,860 --> 00:15:52,839
the log function if we scroll up a little bit you'll see the multiply
 log函数，如果我们向上滚动一点，您将看到乘

241
00:15:53,039 --> 00:15:58,240
function and then all we have here is a call to log so right before we actually
函数，然后我们在这里只有一个日志调用，因此在我们实际执行之前

242
00:15:58,440 --> 00:16:03,549
do our multiplication by using the I mul we actually call this log function now
通过使用I mul进行乘法，我们现在实际调用此日志函数

243
00:16:03,750 --> 00:16:07,149
you might be wondering why this log function is decorated by what seems like
您可能想知道为什么此日志功能由看起来像这样的装饰

244
00:16:07,350 --> 00:16:11,740
random characters and at signs this is actually the function signature
随机字符和符号，这实际上是函数签名

245
00:16:11,940 --> 00:16:14,799
this needs to uniquely define your function we'll talk more about this in
这需要唯一地定义您的函数，我们将在下面进一步讨论

246
00:16:15,000 --> 00:16:19,089
the linking video but essentially when we have multiple OBJ's and our functions
链接视频，但实际上是当我们有多个OBJ和我们的功能时

247
00:16:19,289 --> 00:16:22,628
are defined in multiple obj it's going to be linkers job to link all them
在多个obj中定义，它将是链接器工作，以链接所有它们

248
00:16:22,828 --> 00:16:25,568
together and the way that it's going to do that is going to look up this
在一起，它要做的方式就是查找

249
00:16:25,769 --> 00:16:29,169
function signature so all you need to know here is that we're calling this log
函数签名，所以您在这里需要知道的就是我们正在调用此日志

250
00:16:29,370 --> 00:16:32,529
function that's what the compiler will actually do when you call a function it
函数是编译器在调用函数时实际执行的操作

251
00:16:32,730 --> 00:16:36,128
will generate a call instruction now in this case it might be a little bit
在这种情况下将立即生成一个呼叫指令

252
00:16:36,328 --> 00:16:39,878
stupid because you can see that we're simply calling log we're not even
愚蠢的，因为您可以看到我们只是在调用log，我们甚至没有

253
00:16:40,078 --> 00:16:43,779
storing the return value basically this could be optimized quite a bit if we go
基本上存储返回值，如果我们去的话，可以优化很多

254
00:16:43,980 --> 00:16:49,179
back here and we turn on optimization to maximize speed and in control of 7
回到这里，我们打开优化以最大化速度并控制7 

255
00:16:49,379 --> 00:16:54,159
you'll actually see that that just disappears entirely yep the compiler
您实际上会看到，编译器完全消失了

256
00:16:54,360 --> 00:16:58,120
just decided that does nothing I'm going to remove that code but you should
刚刚决定不执行任何操作，我将删除该代码，但是您应该

257
00:16:58,320 --> 00:17:01,839
basically now understand the gist of how the compiler work it will take our
现在基本上了解了编译器如何工作的要点

258
00:17:02,039 --> 00:17:05,440
source files and output an object file which contains machine code and any
源文件并输出一个包含机器代码和任何其他内容的目标文件

259
00:17:05,640 --> 00:17:08,858
other constant data that we've defined that's basically it and now that we've
我们定义的其他常量数据基本上就是它，现在我们已经

260
00:17:09,058 --> 00:17:12,489
got these object files we can link them into one executable which contains all
得到了这些目标文件后，我们可以将它们链接到一个包含所有内容的可执行文件中

261
00:17:12,689 --> 00:17:15,460
of the machine code that we actually need to run and that's how we make a
我们实际需要运行的机器代码，这就是我们制作一个

262
00:17:15,660 --> 00:17:18,999
program in C++ pretty simple make sure that you check out my video on how to
 C ++程序非常简单，请确保您查看了有关如何

263
00:17:19,199 --> 00:17:22,149
link work so that you can see the next step but apart from that I hope you guys
链接工作，以便您可以看到下一步，但除此之外，我希望你们

264
00:17:22,349 --> 00:17:24,878
enjoyed this video hope you learned something new you should now have a
欣赏这个视频，希望您学到了一些新知识，现在应该有一个

265
00:17:25,078 --> 00:17:27,579
basic understanding of what the simple of compiler actually
基本了解什么是简单的编译器

266
00:17:27,779 --> 00:17:30,669
and that's going to be really important when it comes to debugging and also when
当涉及到调试以及

267
00:17:30,869 --> 00:17:33,399
we get into more advanced topics in the future make sure you follow me on
我们将来会涉及更高级的主题，请确保您关注我

268
00:17:33,599 --> 00:17:36,970
Twitter and Instagram and if you really like this you can support me on patreon
 Twitter和Instagram，如果您真的喜欢，可以在patreon上支持我

269
00:17:37,170 --> 00:17:43,819
I'll see you guys next time goodbye warm
下次再见你们再见

