﻿1
00:00:00,000 --> 00:00:02,859
hey what's up guys my name is Archana welcome back to my staples loss series
嘿，大家好，我叫Archana，欢迎回到我的主食损失系列

2
00:00:03,060 --> 00:00:06,099
today we're gonna be talking all about how you can set up multiple projects in
今天我们将要讨论的是如何在

3
00:00:06,299 --> 00:00:09,580
Visual Studio and how you can create a library that you can use in all of your
 Visual Studio以及如何创建可在所有数据库中使用的库

4
00:00:09,779 --> 00:00:12,100
projects so this is something that's really important if you're working on
项目，所以这是非常重要的，如果您正在从事

5
00:00:12,300 --> 00:00:15,760
anything kind of sizable not only does it help you to create a module or a
任何种类的大小不仅有助于您创建模块或

6
00:00:15,960 --> 00:00:19,839
library with your code and reuse that code multiple times but it also lets you
库与您的代码并多次重复使用该代码，但它也使您可以

7
00:00:20,039 --> 00:00:23,379
mix languages and that's something that might be important to you if again
混合语言，这对您来说可能很重要

8
00:00:23,579 --> 00:00:27,310
you're working on something fairly large so today we're just gonna stick with C++
您正在从事相当大的工作，所以今天我们要坚持使用C ++ 

9
00:00:27,510 --> 00:00:30,339
and we're gonna talk about how you can basically create one project that is
我们将讨论如何基本创建一个项目

10
00:00:30,539 --> 00:00:34,599
compiled as a library as specifically a static library and then link that into
编译为库（特别是静态库），然后将其链接到

11
00:00:34,799 --> 00:00:38,049
an executable and how that all kind of works in a visual studio so let's take a
可执行文件以及Visual Studio中的各种工作方式，让我们来看一下

12
00:00:38,250 --> 00:00:40,959
look so here I'm just starting with a completely empty folder because a folder
看起来所以这里我只是从一个完全空的文件夹开始，因为一个文件夹

13
00:00:41,159 --> 00:00:44,858
called C++ and it's going to contain all these projects of money back in Visual
称为C ++，它将在Visual中包含所有这些金钱项目

14
00:00:45,058 --> 00:00:48,969
Studio I'm just going to go over here and select file new project so I'm going
 Studio我要去这里并选择文件新项目，所以我要去

15
00:00:49,170 --> 00:00:52,448
to be setting up a brand new solution which will contain my multiple projects
建立一个全新的解决方案，其中将包含我的多个项目

16
00:00:52,649 --> 00:00:55,570
so I already here on the templates and visual C++ I'm just gonna just like
所以我已经在模板和可视C ++上了，我就像

17
00:00:55,770 --> 00:00:59,198
general and an empty project and then I'm going to give this a name now I'm
一般和一个空项目，然后我要给它起个名字

18
00:00:59,399 --> 00:01:02,259
gonna call this game you'll notice that's also the solution name and I'm
要打这个游戏，您会注意到这也是解决方案的名称，我

19
00:01:02,460 --> 00:01:05,918
also going to make sure the crate directory for solution is checked now
还要确保现在检查解决方案的箱子目录

20
00:01:06,118 --> 00:01:08,890
I'll call this game because it's gonna be like a game project our actual
我将其称为“游戏”，因为它就像我们实际的游戏项目

21
00:01:09,090 --> 00:01:12,488
executable and then I'm going to make a library called engine inside there which
可执行文件，然后我将在其中创建一个名为engine的库

22
00:01:12,688 --> 00:01:16,000
links into game and you'll see that in a minute so click OK here set the visual
链接到游戏中，您很快就会看到它，因此请单击“确定”，在此处设置视觉效果

23
00:01:16,200 --> 00:01:19,450
studio creates our solution and project and you can see that we both solution
工作室创建了我们的解决方案和项目，您可以看到我们两个解决方案

24
00:01:19,650 --> 00:01:22,988
called game and a project called game as well if I go back to Windows Explorer
如果我回到Windows资源管理器，则称为游戏，也称为游戏的项目

25
00:01:23,188 --> 00:01:26,168
and take a look at that directory structure I've got game and then I've
看看这个目录结构，我有游戏，然后我有

26
00:01:26,368 --> 00:01:29,738
got a solution file here and then inside another folder called game I've got my
在这里有一个解决方案文件，然后在另一个名为game的文件夹中，我有

27
00:01:29,938 --> 00:01:33,579
actual C++ project you guys will know a little bit more about the structure of
实际的C ++项目，你们会了解更多有关

28
00:01:33,780 --> 00:01:37,028
Visual Studio C++ projects and how to set them up better I've made a video
 Visual Studio C ++项目以及如何更好地设置它们我制作了一个视频

29
00:01:37,228 --> 00:01:41,259
directly about that called the best way to set up your sleep uh suppose projects
直接关于所谓的最佳睡眠方式，就是假设你的项目

30
00:01:41,459 --> 00:01:45,278
and visual studios in the top right corner check it out okay so that's
和右上角的视觉工作室可以检查出来，所以

31
00:01:45,478 --> 00:01:48,519
pretty good to me we've got our first project now let's go ahead and add
对我来说很好，我们已经有了第一个项目，让我们继续

32
00:01:48,719 --> 00:01:53,590
another project I'm going to right click on this solution here select add and
我要右键单击此解决方案的另一个项目，在此处选择添加并

33
00:01:53,790 --> 00:01:59,768
then new project now again I like to set up my C++ projects just as empty project
然后再次创建新项目，我想像空项目一样设置C ++项目

34
00:01:59,968 --> 00:02:03,369
so that's all I'm going to do I'm going to select empty project here I'm going
这就是我要做的所有事情，我要在这里选择一个空项目

35
00:02:03,569 --> 00:02:07,269
to give it the name engine and I'm going to click ok so now if I
给它命名引擎，然后我要单击“确定” 

36
00:02:07,469 --> 00:02:11,080
collapse all of this you can see that our solution has two projects engine and
折叠所有这些，您可以看到我们的解决方案有两个项目引擎和

37
00:02:11,280 --> 00:02:15,010
game game is going to be our executable if we right click and hit properties
如果我们右键单击和点击属性，游戏将成为我们的可执行文件

38
00:02:15,210 --> 00:02:18,850
which is going to make sure that under the game property pages our
这将确保在游戏属性页面下我们的

39
00:02:19,050 --> 00:02:22,240
configuration type under the duration properties in general is set to
持续时间属性下的配置类型通常设置为

40
00:02:22,439 --> 00:02:26,140
application okay that's that's final I'll click OK and then let's take a look
好的，那是最终的操作，我将单击“确定”，然后让我们看一下

41
00:02:26,340 --> 00:02:29,830
at engine I'll right click on engine hit properties and then for this since we
在引擎上，我将右键单击引擎命中属性，然后执行此操作，因为我们

42
00:02:30,030 --> 00:02:32,860
are going to be linking statically I'm going to make sure that my configuration
将要静态链接，我要确保我的配置

43
00:02:33,060 --> 00:02:38,230
type is set to static library and that's all that we need to do to make this a
类型设置为静态库，这就是我们要做的全部工作

44
00:02:38,430 --> 00:02:41,770
static library for the most part again I'm just making sure that I'm modifying
静态库在大多数情况下还是要确保我正在修改

45
00:02:41,969 --> 00:02:45,610
this for all configurations and even for all platforms as well so I'm going to
这适用于所有配置，甚至适用于所有平台，所以我要

46
00:02:45,810 --> 00:02:48,969
just not save the changes here and make sure that for all configurations and all
只是不要在此处保存更改，并确保对于所有配置和所有

47
00:02:49,169 --> 00:02:53,350
platforms I'm setting this to be a static library I'm going to hit OK and
平台，我将其设置为静态库，然后点击确定， 

48
00:02:53,550 --> 00:02:56,860
then now let's write some code to test this out so what I'm going to do is just
那么现在让我们写一些代码来测试一下，所以我要做的就是

49
00:02:57,060 --> 00:03:00,550
show all files here for both of these projects so that I can actually make my
在这里显示这两个项目的所有文件，以便我可以实际制作

50
00:03:00,750 --> 00:03:05,650
sauce directories right click add new folder source contributors for both of
酱目录右键单击添加两个新的文件夹源贡献者

51
00:03:05,849 --> 00:03:10,630
the projects and game is going to get a application door CPP file which is going
项目和游戏将要获得一个应用程序门CPP文件

52
00:03:10,830 --> 00:03:14,289
to be basically the source of our application I guess will be the main
基本上是我们应用程序的来源，我想它将是主要的

53
00:03:14,489 --> 00:03:18,130
file and on the engine I'm just going to make a new item it's going to be a
文件，然后在引擎上，我将要制作一个新商品

54
00:03:18,330 --> 00:03:22,480
header file and I'm going to call this image piece over here I'm going to call
头文件，我将在此处调用此图像片段

55
00:03:22,680 --> 00:03:28,210
this engine H and I'm also going to make a C++ file it's going to be called
这个引擎H，我还将创建一个C ++文件，它将被称为

56
00:03:28,409 --> 00:03:33,430
engine Dulce TP ok cool and for this incredibly basics test I'm just going to
引擎Dulce TP很好，对于这个难以置信的基础测试，我只是要

57
00:03:33,629 --> 00:03:40,480
write name space engine and then on the here void print message and not going to
写名称空间引擎，然后在此处显示无效的打印消息，并且不会

58
00:03:40,680 --> 00:03:42,430
take any parameters or anything like that I'm going to leave this
采取任何参数或类似的东西，我要离开这个

59
00:03:42,629 --> 00:03:46,689
unimplemented because I'm going to go to the C++ file include my engine dot H
未实现，因为我要转到C ++文件，包括我的引擎点H 

60
00:03:46,889 --> 00:03:52,689
header file right name space engine and then implement that print message
头文件权限名称空间引擎，然后实现该打印消息

61
00:03:52,889 --> 00:03:59,770
function here and I'm just gonna make it print hello world to the console I'm
在这里运行，我只是要让它向控制台打印你好世界

62
00:03:59,969 --> 00:04:03,400
also going to make sure that in the C++ file I include iostream so that we can
还要确保在C ++文件中我包含iostream，以便我们可以

63
00:04:03,599 --> 00:04:07,930
print that ok cool so very very basic there's my header file there's my sleep
打印好酷，所以非常非常基础，有我的头文件，有我的睡眠

64
00:04:08,129 --> 00:04:10,599
locals file we're not even dealing with classes or anything like that to now
本地人文件，我们甚至都没有处理过类或类似的东西

65
00:04:10,799 --> 00:04:14,259
just really really simple we just got one function in our engine so back an
真的非常简单，我们的引擎中只有一个功能，所以返回

66
00:04:14,459 --> 00:04:16,730
application built CPP my goal here as I write
我编写此应用程序时构建了CPP，这是我的目标

67
00:04:16,930 --> 00:04:20,809
this little main function of mine my goal here is basically to just be able
我的这个主要功能我的目标基本上是

68
00:04:21,009 --> 00:04:24,860
to call engine print message I want to be able to write this card I wanted to
调用引擎打印消息，我希望能够写出我想要的这张卡

69
00:04:25,060 --> 00:04:28,100
be able to execute the link successfully and all that right into my executable
能够成功执行链接，并将其全部导入我的可执行文件

70
00:04:28,300 --> 00:04:32,569
how do I do that so the first step is I need to include a header file which
我该怎么做，所以第一步是我需要包含一个头文件

71
00:04:32,769 --> 00:04:36,588
defines this I don't technically speaking need to do this like in this
定义这个我从技术上讲并不是要像这样

72
00:04:36,788 --> 00:04:40,129
case because it's so simple I could literally write a namespace engine and
案例，因为它是如此简单，我可以从字面上编写一个名称空间引擎， 

73
00:04:40,329 --> 00:04:44,660
then void print message and of course like that's it you can see that that
然后取消打印消息，当然就是这样，您可以看到

74
00:04:44,860 --> 00:04:49,189
error goes away if I try and compile my code but by pressing control f7 you can
如果我尝试编译我的代码，错误就会消失，但是通过按f7键，您可以

75
00:04:49,389 --> 00:04:52,100
see over here that it does in fact compiled successfully of course it's not
在这里看到它确实可以成功编译，当然不是

76
00:04:52,300 --> 00:04:55,129
going to link because I don't actually have a definition to this so if I build
将要链接，因为我实际上对此没有定义，所以如果我建立

77
00:04:55,329 --> 00:04:58,490
this all the way it's not going on without we're gonna get a linking error
没有我们，我们将一直遇到连接错误

78
00:04:58,689 --> 00:05:03,499
however you kind of get the point now this code is exactly what is in my
但是您有点明白了，这段代码正是我的

79
00:05:03,699 --> 00:05:08,389
engine - right so of course instead of using my own kind of solution here my
引擎-当然可以，而不是在这里使用我自己的解决方案

80
00:05:08,589 --> 00:05:12,079
own version of this we want to be using this actual header file so how do we do
自己的版本，我们希望使用此实际的头文件，因此我们该怎么做

81
00:05:12,279 --> 00:05:15,680
that well all we need to do is include that
我们要做的就是包括

82
00:05:15,879 --> 00:05:18,770
actual header file that is inside our engine project there's a few ways we
我们引擎项目中的实际头文件有几种方法

83
00:05:18,970 --> 00:05:23,120
could do this I'll show you a couple so one way is just relative to this current
可以做到这一点，我给你看几个，所以一种方法只是相对于当前

84
00:05:23,319 --> 00:05:26,899
path so application does leave it be if I right-click on this actual tab and
路径，因此如果我右键单击此实际选项卡并

85
00:05:27,098 --> 00:05:31,550
click open containing folder because the application of CPP is inside game game
单击打开包含文件夹，因为CPP的应用程序在游戏内部

86
00:05:31,750 --> 00:05:35,718
and their source so if I go back one directory and then another directory I
和它们的来源，所以如果我返回一个目录，然后再返回另一个目录

87
00:05:35,918 --> 00:05:41,149
can theoretically go into engine source and then engine I so what I could do
从理论上讲可以进入引擎源，然后再进入引擎我，所以我能做的

88
00:05:41,348 --> 00:05:46,699
with an include is just go back to directories go into engine source and
带有include只是回到目录进入引擎源

89
00:05:46,899 --> 00:05:50,809
then engine dilash and you can see there we go our error goes away and we can
然后发动机发生故障，您可以看到我们走了，我们的错误消失了，我们可以

90
00:05:51,009 --> 00:05:54,680
compile our coach is fine now obviously this is just not not ideal at all I mean
现在编译我们的教练很好，很明显，这一点都不理想。 

91
00:05:54,879 --> 00:05:58,370
we have a relative path that is going back to directories and then into an
我们有一个相对路径，该路径将返回目录，然后进入

92
00:05:58,569 --> 00:06:02,028
entirely different project in the source it looks very messy and if we move
源代码中完全不同的项目，看起来非常混乱，如果我们移动

93
00:06:02,228 --> 00:06:04,879
folders or rename the name of the project or something like that
文件夹或重命名项目名称或类似名称

94
00:06:05,079 --> 00:06:08,209
everything's gonna break so what we actually want to do is use absolute
一切都会崩溃，所以我们实际上要使用绝对

95
00:06:08,408 --> 00:06:12,170
paths and specifically using compiler include paths so let's take a look at
路径，特别是使用编译器包含路径，让我们来看一下

96
00:06:12,370 --> 00:06:15,649
what that looks like so instead of this I'm going to right-click on game go to
看起来像什么，而不是这个，我要右键单击游戏转到

97
00:06:15,848 --> 00:06:19,550
properties and then under C C++ in general as an additional include
属性，然后一般在C C ++下作为附加包含

98
00:06:19,750 --> 00:06:24,350
directory I'm going to include that engine source directory which is over
目录，我将包括结束的引擎源目录

99
00:06:24,550 --> 00:06:28,850
here this engine source directory and I'm going to do that by typing in dollar
在这里，此引擎源目录，我将通过输入美元来完成

100
00:06:29,050 --> 00:06:32,179
sign that's solution directory engine which is the
这是解决方案目录引擎的标志

101
00:06:32,379 --> 00:06:36,499
aim of the project and then backslash source right that is my include
该项目的目标，然后反斜杠是我的源代码

102
00:06:36,699 --> 00:06:40,999
directory if I expand this and click edit you can see that it evaluates to
目录，如果我将其展开并单击“编辑”，则可以看到该目录的计算结果为

103
00:06:41,199 --> 00:06:45,379
that James slash engine slash source directory right solution directory is
 James斜杠引擎斜杠源目录正确的解决方案目录是

104
00:06:45,579 --> 00:06:49,160
just a macro which expands to be location or specifically the directory
只是一个扩展为位置或目录的宏

105
00:06:49,360 --> 00:06:54,259
of that solution file so in other words the directory that contains this dot SLN
解决方案文件的名称，换句话说就是包含该点SLN的目录

106
00:06:54,459 --> 00:06:58,338
file that's what that solution directory macro is and if we add on the name of
解决方案目录宏是什么文件，如果我们加上名称

107
00:06:58,538 --> 00:07:01,968
the project and then source we get there so now what I can do inside this
该项目，然后我们到达那里，所以现在我可以在其中做些什么

108
00:07:02,168 --> 00:07:05,838
application the CPP file is instead of including that massive like relative
应用程序的CPP文件而不是包括大量的相对

109
00:07:06,038 --> 00:07:10,189
path here I can just replace this with a path that is relative to my actual
在这里，我可以用相对于我实际的路径替换它

110
00:07:10,389 --> 00:07:13,730
source directory inside engines so in this case that's going to end up just
引擎中的源目录，所以在这种情况下，它只会以

111
00:07:13,930 --> 00:07:18,259
being engine dot H like that and that's all I need to do right and I can get rid
这样成为引擎点H，这就是我需要做的所有事情，而且我可以摆脱

112
00:07:18,459 --> 00:07:22,129
of that other one and hit ctrl f7 you can see compositors fine now because
另一个并按ctrl f7键，您现在可以看到合成器正常，因为

113
00:07:22,329 --> 00:07:26,929
we've specified it as a compiler include path I could also replace these quotes
我们已将其指定为编译器包含路径，我也可以替换这些引号

114
00:07:27,129 --> 00:07:31,670
with angular brackets like this and some people may prefer to do this personally
用这样的尖括号，有些人可能更喜欢亲自做

115
00:07:31,870 --> 00:07:35,778
I don't I use angular brackets if I'm including something that is outside of
如果我包含的内容不在括号内，我不会使用尖括号

116
00:07:35,978 --> 00:07:39,170
this visual studio solution so something that is an external dependency in
这个Visual Studio解决方案，因此是外部依赖项

117
00:07:39,370 --> 00:07:43,338
completely external to the project if the code for this engine dot H file is
如果此引擎点H文件的代码是完全在项目外部

118
00:07:43,538 --> 00:07:47,269
actually found inside this solution or specifically something written by me
实际上在此解决方案中找到，或者特别是我写的东西

119
00:07:47,468 --> 00:07:51,619
inside this solution not something like this a bus bus standard library then I
在这个解决方案中不是这样的总线标准库，然后我

120
00:07:51,819 --> 00:07:55,309
will use quotes to indicate that this is kind of one of our source files so I'm
将使用引号表示这是我们的源文件之一，所以我

121
00:07:55,509 --> 00:07:59,420
going to bring back those quotes ok fantastic so we've got our header files
可以带回那些报价，好极了，所以我们有了头文件

122
00:07:59,620 --> 00:08:03,309
all included now everything's working great but project isn't linking because
现在都包括在内，一切工作正常，但项目未链接，因为

123
00:08:03,509 --> 00:08:07,189
we're not actually linking to the library so there's a few things there's
我们实际上并没有链接到库，所以有一些东西

124
00:08:07,389 --> 00:08:10,189
a few ways that we could actually do that first of all you need to understand
首先我们需要实际了解的几种方法

125
00:08:10,389 --> 00:08:13,759
what happens to the library if I right-click on my engine and hit build
如果我右键单击引擎并点击构建，库会发生什么？ 

126
00:08:13,959 --> 00:08:17,540
you can see that what Visual Studio does is it generates this Lib file and if we
您可以看到Visual Studio所做的是生成此Lib文件，如果我们

127
00:08:17,740 --> 00:08:21,709
actually go to the path that it specifies over here then you can see we
实际转到它在此处指定的路径，然后您可以看到我们

128
00:08:21,908 --> 00:08:25,369
have this live file this is what we want to actually link against now what we
有了这个实时文件，这就是我们现在要实际链接的内容

129
00:08:25,569 --> 00:08:29,480
could do if you watch my video on static linking which is linked over there we
如果您在静态链接上观看我的视频，那该怎么办？ 

130
00:08:29,680 --> 00:08:34,128
could just kind of go to our linker settings and put that in as an input but
可以只是转到我们的链接器设置，并将其作为输入，但是

131
00:08:34,328 --> 00:08:37,429
we don't have to do that Visual Studio can actually kind of automate this for
我们不必这样做，Visual Studio实际上可以自动化

132
00:08:37,629 --> 00:08:41,439
us because this project is in the App solution so what we can do is actually
我们，因为该项目在App解决方案中，所以我们实际上可以做的是

133
00:08:41,639 --> 00:08:46,539
right-click on game here add and then reference and then we can select inside
右键单击游戏，在此处添加，然后引用，然后我们可以在里面选择

134
00:08:46,740 --> 00:08:50,828
our projects and solution we can select this engine project and then just click
我们的项目和解决方案，我们可以选择此引擎项目，然后单击

135
00:08:51,028 --> 00:08:55,719
OK now this is basically going to link in that live file into our executable as
好的，现在基本上是将活动文件链接到我们的可执行文件中，如下所示： 

136
00:08:55,919 --> 00:09:00,099
if we had added it to the linker input but this also gives us another advantage
如果我们将其添加到链接器输入中，但这也给我们带来了另一个优势

137
00:09:00,299 --> 00:09:03,309
apart from not having to kind of deal with setting up an actual file name
除了不必处理设置实际文件名之外

138
00:09:03,509 --> 00:09:07,120
because obviously if we change the name of engine to call or something like that
因为很明显，如果我们将引擎名称更改为call或类似名称

139
00:09:07,320 --> 00:09:10,750
if it's now called core we have to go back to our linker settings for game and
如果现在称为核心，则必须返回游戏的链接器设置， 

140
00:09:10,950 --> 00:09:14,859
actually change the input to be court live instead of engine the little Lib
实际上将输入更改为现场直播，而不是引擎上的小Lib 

141
00:09:15,059 --> 00:09:17,769
and if we use a reference like this we don't have to do that that's all
如果我们使用这样的参考，就不必这样做

142
00:09:17,970 --> 00:09:20,859
automated but apart from that I'm not even talking about that another benefit
自动化，但除此之外，我什至没有在谈论另一个好处

143
00:09:21,059 --> 00:09:25,719
is engine is now a dependency again so game depends on engine meaning that if
是引擎现在又是一个依赖项，因此游戏取决于引擎，这意味着

144
00:09:25,919 --> 00:09:30,459
something inside engine changes and we go to compile game game is actually
引擎内部发生了一些变化，我们去编译游戏

145
00:09:30,659 --> 00:09:34,209
going to compile engine and game so we know that we're dealing always with the
要编译引擎和游戏，所以我们知道我们一直在处理

146
00:09:34,409 --> 00:09:37,719
most up-to-date code there's never a chance that oh wait I forgot to compile
最新代码，永远没有机会哦，等等，我忘了编译

147
00:09:37,919 --> 00:09:41,049
engine that's why this isn't working or something like that it's all kind of
引擎，这就是为什么它不起作用或诸如此类的原因

148
00:09:41,250 --> 00:09:44,620
handled automatically it creates like a full dependency graph for us so
自动处理它会为我们创建一个完全依赖图

149
00:09:44,820 --> 00:09:48,309
everything works really nicely so now back in our code I'll even prove this by
一切都很好，所以现在回到我们的代码中，我什至将通过以下方式证明这一点

150
00:09:48,509 --> 00:09:51,849
just right clicking here and clicking clean solution you'll know that I'm just
只需单击此处，然后单击干净的解决方案，您就会知道我只是

151
00:09:52,049 --> 00:09:55,899
going to go to game and build game but you can see that what it actually does
去玩游戏并制作游戏，但您可以看到它的实际作用

152
00:09:56,100 --> 00:10:01,569
is build engine first and then game because engine is actually required for
首先构建引擎，然后再构建游戏，因为引擎实际上是

153
00:10:01,769 --> 00:10:04,870
game to work since we've added it as a reference and since we're linking
自从我们将其添加为参考并链接以来，游戏就可以正常工作

154
00:10:05,070 --> 00:10:09,699
against it back here in our turn I'm just going to add a cm get I'll have to
在我们这里，反过来，我要增加一个厘米，我不得不

155
00:10:09,899 --> 00:10:13,269
include iostream to make that happen this is just to pause the console so
包括iostream来实现这一点，只是暂停控制台，所以

156
00:10:13,470 --> 00:10:16,990
that we don't quit straightaway and now I'm going to run my application and you
我们不会立即退出，现在我要运行我的应用程序，你

157
00:10:17,190 --> 00:10:20,019
can see that HelloWorld gets printed at the console and everything seems to work
可以看到HelloWorld在控制台上打印出来，并且一切似乎正常

158
00:10:20,220 --> 00:10:23,109
successfully so that's it I hope you guys enjoyed the video this was just
成功了，就是这样，我希望你们喜欢这个视频

159
00:10:23,309 --> 00:10:26,289
about static linking in the future we might I might do a video that's
关于将来的静态链接，我们可能会制作一个视频

160
00:10:26,490 --> 00:10:29,529
specifically about this but with dynamic linking because it is a little bit
专门针对此问题，但具有动态链接，因为它有点

161
00:10:29,730 --> 00:10:32,949
different and there are some things that you need to be aware of but generally
不同，有些事情您需要了解，但通常

162
00:10:33,149 --> 00:10:36,370
again I will link statically in fact if we go back to our code real quick what's
再次，实际上，如果我们快速返回代码，我将静态链接。 

163
00:10:36,570 --> 00:10:39,849
happened now is even though we've had those two projects if I go into my
现在发生的是，即使我进入了这两个项目

164
00:10:40,049 --> 00:10:43,929
solution directory debug you can see that there's just game don't you see if
解决方案目录调试，您可以看到只有游戏不可以

165
00:10:44,129 --> 00:10:47,379
I take that game don't yet see and I'll open two completely different folder
我以为那游戏还没看，我将打开两个完全不同的文件夹

166
00:10:47,580 --> 00:10:49,779
here called dist which is on my desktop I'll paste it in
这是我桌面上的dist，我将其粘贴到

167
00:10:49,980 --> 00:10:53,139
here that's all I need to actually run this program if I double click on this
如果我双击此，这就是我实际运行该程序所需的全部

168
00:10:53,339 --> 00:10:57,008
ax a file you can see it's going to run my program and that's it because we're
斧头一个文件，您可以看到它将运行我的程序，仅此而已，因为我们

169
00:10:57,208 --> 00:11:00,459
linking statically so everything gets put into that exe file there's no actual
静态链接，以便一切都放入该exe文件中，没有实际的

170
00:11:00,659 --> 00:11:04,628
external file dependency gangully ACC has everything we want so we've managed
外部文件依赖性ACC具有我们想要的一切，因此我们已经进行了管理

171
00:11:04,828 --> 00:11:08,139
to successfully link statically and still kind of have multiple projects in
成功地静态链接，仍然有多个项目

172
00:11:08,339 --> 00:11:11,469
visual studio now this is very useful because again if we were actually
视觉工作室，这非常有用，因为如果我们真的

173
00:11:11,669 --> 00:11:15,609
building a game engine we might have this like this this solution might have
构建游戏引擎，我们可能会有这样的解决方案

174
00:11:15,809 --> 00:11:20,049
several examples of how to use the engine or several games or something
如何使用引擎或玩几款游戏的几个例子

175
00:11:20,250 --> 00:11:24,429
like that that all reference that one engine file that one engine project so
就像所有引用一个引擎文件的那个引擎文件一样

176
00:11:24,629 --> 00:11:27,399
that's why we kind of what we kind of want to modularize this so that we have
这就是为什么我们想要将其模块化，以便我们拥有

177
00:11:27,600 --> 00:11:31,750
all about kind of core code or centralized in one project and then we
关于所有类型的核心代码或集中在一个项目中，然后我们

178
00:11:31,950 --> 00:11:34,899
can just you know have all of our examples or all of our actual
你能知道我们所有的例子还是我们所有的实际

179
00:11:35,100 --> 00:11:38,559
applications our executables linked against that one centralized project
应用程序，我们的可执行文件链接到一个集中项目

180
00:11:38,759 --> 00:11:41,769
hope you guys enjoyed this video if you did you hit that like button if you have
希望大家喜欢这部影片，如果您按下了喜欢的按钮

181
00:11:41,970 --> 00:11:44,919
any suggestions for what you want me to cover and future c++ videos just leave a
关于您希望我介绍的内容以及以后的c ++视频的任何建议，只需留下一个

182
00:11:45,120 --> 00:11:48,069
comment below you can also port the series by going over to patreon hopeful
在下面评论，您也可以通过希望的patreon来移植该系列

183
00:11:48,269 --> 00:11:51,339
such that show know you'll get some cool rewards and just helps make sure that I
这样的节目知道你会得到一些很酷的奖励，并有助于确保我

184
00:11:51,539 --> 00:11:55,449
can keep making these videos as often as possible for you guys if you want any
可以尽可能多地为您制作这些视频

185
00:11:55,649 --> 00:11:58,689
more help with see both boss as well the best place to get it is just to join my
与老板见面以及获得帮助的更多帮助只是加入我的行列

186
00:11:58,889 --> 00:12:02,378
discord at the trailer home slash discord lots of great people there that
拖车之家的不和谐大大减少了那里很多伟大的人

187
00:12:02,578 --> 00:12:05,349
can help you out and we just talk about programming and stuff like that all day
可以帮助您，我们整天只谈论编程和类似的东西

188
00:12:05,549 --> 00:12:09,500
it's a great time I will see you guys next time goodbye
这是一个美好的时光，下次我会再见

189
00:12:09,700 --> 00:12:14,700
[Music]
 [音乐] 

