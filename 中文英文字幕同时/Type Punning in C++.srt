﻿1
00:00:00,000 --> 00:00:01,179
嘿，大家好，我叫乔纳（Jonah），欢迎回到我的C ++系列，所以今天
hey what's up guys my name is Jonah

2
00:00:01,379 --> 00:00:03,488
嘿，大家好，我叫乔纳（Jonah），欢迎回到我的C ++系列，所以今天
welcome back to my C++ series so today

3
00:00:03,689 --> 00:00:05,049
我们将讨论有关C ++中的类型修剪的所有内容，并且类型计划是
we're gonna be talking all about type

4
00:00:05,250 --> 00:00:07,328
我们将讨论有关C ++中的类型修剪的所有内容，并且类型计划是
punning in C++ and type planning is

5
00:00:07,528 --> 00:00:09,850
对于我们来说，实际上只是个花哨的术语
really just a fancy term for just us

6
00:00:10,050 --> 00:00:11,620
对于我们来说，实际上只是个花哨的术语
kind of getting around the type system

7
00:00:11,820 --> 00:00:14,710
在C ++中，所以C ++是一种强类型语言，这意味着我们拥有一种类型
in C++ so C++ is a strongly typed

8
00:00:14,910 --> 00:00:16,720
在C ++中，所以C ++是一种强类型语言，这意味着我们拥有一种类型
language which means that we have a type

9
00:00:16,920 --> 00:00:18,519
系统我们不会将所有内容都声明为auto，这是因为
system we don't declare everything as

10
00:00:18,719 --> 00:00:20,499
系统我们不会将所有内容都声明为auto，这是因为
auto I mean we kind of can because of

11
00:00:20,699 --> 00:00:22,300
该套件字，但是在其他语言（例如JavaScript）中，
that kit word but in other languages

12
00:00:22,500 --> 00:00:24,460
该套件字，但是在其他语言（例如JavaScript）中，
like JavaScript there are no kind of

13
00:00:24,660 --> 00:00:27,280
变量类型的意义，我们只需要声明一个
sense of variable types really and we

14
00:00:27,480 --> 00:00:28,929
变量类型的意义，我们只需要声明一个
just have we don't need to declare a

15
00:00:29,129 --> 00:00:30,370
创建变量并在其参数中接受变量时的变量类型
variable type when we create a variable

16
00:00:30,570 --> 00:00:32,439
创建变量并在其参数中接受变量时的变量类型
and when we accept it in its parameters

17
00:00:32,640 --> 00:00:34,719
进入函数或类似的东西，我们实际上没有类型系统，但是
into functions or anything like that we

18
00:00:34,920 --> 00:00:36,250
进入函数或类似的东西，我们实际上没有类型系统，但是
don't have a type system really but it's

19
00:00:36,450 --> 00:00:37,570
加号加号当然我们有一个类型系统，我们必须将事物声明为
a plus plus of course we do have a type

20
00:00:37,770 --> 00:00:39,009
加号加号当然我们有一个类型系统，我们必须将事物声明为
system we have to declare things as

21
00:00:39,210 --> 00:00:41,169
整数或双精度数或欺负的或结构或类的所有东西，当
integers or doubles or bullying's or

22
00:00:41,369 --> 00:00:43,659
整数或双精度数或欺负的或结构或类的所有东西，当
structs or classes all that stuff when

23
00:00:43,859 --> 00:00:45,369
我们实际上创建了变量，但是这种类型的系统并没有真正执行
we actually create variables however

24
00:00:45,570 --> 00:00:48,878
我们实际上创建了变量，但是这种类型的系统并没有真正执行
this type system is not really enforced

25
00:00:49,079 --> 00:00:51,038
与其他语言（例如Java）一样强大
as strongly as it is in other languages

26
00:00:51,238 --> 00:00:53,259
与其他语言（例如Java）一样强大
such as Java where it's really hard to

27
00:00:53,460 --> 00:00:55,570
绕过类型甚至C锐化，但您可以做到，但是还有更多工作要做
get around types and even c-sharp but

28
00:00:55,770 --> 00:00:57,759
绕过类型甚至C锐化，但您可以做到，但是还有更多工作要做
you can do it but it's a bit more work

29
00:00:57,960 --> 00:01:01,748
在C ++中，类型是由编译器强制执行的，您可以直接访问
in C++ whilst types are kind of enforced

30
00:01:01,948 --> 00:01:04,149
在C ++中，类型是由编译器强制执行的，您可以直接访问
by the compiler you can directly access

31
00:01:04,349 --> 00:01:07,539
记忆，这意味着您可以说稍等一下，我知道
memory which means that you can kind of

32
00:01:07,739 --> 00:01:10,599
记忆，这意味着您可以说稍等一下，我知道
say that hang on a minute I know that at

33
00:01:10,799 --> 00:01:12,579
在我的代码中，我一直在使用这种类型作为整数，但是实际上
the moment in my code I've been using

34
00:01:12,780 --> 00:01:16,269
在我的代码中，我一直在使用这种类型作为整数，但是实际上
this type as say an integer but actually

35
00:01:16,469 --> 00:01:18,340
我要抓住那个记忆，把它当作一个记忆
I'm gonna just grab that memory that

36
00:01:18,540 --> 00:01:20,019
我要抓住那个记忆，把它当作一个记忆
same memory and just treat it as a

37
00:01:20,219 --> 00:01:22,269
现在加倍或作为一堂课之类的
double now or as a class or something

38
00:01:22,469 --> 00:01:23,259
现在加倍或作为一堂课之类的
like that

39
00:01:23,459 --> 00:01:25,149
现在您可以非常轻松地绕过类型系统
and you can just get around the type

40
00:01:25,349 --> 00:01:27,879
现在您可以非常轻松地绕过类型系统
system really easily now whether what

41
00:01:28,079 --> 00:01:30,579
在某些情况下，您应该再次取决于您的实际情况
you should is going to again depend on

42
00:01:30,780 --> 00:01:33,429
在某些情况下，您应该再次取决于您的实际情况
your actual circumstance in some cases

43
00:01:33,629 --> 00:01:35,500
您绝对不应绕过类型系统，因为类型系统是
you absolutely should not circumvent the

44
00:01:35,700 --> 00:01:38,259
您绝对不应绕过类型系统，因为类型系统是
type system because the type system is

45
00:01:38,459 --> 00:01:39,009
在那里是有原因的，您可能不想
there for a reason

46
00:01:39,209 --> 00:01:40,359
在那里是有原因的，您可能不想
and you probably don't want to be

47
00:01:40,560 --> 00:01:42,099
除非您有充分的理由，否则请玩两个月
playing around with a 2-month unless you

48
00:01:42,299 --> 00:01:43,719
除非您有充分的理由，否则请玩两个月
have a good reason but in other cases

49
00:01:43,920 --> 00:01:46,269
这样做完全没问题，例如，让我们说我们有一堂课，我们
it's totally okay to do that for example

50
00:01:46,469 --> 00:01:48,308
这样做完全没问题，例如，让我们说我们有一堂课，我们
let's just say we had a class and we

51
00:01:48,509 --> 00:01:49,778
想把它写成字节流而不是那种
wanted to write it out as a stream of

52
00:01:49,978 --> 00:01:51,698
想把它写成字节流而不是那种
bytes well instead of kind of going

53
00:01:51,899 --> 00:01:53,439
通过一切假设它就像原始类型的结构
through everything as assuming it's like

54
00:01:53,640 --> 00:01:54,789
通过一切假设它就像原始类型的结构
a structure of primitive types it

55
00:01:54,989 --> 00:01:56,230
在内存中没有指向其他位置的指针，我们可以重新解释
doesn't have pointers to other places in

56
00:01:56,430 --> 00:01:59,319
在内存中没有指向其他位置的指针，我们可以重新解释
memory we can just kind of reinterpret

57
00:01:59,519 --> 00:02:01,359
整个结构或类，或者我们所拥有的只是一个字节数组而已
that entire struct or class or whatever

58
00:02:01,560 --> 00:02:04,119
整个结构或类，或者我们所拥有的只是一个字节数组而已
we have as just a byte array and just

59
00:02:04,319 --> 00:02:06,698
将其写出或流式传输到我们不需要关心的地方
write that out or stream it somewhere

60
00:02:06,899 --> 00:02:09,009
将其写出或流式传输到我们不需要关心的地方
right we don't have to even care about

61
00:02:09,209 --> 00:02:10,990
里面有什么类型，所以我们知道尺寸
what types are in it so we know the size

62
00:02:11,189 --> 00:02:11,969
里面有什么类型，所以我们知道尺寸
we can just get the

63
00:02:12,169 --> 00:02:13,770
获得数据访问权，然后将其放置在某处
get to get the data access it and then

64
00:02:13,969 --> 00:02:16,590
获得数据访问权，然后将其放置在某处
just put it somewhere so in a lot of

65
00:02:16,789 --> 00:02:18,630
的情况下，它真的很有用，这意味着我猜有点
cases it is really useful and it means

66
00:02:18,830 --> 00:02:22,259
的情况下，它真的很有用，这意味着我猜有点
that it's kind of that raw I guess

67
00:02:22,459 --> 00:02:25,140
低层访问，这就是为什么我真的很喜欢C ++以及为什么应该是这样的原因
low level access that is why I really

68
00:02:25,340 --> 00:02:27,180
低层访问，这就是为什么我真的很喜欢C ++以及为什么应该是这样的原因
like C++ and why suppose was is so

69
00:02:27,379 --> 00:02:28,950
在无论如何都不要求性能的应用中有效
effective in applications where

70
00:02:29,150 --> 00:02:31,110
在无论如何都不要求性能的应用中有效
performance is required anyway not

71
00:02:31,310 --> 00:02:32,368
谈谈，让我们潜入一些例子，这就是
talking let's just dive in take a look

72
00:02:32,568 --> 00:02:34,230
谈谈，让我们潜入一些例子，这就是
at a few examples this is something that

73
00:02:34,430 --> 00:02:36,868
我经常使用，所以您可能会在其他视频中看到它，尤其是
I use quite often so you'll probably see

74
00:02:37,068 --> 00:02:38,250
我经常使用，所以您可能会在其他视频中看到它，尤其是
it in other videos and especially like

75
00:02:38,449 --> 00:02:39,990
在OpenGL中，游戏结束并消失了，所以您会看到
in the OpenGL serious and the game end

76
00:02:40,189 --> 00:02:41,100
在OpenGL中，游戏结束并消失了，所以您会看到
deserts and all that so you'll see

77
00:02:41,300 --> 00:02:42,600
很多，这只是一个基本的概述，但让我们潜入
plenty of this this is just gonna be a

78
00:02:42,800 --> 00:02:44,100
很多，这只是一个基本的概述，但让我们潜入
basic overview but let's just dive in

79
00:02:44,300 --> 00:02:45,480
看看我们能做些什么，所以在第一个示例中，我声明了一个
and see what we can do okay so in that

80
00:02:45,680 --> 00:02:47,009
看看我们能做些什么，所以在第一个示例中，我声明了一个
first example I kind of declared an

81
00:02:47,209 --> 00:02:48,930
我只是说我有一个整数，然后我只想治疗
integer well I just said that I had an

82
00:02:49,129 --> 00:02:50,700
我只是说我有一个整数，然后我只想治疗
integer and then I just wanted to treat

83
00:02:50,900 --> 00:02:53,580
在这种情况下，它当然是双井，可能会导致
it as a double well of course in this

84
00:02:53,780 --> 00:02:55,319
在这种情况下，它当然是双井，可能会导致
case that's probably gonna result in and

85
00:02:55,519 --> 00:02:57,000
看起来很古怪的双人间，而不是您想做的事，但是再次
pretty weird-looking double and not

86
00:02:57,199 --> 00:02:58,349
看起来很古怪的双人间，而不是您想做的事，但是再次
something that you want to do but again

87
00:02:58,549 --> 00:02:59,849
只是为了演示它是如何工作的，无论如何我都会向您展示
just to demonstrate how this works I'll

88
00:03:00,049 --> 00:03:01,740
只是为了演示它是如何工作的，无论如何我都会向您展示
show you anyway we're going to make it

89
00:03:01,939 --> 00:03:03,750
加倍，我们将其称为值，并将其设置为等于
double we're gonna call it value and

90
00:03:03,949 --> 00:03:05,368
加倍，我们将其称为值，并将其设置为等于
we're gonna set it equal to a that's

91
00:03:05,568 --> 00:03:07,080
首先，您可以看到它运作正常，似乎一切正常
kind of step one now first of all you

92
00:03:07,280 --> 00:03:08,969
首先，您可以看到它运作正常，似乎一切正常
can see it works right everything seems

93
00:03:09,169 --> 00:03:11,640
如果我们要像这样打印出来，就可以正常工作，我什至放了一个
to work fine if we were to print this

94
00:03:11,840 --> 00:03:15,390
如果我们要像这样打印出来，就可以正常工作，我什至放了一个
out like this and I even put a

95
00:03:15,590 --> 00:03:17,490
这里的断点是五点，你可以看到我们得到的是50好吧有趣
breakpoint over here get at five you can

96
00:03:17,689 --> 00:03:19,110
这里的断点是五点，你可以看到我们得到的是50好吧有趣
see what we get is 50 okay interesting

97
00:03:19,310 --> 00:03:21,330
合理，我想我的意思是现在它把我们的整数转换成双精度，所以是50
reasonable I guess I mean it converted

98
00:03:21,530 --> 00:03:23,759
合理，我想我的意思是现在它把我们的整数转换成双精度，所以是50
our integer to a double now so it's 50

99
00:03:23,959 --> 00:03:25,680
让我们看一下内存，以便调试Windows内存和内存1
let's take a look at the memory so I'll

100
00:03:25,879 --> 00:03:29,340
让我们看一下内存，以便调试Windows内存和内存1
go debug Windows memory and memory 1 I'm

101
00:03:29,539 --> 00:03:30,390
要去的内存地址，看看里面有什么好酷的
gonna go to the memory address of a and

102
00:03:30,590 --> 00:03:32,460
要去的内存地址，看看里面有什么好酷的
take a look at what is in there ok cool

103
00:03:32,659 --> 00:03:35,189
32零零零零零零零好吧，现在以十六进制表示32是50
32 zero zero zero zero zero zero okay

104
00:03:35,389 --> 00:03:38,460
32零零零零零零零好吧，现在以十六进制表示32是50
makes sense 32 is 50 in hexadecimal now

105
00:03:38,659 --> 00:03:39,539
让我们看一下value的内存地址，首先
let's take a look at the memory address

106
00:03:39,739 --> 00:03:41,039
让我们看一下value的内存地址，首先
of value now first of all is going to

107
00:03:41,239 --> 00:03:42,689
具有不同的内存地址首先，我们知道有多少个
have a different memory address the

108
00:03:42,889 --> 00:03:44,189
具有不同的内存地址首先，我们知道有多少个
second of all well we know how many

109
00:03:44,389 --> 00:03:45,840
个字节是八个字节
bytes it is it's eight bytes look at

110
00:03:46,039 --> 00:03:46,110
个字节是八个字节
that

111
00:03:46,310 --> 00:03:48,539
这是完全不同的，我是说49 49和40在那边，就像发生了什么
it's completely different I mean 49 49

112
00:03:48,739 --> 00:03:50,819
这是完全不同的，我是说49 49和40在那边，就像发生了什么
and 40 over here and like what's going

113
00:03:51,019 --> 00:03:52,890
在那儿，甚至不是，这根本不是同一个记忆，因为那是什么
on it's it's not even it's not it's not

114
00:03:53,090 --> 00:03:54,689
在那儿，甚至不是，这根本不是同一个记忆，因为那是什么
the same memory at all because what's

115
00:03:54,889 --> 00:03:57,210
实际完成的是一次转换，将int转换为
actually done is this just actually done

116
00:03:57,409 --> 00:03:59,310
实际完成的是一次转换，将int转换为
a conversion it's converted an int into

117
00:03:59,509 --> 00:04:00,900
一双，因为如果您现在在此执行类似的操作，将会发生这种情况
a double because that's what happens if

118
00:04:01,099 --> 00:04:02,640
一双，因为如果您现在在此执行类似的操作，将会发生这种情况
you do something like this now in this

119
00:04:02,840 --> 00:04:04,469
情况是一个隐式转换，这意味着我们实际上并没有告诉它
case it was an implicit conversion

120
00:04:04,669 --> 00:04:05,730
情况是一个隐式转换，这意味着我们实际上并没有告诉它
meaning we didn't actually tell it to

121
00:04:05,930 --> 00:04:07,319
转换任何东西，如果我们想要明确的话，我们可以写一个double like
convert anything if we wanted to be

122
00:04:07,519 --> 00:04:10,050
转换任何东西，如果我们想要明确的话，我们可以写一个double like
explicit we could write double a like

123
00:04:10,250 --> 00:04:11,460
很好，我的意思是在这种情况下它什么也没得到，我们不必
that which is fine I mean it doesn't get

124
00:04:11,659 --> 00:04:12,990
很好，我的意思是在这种情况下它什么也没得到，我们不必
anything in this case we don't have to

125
00:04:13,189 --> 00:04:14,550
在这里写double，因为它将做一个隐式
write double here because it will do an

126
00:04:14,750 --> 00:04:15,030
在这里写double，因为它将做一个隐式
implicit

127
00:04:15,229 --> 00:04:16,740
但是如果我们想明确一点，这实际上是在转换
but if we wanted to be explicit this is

128
00:04:16,939 --> 00:04:18,569
但是如果我们想明确一点，这实际上是在转换
actually what's happening is converting

129
00:04:18,769 --> 00:04:20,520
从整数变成实际的两倍现在如果我们想
that a from being an integer into an

130
00:04:20,720 --> 00:04:22,439
从整数变成实际的两倍现在如果我们想
actual double now what if we wanted to

131
00:04:22,639 --> 00:04:26,160
只需占用现有内存，我们看到32，然后零零零零零零零
just take that existing memory we saw 32

132
00:04:26,360 --> 00:04:28,439
只需占用现有内存，我们看到32，然后零零零零零零零
and then zero zero zero zero zero zero

133
00:04:28,639 --> 00:04:30,660
在我们的记忆中，我不会占用相同的记忆，但我只是希望该记忆
in our memory I wouldn't take that same

134
00:04:30,860 --> 00:04:34,020
在我们的记忆中，我不会占用相同的记忆，但我只是希望该记忆
memory but I just want that memory to be

135
00:04:34,220 --> 00:04:36,180
被视为双重我该如何做好呢我们要做的第一件事
treated as a double how do I do that

136
00:04:36,379 --> 00:04:37,889
被视为双重我该如何做好呢我们要做的第一件事
well the first thing to do that that we

137
00:04:38,089 --> 00:04:39,449
需要做的实际上有几种方法，但这是我的一种错误方法
need to do there are a few ways actually

138
00:04:39,649 --> 00:04:41,160
需要做的实际上有几种方法，但这是我的一种错误方法
but this is the kind of wrong way I

139
00:04:41,360 --> 00:04:43,759
猜想我们取a的内存剂量，然后将其转换为双指针，因此
guess we take the memory dose of a and

140
00:04:43,959 --> 00:04:48,170
猜想我们取a的内存剂量，然后将其转换为双指针，因此
then we cast that to a double pointer so

141
00:04:48,370 --> 00:04:50,579
一个双指针双指针，我的意思是显然一个双指针而不是两个
and a double by double pointer I mean

142
00:04:50,779 --> 00:04:52,680
一个双指针双指针，我的意思是显然一个双指针而不是两个
obviously a double pointer not two

143
00:04:52,879 --> 00:04:56,670
指针，现在这部分在这里当然只是取一个so的内存地址
pointers now this part here is of course

144
00:04:56,870 --> 00:04:58,710
指针，现在这部分在这里当然只是取一个so的内存地址
just taking the memory address of a so

145
00:04:58,910 --> 00:05:00,629
这给了我们一个整数指针，然后我们将其转换为指针
this gives us an integer pointer and

146
00:05:00,829 --> 00:05:02,879
这给了我们一个整数指针，然后我们将其转换为指针
then we're converting that into pointer

147
00:05:03,079 --> 00:05:05,968
变成双指针，然后我们显然需要回到具有
into a double pointer and then we

148
00:05:06,168 --> 00:05:07,560
变成双指针，然后我们显然需要回到具有
obviously need to get back to having a

149
00:05:07,759 --> 00:05:09,088
加倍，所以我们如何从指针转到实际类型，然后取消引用它，
double so how do we go from pointer to

150
00:05:09,288 --> 00:05:11,370
加倍，所以我们如何从指针转到实际类型，然后取消引用它，
the actual type we dereference it and

151
00:05:11,569 --> 00:05:13,560
这就是我们得到的，所以这可能有点奇怪，如果是第一次
that's what we get so this is this looks

152
00:05:13,759 --> 00:05:15,329
这就是我们得到的，所以这可能有点奇怪，如果是第一次
a bit weird maybe if it's the first time

153
00:05:15,529 --> 00:05:16,800
您已经看到了，但实际上我们在这里所做的是键入punting
you've seen it but essentially what

154
00:05:17,000 --> 00:05:19,079
您已经看到了，但实际上我们在这里所做的是键入punting
we're doing here is we're type punting

155
00:05:19,279 --> 00:05:21,778
我们的整数变成双精度数，所以如果我们现在按下f5，让我们看看我们可以
our integer into a double so if we hit

156
00:05:21,978 --> 00:05:23,639
我们的整数变成双精度数，所以如果我们现在按下f5，让我们看看我们可以
f5 now let's see what we get okay so

157
00:05:23,839 --> 00:05:25,079
这是一个看起来很奇怪的双倍，我的意思是负二分
it's a pretty weird-looking double

158
00:05:25,279 --> 00:05:26,430
这是一个看起来很奇怪的双倍，我的意思是负二分
I mean it's negative nine point two

159
00:05:26,629 --> 00:05:29,939
八加六十一，所以是的，显然不是我们可能会想到的
eight plus sixty-one so yeah whatever

160
00:05:30,139 --> 00:05:31,680
八加六十一，所以是的，显然不是我们可能会想到的
clearly not something that we'd probably

161
00:05:31,879 --> 00:05:33,028
想做，但是如果我们看一下内存，这只是一个例子
want to do but again it's just an

162
00:05:33,228 --> 00:05:34,829
想做，但是如果我们看一下内存，这只是一个例子
example if we take a look at the memory

163
00:05:35,029 --> 00:05:36,600
一个的地址，您可以看到它当然有32个类似的字
address of a you can see that of course

164
00:05:36,800 --> 00:05:39,149
一个的地址，您可以看到它当然有32个类似的字
it has 32 like that and if we take it

165
00:05:39,348 --> 00:05:41,430
B的内存地址哦，很抱歉，我很重视您可以在这里看到我们拥有的内容
the memory address of B oh sorry I'll

166
00:05:41,629 --> 00:05:44,399
B的内存地址哦，很抱歉，我很重视您可以在这里看到我们拥有的内容
value you can see what we have over here

167
00:05:44,598 --> 00:05:47,850
现在您还看到的是，如果您还记得我们有八个字节的双倍
now what you also see is if you remember

168
00:05:48,050 --> 00:05:49,889
现在您还看到的是，如果您还记得我们有八个字节的双倍
that we have eight bytes of double the

169
00:05:50,089 --> 00:05:52,110
剩下的就是我们的初始化内存，因为我们在这里所做的事情很漂亮
rest is our initialize memory because

170
00:05:52,310 --> 00:05:55,350
剩下的就是我们的初始化内存，因为我们在这里所做的事情很漂亮
what we've done here is something pretty

171
00:05:55,550 --> 00:05:57,480
不好，因为这些是不同的大小，对不起，我是说我们已经占用了四个字节
bad because these are different sizes

172
00:05:57,680 --> 00:05:59,399
不好，因为这些是不同的大小，对不起，我是说我们已经占用了四个字节
right I mean we've taken a four byte

173
00:05:59,598 --> 00:06:03,210
整数和种类决定要加倍，所以自从我们做了
integer and kind of decided to a double

174
00:06:03,410 --> 00:06:04,620
整数和种类决定要加倍，所以自从我们做了
so what that's done since we've

175
00:06:04,819 --> 00:06:06,449
将一个int指针转换为一个双指针，然后取消引用它
converted an int pointer into a double

176
00:06:06,649 --> 00:06:08,910
将一个int指针转换为一个双指针，然后取消引用它
pointer and then dereferenced it that's

177
00:06:09,110 --> 00:06:10,199
实际向前看了看我们整数之后的四个字节，然后抓住了
actually gone ahead and looked four

178
00:06:10,399 --> 00:06:12,870
实际向前看了看我们整数之后的四个字节，然后抓住了
bytes past our integer and grabbed that

179
00:06:13,069 --> 00:06:14,999
内存不是我们的内存，这不是我们拥有整数的内存，所以这
memory which is not our memory it's not

180
00:06:15,199 --> 00:06:16,949
内存不是我们的内存，这不是我们拥有整数的内存，所以这
the memory we have for integer so this

181
00:06:17,149 --> 00:06:18,810
非常糟糕，在某些情况下甚至可能导致崩溃
is very bad and it could even cause a

182
00:06:19,009 --> 00:06:20,189
非常糟糕，在某些情况下甚至可能导致崩溃
crash in some circumstance it's

183
00:06:20,389 --> 00:06:21,040
绝对是您想要的东西，我的意思是我们已经将该内存复制到了
definitely something you want

184
00:06:21,240 --> 00:06:23,350
绝对是您想要的东西，我的意思是我们已经将该内存复制到了
do I mean we've copied that memory into

185
00:06:23,550 --> 00:06:25,210
一个新的双值博客，这样很安全，所以我们没有写过，如果不能，我们会写
a new double value blog so that's safe

186
00:06:25,410 --> 00:06:27,999
一个新的双值博客，这样很安全，所以我们没有写过，如果不能，我们会写
so we haven't written and we if we can't

187
00:06:28,199 --> 00:06:29,439
在这一点上，即使我们确实写价值，我们也不会写
at this point even if we do write to

188
00:06:29,639 --> 00:06:31,540
在这一点上，即使我们确实写价值，我们也不会写
value we're not going to be writing to

189
00:06:31,740 --> 00:06:33,610
记忆在我们的记忆中，但我们绝对是从记忆中读取的不是我们的记忆
memory this on ours but we definitely

190
00:06:33,810 --> 00:06:35,439
记忆在我们的记忆中，但我们绝对是从记忆中读取的不是我们的记忆
have read from memory that's not ours

191
00:06:35,639 --> 00:06:37,270
这不好，让我们看一个更实际的例子，所以我要去
that's not good let's take a look at a

192
00:06:37,470 --> 00:06:39,968
这不好，让我们看一个更实际的例子，所以我要去
more practical example so what I'm going

193
00:06:40,168 --> 00:06:41,588
如果您不希望只是快速记录一下，现在当然可以做
to do now of course if you didn't want

194
00:06:41,788 --> 00:06:43,270
如果您不希望只是快速记录一下，现在当然可以做
to just just a quick note if you didn't

195
00:06:43,470 --> 00:06:44,949
想要创建一个全新的变量，而您只想访问
want to actually create a whole new

196
00:06:45,149 --> 00:06:46,600
想要创建一个全新的变量，而您只想访问
variable and you just wanted to access

197
00:06:46,800 --> 00:06:48,699
这个int好像是double一样，那么您只需在＆的末尾添加一个＆号即可
this int as if it was a double then you

198
00:06:48,899 --> 00:06:50,499
这个int好像是double一样，那么您只需在＆的末尾添加一个＆号即可
can just add an ampersand to the end of

199
00:06:50,699 --> 00:06:53,350
此双在这里只引用它，而不将其复制到
this double here which will just

200
00:06:53,550 --> 00:06:55,059
此双在这里只引用它，而不将其复制到
reference it and not copy it into a

201
00:06:55,259 --> 00:06:56,649
整个新变量，这样您将实际编辑此整数内存
whole new variable and that way you will

202
00:06:56,848 --> 00:06:58,899
整个新变量，这样您将实际编辑此整数内存
be actually editing this integer memory

203
00:06:59,098 --> 00:07:00,790
这很危险，因为如果我决定在此处写入类似0的内容
so that is dangerous because if I decide

204
00:07:00,990 --> 00:07:03,520
这很危险，因为如果我决定在此处写入类似0的内容
to write something like 0 into here then

205
00:07:03,720 --> 00:07:05,110
猜猜实际上要写8个字节而不是4个字节，但是我们只有
guess what it's actually going to write

206
00:07:05,310 --> 00:07:06,879
猜猜实际上要写8个字节而不是4个字节，但是我们只有
8 bytes instead of 4 but we only have

207
00:07:07,079 --> 00:07:08,649
在此之前可能还没有崩溃的空间，所以让我们来看一个更实用的
space before this might even crash so

208
00:07:08,848 --> 00:07:10,809
在此之前可能还没有崩溃的空间，所以让我们来看一个更实用的
let's take a look at a more practical

209
00:07:11,009 --> 00:07:13,838
例如，如果我有一个称为实体的结构，并且在XY中有两个变量，该怎么办
example what if I have a struct called

210
00:07:14,038 --> 00:07:17,319
例如，如果我有一个称为实体的结构，并且在XY中有两个变量，该怎么办
entity and this has two variables in X Y

211
00:07:17,519 --> 00:07:19,588
现在我们知道我真的说我希望你知道，如果我们让我们进行初始化
now we know really say I hope you know

212
00:07:19,788 --> 00:07:22,119
现在我们知道我真的说我希望你知道，如果我们让我们进行初始化
that if we have let's just initializes

213
00:07:22,319 --> 00:07:24,639
到5和8如果我们在内存中有这样的东西，那么实际上
to 5 and 8 if we have something like

214
00:07:24,839 --> 00:07:27,100
到5和8如果我们在内存中有这样的东西，那么实际上
this in memory then really all that

215
00:07:27,300 --> 00:07:31,120
struct就像它是由它实际上具有的两个整数组成的
struct is is like it it is it is made up

216
00:07:31,319 --> 00:07:32,410
struct就像它是由它实际上具有的两个整数组成的
of those two integers it has literally

217
00:07:32,610 --> 00:07:34,300
那两个整数，就是这样，如果我们查看它的内存，您会发现我们有5
those two integers and that's it if we

218
00:07:34,500 --> 00:07:36,309
那两个整数，就是这样，如果我们查看它的内存，您会发现我们有5
look at its memory you can see we have 5

219
00:07:36,509 --> 00:07:38,740
＆8就可以了，结构本身不包含任何填充类型
& 8 that's it the struct itself does not

220
00:07:38,939 --> 00:07:40,660
＆8就可以了，结构本身不包含任何填充类型
contain any kind of padding any kind of

221
00:07:40,860 --> 00:07:43,120
数据，如果它是C结构，那么至少要咬一口
data whatever if it's an am C struct

222
00:07:43,319 --> 00:07:44,620
数据，如果它是C结构，那么至少要咬一口
then there has to be at least one bites

223
00:07:44,819 --> 00:07:46,209
那里会有东西，因为我们需要能够解决这个问题
the will be something in there because

224
00:07:46,408 --> 00:07:47,379
那里会有东西，因为我们需要能够解决这个问题
we need to be able to address that

225
00:07:47,579 --> 00:07:50,050
内存，我们无法解决它不存在的问题，因此我们必须至少拥有一个
memory and we can't address it it's not

226
00:07:50,250 --> 00:07:51,939
内存，我们无法解决它不存在的问题，因此我们必须至少拥有一个
there so we have to have at least one

227
00:07:52,139 --> 00:07:54,160
字节指令，是否为空，但是如果我们像x中那样有变量
byte instructive and if it is empty but

228
00:07:54,360 --> 00:07:56,528
字节指令，是否为空，但是如果我们像x中那样有变量
if we have variables in there like in x

229
00:07:56,728 --> 00:07:59,709
和y那个struct只是两个整数而已，就没有多余的了
and y that struct is just two integers

230
00:07:59,908 --> 00:08:01,600
和y那个struct只是两个整数而已，就没有多余的了
that's all it is there's no like extra

231
00:08:01,800 --> 00:08:04,240
数据表明这是一个实体结构，而不是它的工作原理
data that says this is an entity struct

232
00:08:04,439 --> 00:08:06,519
数据表明这是一个实体结构，而不是它的工作原理
it's not how any of that works it's just

233
00:08:06,718 --> 00:08:10,449
这两个整数，所以我们在这里可以做的就是实际上可以将这个空
the two integers so what we can do here

234
00:08:10,649 --> 00:08:12,369
这两个整数，所以我们在这里可以做的就是实际上可以将这个空
is we can actually just treat this empty

235
00:08:12,569 --> 00:08:15,509
将结构作为一个int数组，或者甚至不提取其整数之一
struct as an int array or maybe just

236
00:08:15,709 --> 00:08:18,249
将结构作为一个int数组，或者甚至不提取其整数之一
extract one of its integers by not even

237
00:08:18,449 --> 00:08:20,019
这样做，所以您不了解它的原因，所以我只能说一个点X，
doing it so you don't why it's take a

238
00:08:20,218 --> 00:08:22,180
这样做，所以您不了解它的原因，所以我只能说一个点X，
look so I could just say a dot X and

239
00:08:22,379 --> 00:08:25,689
那会给我我的X变量，但我也能做的就是说好
that would give me my X variable but

240
00:08:25,889 --> 00:08:27,968
那会给我我的X变量，但我也能做的就是说好
what I could do also is just say well

241
00:08:28,168 --> 00:08:30,459
让我们将其读出来，所以这将是原始数组
let's make it a read out of it so this

242
00:08:30,658 --> 00:08:32,829
让我们将其读出来，所以这将是原始数组
will be a raw array say in point up

243
00:08:33,029 --> 00:08:36,639
在我的位置上称它等于a的内存地址，然后花费
call this out of my position equals the

244
00:08:36,840 --> 00:08:40,179
在我的位置上称它等于a的内存地址，然后花费
memory address of a and then just cost

245
00:08:40,379 --> 00:08:42,399
Twitter就是这样指向的，现在，如果我想将其打印出来，我将
that Twitter points up like that and now

246
00:08:42,600 --> 00:08:43,899
Twitter就是这样指向的，现在，如果我想将其打印出来，我将
if I wanted to print this out I'll just

247
00:08:44,100 --> 00:08:50,370
将其写为CDC输出位置0，然后像这样逗号位置1
write it as CDC out position zero and

248
00:08:50,570 --> 00:08:54,669
将其写为CDC输出位置0，然后像这样逗号位置1
then comma position one like that as if

249
00:08:54,870 --> 00:08:56,379
贾斯汀还好，你可以看到我有它
it was Justin all right and you can see

250
00:08:56,580 --> 00:08:57,039
贾斯汀还好，你可以看到我有它
there I have it

251
00:08:57,240 --> 00:08:58,839
五个八我是说我像访问数组一样访问它，因为我已经转换了
five eight I mean I'm accessing it like

252
00:08:59,039 --> 00:09:00,309
五个八我是说我像访问数组一样访问它，因为我已经转换了
it was an array because I've converted

253
00:09:00,509 --> 00:09:01,809
基本上我已经说过这个实体内存地址包含
it into an array basically I've said

254
00:09:02,009 --> 00:09:04,959
基本上我已经说过这个实体内存地址包含
that this entity memory address contains

255
00:09:05,159 --> 00:09:07,359
int数组右边的开始，或者它只包含一个指向整数的指针
the beginning of an int array right or

256
00:09:07,559 --> 00:09:08,679
int数组右边的开始，或者它只包含一个指向整数的指针
it just contains a pointer to an integer

257
00:09:08,879 --> 00:09:10,419
然后我只是使用相同的方法查看了一个整数
and then I've just looked one integer

258
00:09:10,620 --> 00:09:12,759
然后我只是使用相同的方法查看了一个整数
ahead using the same kind of method what

259
00:09:12,960 --> 00:09:14,859
我可以做的甚至是更疯狂的事情，也许只是说我想得到
I could do is something even crazier

260
00:09:15,059 --> 00:09:16,839
我可以做的甚至是更疯狂的事情，也许只是说我想得到
perhaps let's just say I wanted to get

261
00:09:17,039 --> 00:09:18,819
当然，任何理智的人都会从中得出Y变量
that Y variable out of it now of course

262
00:09:19,019 --> 00:09:20,500
当然，任何理智的人都会从中得出Y变量
any sane person would just write that

263
00:09:20,700 --> 00:09:22,149
事实上这可能就是我写的一口井，我的意思是男人，因为我
and fact that's probably what I would

264
00:09:22,350 --> 00:09:23,769
事实上这可能就是我写的一口井，我的意思是男人，因为我
write as a well I mean man because I'm

265
00:09:23,970 --> 00:09:27,459
非常理智，但是您可以做的是，您可以像
pretty sane but what you could do is you

266
00:09:27,659 --> 00:09:29,319
非常理智，但是您可以做的是，您可以像
could get the memory address off e like

267
00:09:29,519 --> 00:09:32,500
再次转换为可能像char指针的指针，不确定为什么
that converted into maybe like a char

268
00:09:32,700 --> 00:09:34,870
再次转换为可能像char指针的指针，不确定为什么
pointer again not sure why you would but

269
00:09:35,070 --> 00:09:37,029
我们只说您这样做的大小当然是一个字节，所以我们需要
let's just say that you did that is of

270
00:09:37,230 --> 00:09:39,039
我们只说您这样做的大小当然是一个字节，所以我们需要
course of one byte in size so we need to

271
00:09:39,240 --> 00:09:43,120
像这样向前走四个字节，然后将该内存转换为int指针
go four bytes forward like that and then

272
00:09:43,320 --> 00:09:47,219
像这样向前走四个字节，然后将该内存转换为int指针
convert that memory into an int pointer

273
00:09:47,419 --> 00:09:50,469
然后取消引用，然后我们就可以访问Y了，让我们继续
and then dereference that and then we

274
00:09:50,669 --> 00:09:52,269
然后取消引用，然后我们就可以访问Y了，让我们继续
have access to Y so let's go ahead and

275
00:09:52,470 --> 00:09:56,529
像这样打印这张疯狂的卡片，我会检查一下，我们得到一张
print this crazy piece of card like that

276
00:09:56,730 --> 00:09:59,949
像这样打印这张疯狂的卡片，我会检查一下，我们得到一张
and I'll check that out we get a which

277
00:10:00,149 --> 00:10:02,049
当然是实体的Y位置，但重点是我们所做的
is of course the Y position of the

278
00:10:02,250 --> 00:10:03,250
当然是实体的Y位置，但重点是我们所做的
entity but the point is what we've done

279
00:10:03,450 --> 00:10:05,319
这是我们只是恢复到原始内存操作而已，所以我的意思是
here is we're just kind of just reverted

280
00:10:05,519 --> 00:10:07,089
这是我们只是恢复到原始内存操作而已，所以我的意思是
to raw memory operation so I mean that

281
00:10:07,289 --> 00:10:08,889
simples子句确实做得很好，这就是为什么它是如此强大的语言之一
simples Clause does really well which is

282
00:10:09,090 --> 00:10:10,809
simples子句确实做得很好，这就是为什么它是如此强大的语言之一
why it's such a powerful language one of

283
00:10:11,009 --> 00:10:11,949
之所以这么强大的语言，是因为它具有
the big reasons why it is such a

284
00:10:12,149 --> 00:10:13,779
之所以这么强大的语言，是因为它具有
powerful language because it has that

285
00:10:13,980 --> 00:10:16,959
它可以非常轻松，真正地自由操作内存，
kind of it can manipulate memory really

286
00:10:17,159 --> 00:10:19,659
它可以非常轻松，真正地自由操作内存，
easily and really freely and memory is

287
00:10:19,860 --> 00:10:21,819
到目前为止，我们要做的最大的事情之一就是
by far one of the biggest things we

288
00:10:22,019 --> 00:10:23,799
到目前为止，我们要做的最大的事情之一就是
actually have to deal with when we're

289
00:10:24,000 --> 00:10:27,219
实际编程，因此尽管此代码可能永远不应该像真实的那样存在
actually programming so whilst this code

290
00:10:27,419 --> 00:10:30,339
实际编程，因此尽管此代码可能永远不应该像真实的那样存在
probably should never exist like in real

291
00:10:30,539 --> 00:10:31,990
应用程序，因为它很疯狂，当然在这种情况下，您可以使用
applications because it's crazy and of

292
00:10:32,190 --> 00:10:34,029
应用程序，因为它很疯狂，当然在这种情况下，您可以使用
course in this case you can just use a

293
00:10:34,230 --> 00:10:37,539
点Y，如果您不想处理某种复制或
dot Y it can be very useful if you don't

294
00:10:37,740 --> 00:10:39,159
点Y，如果您不想处理某种复制或
want to deal with kind of copying or

295
00:10:39,360 --> 00:10:42,009
转换，因为再次，如果我们希望将此作为数组
conversions because again if we wanted

296
00:10:42,210 --> 00:10:43,209
转换，因为再次，如果我们希望将此作为数组
this as an array

297
00:10:43,409 --> 00:10:46,178
好吧，我们不喜欢您是否想说要创建自己，我知道我是怎么做到的
well we can't like if you let's just say

298
00:10:46,379 --> 00:10:47,798
好吧，我们不喜欢您是否想说要创建自己，我知道我是怎么做到的
you wanted to create you know how did I

299
00:10:47,999 --> 00:10:51,038
获得职位或其他作为int数组的对象你会如何做得好
get positions or whatever as an int

300
00:10:51,239 --> 00:10:52,899
获得职位或其他作为int数组的对象你会如何做得好
array well how would you do that well I

301
00:10:53,099 --> 00:10:55,358
是的，我猜想很好，您将必须做得很好，这不是数组
mean I guess like well you would have to

302
00:10:55,558 --> 00:10:56,948
是的，我猜想很好，您将必须做得很好，这不是数组
kind of do it well this is not an array

303
00:10:57,149 --> 00:10:59,078
所以我现在必须像一个新的int数组一样创建，因为这不是
so I have to just create like a new int

304
00:10:59,278 --> 00:11:01,208
所以我现在必须像一个新的int数组一样创建，因为这不是
array right now because this isn't a

305
00:11:01,408 --> 00:11:03,668
功能，并且您知道我们不会像使用某种内存那样
function and you know we're not taking

306
00:11:03,869 --> 00:11:05,948
功能，并且您知道我们不会像使用某种内存那样
in like some kind of memory to put that

307
00:11:06,149 --> 00:11:08,258
将该数组放入我们可能需要创建自己的数组，这意味着我们需要
that array into we probably need to

308
00:11:08,458 --> 00:11:10,598
将该数组放入我们可能需要创建自己的数组，这意味着我们需要
create our own array which means we need

309
00:11:10,798 --> 00:11:12,668
堆分配，然后复制它，我什至不想写这段代码
to heap allocated and then copy that and

310
00:11:12,869 --> 00:11:13,658
堆分配，然后复制它，我什至不想写这段代码
I don't even want to write this code

311
00:11:13,859 --> 00:11:15,188
令人恶心的是它的速度太慢了，可怕的是我能做的只是
it's disgusting it's terribly slow it's

312
00:11:15,389 --> 00:11:17,348
令人恶心的是它的速度太慢了，可怕的是我能做的只是
awful what I could do instead is just

313
00:11:17,548 --> 00:11:19,988
返回好我现在当然不知道X的内存地址
return well I don't know memory address

314
00:11:20,188 --> 00:11:23,108
返回好我现在当然不知道X的内存地址
of X right now this of course really

315
00:11:23,308 --> 00:11:25,598
取决于用户不尝试执行类似于第二个位置的操作，因为
depends on the user not trying to do

316
00:11:25,798 --> 00:11:28,718
取决于用户不尝试执行类似于第二个位置的操作，因为
something like position two because

317
00:11:28,918 --> 00:11:30,038
没有第三个元素，但是它将消失到总线到内存，但是
there is no third element and that's

318
00:11:30,239 --> 00:11:32,108
没有第三个元素，但是它将消失到总线到内存，但是
going to go away bus to memory however

319
00:11:32,308 --> 00:11:34,088
您可以看到，如果我跌倒了，我现在可以访问的职位他是一个
you can see that now what I have access

320
00:11:34,288 --> 00:11:37,028
您可以看到，如果我跌倒了，我现在可以访问的职位他是一个
to if I fall you get positions he is an

321
00:11:37,229 --> 00:11:38,889
实际的int数组，其中有两个可以访问的元素，但我不能访问
actual int array with two elements in it

322
00:11:39,089 --> 00:11:41,258
实际的int数组，其中有两个可以访问的元素，但我不能访问
that I can access and I haven't I can

323
00:11:41,458 --> 00:11:44,588
也写信给我，所以我可以说位置零等于二，我改变了
also write to it so I can say position

324
00:11:44,788 --> 00:11:46,508
也写信给我，所以我可以说位置零等于二，我改变了
zero equals two and I've changed the

325
00:11:46,708 --> 00:11:50,198
我实体的x坐标，但我也知道我没有复制任何我没有的东西
x-coordinate of my entity but also I you

326
00:11:50,399 --> 00:11:52,808
我实体的x坐标，但我也知道我没有复制任何我没有的东西
know I haven't copy anything I haven't

327
00:11:53,009 --> 00:11:55,088
冗余复制了内存，我只是链接到了我刚才的内存
copied memory redundantly I've just

328
00:11:55,288 --> 00:11:56,498
冗余复制了内存，我只是链接到了我刚才的内存
linking to the same memory I'm just

329
00:11:56,698 --> 00:11:58,269
如果您不想，选择另一种方式来实际解释该内存
choosing a different way to actually

330
00:11:58,470 --> 00:12:00,548
如果您不想，选择另一种方式来实际解释该内存
interpret that memory if you don't want

331
00:12:00,749 --> 00:12:02,048
处理原始成本，因此您可以使用reinterpret类执行完全相同的操作
to deal with the raw cost so you can use

332
00:12:02,249 --> 00:12:03,848
处理原始成本，因此您可以使用reinterpret类执行完全相同的操作
reinterpret class to do exactly the same

333
00:12:04,048 --> 00:12:06,188
将来我们会为简单的空间成本制作一个视频
thing we will have a video for simple

334
00:12:06,389 --> 00:12:08,678
将来我们会为简单的空间成本制作一个视频
space costs in the future reason I

335
00:12:08,879 --> 00:12:09,548
还没做过，那是因为我不经常使用它们-您会看到样式
haven't done one yet it's because I

336
00:12:09,749 --> 00:12:11,348
还没做过，那是因为我不经常使用它们-您会看到样式
don't use them too often - you see style

337
00:12:11,548 --> 00:12:13,389
大多数时候都过去了，如果我发现星级，我可能应该使用，但是
passed most of the time I probably

338
00:12:13,589 --> 00:12:15,428
大多数时候都过去了，如果我发现星级，我可能应该使用，但是
should use if I spot star class but what

339
00:12:15,629 --> 00:12:17,348
无论如何，你会去做扎布丁吗？
are you gonna do anyway that is

340
00:12:17,548 --> 00:12:19,718
无论如何，你会去做扎布丁吗？
basically tie pudding it's just us being

341
00:12:19,918 --> 00:12:21,908
能够说我将把我拥有的记忆与其他记忆
able to say I'm gonna treat this memory

342
00:12:22,109 --> 00:12:23,408
能够说我将把我拥有的记忆与其他记忆
I have as a different type than it

343
00:12:23,609 --> 00:12:24,878
实际上是，您可以看到我们是否可以真正自由地做到这一点，而这正是我们真正需要的
actually is and you can see if we can do

344
00:12:25,078 --> 00:12:26,978
实际上是，您可以看到我们是否可以真正自由地做到这一点，而这正是我们真正需要的
that really freely all we really need to

345
00:12:27,178 --> 00:12:30,639
要做的只是获取该类型作为指针，然后将其花费到其他指针上
do is just get that type as a pointer

346
00:12:30,839 --> 00:12:33,308
要做的只是获取该类型作为指针，然后将其花费到其他指针上
and then cost it to a different pointer

347
00:12:33,509 --> 00:12:35,258
然后我们可以根据需要取消引用它，反正我会处理它
and then we can dereference it if we

348
00:12:35,458 --> 00:12:37,269
然后我们可以根据需要取消引用它，反正我会处理它
need to and just deal with it anyway I

349
00:12:37,470 --> 00:12:38,378
希望大家喜欢这个视频，如果您喜欢，请按“赞”按钮
hope you guys enjoy this video if you

350
00:12:38,578 --> 00:12:39,458
希望大家喜欢这个视频，如果您喜欢，请按“赞”按钮
did please hit the like button let me

351
00:12:39,658 --> 00:12:40,658
知道您对尝试计划和这种类型的系统的看法，以及为什么会这样
know what you think about try planning

352
00:12:40,859 --> 00:12:43,088
知道您对尝试计划和这种类型的系统的看法，以及为什么会这样
and this type system and why like why

353
00:12:43,288 --> 00:12:44,649
尝试计划很有用，为什么您那么喜欢它，或者您讨厌我不喜欢
try planning is useful and why you love

354
00:12:44,849 --> 00:12:46,958
尝试计划很有用，为什么您那么喜欢它，或者您讨厌我不喜欢
it so much or you do I hated I don't

355
00:12:47,158 --> 00:12:47,490
知道你对它的感觉如何离开你
know

356
00:12:47,690 --> 00:12:49,079
知道你对它的感觉如何离开你
well how do you feel about it leave your

357
00:12:49,279 --> 00:12:51,000
如果您想支持该系列，请在下面的评论部分中发表想法
thoughts in the comment section below if

358
00:12:51,200 --> 00:12:52,769
如果您想支持该系列，请在下面的评论部分中发表想法
you want to support the series please go

359
00:12:52,970 --> 00:12:54,809
对佩特拉非作曲家-切尔诺像往常一样大声喊叫给我所有可爱的人
to Petra non compos - to Cherno huge

360
00:12:55,009 --> 00:12:56,370
对佩特拉非作曲家-切尔诺像往常一样大声喊叫给我所有可爱的人
shout out as always to all my lovely

361
00:12:56,570 --> 00:12:58,229
支持者，如果没有你们，这个系列赛就不会在这里了，谢谢，我会
supporters this series would not be here

362
00:12:58,429 --> 00:13:00,809
支持者，如果没有你们，这个系列赛就不会在这里了，谢谢，我会
without you guys so thank you and I'll

363
00:13:01,009 --> 00:13:03,459
下次再见[音乐]
see you next time goodbye

364
00:13:03,659 --> 00:13:08,659
下次再见[音乐]
[Music]

