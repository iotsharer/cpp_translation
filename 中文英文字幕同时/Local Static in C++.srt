﻿1
00:00:00,000 --> 00:00:03,309
Hey look guys my name is a chana welcome back to my stable flood theories over
嘿，伙计们，我的名字很受欢迎，欢迎回到我稳定的洪水理论上

2
00:00:03,509 --> 00:00:06,879
the last couple of episodes we took a look at what the static keyword meant in
在最后几集中，我们了解了static关键字的含义

3
00:00:07,080 --> 00:00:11,169
certain contexts and today we're going to have a look at one other contact that
在某些情况下，今天我们将看看另一位联系人

4
00:00:11,369 --> 00:00:14,410
you might find the static keyword in and that is in a local scope so you can
您可能会在本地范围内找到static关键字，因此您可以

5
00:00:14,609 --> 00:00:18,609
actually use that akin a local scope to declare a variable and that has a bit of
实际上使用类似于局部作用域的方法来声明变量，并且该变量具有

6
00:00:18,809 --> 00:00:22,030
a different meaning than the altitude static that we looked at and the idea
与我们观察到的海拔高度静静和想法不同

7
00:00:22,230 --> 00:00:25,030
behind this meaning makes a lot more sense if you think about the two kind of
如果您考虑两种

8
00:00:25,230 --> 00:00:28,600
considerations about to make one way declaring a variable and that is the
考虑采取一种声明变量的方式，那就是

9
00:00:28,800 --> 00:00:32,349
lifetime of the variable and the scope of the variable the lifetime refers to
变量的生存期以及生存期所指变量的范围

10
00:00:32,549 --> 00:00:36,070
how long that variable will actually stick around for in other words how long
该变量将持续多长时间，换句话说，持续多长时间

11
00:00:36,270 --> 00:00:40,119
it will remain in our memory before it gets deleted and then the scope refers
它将在删除之前保留在我们的内存中，然后作用域引用

12
00:00:40,320 --> 00:00:44,169
to where we can actually access that variable so of course if we declare a
到实际上可以访问该变量的位置，所以如果我们声明一个

13
00:00:44,369 --> 00:00:47,678
variable is that a function for example we can't just access it in another
变量是一个函数，例如，我们不能只在另一个函数中访问它

14
00:00:47,878 --> 00:00:51,070
function because the variable we declared is going to be local to the
函数，因为我们声明的变量将是局部的

15
00:00:51,270 --> 00:00:55,570
function we declared it in so a static local variable allows us to declare a
函数我们在其中声明了它，因此静态局部变量允许我们声明一个

16
00:00:55,770 --> 00:01:00,788
variable that has a lifetime of essentially our entire program however
变量的寿命实际上是整个程序的寿命

17
00:01:00,988 --> 00:01:05,738
its scope is limited to be inside that function and it really has nothing to do
它的范围仅限于该函数内部，并且实际上与它无关

18
00:01:05,938 --> 00:01:09,819
with functions in particular I mean you can declare this in any scope really I'm
特别是带有函数的意思是我可以在任何范围内声明这个

19
00:01:10,019 --> 00:01:13,329
just using function as an example it's not really just limited to being inside
仅以功能为例，并不仅仅局限于内部

20
00:01:13,530 --> 00:01:17,590
a function could be in front if statement to be anywhere which is why
如果语句在任何地方，则函数可能在最前面，这就是为什么

21
00:01:17,790 --> 00:01:20,649
again there's not much of a difference between static in a function scope
同样，在功能范围内，静态之间没有太大区别

22
00:01:20,849 --> 00:01:25,028
versus static in a class scope because the life time is actually going to be
相对于静态在类范围内，因为生命周期实际上是

23
00:01:25,228 --> 00:01:29,140
the same the only difference is that the one in the class scope anything in the
唯一的区别是，该类中的作用域作用于

24
00:01:29,340 --> 00:01:32,649
class can access it anything that has anything that is inside that class scope
类可以访问该类范围内的任何内容

25
00:01:32,849 --> 00:01:35,890
can access that static variable however of course if you declare one inside a
可以访问该静态变量，但是，如果在一个

26
00:01:36,090 --> 00:01:39,399
function scope it's going to be local to that function of living local to that
功能范围将仅限于该功能

27
00:01:39,599 --> 00:01:42,819
class let's jump in and take a look at some examples so the simplest example is
让我们进入并看一些例子，所以最简单的例子是

28
00:01:43,019 --> 00:01:46,659
going to be just to create a function and then declare some kind of static
只是创建一个函数，然后声明某种静态

29
00:01:46,859 --> 00:01:51,429
variable inside it so I'll just make that again I equals zero what this means
它里面的变量，所以我将再次使其等于零，这意味着

30
00:01:51,629 --> 00:01:55,329
is that when I call functions to the third time this variable will be
是当我第三次调用函数时

31
00:01:55,530 --> 00:01:59,230
initialized to zero and then on subsequent order functions it's not
初始化为零，然后在后续的订单函数上

32
00:01:59,430 --> 00:02:02,918
actually going to create a brand new variable really easy way to check this
实际要创建一个全新的变量，非常简单的方法来检查这一点

33
00:02:03,118 --> 00:02:07,329
out is if I just print the value of I and then I increment it every time I
出来的是，如果我只是打印I的值，然后每次我增加I 

34
00:02:07,530 --> 00:02:11,469
call the function so if we didn't have static here what you would expect is
调用该函数，因此，如果我们在此处没有静态的，则您期望的是

35
00:02:11,669 --> 00:02:14,429
that time you call this function I get set to
那个时候你调用这个函数

36
00:02:14,628 --> 00:02:18,929
zero then I get incremented to one and then we print one - pecan well and in
零，然后我增加到一，然后我们打印一个-山核桃以及

37
00:02:19,128 --> 00:02:21,899
fact we can test that out by just calling function a whole bunch of times
实际上，我们可以通过多次调用函数来进行测试

38
00:02:22,098 --> 00:02:26,429
here we'll call it five times we'll hit up five to run our program and you can
在这里，我们将其称为五次，我们将五次调用它来运行我们的程序，您可以

39
00:02:26,628 --> 00:02:30,179
see that we get one printing five times because as I just explained we create a
看到我们一次打印了五次，因为正如我刚才所解释的，我们创建了一个

40
00:02:30,378 --> 00:02:34,230
variable every single time and we set it equal to zero we then increment it to
每一次变量，并将其设置为零，然后将其递增到

41
00:02:34,430 --> 00:02:38,368
one and then we print that to the console now if we wish make this static
一个，然后我们现在将其打印到控制台，如果我们希望使此静态

42
00:02:38,568 --> 00:02:44,039
then it's very very similar to if we had just moved this original declaration out
那么这与我们刚刚将原始声明移出的情况非常相似

43
00:02:44,239 --> 00:02:48,629
here so if we run this code and it doesn't really matter if this has static
在这里，所以如果我们运行这段代码，并且它是否具有静态的值并不重要

44
00:02:48,829 --> 00:02:51,360
or not and our case is going to be the same if you want to know more about what
是否，如果您想更多地了解什么，我们的情况将是相同的

45
00:02:51,560 --> 00:02:55,800
static doesn't that sense you can check out the video that's linked somewhere so
静态不是那种感觉，您可以检出链接到某处的视频，因此

46
00:02:56,000 --> 00:03:01,319
if I run this code you expect I to be 0 incremented 5 times and so you'll get 1
如果我运行此代码，则您希望我将0递增5次，因此您将获得1 

47
00:03:01,519 --> 00:03:04,950
2 3 4 5 which is exactly what you get however the problem with this approach
 2 3 4 5正是您所得到的，但是这种方法存在问题

48
00:03:05,150 --> 00:03:12,179
is that I can access I any where I could set I equal to 10 over here in between
是我可以访问的任何地方都可以设置为等于10 

49
00:03:12,378 --> 00:03:15,989
function calls and suddenly this dramatically changes what our program
函数调用，这突然改变了我们的程序

50
00:03:16,188 --> 00:03:21,629
does so the cases where you want this kind of behavior however you do not want
这样做是在您想要这种行为但您不想要的情况下

51
00:03:21,829 --> 00:03:25,950
to give everyone access to that variable you can declare static in a local scope
为了让所有人都可以访问该变量，您可以在本地范围内声明静态

52
00:03:26,150 --> 00:03:29,429
so let's get rid of this and move our I variable back into the function scope
所以让我们摆脱它，将我们的I变量移回函数范围

53
00:03:29,628 --> 00:03:34,230
I'm going to stick static out the front which means that again the first time we
我要把静电放在前面，这意味着我们第一次

54
00:03:34,430 --> 00:03:37,709
run this function is going to set it equal to 0 and create a variable in the
运行此函数将其设置为0并在

55
00:03:37,908 --> 00:03:41,159
first place then on subsequent times is just going to refer to that original
第一位，然后在随后的时间中，将仅引用该原始图片

56
00:03:41,359 --> 00:03:46,499
variable so if I run micro now you can see I get 1 2 3 4 5 and I get the exact
变量，所以如果我现在运行微型，您可以看到我得到1 2 3 4 5，我得到精确的

57
00:03:46,699 --> 00:03:50,730
same behavior as I did before however that I variable isn't visible no global
与以前相同的行为，但是我的变量不可见，没有全局变量

58
00:03:50,930 --> 00:03:55,099
scope it's just local to that function now as for uses some people do like to
范围，它只是该功能的局部功能，因为某些人喜欢

59
00:03:55,299 --> 00:03:59,580
discourage the use of this because of certain reasons that I don't fully
由于某些原因（我不完全了解）而不鼓励使用此功能

60
00:03:59,780 --> 00:04:03,149
understand because I don't really see a problem with this it definitely has its
理解，因为我真的没有看到这个问题，它肯定有

61
00:04:03,348 --> 00:04:07,499
use it's one of those things where yes you can achieve the exact same behavior
使用它是可以实现完全相同行为的事情之一

62
00:04:07,699 --> 00:04:12,118
using other methods however that can also be said for classes you don't
使用其他方法，但是对于您不使用的类也可以说

63
00:04:12,318 --> 00:04:15,930
necessarily have to use classes at all to write a program however it does make
一定要使用类来编写程序，但是它确实使

64
00:04:16,129 --> 00:04:19,588
your life easier same with this case it's one of those things that you can
在这种情况下，您的生活会更轻松，这是您可以做的其中一件事情

65
00:04:19,788 --> 00:04:21,629
just do to keep your code a little bit cleaner
只是为了使您的代码更整洁

66
00:04:21,829 --> 00:04:26,379
another example might be if you singleton class so singleton class is a
另一个例子可能是如果您单身人士班级，所以单身人士班级是

67
00:04:26,579 --> 00:04:30,610
class that should only have one instance in existence if I wanted to create a
如果我想创建一个只存在一个实例的类

68
00:04:30,810 --> 00:04:34,750
singleton class without using this static local scope thing I would have to
单例类，而无需使用此静态局部范围的东西

69
00:04:34,949 --> 00:04:39,790
create some kind of static singleton instance probably a pointer at this rate
创建某种静态单例实例，可能以此速率指向一个指针

70
00:04:39,990 --> 00:04:44,620
I'll set it here I would have to have a if I just want to return a normal
我要在这里设置它，如果我只想返回正常值，则必须有一个

71
00:04:44,819 --> 00:04:48,610
reference I would have to have a single some reference returning get function
参考我将必须有一个参考返回get函数

72
00:04:48,810 --> 00:04:53,139
which was static and returned my instance after the referencing it of
这是静态的，并在引用它后返回我的实例

73
00:04:53,339 --> 00:04:57,879
course I would then have to actually declare this instance over here single
当然，我将不得不在这里单独声明该实例

74
00:04:58,079 --> 00:05:01,810
term single term as instance I can probably set it equal to the melon
我可以将其设置为等于瓜

75
00:05:02,009 --> 00:05:06,579
pointer as a default and now I have the ability let's get rid of this now of
指针作为默认值，现在我有能力摆脱这种情况

76
00:05:06,779 --> 00:05:11,319
course I have my singleton class in which I could just call singleton get
当然我有单身人士班，我可以在其中叫单身人士获得

77
00:05:11,519 --> 00:05:15,420
and then do whatever I want with it so suppose we had a method here called
然后做我想做的任何事情，所以假设我们这里有一个方法

78
00:05:15,620 --> 00:05:20,259
hello don't let me know what I'm doing just an example was call a hello method
你好，不要让我知道我在做什么，只是一个例子被称为hello方法

79
00:05:20,459 --> 00:05:22,509
and everything will be well you can see we're gonna call one instance of that
一切都会好起来的，您可以看到我们将调用该实例的一个实例

80
00:05:22,709 --> 00:05:25,840
class that we can use so we can kind of use it statically now this is quite a
我们可以使用的类，这样我们就可以静态地使用它了

81
00:05:26,040 --> 00:05:29,710
lot of code you don't necessarily have to do it this way another way to do this
很多代码，您不一定非要用另一种方式做到这一点

82
00:05:29,910 --> 00:05:33,370
with our newfound knowledge of this local static thing might be like this we
有了我们对这一局部静态事物的新发现，我们可能会像这样

83
00:05:33,569 --> 00:05:39,189
can get rid of this entirely we can get rid of this external definition of F
可以完全摆脱这一点，我们可以摆脱F的外部定义

84
00:05:39,389 --> 00:05:45,100
instance as well and then we can just expand our guest function to actually
实例，然后我们可以将访客功能扩展到实际

85
00:05:45,300 --> 00:05:52,420
create a static singleton instance and then return that instance and there you
创建一个静态单例实例，然后返回该实例，在那里

86
00:05:52,620 --> 00:05:55,420
go we get the exact same behavior you can see everything here stays the same
我们得到的行为完全相同，您可以看到这里的一切保持不变

87
00:05:55,620 --> 00:05:59,199
we can run our code no problem of course the codes not actually going to do
我们可以运行我们的代码，当然没问题，这些代码实际上不会做

88
00:05:59,399 --> 00:06:02,620
anything at this point we can see it run successfully without crashing and we
此时，我们可以看到它成功运行而不会崩溃，我们

89
00:06:02,819 --> 00:06:05,319
don't really have any problems with this at all and you can see that our code is
根本没有任何问题，您可以看到我们的代码是

90
00:06:05,519 --> 00:06:11,470
a lot lot cleaner now of course if you didn't have the static keyword here then
当然，如果您在此处没有static关键字，那么现在要干净得多

91
00:06:11,670 --> 00:06:16,540
this singleton since it is just created on the staff would get destroyed as soon
这个单例，因为它只是在工作人员上创建的，将很快被销毁

92
00:06:16,740 --> 00:06:21,009
as you hit this curly bracket and then the functions curve ends so this would
当您点击此花括号，然后函数曲线结束时， 

93
00:06:21,209 --> 00:06:23,500
be a grave error especially if you're returning it by reference if you're
是一个严重的错误，尤其是如果您通过引用将其返回时

94
00:06:23,699 --> 00:06:26,800
copying it of course you wouldn't really have a problem however since we are
复制它当然不会有什么问题，但是因为我们

95
00:06:27,000 --> 00:06:31,120
resetting a reference to it this would would be a huge problem however by
重置对它的引用将是一个巨大的问题，但是通过

96
00:06:31,319 --> 00:06:35,230
adding static we're extending the last time of death to be eventually for
加上静态，我们将死亡的最后时间延长到最终

97
00:06:35,430 --> 00:06:38,620
rather which means that every time we call get the first time it will actually
而是意味着每次我们调用一次都会得到一次

98
00:06:38,819 --> 00:06:42,670
construct that singleton instance and then are all subsequent times it will
构造那个单例实例，然后是所有后续时间

99
00:06:42,870 --> 00:06:46,389
just return that existing instance so this is a great example of where you
只需返回现有实例，这就是您在哪里的一个很好的例子

100
00:06:46,589 --> 00:06:49,120
might want to use something like this it doesn't necessarily have to be with
可能想要使用类似这样的东西，它不一定必须与

101
00:06:49,319 --> 00:06:52,840
single terms it can help you out a lot by replacing initialization functions
单个术语，它可以通过替换初始化函数为您提供很多帮助

102
00:06:53,040 --> 00:06:56,829
where for example you might have to call a static initializer where for example
例如，您可能必须调用静态初始化程序，例如

103
00:06:57,029 --> 00:07:00,129
you might have to call it static initialization function at some point in
您可能需要在某个时候将其称为静态初始化函数

104
00:07:00,329 --> 00:07:02,860
your program to create all of your objects so that you could actually use
您的程序可以创建所有对象，以便您可以实际使用

105
00:07:03,060 --> 00:07:06,100
the static get methods and stuff like that you can just kind of simplify that
静态获取方法和类似的东西，您可以简化一下

106
00:07:06,300 --> 00:07:10,660
by using this in a lot of cases so you can see there are tons of uses to this
通过在很多情况下使用它，您可以看到有很多用途

107
00:07:10,860 --> 00:07:15,850
and it's really not all that bad so feel free to use it that's my opinion let me
并不是真的那么糟糕，所以随时使用它是我的意见

108
00:07:16,050 --> 00:07:19,060
know what you guys think of this static in a local scope thing in the comments
在评论中知道你们对本地范围内的静态事物的看法

109
00:07:19,259 --> 00:07:22,990
below and I'll see you guys in the next video goodbye
下面，我会在下一个视频中再见大家

110
00:07:23,189 --> 00:07:28,430
[Music]
[音乐]

111
00:07:35,160 --> 00:07:40,160
[Music]
 [音乐] 

