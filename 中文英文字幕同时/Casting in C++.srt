﻿1
00:00:00,000 --> 00:00:01,209
嘿，各位，我的名字叫“中国”，欢迎回到我的C ++系列，所以今天
hey what look guys my name is a China

2
00:00:01,409 --> 00:00:04,118
嘿，各位，我的名字叫“中国”，欢迎回到我的C ++系列，所以今天
welcome back to my C++ series so today

3
00:00:04,318 --> 00:00:05,649
我们将要谈论的是C ++的成本计算，我们真的还没有谈论过
we're gonna be talking all about costing

4
00:00:05,849 --> 00:00:07,359
我们将要谈论的是C ++的成本计算，我们真的还没有谈论过
in C++ we really haven't talked about

5
00:00:07,559 --> 00:00:10,060
尽管我们实际上已经使用了成本核算，但通常会谈论
this often although we have in fact used

6
00:00:10,259 --> 00:00:12,430
尽管我们实际上已经使用了成本核算，但通常会谈论
costing and will talk more about kind of

7
00:00:12,630 --> 00:00:14,349
说样式成本计算，同时也节省了我们的成本，而这个视频
say style costing as well as save us

8
00:00:14,548 --> 00:00:17,019
说样式成本计算，同时也节省了我们的成本，而这个视频
lost out costing and this this video one

9
00:00:17,219 --> 00:00:18,699
进行一系列后续操作，绝对可以使所有内容着色
color absolutely everything we'll get

10
00:00:18,899 --> 00:00:20,289
进行一系列后续操作，绝对可以使所有内容着色
into this as a series kind of goes on

11
00:00:20,489 --> 00:00:23,019
我认为该主题只是您必须要做的事情之一
and I think that this topic is one of

12
00:00:23,219 --> 00:00:24,190
我认为该主题只是您必须要做的事情之一
those things that you just have to

13
00:00:24,390 --> 00:00:26,409
与他人一起练习并通过经验学习，而不是我只是在讲
practice with and kind of learn through

14
00:00:26,609 --> 00:00:27,999
与他人一起练习并通过经验学习，而不是我只是在讲
experience rather than me just telling

15
00:00:28,199 --> 00:00:29,829
你这就是它的运作方式，因为如果你只是不加实践地接受理论
you this is how it works because if you

16
00:00:30,028 --> 00:00:31,629
你这就是它的运作方式，因为如果你只是不加实践地接受理论
just take in the theory with no practice

17
00:00:31,829 --> 00:00:33,788
对于这个特定的主题，它并没有真正帮助您，所以请继续
for this specific topic it's not really

18
00:00:33,988 --> 00:00:36,279
对于这个特定的主题，它并没有真正帮助您，所以请继续
gonna help you that much so just keep

19
00:00:36,479 --> 00:00:37,209
考虑到还可以，所以首先要很好地铸造
that in mind okay cool

20
00:00:37,409 --> 00:00:38,619
考虑到还可以，所以首先要很好地铸造
so first of all what is casting well

21
00:00:38,820 --> 00:00:39,820
我在谈论演员，我特别在谈论类型转换
what I'm talking about casting I'm

22
00:00:40,020 --> 00:00:41,198
我在谈论演员，我特别在谈论类型转换
specifically talking about typecasting

23
00:00:41,399 --> 00:00:43,628
或我们必须在类型系统中进行的任何类型的转换
or any kind of conversions that we have

24
00:00:43,829 --> 00:00:45,579
或我们必须在类型系统中进行的任何类型的转换
to do within the type system that we

25
00:00:45,780 --> 00:00:48,608
可以用C ++向我们提供C ++基本上是一种强类型语言
have available to us in C++ C++ being a

26
00:00:48,808 --> 00:00:50,349
可以用C ++向我们提供C ++基本上是一种强类型语言
strongly typed language basically just

27
00:00:50,549 --> 00:00:53,320
表示如果我将某物作为
means that there is a type system and

28
00:00:53,520 --> 00:00:55,809
表示如果我将某物作为
types enforce if I make something as an

29
00:00:56,009 --> 00:00:56,288
整数我不能突然开始治疗它
integer

30
00:00:56,488 --> 00:00:58,448
整数我不能突然开始治疗它
I can't suddenly just start treating it

31
00:00:58,649 --> 00:01:00,219
好像是双重或调情之类的，反之亦然
as if it was a double or a flirt or

32
00:01:00,420 --> 00:01:02,408
好像是双重或调情之类的，反之亦然
something like that or vice versa I have

33
00:01:02,609 --> 00:01:04,329
坚持我的类型，除非有一个简单的隐式转换
to kind of stick to my type unless

34
00:01:04,530 --> 00:01:06,488
坚持我的类型，除非有一个简单的隐式转换
there's an easy implicit conversion

35
00:01:06,688 --> 00:01:09,099
这意味着这种简单的交换知道如何在两种类型之间进行转换
which means that this simple swaps knows

36
00:01:09,299 --> 00:01:10,209
这意味着这种简单的交换知道如何在两种类型之间进行转换
how to convert between the two types

37
00:01:10,409 --> 00:01:12,640
没有数据丢失，基本上标记为隐式转换为
with no data loss and basically what's

38
00:01:12,840 --> 00:01:14,319
没有数据丢失，基本上标记为隐式转换为
labeled as an implicit conversion as

39
00:01:14,519 --> 00:01:16,539
好吧，或者有一个明确的转换，这就是我实际上告诉的地方
well or there's an explicit conversion

40
00:01:16,739 --> 00:01:18,099
好吧，或者有一个明确的转换，这就是我实际上告诉的地方
which is where I'm actually telling

41
00:01:18,299 --> 00:01:20,500
很简单，嘿，您现在需要将这种类型转换为这种类型
simple as hey you need to convert this

42
00:01:20,700 --> 00:01:24,250
很简单，嘿，您现在需要将这种类型转换为这种类型
type into this type now we have covered

43
00:01:24,450 --> 00:01:27,219
谈到类型时，某种类型的转换和类型转换
kind of casting and type conversions to

44
00:01:27,420 --> 00:01:28,929
谈到类型时，某种类型的转换和类型转换
an extent when we talked about type

45
00:01:29,129 --> 00:01:30,609
pun，如果您还没有看过该视频，请务必查看
punting so if you haven't seen that

46
00:01:30,810 --> 00:01:31,929
pun，如果您还没有看过该视频，请务必查看
video definitely check that out

47
00:01:32,129 --> 00:01:33,789
但在这段视频中，我们将正式报道
but in this video we're gonna kind of

48
00:01:33,989 --> 00:01:35,980
但在这段视频中，我们将正式报道
formally cover what casting actually

49
00:01:36,180 --> 00:01:39,128
表示并查看我们如何使用它，以便我们执行类型转换的方法是
means and see how we can use it so the

50
00:01:39,328 --> 00:01:40,569
表示并查看我们如何使用它，以便我们执行类型转换的方法是
way that we perform type conversions is

51
00:01:40,769 --> 00:01:43,539
显式类型转换实际上是至少两种方法之一，或者至少我看到了
visibly explicit type conversions is one

52
00:01:43,739 --> 00:01:45,278
显式类型转换实际上是至少两种方法之一，或者至少我看到了
of two ways really or at least I see it

53
00:01:45,478 --> 00:01:47,109
作为两种方式之一，存在某种C样式成本，然后我们还有一个C ++
as one of two ways there's the kind of C

54
00:01:47,310 --> 00:01:49,689
作为两种方式之一，存在某种C样式成本，然后我们还有一个C ++
style costs and then we also have a C++

55
00:01:49,890 --> 00:01:51,579
现在，如果我们看一个简单的例子，样式成本就高了
style costs now if we just take a look

56
00:01:51,780 --> 00:01:53,259
现在，如果我们看一个简单的例子，样式成本就高了
at a quick example I just say I've

57
00:01:53,459 --> 00:01:55,659
得到这样的整数，我将其称为a，并将其设置为等于5，然后
gotten the integer like this I'll call

58
00:01:55,859 --> 00:01:57,909
得到这样的整数，我将其称为a，并将其设置为等于5，然后
it a and I'll set it equal to 5 and I

59
00:01:58,109 --> 00:01:59,049
我想在这种情况下现在将其视为一倍
want to I want to treat it as a double

60
00:01:59,250 --> 00:02:02,049
我想在这种情况下现在将其视为一倍
now in this scenario if I make a double

61
00:02:02,250 --> 00:02:05,079
调用值并将其设置为等于a，您可以看到我们不需要
called value and set it equal to a you

62
00:02:05,280 --> 00:02:06,759
调用值并将其设置为等于a，您可以看到我们不需要
can see that that we don't need to

63
00:02:06,959 --> 00:02:09,169
明确指定嘿，我想让你成双，因为那是一个
explicitly specify hey I want you

64
00:02:09,368 --> 00:02:11,899
明确指定嘿，我想让你成双，因为那是一个
a into a double because that's that's a

65
00:02:12,098 --> 00:02:13,429
这种转换被认为是隐式的，很容易做到
that's kind of a conversion that it

66
00:02:13,628 --> 00:02:15,679
这种转换被认为是隐式的，很容易做到
deems as implicit it's easy to do

67
00:02:15,878 --> 00:02:16,850
如果我有其他方法，现在不会有类似的数据丢失
there's no data loss anything like that

68
00:02:17,050 --> 00:02:19,310
如果我有其他方法，现在不会有类似的数据丢失
now if I had something the other way

69
00:02:19,509 --> 00:02:21,200
在我可能拥有双倍价值的地方，大约等于五点三五点
around where I had maybe double value

70
00:02:21,400 --> 00:02:23,390
在我可能拥有双倍价值的地方，大约等于五点三五点
equals like five point three five point

71
00:02:23,590 --> 00:02:24,649
二五个或类似的东西，我想将其转换为整数
two five or something like that and I

72
00:02:24,848 --> 00:02:27,710
二五个或类似的东西，我想将其转换为整数
wanted to convert it into a integer that

73
00:02:27,909 --> 00:02:29,599
也将被视为隐式转换，因为它不是
would also be deemed as an implicit

74
00:02:29,799 --> 00:02:31,189
也将被视为隐式转换，因为它不是
conversion because it's not something

75
00:02:31,389 --> 00:02:33,289
我们一定要指定嘿，我正在将这种类型转换为这种类型
that we necessarily have to specify hey

76
00:02:33,489 --> 00:02:34,789
我们一定要指定嘿，我正在将这种类型转换为这种类型
I'm converting this type into this type

77
00:02:34,989 --> 00:02:37,399
现在，如果我们确实想明确地举例说明，我们实际上可以做的就是
now if we did want to be explicit for

78
00:02:37,598 --> 00:02:40,340
现在，如果我们确实想明确地举例说明，我们实际上可以做的就是
example what we can actually do is just

79
00:02:40,539 --> 00:02:43,910
通过立即执行以下操作将此双精度转换为整数
cast this double into into an integer by

80
00:02:44,110 --> 00:02:45,349
通过立即执行以下操作将此双精度转换为整数
doing something like this right now of

81
00:02:45,549 --> 00:02:47,118
当然，在这种情况下，它隐含地能够做到这一点，但为此
course in this case it implicitly is

82
00:02:47,318 --> 00:02:48,830
当然，在这种情况下，它隐含地能够做到这一点，但为此
able to do that but for the sake of this

83
00:02:49,030 --> 00:02:50,629
因为我们在谈论成本，所以我们假装不是，所以
let's and since we are talking about

84
00:02:50,829 --> 00:02:52,580
因为我们在谈论成本，所以我们假装不是，所以
cost and let's pretend it wasn't so we

85
00:02:52,780 --> 00:02:53,960
有一个隐式，我们在这里有一个显式转换，因为我们是说我
have an implicit we have an explicit

86
00:02:54,159 --> 00:02:55,490
有一个隐式，我们在这里有一个显式转换，因为我们是说我
conversion here because we're saying I

87
00:02:55,689 --> 00:02:57,289
希望此值现在对整数具有竞争力，为什么我们需要这种值
want this value to be competitive to an

88
00:02:57,489 --> 00:03:00,849
希望此值现在对整数具有竞争力，为什么我们需要这种值
integer now why we need this is kind of

89
00:03:01,049 --> 00:03:05,000
也许这不是一个更好的例子，例如，如果我们想拥有
it's not really perhaps a better example

90
00:03:05,199 --> 00:03:07,429
也许这不是一个更好的例子，例如，如果我们想拥有
might be like if we wanted to maybe have

91
00:03:07,628 --> 00:03:10,640
另一双在这里称为a，我们想添加五个点三
another double here called a and we

92
00:03:10,840 --> 00:03:13,400
另一双在这里称为a，我们想添加五个点三
wanted to add you know five point three

93
00:03:13,599 --> 00:03:17,360
如果我们只是摆脱了这一点并打印出实际上是
to this value now if we just kind of get

94
00:03:17,560 --> 00:03:20,289
如果我们只是摆脱了这一点并打印出实际上是
rid of this and print what a actually is

95
00:03:20,489 --> 00:03:22,969
那么您可能会看到的显然是您对某种东西的期望
then what you might see it's obviously

96
00:03:23,169 --> 00:03:24,890
那么您可能会看到的显然是您对某种东西的期望
what you might expect with kind of

97
00:03:25,090 --> 00:03:26,990
如果我将此值计入一个
adding five point two five to five point

98
00:03:27,189 --> 00:03:30,770
如果我将此值计入一个
three now if I cost this value into an

99
00:03:30,969 --> 00:03:32,629
整数，那么它将被转换为整数，这意味着它将
integer then that's going to converted

100
00:03:32,829 --> 00:03:33,830
整数，那么它将被转换为整数，这意味着它将
into an integer which means it's going

101
00:03:34,030 --> 00:03:35,300
截去第二点五点，最后我只能是五点五点
to truncate at the point two five and

102
00:03:35,500 --> 00:03:36,950
截去第二点五点，最后我只能是五点五点
I'll end up just being five plus five

103
00:03:37,150 --> 00:03:38,240
第三点而不是给我们十点五五会给我们
point three which instead of giving us

104
00:03:38,439 --> 00:03:40,610
第三点而不是给我们十点五五会给我们
ten point five five would give us the

105
00:03:40,810 --> 00:03:42,379
值十点三提交在这种情况下实际上是改变它，这
value ten point three submit in this

106
00:03:42,579 --> 00:03:44,210
值十点三提交在这种情况下实际上是改变它，这
case is actually changing it and this

107
00:03:44,409 --> 00:03:45,800
我们使用的实际技术称为安全样式成本，因为我们
actual technique that we've used is

108
00:03:46,000 --> 00:03:47,480
我们使用的实际技术称为安全样式成本，因为我们
called a safe style cost because we

109
00:03:47,680 --> 00:03:49,069
基本上在括号中指定了导致这种类型的类型，例如
basically specified the type that were

110
00:03:49,269 --> 00:03:51,259
基本上在括号中指定了导致这种类型的类型，例如
causing to in parentheses like this and

111
00:03:51,459 --> 00:03:52,250
那么我们实际上可以选择花费的变量我们当然可以
then the variable that we're actually

112
00:03:52,449 --> 00:03:54,580
那么我们实际上可以选择花费的变量我们当然可以
costing optionally we can of course

113
00:03:54,780 --> 00:03:57,530
像这样用括号括起来，如果我们
surrounded in parentheses like this

114
00:03:57,729 --> 00:03:59,420
像这样用括号括起来，如果我们
which is particularly useful if maybe we

115
00:03:59,620 --> 00:04:01,789
想要在将其实际评估为
wanted to cost that entire thing into an

116
00:04:01,989 --> 00:04:04,069
想要在将其实际评估为
int once it had actually evaluated in

117
00:04:04,269 --> 00:04:06,080
事实上，如果我们这样做，仍将获得10作为值，因为值将为5
fact if we do this will still get 10 as

118
00:04:06,280 --> 00:04:08,780
事实上，如果我们这样做，仍将获得10作为值，因为值将为5
the value because value is gonna be five

119
00:04:08,979 --> 00:04:09,980
第二点五点加五点三点给我们十点五点五点
point two five plus five point three

120
00:04:10,180 --> 00:04:11,629
第二点五点加五点三点给我们十点五点五点
which gives us ten point five five and

121
00:04:11,829 --> 00:04:13,789
然后它会被截断为蚂蚁，现在实现此目的的C ++方法是
then it would get truncated into an ant

122
00:04:13,989 --> 00:04:16,908
然后它会被截断为蚂蚁，现在实现此目的的C ++方法是
now the C++ way of doing this would be

123
00:04:17,108 --> 00:04:17,889
通过同时用作星空广播，将有四天的时间
by using as a both

124
00:04:18,089 --> 00:04:20,348
通过同时用作星空广播，将有四天的时间
starcast and it'll be four days who

125
00:04:20,548 --> 00:04:22,259
将使用一种称为静态类型转换的东西，并且基本上做到这一点
would use something called a static cast

126
00:04:22,459 --> 00:04:25,480
将使用一种称为静态类型转换的东西，并且基本上做到这一点
and to do that basically this would look

127
00:04:25,680 --> 00:04:27,400
像下面这样，所以让我回到我们之前的位置
like the following so let me just get

128
00:04:27,600 --> 00:04:29,829
像下面这样，所以让我回到我们之前的位置
back to where we were before which is

129
00:04:30,029 --> 00:04:32,980
就是这个，这就是我们要传递的静态成本
just this and then it's been this would

130
00:04:33,180 --> 00:04:36,699
就是这个，这就是我们要传递的静态成本
just be a static cost what we're passing

131
00:04:36,899 --> 00:04:38,800
它需要的是什么，然后是值，然后我们将5.3加
into which is it needs and then the

132
00:04:39,000 --> 00:04:42,189
它需要的是什么，然后是值，然后我们将5.3加
value and then we'd add 5.3 and that

133
00:04:42,389 --> 00:04:45,939
现在将是我们的价值C ++贴图成本我们有很多成本其中之一是
would be our value now C++ tile costs we

134
00:04:46,139 --> 00:04:47,379
现在将是我们的价值C ++贴图成本我们有很多成本其中之一是
have a number of them one of them is

135
00:04:47,579 --> 00:04:48,999
叫做静态成本，我们还有别的东西，叫做重新解释成本
called static cost we have something

136
00:04:49,199 --> 00:04:50,740
叫做静态成本，我们还有别的东西，叫做重新解释成本
else called reinterpret cost we have

137
00:04:50,939 --> 00:04:52,360
所谓的动态成本，而我们有所谓的const
something called dynamic cost and we

138
00:04:52,560 --> 00:04:53,680
所谓的动态成本，而我们有所谓的const
have something something called const

139
00:04:53,879 --> 00:04:55,300
成本，现在您要意识到的是四种主要成本
cost there's a kind of the four main

140
00:04:55,500 --> 00:04:57,610
成本，现在您要意识到的是四种主要成本
costs now what you have to realize is

141
00:04:57,810 --> 00:05:00,850
他们没有做任何C样式转换无法完成我的意思的事情
that they do not do anything that C

142
00:05:01,050 --> 00:05:04,210
他们没有做任何C样式转换无法完成我的意思的事情
style casts cannot do what I mean by

143
00:05:04,410 --> 00:05:05,710
那是确保他们可能会做其他事情，但实际
that is that sure they might do

144
00:05:05,910 --> 00:05:07,629
那是确保他们可能会做其他事情，但实际
additional things but the actual

145
00:05:07,829 --> 00:05:10,468
转换您得到的结果是成功的类型转换c-style
conversion the result of what you get is

146
00:05:10,668 --> 00:05:14,770
转换您得到的结果是成功的类型转换c-style
a successful type conversion c-style

147
00:05:14,970 --> 00:05:17,230
演员表可以实现所有这些，所以这并没有真正添加新功能
cast can achieve all of that so this

148
00:05:17,430 --> 00:05:18,550
演员表可以实现所有这些，所以这并没有真正添加新功能
isn't really adding new functionality

149
00:05:18,750 --> 00:05:21,278
它添加的就像是现在您的代码中的语法糖一样，当然是动态的
what it is adding is kind of like syntax

150
00:05:21,478 --> 00:05:24,278
它添加的就像是现在您的代码中的语法糖一样，当然是动态的
sugar to your code now of course dynamic

151
00:05:24,478 --> 00:05:26,079
我们将在另一个视频中具体讨论的演员表，我将向您展示
cast which we'll talk about specifically

152
00:05:26,279 --> 00:05:27,460
我们将在另一个视频中具体讨论的演员表，我将向您展示
in another video I'll show you an

153
00:05:27,660 --> 00:05:29,199
它在这里工作的示例，但我们将更深入地讨论它的作用
example of it working here but we'll

154
00:05:29,399 --> 00:05:30,759
它在这里工作的示例，但我们将更深入地讨论它的作用
talk more in depth about what it

155
00:05:30,959 --> 00:05:31,838
实际如何运作以及如何在另一个视频中运作，例如
actually does and how it works in

156
00:05:32,038 --> 00:05:33,968
实际如何运作以及如何在另一个视频中运作，例如
another video that for example will

157
00:05:34,168 --> 00:05:36,009
实际执行检查，如果转换失败，则可能返回null
actually perform a check and may return

158
00:05:36,209 --> 00:05:38,949
实际执行检查，如果转换失败，则可能返回null
null if the conversion wasn't successful

159
00:05:39,149 --> 00:05:41,319
所以它确实做了很多额外的事情，这也意味着它实际上会减慢您的速度
so it does do extra things which also

160
00:05:41,519 --> 00:05:42,670
所以它确实做了很多额外的事情，这也意味着它实际上会减慢您的速度
means that it actually kind of slows you

161
00:05:42,870 --> 00:05:45,338
但是大多数情况下，所有这些河马老板风格的演员都不会
down but for the for the most part all

162
00:05:45,538 --> 00:05:47,079
但是大多数情况下，所有这些河马老板风格的演员都不会
of these hippos boss style casts don't

163
00:05:47,279 --> 00:05:50,319
做一些额外的事情，只是将它们放入英语单词中
do anything really extra they just kind

164
00:05:50,519 --> 00:05:53,278
做一些额外的事情，只是将它们放入英语单词中
of are a way to put into English words

165
00:05:53,478 --> 00:05:55,870
静态演员，例如，这是一个静态演员，他们还做了其他
static cast for example that it's a

166
00:05:56,069 --> 00:05:58,718
静态演员，例如，这是一个静态演员，他们还做了其他
static cast and they also do some other

167
00:05:58,918 --> 00:06:00,250
在进行静态强制转换的情况下进行编译时检查，以查看是否进行了转换
compile time checks in the case of

168
00:06:00,449 --> 00:06:01,870
在进行静态强制转换的情况下进行编译时检查，以查看是否进行了转换
static cast to see if that conversions

169
00:06:02,069 --> 00:06:03,670
实际上有可能并且重新解释演员表是一种
actually at all ever possible and

170
00:06:03,870 --> 00:06:05,860
实际上有可能并且重新解释演员表是一种
reinterpret cast as well is kind of

171
00:06:06,060 --> 00:06:07,480
把我们讨论过的整个Pawnee类型写成文字
putting into words the whole type Pawnee

172
00:06:07,680 --> 00:06:09,100
把我们讨论过的整个Pawnee类型写成文字
thing that we talked about it's just a

173
00:06:09,300 --> 00:06:10,180
像重新解释演员这样用词表达的方式我正在重新解释
way to put it into words like

174
00:06:10,379 --> 00:06:11,860
像重新解释演员这样用词表达的方式我正在重新解释
reinterpret cast I'm reinterpreting this

175
00:06:12,060 --> 00:06:14,170
内存作为Const的其他内容我要消除成本，我要添加Const
memory as something else Const cast I'm

176
00:06:14,370 --> 00:06:16,389
内存作为Const的其他内容我要消除成本，我要添加Const
removing cost I'm adding Const the

177
00:06:16,589 --> 00:06:18,730
受益于除这些编译时间检查之外的所有这些成本
benefit to having all these costs apart

178
00:06:18,930 --> 00:06:20,410
受益于除这些编译时间检查之外的所有这些成本
from those kind of compile time checks

179
00:06:20,610 --> 00:06:22,600
您可能会收到的是，现在您可以在代码库中全部搜索它们
that you might receive is you can now

180
00:06:22,800 --> 00:06:24,610
您可能会收到的是，现在您可以在代码库中全部搜索它们
search for them in your code base all

181
00:06:24,810 --> 00:06:26,350
对，如果我想看看我在哪里穿我的铸件，也许是我有表现
right if I want to see where I wear my

182
00:06:26,550 --> 00:06:28,829
对，如果我想看看我在哪里穿我的铸件，也许是我有表现
casting is maybe I having performance

183
00:06:29,029 --> 00:06:30,569
鞋子，我不想动态投放某些东西，我可以搜索动态投放
shoes and I want to not dynamic cast

184
00:06:30,769 --> 00:06:32,670
鞋子，我不想动态投放某些东西，我可以搜索动态投放
something I can search for dynamic cast

185
00:06:32,870 --> 00:06:35,160
如果您只是像sa self。这样的演员，我可以搜索静态演员。
I can search for static cast if you just

186
00:06:35,360 --> 00:06:37,680
如果您只是像sa self。这样的演员，我可以搜索静态演员。
have a cast like sa self.cards like int

187
00:06:37,879 --> 00:06:39,150
或者像我们在前面的示例中所做的一样，您将如何
or whatever like we did with that

188
00:06:39,350 --> 00:06:41,430
或者像我们在前面的示例中所做的一样，您将如何
previous example how you're going to

189
00:06:41,629 --> 00:06:43,319
搜索您无法真正搜索到的内容
search for that you can't really search

190
00:06:43,519 --> 00:06:44,670
搜索您无法真正搜索到的内容
for it's not something that you can of

191
00:06:44,870 --> 00:06:46,139
当然，您可以使用正则表达式之类的东西，实际上我只是
course you could use like a regex or

192
00:06:46,339 --> 00:06:47,939
当然，您可以使用正则表达式之类的东西，实际上我只是
something like that and actually just I

193
00:06:48,139 --> 00:06:49,829
不知道这样做只是不实际，而在这里
don't know it just it wouldn't actually

194
00:06:50,029 --> 00:06:51,990
不知道这样做只是不实际，而在这里
be practical to do that whereas here

195
00:06:52,189 --> 00:06:53,910
我们有非常容易搜索的实际英语单词，因此对您有所帮助
we've got actual English words that are

196
00:06:54,110 --> 00:06:56,009
我们有非常容易搜索的实际英语单词，因此对您有所帮助
very easy to search for so it just helps

197
00:06:56,209 --> 00:06:58,199
作为程序员，我们既阅读代码又编写代码，但这也有帮助
us both as a programmer reading our code

198
00:06:58,399 --> 00:07:00,900
作为程序员，我们既阅读代码又编写代码，但这也有帮助
and writing our code but also it helps

199
00:07:01,100 --> 00:07:02,310
我们可以减少如果我们尝试并
us to reduce kind of errors that we

200
00:07:02,509 --> 00:07:04,079
我们可以减少如果我们尝试并
might accidentally make if we try and

201
00:07:04,279 --> 00:07:05,759
投放某些不兼容的东西很好的例子是我们有一个
cast certain things that are

202
00:07:05,959 --> 00:07:08,490
投放某些不兼容的东西很好的例子是我们有一个
incompatible good example is we've got a

203
00:07:08,689 --> 00:07:10,650
如果我只是尝试和花费，这里的几个班级的银行会在另一个班级中派生
few classes here banks derived in

204
00:07:10,850 --> 00:07:13,379
如果我只是尝试和花费，这里的几个班级的银行会在另一个班级中派生
another class if I just try and cost

205
00:07:13,579 --> 00:07:15,840
这个值显然是一个整数，就像我不知道另一个
this value which is clearly an int into

206
00:07:16,040 --> 00:07:18,060
这个值显然是一个整数，就像我不知道另一个
like I don't know another class that's

207
00:07:18,259 --> 00:07:21,449
会导致错误，因为在这种情况下，它将是
going to result in an error because well

208
00:07:21,649 --> 00:07:22,860
会导致错误，因为在这种情况下，它将是
it came in this case it's going to be

209
00:07:23,060 --> 00:07:24,840
因为有一个构造函数，但是如果我添加一个指针或类似的东西
because of a constructor but if I add

210
00:07:25,040 --> 00:07:26,189
因为有一个构造函数，但是如果我添加一个指针或类似的东西
like a pointer or something like that

211
00:07:26,389 --> 00:07:28,530
现在到这种情况，您可以看到这是无效的类型转换，那么简单
now to this case you can see it's an

212
00:07:28,730 --> 00:07:30,360
现在到这种情况，您可以看到这是无效的类型转换，那么简单
invalid type conversion so what simple

213
00:07:30,560 --> 00:07:31,470
类所做的就是编译器已经查看过了，就像那样
class has done is the compiler has

214
00:07:31,670 --> 00:07:32,970
类所做的就是编译器已经查看过了，就像那样
looked at that and been like that's

215
00:07:33,170 --> 00:07:35,850
永远不会正确工作，即使我们确实使用了正确的内存地址
never gonna work right and even if we do

216
00:07:36,050 --> 00:07:38,730
永远不会正确工作，即使我们确实使用了正确的内存地址
take the memory address of it right

217
00:07:38,930 --> 00:07:40,020
当然，这给了我们一个内部指针，我们尝试在其上键入
which of course gives us an inner

218
00:07:40,220 --> 00:07:42,120
当然，这给了我们一个内部指针，我们尝试在其上键入
pointer and we try and type on it that's

219
00:07:42,319 --> 00:07:43,710
不能工作，因为我只是现在对类型拥有无效的类型转换
not gonna work it's invalid type

220
00:07:43,910 --> 00:07:46,590
不能工作，因为我只是现在对类型拥有无效的类型转换
conversion now for type owning as I just

221
00:07:46,790 --> 00:07:47,759
说明，我们将需要使用重新解释演员表，如果可以的话，您可以
explained we would need to use

222
00:07:47,959 --> 00:07:49,379
说明，我们将需要使用重新解释演员表，如果可以的话，您可以
reinterpret cast and if we do you can

223
00:07:49,579 --> 00:07:51,120
看到我们没有任何错误，他们现在已经重新解释了当时的数据
see we don't get any errors who've now

224
00:07:51,319 --> 00:07:53,879
看到我们没有任何错误，他们现在已经重新解释了当时的数据
reinterpreted the data that is at that

225
00:07:54,079 --> 00:07:56,160
值指针成为另一个类实例的指针，但要点是
value pointer into being a pointer to

226
00:07:56,360 --> 00:07:59,579
值指针成为另一个类实例的指针，但要点是
another class instance but the point is

227
00:07:59,779 --> 00:08:01,740
与这样的事情，它增加了实际的编译时检查，因为
with with things like this it adds

228
00:08:01,939 --> 00:08:03,689
与这样的事情，它增加了实际的编译时检查，因为
actual compile time checking because it

229
00:08:03,889 --> 00:08:05,009
知道我们无法进行某些转换，而这不会是
knows that we can't do certain

230
00:08:05,209 --> 00:08:07,829
知道我们无法进行某些转换，而这不会是
conversions whereas this wouldn't be the

231
00:08:08,029 --> 00:08:10,230
当然，如果我们只是用一个普通的说明星来表演的话
case of course if we just used a normal

232
00:08:10,430 --> 00:08:11,699
当然，如果我们只是用一个普通的说明星来表演的话
say star cast it were just kind of

233
00:08:11,899 --> 00:08:13,980
默认情况下会执行Shepherd Cast的范围，我说很多
default to doing what a range Shepherd

234
00:08:14,180 --> 00:08:15,810
默认情况下会执行Shepherd Cast的范围，我说很多
cast would do and I'm saying a lot of

235
00:08:16,009 --> 00:08:17,790
文字，可能很难遵循，但再次是最好的方法
words and it might be a little bit hard

236
00:08:17,990 --> 00:08:21,360
文字，可能很难遵循，但再次是最好的方法
to follow but again the best way to to

237
00:08:21,560 --> 00:08:23,280
实际学习这些东西只是一种尝试，可以尝试建立自己的
actually learn this stuff is just a

238
00:08:23,480 --> 00:08:27,360
实际学习这些东西只是一种尝试，可以尝试建立自己的
practice it try and build yourself an

239
00:08:27,560 --> 00:08:29,910
使用静态强制转换或解释强制转换的示例
example of using static cast or

240
00:08:30,110 --> 00:08:32,759
使用静态强制转换或解释强制转换的示例
interpret cast a Const cast a dynamic

241
00:08:32,960 --> 00:08:34,120
我会在任何时候向您展示一个动态的汽车模型示例，但只是尝试
cast I'll show you a dynamic car

242
00:08:34,320 --> 00:08:35,889
我会在任何时候向您展示一个动态的汽车模型示例，但只是尝试
cast example any minute but just trying

243
00:08:36,090 --> 00:08:39,129
让自己变得更好，然后这是弄清楚实际情况的最好方法
to get yourself and then that's the best

244
00:08:39,330 --> 00:08:40,539
让自己变得更好，然后这是弄清楚实际情况的最好方法
way to kind of work out how it actually

245
00:08:40,740 --> 00:08:43,629
以及何时可以使用每一个，所以如果我们看一下动态演员表，
works and when you can use each one so

246
00:08:43,830 --> 00:08:46,509
以及何时可以使用每一个，所以如果我们看一下动态演员表，
if we take a look at dynamic cast what

247
00:08:46,710 --> 00:08:49,120
动态类实际上会做的是假设我们也许已经做了一个
dynamic class will actually do is

248
00:08:49,320 --> 00:08:50,620
动态类实际上会做的是假设我们也许已经做了一个
suppose that we had maybe we made a

249
00:08:50,820 --> 00:08:52,929
派生类实例的权利，所以我说派生派生等于新派生和
derived class instance right so I said

250
00:08:53,129 --> 00:08:55,120
派生类实例的权利，所以我说派生派生等于新派生和
derived derived equals new derived and

251
00:08:55,320 --> 00:08:57,039
然后沿着这条线的某个地方，我决定将其实际投射到基本权利中
then somewhere along the line I decided

252
00:08:57,240 --> 00:09:00,789
然后沿着这条线的某个地方，我决定将其实际投射到基本权利中
to actually cast that into a base right

253
00:09:00,990 --> 00:09:04,539
所以现在得出一个基数等于我想稍后知道的好吗
so a base base equals derived now what I

254
00:09:04,740 --> 00:09:06,969
所以现在得出一个基数等于我想稍后知道的好吗
want to do later is figure out ok hey I

255
00:09:07,169 --> 00:09:09,909
有一个基本指针是它实际上是派生实例还是在另一个实例中
have a base pointer is it actually a

256
00:09:10,110 --> 00:09:12,370
有一个基本指针是它实际上是派生实例还是在另一个实例中
derived instance or is it in another

257
00:09:12,570 --> 00:09:14,019
类，因为我们可以说如果我尝试使用它们，它们现在都可以扩展基础
classes since we can say they both

258
00:09:14,220 --> 00:09:18,099
类，因为我们可以说如果我尝试使用它们，它们现在都可以扩展基础
extend base now if I try and use it what

259
00:09:18,299 --> 00:09:19,839
我可以使用动态类型转换进行操作，实际上不仅是在问这个问题，而且
I can do with dynamic cast is actually

260
00:09:20,039 --> 00:09:21,939
我可以使用动态类型转换进行操作，实际上不仅是在问这个问题，而且
kind of not only asked that question but

261
00:09:22,139 --> 00:09:23,829
尝试进行转换，如果转换失败，请执行一些操作，以便我们了解事实
attempt that conversion and do something

262
00:09:24,029 --> 00:09:26,500
尝试进行转换，如果转换失败，请执行一些操作，以便我们了解事实
if it fails so we know for a fact based

263
00:09:26,700 --> 00:09:28,599
根据这段代码，实际上是派生类的实例，但让我们
on this code that base is actually an

264
00:09:28,799 --> 00:09:31,120
根据这段代码，实际上是派生类的实例，但让我们
instance of the derived class but let's

265
00:09:31,320 --> 00:09:32,679
假装我们不知道，我们只说另一个等级AC等于
pretend that we didn't know that and

266
00:09:32,879 --> 00:09:35,969
假装我们不知道，我们只说另一个等级AC等于
we'll just say another class AC equals

267
00:09:36,169 --> 00:09:42,639
动态转换另一个类，然后AC向右和一个指针，当然还可以
dynamic cast another class and then AC

268
00:09:42,840 --> 00:09:45,939
动态转换另一个类，然后AC向右和一个指针，当然还可以
right and one pointer and of course ok

269
00:09:46,139 --> 00:09:48,039
现在，如果我们刚刚使用了静态演员表，
now what's gonna happen here is if we

270
00:09:48,240 --> 00:09:50,769
现在，如果我们刚刚使用了静态演员表，
had just used a static cast which by the

271
00:09:50,970 --> 00:09:53,259
在这种情况下，我们可以看到我们可以做的事情与安全星级演员所做的事情一样
way we can see we can do in this case it

272
00:09:53,460 --> 00:09:55,329
在这种情况下，我们可以看到我们可以做的事情与安全星级演员所做的事情一样
does the same thing as a safe star cast

273
00:09:55,529 --> 00:09:57,339
或者，如果我们使用这种星型铸造，它会像不是那样工作
or if we use this a star cast it would

274
00:09:57,539 --> 00:09:59,139
或者，如果我们使用这种星型铸造，它会像不是那样工作
just work like isn't it would just give

275
00:09:59,340 --> 00:10:01,509
我们认为价值，当然事后可能会出错，因为您知道AC是
us that value and of course things might

276
00:10:01,710 --> 00:10:04,299
我们认为价值，当然事后可能会出错，因为您知道AC是
go wrong later on because you know AC is

277
00:10:04,500 --> 00:10:06,309
实际上不是另一个类，它实际上是我们基本上已经衍生的实例
not in fact another class it's actually

278
00:10:06,509 --> 00:10:08,500
实际上不是另一个类，它实际上是我们基本上已经衍生的实例
a derived instance we've just basically

279
00:10:08,700 --> 00:10:11,109
键入它，但是使用动态强制转换，它实际上要做的是
type ones it but with dynamic cast what

280
00:10:11,309 --> 00:10:12,759
键入它，但是使用动态强制转换，它实际上要做的是
it's actually going to do is it's going

281
00:10:12,960 --> 00:10:16,089
看看是否确实如此，当然在这里，我需要确保
to see if that is actually the case and

282
00:10:16,289 --> 00:10:18,099
看看是否确实如此，当然在这里，我需要确保
of course over here I need to make sure

283
00:10:18,299 --> 00:10:21,459
我通过基准而不是交流，让我们按一下五个，然后看看会发生什么，以便您可以看到
I pass in base and not AC let's hit that

284
00:10:21,659 --> 00:10:24,069
我通过基准而不是交流，让我们按一下五个，然后看看会发生什么，以便您可以看到
five and see what happens so you can see

285
00:10:24,269 --> 00:10:26,229
一旦我真的这样做，如果我按f10，AC等于null，因为实际上
once I actually do that if I hit f10 AC

286
00:10:26,429 --> 00:10:28,029
一旦我真的这样做，如果我按f10，AC等于null，因为实际上
is equal to null because it is in fact

287
00:10:28,230 --> 00:10:29,589
还不错，您当然可以检查一下，然后说如果不是AC或
not bad and of course you could then

288
00:10:29,789 --> 00:10:31,959
还不错，您当然可以检查一下，然后说如果不是AC或
check it you could say if not AC or if

289
00:10:32,159 --> 00:10:34,569
IC等于null，那么也许我们知道这不是该类或一种检查方法
IC equals null then maybe we know that

290
00:10:34,769 --> 00:10:36,519
IC等于null，那么也许我们知道这不是该类或一种检查方法
it's not that class or a way to check

291
00:10:36,720 --> 00:10:38,379
因为如果是该类，则仅表示AC是否表示
for if it is that class is just to say

292
00:10:38,580 --> 00:10:41,169
因为如果是该类，则仅表示AC是否表示
if AC that means that the type of

293
00:10:41,370 --> 00:10:42,699
转换成功，我们现在知道
conversion was successful and we now

294
00:10:42,899 --> 00:10:43,099
转换成功，我们现在知道
know

295
00:10:43,299 --> 00:10:45,740
低音实际上是一个实例，派生的权利实例对不起
bass is in fact an instance and instance

296
00:10:45,940 --> 00:10:49,250
低音实际上是一个实例，派生的权利实例对不起
of derived right sorry an instance of

297
00:10:49,450 --> 00:10:52,669
另一个班级，当然，如果我确实改变了这一点，那么它可以通过
another class and of course if I do

298
00:10:52,870 --> 00:10:54,198
另一个班级，当然，如果我确实改变了这一点，那么它可以通过
change this around so it works by

299
00:10:54,399 --> 00:10:57,679
将其设置为等于5的派生命中率，然后您可以查看我是否达到10
setting it equal to derived hit at five

300
00:10:57,879 --> 00:10:59,419
将其设置为等于5的派生命中率，然后您可以查看我是否达到10
and then you can see if I hit up ten

301
00:10:59,620 --> 00:11:01,370
在这里它实际上也对指针有效，因为该转换是
here it does in fact you also valid

302
00:11:01,570 --> 00:11:03,078
在这里它实际上也对指针有效，因为该转换是
pointer because that conversion was

303
00:11:03,278 --> 00:11:05,209
成功，因此动态投放是查看其是否真的有效的好方法
successful so dynamic cast is a great

304
00:11:05,409 --> 00:11:07,370
成功，因此动态投放是查看其是否真的有效的好方法
way to see if it's actually worked now

305
00:11:07,570 --> 00:11:09,139
这确实与运行时类型信息RT TI紧密相关，并且
this does tie in very closely to the

306
00:11:09,339 --> 00:11:11,719
这确实与运行时类型信息RT TI紧密相关，并且
runtime type information RT TI and it

307
00:11:11,919 --> 00:11:13,370
需要一堆东西，我们将在
requires requires a whole bunch of

308
00:11:13,570 --> 00:11:14,419
需要一堆东西，我们将在
things which we'll talk about in a

309
00:11:14,620 --> 00:11:17,089
有关动态铸造的特定视频，但您知道这些铸造操作员
specific video about dynamic casting but

310
00:11:17,289 --> 00:11:18,799
有关动态铸造的特定视频，但您知道这些铸造操作员
just so you know these casting operators

311
00:11:19,000 --> 00:11:20,689
可以简化铸造过程，甚至可以使铸造过程变得可行
are kind of available to you as a way to

312
00:11:20,889 --> 00:11:23,809
可以简化铸造过程，甚至可以使铸造过程变得可行
simplify casting and to maybe make it

313
00:11:24,009 --> 00:11:26,120
从某种意义上讲，它会更可靠，因为它会在编译时检查动态类型转换
more solid in the sense that it will do

314
00:11:26,320 --> 00:11:28,219
从某种意义上讲，它会更可靠，因为它会在编译时检查动态类型转换
compile time checking dynamic cast will

315
00:11:28,419 --> 00:11:31,219
做运行时检查，您将获得所有可能更可靠的代码
do runtime checking and you'll get all

316
00:11:31,419 --> 00:11:32,899
做运行时检查，您将获得所有可能更可靠的代码
that kind of hopefully more solid code

317
00:11:33,100 --> 00:11:35,539
通过使用像我这样的强制转换运算符，我倾向于使用C风格
base by using casting operators like

318
00:11:35,740 --> 00:11:37,878
通过使用像我这样的强制转换运算符，我倾向于使用C风格
that me personally I tend to use C style

319
00:11:38,078 --> 00:11:40,549
大部分时间都投放广告，但我鼓励您使用品牌
casts most of the time but I do

320
00:11:40,750 --> 00:11:42,198
大部分时间都投放广告，但我鼓励您使用品牌
encourage if you're working on a brand

321
00:11:42,399 --> 00:11:43,729
您是从头开始的新项目，或者您的项目很小
new project that you're starting from

322
00:11:43,929 --> 00:11:45,378
您是从头开始的新项目，或者您的项目很小
scratch or if you have a very small

323
00:11:45,578 --> 00:11:47,448
项目，您应该使用海盐对不起C ++开始像静态转换一样开始转换
project you should use sea salt say

324
00:11:47,649 --> 00:11:49,878
项目，您应该使用海盐对不起C ++开始像静态转换一样开始转换
sorry C++ start casting like static cast

325
00:11:50,078 --> 00:11:52,069
动态重新解释演员阵容，因为那些演员
dynamic re interpret cast all of that

326
00:11:52,269 --> 00:11:56,139
动态重新解释演员阵容，因为那些演员
kind of stuff because those those casts

327
00:11:56,339 --> 00:11:58,969
确实使您的代码更可靠，并且对真正涉及的每个人和
do make your code more solid and are

328
00:11:59,169 --> 00:12:02,539
确实使您的代码更可靠，并且对真正涉及的每个人和
better for everyone involved really and

329
00:12:02,740 --> 00:12:04,789
缺点车并没有真正接触到，但实际上只是您使用它来添加
cons car didn't really touch on but

330
00:12:04,990 --> 00:12:06,378
缺点车并没有真正接触到，但实际上只是您使用它来添加
that's just literally you use it to add

331
00:12:06,578 --> 00:12:09,319
或将Const移除为简单的内容，无论如何都可以不断添加，但这是
or remove Const to something easy you

332
00:12:09,519 --> 00:12:11,149
或将Const移除为简单的内容，无论如何都可以不断添加，但这是
can add constantly anyway but it's

333
00:12:11,350 --> 00:12:13,639
主要是为了删除Const，最好在代码库中添加它
mostly for removing Const and again it's

334
00:12:13,839 --> 00:12:14,750
主要是为了删除Const，最好在代码库中添加它
good to just have that in your code base

335
00:12:14,950 --> 00:12:16,250
因为现在您可以搜索所有已决定删除的顽皮时间
because now you can search for all the

336
00:12:16,450 --> 00:12:17,990
因为现在您可以搜索所有已决定删除的顽皮时间
naughty times you've decided to remove

337
00:12:18,190 --> 00:12:20,599
const，也许可以解决这个问题，或者也可以提出其他建议
Const and maybe fix that or just come up

338
00:12:20,799 --> 00:12:22,279
const，也许可以解决这个问题，或者也可以提出其他建议
with something else as well

339
00:12:22,480 --> 00:12:23,689
所以这真的很有用，然后另一件事是重新解释成本，如果您
so that's really useful and then the

340
00:12:23,889 --> 00:12:25,578
所以这真的很有用，然后另一件事是重新解释成本，如果您
other thing was reinterpret cost if you

341
00:12:25,778 --> 00:12:26,870
看一下我在本文开头链接的那种狩猎视频
look at that type hunting video that

342
00:12:27,070 --> 00:12:27,769
看一下我在本文开头链接的那种狩猎视频
I've linked at the beginning of this

343
00:12:27,970 --> 00:12:31,248
视频我在那里所做的一切，基本上可以使用非常昂贵的Tober费用，
video everything I did there you can

344
00:12:31,448 --> 00:12:32,599
视频我在那里所做的一切，基本上可以使用非常昂贵的Tober费用，
basically use very Tober cost so that

345
00:12:32,799 --> 00:12:34,159
这就是重新解释类型转换的原因，因为当我实际上不想要
kind of that's what reinterpret cast is

346
00:12:34,360 --> 00:12:36,078
这就是重新解释类型转换的原因，因为当我实际上不想要
used for it's when I don't actually want

347
00:12:36,278 --> 00:12:37,849
转换任何我只想采用的指针并对其进行解释的东西
to convert anything I just want to take

348
00:12:38,049 --> 00:12:39,929
转换任何我只想采用的指针并对其进行解释的东西
that kind of pointer and interpret it

349
00:12:40,129 --> 00:12:41,129
我想将现有内存解释为另一种
something else I want to interpret

350
00:12:41,330 --> 00:12:43,649
我想将现有内存解释为另一种
existing memory as another type that's

351
00:12:43,850 --> 00:12:45,059
无论如何，我们的解释种姓很有用，我希望你们喜欢
what our interpret caste is useful

352
00:12:45,259 --> 00:12:46,349
无论如何，我们的解释种姓很有用，我希望你们喜欢
anyway I hope you guys enjoyed this

353
00:12:46,549 --> 00:12:47,669
视频，如果您没有听到类似的按钮，则可以帮助支持本系列
video if you didn't hear that like

354
00:12:47,870 --> 00:12:49,379
视频，如果您没有听到类似的按钮，则可以帮助支持本系列
button you can help support this series

355
00:12:49,580 --> 00:12:50,729
在转弯之后，去陪同patreon的同伙，向所有人展示
by going to patreon accomplice after the

356
00:12:50,929 --> 00:12:52,859
在转弯之后，去陪同patreon的同伙，向所有人展示
turn okay showed out to everyone who

357
00:12:53,059 --> 00:12:54,479
支持该系列，因为没有你们，它就不会在这里了
supports this series because it would

358
00:12:54,679 --> 00:12:56,309
支持该系列，因为没有你们，它就不会在这里了
not be here without you guys and it

359
00:12:56,509 --> 00:12:58,529
我不会一直这样做，这肯定不会成为正在进行的事情
certainly wouldn't be wouldn't be an

360
00:12:58,730 --> 00:13:00,719
我不会一直这样做，这肯定不会成为正在进行的事情
ongoing thing that I keep keep doing so

361
00:13:00,919 --> 00:13:02,339
是的，谢谢，因为我喜欢为你们制作视频，下个再见
yeah thank you because I love making

362
00:13:02,539 --> 00:13:05,639
是的，谢谢，因为我喜欢为你们制作视频，下个再见
videos for you guys I will see you next

363
00:13:05,840 --> 00:13:07,859
时间再见[音乐]
time good bye

364
00:13:08,059 --> 00:13:13,059
时间再见[音乐]
[Music]

