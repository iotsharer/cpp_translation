﻿1
00:00:00,030 --> 00:00:03,549
hello guys my name is a Chennai welcome back to my say plus plus series today
大家好，我叫Chennai，今天回到我的发言加系列会议

2
00:00:03,750 --> 00:00:06,759
we're going to be talking all about a keyword called mutable and how we can
我们将要讨论的是一个叫做mutable的关键字，以及如何

3
00:00:06,960 --> 00:00:11,589
use it so mutable actually has two fairly different uses one of them is to
使用它，所以可变实际上有两个相当不同的用途，其中之一是

4
00:00:11,789 --> 00:00:16,240
do with Const if you don't know how constants work in C++ you can check out
如果您不知道常量在C ++中的工作方式，可以使用Const进行检查

5
00:00:16,440 --> 00:00:20,260
the video that I made yesterday about that just click on the card or the link
我昨天制作的有关该视频的视频，只需单击卡片或链接

6
00:00:20,460 --> 00:00:23,530
in the description below and the other use is actually with lambdas so we'll
在下面的说明中，其他用途实际上是与lambdas一起使用的，因此我们将

7
00:00:23,730 --> 00:00:26,560
kind of cover both of them I know I haven't actually talked about lambdas
我都没真正谈论过lambdas 

8
00:00:26,760 --> 00:00:29,409
and what they even are a few of you're probably wondering what I'm talking
甚至你们当中的一些人都想知道我在说什么

9
00:00:29,609 --> 00:00:33,549
about but definitely will in the future but I thought just make a quick video to
关于但肯定会在将来，但我想只是快速制作一个视频

10
00:00:33,750 --> 00:00:36,939
cover what mutable actually means because the two cases are actually
涵盖可变的实际含义，因为这两种情况实际上

11
00:00:37,140 --> 00:00:41,858
rather different the word the English word mutable of course means that it's
英语单词mutable的意思当然是不同的， 

12
00:00:42,058 --> 00:00:45,159
something that is liable to change it's something that can change and you've
容易改变的东西可以改变的东西

13
00:00:45,359 --> 00:00:48,939
probably heard me say immutable before meaning that something cannot be changed
可能听过我说过一成不变，这意味着无法更改某些内容

14
00:00:49,140 --> 00:00:53,169
mutable as the opposite of that means something can change so when we talk
可变的，因为相反的意思意味着某些东西可以改变，所以当我们说话时

15
00:00:53,369 --> 00:00:57,849
about mutable in the context of Const obviously we're talking about something
关于在Const上下文中的可变性，显然，我们在谈论一些东西

16
00:00:58,049 --> 00:01:02,408
that is kind of Const but actually can change so it's almost like mutable
这是一种const，但实际上可以改变，所以几乎就像可变的

17
00:01:02,609 --> 00:01:06,488
reverses the meaning of Const we really only have one application of mutable in
颠倒了Const的含义，我们实际上只有一个可变的应用

18
00:01:06,688 --> 00:01:09,879
that context and that is with class methods so let's take a look at that I'm
该上下文以及类方法，所以让我们看一下

19
00:01:10,079 --> 00:01:13,269
going to create a class here called entity I'm going to give it one private
在这里创建一个称为实体的类，我将给它一个私有的

20
00:01:13,469 --> 00:01:17,829
member which is going to be in name I'm also going to write a getter for this
我将要为此命名的成员

21
00:01:18,030 --> 00:01:21,579
function so we'll have a Const STD string reference we'll call it get name
函数，所以我们将有一个Const STD字符串引用，我们将其称为获取名称

22
00:01:21,780 --> 00:01:25,058
it will good function will be marked as Const if you don't know what that means
如果您不知道这意味着什么，它将被标记为Const。 

23
00:01:25,259 --> 00:01:28,359
and check out the video that I made yesterday about constants a plus plus
并查看我昨天制作的有关常量的视频加号加号

24
00:01:28,560 --> 00:01:32,289
and this will just basically return our name okay so we have a fairly basic
这基本上会返回我们的名字，所以我们有一个相当基本的

25
00:01:32,489 --> 00:01:36,159
function here together I'm actually going to move this down over here the
功能一起在这里，我实际上是将其移至此处

26
00:01:36,359 --> 00:01:39,069
Const over here means that were not allowed to modify the actual class
此处的常量表示不允许修改实际的类

27
00:01:39,269 --> 00:01:42,488
members so I can't for example reassign name to be
成员，所以我不能例如将名称重新分配为

28
00:01:42,688 --> 00:01:46,299
something else and really this applies to every variable the primary reason for
其他的东西，实际上这适用于每个变量的主要原因

29
00:01:46,500 --> 00:01:49,778
making these methods Const in the first place and kind of promising that you're
使这些方法首先成为const，并希望您

30
00:01:49,978 --> 00:01:53,558
not going to touch the class is because if we actually do wind up having some
不去上课是因为如果我们真的完成了一些

31
00:01:53,759 --> 00:01:58,869
kind of Const entity object over here we would be able to call those cost methods
一种Const实体对象，我们可以调用这些成本方法

32
00:01:59,069 --> 00:02:02,469
whereas if they weren't marked as Const we actually wouldn't be able to do that
而如果没有将它们标记为Const，我们实际上将无法做到这一点

33
00:02:02,670 --> 00:02:04,840
so that's why we mark these methods as Const
这就是为什么我们将这些方法标记为Const的原因

34
00:02:05,040 --> 00:02:09,370
the first place however in some situations we kind of do still want to
但是，在某些情况下，我们仍然想保持第一

35
00:02:09,569 --> 00:02:13,360
market Methodist Const because for all intents and purposes it is constant it's
市场卫理公会，因为就所有意图和目的而言，它都是不变的

36
00:02:13,560 --> 00:02:18,250
not modifying the object they say but maybe just maybe it needs to just touch
不修改他们说的对象，但也许只是需要触摸

37
00:02:18,449 --> 00:02:23,020
that variable over there that's kind of in the entity class technically but not
从技术上讲，那是实体类中的那个变量，但不是

38
00:02:23,219 --> 00:02:27,250
really meant to kind of be there I'll give you an example let's just say that
真的意味着要在那里，我给你举个例子，让我们说

39
00:02:27,449 --> 00:02:31,420
for debugging purposes we wanted to count how many times this function was
出于调试目的，我们想计算此函数的执行次数

40
00:02:31,620 --> 00:02:35,800
called in our program we might have some kind of integer pulled debug count or
在我们的程序中调用我们可能会有某种整数提取调试计数或

41
00:02:36,000 --> 00:02:40,570
something which is initialized to zero and every time we call this function we
初始化为零的东西，每次我们调用此函数时， 

42
00:02:40,770 --> 00:02:45,009
just wanted to increment that count now we can't do that here so a solution
只是想增加计数，现在我们不能在这里做，所以一个解决方案

43
00:02:45,209 --> 00:02:49,870
would be to kind of get rid of Const and then we would be fine right but now we
是要摆脱Const，然后我们会好的，但是现在我们

44
00:02:50,069 --> 00:02:53,650
can't call it over here and we've kind of broken this and really this is just a
不能在这里打电话，我们已经打破了这个问题，实际上这只是一个

45
00:02:53,849 --> 00:02:59,680
getter for name so surely we can kind of just increment this without losing the
名称的获取者，所以我们当然可以在不损失

46
00:02:59,879 --> 00:03:03,460
constants of this method and of course we could move this out into some other
此方法的常量，当然我们可以将其移至其他

47
00:03:03,659 --> 00:03:06,219
class or something and that will be totally fine but that's going to be
上课之类的东西，那将是完全可以的，但这将是

48
00:03:06,419 --> 00:03:10,810
messy because this applies specifically to this functional through this class so
凌乱的，因为这通过此类专门适用于此功能，因此

49
00:03:11,009 --> 00:03:15,069
what we can do is restore our quantity R and just mark this variable debug count
我们能做的是恢复数量R并标记此变量调试计数

50
00:03:15,269 --> 00:03:19,390
as mutable I mean we're allowing constant methods to change it and now
易变，我的意思是我们允许使用常量方法来更改它，现在

51
00:03:19,590 --> 00:03:23,050
all was well in the world we can do this Constanta seed stuff here and we can
世界上一切都很好，我们可以在这里做这个康斯坦萨种子的事情，我们可以

52
00:03:23,250 --> 00:03:26,680
also have a nice constant method which actually does modify this particular
也有一个不错的常量方法，实际上确实修改了这个特殊的

53
00:03:26,879 --> 00:03:31,300
class member so marking a class member is mutable means that constants inside
类成员，因此标记类成员是可变的，这意味着内部的常量

54
00:03:31,500 --> 00:03:36,460
that class can actually modify that member that right there is probably the
该类实际上可以修改该成员， 

55
00:03:36,659 --> 00:03:41,860
most common usage of mutable by quite a quite a big margin using mutable with
 mutable的最常见用法相当多，使用mutable与

56
00:03:42,060 --> 00:03:46,480
class members like this is probably the only time you'll ever use it to be
像这样的班级成员可能是唯一一次使用它成为

57
00:03:46,680 --> 00:03:49,990
honest but there is however one more use for mutable and I might as well cover
老实说，但是可变的还有更多用途，我不妨介绍一下

58
00:03:50,189 --> 00:03:54,789
that today it's to do with lambdas so without making this too complicated
今天是和lambdas有关的，所以不必太复杂

59
00:03:54,989 --> 00:03:58,689
because I haven't covered lambdas the I suppose that we had some kind of
因为我没有涉及lambda，所以我想我们有某种

60
00:03:58,889 --> 00:04:02,259
bearable here we'll make in X equal Sarah and then I wanted to declare some
可以忍受的，我们用X等于Sarah，然后我想声明一些

61
00:04:02,459 --> 00:04:05,140
kind of so I'll just call it ass and I'll write
有点，所以我就称它为屁股，然后我写

62
00:04:05,340 --> 00:04:08,860
some code over here maybe do something like Prince how low to the console I
这里的一些代码可能会做一些事情，例如Prince我对控制台有多低

63
00:04:09,060 --> 00:04:11,710
don't know it could be really anything that's just a lambda a lambda is
不知道这真的可以是lambda 

64
00:04:11,909 --> 00:04:15,219
basically like a little throwaway function that you can write and assign
基本上就像一个小的一次性函数，您可以编写和分配

65
00:04:15,419 --> 00:04:18,579
to a variable quickly like we've done here we can call a lambda just like any
像我们在这里一样快速地将其转换为变量，我们可以像任何

66
00:04:18,779 --> 00:04:21,819
other function by using its name like this and just specifying any parameters
其他功能，使用这样的名称，并仅指定任何参数

67
00:04:22,019 --> 00:04:26,650
we might have now suppose that we actually wanted to use x over here like
我们现在可能已经假设我们实际上想在此处使用x 

68
00:04:26,850 --> 00:04:28,300
instead of printing color I want it to print
而不是打印颜色，我希望它打印

69
00:04:28,500 --> 00:04:31,780
X that's going to be fine I can just pass in X like this however I do need to
 X会很好的，我可以像这样传递X，但是我确实需要

70
00:04:31,980 --> 00:04:36,040
define some kind of capture method so we can either send this variable by
定义某种捕获方法，以便我们可以通过以下方式发送此变量

71
00:04:36,240 --> 00:04:40,000
reference like this or by value like that or you can just type in equals or
像这样的引用或像这样的值引用，或者您可以只输入等于或

72
00:04:40,199 --> 00:04:43,689
absent to send everything basically that's using here by reference or by
基本上没有发送所有通过引用或此处使用的内容

73
00:04:43,889 --> 00:04:47,139
value now suppose that this lambda actually did some extra stuff maybe did
现在的价值假设该lambda实际上做了一些额外的操作

74
00:04:47,339 --> 00:04:51,400
X plus plus however we still wanted this to be by value you'll see that if I
 X加号，但是我们仍然希望按值显示，如果我

75
00:04:51,600 --> 00:04:55,120
actually change this to be by a value and I try to do something like X plus
实际上将其更改为一个值，我尝试做类似X plus的操作

76
00:04:55,319 --> 00:04:59,560
plus I get an error and what I actually would have to do in this situation is
而且我得到一个错误，在这种情况下我实际上要做的是

77
00:04:59,759 --> 00:05:04,240
maybe make another variable assign it to this and then kind of essentially
也许让另一个变量分配给它，然后本质上

78
00:05:04,439 --> 00:05:08,379
incremental or modify that variable I have to copy it and I have to basically
增量或修改该变量，我必须将其复制，并且基本上

79
00:05:08,579 --> 00:05:12,100
create a local variable which can actually take the value from the
创建一个本地变量，该变量实际上可以从

80
00:05:12,300 --> 00:05:15,850
variable and passing in this is a little bit messy so what I could do instead is
变量并传入它有点混乱，所以我能做的是

81
00:05:16,050 --> 00:05:19,360
use that wonderful mutable keyword so I'll go back to doing this and all I
使用那个很棒的可变关键字，所以我会回头再做

82
00:05:19,560 --> 00:05:23,920
have to do is say that this lambda is mutable by just sticking that mutable
要做的就是说，只要把那个lambda贴上即可

83
00:05:24,120 --> 00:05:27,129
keyword right there what this means is that variables that you passed by a
关键字就在那里，这意味着您通过

84
00:05:27,329 --> 00:05:30,759
value like I just did with this X you can change them and of course what this
就像我刚刚使用X所做的值一样，您可以更改它们，当然，这是什么

85
00:05:30,959 --> 00:05:34,870
will do is basically what I showed you with that y example it's going to create
基本上将是我通过该示例向您展示的内容

86
00:05:35,069 --> 00:05:37,900
a local variable item it's just that in your source code it's going to look a
一个局部变量项，只是在您的源代码中它看起来像

87
00:05:38,100 --> 00:05:42,069
lot cleaner and of course outside of this function so like after I call this
更清洁，当然不在此功能范围之内，就像我称之为

88
00:05:42,269 --> 00:05:46,600
lambda over here X will still be set to eight it will not increment and be set
 X此处的lambda仍将设置为8，不会递增并设置

89
00:05:46,800 --> 00:05:49,600
to nine you are not passing it by reference now like you would if you did
到9，您现在不像以前那样通过引用传递它

90
00:05:49,800 --> 00:05:53,468
this you're still passing it by value so it still will be eight here because
您仍然按值传递它，所以这里仍然是8，因为

91
00:05:53,668 --> 00:05:57,460
you're just copying that value eight into this lambda alright that is what
您只是将那个值8复制到此lambda中，好吧

92
00:05:57,660 --> 00:06:01,000
the mutable keyword is again as I said earlier you'll be using it with classes
可变关键字再次如我之前所说，将在类中使用它

93
00:06:01,199 --> 00:06:06,040
and Const like 90% of the time I honestly do not think I've ever written
老实说，我有90％的时间都喜欢Const， 

94
00:06:06,240 --> 00:06:10,540
it inside a land like the before it just it just doesn't happen in
它像以前一样在土地内部，只是它不会发生在

95
00:06:10,740 --> 00:06:14,110
practice I haven't even seen code like this very often at all if you have any
实践中，我什至没有看过像这样的代码

96
00:06:14,310 --> 00:06:17,230
more questions about mutable or constants or anything you can leave a
有关可变或常数或您可以留下的任何内容的更多问题

97
00:06:17,430 --> 00:06:20,170
question in the comment section below I'll try to respond to as many as I can
以下评论部分中的问题，我将尽可能回答

98
00:06:20,370 --> 00:06:23,650
if you guys enjoy this video you can show me the e did by hitting that like
如果你们喜欢这部影片，您可以按一下

99
00:06:23,850 --> 00:06:26,920
button below and you can also support the series on patreon to make sure that
按钮，您还可以在patreon上支持该系列，以确保

100
00:06:27,120 --> 00:06:30,009
more episodes are made by going to the patreon account for sex Cherno and get
通过前往有关Cherno的顾客帐户获得更多剧集，并获得

101
00:06:30,209 --> 00:06:33,160
some cool rewards like seeing excess early and contributing to the planning
一些很酷的奖励，例如尽早发现多余的东西并为计划做出贡献

102
00:06:33,360 --> 00:06:37,210
and suggesting topics and all that fun stuff I'll see you guys in next video
并提出主题和所有有趣的内容，我将在下一个视频中看到大家

103
00:06:37,410 --> 00:06:44,610
goodbye [Music]
再见 

104
00:06:47,579 --> 00:06:52,579
[Music]
 [音乐] 

