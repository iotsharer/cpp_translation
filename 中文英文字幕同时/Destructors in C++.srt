﻿1
00:00:00,000 --> 00:00:03,099
Hey look guys my name is the Cherno and welcome back to my stay plus plus series
嗨，大家好，我叫Cherno，欢迎回到我的住宿加系列

2
00:00:03,299 --> 00:00:06,219
a lot of time we talked about constructors in C++ and what they are
很多时间，我们讨论了C ++中的构造函数及其含义

3
00:00:06,419 --> 00:00:09,640
and how to use them definitely check out that video if you haven't already and
以及如何使用它们（如果您还没有的话）一定要查看该视频

4
00:00:09,839 --> 00:00:12,880
today we're going to talk about that evil twin B destructor so it's the only
今天我们要谈论那个邪恶的双胞胎B破坏者，所以这是唯一的

5
00:00:13,080 --> 00:00:16,449
how a constructor runs when you create a new instance of an object a destructor
创建对象的新实例时，构造函数如何运行

6
00:00:16,649 --> 00:00:20,739
runs when you destroy an object so any time an object gets destroyed the
在销毁对象时运行，因此只要销毁对象， 

7
00:00:20,939 --> 00:00:24,579
destruction method will get called the constructor is usually we set variables
破坏方法会被称为构造函数，通常是我们设置变量

8
00:00:24,778 --> 00:00:27,909
up or do any kind of initialization that you need to do and similarly the
进行或执行您需要进行的任何类型的初始化，类似地， 

9
00:00:28,109 --> 00:00:31,720
destructor is where you uninitialized anything that you might have to or clean
析构函数是您未初始化或清理的所有内容的地方

10
00:00:31,920 --> 00:00:34,659
any memory that you've used the destructor applies to both stack and
您使用了析构函数的所有内存都适用于堆栈和

11
00:00:34,859 --> 00:00:38,409
heap allocated objects so if you allocate an object using new when you
堆分配的对象，所以如果您在使用新对象时分配对象

12
00:00:38,609 --> 00:00:41,649
call delete the destructor will get called if you just have a stack-based
如果您只是基于堆栈，则调用delete析构函数将被调用

13
00:00:41,850 --> 00:00:45,369
object and when the scope and the object will get deleted and thus the destructor
对象以及范围和对象何时被删除，从而析构函数

14
00:00:45,570 --> 00:00:48,640
will get called let's dive in and take a look at some examples so in the
将被称为让我们潜入并看一些例子，所以在

15
00:00:48,840 --> 00:00:51,489
constructor video we created this wonderful entity class which had
构造函数视频，我们创建了这个很棒的实体类

16
00:00:51,689 --> 00:00:55,088
multiple constructors here let's go ahead and add a destructor and these
这里有多个构造函数，让我们继续添加一个析构函数，这些

17
00:00:55,289 --> 00:00:58,658
structure begins with a tilde and then the name of our class so basically the
结构以波浪号开头，然后是类的名称，因此基本上

18
00:00:58,859 --> 00:01:01,748
only difference between an constructor and destructor in terms of declaration
就声明而言，构造函数和析构函数之间的唯一区别

19
00:01:01,948 --> 00:01:06,730
and definition is the actual tilde that you put out the front of the destructor
定义是您在析构函数前面放的实际波浪号

20
00:01:06,930 --> 00:01:10,060
now you know at the destructor in this case we just have a single class with
现在您知道在这种情况下的析构函数中，我们只有一个类

21
00:01:10,260 --> 00:01:14,259
two members x and y there is absolutely no cleaner when you do some memories
两个成员x和y当您进行一些记忆时，绝对没有清洁剂

22
00:01:14,459 --> 00:01:18,849
here at all since these two floats are allocated in place where our class will
因为这两个浮动元素分配在我们班级将要放置的地方

23
00:01:19,049 --> 00:01:22,569
talk more about memory allocations and all of our complicated stuff in the
在中进一步讨论内存分配和我们所有复杂的内容

24
00:01:22,769 --> 00:01:26,200
future so don't you worry but for example let's go ahead and add a message
未来，所以您不用担心，但是例如，让我们继续并添加一条消息

25
00:01:26,400 --> 00:01:29,528
which tells us that the object has been deleted so I might write something like
告诉我们该对象已被删除，所以我可能会写类似

26
00:01:29,728 --> 00:01:36,390
destroyed empty and that's a fine in our constructor here I'm going to write
销毁为空，这在我们的构造函数中很好，我要写

27
00:01:36,590 --> 00:01:42,340
constructor and T or I guess we should call it created since the cult is
构造函数和T，或者我想我们应该将其称为“创建”，因为

28
00:01:42,540 --> 00:01:45,819
destroyed and not disrupted I want to get rid of is constructed it's okay to
销毁，而不是打乱我想摆脱的是建造它可以

29
00:01:46,019 --> 00:01:50,918
confused I'll initialize X and Y is 0 as well just so we're not crazy I'll get
困惑，我也将X和Y都初始化为0，所以我们不会发疯

30
00:01:51,118 --> 00:01:54,759
rid of this as well now since this is duck allocated we will
现在也要摆脱它，因为这是鸭子分配，我们将

31
00:01:54,959 --> 00:01:58,599
only see the destructor getting called when the main function exits which we're
仅在主函数退出时看到析构函数被调用

32
00:01:58,799 --> 00:02:01,209
not really going to see because our program will close immediately after
不会真正看到，因为我们的程序将在之后立即关闭

33
00:02:01,409 --> 00:02:04,778
that so I'm going to just write a function here which will perform all of
所以我要在这里写一个函数来执行所有

34
00:02:04,978 --> 00:02:10,000
our entity operations I'll move all this code over here and I will call function
我们的实体操作，我将所有这些代码移到这里，然后我将调用函数

35
00:02:10,199 --> 00:02:14,590
now if I run my code just like this you should be created entity then our print
现在，如果我像这样运行代码，则应该创建一个实体，然后打印

36
00:02:14,789 --> 00:02:18,670
function running which prints x and y and then finally destroyed entity let's
正在运行的函数将打印x和y，然后最终销毁实体，让我们

37
00:02:18,870 --> 00:02:22,450
take a deeper look at the how this works I'm going to put a breakpoint on line 28
深入了解它是如何工作的，我将在第28行放置一个断点

38
00:02:22,650 --> 00:02:26,350
here next to our entity and here f5 currently about absolutely nothing in a
在我们实体旁边，在这里F5目前在

39
00:02:26,550 --> 00:02:30,670
console if I hit f10 here and look back you can say that my entity has been
控制台，如果我在这里按f10键，然后回头看，您可以说我的实体已经

40
00:02:30,870 --> 00:02:34,660
created I'm now going to hit f10 to print and you can see that my x and y
创建后，我现在要按f10键进行打印，您可以看到我的x和y 

41
00:02:34,860 --> 00:02:38,439
position gets printed and finally the end of the scope is reached here we're
位置被打印，最后到达作用域的尽头

42
00:02:38,639 --> 00:02:43,030
about a jump back to line 34 which is our return address since this object was
大约跳回到第34行，这是我们的返回地址，因为此对象是

43
00:02:43,229 --> 00:02:46,810
created on the stack it should get automatically destroyed when it goes out
创建在堆栈上，它应该在销毁后自动销毁

44
00:02:47,009 --> 00:02:51,340
of scope which is right over here so if I hit f10 you can see that destroyed
范围就在这里，所以如果我按f10键，您会看到它被摧毁了

45
00:02:51,539 --> 00:02:55,509
entity gets printed to the console because the destructor is called we jump
实体被打印到控制台，因为析构函数被称为我们跳转

46
00:02:55,709 --> 00:02:58,569
back to our return address and everything is good so that is
回到我们的寄信人地址，一切都很好，那是

47
00:02:58,769 --> 00:03:01,480
essentially what a destructor is it's just a special function and special
本质上是一个析构函数，它只是一个特殊的功能和特殊的

48
00:03:01,680 --> 00:03:05,319
method which gets called when an object gets destroyed after some real-world
当对象在真实世界中被摧毁时调用的方法

49
00:03:05,519 --> 00:03:09,520
examples as to why you would ever write a destructor well again if there is
关于为什么如果有的话您将再次写出析构函数的示例

50
00:03:09,719 --> 00:03:13,450
certain initialization code that you call it in the constructor you would
您将在构造函数中调用的某些初始化代码

51
00:03:13,650 --> 00:03:17,379
likely want to uninitialized or destroy all that starts in the destructor
可能想取消初始化或销毁所有在析构函数中开始的东西

52
00:03:17,579 --> 00:03:20,590
because if you don't you can get memory leaks a good example of that is heap
因为如果不这样做，可能会导致内存泄漏，这是堆的一个很好的例子

53
00:03:20,789 --> 00:03:24,280
allocated objects if you've allocated any kind of memory on the heap manually
如果您已在堆上手动分配了任何类型的内存，则分配对象

54
00:03:24,479 --> 00:03:29,349
and you need to manually clean it up if it was allocated throughout the usage of
如果在整个使用过程中分配了它，则需要手动清理它

55
00:03:29,549 --> 00:03:32,560
the MTR in the entity constructor you'll actually going to want to get rid of it
您实际上要摆脱它的实体构造函数中的MTR 

56
00:03:32,759 --> 00:03:37,629
in the destructor because when they try to get called that entity is gone you
在析构函数中，因为当他们尝试调用该实体时，您消失了

57
00:03:37,829 --> 00:03:41,830
can also call the destructor manually not something that I see a lot of people
也可以手动调用析构函数，而不是我看到的很多人

58
00:03:42,030 --> 00:03:46,990
doing rightly so it's a bit weird if you end up doing something like that the
正确地做，如果您最终做这样的事情，那会有点奇怪

59
00:03:47,189 --> 00:03:50,110
only reason I can only think of doing that is maybe if you've allocated using
我只能想到的唯一原因可能是如果您使用

60
00:03:50,310 --> 00:03:54,069
new however when you delete it you decide to use something like the free
新的，但是当您删除它时，您决定使用类似免费的东西

61
00:03:54,269 --> 00:03:58,118
function and then you also want to call disrupt and manually make it I don't
功能，然后您还想调用打扰并手动将其设置为不

62
00:03:58,318 --> 00:04:03,700
know not really use that often but you can do so by just calling e dot and see
知道不是经常使用，但是您可以通过调用e点来查看

63
00:04:03,900 --> 00:04:06,550
you like this as if it was any other function and that
您喜欢它，就像其他任何功能一样

64
00:04:06,750 --> 00:04:11,740
will actually call the destructor if I run this code which is innocent enough
如果我运行足够纯真的代码，它将实际上调用析构函数

65
00:04:11,939 --> 00:04:14,890
you can see that it destroys the entity twice seemingly we're not actually
您可以看到它似乎两次破坏了实体，实际上我们并不是

66
00:04:15,090 --> 00:04:17,889
releasing any resources here so it doesn't crash it just prints this
在这里释放任何资源，这样就不会崩溃，只会打印此

67
00:04:18,089 --> 00:04:22,160
message twice again not something I would recommend or not something
消息再次两次不是我会推荐的东西

68
00:04:22,360 --> 00:04:26,959
often at all very very rarely asking something like this so yeah destructors
通常根本很少问这样的事情，所以破坏者

69
00:04:27,158 --> 00:04:29,879
sorted I'll see you guys next time good bye
排序，下次再见。再见

70
00:04:30,079 --> 00:04:35,609
[Music]
[音乐]

71
00:04:38,579 --> 00:04:43,579
[Music]
 [音乐] 

