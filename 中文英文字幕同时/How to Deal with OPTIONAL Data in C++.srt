﻿1
00:00:00,030 --> 00:00:01,779
嘿，大家好，我叫Chennai，欢迎回到我的C ++系列，所以
hey what's up guys my name is the

2
00:00:01,979 --> 00:00:05,049
嘿，大家好，我叫Chennai，欢迎回到我的C ++系列，所以
Chennai welcome back to my C++ series so

3
00:00:05,250 --> 00:00:05,889
今天要谈论的是当我们拥有可能
today were going to be talking about

4
00:00:06,089 --> 00:00:08,530
今天要谈论的是当我们拥有可能
what happens when we have data that may

5
00:00:08,730 --> 00:00:10,780
也许不是很多时候我们都喜欢这样的功能，例如
or may not be there a lot of the times

6
00:00:10,980 --> 00:00:12,460
也许不是很多时候我们都喜欢这样的功能，例如
we have like a function that for example

7
00:00:12,660 --> 00:00:14,349
返回数据，我们只是说我们正在读取文件，但是如果
returns data let's just say we're

8
00:00:14,548 --> 00:00:16,690
返回数据，我们只是说我们正在读取文件，但是如果
reading a file but what happens if the

9
00:00:16,890 --> 00:00:18,370
无法读取文件，如果文件未加载则无法加载
file could not be read what happens if

10
00:00:18,570 --> 00:00:20,230
无法读取文件，如果文件未加载则无法加载
we couldn't load the file if it's not

11
00:00:20,429 --> 00:00:21,999
如果数据不是我们期望的格式，则仍然需要
present if the data just isn't in the

12
00:00:22,199 --> 00:00:24,429
如果数据不是我们期望的格式，则仍然需要
format that we expect we still need to

13
00:00:24,629 --> 00:00:26,169
现在很多时候从那个函数返回一些东西
return something from the function now a

14
00:00:26,368 --> 00:00:27,699
现在很多时候从那个函数返回一些东西
lot of the time in that particular

15
00:00:27,899 --> 00:00:30,249
场景中，您可能只是返回一个空字符串，但这并不是那么好
scenario you might just return an empty

16
00:00:30,449 --> 00:00:33,399
场景中，您可能只是返回一个空字符串，但这并不是那么好
string but that's not really that great

17
00:00:33,600 --> 00:00:35,079
因为没有太大意义，我的意思是如果文件只是空的
because it doesn't make much sense I

18
00:00:35,280 --> 00:00:36,369
因为没有太大意义，我的意思是如果文件只是空的
mean what if the file was just empty

19
00:00:36,570 --> 00:00:38,320
应该有一种方法让我们实际查看数据是否存在或
there should be a way for us to actually

20
00:00:38,520 --> 00:00:41,288
应该有一种方法让我们实际查看数据是否存在或
see if the data was present or if the

21
00:00:41,488 --> 00:00:43,448
数据不存在，这是STD可选项出现的地方
data wasn't present and that's where STD

22
00:00:43,649 --> 00:00:45,399
数据不存在，这是STD可选项出现的地方
optional comes in so as to the optional

23
00:00:45,600 --> 00:00:48,159
是C ++ 17中的新功能，它将像一个三部分的迷你剧集的一部分
is new in C++ 17 and it's gonna be like

24
00:00:48,359 --> 00:00:49,479
是C ++ 17中的新功能，它将像一个三部分的迷你剧集的一部分
a part of like a three-part miniseries

25
00:00:49,679 --> 00:00:51,579
其中涵盖了三个不同的类别三种不同的新类型
in which we cover three different

26
00:00:51,780 --> 00:00:53,140
其中涵盖了三个不同的类别三种不同的新类型
classes three different new types that

27
00:00:53,340 --> 00:00:55,209
紫貂可以让我们处理可能存在或不存在或存在的数据
Sables gives us to deal with data that

28
00:00:55,409 --> 00:00:58,059
紫貂可以让我们处理可能存在或不存在或存在的数据
either may or may not be there or is of

29
00:00:58,259 --> 00:01:00,279
一种我们不确定的类型，让我们跳进去，花点时间
a type that we're not sure about so

30
00:01:00,479 --> 00:01:01,989
一种我们不确定的类型，让我们跳进去，花点时间
let's jump in and take a little bit of a

31
00:01:02,189 --> 00:01:04,209
看一些可以用一种方式编写的代码，但是现在有了SED可选，我们
look at some code that could be written

32
00:01:04,409 --> 00:01:06,789
看一些可以用一种方式编写的代码，但是现在有了SED可选，我们
one way but now with SED optional we

33
00:01:06,989 --> 00:01:08,500
有一个更好的方法来处理可能存在或可能不存在的数据，所以这是
have a better way of dealing with data

34
00:01:08,700 --> 00:01:10,480
有一个更好的方法来处理可能存在或可能不存在的数据，所以这是
that may or may not be there so this is

35
00:01:10,680 --> 00:01:11,769
将是一个非常简单的示例，我从根本没有任何代码开始
going to be a super simple example I'm

36
00:01:11,969 --> 00:01:13,509
将是一个非常简单的示例，我从根本没有任何代码开始
starting with absolutely no code at all

37
00:01:13,709 --> 00:01:15,549
而我要做的是编写一个读取文件的小功能，这样
and what I'm gonna do is write a little

38
00:01:15,750 --> 00:01:17,679
而我要做的是编写一个读取文件的小功能，这样
function which reads a file so this will

39
00:01:17,879 --> 00:01:20,079
基本上就像从文件右侧读取字符串，否则我们可以将其重命名为read
basically be like read string from file

40
00:01:20,280 --> 00:01:23,230
基本上就像从文件右侧读取字符串，否则我们可以将其重命名为read
right or maybe we'll rename it to read

41
00:01:23,430 --> 00:01:25,869
文件为字符串，因此应以字符串形式返回整个文件，例如
file as string so this should return the

42
00:01:26,069 --> 00:01:28,119
文件为字符串，因此应以字符串形式返回整个文件，例如
entire file for example as a string so

43
00:01:28,319 --> 00:01:31,750
我们将输入一个Const STD字符串文件路径，然后在这里，我们只有
we'll take in a Const STD string file

44
00:01:31,950 --> 00:01:35,079
我们将输入一个Const STD字符串文件路径，然后在这里，我们只有
path and then over here we'll just have

45
00:01:35,280 --> 00:01:38,590
输入文件流，我们将输入我们实际想要的文件路径
an input file stream we'll take in the

46
00:01:38,790 --> 00:01:40,539
输入文件流，我们将输入我们实际想要的文件路径
file path of what we actually want to

47
00:01:40,739 --> 00:01:42,789
阅读，我将快速添加F流，是的，我将很快制作一个视频
read I'll just quickly include F stream

48
00:01:42,989 --> 00:01:44,738
阅读，我将快速添加F流，是的，我将很快制作一个视频
and yes I will make a video soon about

49
00:01:44,938 --> 00:01:46,090
如何读取文件，看看我是否知道我现在还没有这样做
how to read files and see if I suppose I

50
00:01:46,290 --> 00:01:47,439
如何读取文件，看看我是否知道我现在还没有这样做
know I haven't done that yet and now

51
00:01:47,640 --> 00:01:49,509
如果输入文件流有效，我们想要做的就是读取它
that we have our input file stream what

52
00:01:49,709 --> 00:01:52,000
如果输入文件流有效，我们想要做的就是读取它
we want to do is read it if it's valid

53
00:01:52,200 --> 00:01:54,488
如果文件成功打开，或者不是，我们也必须处理
if the file was open successfully or if

54
00:01:54,688 --> 00:01:56,439
如果文件成功打开，或者不是，我们也必须处理
it wasn't we have to handle that as well

55
00:01:56,640 --> 00:01:58,599
非常简单，我们可以只编写if流，然后在这里读取文件
so very simply we can just write if

56
00:01:58,799 --> 00:02:01,028
非常简单，我们可以只编写if流，然后在这里读取文件
stream and then we'll read the file here

57
00:02:01,228 --> 00:02:02,500
然后最终我们将关闭该文件流，但是如果没有
and then eventually we'll just close

58
00:02:02,700 --> 00:02:04,959
然后最终我们将关闭该文件流，但是如果没有
that file stream however if it didn't

59
00:02:05,159 --> 00:02:07,539
找出正确的方法，我们也需要处理，所以通常我们可能会这样做
work out right we need to handle that as

60
00:02:07,739 --> 00:02:09,669
找出正确的方法，我们也需要处理，所以通常我们可能会这样做
well so usually we might do that and

61
00:02:09,868 --> 00:02:10,959
就像如果我以不同的方式编写这段代码，我们基本上会返回
like if I just write this code a bit

62
00:02:11,158 --> 00:02:12,880
就像如果我以不同的方式编写这段代码，我们基本上会返回
differently we would basically return

63
00:02:13,080 --> 00:02:14,860
我们读取的字符串，所以我们只说我们有一个STD字符串
our string that we read so let's just

64
00:02:15,060 --> 00:02:17,050
我们读取的字符串，所以我们只说我们有一个STD字符串
say we had an STD string with our

65
00:02:17,250 --> 00:02:19,539
结果，然后我们基本上从此处读取该文件，然后返回
results and then we basically read that

66
00:02:19,739 --> 00:02:21,969
结果，然后我们基本上从此处读取该文件，然后返回
file over here and then we returned our

67
00:02:22,169 --> 00:02:24,340
结果正确，那就是成功之后的样子
result right that would be what it would

68
00:02:24,539 --> 00:02:25,719
结果正确，那就是成功之后的样子
be like if it were successful and then

69
00:02:25,919 --> 00:02:27,910
如果执行不成功，我们该返回什么，我们可以返回一个
if it's not successful well what do we

70
00:02:28,110 --> 00:02:29,740
如果执行不成功，我们该返回什么，我们可以返回一个
return right we could just return an

71
00:02:29,939 --> 00:02:32,230
空字符串，否则我们可以返回一个像这样构造的字符串
empty string or we could return a string

72
00:02:32,430 --> 00:02:34,509
空字符串，否则我们可以返回一个像这样构造的字符串
constructed like this which is the same

73
00:02:34,709 --> 00:02:36,250
确切的结果，但关键是我们很难知道该文件是否为
exact result but the point is it's kind

74
00:02:36,449 --> 00:02:37,719
确切的结果，但关键是我们很难知道该文件是否为
of hard for us to know if that file was

75
00:02:37,919 --> 00:02:39,849
打开成功与否，因为您可以想象我们是否在这里拥有我们的数据，
open successfully or not because you can

76
00:02:40,049 --> 00:02:41,920
打开成功与否，因为您可以想象我们是否在这里拥有我们的数据，
imagine if we have our data here and

77
00:02:42,120 --> 00:02:44,409
这是我们的价目表文件（字符串），我们有文件路径，即点
it's our rate file as string and we have

78
00:02:44,609 --> 00:02:46,509
这是我们的价目表文件（字符串），我们有文件路径，即点
our file path which is let us say dot

79
00:02:46,709 --> 00:02:49,629
成人文字，如果我们在这里有，我们该如何检查它是否已打开
adult text if we have that here what how

80
00:02:49,829 --> 00:02:50,950
成人文字，如果我们在这里有，我们该如何检查它是否已打开
do we check if that was open

81
00:02:51,150 --> 00:02:52,868
成功的很好，我们基本上必须做一些事情，如果数据不存在
successfully well we we basically have

82
00:02:53,068 --> 00:02:54,580
成功的很好，我们基本上必须做一些事情，如果数据不存在
to do something like if data doesn't

83
00:02:54,780 --> 00:02:57,129
等于没有权利，然后对待的是，那根本不好
equal nothing right and then treated is

84
00:02:57,329 --> 00:02:59,530
等于没有权利，然后对待的是，那根本不好
that that's that's not nice at all what

85
00:02:59,729 --> 00:03:01,420
我更希望通过某种方式实际知道该数据是否已读取
I would much prefer is some way of

86
00:03:01,620 --> 00:03:03,069
我更希望通过某种方式实际知道该数据是否已读取
actually knowing if that data was read

87
00:03:03,269 --> 00:03:05,050
成功与否，因为再次可能该文件在那里但它为空
successfully or not because again maybe

88
00:03:05,250 --> 00:03:06,520
成功与否，因为再次可能该文件在那里但它为空
that file was there but it was empty

89
00:03:06,719 --> 00:03:08,679
那可能是有效的，我们需要一种方法来知道它是否无效，所以另一种方法
that might be valid we need a way to

90
00:03:08,878 --> 00:03:11,230
那可能是有效的，我们需要一种方法来知道它是否无效，所以另一种方法
know if it's not valid and so another

91
00:03:11,430 --> 00:03:13,629
解决方案可能是基本上让此输出布尔值正确，这是您知道的
solution might be to basically have this

92
00:03:13,829 --> 00:03:16,090
解决方案可能是基本上让此输出布尔值正确，这是您知道的
output boolean right which was you know

93
00:03:16,289 --> 00:03:18,399
成功，我们可以说是正确的，这是我们通过引用传递的布尔值
a success let's just say right it's a

94
00:03:18,598 --> 00:03:19,990
成功，我们可以说是正确的，这是我们通过引用传递的布尔值
boolean that we passed by reference is

95
00:03:20,189 --> 00:03:21,849
将成为我们的输出变量，所以我可以称之为成功，然后如果
going to be our output variable so I

96
00:03:22,049 --> 00:03:24,189
将成为我们的输出变量，所以我可以称之为成功，然后如果
might call it our success and then if

97
00:03:24,389 --> 00:03:25,810
流已打开，您知道它将成功设置为true，我们将返回
the stream was opened you know it will

98
00:03:26,009 --> 00:03:27,159
流已打开，您知道它将成功设置为true，我们将返回
set success to true and we'll return

99
00:03:27,359 --> 00:03:29,289
否则我们会成功，成功则为假，我们基本上
that otherwise we'll success well that

100
00:03:29,489 --> 00:03:31,629
否则我们会成功，成功则为假，我们基本上
success to false and we'll basically

101
00:03:31,829 --> 00:03:33,009
只是返回一个空字符串，所以在这种情况下，我们不这样做
just return an empty string

102
00:03:33,209 --> 00:03:35,409
只是返回一个空字符串，所以在这种情况下，我们不这样做
so in that case instead of doing this we

103
00:03:35,609 --> 00:03:37,090
需要接受一个布尔值，基本上是文件成功打开，
need to take in a boolean which is

104
00:03:37,289 --> 00:03:40,300
需要接受一个布尔值，基本上是文件成功打开，
basically file open successfully and

105
00:03:40,500 --> 00:03:42,969
然后我们简单地将其传递到这里，然后我们可以检查
then we simply pass that into here right

106
00:03:43,169 --> 00:03:45,340
然后我们简单地将其传递到这里，然后我们可以检查
and then we can check that

107
00:03:45,539 --> 00:03:46,629
而且显然比空字符串读起来要好得多，但是它仍然
and that obviously reads a lot better

108
00:03:46,829 --> 00:03:48,999
而且显然比空字符串读起来要好得多，但是它仍然
than an empty string however it's still

109
00:03:49,199 --> 00:03:51,610
不是特别好，所以STD可选项可以帮助我们选择City
not particularly nice so that is where

110
00:03:51,810 --> 00:03:54,879
不是特别好，所以STD可选项可以帮助我们选择City
STD optional can help us a City optional

111
00:03:55,079 --> 00:03:57,340
我们可以通过仅在此处包含可选头文件来包括
which we can include by just including

112
00:03:57,539 --> 00:03:58,899
我们可以通过仅在此处包含可选头文件来包括
the optional header file here is

113
00:03:59,098 --> 00:04:00,640
基本上听起来像是可选的
basically exactly what it sounds like

114
00:04:00,840 --> 00:04:03,189
基本上听起来像是可选的
it's optional as to whether or not the

115
00:04:03,389 --> 00:04:05,830
数据存在，因此我们将其更改为STD可选，然后我们当然仍然
data is present so we changed this to be

116
00:04:06,030 --> 00:04:08,890
数据存在，因此我们将其更改为STD可选，然后我们当然仍然
STD optional and then of course we still

117
00:04:09,090 --> 00:04:10,899
将我们的字符串作为可选类型，您可以看到
have our string as our type that is

118
00:04:11,098 --> 00:04:12,730
将我们的字符串作为可选类型，您可以看到
optional and you can see that is giving

119
00:04:12,930 --> 00:04:14,259
我在这里说一个错误，说名称空间STD不记得是可选的，因为我
me an error here saying that namespace

120
00:04:14,459 --> 00:04:16,060
我在这里说一个错误，说名称空间STD不记得是可选的，因为我
STD has no remember optional as I

121
00:04:16,259 --> 00:04:18,639
前面提到这是C ++ 17功能，因此请确保您进入
mentioned earlier this is a C++ 17

122
00:04:18,839 --> 00:04:20,800
前面提到这是C ++ 17功能，因此请确保您进入
feature so make sure that you go into

123
00:04:21,000 --> 00:04:21,968
您的设置进入编译器设置
your settings into your compiler

124
00:04:22,168 --> 00:04:22,850
您的设置进入编译器设置
settings

125
00:04:23,050 --> 00:04:26,480
将您的语言版本切换到C ++ 17，然后我们要做的是
switch your language version to C++ 17

126
00:04:26,680 --> 00:04:28,309
将您的语言版本切换到C ++ 17，然后我们要做的是
and then now what we do is we still

127
00:04:28,509 --> 00:04:30,468
像以前一样对待所有内容，并以某种方式删除了F流，包括
treat everything as it was before and

128
00:04:30,668 --> 00:04:34,040
像以前一样对待所有内容，并以某种方式删除了F流，包括
somehow I removed the F stream include

129
00:04:34,240 --> 00:04:35,509
由于某种原因，我们仍然会做以前做过的一切，否则我会做
for some reason so we still do

130
00:04:35,709 --> 00:04:36,980
由于某种原因，我们仍然会做以前做过的一切，否则我会做
everything that we did before or I will

131
00:04:37,180 --> 00:04:38,600
摆脱这一点，我们的成功布尔值，我们不再需要它了
get rid of this our success boolean we

132
00:04:38,800 --> 00:04:39,759
摆脱这一点，我们的成功布尔值，我们不再需要它了
don't need that anymore

133
00:04:39,959 --> 00:04:42,319
然后我们不返回空字符串，而是返回一个空
and then instead of returning an empty

134
00:04:42,519 --> 00:04:43,910
然后我们不返回空字符串，而是返回一个空
string what we can do is return an empty

135
00:04:44,110 --> 00:04:45,588
作为愚蠢的可选内容，您可以通过使用像这样的花括号来做到这一点
as silly optional and you can do that

136
00:04:45,788 --> 00:04:46,910
作为愚蠢的可选内容，您可以通过使用像这样的花括号来做到这一点
just by having curly brackets like this

137
00:04:47,110 --> 00:04:49,520
然后，如果我们希望返回字符串，则只需返回结果，然后您
and then if we want our string to be

138
00:04:49,720 --> 00:04:51,468
然后，如果我们希望返回字符串，则只需返回结果，然后您
returned we simply return result and you

139
00:04:51,668 --> 00:04:53,600
可以看到它真的那么简单，然后如果我们向下滚动到main
can see it's really that simple and then

140
00:04:53,800 --> 00:04:55,249
可以看到它真的那么简单，然后如果我们向下滚动到main
if we scroll down into main here we

141
00:04:55,449 --> 00:04:56,480
不再需要这个了，我们不需要了，我们将其更改为
don't need this anymore

142
00:04:56,680 --> 00:04:59,600
不再需要这个了，我们不需要了，我们将其更改为
we don't need this we change this to be

143
00:04:59,800 --> 00:05:03,920
STD可选STD字符串权限，或者您可以选择使用订单权限
an STD optional STD string right or

144
00:05:04,120 --> 00:05:06,709
STD可选STD字符串权限，或者您可以选择使用订单权限
optionally you could use order right

145
00:05:06,908 --> 00:05:08,959
那也很好，然后我们要做的就是检查数据点是否有价值，如果
that's also fine and then what we do is

146
00:05:09,158 --> 00:05:14,899
那也很好，然后我们要做的就是检查数据点是否有价值，如果
we just check data dot has value and if

147
00:05:15,098 --> 00:05:16,639
如果值是true，则表示已设置了可选参数，我们可以
has value is true that means that

148
00:05:16,839 --> 00:05:18,379
如果值是true，则表示已设置了可选参数，我们可以
optional has been set and we can

149
00:05:18,579 --> 00:05:19,730
基本上，我们将登出控制台文件并成功读取
basically we'll just log out to the

150
00:05:19,930 --> 00:05:24,350
基本上，我们将登出控制台文件并成功读取
console file up read successfully and by

151
00:05:24,550 --> 00:05:25,939
顺便一提，当我编辑此视频时，这就是未来的切尔诺
the way just a quick note while I was

152
00:05:26,139 --> 00:05:27,860
顺便一提，当我编辑此视频时，这就是未来的切尔诺
editing this video this is future Cherno

153
00:05:28,060 --> 00:05:30,528
如果您有数据，您也可以说对，因为有牛市操作员，所以
talking you can also just right if data

154
00:05:30,728 --> 00:05:32,389
如果您有数据，您也可以说对，因为有牛市操作员，所以
because there's a bull operator so

155
00:05:32,589 --> 00:05:33,889
如果数据也完全合法，那么不必写数据就有价值
instead of having to write data has

156
00:05:34,089 --> 00:05:36,829
如果数据也完全合法，那么不必写数据就有价值
value if data is also totally legit and

157
00:05:37,028 --> 00:05:38,778
我认为无论是回到牧场还是看起来更干净，更好
I think it looks even cleaner and nicer

158
00:05:38,978 --> 00:05:41,329
我认为无论是回到牧场还是看起来更干净，更好
anyway back to pasture no otherwise if

159
00:05:41,528 --> 00:05:43,160
它没有价值，那么我们知道该文件未成功读取，因此
it doesn't have value then we know that

160
00:05:43,360 --> 00:05:45,139
它没有价值，那么我们知道该文件未成功读取，因此
that file was not read successfully so

161
00:05:45,339 --> 00:05:48,800
文件无法打开好，因此我们基于以下两个不同的分支
file could not be opened okay so we have

162
00:05:49,000 --> 00:05:50,180
文件无法打开好，因此我们基于以下两个不同的分支
two different branches here based on

163
00:05:50,379 --> 00:05:52,069
是否实际设置了数据，当然要访问它，我们
whether or not that data was actually

164
00:05:52,269 --> 00:05:54,259
是否实际设置了数据，当然要访问它，我们
set on and of course to access it we

165
00:05:54,459 --> 00:05:56,600
还有很多访问方法，我们可以简单地使用数据和错误
have a bunch of ways to access it as

166
00:05:56,800 --> 00:05:58,939
还有很多访问方法，我们可以简单地使用数据和错误
well we can simply use data and an error

167
00:05:59,139 --> 00:06:00,559
然后我们在那里有了我们的字符串，我们也可以简单地取消引用字符串
and then we just have our string there

168
00:06:00,759 --> 00:06:03,050
然后我们在那里有了我们的字符串，我们也可以简单地取消引用字符串
we can also simply dereference a string

169
00:06:03,250 --> 00:06:05,778
所以我们会说这是我们的字符串，基本上只是数据取消引用，例如
so we'll say this is our string and it's

170
00:06:05,978 --> 00:06:07,610
所以我们会说这是我们的字符串，基本上只是数据取消引用，例如
just basically data dereference like

171
00:06:07,810 --> 00:06:09,230
我们可以将其作为普通字符串访问，因此它与智能字符串基本相同
that and we can access it as a normal

172
00:06:09,430 --> 00:06:11,149
我们可以将其作为普通字符串访问，因此它与智能字符串基本相同
string so it's much the same as a smart

173
00:06:11,348 --> 00:06:12,829
关于是否要实际访问该数据的指针，实际上我
pointer in terms of if you want to

174
00:06:13,028 --> 00:06:15,439
关于是否要实际访问该数据的指针，实际上我
actually access that data and in fact I

175
00:06:15,639 --> 00:06:18,410
认为还有一个数据点值可以使用，所以让我们测试一下
think there's also a data dot value

176
00:06:18,610 --> 00:06:21,019
认为还有一个数据点值可以使用，所以让我们测试一下
which you can use so let's test this out

177
00:06:21,218 --> 00:06:23,179
看看是否可行，我要做的就是在此处创建一个虚拟文件
and see if that works what I'll do is

178
00:06:23,379 --> 00:06:24,980
看看是否可行，我要做的就是在此处创建一个虚拟文件
just create a dummy file here I'll make

179
00:06:25,180 --> 00:06:27,050
确保我正在显示所有文件，只是在沙箱中
sure that I'm in show all files and just

180
00:06:27,250 --> 00:06:28,119
确保我正在显示所有文件，只是在沙箱中
in sandbox

181
00:06:28,319 --> 00:06:31,869
我将创建一个新文件，我将其称为数据点txt，我们可能会抛出
I will create a new file and I'll just

182
00:06:32,069 --> 00:06:34,600
我将创建一个新文件，我将其称为数据点txt，我们可能会抛出
call it data dot txt we might just throw

183
00:06:34,800 --> 00:06:37,660
在这里输入一些数据，然后在解决方案资源管理器中执行该操作
in some data into here and then here it

184
00:06:37,860 --> 00:06:40,540
在这里输入一些数据，然后在解决方案资源管理器中执行该操作
is in my solution Explorer what I'll do

185
00:06:40,740 --> 00:06:42,549
是尝试加载它，所以如果我点击，当然会有数据文本，我只会扔一个
is try and load it so there's data text

186
00:06:42,749 --> 00:06:45,189
是尝试加载它，所以如果我点击，当然会有数据文本，我只会扔一个
of course if I hit I'll just throw in a

187
00:06:45,389 --> 00:06:46,899
这里没有C，这样我不会立即关闭控制台
little C in yet here so that I'll

188
00:06:47,098 --> 00:06:48,819
这里没有C，这样我不会立即关闭控制台
console doesn't close immediately and

189
00:06:49,019 --> 00:06:50,619
我们将运行该程序，您可以看到文件已成功读取，现在让我们
we'll just run this program you can see

190
00:06:50,819 --> 00:06:52,809
我们将运行该程序，您可以看到文件已成功读取，现在让我们
the file was read successfully now let's

191
00:06:53,009 --> 00:06:54,730
只是说该文件不存在，我将其删除
just say that file didn't exist I'll go

192
00:06:54,930 --> 00:06:55,989
只是说该文件不存在，我将其删除
ahead and delete it

193
00:06:56,189 --> 00:06:58,569
我们将按f5键，您会看到它说现在无法轻松打开文件
we'll hit f5 and you can see that it

194
00:06:58,769 --> 00:07:00,459
我们将按f5键，您会看到它说现在无法轻松打开文件
says file cannot be opened so easily now

195
00:07:00,658 --> 00:07:01,660
我们可以打开该文件是否现在已成功读取
we're able to switch on whether or not

196
00:07:01,860 --> 00:07:03,879
我们可以打开该文件是否现在已成功读取
that file was read successfully now

197
00:07:04,079 --> 00:07:05,829
我们可以用另一件事做一件非常有用的事情
there's one other really useful thing

198
00:07:06,028 --> 00:07:07,989
我们可以用另一件事做一件非常有用的事情
that we can do with this let's just say

199
00:07:08,189 --> 00:07:09,660
我们正在尝试读取文件，而我们并不太在乎
that we're trying to read the file and

200
00:07:09,860 --> 00:07:12,309
我们正在尝试读取文件，而我们并不太在乎
we don't really care too much about

201
00:07:12,509 --> 00:07:13,899
是否像我们仍然在读取文件一样仍然在意它，但是
whether or not the file was read or not

202
00:07:14,098 --> 00:07:16,028
是否像我们仍然在读取文件一样仍然在意它，但是
like as in we still care about it but

203
00:07:16,228 --> 00:07:17,559
如果无法打开文件，这不是世界末日，因为如果
it's not the end of the world if the

204
00:07:17,759 --> 00:07:19,359
如果无法打开文件，这不是世界末日，因为如果
file couldn't be opened because if the

205
00:07:19,559 --> 00:07:21,129
无法打开文件，或者文件的特定部分未打开
file couldn't be opened or if that

206
00:07:21,329 --> 00:07:23,019
无法打开文件，或者文件的特定部分未打开
particular section of the file wasn't

207
00:07:23,218 --> 00:07:24,968
设置或未读取，也许我们有一个默认值，也很
set or wasn't read maybe we have a

208
00:07:25,168 --> 00:07:27,519
设置或未读取，也许我们有一个默认值，也很
default value for it that's also quite

209
00:07:27,718 --> 00:07:29,619
常见的，所以我们实际上可以做的是有一个城市字符串值，我们可以设置
common so what we can actually do is

210
00:07:29,819 --> 00:07:32,859
常见的，所以我们实际上可以做的是有一个城市字符串值，我们可以设置
have a city string value and we can set

211
00:07:33,059 --> 00:07:37,269
这到数据点值或好吧，这将正是它的作用
this to data dot value or alright and

212
00:07:37,468 --> 00:07:39,338
这到数据点值或好吧，这将正是它的作用
what this will do is exactly what it

213
00:07:39,538 --> 00:07:41,350
听起来像是如果该SV可选数据中实际存在数据
sounds like if the if the data is

214
00:07:41,550 --> 00:07:44,170
听起来像是如果该SV可选数据中实际存在数据
actually present in that SV optional it

215
00:07:44,370 --> 00:07:46,629
如果不是，它将返回给我们该字符串，它将返回任何值
will return to us that string if it's

216
00:07:46,829 --> 00:07:49,179
如果不是，它将返回给我们该字符串，它将返回任何值
not it will return whatever value we

217
00:07:49,379 --> 00:07:52,179
通过这里，所以我们可以说您知道不存在，例如，这真的
pass in here so we can say you know not

218
00:07:52,379 --> 00:07:55,359
通过这里，所以我们可以说您知道不存在，例如，这真的
present for example right this is really

219
00:07:55,559 --> 00:07:57,218
如果您实际上是在暂停文件并尝试提取例如
useful for if you're actually pausing

220
00:07:57,418 --> 00:07:59,410
如果您实际上是在暂停文件并尝试提取例如
files and trying to for example extract

221
00:07:59,610 --> 00:08:02,230
例如变量或已设置的任何种类的元素，因为通常
like variables or any kind of elements

222
00:08:02,430 --> 00:08:04,119
例如变量或已设置的任何种类的元素，因为通常
that have been set because quite often

223
00:08:04,319 --> 00:08:06,100
您可能在文件中确实有一些可选内容
you might have something that is

224
00:08:06,300 --> 00:08:07,838
您可能在文件中确实有一些可选内容
literally optional to have in the file

225
00:08:08,038 --> 00:08:10,509
如果文件中未设置某个参数，则可以使用默认值
and if a certain parameter was not set

226
00:08:10,709 --> 00:08:12,639
如果文件中未设置某个参数，则可以使用默认值
in the file you can go with a default

227
00:08:12,838 --> 00:08:15,009
您在此处指定的值非常有用，因为例如
value of what you specify here this is

228
00:08:15,209 --> 00:08:17,350
您在此处指定的值非常有用，因为例如
so useful because for example we could

229
00:08:17,550 --> 00:08:19,629
只是在这里有一个int计数，例如，也许我们正在尝试运行一个
just simply have an int here count for

230
00:08:19,829 --> 00:08:20,769
只是在这里有一个int计数，例如，也许我们正在尝试运行一个
example maybe we're trying to run a

231
00:08:20,968 --> 00:08:22,329
基准，并在文件中说明如果基准现在有多少次
benchmark and in the file it says how

232
00:08:22,528 --> 00:08:23,889
基准，并在文件中说明如果基准现在有多少次
many times are on the benchmark now if

233
00:08:24,088 --> 00:08:25,718
这存在于文件中，我们只需调用即可提取该计数
this was present in the file we can

234
00:08:25,918 --> 00:08:28,749
这存在于文件中，我们只需调用即可提取该计数
extract that count right by just calling

235
00:08:28,949 --> 00:08:33,008
计算点值，或者如果不存在，我们可以使用默认值a
counts dot value or if it wasn't present

236
00:08:33,208 --> 00:08:34,599
计算点值，或者如果不存在，我们可以使用默认值a
we can go with the default value of a

237
00:08:34,799 --> 00:08:36,039
百，您会看到我们如何做到这一点，因为在这种情况下
hundred and you can see how we can do

238
00:08:36,240 --> 00:08:37,778
百，您会看到我们如何做到这一点，因为在这种情况下
that because this case this would come

239
00:08:37,979 --> 00:08:39,539
从文件开始，所以我们也开始吧
from the file so let's

240
00:08:39,740 --> 00:08:41,639
从文件开始，所以我们也开始吧
start out as well what we'll do is we

241
00:08:41,839 --> 00:08:44,219
将记录该值，当然，因为文件不存在，我们应该
will log that value and of course since

242
00:08:44,419 --> 00:08:45,899
将记录该值，当然，因为文件不存在，我们应该
the file does not exist what we should

243
00:08:46,100 --> 00:08:48,089
看到在这种情况下，我将只记录日志和结束行
see in this case I'll just log and end

244
00:08:48,289 --> 00:08:49,709
看到在这种情况下，我将只记录日志和结束行
line as well what we should see in this

245
00:08:49,909 --> 00:08:51,959
案例不存在，因为您可以看到那是我们不存在的原因，因为
case is not present as you can see

246
00:08:52,159 --> 00:08:53,339
案例不存在，因为您可以看到那是我们不存在的原因，因为
that's what we get not present because

247
00:08:53,539 --> 00:08:55,589
该文件无法完全打开，所以我希望大家喜欢这个视频
the file couldn't be opened all right so

248
00:08:55,789 --> 00:08:56,819
该文件无法完全打开，所以我希望大家喜欢这个视频
I hope you guys enjoyed this video

249
00:08:57,019 --> 00:08:58,469
希望它能像我提到的那样帮助您成为可选的
hopefully it helped you out as to be

250
00:08:58,669 --> 00:09:00,508
希望它能像我提到的那样帮助您成为可选的
optional as I mentioned it's incredibly

251
00:09:00,708 --> 00:09:02,129
有用，您会看到它确实有助于简化代码，并且
useful and you can see that it really

252
00:09:02,330 --> 00:09:04,799
有用，您会看到它确实有助于简化代码，并且
does help kind of simplify your code and

253
00:09:05,000 --> 00:09:07,139
如果你们喜欢这部影片，希望您能产生出更具可读性的代码
hopefully let you produce more readable

254
00:09:07,339 --> 00:09:09,029
如果你们喜欢这部影片，希望您能产生出更具可读性的代码
code if you guys enjoyed this video you

255
00:09:09,230 --> 00:09:10,439
还可以通过访问patreon帮助支持我在youtube上所做的所有操作
can also help support everything that I

256
00:09:10,639 --> 00:09:12,149
还可以通过访问patreon帮助支持我在youtube上所做的所有操作
do here on youtube by going to patreon

257
00:09:12,350 --> 00:09:13,948
大幅削减演出费用，对所有赞助商
account for slash the show no huge thank

258
00:09:14,149 --> 00:09:15,599
大幅削减演出费用，对所有赞助商
you as always to all the patrons that

259
00:09:15,799 --> 00:09:17,758
使这些视频成为可能在下方留下您想要看到我的评论
make these videos possible leave a

260
00:09:17,958 --> 00:09:19,620
使这些视频成为可能在下方留下您想要看到我的评论
comment below on what you want to see me

261
00:09:19,820 --> 00:09:22,229
接下来，我的课程确实很长，正如我之前提到的sed
cover next I do have a very long list of

262
00:09:22,429 --> 00:09:24,389
接下来，我的课程确实很长，正如我之前提到的sed
course and as I mentioned earlier sed

263
00:09:24,589 --> 00:09:27,389
Baran和STV Annie以及超级有用的符号也在该列表中，其中17是
Baran and STV Annie are on that list as

264
00:09:27,589 --> 00:09:29,698
Baran和STV Annie以及超级有用的符号也在该列表中，其中17是
well super useful symbols well 17 is

265
00:09:29,899 --> 00:09:31,109
实际上是一个很棒的版本，我可能会在我的手上制作一个视频
actually such a great release

266
00:09:31,309 --> 00:09:33,120
实际上是一个很棒的版本，我可能会在我的手上制作一个视频
I might actually make a video on my

267
00:09:33,320 --> 00:09:35,399
最喜欢的C ++ 17功能，因此，如果您不订阅，请确保
favorite C++ 17 features so if you're

268
00:09:35,600 --> 00:09:36,779
最喜欢的C ++ 17功能，因此，如果您不订阅，请确保
not subscribe to make sure that you do

269
00:09:36,980 --> 00:09:39,269
这样您就不会错过它，下次我会再见
that so that you don't miss that and I

270
00:09:39,470 --> 00:09:42,379
这样您就不会错过它，下次我会再见
will see you guys next time goodbye

271
00:09:42,580 --> 00:09:47,580
[音乐]
[Music]

