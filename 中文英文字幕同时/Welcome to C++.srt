﻿1
00:00:00,030 --> 00:00:01,449
嘿，大家好，我叫切尔诺（Cherno），欢迎您来参加这个全新的系列赛
hey what's up guys my name is a Cherno

2
00:00:01,649 --> 00:00:04,030
嘿，大家好，我叫切尔诺（Cherno），欢迎您来参加这个全新的系列赛
and welcome to a brand new series this

3
00:00:04,230 --> 00:00:05,139
本系列将教您有关C ++的所有知识
series is going to teach you everything

4
00:00:05,339 --> 00:00:07,629
本系列将教您有关C ++的所有知识
you need to know about C++ we're gonna

5
00:00:07,830 --> 00:00:09,790
在游戏开发的背景下专门学习C ++，但不仅仅是
learn C++ specifically in the context of

6
00:00:09,990 --> 00:00:11,200
在游戏开发的背景下专门学习C ++，但不仅仅是
game development however it's not just

7
00:00:11,400 --> 00:00:13,120
将应用于游戏开发游戏编程，我们将使用游戏
gonna apply to game developing a game

8
00:00:13,320 --> 00:00:14,979
将应用于游戏开发游戏编程，我们将使用游戏
programming we're just gonna use games

9
00:00:15,179 --> 00:00:17,499
举例来说，无论您是
as an example this series is gonna apply

10
00:00:17,699 --> 00:00:18,940
举例来说，无论您是
to pretty much everyone whether you're a

11
00:00:19,140 --> 00:00:20,650
初学者或高级，我们将介绍该语言的基础知识，但我们会
beginner or you're advanced we'll cover

12
00:00:20,850 --> 00:00:22,359
初学者或高级，我们将介绍该语言的基础知识，但我们会
the basics of the language but we'll be

13
00:00:22,559 --> 00:00:25,030
相当简洁，所以基本上，如果您是初学者，您仍然可以关注
quite concise with that so basically if

14
00:00:25,230 --> 00:00:26,829
相当简洁，所以基本上，如果您是初学者，您仍然可以关注
you are a beginner you can still follow

15
00:00:27,028 --> 00:00:28,300
以及本系列文章，但您将必须能够使用Google的东西并做
along with this series but you're gonna

16
00:00:28,500 --> 00:00:30,339
以及本系列文章，但您将必须能够使用Google的东西并做
have to be able to Google things and do

17
00:00:30,539 --> 00:00:32,500
一些研究和发现基本上为您自己找到了一些基础知识
some research and finds find some of the

18
00:00:32,700 --> 00:00:34,329
一些研究和发现基本上为您自己找到了一些基础知识
basics out for yourself basically just

19
00:00:34,530 --> 00:00:35,890
成为一个普通人和你不了解的Google东西，我不想
be a normal person and Google things

20
00:00:36,090 --> 00:00:37,448
成为一个普通人和你不了解的Google东西，我不想
that you don't understand I don't want

21
00:00:37,649 --> 00:00:39,250
停留太久的基础知识，并涵盖了极端的细节，所以
to dwell on the basics for too long and

22
00:00:39,450 --> 00:00:40,839
停留太久的基础知识，并涵盖了极端的细节，所以
cover them in extreme detail so just

23
00:00:41,039 --> 00:00:42,128
确保您在Google上搜索到的东西并且一切都好，所以现在的问题是为什么
make sure you google things and you'll

24
00:00:42,329 --> 00:00:44,529
确保您在Google上搜索到的东西并且一切都好，所以现在的问题是为什么
be fine so now the question is why why

25
00:00:44,729 --> 00:00:47,619
我想学习C ++还是一种过时的语言吗，为什么
would I want to learn C++ it's in C++

26
00:00:47,820 --> 00:00:49,419
我想学习C ++还是一种过时的语言吗，为什么
kind of an outdated language why what

27
00:00:49,619 --> 00:00:51,309
如今在学习C ++方面有很多好处，所以C ++仍然是最多的
good is there in learning C++ nowadays

28
00:00:51,509 --> 00:00:53,079
如今在学习C ++方面有很多好处，所以C ++仍然是最多的
so C++ is still pretty much the most

29
00:00:53,280 --> 00:00:55,059
需要编写性能良好的快速代码时使用的语言
used language when you need to write

30
00:00:55,259 --> 00:00:58,268
需要编写性能良好的快速代码时使用的语言
fast code that performs well or if

31
00:00:58,469 --> 00:01:00,158
您正在为怪异的体系结构或平台编写代码，并且需要运行代码
you're writing for a weird architecture

32
00:01:00,359 --> 00:01:01,748
您正在为怪异的体系结构或平台编写代码，并且需要运行代码
or a platform and you need the code run

33
00:01:01,948 --> 00:01:03,518
如果您想直接控制Hardware C ++，那么本机就适合您
natively if you want direct control over

34
00:01:03,719 --> 00:01:05,918
如果您想直接控制Hardware C ++，那么本机就适合您
Hardware C++ is for you the games

35
00:01:06,118 --> 00:01:07,750
例如，行业广泛使用C ++和像unity这样的游戏引擎
industry for example uses C++

36
00:01:07,950 --> 00:01:10,028
例如，行业广泛使用C ++和像unity这样的游戏引擎
extensively and game engines like unity

37
00:01:10,228 --> 00:01:12,519
它们都是用C ++编写的，所以问题是为什么我们为什么要
Unreal Frostbite they're all written in

38
00:01:12,719 --> 00:01:14,890
它们都是用C ++编写的，所以问题是为什么我们为什么要
C++ so the question is why why would we

39
00:01:15,090 --> 00:01:16,659
想要访问硬件，为什么所有这些用C ++编写的游戏引擎，为什么
want to act access to hardware why all

40
00:01:16,859 --> 00:01:19,000
想要访问硬件，为什么所有这些用C ++编写的游戏引擎，为什么
of these game engines written in C++ why

41
00:01:19,200 --> 00:01:20,558
不使用其他语言使用C ++的最大原因是直接
not use another language the biggest

42
00:01:20,759 --> 00:01:22,569
不使用其他语言使用C ++的最大原因是直接
reason for using C++ is the direct

43
00:01:22,769 --> 00:01:24,340
控制硬件，让我们谈谈C ++的工作原理，以便您
control over hardware let's talk a

44
00:01:24,540 --> 00:01:26,319
控制硬件，让我们谈谈C ++的工作原理，以便您
little bit about how C++ works so you

45
00:01:26,519 --> 00:01:28,899
用C ++编写代码，然后将代码传递到编译器中
write your code in C++ you pass your

46
00:01:29,099 --> 00:01:31,359
用C ++编写代码，然后将代码传递到编译器中
code into a compiler and that compiler

47
00:01:31,560 --> 00:01:33,849
将输出您目标平台机器代码的机器代码是实际的
will output machine code for your target

48
00:01:34,049 --> 00:01:36,009
将输出您目标平台机器代码的机器代码是实际的
platform machine code is the actual

49
00:01:36,209 --> 00:01:38,168
设备的CPU实际执行的指令，因此使用C ++我们可以
instructions that your device's CPU will

50
00:01:38,368 --> 00:01:40,869
设备的CPU实际执行的指令，因此使用C ++我们可以
actually perform so using C++ we can

51
00:01:41,069 --> 00:01:42,429
从字面上控制您的CPU执行的每条指令
literally control every single

52
00:01:42,629 --> 00:01:45,369
从字面上控制您的CPU执行的每条指令
instruction that your CPU executes what

53
00:01:45,569 --> 00:01:47,558
C ++可以在平台上运行吗，您几乎需要任何只需要编译器的东西
platforms does C++ run on you ask pretty

54
00:01:47,759 --> 00:01:49,299
C ++可以在平台上运行吗，您几乎需要任何只需要编译器的东西
much anything you just need a compiler

55
00:01:49,500 --> 00:01:51,189
它将输出该平台的机器代码，例如x64编译器
that will output machine code for that

56
00:01:51,390 --> 00:01:53,769
它将输出该平台的机器代码，例如x64编译器
platform for example an x64 compiler

57
00:01:53,969 --> 00:01:56,289
将输出x64机器代码，从而在x64 CPU上运行，以便为您提供
will output x64 machine code and thus

58
00:01:56,489 --> 00:01:58,778
将输出x64机器代码，从而在x64 CPU上运行，以便为您提供
run on x64 CPUs so to give you an

59
00:01:58,978 --> 00:02:00,069
C ++通常用于开发的某些平台的示例
example of some of the platforms that

60
00:02:00,269 --> 00:02:02,378
C ++通常用于开发的某些平台的示例
C++ is commonly used to develop for

61
00:02:02,578 --> 00:02:05,319
Windows Mac Linux其他任何桌面操作系统几乎都
Windows Mac Linux any other desktop

62
00:02:05,519 --> 00:02:06,609
Windows Mac Linux其他任何桌面操作系统几乎都
operating systems pretty much all of

63
00:02:06,810 --> 00:02:08,830
他们的移动操作系统iOS Android例如，您可以编写
them mobile operating systems iOS

64
00:02:09,030 --> 00:02:10,270
他们的移动操作系统iOS Android例如，您可以编写
Android for example you can write

65
00:02:10,469 --> 00:02:10,920
C ++和所有控制台中的贴花，因此Xbox
applique

66
00:02:11,120 --> 00:02:14,368
C ++和所有控制台中的贴花，因此Xbox
in C++ and all the consoles so Xbox

67
00:02:14,568 --> 00:02:16,080
Playstation和所有Nintendo物品，例如3ds Wii U开关
Playstation and all the Nintendo stuff

68
00:02:16,280 --> 00:02:18,390
Playstation和所有Nintendo物品，例如3ds Wii U开关
like the 3ds the Wii U the switch

69
00:02:18,590 --> 00:02:21,569
当您需要Y平台支持时，所有C ++都很棒，因为只要
everything C++ is great when you need Y

70
00:02:21,769 --> 00:02:23,340
当您需要Y平台支持时，所有C ++都很棒，因为只要
platform support because as long as

71
00:02:23,539 --> 00:02:25,920
有一个编译器，您可以将C ++编译为将运行的本机代码
there's a compiler you can get C++ to

72
00:02:26,120 --> 00:02:27,630
有一个编译器，您可以将C ++编译为将运行的本机代码
compile into native code that will run

73
00:02:27,830 --> 00:02:29,640
在那个平台上当然还有其他本地语言
on that platform of course there are

74
00:02:29,840 --> 00:02:31,800
在那个平台上当然还有其他本地语言
other native languages out there it's

75
00:02:32,000 --> 00:02:33,450
只是C ++就是80年代初以来出现的一种
just the C++ as the one that's been

76
00:02:33,650 --> 00:02:35,550
只是C ++就是80年代初以来出现的一种
around since the early 80s and it's just

77
00:02:35,750 --> 00:02:37,368
如此受欢迎，每个人都知道，只是每个人都在使用它
so popular and everyone knows it that

78
00:02:37,568 --> 00:02:40,530
如此受欢迎，每个人都知道，只是每个人都在使用它
it's just everyone's using it other

79
00:02:40,729 --> 00:02:42,750
C Sharp或Java之类的语言之所以不同，是因为它们在虚拟机上运行
languages like C sharp or Java differ

80
00:02:42,949 --> 00:02:44,160
C Sharp或Java之类的语言之所以不同，是因为它们在虚拟机上运行
because they run on a virtual machine

81
00:02:44,360 --> 00:02:46,200
这意味着您的代码将首先被编译为中间语言，然后
this means that your code gets compiled

82
00:02:46,400 --> 00:02:48,030
这意味着您的代码将首先被编译为中间语言，然后
into an intermediate language first and

83
00:02:48,229 --> 00:02:48,990
然后，当您在该目标平台上实际运行应用程序时，
then when you actually run your

84
00:02:49,189 --> 00:02:51,509
然后，当您在该目标平台上实际运行应用程序时，
application on that target platform the

85
00:02:51,709 --> 00:02:53,879
VM虚拟机基本上会在运行时将其转换为机器代码
VM the virtual machine will basically

86
00:02:54,079 --> 00:02:55,679
VM虚拟机基本上会在运行时将其转换为机器代码
convert it into machine code at runtime

87
00:02:55,878 --> 00:02:57,719
如果想假装自己用英语写了一本书，但想
so picture this if you will pretend that

88
00:02:57,919 --> 00:02:59,340
如果想假装自己用英语写了一本书，但想
you wrote a book in English but you want

89
00:02:59,539 --> 00:03:01,080
在德国只说德语的人会读你的书，所以你已经
someone in Germany who only speaks

90
00:03:01,280 --> 00:03:03,030
在德国只说德语的人会读你的书，所以你已经
German to read your book so what you've

91
00:03:03,229 --> 00:03:05,099
决定要做的实际上是在德国商店出售英语书籍
decided to do is actually sell the book

92
00:03:05,299 --> 00:03:08,480
决定要做的实际上是在德国商店出售英语书籍
in English at the German store however

93
00:03:08,680 --> 00:03:11,429
当您买书时，您也会像翻译一样像一个真正的人
when you buy the book you also get like

94
00:03:11,628 --> 00:03:13,649
当您买书时，您也会像翻译一样像一个真正的人
a translator like an actual person to

95
00:03:13,848 --> 00:03:15,450
和你一起回家我知道这个比喻有点怪异，但只是忍受
come home with you I know this metaphor

96
00:03:15,650 --> 00:03:16,890
和你一起回家我知道这个比喻有点怪异，但只是忍受
is getting a bit weird but just bear

97
00:03:17,090 --> 00:03:18,780
和我在一起，这样那个人回家，然后当那个德国人
with me so the person comes home and

98
00:03:18,979 --> 00:03:20,490
和我在一起，这样那个人回家，然后当那个德国人
then when the when that German person

99
00:03:20,689 --> 00:03:22,800
想读这本书，译者用英语选出这本书，然后
wants to read the book the translator

100
00:03:23,000 --> 00:03:25,618
想读这本书，译者用英语选出这本书，然后
breeds the book in English and then just

101
00:03:25,818 --> 00:03:27,480
基本上停止用德语说话，所以就像现场翻译整个
basically stops talking in German so

102
00:03:27,680 --> 00:03:28,950
基本上停止用德语说话，所以就像现场翻译整个
it's like live translating the whole

103
00:03:29,150 --> 00:03:30,930
为某人预订，这是在虚拟机上运行所看不到的
book for the person that's kind of what

104
00:03:31,129 --> 00:03:32,730
为某人预订，这是在虚拟机上运行所看不到的
running on a virtual machine is blind of

105
00:03:32,930 --> 00:03:34,500
当然，如果您只翻译这本书本身就会更有效率
course it would be a lot more efficient

106
00:03:34,699 --> 00:03:36,300
当然，如果您只翻译这本书本身就会更有效率
if you just translated the book itself

107
00:03:36,500 --> 00:03:39,060
进入德语，然后在德国商店中出售，这样就可以
into German and then sold that in German

108
00:03:39,259 --> 00:03:41,310
进入德语，然后在德国商店中出售，这样就可以
stores so that way it would be in the

109
00:03:41,509 --> 00:03:43,469
德语作为母语，这当然不是一个完美的隐喻，但这是
native language in German and that's of

110
00:03:43,669 --> 00:03:45,300
德语作为母语，这当然不是一个完美的隐喻，但这是
course not a perfect metaphor but that's

111
00:03:45,500 --> 00:03:47,159
像boss blasts和Java或C之类的语言一样运行
kind of what it's like to run say boss

112
00:03:47,359 --> 00:03:49,080
像boss blasts和Java或C之类的语言一样运行
blasts versus a language like Java or C

113
00:03:49,280 --> 00:03:51,868
Sharp C ++是一种本地语言，C ++编译器为此生成机器代码
sharp C++ is a native language the C++

114
00:03:52,068 --> 00:03:53,819
Sharp C ++是一种本地语言，C ++编译器为此生成机器代码
compiler produces machine code for that

115
00:03:54,019 --> 00:03:55,530
目标平台并针对该目标体系结构，仅此而已
target platform and for that target

116
00:03:55,729 --> 00:03:57,420
目标平台并针对该目标体系结构，仅此而已
architecture and then that's it it's

117
00:03:57,620 --> 00:03:58,920
作为本地代码，它将仅在该平台上运行，但是已经在
there as native code it will only run on

118
00:03:59,120 --> 00:04:01,289
作为本地代码，它将仅在该平台上运行，但是已经在
that platform however it's already in

119
00:04:01,489 --> 00:04:03,149
该平台使用机器代码语言，因此绝对没有翻译或
that platforms machine code language so

120
00:04:03,348 --> 00:04:04,830
该平台使用机器代码语言，因此绝对没有翻译或
there's absolutely no translation or

121
00:04:05,030 --> 00:04:06,450
只需运行它们，只要推动机器代码即可
anything required you just run them

122
00:04:06,650 --> 00:04:08,099
只需运行它们，只要推动机器代码即可
which you just push the machine code

123
00:04:08,299 --> 00:04:08,969
指令CPU指令进入CPU及其它
instructions

124
00:04:09,169 --> 00:04:10,830
指令CPU指令进入CPU及其它
CPU instructions into the CPU and it

125
00:04:11,030 --> 00:04:12,509
当然会因为您的代码是本机代码而执行所有指令
will perform all of your instructions of

126
00:04:12,709 --> 00:04:14,789
当然会因为您的代码是本机代码而执行所有指令
course just because your code is native

127
00:04:14,989 --> 00:04:17,038
并不意味着如果您用C ++编写错误的代码，它将很快
doesn't mean it's going to be fast if

128
00:04:17,238 --> 00:04:19,319
并不意味着如果您用C ++编写错误的代码，它将很快
you write bad code in C++

129
00:04:19,519 --> 00:04:21,629
实际上这会很慢，甚至可能会比虚拟机慢
it's gonna be slow in fact it'll likely

130
00:04:21,829 --> 00:04:23,730
实际上这会很慢，甚至可能会比虚拟机慢
even be slower than virtual machine

131
00:04:23,930 --> 00:04:25,468
C Sharp或Java之类的语言，因为它们倾向于动态地优化事物
languages like C sharp or Java because

132
00:04:25,668 --> 00:04:27,088
C Sharp或Java之类的语言，因为它们倾向于动态地优化事物
they tend to optimize things on the fly

133
00:04:27,288 --> 00:04:29,249
比C ++世界好很多，所以如果您是对的，那么C ++ C ++肯定会
a lot better than C++ world so if you're

134
00:04:29,449 --> 00:04:32,429
比C ++世界好很多，所以如果您是对的，那么C ++ C ++肯定会
right bad C++ C++ will definitely be

135
00:04:32,629 --> 00:04:34,410
比C Sharp或Java慢，因此我是说我喜欢使用语言
slower than C sharp or Java and because

136
00:04:34,610 --> 00:04:36,088
比C Sharp或Java慢，因此我是说我喜欢使用语言
of that I mean I love using languages

137
00:04:36,288 --> 00:04:37,559
当我只需要编写工具或不必要的东西时，就像C Sharp
like C sharp when I just need to write

138
00:04:37,759 --> 00:04:39,509
当我只需要编写工具或不必要的东西时，就像C Sharp
tools or things that don't necessarily

139
00:04:39,709 --> 00:04:41,910
需要我从他们身上榨取每一点表现C锋利的是
need me to squeeze every bit of

140
00:04:42,110 --> 00:04:44,129
需要我从他们身上榨取每一点表现C锋利的是
performance out of them C sharp is a

141
00:04:44,329 --> 00:04:45,838
很棒的语言计划，但是本系列适合我们真正需要的时间
fantastic language plan but this series

142
00:04:46,038 --> 00:04:47,459
很棒的语言计划，但是本系列适合我们真正需要的时间
is gonna be for when we actually need

143
00:04:47,658 --> 00:04:48,990
当我们需要写圈巴士时的表现，我们将学习
that performance for when we need to

144
00:04:49,189 --> 00:04:50,249
当我们需要写圈巴士时的表现，我们将学习
write circles bus so we're gonna learn

145
00:04:50,449 --> 00:04:53,129
关于如何正确编写C ++的所有信息如何编写良好的代码如何快速编写
all about how to write C++ correctly how

146
00:04:53,329 --> 00:04:54,869
关于如何正确编写C ++的所有信息如何编写良好的代码如何快速编写
to write good code how to write fast

147
00:04:55,069 --> 00:04:57,689
代码，所以我们要涵盖的内容您可能一开始就会问得很好
code so what are we gonna cover you may

148
00:04:57,889 --> 00:04:59,850
代码，所以我们要涵盖的内容您可能一开始就会问得很好
ask well of course from the beginning

149
00:05:00,050 --> 00:05:01,649
我们将从基础知识开始，当涉及到
we're gonna start with the basics I'm

150
00:05:01,848 --> 00:05:03,660
我们将从基础知识开始，当涉及到
gonna be real concise when it comes to

151
00:05:03,860 --> 00:05:05,490
基础知识，因为如果您愿意，我真的不想过多地讨论它们
the basics because I don't really want

152
00:05:05,689 --> 00:05:07,410
基础知识，因为如果您愿意，我真的不想过多地讨论它们
to dwell on them too much if you've

153
00:05:07,610 --> 00:05:09,778
在我仍然鼓励您观看本系列之前，从未编程过，但是
never programmed before I still do

154
00:05:09,978 --> 00:05:11,278
在我仍然鼓励您观看本系列之前，从未编程过，但是
encourage you to watch this series but

155
00:05:11,478 --> 00:05:12,809
就像我之前说的，您将不得不谷歌搜索您不喜欢的东西
as I said earlier you're gonna have to

156
00:05:13,009 --> 00:05:14,369
就像我之前说的，您将不得不谷歌搜索您不喜欢的东西
just google the things that you don't

157
00:05:14,569 --> 00:05:16,499
理解跟随钥匙学习新事物应该很容易
understand it should be pretty easy to

158
00:05:16,699 --> 00:05:19,410
理解跟随钥匙学习新事物应该很容易
follow the key to learning anything new

159
00:05:19,610 --> 00:05:21,059
真的只是去尝试它，并尝试它，所以我
really is just to play around with it

160
00:05:21,259 --> 00:05:22,468
真的只是去尝试它，并尝试它，所以我
and just to experiment with it so I'm

161
00:05:22,668 --> 00:05:24,360
如果您对它充满热情，并且如果您
sure you'll be absolutely fine if you're

162
00:05:24,560 --> 00:05:25,949
如果您对它充满热情，并且如果您
just passionate about it and if you

163
00:05:26,149 --> 00:05:28,290
实际上有学习的意愿，我们将讨论如何使用C ++库
actually have the willingness to learn

164
00:05:28,490 --> 00:05:30,480
实际上有学习的意愿，我们将讨论如何使用C ++库
we'll talk about how to use C++ library

165
00:05:30,680 --> 00:05:32,040
有利于您，如果您关心的话应该避免什么
to your advantage and what things to

166
00:05:32,240 --> 00:05:33,269
有利于您，如果您关心的话应该避免什么
kind of avoid if you care about

167
00:05:33,468 --> 00:05:34,860
性能，我们将讨论销售亏损的实际运作方式，我们将讨论
performance we'll talk about how sales

168
00:05:35,060 --> 00:05:36,420
性能，我们将讨论销售亏损的实际运作方式，我们将讨论
loss actually works we'll talk about

169
00:05:36,620 --> 00:05:39,180
诸如内存和指针之类的东西由于某种原因而被严重教授
stuff like memory and pointers seriously

170
00:05:39,379 --> 00:05:40,499
诸如内存和指针之类的东西由于某种原因而被严重教授
this for some reason is taught

171
00:05:40,699 --> 00:05:42,899
在大多数情况下绝对非常糟糕，因此我们将实际教指针和
absolutely terribly in most cases so

172
00:05:43,098 --> 00:05:45,149
在大多数情况下绝对非常糟糕，因此我们将实际教指针和
we're gonna actually teach pointers and

173
00:05:45,348 --> 00:05:46,889
这次，内存和东西正确地存储了arenas自定义分配器
memory and stuff properly this time

174
00:05:47,089 --> 00:05:49,920
这次，内存和东西正确地存储了arenas自定义分配器
memory arenas custom allocators smart

175
00:05:50,120 --> 00:05:52,619
指针移动诸如模板之类的语义内容以及如何实际使用它们
pointers move semantics stuff like that

176
00:05:52,819 --> 00:05:54,329
指针移动诸如模板之类的语义内容以及如何实际使用它们
templates and how to actually use them

177
00:05:54,528 --> 00:05:55,319
如果您知道如何很好地使用模板，那么它们非常好
properly if you know how to use

178
00:05:55,519 --> 00:05:56,999
如果您知道如何很好地使用模板，那么它们非常好
templates well they're extremely

179
00:05:57,199 --> 00:05:58,528
功能强大，它们将使您的生活更加轻松，我们将谈论诸如
powerful and they'll make your life a

180
00:05:58,728 --> 00:06:00,240
功能强大，它们将使您的生活更加轻松，我们将谈论诸如
lot easier we'll talk about stuff like

181
00:06:00,439 --> 00:06:02,369
宏以及如何针对多个平台进行编程，我们将编写自己的数据
macros and how to program for multiple

182
00:06:02,569 --> 00:06:03,899
宏以及如何针对多个平台进行编程，我们将编写自己的数据
platforms we'll write our own data

183
00:06:04,098 --> 00:06:05,338
结构，并学习如何使其比标准结构快得多
structures and learn how to make them a

184
00:06:05,538 --> 00:06:06,869
结构，并学习如何使其比标准结构快得多
lot faster than the standard ones you

185
00:06:07,069 --> 00:06:08,790
使用C ++库，我们甚至可以通过
get with the C++ library we'll even

186
00:06:08,990 --> 00:06:10,410
使用C ++库，我们甚至可以通过
cover low-level optimization via

187
00:06:10,610 --> 00:06:12,600
编译器的内在函数和汇编程序，例如编写我们的数学和SSE，这就是
compiler intrinsics and assembly such as

188
00:06:12,800 --> 00:06:14,579
编译器的内在函数和汇编程序，例如编写我们的数学和SSE，这就是
writing our maths and SSE and that's

189
00:06:14,778 --> 00:06:16,499
几乎可以涵盖整个过程中的很多事情
pretty much it will probably cover a lot

190
00:06:16,699 --> 00:06:17,309
几乎可以涵盖整个过程中的很多事情
of things along the way

191
00:06:17,509 --> 00:06:19,019
如果你们有特定要求
if there are specific things that you

192
00:06:19,218 --> 00:06:20,500
如果你们有特定要求
guys request or

193
00:06:20,699 --> 00:06:22,509
可能会涉及到我谈论10分钟，关于我可能会
that probably could involve me talking

194
00:06:22,709 --> 00:06:23,920
可能会涉及到我谈论10分钟，关于我可能会
to 10 minutes about I might make a

195
00:06:24,120 --> 00:06:25,780
最后，如果您想知道我是谁，请分别提供有关他们的视频
separate video about them just in

196
00:06:25,980 --> 00:06:27,699
最后，如果您想知道我是谁，请分别提供有关他们的视频
general lastly if you want to know who I

197
00:06:27,899 --> 00:06:29,530
我是一名软件工程师，我们在EA墨尔本的游戏引擎团队工作，
am I'm a software engineer who works on

198
00:06:29,730 --> 00:06:31,540
我是一名软件工程师，我们在EA墨尔本的游戏引擎团队工作，
the game engine team at EA Melbourne we

199
00:06:31,740 --> 00:06:33,160
可以制作出最好的3D手机游戏引擎，因此我们所拥有的游戏
make pretty much the best 3d mobile game

200
00:06:33,360 --> 00:06:35,139
可以制作出最好的3D手机游戏引擎，因此我们所拥有的游戏
engine out there so the games that we've

201
00:06:35,339 --> 00:06:37,210
最近发货的是《极品飞车》和《极品飞车》
recently shipped were Need for Speed no

202
00:06:37,410 --> 00:06:39,759
最近发货的是《极品飞车》和《极品飞车》
limits and Need for Speed No Limits VR

203
00:06:39,959 --> 00:06:41,740
如果想了解我，可以在Twitter或Instagram上关注我
you can follow me on Twitter or

204
00:06:41,939 --> 00:06:42,939
如果想了解我，可以在Twitter或Instagram上关注我
Instagram if you want to get to know me

205
00:06:43,139 --> 00:06:44,920
该系列的最佳剧集将在大约每周左右发布
better episodes for this series will be

206
00:06:45,120 --> 00:06:47,020
该系列的最佳剧集将在大约每周左右发布
released around every week or so

207
00:06:47,220 --> 00:06:49,240
根据我的日程安排，如果您想支持本系列并确保
depending on my schedule if you want to

208
00:06:49,439 --> 00:06:50,800
根据我的日程安排，如果您想支持本系列并确保
support this series and ensure that the

209
00:06:51,000 --> 00:06:52,660
视频尽快播出，您可以访问patreon.com
videos get out as soon as possible and

210
00:06:52,860 --> 00:06:54,129
视频尽快播出，您可以访问patreon.com
you can do so by going to patreon.com

211
00:06:54,329 --> 00:06:56,439
4/2切尔诺，除了这个系列应该是绝对的爆炸我真的
4/2 Cherno other than that this series

212
00:06:56,639 --> 00:06:58,030
4/2切尔诺，除了这个系列应该是绝对的爆炸我真的
should be an absolute blast I'm really

213
00:06:58,230 --> 00:06:59,590
期待它，让我们了解我是否正确输了，接下来我再见
looking forward to it let's learn see if

214
00:06:59,790 --> 00:07:01,120
期待它，让我们了解我是否正确输了，接下来我再见
I lost correctly I'll see you guys next

215
00:07:01,319 --> 00:07:06,319
一周再见战争
week goodbye war

