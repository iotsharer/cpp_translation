﻿1
00:00:00,000 --> 00:00:02,079
嘿，大家好，我叫Cherno，今天回到我的c ++系列
hey what's up guys my name is the Cherno

2
00:00:02,279 --> 00:00:04,540
嘿，大家好，我叫Cherno，今天回到我的c ++系列
welcome back to my c++ series today

3
00:00:04,740 --> 00:00:07,118
我们将要研究战利品，因为我特意有循环
we're going to be taking a look at loot

4
00:00:07,318 --> 00:00:08,740
我们将要研究战利品，因为我特意有循环
so as have loops I'm specifically

5
00:00:08,939 --> 00:00:10,660
谈论for循环和while循环，将其简单化
talking about for loops and while loops

6
00:00:10,859 --> 00:00:12,790
谈论for循环和while循环，将其简单化
to put it into simple terms moves are

7
00:00:12,990 --> 00:00:14,740
基本上，当我们想要变形某些东西时，我们可以在我们的代码中写的东西
basically things that we can write in

8
00:00:14,939 --> 00:00:16,600
基本上，当我们想要变形某些东西时，我们可以在我们的代码中写的东西
our code when we want to deform certain

9
00:00:16,800 --> 00:00:18,789
多次操作，所以一个非常简单的例子就是如果我们想
operations multiple times so a really

10
00:00:18,989 --> 00:00:21,100
多次操作，所以一个非常简单的例子就是如果我们想
simple example is what if we wanted to

11
00:00:21,300 --> 00:00:24,249
正确打印彩色世界五次，我们是否只需复制并粘贴该行代码
print color world five times right would

12
00:00:24,449 --> 00:00:26,050
正确打印彩色世界五次，我们是否只需复制并粘贴该行代码
we just copy and paste that line of code

13
00:00:26,250 --> 00:00:28,390
五次，或者将代码放入函数中，然后调用该函数
five times or maybe put that code into a

14
00:00:28,589 --> 00:00:29,859
五次，或者将代码放入函数中，然后调用该函数
function and then call that function

15
00:00:30,059 --> 00:00:32,229
五次，实际上是我们代码中的五次，我们应该那样做吗
five times like literally five times in

16
00:00:32,429 --> 00:00:34,119
五次，实际上是我们代码中的五次，我们应该那样做吗
our code should we do it that way

17
00:00:34,320 --> 00:00:36,518
还是我们可以使用循环说嘿，我希望此代码在其中运行五次
or can we just use a loop to say hey I

18
00:00:36,719 --> 00:00:39,070
还是我们可以使用循环说嘿，我希望此代码在其中运行五次
want this code here to run five times in

19
00:00:39,270 --> 00:00:41,018
连续循环对于大图来说也是很重要的一种，所以想象一下游戏
a row loops are also important kind of

20
00:00:41,219 --> 00:00:43,329
连续循环对于大图来说也是很重要的一种，所以想象一下游戏
for big picture stuff so picture a game

21
00:00:43,530 --> 00:00:45,969
如果您编写了游戏赔率，那么您是想要保持游戏正常运行的权利
right if you've written a game odds are

22
00:00:46,170 --> 00:00:47,858
如果您编写了游戏赔率，那么您是想要保持游戏正常运行的权利
you want to keep the game running right

23
00:00:48,058 --> 00:00:49,358
您不想只喜欢运行游戏并渲染一帧然后退出，那就是
you don't want to just like run the game

24
00:00:49,558 --> 00:00:51,309
您不想只喜欢运行游戏并渲染一帧然后退出，那就是
and render one frame and exit and that's

25
00:00:51,509 --> 00:00:53,649
正确的游戏就像一帧画一样我租了我的画框我要退出了
it right game done like one frame I

26
00:00:53,850 --> 00:00:55,390
正确的游戏就像一帧画一样我租了我的画框我要退出了
rented my frame I'm going to exit I'm

27
00:00:55,590 --> 00:00:57,338
现在完成，您想要保持游戏的实际状态将很奇怪
done now that would be very weird you

28
00:00:57,539 --> 00:00:59,378
现在完成，您想要保持游戏的实际状态将很奇怪
want to keep the game actually playing

29
00:00:59,579 --> 00:01:01,329
因此，您所需要的就是一个游戏循环和一个游戏
and so for that what you need is

30
00:01:01,530 --> 00:01:02,979
因此，您所需要的就是一个游戏循环和一个游戏
something called a game loop and a game

31
00:01:03,179 --> 00:01:04,778
循环本质上是说“嘿”，就像在游戏运行时一样
loop is just something that essentially

32
00:01:04,978 --> 00:01:07,599
循环本质上是说“嘿”，就像在游戏运行时一样
says hey like while the game is running

33
00:01:07,799 --> 00:01:10,119
因此，尽管用户尚未决定退出游戏，请继续更新游戏保持
so while the user hasn't decided to quit

34
00:01:10,319 --> 00:01:12,789
因此，尽管用户尚未决定退出游戏，请继续更新游戏保持
the game keep updating the game keep

35
00:01:12,989 --> 00:01:14,709
渲染游戏，继续移动所有角色，继续做所有事情
rendering the game keep moving all the

36
00:01:14,909 --> 00:01:16,659
渲染游戏，继续移动所有角色，继续做所有事情
characters around keep doing everything

37
00:01:16,859 --> 00:01:18,969
像一遍又一遍地重复一遍，这对于编程非常重要
like over and over again frame by frame

38
00:01:19,170 --> 00:01:21,069
像一遍又一遍地重复一遍，这对于编程非常重要
so it's a very important for programming

39
00:01:21,269 --> 00:01:23,079
他们像每个程序一样使用，就像条件语句一样
they used in like every program right

40
00:01:23,280 --> 00:01:24,819
他们像每个程序一样使用，就像条件语句一样
it's the same thing as like conditionals

41
00:01:25,019 --> 00:01:26,679
这是我们上周在视频中讨论的内容，这是
which we talked about last week video up

42
00:01:26,879 --> 00:01:29,378
这是我们上周在视频中讨论的内容，这是
here that kind of stuff right this is

43
00:01:29,578 --> 00:01:31,119
就像这样，您需要非常了解这些内容，因为您
much like that like you need to know

44
00:01:31,319 --> 00:01:32,799
就像这样，您需要非常了解这些内容，因为您
this stuff really well because you're

45
00:01:33,000 --> 00:01:34,058
实际编写时将其用作主要工具之一
going to be using it as one of your

46
00:01:34,259 --> 00:01:35,619
实际编写时将其用作主要工具之一
primary tools when you actually write

47
00:01:35,819 --> 00:01:37,390
代码，当您编写程序时，首先让我们将其折叠
code when you write a program so first

48
00:01:37,590 --> 00:01:38,980
代码，当您编写程序时，首先让我们将其折叠
of all let's start with fold it the

49
00:01:39,180 --> 00:01:40,569
我之前给出的示例是五次打印色彩世界，
example that I gave earlier was printing

50
00:01:40,769 --> 00:01:42,250
我之前给出的示例是五次打印色彩世界，
color world five times and again we

51
00:01:42,450 --> 00:01:44,140
只需复制并粘贴此日志调用五次即可
could achieve that just by copying and

52
00:01:44,340 --> 00:01:46,448
只需复制并粘贴此日志调用五次即可
pasting this log call five times and if

53
00:01:46,649 --> 00:01:48,308
我运行这段代码，您将看到我们当然可以进行世界打印了
I run this code you're going to see that

54
00:01:48,509 --> 00:01:50,168
我运行这段代码，您将看到我们当然可以进行世界打印了
we of course do get hello world printing

55
00:01:50,368 --> 00:01:52,599
五次进入控制台，让我们的生活更轻松些
to the console five times however to

56
00:01:52,799 --> 00:01:54,069
五次进入控制台，让我们的生活更轻松些
make our lives a little bit easier what

57
00:01:54,269 --> 00:01:55,719
我们可以做的是编写一个for循环for循环，以关键字for开头，然后
we can do is write a for loop for loop

58
00:01:55,920 --> 00:01:57,939
我们可以做的是编写一个for循环for循环，以关键字for开头，然后
to start with the keyword for and then

59
00:01:58,140 --> 00:02:00,640
基本上分为三部分
basically break up into three parts each

60
00:02:00,840 --> 00:02:02,619
基本上分为三部分
of these three parts are separated by a

61
00:02:02,819 --> 00:02:05,289
分号的第一部分是变量声明，因此通常您会声明
semicolon the first part is a variable

62
00:02:05,489 --> 00:02:07,058
分号的第一部分是变量声明，因此通常您会声明
declaration so usually you would declare

63
00:02:07,259 --> 00:02:08,979
这里有某种变量，所以我们要写int我等于0现在
some sort of variable here so we are

64
00:02:09,179 --> 00:02:11,289
这里有某种变量，所以我们要写int我等于0现在
going to write int I equals 0 now the

65
00:02:11,489 --> 00:02:13,300
变量名是我有点习惯，有人说它表示迭代器
variable name being I is a little bit of

66
00:02:13,500 --> 00:02:15,550
变量名是我有点习惯，有人说它表示迭代器
convention some say it says the iterator

67
00:02:15,750 --> 00:02:17,290
当然哪个是相关的，因为我们将遍历此代码，
which of course is relevant can because

68
00:02:17,490 --> 00:02:19,000
当然哪个是相关的，因为我们将遍历此代码，
we'll be iterating over this code you

69
00:02:19,199 --> 00:02:20,560
可以真正将这个变量称为绝对任何东西，因此不必一定是
can really call this variable absolutely

70
00:02:20,759 --> 00:02:22,030
可以真正将这个变量称为绝对任何东西，因此不必一定是
anything at all so doesn't have to be an

71
00:02:22,229 --> 00:02:23,469
整数，并且在向大家展示a之后，它也不必等于零
integer and it also doesn't have to be

72
00:02:23,669 --> 00:02:25,360
整数，并且在向大家展示a之后，它也不必等于零
equal to zero after I show you guys a

73
00:02:25,560 --> 00:02:26,980
简单的for循环，我们将分解一下，看看我们
simple for loop we are going to break

74
00:02:27,180 --> 00:02:28,210
简单的for循环，我们将分解一下，看看我们
the down a little bit and see what we

75
00:02:28,409 --> 00:02:29,950
可以这样做，因为子弹实际上真的非常灵活，您可以
can do because bullets are actually

76
00:02:30,150 --> 00:02:31,689
可以这样做，因为子弹实际上真的非常灵活，您可以
really really flexible and you can

77
00:02:31,889 --> 00:02:33,009
真的很想让他们做几乎所有您想做的事情第二部分是
really like make them do pretty much

78
00:02:33,209 --> 00:02:34,930
真的很想让他们做几乎所有您想做的事情第二部分是
anything you want the second part is a

79
00:02:35,129 --> 00:02:37,000
这个条件基本上说只要这个条件成立，我就去
condition basically this says that as

80
00:02:37,199 --> 00:02:39,009
这个条件基本上说只要这个条件成立，我就去
long as this condition is true I'm going

81
00:02:39,209 --> 00:02:41,290
保持在for循环内执行代码，因此在这种情况下，因为我们想要这样
to keep executing code inside the for

82
00:02:41,490 --> 00:02:43,540
保持在for循环内执行代码，因此在这种情况下，因为我们想要这样
loop so in this case since we want this

83
00:02:43,740 --> 00:02:45,969
执行五次的代码写这个日志颜色的世界，我们想称之为
code to execute five times write this

84
00:02:46,169 --> 00:02:47,530
执行五次的代码写这个日志颜色的世界，我们想称之为
log color world we want to call that

85
00:02:47,729 --> 00:02:49,630
五次我们要写的AI少于五次，我将解释如何
five times we're going to write AI is

86
00:02:49,830 --> 00:02:51,250
五次我们要写的AI少于五次，我将解释如何
less than five and I'll explain how this

87
00:02:51,449 --> 00:02:53,259
在短短的一秒钟内联系在一起，最后一条语句基本上就是
ties in together in just a second the

88
00:02:53,459 --> 00:02:55,689
在短短的一秒钟内联系在一起，最后一条语句基本上就是
last statement is basically code that

89
00:02:55,889 --> 00:02:58,120
将在for循环的下一次迭代之前被调用，因此在这种情况下，我们
will be called before the next iteration

90
00:02:58,319 --> 00:03:00,400
将在for循环的下一次迭代之前被调用，因此在这种情况下，我们
of the for loop so in this case what we

91
00:03:00,599 --> 00:03:02,140
想做的就是写AI plus plus，基本上和我说的是一样的
want to do is write AI plus plus which

92
00:03:02,340 --> 00:03:03,670
想做的就是写AI plus plus，基本上和我说的是一样的
basically is the same thing as saying I

93
00:03:03,870 --> 00:03:07,150
加等于一或我等于加一，这基本上意味着我们要
plus equals one or I equals I plus one

94
00:03:07,349 --> 00:03:08,620
加等于一或我等于加一，这基本上意味着我们要
which basically means that we're going

95
00:03:08,819 --> 00:03:11,500
增加我们的变量，当然从零开始
to increment our variable which starts

96
00:03:11,699 --> 00:03:13,210
增加我们的变量，当然从零开始
at zero of course here we're going to

97
00:03:13,409 --> 00:03:15,640
将其递增一，我们将为此for循环的每一次迭代为其+1
increment it by one we're going to +1 it

98
00:03:15,840 --> 00:03:18,219
将其递增一，我们将为此for循环的每一次迭代为其+1
every single iteration of this for loop

99
00:03:18,419 --> 00:03:19,870
然后我们当然会提供一个基本上只是说
and then of course we provide a body

100
00:03:20,069 --> 00:03:21,700
然后我们当然会提供一个基本上只是说
which is basically just saying that

101
00:03:21,900 --> 00:03:23,590
这个身体中的任何东西都是我们前进的内容，所以这实际上是
anything in this body is the contents of

102
00:03:23,789 --> 00:03:24,939
这个身体中的任何东西都是我们前进的内容，所以这实际上是
our forward so this is actually what's

103
00:03:25,139 --> 00:03:26,530
将要循环这是闭包将执行多次我说
going to be looped this is the closure

104
00:03:26,729 --> 00:03:28,990
将要循环这是闭包将执行多次我说
will be executed multiple times I say

105
00:03:29,189 --> 00:03:30,400
多次，但是这些条件当然会决定
multiple times but of course these

106
00:03:30,599 --> 00:03:31,509
多次，但是这些条件当然会决定
conditions are going to determine

107
00:03:31,709 --> 00:03:34,420
此代码是全部执行还是一次执行，或者可能是一百
whether this code is executed at all or

108
00:03:34,620 --> 00:03:36,130
此代码是全部执行还是一次执行，或者可能是一百
maybe once or it could be a hundred

109
00:03:36,330 --> 00:03:37,990
正确的时间完全取决于这种情况，让我们继续坚持下去
times right it's all depending on this

110
00:03:38,189 --> 00:03:39,700
正确的时间完全取决于这种情况，让我们继续坚持下去
condition let's go ahead and stick one

111
00:03:39,900 --> 00:03:42,640
我们的日志调用进入此处，如果我们点击了，我将删除所有这些多余的日志
of our log calls into here and I'll get

112
00:03:42,840 --> 00:03:44,710
我们的日志调用进入此处，如果我们点击了，我将删除所有这些多余的日志
rid of all of these extra one if we hit

113
00:03:44,909 --> 00:03:46,539
我们的5来运行我们的代码，您会看到我们得到了五次相同的结果hello world
our 5 to run our code you'll see we get

114
00:03:46,739 --> 00:03:48,670
我们的5来运行我们的代码，您会看到我们得到了五次相同的结果hello world
the same result five times hello world

115
00:03:48,870 --> 00:03:50,500
好吧，让我们分解一下这个声明，这是第一件事
ok cool let's break this down a little

116
00:03:50,699 --> 00:03:52,780
好吧，让我们分解一下这个声明，这是第一件事
bit this declaration is the first thing

117
00:03:52,979 --> 00:03:54,730
当我们的指令指针到达此处的这一行代码时，就会发生这种情况
that will happen when our instruction

118
00:03:54,930 --> 00:03:56,080
当我们的指令指针到达此处的这一行代码时，就会发生这种情况
pointer reaches this line of code here

119
00:03:56,280 --> 00:03:59,020
在这种情况下，它将首先声明此处编写的内容
it will first of all declare whatever is

120
00:03:59,219 --> 00:04:00,789
在这种情况下，它将首先声明此处编写的内容
written in here right from this case

121
00:04:00,989 --> 00:04:02,200
我们正在创建一个名为I的新变量，然后它将立即执行
we're creating a new variable called I

122
00:04:02,400 --> 00:04:04,360
我们正在创建一个名为I的新变量，然后它将立即执行
what it will then do is immediately

123
00:04:04,560 --> 00:04:06,160
检查此条件是否为真，如果此条件为真，我们将
check to see if this condition is true

124
00:04:06,360 --> 00:04:08,860
检查此条件是否为真，如果此条件为真，我们将
if this condition is true we're going to

125
00:04:09,060 --> 00:04:11,050
继续跳到我们循环的身体，并能够听到那里的声音
go ahead and jump to the body of our for

126
00:04:11,250 --> 00:04:12,730
继续跳到我们循环的身体，并能够听到那里的声音
loop and able to hear whatever is there

127
00:04:12,930 --> 00:04:14,770
当我们完成执行身体，然后在这里点击这个大括号
when we have finished executing the body

128
00:04:14,969 --> 00:04:16,629
当我们完成执行身体，然后在这里点击这个大括号
and we hit this curly bracket here this

129
00:04:16,829 --> 00:04:18,879
关闭一个，我们将回到这里并执行任何代码
closing one we're going to go back up to

130
00:04:19,079 --> 00:04:20,860
关闭一个，我们将回到这里并执行任何代码
here and perform whatever code is

131
00:04:21,060 --> 00:04:22,060
写在这里，所以在这种情况下增加
written here so in this case were

132
00:04:22,259 --> 00:04:22,889
写在这里，所以在这种情况下增加
incrementing

133
00:04:23,089 --> 00:04:24,689
发生这种情况后，我们将回到条件并检查
by one after that happens we're going to

134
00:04:24,889 --> 00:04:26,460
发生这种情况后，我们将回到条件并检查
go back to the condition and check to

135
00:04:26,660 --> 00:04:28,170
当然现在看看条件是否为真我曾经是零
see if the condition is true now of

136
00:04:28,370 --> 00:04:29,968
当然现在看看条件是否为真我曾经是零
course at this point I used to be zero

137
00:04:30,168 --> 00:04:31,949
现在我们已经染色了一个，所以我们正在检查一个是否小于五
now we've been chromatid by one so now

138
00:04:32,149 --> 00:04:33,389
现在我们已经染色了一个，所以我们正在检查一个是否小于五
we're checking if one is less than five

139
00:04:33,589 --> 00:04:35,129
它当然少于五个，所以我们将跳回此处并执行此操作
it is of course less than five so we're

140
00:04:35,329 --> 00:04:36,749
它当然少于五个，所以我们将跳回此处并执行此操作
going to jump back here and perform this

141
00:04:36,949 --> 00:04:38,520
代码，它将一直继续下去，直到最后我将等于四个
code it's going to keep going until

142
00:04:38,720 --> 00:04:41,189
代码，它将一直继续下去，直到最后我将等于四个
finally I will be equal to four we're

143
00:04:41,389 --> 00:04:43,379
要运行此代码，在此处将I递增一，这使我们有五个
going to run this code jump up here

144
00:04:43,579 --> 00:04:46,020
要运行此代码，在此处将I递增一，这使我们有五个
increment I by one which gives us five

145
00:04:46,220 --> 00:04:48,180
现在寻找配对，如果五个小于五个，但五个不小于
and now looking pairing if five is less

146
00:04:48,379 --> 00:04:49,800
现在寻找配对，如果五个小于五个，但五个不小于
than five however five is not less than

147
00:04:50,000 --> 00:04:52,020
五，因为五等于五，所以这个条件是假的，这意味着我们
five because five is equal to five so

148
00:04:52,220 --> 00:04:53,550
五，因为五等于五，所以这个条件是假的，这意味着我们
this condition is false which means we

149
00:04:53,750 --> 00:04:55,710
现在跳到此处的第10行以执行末端间隙，本质上是
now jump to line ten over here to

150
00:04:55,910 --> 00:04:58,230
现在跳到此处的第10行以执行末端间隙，本质上是
execute end gap and that in essence is

151
00:04:58,430 --> 00:05:00,270
该文件夹现在将如何实际运行五次，我只想强调一下
how that folders will actually run five

152
00:05:00,470 --> 00:05:02,310
该文件夹现在将如何实际运行五次，我只想强调一下
times now I just want to stress that

153
00:05:02,509 --> 00:05:04,259
完美声明的这三个部分正是我所说的
these three parts of this flawless

154
00:05:04,459 --> 00:05:06,900
完美声明的这三个部分正是我所说的
declaration are exactly what I said they

155
00:05:07,100 --> 00:05:08,759
是的，这只是将在for开头执行的代码
are right this is just code that will be

156
00:05:08,959 --> 00:05:10,740
是的，这只是将在for开头执行的代码
executed at the beginning of the for

157
00:05:10,939 --> 00:05:14,579
循环一次，这只是对某种欺凌行为的比较，
loop once only this is a comparison upon

158
00:05:14,779 --> 00:05:16,620
循环一次，这只是对某种欺凌行为的比较，
some kind of bullying that will be

159
00:05:16,819 --> 00:05:18,990
在下一次for循环迭代之前求值，然后这是某种
evaluated before the next iteration of

160
00:05:19,189 --> 00:05:21,389
在下一次for循环迭代之前求值，然后这是某种
the for loop and then this is some kind

161
00:05:21,589 --> 00:05:23,520
死亡看起来像某种代码，将在我们的末尾执行
of death looks like some kind of code

162
00:05:23,720 --> 00:05:26,009
死亡看起来像某种代码，将在我们的末尾执行
that will be executed at the end of our

163
00:05:26,209 --> 00:05:28,079
for循环，所以我能做的就像我可以说的那样
for loop so what I could do is like

164
00:05:28,279 --> 00:05:30,718
for循环，所以我能做的就像我可以说的那样
literally I could say actually in I

165
00:05:30,918 --> 00:05:32,278
等于伊朗，我们将其放在此处并将其完全空白，就像
equals Iran we'll put that over here and

166
00:05:32,478 --> 00:05:34,199
等于伊朗，我们将其放在此处并将其完全空白，就像
leave this completely blank just like

167
00:05:34,399 --> 00:05:36,360
那是完全有效的代码，我也可以说这是我加上我
that right that's perfectly valid code I

168
00:05:36,560 --> 00:05:38,550
那是完全有效的代码，我也可以说这是我加上我
could also say that this I plus plus I'm

169
00:05:38,750 --> 00:05:39,718
实际上只是将其放置在此处，因为这将具有
actually just going to drop it here

170
00:05:39,918 --> 00:05:41,639
实际上只是将其放置在此处，因为这将具有
right because it's that's going to have

171
00:05:41,839 --> 00:05:43,620
同样的结果，即使看起来有点怪异，如果我运行它，我们
the same result and if I run this even

172
00:05:43,819 --> 00:05:45,028
同样的结果，即使看起来有点怪异，如果我运行它，我们
though it looks a little bit weird we're

173
00:05:45,228 --> 00:05:46,259
实际上将获得完全相同的结果，因为我们实际上并没有
actually going to get the exact same

174
00:05:46,459 --> 00:05:47,399
实际上将获得完全相同的结果，因为我们实际上并没有
result because we haven't actually

175
00:05:47,598 --> 00:05:49,800
完全改变了行为代码，我们只是类似地移动了一些东西
changed the behavior code at all we've

176
00:05:50,000 --> 00:05:51,480
完全改变了行为代码，我们只是类似地移动了一些东西
just moved some things around similarly

177
00:05:51,680 --> 00:05:53,100
我可以在这里写一个恶霸，叫做条件或类似的东西
I could write a bully in here called

178
00:05:53,300 --> 00:05:55,170
我可以在这里写一个恶霸，叫做条件或类似的东西
like condition or something set it equal

179
00:05:55,370 --> 00:05:58,189
真实，并把它作为我的实际状况，所以我得到了我的状况
to true and have that to be my actual

180
00:05:58,389 --> 00:06:01,170
真实，并把它作为我的实际状况，所以我得到了我的状况
condition right so got my condition I'm

181
00:06:01,370 --> 00:06:02,670
我要做加，然后我可能会做类似的事情，如果我不小于
going to do I plus plus and then I might

182
00:06:02,870 --> 00:06:07,170
我要做加，然后我可能会做类似的事情，如果我不小于
do something like if I is not less than

183
00:06:07,370 --> 00:06:08,160
我现在将五个条件设置为相等
five anymore

184
00:06:08,360 --> 00:06:10,468
我现在将五个条件设置为相等
right I'm going to set condition equal

185
00:06:10,668 --> 00:06:12,960
为假，如果我运行此代码，您可以看到我仍然会打招呼
to false and if I run this code you can

186
00:06:13,160 --> 00:06:14,218
为假，如果我运行此代码，您可以看到我仍然会打招呼
see that I'm still going to get hello

187
00:06:14,418 --> 00:06:15,540
世界印刷了五次，因为我再也没有改变行为
world printed in five times because

188
00:06:15,740 --> 00:06:16,829
世界印刷了五次，因为我再也没有改变行为
again I haven't changed the behavior of

189
00:06:17,029 --> 00:06:18,810
我的程序我只是以稍微不同的方式重写了代码，但是我
my program I've just rewritten the code

190
00:06:19,009 --> 00:06:20,610
我的程序我只是以稍微不同的方式重写了代码，但是我
in a slightly different way but I'm

191
00:06:20,810 --> 00:06:21,870
告诉您所有这些信息，以便您知道for循环可以执行任何操作
telling you all this so that you know

192
00:06:22,069 --> 00:06:23,490
告诉您所有这些信息，以便您知道for循环可以执行任何操作
that for loops they can do anything

193
00:06:23,689 --> 00:06:25,350
就像你不必总是像十四只i20的眼睛比五只差
right like you don't have to always be

194
00:06:25,550 --> 00:06:27,660
就像你不必总是像十四只i20的眼睛比五只差
like fourteen i20 eyes worse than five

195
00:06:27,860 --> 00:06:29,490
钱加上很无聊的权利，他们可以包含任何类型的句子
bucks plus that's boring right they can

196
00:06:29,689 --> 00:06:31,030
钱加上很无聊的权利，他们可以包含任何类型的句子
contain any kind of sentence

197
00:06:31,230 --> 00:06:32,530
希望您当然还可以调用诸如循环设置之类的函数
want you can also of course call

198
00:06:32,730 --> 00:06:34,840
希望您当然还可以调用诸如循环设置之类的函数
functions in that like for loop setting

199
00:06:35,040 --> 00:06:37,360
任何正确的可能性几乎是无穷无尽的，您也可以摆脱
anything right the possibilities are

200
00:06:37,560 --> 00:06:38,949
任何正确的可能性几乎是无穷无尽的，您也可以摆脱
literally endless you can also get rid

201
00:06:39,149 --> 00:06:40,150
基本上完全意味着同一件事
of the condition entirely which

202
00:06:40,350 --> 00:06:41,500
基本上完全意味着同一件事
basically means that it's the same thing

203
00:06:41,699 --> 00:06:43,449
如写正确的话，这永远不会是假的，如果我运行它，那实际上是
as writing true right this will never be

204
00:06:43,649 --> 00:06:45,819
如写正确的话，这永远不会是假的，如果我运行它，那实际上是
false and if I run this it's actually

205
00:06:46,019 --> 00:06:48,220
将会是一个无休止的循环，它永远不会结束，所以如果我按f5
going to be an endless loop right it's

206
00:06:48,420 --> 00:06:50,379
将会是一个无休止的循环，它永远不会结束，所以如果我按f5
never going to end so if I hit f5 right

207
00:06:50,579 --> 00:06:52,000
现在运行此命令，您将看到我们实际上将继续打印问候
now to run this you'll see that we're

208
00:06:52,199 --> 00:06:53,350
现在运行此命令，您将看到我们实际上将继续打印问候
going to actually keep printing hello

209
00:06:53,550 --> 00:06:55,569
因为我们的程序一直运行，直到我们手动终止它为止
world as our program keeps running until

210
00:06:55,769 --> 00:06:57,160
因为我们的程序一直运行，直到我们手动终止它为止
we manually terminate it that's pretty

211
00:06:57,360 --> 00:06:58,509
对于循环来说，我的意思是非常简单
much it for for loops I mean they're

212
00:06:58,709 --> 00:07:01,079
对于循环来说，我的意思是非常简单
really really simple for doing like

213
00:07:01,279 --> 00:07:03,460
多次运行代码，例如我想打印五次世界
running code multiple times for example

214
00:07:03,660 --> 00:07:05,100
多次运行代码，例如我想打印五次世界
I want to print hello world five times

215
00:07:05,300 --> 00:07:07,240
太棒了，因为它真的很容易设置，也非常有用
fantastic for that really easy to set up

216
00:07:07,439 --> 00:07:09,310
太棒了，因为它真的很容易设置，也非常有用
for that also they're really useful with

217
00:07:09,509 --> 00:07:11,350
数组，当您想进行遍历和线性排列时
arrays when you want to kind of go

218
00:07:11,550 --> 00:07:13,060
数组，当您想进行遍历和线性排列时
through and array kind of linearly like

219
00:07:13,259 --> 00:07:15,970
这对我们接下来要进行的工作非常有用
that as well very useful for that the

220
00:07:16,170 --> 00:07:17,560
这对我们接下来要进行的工作非常有用
next thing that we'll move onto is wild

221
00:07:17,759 --> 00:07:19,780
所以while循环很像for循环，只是它没有这个
it so a while loop is much like a for

222
00:07:19,980 --> 00:07:21,819
所以while循环很像for循环，只是它没有这个
loop it's just that it doesn't have this

223
00:07:22,019 --> 00:07:23,740
开头的声明，结尾的付款只是
kind of statement at the beginning and

224
00:07:23,939 --> 00:07:25,210
开头的声明，结尾的付款只是
this payment at the end is just got a

225
00:07:25,410 --> 00:07:25,750
条件，所以基本上尝试了while循环
condition

226
00:07:25,949 --> 00:07:27,639
条件，所以基本上尝试了while循环
so basically tried a while loop will

227
00:07:27,839 --> 00:07:29,920
写一会儿关键字，然后在里面放入一些条件，
write the keyword while and then inside

228
00:07:30,120 --> 00:07:31,960
写一会儿关键字，然后在里面放入一些条件，
here we put some kind of condition so

229
00:07:32,160 --> 00:07:34,870
例如我不到五岁，有一个例子，我当然会
for example I is less than five right

230
00:07:35,069 --> 00:07:36,490
例如我不到五岁，有一个例子，我当然会
has an example and of course I would

231
00:07:36,689 --> 00:07:39,069
在这里声明，所以如果我们运行它，它将基本上执行任何代码
declared up here so if we run this this

232
00:07:39,269 --> 00:07:40,930
在这里声明，所以如果我们运行它，它将基本上执行任何代码
will basically perform whatever code is

233
00:07:41,129 --> 00:07:43,120
只要我不到五岁，那么如果我们要重写for循环
in here as long as I is less than five

234
00:07:43,319 --> 00:07:45,250
只要我不到五岁，那么如果我们要重写for循环
so if we were to rewrite our for loop

235
00:07:45,449 --> 00:07:46,689
让我回到原来的样子
let me just bring back to follows to

236
00:07:46,889 --> 00:07:48,430
让我回到原来的样子
what it was originally

237
00:07:48,629 --> 00:07:50,470
如果我们要在这里基本复制此for循环，我们将
if we if we wanted to basically

238
00:07:50,670 --> 00:07:53,920
如果我们要在这里基本复制此for循环，我们将
replicate this for loop in here we would

239
00:07:54,120 --> 00:07:56,110
必须在这里写入I等于0，然后在末尾加上I加号
have to write into I equals 0 out here

240
00:07:56,310 --> 00:07:58,810
必须在这里写入I等于0，然后在末尾加上I加号
and then do the I plus plus at the end

241
00:07:59,009 --> 00:08:01,420
的循环，所以在这里，然后现在，我们在这里实际上是
of the loop so over here right and then

242
00:08:01,620 --> 00:08:03,310
的循环，所以在这里，然后现在，我们在这里实际上是
right now what we have here is actually

243
00:08:03,509 --> 00:08:05,199
我将要打印的这些完全相同
exactly the same I'm just going to in

244
00:08:05,399 --> 00:08:06,460
我将要打印的这些完全相同
between these I'm just going to print

245
00:08:06,660 --> 00:08:08,020
就像一堆等号只是将两个语句分开，但是如果我
like a whole bunch of equal signs just

246
00:08:08,220 --> 00:08:10,569
就像一堆等号只是将两个语句分开，但是如果我
to separate be two statements but if I

247
00:08:10,769 --> 00:08:12,250
运行此命令，您可以看到我们从两个循环中都得到了相同的结果
run this you can see that we get the

248
00:08:12,449 --> 00:08:13,960
运行此命令，您可以看到我们从两个循环中都得到了相同的结果
same results from both loops right there

249
00:08:14,160 --> 00:08:16,270
都打印了五次hello world，所以为什么使用while循环而不是for
both printing hello world five times so

250
00:08:16,470 --> 00:08:18,009
都打印了五次hello world，所以为什么使用while循环而不是for
why use a while loop instead of a for

251
00:08:18,209 --> 00:08:19,900
循环或类似什么时候，您基本上会使用for循环而不是while循环
loop or like when would you use a for

252
00:08:20,100 --> 00:08:21,879
循环或类似什么时候，您基本上会使用for循环而不是while循环
loop instead of a while loop basically

253
00:08:22,079 --> 00:08:23,889
取决于您是否需要变量，因为这些循环是
it comes down to whether or not you need

254
00:08:24,089 --> 00:08:25,629
取决于您是否需要变量，因为这些循环是
a variable right because these loops are

255
00:08:25,829 --> 00:08:26,980
完全相同，您可以互换使用它们，这更是一个问题
identical you can use them

256
00:08:27,180 --> 00:08:29,290
完全相同，您可以互换使用它们，这更是一个问题
interchangeably right it's more a matter

257
00:08:29,490 --> 00:08:31,750
惯例或风格问题，而不是实际的卡片规则，因为再次
of convention or a matter of style than

258
00:08:31,949 --> 00:08:34,718
惯例或风格问题，而不是实际的卡片规则，因为再次
it is an actual card rule because again

259
00:08:34,918 --> 00:08:36,549
就像他们可以做的两个循环之间没有实际的区别
like there's no actual difference

260
00:08:36,750 --> 00:08:37,870
就像他们可以做的两个循环之间没有实际的区别
between the two loops they can do

261
00:08:38,070 --> 00:08:40,839
如果您想要他们完全相同的事情，但是约定是
exactly the same thing if you so want

262
00:08:41,039 --> 00:08:42,459
如果您想要他们完全相同的事情，但是约定是
want them to but the conventions are

263
00:08:42,659 --> 00:08:44,529
基本上，如果您有一定的能力可以做到，您已经想要
basically if you have a certain can do

264
00:08:44,730 --> 00:08:46,508
基本上，如果您有一定的能力可以做到，您已经想要
that already exists that you want to

265
00:08:46,708 --> 00:08:48,549
只是一种比较，例如，我提到整个游戏
just kind of compare so for example I

266
00:08:48,750 --> 00:08:50,258
只是一种比较，例如，我提到整个游戏
mentioned that whole game would example

267
00:08:50,458 --> 00:08:52,179
您有一个名为running的变量，您可能想要保留该变量
where you had a variable called running

268
00:08:52,379 --> 00:08:54,578
您有一个名为running的变量，您可能想要保留该变量
and you might want to kind of keep that

269
00:08:54,778 --> 00:08:56,378
只要运行变量为真，游戏循环就会继续，因为运行等于
game loop going as long as that running

270
00:08:56,578 --> 00:08:58,240
只要运行变量为真，游戏循环就会继续，因为运行等于
variable is true because running equals

271
00:08:58,440 --> 00:08:59,828
如果为true，则表示您的程序仍在运行
true basically means that your program

272
00:09:00,028 --> 00:09:01,448
如果为true，则表示您的程序仍在运行
is still running if you wanted to do

273
00:09:01,649 --> 00:09:02,620
我可能会使用while循环，因为我们需要的只是一个
something like that I would probably use

274
00:09:02,820 --> 00:09:04,240
我可能会使用while循环，因为我们需要的只是一个
a while loop because all we need is a

275
00:09:04,440 --> 00:09:05,559
条件，我们不需要在每次迭代后更改条件
condition we don't need to be changing

276
00:09:05,759 --> 00:09:07,299
条件，我们不需要在每次迭代后更改条件
the condition after every iteration we

277
00:09:07,500 --> 00:09:08,948
无需声明条件即可开始循环，它已经是
don't need to declare the condition to

278
00:09:09,149 --> 00:09:10,328
无需声明条件即可开始循环，它已经是
start off the loop it's already a

279
00:09:10,528 --> 00:09:11,919
我们可以使用的变量或可以使用的函数调用
variable that we've got or a function

280
00:09:12,120 --> 00:09:13,899
我们可以使用的变量或可以使用的函数调用
call that we can use and that's just

281
00:09:14,100 --> 00:09:15,490
完全没问题，这实际上不是我们所拥有的
going to be completely like fine right

282
00:09:15,690 --> 00:09:16,929
完全没问题，这实际上不是我们所拥有的
it's not something that we actually have

283
00:09:17,129 --> 00:09:19,209
保持某种更新或最初设置这些东西的权利
to keep kind of updating or set up

284
00:09:19,409 --> 00:09:20,799
保持某种更新或最初设置这些东西的权利
initially none of that stuff right

285
00:09:21,000 --> 00:09:22,419
而如果我想遍历一个数组，并且数组有一定的大小
whereas if I wanted to go through an

286
00:09:22,620 --> 00:09:24,519
而如果我想遍历一个数组，并且数组有一定的大小
array and the array had a certain size

287
00:09:24,720 --> 00:09:26,559
例如我的数组有十个元素，那么我将使用以下内容，因为
for example my array with ten elements

288
00:09:26,759 --> 00:09:28,479
例如我的数组有十个元素，那么我将使用以下内容，因为
long then I will use as follows because

289
00:09:28,679 --> 00:09:30,578
首先，我想准确运行该代码十次，所以我想保持
first of all I want to run that code

290
00:09:30,778 --> 00:09:32,349
首先，我想准确运行该代码十次，所以我想保持
exactly ten times so I want to keep

291
00:09:32,549 --> 00:09:34,448
跟踪某种变量，只运行该循环十次，
track of some kind of variable and only

292
00:09:34,649 --> 00:09:36,789
跟踪某种变量，只运行该循环十次，
run that loop ten times but also that

293
00:09:36,990 --> 00:09:38,529
变量对于访问该数组中的元素很有用，因为如果我
variable will be useful for accessing

294
00:09:38,730 --> 00:09:41,139
变量对于访问该数组中的元素很有用，因为如果我
elements inside that array because if I

295
00:09:41,339 --> 00:09:43,059
要访问十元素数组中的每个元素，我需要某种偏移量
want to access every element in a ten

296
00:09:43,259 --> 00:09:45,758
要访问十元素数组中的每个元素，我需要某种偏移量
element array I need some kind of offset

297
00:09:45,958 --> 00:09:47,919
这样我就可以使用索引，以便可以访问该数组中的元素，
so that I can like an index so that I

298
00:09:48,120 --> 00:09:50,349
这样我就可以使用索引，以便可以访问该数组中的元素，
can access an element in that array and

299
00:09:50,549 --> 00:09:51,368
当然，我们将来会讨论数组，但是我会变
of course we're going to talk about

300
00:09:51,568 --> 00:09:53,948
当然，我们将来会讨论数组，但是我会变
arrays in the future but that I variable

301
00:09:54,149 --> 00:09:55,809
我们恰好正在跟踪，它将以0 1 2 3 4 5作为
that we happen to be tracking and it's

302
00:09:56,009 --> 00:09:57,758
我们恰好正在跟踪，它将以0 1 2 3 4 5作为
going to go 0 1 2 3 4 5 as our

303
00:09:57,958 --> 00:09:59,049
迭代将是绝对完美的，所以我们
iterations go on is going to be

304
00:09:59,250 --> 00:10:01,269
迭代将是绝对完美的，所以我们
absolutely perfect for that so we've got

305
00:10:01,470 --> 00:10:02,618
4，虽然我们有一段时间，但实际上还有一个循环，
4 and we've got while but there is

306
00:10:02,818 --> 00:10:04,359
4，虽然我们有一段时间，但实际上还有一个循环，
actually one more loop that we have and

307
00:10:04,559 --> 00:10:06,008
这就是所谓的“ do-while”循环，现在这些对我来说不是那么有用
it's called a do-while loop now these

308
00:10:06,208 --> 00:10:08,078
这就是所谓的“ do-while”循环，现在这些对我来说不是那么有用
aren't that useful I don't personally

309
00:10:08,278 --> 00:10:11,139
经常使用这些，但它们确实存在，尽管有一定用途
use these that often but they are there

310
00:10:11,339 --> 00:10:13,659
经常使用这些，但它们确实存在，尽管有一定用途
and they do have some uses although

311
00:10:13,860 --> 00:10:15,490
再次，您将不会在跟随或while循环的任何地方看到这些
again you won't be seeing these anywhere

312
00:10:15,690 --> 00:10:17,889
再次，您将不会在跟随或while循环的任何地方看到这些
near as much as follows or while loops

313
00:10:18,089 --> 00:10:19,809
所以基本上我们上升到做关键字，然后我们有一个身体，然后在最后
so basically we rise to do keyword and

314
00:10:20,009 --> 00:10:21,789
所以基本上我们上升到做关键字，然后我们有一个身体，然后在最后
then we have a body and then at the end

315
00:10:21,990 --> 00:10:23,769
我们写的时候，我们有某种条件，所以例如，我再次
we write while and we have some kind of

316
00:10:23,970 --> 00:10:25,899
我们写的时候，我们有某种条件，所以例如，我再次
condition so for example again I'll just

317
00:10:26,100 --> 00:10:27,729
眼睛小于5，do-while循环和一会之间的唯一区别
do eyes less than 5 the only difference

318
00:10:27,929 --> 00:10:29,318
眼睛小于5，do-while循环和一会之间的唯一区别
between the do-while loop and a while

319
00:10:29,519 --> 00:10:32,019
循环是，无论什么正确，此主体将至少执行一次
loop is that this body will be executed

320
00:10:32,220 --> 00:10:35,078
循环是，无论什么正确，此主体将至少执行一次
at least once no matter what right so

321
00:10:35,278 --> 00:10:37,539
例如，如果我们将其更改为I情况，那么实际上
for example if we if we change this into

322
00:10:37,740 --> 00:10:40,149
例如，如果我们将其更改为I情况，那么实际上
I situation so that instead we actually

323
00:10:40,350 --> 00:10:42,549
只是有一个条件，如果我坚持那个条件，可能等于解决
just have a condition and it might be

324
00:10:42,750 --> 00:10:45,128
只是有一个条件，如果我坚持那个条件，可能等于解决
equal to solve if I stick that condition

325
00:10:45,328 --> 00:10:47,649
在这里，这实际上就像一个if语句
in here this while is actually going to

326
00:10:47,850 --> 00:10:50,498
在这里，这实际上就像一个if语句
function much like an if statement in a

327
00:10:50,698 --> 00:10:53,169
感觉到如果这是错误的话，那就永远不会在这里运行代码
sense that if this is false well then

328
00:10:53,370 --> 00:10:54,818
感觉到如果这是错误的话，那就永远不会在这里运行代码
it's never going to run the code in here

329
00:10:55,019 --> 00:10:57,490
但是，do-while循环即使是这样也是错误的，如果这样
however a do-while loop even

330
00:10:57,690 --> 00:10:59,319
但是，do-while循环即使是这样也是错误的，如果这样
this is false right if it is such a

331
00:10:59,519 --> 00:11:01,329
条件调节器故障，它将像没有循环一样起作用
condition conditioners fault it will

332
00:11:01,529 --> 00:11:02,529
条件调节器故障，它将像没有循环一样起作用
function as if it's not a loop it's

333
00:11:02,730 --> 00:11:04,389
要在这里运行代码，最后要看一下电线
going to run the code here one it's

334
00:11:04,590 --> 00:11:05,740
要在这里运行代码，最后要看一下电线
going to come down to the wire look at

335
00:11:05,940 --> 00:11:07,059
条件是条件错误，所以我不会循环，那就是
the condition are the conditions false

336
00:11:07,259 --> 00:11:08,649
条件是条件错误，所以我不会循环，那就是
so I'm not going to loop and that's

337
00:11:08,850 --> 00:11:10,120
这就是while循环正常工作的方式，所以这差不多
pretty much it that's how do while loops

338
00:11:10,320 --> 00:11:11,620
这就是while循环正常工作的方式，所以这差不多
work alright so that's pretty much it

339
00:11:11,820 --> 00:11:12,370
对于今天的视频，希望你们喜欢非常基础的
for today's video

340
00:11:12,570 --> 00:11:14,109
对于今天的视频，希望你们喜欢非常基础的
hope you guys enjoy very basic

341
00:11:14,309 --> 00:11:15,490
循环的简介在整个系列中我们将大量使用这些循环
introduction to loops we're going to be

342
00:11:15,690 --> 00:11:17,019
循环的简介在整个系列中我们将大量使用这些循环
using these a lot throughout the series

343
00:11:17,220 --> 00:11:19,449
它们几乎用在每个您可以想到的循环算法中
they're used in like pretty much every

344
00:11:19,649 --> 00:11:21,519
它们几乎用在每个您可以想到的循环算法中
algorithm you can think of loops are

345
00:11:21,720 --> 00:11:23,169
当我们谈论数组时，它非常有用，我们将谈论如何
extremely useful when we talk about our

346
00:11:23,370 --> 00:11:24,819
当我们谈论数组时，它非常有用，我们将谈论如何
arrays we'll be talking about how we can

347
00:11:25,019 --> 00:11:26,559
使用叶子来访问数组和所有其他东西，以便一切都可以配合
use foliage to access arrays and all

348
00:11:26,759 --> 00:11:28,299
使用叶子来访问数组和所有其他东西，以便一切都可以配合
that stuff so everything will tie in

349
00:11:28,500 --> 00:11:30,909
很好，将来我会更深入地研究它们
nicely I'm going to do a more in-depth

350
00:11:31,110 --> 00:11:32,439
很好，将来我会更深入地研究它们
look at them sometime in the future

351
00:11:32,639 --> 00:11:33,609
我们实际上将要查看反汇编并查看实际
where we're actually going to look at

352
00:11:33,809 --> 00:11:35,529
我们实际上将要查看反汇编并查看实际
the disassembly and look at the actual

353
00:11:35,730 --> 00:11:37,389
我为循环生成的CPU指令让我更深入地了解了我
CPU instructions that get generated for

354
00:11:37,590 --> 00:11:38,979
我为循环生成的CPU指令让我更深入地了解了我
loops and take a bit of a deeper look I

355
00:11:39,179 --> 00:11:40,389
只是不想让这部影片太复杂，所以请深入了解
just didn't want to make this video too

356
00:11:40,590 --> 00:11:42,399
只是不想让这部影片太复杂，所以请深入了解
complicated so the link to that in depth

357
00:11:42,600 --> 00:11:43,870
该视频将在说明中的某个位置
video will be somewhere in the

358
00:11:44,070 --> 00:11:45,189
该视频将在说明中的某个位置
description when that video actually

359
00:11:45,389 --> 00:11:46,809
出来，我希望你们喜欢这个视频，如果你喜欢，请点击
comes out I hope you guys enjoy this

360
00:11:47,009 --> 00:11:48,159
出来，我希望你们喜欢这个视频，如果你喜欢，请点击
video if you did please hit that like

361
00:11:48,360 --> 00:11:49,959
按钮，您可以在Twitter和Instagram上关注我，如果您真的喜欢这个
button you can follow me on Twitter and

362
00:11:50,159 --> 00:11:52,629
按钮，您可以在Twitter和Instagram上关注我，如果您真的喜欢这个
Instagram and if you really like this

363
00:11:52,830 --> 00:11:53,799
系列，您想看更多剧集，并且想要支持该系列，
series and you want to see more episodes

364
00:11:54,000 --> 00:11:55,899
系列，您想看更多剧集，并且想要支持该系列，
and you want to support the series you

365
00:11:56,100 --> 00:11:57,759
可以支持我在patreon上回家大幅削减演出，我会见到你
can support me on patreon back home for

366
00:11:57,960 --> 00:12:00,159
可以支持我在patreon上回家大幅削减演出，我会见到你
slash the show now and I will see you

367
00:12:00,360 --> 00:12:03,320
下次再见
next time goodbye

368
00:12:03,350 --> 00:12:08,590
[音乐]
[Music]

369
00:12:12,909 --> 00:12:17,909
[音乐]
[Music]

