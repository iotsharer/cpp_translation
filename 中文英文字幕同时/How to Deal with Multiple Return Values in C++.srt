﻿1
00:00:00,000 --> 00:00:04,059
hey what's up guys my name is a China welcome back to my C++ series so this
嘿，大家好我叫C ++系列，欢迎中国来

2
00:00:04,259 --> 00:00:09,010
episode we're going to talk about basically what a tuple is what a pair is
这集我们将主要讨论一个元组是一对

3
00:00:09,210 --> 00:00:13,809
how you can deal with multiple return types in C++ and how I personally like
如何在C ++中处理多种返回类型以及我个人的喜好

4
00:00:14,009 --> 00:00:18,400
to deal with them as well this video is kind of stemming off a bit of a spin-off
还要与他们打交道这个视频有点像衍生产品

5
00:00:18,600 --> 00:00:22,359
for us from the latest OpenGL video where we basically talked about shaders
在最新的OpenGL视频中为我们介绍了基本的着色器

6
00:00:22,559 --> 00:00:26,230
and how we can read shedders in a more convenient way check that out up there
以及我们如何以更方便的方式阅读脱皮机，检查一下

7
00:00:26,429 --> 00:00:29,019
it's a great series as well where we talk all about graphics programming and
这也是一个很棒的系列，我们讨论图形编程和

8
00:00:29,219 --> 00:00:32,259
how you can make your own graphics and and stuff like that from scratch so if
如何从头开始制作自己的图形和类似的东西，所以如果

9
00:00:32,460 --> 00:00:34,719
you guys haven't if you guys are interested in that then you're just
你们没有，如果你们对此感兴趣，那么您就是

10
00:00:34,920 --> 00:00:37,869
following this C++ series definitely definitely check out that
遵循这个C ++系列绝对可以肯定地看到

11
00:00:38,070 --> 00:00:42,729
opengl series I hear it's pretty awesome anyway let's talk about this so this is
 opengl系列我听说它真棒，让我们谈谈这是

12
00:00:42,929 --> 00:00:47,799
a bit of a real-world example and I like this because we're finally kind of at
有点真实的例子，我喜欢这个，因为我们终于有点

13
00:00:48,000 --> 00:00:52,838
the point where I don't have to make up weird kind of examples on the spot and
在这一点上，我不必当场制作怪异的例子， 

14
00:00:53,039 --> 00:00:56,198
they're usually not very good I can kind of start showing you guys existing code
他们通常不是很好我可以开始向大家展示现有代码

15
00:00:56,399 --> 00:01:00,788
real code pretty much and we can talk about certain decisions that we can make
真正的代码，我们可以谈论我们可以做出的某些决策

16
00:01:00,988 --> 00:01:05,079
as as engineers as programmers as whatever as developers whatever want to
像工程师一样像程序员一样，像开发者一样想要

17
00:01:05,280 --> 00:01:08,500
call yourself really so specifically here we had an issue
真的这么称呼自己，我们遇到了一个问题

18
00:01:08,700 --> 00:01:12,700
where we had a function and we needed to return two strings there are a lot of
我们有一个函数，需要返回两个字符串，其中有很多

19
00:01:12,900 --> 00:01:17,019
different ways that we can achieve kinda returning two types obviously in C++ by
在C ++中，我们可以通过不同方式明显地返回两种类型

20
00:01:17,219 --> 00:01:21,549
default you can't really return like two types in Python you can kind of
默认情况下，您实际上无法像Python中的两种类型那样返回

21
00:01:21,750 --> 00:01:25,179
automatically automagically I'll say return two types it does some stuff
自动自动地我会说返回两种类型的东西

22
00:01:25,379 --> 00:01:28,899
behind the scenes to support that but traditionally when you have a function a
在后台支持该功能，但是传统上，当您具有某个功能时， 

23
00:01:29,099 --> 00:01:34,659
function can return one type right you can return specifically one variable so
函数可以返回一种类型，就可以专门返回一个变量，所以

24
00:01:34,859 --> 00:01:40,869
if we have something like a function a function that needs to return an integer
如果我们有类似函数之类的函数，则该函数需要返回整数

25
00:01:41,069 --> 00:01:44,528
and a string or something like that we might be in trouble
还有一个字符串或类似的东西，我们可能会遇到麻烦

26
00:01:44,728 --> 00:01:49,179
because well we can maybe return an integer or a string but we definitely
因为我们可以返回整数或字符串，但是我们绝对

27
00:01:49,379 --> 00:01:54,250
can't return both if we have to if we have a function that needs to return two
如果必须的话，不能返回两个，如果我们有一个需要返回两个的函数

28
00:01:54,450 --> 00:01:59,439
variables two or more variables of the same type we can return like a vector or
变量我们可以像矢量一样返回两个或多个相同类型的变量

29
00:01:59,640 --> 00:02:03,939
an array or something like that just containing those two elements that's
数组或类似的东西仅包含这两个元素

30
00:02:04,140 --> 00:02:08,410
also not the best thing to do though for a few reasons and we'll check that out
出于某些原因，这也不是最好的选择，我们会检查一下

31
00:02:08,610 --> 00:02:12,020
in a minute but really we have this issue where hey
一分钟后，但实际上我们遇到了这个问题

32
00:02:12,219 --> 00:02:16,730
you know how do we return how do we return to different variables maybe we
你知道我们如何返回我们如何返回不同的变量也许我们

33
00:02:16,930 --> 00:02:19,670
do have an integer in a string or something like that how do we deal with
在字符串或类似的东西中确实有一个整数，我们该如何处理

34
00:02:19,870 --> 00:02:24,170
that and see if I suppose gives us a few ways to deal with it I personally hate
看看我是否想给我们一些解决方法，我个人讨厌

35
00:02:24,370 --> 00:02:31,069
like all of them there and I like to deal with it in kind of my own way all
像那里的所有人一样，我喜欢以自己的方式处理一切

36
00:02:31,269 --> 00:02:36,140
right so wait anyway sorry um gonna be professional right I mean do you like do
对，所以无论如何都要等抱歉，我会成为专业人士对，我是说你喜欢吗

37
00:02:36,340 --> 00:02:39,410
you guys like this new kind of casual setup or ever sit on the couch here and
你们喜欢这种新型的休闲装置，或者坐在这里的沙发上， 

38
00:02:39,610 --> 00:02:44,210
just I like it I think it's cool cuz I can just kind of have a chat to you guys
我喜欢它，我觉得这很酷，因为我可以跟大家聊天

39
00:02:44,409 --> 00:02:48,050
and it's a lot more kind of like almost like a podcast actually to be honest
老实说，这有点像播客

40
00:02:48,250 --> 00:02:52,099
with you anyway let's not get too casual let's continue talking about what we'll
无论如何，都不要和你在一起，让我们继续谈论我们将要做什么

41
00:02:52,299 --> 00:02:56,390
be talking about so I'll just show you guys a real-world example and stop
在谈论，所以我只给大家展示一个真实的例子，然后停下来

42
00:02:56,590 --> 00:02:59,509
talking so that you know what I mean so if we flip over flip on over to our
说话，以便您了解我的意思，所以如果我们翻过来翻过来

43
00:02:59,709 --> 00:03:03,890
code from the last episode of the OpenGL series we have this function here called
 OpenGL系列最后一集中的代码，我们在这里将此功能称为

44
00:03:04,090 --> 00:03:09,050
past shader right this needs to return two strengths because we actually what
过去的着色器，这需要返回两个优势，因为我们实际上

45
00:03:09,250 --> 00:03:12,410
we build up two strings here the way that I've chosen to solve it in the
我们在这里建立了两个字符串，这是我在解决方案中选择的方式

46
00:03:12,610 --> 00:03:15,530
OpenGL series is my favorite way to kind of solve these things and that's by
 OpenGL系列是我最喜欢的解决这类问题的方法，那就是

47
00:03:15,729 --> 00:03:19,039
actually creating a struct called shader program sauce which just contains the
实际上创建了一个名为shader program sauce的结构，其中仅包含

48
00:03:19,239 --> 00:03:22,280
two strings obviously if we wanted to also return an integer or something that
很明显，如果我们还想返回一个整数或一些

49
00:03:22,479 --> 00:03:25,310
was a different type we could just add it to destruct and return it and I'll
是另一种类型，我们可以将其添加到销毁并返回它，我将

50
00:03:25,509 --> 00:03:29,539
tell you why that's my favorite way kind of at the end of this video really but
告诉你为什么这是我最喜欢的方式，在视频的结尾，但是

51
00:03:29,739 --> 00:03:33,080
if we if we talk about all the different ways that the C++ actually provides us
如果我们谈论C ++实际为我们提供的所有不同方式

52
00:03:33,280 --> 00:03:36,409
with to deal with our let's just get rid of this unless let's kind of deal with
来处理我们，让我们摆脱它，除非让我们来处理

53
00:03:36,609 --> 00:03:40,700
one way that isn't specific to any kind of class that exists in C++
一种不特定于C ++中存在的类的方式

54
00:03:40,900 --> 00:03:44,330
I'm going to make the return type void and what I'm actually going to do is
我将使返回类型无效，而我实际上要做的是

55
00:03:44,530 --> 00:03:49,189
simply take in two strings as reference so string you know you know vertex
只需将两个字符串作为参考，这样就可以知道顶点

56
00:03:49,389 --> 00:03:56,509
source or whatever and then sed string fragment source now because these are
源或其他任何东西，然后现在sed字符串片段源，因为这些是

57
00:03:56,709 --> 00:04:01,069
kind of parameters that are passed by reference we're allowed to actually
通过引用传递的那种参数，我们实际上可以

58
00:04:01,269 --> 00:04:04,099
write into them well more specifically at the end here instead of returning
在这里末尾更具体地写给他们，而不是返回

59
00:04:04,299 --> 00:04:09,469
something we can actually basically just set vertex source equal to SS zero adult
实际上，我们基本上可以将顶点源设置为等于SS 0成人

60
00:04:09,669 --> 00:04:12,379
string now you really need to know much about this let me just quickly write the
字符串，现在您真的需要了解很多，让我快速写一下

61
00:04:12,579 --> 00:04:15,170
stuff for people who haven't washed the open shell video and have no idea what's
那些没洗过开壳视频并且不知道是什么的人的东西

62
00:04:15,370 --> 00:04:19,370
going on we basically have two things to deal with we have the vertex source code
继续，我们基本上要处理两件事，我们有了顶点源代码

63
00:04:19,569 --> 00:04:23,520
which is SS 0 STR and then we have the fragment shader
这是SS 0 STR，然后有片段着色器

64
00:04:23,720 --> 00:04:27,569
source code so I'll just write that like this okay so basically just think of
源代码，所以我将像这样写，好吧，基本上，请考虑一下

65
00:04:27,769 --> 00:04:30,810
this as we need to return two strings these these are the two strings when you
这是因为我们需要返回两个字符串，这是当您

66
00:04:31,009 --> 00:04:35,280
to return so what we could do is set vertex source which is our actual
返回，所以我们可以做的是设置顶点源，这是我们实际的

67
00:04:35,480 --> 00:04:41,699
parameter right we could set that equal to vs which is our vertex shader source
参数正确，我们可以将其设置为vs，这是我们的顶点着色器源

68
00:04:41,899 --> 00:04:46,400
code and then F and then for a fragment source we can set this equal to
代码，然后按F，然后对于片段源，可以将其设置为等于

69
00:04:46,600 --> 00:04:51,509
now FS variable just like that and then kind of when we call the function which
现在FS变量就是这样，然后当我们调用该函数时

70
00:04:51,709 --> 00:04:54,930
is over here and we need to actually get the strings out obviously this now
在这里，我们现在需要实际上将弦乐取出来

71
00:04:55,129 --> 00:04:58,889
returns void so I'll get rid of this assignment here and then for path shader
返回void，所以我将在这里摆脱此分配，然后使用路径着色器

72
00:04:59,089 --> 00:05:03,449
I'm gonna make two strings CS and FS and then just basically pass them in like
我要制作两个字符串CS和FS，然后基本上像这样传递它们

73
00:05:03,649 --> 00:05:06,210
that okay and then we're done and then for
好的，然后我们就完成了，然后

74
00:05:06,410 --> 00:05:09,509
this create a function we can obviously just provide these two strings and we're
这创建了一个函数，我们显然可以只提供这两个字符串， 

75
00:05:09,709 --> 00:05:13,850
all good to go so that's one way we could go about this now it's not like
一切都很好，所以这是我们现在可以解决这个问题的一种方式， 

76
00:05:14,050 --> 00:05:19,230
definitely speaking this is probably one of the most optimal ways to do this
绝对可以说这可能是实现此目的的最佳方法之一

77
00:05:19,430 --> 00:05:23,310
because there's no like string copying we do reassign the string and there are
因为没有类似的字符串复制，所以我们确实重新分配了字符串，并且有

78
00:05:23,509 --> 00:05:26,759
some kind of issues with this but performance wise this is pretty good
与此相关的一些问题，但是在性能方面，这是非常好的

79
00:05:26,959 --> 00:05:30,660
because if you take a look at this week we actually construct the string in the
因为如果您看一下本周，我们实际上是在

80
00:05:30,860 --> 00:05:35,310
stack frame of the parent function which is the main function and then we just
父函数的堆栈框架是主要功能，然后我们只是

81
00:05:35,509 --> 00:05:39,270
pass in basically a pointer to those two things and say hey when you when you
基本上传递指向这两件事的指针，然后当您说“嘿”时

82
00:05:39,470 --> 00:05:42,629
make your string write it into this memory so it's kind of pre allocated
使您的字符串将其写入此内存，以便对其进行预分配

83
00:05:42,829 --> 00:05:47,550
memory pass shader doesn't do any kind of actual dynamic memory allocation of
内存传递着色器不执行任何类型的实际动态内存分配

84
00:05:47,750 --> 00:05:51,449
course when we do assign the actual string which happens over here it does
当然，当我们分配发生在此处的实际字符串时，它确实

85
00:05:51,649 --> 00:05:55,889
need to kind of do some kind of dynamic dynamic memory allocation because it
需要做某种动态动态内存分配，因为它

86
00:05:56,089 --> 00:05:59,910
needs to actually well copy this string into this string so there is still a
实际上需要将此字符串复制到此字符串中，所以仍然存在

87
00:06:00,110 --> 00:06:03,660
copy and all that I don't want to talk too much about performance in this video
复制，以及我不想过多谈论此视频中的效果

88
00:06:03,860 --> 00:06:06,028
just here and though I love talking about it on a LOF talking about
就在这里，尽管我喜欢在LOF上谈论它

89
00:06:06,228 --> 00:06:10,889
optimization just because this is more or less I guess syntactically and as an
仅仅因为这在语法上或多或少是我猜的

90
00:06:11,089 --> 00:06:16,710
actual programming style how to deal with multiple or ten times so we cut the
实际的编程风格如何处理多次或十次，因此我们削减了

91
00:06:16,910 --> 00:06:21,480
optimization before must talk right here so from kind of a code point of view
从代码角度来看，优化之前必须在这里谈

92
00:06:21,680 --> 00:06:24,900
it's not bad it's a little bit clunky obviously we kind of kind of fit this on
不错，有点笨重，显然我们有点适合

93
00:06:25,100 --> 00:06:27,460
one line we need to declare these two strings and then pass
一行，我们需要声明这两个字符串，然后传递

94
00:06:27,660 --> 00:06:31,120
I mean like that not the best thing to do to make it a little bit more explicit
我的意思是，这样做并不是使它更加明确的最佳做法

95
00:06:31,319 --> 00:06:35,530
you could also make these actual pointers the benefit here is that you
您也可以使这些实际的指针在这里的好处是您

96
00:06:35,730 --> 00:06:39,639
can actually with a reference you need to pass in a valid variable right with a
实际上可以与一个引用，您需要使用

97
00:06:39,839 --> 00:06:42,430
pointer you can just pass it annulled or something like that
指针，您可以将其传递为空或类似的东西

98
00:06:42,629 --> 00:06:46,360
and then just do a check to say hey if vs is specified or rather if vertex
然后检查一下是否指定了vs，或者是否指定了顶点

99
00:06:46,560 --> 00:06:49,600
sauce which is our output parameter one more thing I'll quickly add is that I
调味料，这是我们的输出参数，我要快速添加的另一件事是， 

100
00:06:49,800 --> 00:06:54,340
like to name output parameters like out or something like that you know just
喜欢命名输出参数，例如out或类似的东西， 

101
00:06:54,540 --> 00:06:59,560
make it clear that this is an output thing so basically if our vertex sauce
明确说明这是输出的东西，所以基本上，如果我们的顶点酱

102
00:06:59,759 --> 00:07:03,250
you know then let's basically dereference this and assign it like that
您知道了，然后让我们基本上取消引用并像这样分配它

103
00:07:03,449 --> 00:07:06,699
and we can kind of deal with it that way that way you can ask your allow to
我们可以通过这种方式处理您可以要求允许的方式

104
00:07:06,899 --> 00:07:11,439
supply you know into here first of all you need to explicitly get the memory
首先，您需要明确地获取内存

105
00:07:11,639 --> 00:07:15,430
address of these variables right which is kind of almost makes you think twice
这些变量的地址对，几乎让您三思

106
00:07:15,629 --> 00:07:18,310
you're like actually what is this oh it's apps you know that means that
您实际上是在想什么，哦，您知道的应用程序意味着

107
00:07:18,509 --> 00:07:22,540
you're going to be riding into this so that's kind of a little bit more useful
您将要参与其中，这样会更有用

108
00:07:22,740 --> 00:07:27,819
but also the other benefit here is that you can actually specify you know null
但这里的另一个好处是您实际上可以指定您知道null 

109
00:07:28,019 --> 00:07:30,310
for one of these like I already care about the fragment shader so don't don't
对于其中一种，例如我已经在意片段着色器，请不要

110
00:07:30,509 --> 00:07:33,100
give me anything for that and that will work just fine because you're a
给我任何东西，那会很好的，因为你是

111
00:07:33,300 --> 00:07:37,210
specified null now since you're taking in a pointer and this function handles
现在指定空值，因为您要使用一个指针，并且此函数可以处理

112
00:07:37,410 --> 00:07:42,310
that correctly so that's kind of the as input parameters kind of way of dealing
正确，所以这是作为输入参数的一种处理方式

113
00:07:42,509 --> 00:07:45,550
with multiple return types let's talk about another kind of simple way that
有多种返回类型让我们讨论另一种简单的方法

114
00:07:45,750 --> 00:07:49,420
you can probably think of by yourself as well and that is just returning an array
您可能也可以自己考虑，这只是返回一个数组

115
00:07:49,620 --> 00:07:53,949
right so we could return an array in the form of just returning a pointer like
对的，所以我们可以以只返回一个指针的形式返回一个数组

116
00:07:54,149 --> 00:07:57,879
this and then at the end here so let's get rid of these extra parameters at the
这个，然后在这里结束，所以让我们摆脱这些额外的参数

117
00:07:58,079 --> 00:08:04,210
end here we can basically just say return new STD string which is our new
在这里结束，我们基本上可以说返回新的STD字符串，这是我们的新

118
00:08:04,410 --> 00:08:07,210
array which is going to be based in array of two and then we can kind of
数组将基于两个数组，然后我们可以

119
00:08:07,410 --> 00:08:10,629
just pipe pipe in or just put in you know I've got a shredder and our
只是把烟斗放进去，或者放进去就知道我有一个碎纸机， 

120
00:08:10,829 --> 00:08:13,840
fragment shader like this now the unfortunate part with this is
像这样的片段着色器，现在不幸的是

121
00:08:14,040 --> 00:08:17,590
that we are using the new key aware so we are causing a heap allocation to
我们正在使用新的密钥，因此我们导致了堆分配给

122
00:08:17,790 --> 00:08:21,670
happen I would like to avoid doing that especially because these strings already
发生我想避免这样做，特别是因为这些字符串已经

123
00:08:21,870 --> 00:08:24,280
are kind of heap allocating and yet don't want to talk too much about
是堆分配的一种，但不想谈论太多

124
00:08:24,480 --> 00:08:28,480
performance obviously in this video but just keep that in mind that I kind of
影片中的表现很明显，但请记住，我有点

125
00:08:28,680 --> 00:08:31,990
not a bit know each fan of that but we do get an array back and we can
一点也不了解每个粉丝，但是我们确实得到了一个阵列，我们可以

126
00:08:32,190 --> 00:08:38,859
obviously deal with it just by setting this equal to s city string
显然只需将其设置为等于城市字符串即可处理

127
00:08:39,059 --> 00:08:43,870
shadows or sources now this type of array is a little bit annoying because
阴影或源现在这种类型的数组有点烦人，因为

128
00:08:44,070 --> 00:08:48,519
we don't know how big it is right it's just a pointer so the more C++ way of
我们不知道它到底有多大，它只是一个指针，所以更多的C ++方式

129
00:08:48,720 --> 00:08:53,319
doing this will probably be just to return an STD array the tide would be
这样做可能只是返回一个STD数组

130
00:08:53,519 --> 00:08:57,549
string and the size would be to okay that's pretty simple and then we can
字符串和大小就可以了，这很简单，然后我们可以

131
00:08:57,750 --> 00:09:02,049
just basically return an array a city string and - and I don't actually know
基本上只返回一个包含城市字符串的数组，而-我实际上并不知道

132
00:09:02,250 --> 00:09:06,789
how to actually take parameters in or if it even does take parameters in no clue
如何实际获取参数，或者甚至不知道如何获取参数

133
00:09:06,990 --> 00:09:09,459
but you guys kind of get the point whatever the syntax is I don't really
但是你们不管语法是什么，我都不太明白

134
00:09:09,659 --> 00:09:13,209
use standard right very often because you can tell you probably have to
经常使用标准权利，因为您可能会告诉您

135
00:09:13,409 --> 00:09:16,779
actually well one way you could definitely do it I guess I should show
实际上很好，您肯定可以做到的一种方法，我想我应该证明

136
00:09:16,980 --> 00:09:20,709
some working code right as you could basically just create the array like
正确的一些工作代码，您基本上可以像这样创建数组

137
00:09:20,909 --> 00:09:28,059
this call it you know results and then just result 0 equals vs and results 2 or
这个叫它你知道结果，然后结果0等于vs和结果2或

138
00:09:28,259 --> 00:09:33,189
1 equals FS like that and return results now what's wrong with this incomplete
 1等于FS，现在返回结果这个不完整的问题是什么

139
00:09:33,389 --> 00:09:38,250
I'm not allowed I'm not sure is happy about me using array let's include array
我不允许使用数组不确定我是否对我满意

140
00:09:38,450 --> 00:09:41,679
and there you go all right cool that's one way to do it
然后你就可以很酷地做到这一点

141
00:09:41,879 --> 00:09:47,049
if you want an array now this shader function specifically could be fairly
如果您现在想要一个数组，则此着色器功能专门可以相当

142
00:09:47,250 --> 00:09:51,009
variable in the fact that we might have more than two types so another way to do
变量，因为我们可能有两种以上的类型，所以另一种方法

143
00:09:51,210 --> 00:09:55,179
this would just be to returning to to return a vector the difference between
这只是返回到向量之间的差

144
00:09:55,379 --> 00:09:58,389
this and the array method that I just showed you so let's include vector
这个和我刚刚向您展示的数组方法，所以让我们包括向量

145
00:09:58,589 --> 00:10:04,569
instead the difference between the two types is primarily the fact that arrays
相反，两种类型之间的区别主要在于数组

146
00:10:04,769 --> 00:10:07,899
can be written on the stack where as vectors gonna store its underlying
可以写在堆栈上，在那里向量将存储其底层

147
00:10:08,100 --> 00:10:11,740
storage on the heap so technically returning in a standard ray would
存储在堆上，因此技术上以标准射线返回

148
00:10:11,940 --> 00:10:16,959
actually be faster but the vector way again would be very similar which is
实际上更快，但矢量方式又将非常相似，这是

149
00:10:17,159 --> 00:10:21,069
create a vector kind of at the end here with the results and then just assign
在结果的最后创建一个向量，然后分配

150
00:10:21,269 --> 00:10:23,740
results of done and return results and that's kind of it we're done
完成的结果并返回结果，这就是我们已经完成的事情

151
00:10:23,940 --> 00:10:28,059
ok pretty simple we could all with always you know reserved you know two or
好吧，非常简单，我们可以始终保持您知道的保留，您知道两个或

152
00:10:28,259 --> 00:10:32,109
whatever spots if we wanted to be really kind of a damn thick about our memory
如果我们想让我们的记忆真的很丰富

153
00:10:32,309 --> 00:10:35,479
allocated reallocation and recopy and all that
分配的重新分配和重新复制以及所有这些

154
00:10:35,679 --> 00:10:39,620
stuff but it's fine for now again not really talking about performance let's
东西，但现在又可以了，不用再真正谈论性能了，让我们

155
00:10:39,820 --> 00:10:44,689
let's keep going okay so there we go I've shown you guys some ways to return
让我们继续前进，好了，我们走了，我向你们展示了一些返回的方法

156
00:10:44,889 --> 00:10:47,990
multiple return types obviously the veteran the array kind of ways only work
多种返回类型显然是退伍军人的一种数组工作方式

157
00:10:48,190 --> 00:10:52,269
if the types are the same let's talk about a universal way for us to return
如果类型相同，让我们讨论一种通用的返回方法

158
00:10:52,470 --> 00:10:57,919
kind of two different variables that may or may not be of varying types and this
可能是也可能不是不同类型的两个不同变量的种类，这

159
00:10:58,120 --> 00:11:03,500
is specifically a way that c++ actually provides for us and the two kind of ways
特别是c ++实际为我们提供的一种方式以及两种方式

160
00:11:03,700 --> 00:11:07,009
we'll talk about is something called a tuple and something called a pair now a
我们将讨论的是一个称为元组的东西，现在称为一对

161
00:11:07,210 --> 00:11:12,439
tuple is basically a class which can contain X amount of variables and it
元组基本上是一个可以包含X个变量的类，它

162
00:11:12,639 --> 00:11:17,120
doesn't care about the types okay so to show you guys what that would look like
不在乎类型是否正确，因此向大家展示一下会是什么样

163
00:11:17,320 --> 00:11:25,879
it's basically what we would return is the standard tuple of STD string comma
基本上，我们将返回的是STD字符串逗号的标准元组

164
00:11:26,080 --> 00:11:30,229
SC D string if we also wanted to return an integer we can just go comma int okay
 SC D字符串，如果我们也想返回一个整数，我们可以用逗号int好了

165
00:11:30,429 --> 00:11:34,579
like that so two kind of strings of what we're returning now we have to include
这样，我们现在要返回的两种字符串必须包含

166
00:11:34,779 --> 00:11:39,229
two things for this we have to include utility which actually contains the
为此，我们必须包括两件实用程序，其中实际上包含

167
00:11:39,429 --> 00:11:43,549
tuple so if we go back here will include utility actually I'm not even sure if
元组，所以如果我们回到这里，实际上会包含实用程序，我什至不确定

168
00:11:43,750 --> 00:11:46,370
utility does contain triple what was the other one
实用程序确实包含三倍于另一个

169
00:11:46,570 --> 00:11:50,709
there's utility and then there's also I use this so so rally you guys can tell
有实用程序，然后还有我用它，所以你们可以说集会

170
00:11:50,909 --> 00:11:56,449
utility and is it functional maybe isn't functional yesin functional so sorry
实用程序，是否可以正常工作，可能无法正常工作，所以对不起

171
00:11:56,649 --> 00:12:01,009
tuple is in functional utilities what we're going to use to make the tuple so
元组在功能实用程序中，我们将使用它们来制作元组

172
00:12:01,210 --> 00:12:06,559
I'm going to return STV make pair okay which is in the kind of utility thing
我要归还STV make pair，这是一种实用的东西

173
00:12:06,759 --> 00:12:13,339
specifically for making two kind of things here STD string STD string and
专门用于制作两种类型的东西STD字符串STD字符串和

174
00:12:13,539 --> 00:12:19,009
then we'll just pass in a BS in FS okay just like that now we know instance
然后我们就可以通过FS的BS了，就像现在知道实例一样

175
00:12:19,210 --> 00:12:21,799
currently matches the argument list that's fantastic up probably not
当前匹配的参数列表太棒了

176
00:12:22,000 --> 00:12:25,819
including something okay so basically we just don't need I mean we don't need to
包括可以的东西，所以基本上我们不需要，我的意思是我们不需要

177
00:12:26,019 --> 00:12:29,120
specify to here we've had we don't need a specify the template argument at all
指定到这里，我们根本不需要指定template参数

178
00:12:29,320 --> 00:12:32,750
it'll work that out by itself so just return a city make pair like that and
它会自行解决，因此只需返回像这样的城市配对

179
00:12:32,950 --> 00:12:36,500
it's called that returns well in this case our tuple for us the way that we
在这种情况下，我们的元组以一种

180
00:12:36,700 --> 00:12:39,559
would use this we ask this it'll be returning a tuple here yes like it
会用到这个，我们问这会在这里返回一个元组，是这样

181
00:12:39,759 --> 00:12:47,149
basically just call STD tuple you know STD string STD a string
基本上只是调用STD元组，你知道STD字符串STD一个字符串

182
00:12:47,350 --> 00:12:53,779
our sources now and to provide to provide the actual credit function this
现在我们的资源，并提供实际的信用功能

183
00:12:53,980 --> 00:12:56,629
is the type but I would probably if I was using tuples I probably just use
是类型，但是如果使用元组，我可能会使用

184
00:12:56,830 --> 00:13:00,949
order like that by the way to actually get the data out of out of the tuple
这样的顺序实际上是从元组中取出数据

185
00:13:01,149 --> 00:13:06,859
though this is the part that I hate what you actually need to do is use s City
尽管这是我讨厌您真正需要做的部分，是使用s City 

186
00:13:07,059 --> 00:13:14,719
yet and then the template argument here is the index so zero and then the tuple
然而这里的模板参数是索引，所以零，然后是元组

187
00:13:14,919 --> 00:13:17,959
you want to get it from so sources that is what actually gives
您想从实际获得的资源中获取

188
00:13:18,159 --> 00:13:22,819
you your vertex shader source code like that okay so to get that to work in the
您可以像这样使用顶点着色器源代码，以便使其在

189
00:13:23,019 --> 00:13:31,129
Creator function we pass in yesterday get 0 and s to be get 1 okay and that's
我们昨天传入的Creator函数得到0，而s得到1，这就是

190
00:13:31,330 --> 00:13:35,509
how we actually get those two things out about this to me that's horrendous code
我们如何从这两个方面得到可怕的代码

191
00:13:35,710 --> 00:13:41,419
style I mean what is going on here first of all I hate this because well 0 and 1
风格，我的意思是首先发生在这里我讨厌这个，因为0和1 

192
00:13:41,620 --> 00:13:45,889
are essentially the names of our actual variables like it's hard to tell it's
本质上是我们实际变量的名称，就像很难说出来的是

193
00:13:46,090 --> 00:13:49,759
like which one's the vet I show you which one's the fragment I mean it
就像哪个兽医是我告诉你那个碎片是我的意思

194
00:13:49,960 --> 00:13:52,490
logically obviously it makes sense that the first one's gonna be the vertex and
从逻辑上讲，第一个将成为顶点并

195
00:13:52,690 --> 00:13:54,769
then the fragment but someone who doesn't know what they're doing like it
然后是片段，但是一个不知道自己在做什么的人喜欢它

196
00:13:54,970 --> 00:13:58,000
doesn't make sense because we're not dealing with names but isn't was numbers
没有意义，因为我们不是在处理名字，而是数字

197
00:13:58,200 --> 00:14:03,649
so these aren't named kind of named variables essentially in this case
因此在这种情况下，这些基本上不是命名变量

198
00:14:03,850 --> 00:14:08,029
because when we have to we can just return an STD pair the only difference
因为当我们不得不返回一个STD对时，唯一的区别是

199
00:14:08,230 --> 00:14:11,990
here by the way so we have an STD pair of string a string the only difference
顺便说一句，所以我们有一对STD字符串，唯一的区别是字符串

200
00:14:12,190 --> 00:14:18,829
here is that we can still use s here you get and that will work fine but what we
这是我们仍然可以在这里使用s，它将正常工作，但是我们

201
00:14:19,029 --> 00:14:23,899
what we can also do is simplify a little bit by just using sources dot first like
我们还可以做的就是通过首先使用点号来简化一点

202
00:14:24,100 --> 00:14:28,129
this okay because there's only gonna be two of them sources dot second as well
没关系，因为其中只有两个来源也是第二个

203
00:14:28,330 --> 00:14:32,629
because it's a pair okay so that's a little bit better just kind of in terms
因为这对还可以，所以从某种意义上来说要好一些

204
00:14:32,830 --> 00:14:37,519
of syntax I feel like that's a little bit better but it still doesn't solve
语法方面，我觉得这要好一些，但仍然无法解决

205
00:14:37,720 --> 00:14:39,949
that problem because we don't know what the variables that what's first what is
这个问题，因为我们不知道首先是什么的变量是什么

206
00:14:40,149 --> 00:14:44,479
second I have no idea right it's not very clean so that's why
第二我不知道是不是很干净，所以这就是为什么

207
00:14:44,679 --> 00:14:48,979
I always come back to disrupt way of doing things right when we need to
当我们需要时，我总是回来破坏正确的做事方式

208
00:14:49,179 --> 00:14:53,779
return multiple variables like this right I like to just create a struct
像这样返回多个变量，我只想创建一个结构

209
00:14:53,980 --> 00:14:57,709
called well in this case we can call it shader program source or something like
在这种情况下调用得很好，我们可以将其称为着色器程序源或类似

210
00:14:57,909 --> 00:15:00,439
that right and this is just going to
那个权利，这只是要

211
00:15:00,639 --> 00:15:04,759
basically be like returning a pair except our variables are named right I
基本上就像返回一对，只是我们的变量命名为I 

212
00:15:04,960 --> 00:15:11,179
can make STD string here call it vertex source like that and then make another
可以在这里将STD字符串称为顶点源，然后再创建另一个

213
00:15:11,379 --> 00:15:16,250
one for the fragment source like that okay
一个这样的片段源

214
00:15:16,450 --> 00:15:22,159
and then just return shade a program source this is gonna like underline the
然后只返回阴影一个程序源，就像下划线

215
00:15:22,360 --> 00:15:25,579
underlying way that this works is this is just a pair right as a struct it's
这种工作的基本方式是，这是一对正确的结构

216
00:15:25,779 --> 00:15:28,729
literally just two strings made up of two strings everything's gonna be
从字面上看，只有两个字符串由两个字符串组成

217
00:15:28,929 --> 00:15:31,490
created on the stack here if I just return a normal share the program source
如果我只返回普通共享程序源，则在此处创建堆栈

218
00:15:31,690 --> 00:15:36,349
like this and then we can just simply return the vsfs just like that very easy
这样，然后我们就可以简单地返回vsfs 

219
00:15:36,549 --> 00:15:40,699
to write looks great and when we actually come to use this instead of
写起来很棒，当我们实际使用它而不是

220
00:15:40,899 --> 00:15:44,870
specifying first and second we can specify sources of vertex source and
指定第一和第二，我们可以指定顶点源和

221
00:15:45,070 --> 00:15:49,429
sources or fragments source like that and everything it just makes sense to me
像这样的资源或片段，对我来说一切都有意义

222
00:15:49,629 --> 00:15:52,159
you need never gonna mix these up you're never gonna mix first and second or
您永远不需要将这些混合在一起，也永远不会将第一和第二混合在一起，或者

223
00:15:52,360 --> 00:15:55,219
forget what they which ones first which one second never gonna makes any of that
忘记他们先是哪个而后又再也不会做出任何

224
00:15:55,419 --> 00:16:00,259
stuff up it's really easier dead-dead kind of easier to use and makes in my
填充起来真的很容易，死胡同也更容易使用，并且使

225
00:16:00,460 --> 00:16:03,829
opinion the code much clearer up much easier to read so that is how I like to
认为代码更加清晰易读，这就是我喜欢的方式

226
00:16:04,029 --> 00:16:07,939
deal with multiple return types we went on quite and quite an adventure that I
处理多种回报类型，我们进行了相当多的冒险

227
00:16:08,139 --> 00:16:11,929
actually thought of a few things kind of on the spot as well so that's what these
实际上也想到了一些事情，这些就是这些

228
00:16:12,129 --> 00:16:16,009
new style videos on the couch are all about this is our first kind of just
沙发上的新视频都是关于这的，这是我们的第一种

229
00:16:16,210 --> 00:16:20,599
open discussion I think about something in C++ so let me know what your thoughts
公开讨论，我在考虑C ++方面的问题，所以请告诉我您的想法

230
00:16:20,799 --> 00:16:24,529
are about multiple return types which one of these methods or maybe there are
关于多种返回类型，这些方法之一或也许有

231
00:16:24,730 --> 00:16:27,620
more than I mean I'm sure there are more than I'm that I haven't mentioned as
比我的意思还多，我敢肯定还有更多我没有提到的

232
00:16:27,820 --> 00:16:31,849
well what your favorite method of dealing with multiple return types is
那么您最喜欢的处理多种返回类型的方法是

233
00:16:32,049 --> 00:16:35,839
drop a comment below you can also discuss this further by going over to
在下面发表评论，您也可以通过转到以下内容进一步讨论

234
00:16:36,039 --> 00:16:39,019
the channel compilation discord is a discord server where you can talk to
通道编辑不和谐是一个不和谐服务器，您可以在其中与之交谈

235
00:16:39,220 --> 00:16:42,469
other people about this kind of stuff and as always if you enjoy this video
其他人也喜欢这种东西，如果您喜欢这部影片的话

236
00:16:42,669 --> 00:16:45,939
and you want to help support the series and see more great videos like this then
您想帮助支持该系列并观看更多类似的精彩视频，然后

237
00:16:46,139 --> 00:16:50,329
head on over to patreon a course at the chanter huge thank you as always to all
前往patreon在chanter上的课程，非常感谢大家

238
00:16:50,529 --> 00:16:53,509
the people who support all the series that i make without those people this
支持我没有这些人的所有系列的人

239
00:16:53,710 --> 00:16:58,459
series would not be here I can say that as a fact so huge thank you to all of
系列不会在这里我可以说，事实上，非常感谢你们

240
00:16:58,659 --> 00:17:03,579
you guys and I'll see you guys in the next video goodbye
大家，我会在下一个视频再见

241
00:17:03,779 --> 00:17:12,118
[Music]
[音乐]

242
00:17:15,759 --> 00:17:20,759
[Music]
 [音乐] 

