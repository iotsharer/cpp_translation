﻿1
00:00:00,000 --> 00:00:01,449
嗨，大家好，我的名字是欢迎回到我稳定的水疗中心系列的，所以
hey what's up guys my name is a turn-on

2
00:00:01,649 --> 00:00:03,520
嗨，大家好，我的名字是欢迎回到我稳定的水疗中心系列的，所以
welcome back to my stable spas series so

3
00:00:03,720 --> 00:00:04,719
今天我们将讨论所有有关使用C ++进行排序的知识，
today we will be talking all about

4
00:00:04,919 --> 00:00:08,020
今天我们将讨论所有有关使用C ++进行排序的知识，
sorting in C++ so talking about various

5
00:00:08,220 --> 00:00:09,460
不同的数据结构，随着系列的进行，我们将继续讨论
different data structures and as the

6
00:00:09,660 --> 00:00:11,470
不同的数据结构，随着系列的进行，我们将继续讨论
series goes on we will continue talking

7
00:00:11,669 --> 00:00:13,979
关于各种数据结构，因为数据结构是您存储数据的方式
about various data structures because

8
00:00:14,179 --> 00:00:16,300
关于各种数据结构，因为数据结构是您存储数据的方式
data structures are how you store data

9
00:00:16,500 --> 00:00:18,010
在编程和处理信号总线时，数据非常重要
and data is very important when you're

10
00:00:18,210 --> 00:00:19,390
在编程和处理信号总线时，数据非常重要
programming and dealing with signals bus

11
00:00:19,589 --> 00:00:21,820
我们还没有谈论的是如何对已有的数据进行排序
what we haven't talked about yet is how

12
00:00:22,019 --> 00:00:23,890
我们还没有谈论的是如何对已有的数据进行排序
do you sort the data that you have so

13
00:00:24,089 --> 00:00:25,629
假设我有一个向量或值整数数组，我希望它们是
suppose that I have a vector or an array

14
00:00:25,829 --> 00:00:29,140
假设我有一个向量或值整数数组，我希望它们是
of values integers and I want them to be

15
00:00:29,339 --> 00:00:32,468
根据它们的价值或某种谓词排序，我实际上如何获得
sorted by their value or by some kind of

16
00:00:32,668 --> 00:00:34,659
根据它们的价值或某种谓词排序，我实际上如何获得
predicate how do I actually gets

17
00:00:34,859 --> 00:00:36,189
现在不可能为我整理一些东西，显然你可以自己写
impossible to sort something for me now

18
00:00:36,390 --> 00:00:37,719
现在不可能为我整理一些东西，显然你可以自己写
obviously you could just write your own

19
00:00:37,920 --> 00:00:39,518
气泡排序等快速排序算法或仅
algorithm like bubble sort a quick sort

20
00:00:39,719 --> 00:00:41,259
气泡排序等快速排序算法或仅
or any kind of algorithm that just

21
00:00:41,460 --> 00:00:43,299
遍历列表并将元素按所需顺序提供
iterates through the list and source the

22
00:00:43,500 --> 00:00:44,439
遍历列表并将元素按所需顺序提供
elements into the order that you want

23
00:00:44,640 --> 00:00:46,329
将它们排序完全可以，有时您会想要
them to be sorted in that's totally fine

24
00:00:46,530 --> 00:00:48,128
将它们排序完全可以，有时您会想要
and there are cases when you would want

25
00:00:48,329 --> 00:00:49,119
做这样的事情，但是通常如果您正在处理
to do something like that

26
00:00:49,320 --> 00:00:52,119
做这样的事情，但是通常如果您正在处理
however usually if you're dealing with

27
00:00:52,320 --> 00:00:54,759
简单的运动类型，其内置的收集类型（例如STD向量）
simple sports kind of with its inbuilt

28
00:00:54,960 --> 00:00:57,338
简单的运动类型，其内置的收集类型（例如STD向量）
collection types like STD vector there's

29
00:00:57,539 --> 00:00:58,989
如果您可以得到，则没有理由必须自己编写算法
no reason for you to have to write an

30
00:00:59,189 --> 00:01:01,209
如果您可以得到，则没有理由必须自己编写算法
algorithm yourself if you could just get

31
00:01:01,409 --> 00:01:03,128
残疾人专用图书馆为您进行分类，这就是今天的原因
the disabled spot library to do the

32
00:01:03,329 --> 00:01:05,500
残疾人专用图书馆为您进行分类，这就是今天的原因
sorting for you and that's why today

33
00:01:05,700 --> 00:01:06,909
我们将专注于一种叫做STD sort的东西，它是一种
we're going to focus on something called

34
00:01:07,109 --> 00:01:09,519
我们将专注于一种叫做STD sort的东西，它是一种
STD sort which is a sort function that's

35
00:01:09,719 --> 00:01:10,988
内置在这个桌布库中，可以实际执行
kind of built into this tablecloth

36
00:01:11,188 --> 00:01:13,480
内置在这个桌布库中，可以实际执行
library that can actually perform that

37
00:01:13,680 --> 00:01:15,009
通过您提供的任何种类的迭代器为您排序，如果
sorting for you over any kind of

38
00:01:15,209 --> 00:01:16,629
通过您提供的任何种类的迭代器为您排序，如果
iterator that you provide it with so if

39
00:01:16,829 --> 00:01:18,340
我们跳到CBP参考，所以来看看STD，以便您可以看到我们
we pop over to CBP reference so come and

40
00:01:18,540 --> 00:01:20,079
我们跳到CBP参考，所以来看看STD，以便您可以看到我们
take a look at STDs so it you can see we

41
00:01:20,280 --> 00:01:21,909
这里有这个功能，可以对我们在一定范围内的元素进行排序
have this function here that will sort

42
00:01:22,109 --> 00:01:23,469
这里有这个功能，可以对我们在一定范围内的元素进行排序
elements in a certain range that we

43
00:01:23,670 --> 00:01:24,730
提供它，而我们必须提供的基本上只是一个
provide it with and what we have to

44
00:01:24,930 --> 00:01:26,738
提供它，而我们必须提供的基本上只是一个
provide it with is essentially just an

45
00:01:26,938 --> 00:01:28,778
迭代器，开始的索引器和结束的迭代器以及其中的所有内容
iterator and beginning interrater and an

46
00:01:28,978 --> 00:01:30,549
迭代器，开始的索引器和结束的迭代器以及其中的所有内容
end iterate and everything inside that

47
00:01:30,750 --> 00:01:32,500
迭代器将根据我们提供的某个谓词进行排序
iterator will be sorted based on a

48
00:01:32,700 --> 00:01:33,849
迭代器将根据我们提供的某个谓词进行排序
certain predicate that we provide it

49
00:01:34,049 --> 00:01:35,168
所以我们能做的就是不提供任何形式的
with so what we can do is kind of just

50
00:01:35,368 --> 00:01:36,609
所以我们能做的就是不提供任何形式的
not provide it with any kind of

51
00:01:36,810 --> 00:01:38,230
谓词，我们将根据类型或类型尝试按顺序排序
predicate and we'll just attempt to sort

52
00:01:38,430 --> 00:01:40,390
谓词，我们将根据类型或类型尝试按顺序排序
of kind of in order based on the type or

53
00:01:40,590 --> 00:01:42,750
我们实际上可以为其提供某种功能以供实际使用
we can actually give it some kind of

54
00:01:42,950 --> 00:01:44,679
我们实际上可以为其提供某种功能以供实际使用
function for it to actually use for

55
00:01:44,879 --> 00:01:46,448
比较，我们还可以为其提供lambda，我们将对其进行看看
comparison and we can provide it with a

56
00:01:46,649 --> 00:01:48,128
比较，我们还可以为其提供lambda，我们将对其进行看看
lambda as well which we'll take a look

57
00:01:48,328 --> 00:01:49,599
它没有任何返回值，并且排序的复杂性是
at it doesn't have any kind of return

58
00:01:49,799 --> 00:01:51,488
它没有任何返回值，并且排序的复杂性是
value and the complexity for sorting is

59
00:01:51,688 --> 00:01:54,278
n次登录n，这相当不错，因此，如果我们跳回到此处，返回源代码
n times log n which is fairly good so if

60
00:01:54,478 --> 00:01:55,808
n次登录n，这相当不错，因此，如果我们跳回到此处，返回源代码
we jump back here into our source code

61
00:01:56,009 --> 00:01:58,119
让我们看一个基本的例子，所以我在这里要做的是创建一个
let's take a look at a basic example so

62
00:01:58,319 --> 00:01:59,140
让我们看一个基本的例子，所以我在这里要做的是创建一个
what I'll do here is I'll create a

63
00:01:59,340 --> 00:02:00,668
向量，它可能是一个数组，实际上与整数无关紧要
vector it could be an array doesn't

64
00:02:00,868 --> 00:02:02,709
向量，它可能是一个数组，实际上与整数无关紧要
really matter of integers I'll call

65
00:02:02,909 --> 00:02:04,480
这些值，我只提供一些基本值，让我们说三个5
these values and I'll just provide some

66
00:02:04,680 --> 00:02:07,028
这些值，我只提供一些基本值，让我们说三个5
basic values let's just say three five

67
00:02:07,228 --> 00:02:10,630
一四二好吧，这些显然是不正常的
one four two okay so these are clearly

68
00:02:10,830 --> 00:02:11,618
一四二好吧，这些显然是不正常的
not in order

69
00:02:11,818 --> 00:02:13,060
宣传任何一种随机顺序的水，这里我将介绍
promoting any kind of water they're in a

70
00:02:13,259 --> 00:02:15,039
宣传任何一种随机顺序的水，这里我将介绍
bit of a random order here I'll include

71
00:02:15,239 --> 00:02:17,500
向量，还包括我们可以对性病进行排序的算法以及我想做的事情
vector and also include algorithms that

72
00:02:17,699 --> 00:02:20,140
向量，还包括我们可以对性病进行排序的算法以及我想做的事情
we get STDs sort and what I want to do

73
00:02:20,340 --> 00:02:21,429
现在，我要使用此向量进行升序排序，以便它们从
with this vector now is I want to sort

74
00:02:21,628 --> 00:02:22,989
现在，我要使用此向量进行升序排序，以便它们从
this in ascending order so they go from

75
00:02:23,188 --> 00:02:24,670
一二三四五所以要做的就是我只输入STD
one two three four five so to do that

76
00:02:24,870 --> 00:02:26,560
一二三四五所以要做的就是我只输入STD
what I'll do is I'll just type in STDs

77
00:02:26,759 --> 00:02:29,590
我将在此向量迭代器的开头传递排序，即值不
sort I'll pass in the start of this

78
00:02:29,789 --> 00:02:31,149
我将在此向量迭代器的开头传递排序，即值不
vector iterator which is values don't

79
00:02:31,348 --> 00:02:34,269
开始结束希望值或结束，以便为其提供范围，并且它们
begin the end wishes values or end so

80
00:02:34,468 --> 00:02:36,250
开始结束希望值或结束，以便为其提供范围，并且它们
that provides it with a range and they

81
00:02:36,449 --> 00:02:37,450
如果我不提供任何谓词，我现在不需要
don't I need to provide some kind of

82
00:02:37,650 --> 00:02:38,800
如果我不提供任何谓词，我现在不需要
predicate now if I don't provide any

83
00:02:39,000 --> 00:02:40,330
一种谓词，所以最好不要为它提供函数
kind of predicate so best I don't

84
00:02:40,530 --> 00:02:42,219
一种谓词，所以最好不要为它提供函数
provide it with a function for it to

85
00:02:42,419 --> 00:02:44,140
执行这种排序，在以下情况下，它将按照升序排序
perform this sorting it will just sort

86
00:02:44,340 --> 00:02:46,118
执行这种排序，在以下情况下，它将按照升序排序
it in ascending order in the case of

87
00:02:46,318 --> 00:02:47,830
这里是整数，换句话说，如果我是控制台之后的公主
integers here so in other words if I was

88
00:02:48,030 --> 00:02:49,450
这里是整数，换句话说，如果我是控制台之后的公主
a princess to the console after the

89
00:02:49,650 --> 00:02:51,429
我会说排序发生在值内部的单位值之前，我们来做一个CBC
sorting happens I'll say for unit value

90
00:02:51,628 --> 00:02:55,420
我会说排序发生在值内部的单位值之前，我们来做一个CBC
inside values let's do a CBC out the

91
00:02:55,620 --> 00:02:58,749
值只有五点钟，那么如果我
value just like that at five then you'll

92
00:02:58,949 --> 00:03:01,060
值只有五点钟，那么如果我
see I get a perfectly sorted list if I

93
00:03:01,259 --> 00:03:02,679
确实想提供某种方式进行分类，我可以通过传递
do want to provide some kind of a way

94
00:03:02,878 --> 00:03:04,989
确实想提供某种方式进行分类，我可以通过传递
for it to sort I can do that by passing

95
00:03:05,188 --> 00:03:06,550
在一个函数中，这可以是字面上的函数，例如
in a function and this can be either

96
00:03:06,750 --> 00:03:08,560
在一个函数中，这可以是字面上的函数，例如
literally a function inside like a

97
00:03:08,759 --> 00:03:11,080
您所做的中风，也可以是lambda，所以有很多
stroke that you do you make or it can be

98
00:03:11,280 --> 00:03:12,399
您所做的中风，也可以是lambda，所以有很多
a lambda so there are quite a few

99
00:03:12,598 --> 00:03:14,560
内置的，如果我要包括功能，我可以使用类似STD的东西
built-in ones as well if I go to include

100
00:03:14,759 --> 00:03:17,709
内置的，如果我要包括功能，我可以使用类似STD的东西
functional I can use something like STD

101
00:03:17,908 --> 00:03:18,129
像布雷达这样的蚂蚁
Breda

102
00:03:18,329 --> 00:03:21,300
像布雷达这样的蚂蚁
for example with ant just like that

103
00:03:21,500 --> 00:03:23,739
如果我能击中那五个，它将根据更大的值对其进行排序，因此您可以
if I can hit that five this will sort it

104
00:03:23,938 --> 00:03:26,499
如果我能击中那五个，它将根据更大的值对其进行排序，因此您可以
based on the greater value so you can

105
00:03:26,699 --> 00:03:28,029
看到我得到五四三二二，最后我还要写一个lambda
see that I get five four three two one

106
00:03:28,229 --> 00:03:30,550
看到我得到五四三二二，最后我还要写一个lambda
and finally I'll write a lambda as well

107
00:03:30,750 --> 00:03:32,920
所以我将摆脱它，而在A中放入一个普通的lambda
so I'll get rid of this and I'll just

108
00:03:33,120 --> 00:03:35,618
所以我将摆脱它，而在A中放入一个普通的lambda
have a normal lambda in a into B and

109
00:03:35,818 --> 00:03:38,080
然后我会解释一下，所以它是C我基本上只是提供它
then I'll explain this a bit so it's

110
00:03:38,280 --> 00:03:40,209
然后我会解释一下，所以它是C我基本上只是提供它
either C I'm basically just providing it

111
00:03:40,408 --> 00:03:41,618
现在将使用一个函数来基于这些函数对这些值进行排序
with a function that it's going to use

112
00:03:41,818 --> 00:03:44,319
现在将使用一个函数来基于这些函数对这些值进行排序
to sort these values now based on the

113
00:03:44,519 --> 00:03:46,779
文档，您可以看到此比较函数返回布尔值，因此
documentation you can see that this this

114
00:03:46,979 --> 00:03:49,509
文档，您可以看到此比较函数返回布尔值，因此
comparison function returns a bool so

115
00:03:49,709 --> 00:03:52,929
true或false，如果第一个参数是第一个参数，则返回true
true or false and it returns true if the

116
00:03:53,128 --> 00:03:54,789
true或false，如果第一个参数是第一个参数，则返回true
first argument so that is the first

117
00:03:54,989 --> 00:03:56,618
我们在此处传递给参数的参数小于
argument that we pass in here into the

118
00:03:56,818 --> 00:03:59,200
我们在此处传递给参数的参数小于
parameter is less than so is ordered

119
00:03:59,400 --> 00:04:01,539
在第二个之前，基本上这意味着如果我们给它两个值a
before the second so basically what that

120
00:04:01,739 --> 00:04:04,689
在第二个之前，基本上这意味着如果我们给它两个值a
means is that if we give it two values a

121
00:04:04,889 --> 00:04:06,879
和B其中哪一个应该首先出现在列表中
and B which one of these should appear

122
00:04:07,079 --> 00:04:09,550
和B其中哪一个应该首先出现在列表中
first in the list that's kind of what

123
00:04:09,750 --> 00:04:11,499
我们正在尝试确定此处，如果我们返回true，则表示a将是
we're trying to determine here and if we

124
00:04:11,699 --> 00:04:14,050
我们正在尝试确定此处，如果我们返回true，则表示a将是
return true it means that a will be

125
00:04:14,250 --> 00:04:16,718
在B之前排序，如果我们返回false，则它们将在a之前，例如a
ordered before B whereas if we return

126
00:04:16,918 --> 00:04:18,908
在B之前排序，如果我们返回false，则它们将在a之前，例如a
false then they will be before a so as a

127
00:04:19,108 --> 00:04:20,740
基本示例让我们对它进行排序
basic example let's just let it sort

128
00:04:20,939 --> 00:04:21,639
基本示例让我们对它进行排序
this you know in a

129
00:04:21,839 --> 00:04:23,710
从一到五，所以我在这里要做的就是简单地说
in order from one to five so what I'll

130
00:04:23,910 --> 00:04:26,110
从一到五，所以我在这里要做的就是简单地说
do here is I'll simply say return a less

131
00:04:26,310 --> 00:04:29,050
比B小，所以如果a小于B，我们会将其排序到列表的最前面
than B so if a is less than B we'll sort

132
00:04:29,250 --> 00:04:30,400
比B小，所以如果a小于B，我们会将其排序到列表的最前面
it to the front of the list

133
00:04:30,600 --> 00:04:32,020
让我们达到5，您会看到我得到了和以前一样的结果，
let's hit at five and you can see that I

134
00:04:32,220 --> 00:04:33,819
让我们达到5，您会看到我得到了和以前一样的结果，
get the same result as we had before and

135
00:04:34,019 --> 00:04:35,829
如果我扭转了这一点，显然它将以相反的方式对其进行排序
if I reverse this and obviously it's

136
00:04:36,029 --> 00:04:37,270
如果我扭转了这一点，显然它将以相反的方式对其进行排序
going to sort it the other way around

137
00:04:37,470 --> 00:04:39,490
如果a大于B，它将下降到最前面，这意味着
where if a is greater than B it's going

138
00:04:39,689 --> 00:04:40,990
如果a大于B，它将下降到最前面，这意味着
to fall to the front which means that

139
00:04:41,189 --> 00:04:42,520
较大的值排在最前面，然后让我们做一些事情
the bigger values get sorted to the

140
00:04:42,720 --> 00:04:44,379
较大的值排在最前面，然后让我们做一些事情
front and then let's just do something

141
00:04:44,579 --> 00:04:45,819
为了好玩，让我们做到这一点，直到最后
for fun let's just make it so that one

142
00:04:46,019 --> 00:04:47,740
为了好玩，让我们做到这一点，直到最后
goes to the very end I think this

143
00:04:47,939 --> 00:04:49,449
也很好地说明了谓词，所以如果我们说a是一个，那该怎么办
demonstrates the predicate pretty well

144
00:04:49,649 --> 00:04:52,778
也很好地说明了谓词，所以如果我们说a是一个，那该怎么办
as well so if we say if a is one what do

145
00:04:52,978 --> 00:04:55,090
如果a是当前要移到后面的第一个值，我们想做得好
we want to do well if a is currently the

146
00:04:55,290 --> 00:04:56,889
如果a是当前要移到后面的第一个值，我们想做得好
first value we want to move to the back

147
00:04:57,089 --> 00:04:58,689
因此，换句话说，我们要返回false，因为我们不希望它返回
of the list so in other words we want to

148
00:04:58,889 --> 00:05:01,060
因此，换句话说，我们要返回false，因为我们不希望它返回
return false because we don't want it to

149
00:05:01,259 --> 00:05:05,230
在B之前被订购，但是如果B是一个，那么我们希望a实际移至
be ordered before B however if B is one

150
00:05:05,430 --> 00:05:07,569
在B之前被订购，但是如果B是一个，那么我们希望a实际移至
then we want a to actually move to the

151
00:05:07,769 --> 00:05:08,980
最终清单以及我们这样做的方式是返回true
final list and the way that we do that

152
00:05:09,180 --> 00:05:09,939
最终清单以及我们这样做的方式是返回true
is we return true

153
00:05:10,139 --> 00:05:12,699
因为如果我们返回true，则意味着将a移到列表的最前面，这样
because if we return true it means that

154
00:05:12,899 --> 00:05:15,218
因为如果我们返回true，则意味着将a移到列表的最前面，这样
move a to the front of the list right so

155
00:05:15,418 --> 00:05:16,900
如果我将其改回原来的状态，以便我们仍然有一个前B，然后我按
if I change this back to what it was so

156
00:05:17,100 --> 00:05:18,968
如果我将其改回原来的状态，以便我们仍然有一个前B，然后我按
that we still have a before B and I hit

157
00:05:19,168 --> 00:05:21,069
f5您可以看到我们得到的列表是按升序排列的，除了
f5 you can see that we get our list in

158
00:05:21,269 --> 00:05:22,749
f5您可以看到我们得到的列表是按升序排列的，除了
ascending order except for one which is

159
00:05:22,949 --> 00:05:24,520
最后，无论如何，这是如何使用的非常基本且快速的示例
at the end so anyway that was a pretty

160
00:05:24,720 --> 00:05:26,199
最后，无论如何，这是如何使用的非常基本且快速的示例
basic and quick example of how to use

161
00:05:26,399 --> 00:05:28,120
SUV的排序和排序很简单，一般排序当然很有用
SUV sort and sorting a simple plus in

162
00:05:28,319 --> 00:05:30,790
SUV的排序和排序很简单，一般排序当然很有用
general sorting is of course very useful

163
00:05:30,990 --> 00:05:32,350
您可以对所有类型执行此操作，而不必是整数
you can do this with all types doesn't

164
00:05:32,550 --> 00:05:34,150
您可以对所有类型执行此操作，而不必是整数
have to be integers integers that are

165
00:05:34,350 --> 00:05:35,650
真的很容易显示，因为它们实际上是数字，我们有点用
just really easy to show because they're

166
00:05:35,850 --> 00:05:37,718
真的很容易显示，因为它们实际上是数字，我们有点用
literally numbers and we kind of I used

167
00:05:37,918 --> 00:05:39,639
以特定的顺序唱歌他们可以用琴弦来做
to sing them in a particular order you

168
00:05:39,839 --> 00:05:41,319
以特定的顺序唱歌他们可以用琴弦来做
can do this with strings you can do this

169
00:05:41,519 --> 00:05:43,360
这个带有自定义类，这就是这个lambda的谓词
with this with custom classes that's

170
00:05:43,560 --> 00:05:44,588
这个带有自定义类，这就是这个lambda的谓词
what this predicate here this lambda

171
00:05:44,788 --> 00:05:46,088
我们可以提供的内容非常有用，因为这意味着我们可以设置
that we could supply is so useful

172
00:05:46,288 --> 00:05:48,040
我们可以提供的内容非常有用，因为这意味着我们可以设置
because that means that we can set the

173
00:05:48,240 --> 00:05:50,170
规则，它不仅仅依赖于内置类型或任何其他类型的东西
rules and it doesn't rely on only

174
00:05:50,370 --> 00:05:51,759
规则，它不仅仅依赖于内置类型或任何其他类型的东西
working on inbuilt types or anything

175
00:05:51,959 --> 00:05:54,028
像这样，我们可以将其用于任何东西，最后我们有了一个快速视频
like that we can use this with anything

176
00:05:54,228 --> 00:05:56,199
像这样，我们可以将其用于任何东西，最后我们有了一个快速视频
finally we have a bit of a quick video

177
00:05:56,399 --> 00:05:57,579
如果您尝试使用这些方法已经很久了，那么我们最终只能在58 20
if you're trying to do these for ages

178
00:05:57,779 --> 00:05:59,439
如果您尝试使用这些方法已经很久了，那么我们最终只能在58 20
and we end up just making in 58 20

179
00:05:59,639 --> 00:06:02,110
分钟之久，但无论如何排序都非常有用，请让我知道您可以给
minutes long but anyway sorting really

180
00:06:02,310 --> 00:06:03,160
分钟之久，但无论如何排序都非常有用，请让我知道您可以给
useful let me know what you can give

181
00:06:03,360 --> 00:06:04,899
如果您有另一种方式或其他类型的场景，这些是
this if you have another way or other

182
00:06:05,098 --> 00:06:06,639
如果您有另一种方式或其他类型的场景，这些是
kind of scenarios in which this stuff is

183
00:06:06,839 --> 00:06:07,990
有用的捕食者可能会很好地工作，然后离开
useful and some predators that might

184
00:06:08,189 --> 00:06:09,730
有用的捕食者可能会很好地工作，然后离开
work really well then just leave a

185
00:06:09,930 --> 00:06:11,020
在下面发表评论，我们可以谈谈我希望你们喜欢
comment below and we can kind of talk

186
00:06:11,220 --> 00:06:12,879
在下面发表评论，我们可以谈谈我希望你们喜欢
about it I hope you guys enjoyed the

187
00:06:13,079 --> 00:06:13,960
视频，如果您点击了“赞”按钮，那么您还可以拥有支持系列
video if you did you can hit the like

188
00:06:14,160 --> 00:06:15,460
视频，如果您点击了“赞”按钮，那么您还可以拥有支持系列
button you can also have support series

189
00:06:15,660 --> 00:06:16,718
去参加礼拜，我会为这样的切诺基人而来，谢谢大家
by going to patreon I'll come for such

190
00:06:16,918 --> 00:06:18,699
去参加礼拜，我会为这样的切诺基人而来，谢谢大家
the cherokees thank you as always to all

191
00:06:18,899 --> 00:06:20,410
可爱的人们帮助支持了这个系列，使我有可能
the lovely people who helped support

192
00:06:20,610 --> 00:06:23,050
可爱的人们帮助支持了这个系列，使我有可能
this series and make it possible I will

193
00:06:23,250 --> 00:06:26,060
在下一个视频再见，[音乐]
see you guys in the next video goodbye

194
00:06:26,259 --> 00:06:31,259
在下一个视频再见，[音乐]
[Music]

