﻿1
00:00:00,000 --> 00:00:04,589
hey guys my name is DeSean oh and welcome back to my C++ series today
大家好，我叫DeSean哦，今天欢迎回到我的C ++系列

2
00:00:04,790 --> 00:00:08,800
we're going to talking about references last week we talked about pointers so if
我们上周要讨论引用，我们讨论了指针，所以如果

3
00:00:09,000 --> 00:00:14,260
you didn't see that video you are 100% going to have to because reference is a
您没有看到要100％观看的视频，因为参考是

4
00:00:14,460 --> 00:00:18,159
really just an extension of pointers so you need to be able to understand at
实际上只是指针的扩展，因此您需要能够了解

5
00:00:18,359 --> 00:00:22,749
least at a basic level how pointers work in order to actually get this video so
至少在基本层面上，指针如何工作才能真正获得此视频，因此

6
00:00:22,949 --> 00:00:25,749
go ahead will be a link in the description below if you haven't seen
如果您尚未看到，继续将是下面描述中的链接

7
00:00:25,949 --> 00:00:29,379
the pointers video definitely watch that okay so pointers and references are two
指针视频绝对可以观看，所以指针和引用是两个

8
00:00:29,579 --> 00:00:34,448
kind of key words that are tossed around a lot in C++ and other languages they're
在C ++和其他语言中被大量使用的一种关键词

9
00:00:34,649 --> 00:00:37,119
essentially the same thing though I just want to start this video by saying that
本质上是一样的，尽管我只是想说这句话开始

10
00:00:37,320 --> 00:00:41,678
pointers and references are pretty much the same thing as far as what the
指针和引用几乎是一样的东西

11
00:00:41,878 --> 00:00:45,549
computer will actually do with them now semantically so how we actually use them
现在计算机实际上将在语义上处理它们，因此我们如何实际使用它们

12
00:00:45,750 --> 00:00:49,299
and how we write them there are some subtle differences but at the end of the
以及我们如何编写它们有一些细微的差异，但是在最后

13
00:00:49,500 --> 00:00:54,369
day references are just pointers usually in disguise that's just all they are
日引用只是伪装的指针，而仅仅是它们

14
00:00:54,570 --> 00:00:58,088
it's just intact sugar on top of pointers to make it a little bit easier
它只是指针上完整的糖，使它更容易一点

15
00:00:58,289 --> 00:01:01,779
to read and a little bit easier to follow a reference is essentially
阅读和更容易遵循参考实质上

16
00:01:01,979 --> 00:01:05,469
exactly what it sounds like it's a way for us to reference an existing variable
这听起来像是我们引用现有变量的一种方式

17
00:01:05,670 --> 00:01:09,939
unlike a pointer where you could create a new pointer variable and then set it
与指针不同，在指针中您可以创建一个新的指针变量然后进行设置

18
00:01:10,140 --> 00:01:13,119
equal to a null pointer or something like that that is equal to zero you
等于空指针或等于零的东西

19
00:01:13,319 --> 00:01:17,259
can't do that with references because references has to reference an already
无法使用引用做到这一点，因为引用必须引用一个已经存在的引用

20
00:01:17,459 --> 00:01:22,179
existing variable references themselves are not new variable they don't really
现有变量引用本身不是新变量，它们并不是真正的

21
00:01:22,379 --> 00:01:27,250
occupy memory they don't really have storage they're not like your typical
占用内存，他们实际上没有存储空间，与您的典型情况不一样

22
00:01:27,450 --> 00:01:31,988
variables because what they are instead is a reference to a variable I'm going
变量，因为它们实际上是对我要使用的变量的引用

23
00:01:32,188 --> 00:01:34,808
to be bringing up a few different examples in this video however I'm going
在这个视频中提出了一些不同的例子，但是我要

24
00:01:35,009 --> 00:01:37,450
to keep it brief because the references aren't that
保持简短，因为引用不是

25
00:01:37,650 --> 00:01:41,319
complicated and the best way really to learn how to use them is actually to
复杂而真正学习如何使用它们的最佳方法实际上是

26
00:01:41,519 --> 00:01:44,590
start using them so adversaries goes on we'll be using them all the time and
开始使用它们，以便对手继续前进，我们将一直使用它们，并且

27
00:01:44,790 --> 00:01:48,159
you'll see what kind of the best practices or at least my opinion on what
您会看到什么样的最佳做法，或者至少是我对什么的看法

28
00:01:48,359 --> 00:01:51,878
the best practice is to do with references however pohjanmaa doesn't
最佳做法是与参考文献建立联系，但pohjanmaa却没有

29
00:01:52,078 --> 00:01:55,509
code let's say that I make a variable I'll call it a and all political decide
代码让我们说一个变量，我将其称为a，所有政治决定

30
00:01:55,709 --> 00:01:59,140
is just going to be an integer if I want to create a reference to that variable I
如果要创建对该变量的引用，它将只是一个整数

31
00:01:59,340 --> 00:02:02,469
can do so by typing type of the variable followed by an
可以通过键入变量的类型，然后输入

32
00:02:02,670 --> 00:02:06,129
ampersand note here that the ampersand is actually part of the variable
在这里，“＆”符号实际上是变量的一部分

33
00:02:06,329 --> 00:02:11,200
declaration in the previous video about pointers we learned that if we create a
在上一个视频中有关指针的声明中，我们了解到，如果我们创建一个

34
00:02:11,400 --> 00:02:15,340
pointer we can use the ampersand operator here to actually take a memory
指针，我们可以在这里使用＆运算符来实际获取内存

35
00:02:15,539 --> 00:02:20,290
address of an existing variable it's different here because the ampersand is
现有变量的地址在这里有所不同，因为“＆”号是

36
00:02:20,490 --> 00:02:23,770
actually part of the type if not you can see it's not actually next to an
如果不是的话，实际上是该类型的一部分，您可以看到它实际上不在旁边

37
00:02:23,969 --> 00:02:27,880
existing variable it's part of the type so just note that difference because
现有变量是类型的一部分，因此请注意区别，因为

38
00:02:28,080 --> 00:02:31,150
with a lot of people as soon as you see an ampersand I think is the reference or
当您看到一个＆符号时，就会与很多人见面，我认为这是参考或

39
00:02:31,349 --> 00:02:35,740
oh it's definitely an address off it's kind of depending on context here so in
哦，这绝对是一个地址，它取决于上下文，因此在

40
00:02:35,939 --> 00:02:38,830
this case because it's next to the type it's a reference so if we keep writing
这种情况是因为它在类型旁边，是引用，所以如果我们继续写

41
00:02:39,030 --> 00:02:42,250
this I'll call this wrap and I'll set this equal to a and that's really all
我将其称为自动换行，并将其设置为等于a，这实际上就是全部

42
00:02:42,449 --> 00:02:46,360
you have to do there's no kind of weird operator or something that we have to
您必须要做的是，没有任何奇怪的运算符或我们必须要做的事情

43
00:02:46,560 --> 00:02:50,260
use here we just set an equal to an existing variable and so what we've done
在这里使用我们只是将一个等于现有变量的值设置为等值，因此我们已经完成了

44
00:02:50,460 --> 00:02:54,280
now is we've essentially created something called an alias because this
现在我们基本上已经创建了一个称为别名的东西，因为

45
00:02:54,479 --> 00:02:58,600
rest variable I say variables in quotes here because it's not really a variable
剩余变量我在这里用引号表示变量，因为它实际上不是变量

46
00:02:58,800 --> 00:03:03,610
it's just a reference like this rest variable doesn't actually exist it just
它只是一个引用，例如rest变量实际上并不存在

47
00:03:03,810 --> 00:03:07,780
exists in our source code if you compile this code right now you're not going to
如果您现在编译此代码，则在我们的源代码中存在，您不会

48
00:03:07,979 --> 00:03:11,650
get two variables created a and rest you're just going to have a what we can
得到两个变量创建一个，剩下的就是我们可以做的

49
00:03:11,849 --> 00:03:16,870
do now is we can use wrap as if it was a so if we set wrap now equal to two and
现在要做的是，我们可以像使用a那样使用wrap，如果我们将wrap设置为等于2并且

50
00:03:17,069 --> 00:03:22,990
then print a we're on our program here you're going to say that a is equal to
然后在我们的程序上打印a，我们将说a等于

51
00:03:23,189 --> 00:03:26,320
two now because we're printing a and we've just changed crap to 2 because for
现在两个，因为我们正在打印a，而我们将废话更改为2，因为

52
00:03:26,520 --> 00:03:31,390
all intents and purposes rest is a we've just created an alias for a and in this
其余的所有意图和目的是一个，我们刚刚为此创建了一个别名

53
00:03:31,590 --> 00:03:34,810
case our reference isn't a pointer or anything like that there's no need for
如果我们的引用不是指针或类似的东西，则不需要

54
00:03:35,009 --> 00:03:38,080
the compiler to actually create a new variable if you compile this code it's
如果您编译此代码，则编译器实际上会创建一个新变量

55
00:03:38,280 --> 00:03:41,980
just going to after this we just set a to 2 because that's really what we did
只是在此之后我们将a设置为2，因为这确实是我们所做的

56
00:03:42,180 --> 00:03:45,189
this is just something that we can write in our source code to make our life
这只是我们可以在源代码中编写的内容，以使我们的生活更美好

57
00:03:45,389 --> 00:03:48,430
easier if we wish to alias a variable now let's try something a little bit
如果我们想给变量起别名现在比较容易，让我们尝试一下

58
00:03:48,629 --> 00:03:51,310
more complicated suppose that we wanted to write a function which was supposed
更复杂的假设是我们想编写一个原本应该

59
00:03:51,509 --> 00:03:54,860
to increment an integer if we just write the function
如果我们只写函数增加一个整数

60
00:03:55,060 --> 00:04:00,980
this and then do something like value plus plus what will actually happen is
然后再做一些类似价值加上实际会发生的事情

61
00:04:01,180 --> 00:04:06,830
if I create my integer over here and then I call increment with a parameter
如果我在此处创建整数，然后使用参数调用增量

62
00:04:07,030 --> 00:04:10,219
what's going to happen here is since we are just passing this by value you can
这里将要发生的是，因为我们只是通过值传递此值，您可以

63
00:04:10,419 --> 00:04:13,368
see that we're not passing this other pointer or as a reference or anything
看到我们没有传递其他指针或作为参考或其他任何东西

64
00:04:13,568 --> 00:04:15,259
like that it's actually going to copy this value
这样，它实际上将复制此值

65
00:04:15,459 --> 00:04:19,069
five each of this function just copy it's going to create a brand new
每个功能五个，只需复制它即可创建一个全新的

66
00:04:19,269 --> 00:04:22,639
variable called value with that so it's almost as if we were to write something
变量称为value，因此几乎就像我们要写一些东西一样

67
00:04:22,839 --> 00:04:27,740
like that that's exactly what will happen so instead of that and I can
就像那样，那将要发生，所以我可以

68
00:04:27,939 --> 00:04:30,980
prove it of course if I just roll my code here and we love this we're
如果我只是在这里滚动代码，当然可以证明这一点，我们喜欢这个， 

69
00:04:31,180 --> 00:04:33,949
actually going to still end up with five printing to the cons well which can stay
实际上最终还是要进行五次印刷，这样才能保持良好状态

70
00:04:34,149 --> 00:04:37,218
over here what I need to do is I actually need to pass this variable by
在这里我需要做的是我实际上需要通过

71
00:04:37,418 --> 00:04:42,050
reference in order for it to increment because what I really want to do is I
为了增加参考，因为我真正想做的是

72
00:04:42,250 --> 00:04:46,189
want to actually affect this variable here so how can I achieve that how can I
想在这里实际影响此变量，所以我该如何实现

73
00:04:46,389 --> 00:04:50,689
actually modify this variable by passing into a function now last time we talked
实际上是通过上次我们谈到的函数来修改此变量

74
00:04:50,889 --> 00:04:54,949
about pointers and remember pointed on memory addresses so theoretically I hope
关于指针，并记住指向内存地址，因此从理论上讲，我希望

75
00:04:55,149 --> 00:04:57,259
you're putting this together in your mind because so exciting but
您将其整合在您的脑海中，因为如此令人兴奋，但

76
00:04:57,459 --> 00:05:01,490
theoretically what we could do really clever right is we could instead of
从理论上讲，我们可以做的非常聪明的事情是我们可以代替

77
00:05:01,689 --> 00:05:05,629
talking the actual value five into that function which could pass the memory
将实际值5传递给可以传递内存的函数

78
00:05:05,829 --> 00:05:09,410
address of this a variable because then what we can do in that function is we
这个变量的地址，因为那么我们可以在该函数中做的就是

79
00:05:09,610 --> 00:05:13,100
could look up that memory address see that value five and then just modify
可以查找该内存地址，看到该值五，然后修改

80
00:05:13,300 --> 00:05:16,730
that that memory address we can write through that memory address since we've
那个内存地址我们可以通过该内存地址写入，因为我们已经

81
00:05:16,930 --> 00:05:19,430
taught that memory resident function look at that shot what although they'll
教了内存常驻功能看那张镜头，尽管他们会

82
00:05:19,629 --> 00:05:24,230
modify this to actually take in a pointer and I'll just also call it value
修改它以实际使用一个指针，我也将其称为值

83
00:05:24,430 --> 00:05:28,670
and then what I'll do here in increment is I'll pass the memory address of a
然后我将在此处递增执行的操作是传递一个

84
00:05:28,870 --> 00:05:32,810
instead of just the value five it's now passing the memory address of this
而不只是值5，它现在正在传递此地址

85
00:05:33,009 --> 00:05:36,770
variable one more thing I'll have to do is actually if you reference the value
变量我要做的另一件事实际上是如果您引用该值

86
00:05:36,970 --> 00:05:39,800
so that we can actually write through that memory instead of modifying the
这样我们就可以真正通过该内存进行写操作，而不用修改

87
00:05:40,000 --> 00:05:44,000
pointer itself if I just do this without the dereference it's going to just
指针本身，如果我不取消引用就执行此操作

88
00:05:44,199 --> 00:05:47,778
increment that address right so points are of course just memory address just
正确增加地址，所以点当然只是内存地址而已

89
00:05:47,978 --> 00:05:51,860
an integer if I do value for cloth without the asterisk without the
如果我对不带星号的布料进行赋值，则为整数

90
00:05:52,060 --> 00:05:55,939
dereference operator then it is just going to increment my memory address not
解引用运算符，那么它只会增加我的内存地址，而不是

91
00:05:56,139 --> 00:05:59,660
the actual value there now because of the order of operations it's actually
由于操作顺序的原因，现在那里的实际值

92
00:05:59,860 --> 00:06:03,090
going to do the increment first the reference so what I want to do
首先要做增量参考，所以我想做

93
00:06:03,290 --> 00:06:07,259
instead is the reference first and then increment because I don't want to
相反，首先是参考，然后增加，因为我不想

94
00:06:07,459 --> 00:06:09,960
increment the pointer and then be reference that I want to dereference the
增加指针，然后引用我要取消引用的

95
00:06:10,160 --> 00:06:13,680
pointer first and then increment the value at Point a so if I compile the
指针，然后在Point a处增加值，所以如果我编译

96
00:06:13,879 --> 00:06:17,009
code on it you will see that I will get do you think printing so awesome we've
上面的代码，您会看到，我认为您会觉得打印如此出色

97
00:06:17,209 --> 00:06:20,189
successfully managed to pop and variable by reference into a function however
通过引用成功地成功弹出和变量化功能

98
00:06:20,389 --> 00:06:25,079
there's many of our references so we can do exactly what we just did here a lot
我们有很多参考资料，因此我们可以做很多我们在这里所做的事情

99
00:06:25,279 --> 00:06:29,639
easier with less code and with less kind of decorating our pin tax if we just use
如果我们只使用更少的代码和更少的装饰类型，便会更容易

100
00:06:29,839 --> 00:06:32,069
a reference and that's what I would do in this situation I would use a
参考，这就是我在这种情况下会使用的

101
00:06:32,269 --> 00:06:36,210
reference so what we can do is rewrite this to take in a reference instead of a
参考，所以我们可以做的就是重写它以获取参考而不是

102
00:06:36,410 --> 00:06:39,449
pointer and then what that means is that I lose this whole need of dereferencing
指针，那意味着我失去了取消引用的全部需求

103
00:06:39,649 --> 00:06:42,930
I can change this back to exactly what it was and then over here I don't need
我可以将其改回原来的样子，然后在这里我不需要

104
00:06:43,129 --> 00:06:46,230
to pass the memory address today I can just pop in a and since it is being
今天通过内存地址，我可以弹出一个，因为它是

105
00:06:46,430 --> 00:06:48,870
passed by reference we basically rewritten the code to do
通过引用传递，我们基本上重写了代码来做

106
00:06:49,069 --> 00:06:53,009
exactly the same thing internally when it gets compiled it will be exactly the
内部完全相同的东西在被编译时将是

107
00:06:53,209 --> 00:06:56,730
same as what we've written before however this time it just looks nicer in
和我们之前写的一样，但是这次看起来更好

108
00:06:56,930 --> 00:07:00,718
our source code again that's the only difference so I run my program you can
我们的源代码再次是唯一的区别，所以我可以运行我的程序

109
00:07:00,918 --> 00:07:05,430
see we get this so that is all the references really are they are just
看到我们得到了这个，所以所有引用实际上就是

110
00:07:05,629 --> 00:07:10,199
syntax childer references there is nothing you can do with a reference that
语法childer引用，使用引用不能做的事

111
00:07:10,399 --> 00:07:13,980
you cannot do with a pointer pointers are like references except they're even
您不能使用指针做操作指针就像引用一样，只是它们甚至

112
00:07:14,180 --> 00:07:17,460
more useful they're even more powerful however if you can get away with using a
更有用，它们甚至更强大，但是如果您可以使用

113
00:07:17,660 --> 00:07:20,910
reference like what we just did here absolutely use a reference because it's
像我们在这里所做的那样的引用绝对使用引用，因为它是

114
00:07:21,110 --> 00:07:25,319
going to be a lot cleaner and simpler to read not the point is are really really
会变得更干净，更容易阅读，不是重点是真的

115
00:07:25,519 --> 00:07:28,800
difficult to read but it will look a lot cleaner in your source code one other
很难阅读，但在源代码中看起来会更加清晰

116
00:07:29,000 --> 00:07:31,439
important thing that I want to mention with references is that once you declare
我想在引用中提及的重要一点是，一旦声明

117
00:07:31,639 --> 00:07:35,790
a reference you cannot change what it references what I mean by that is this
一个参考，你不能改变它所指的是我的意思是

118
00:07:35,990 --> 00:07:41,100
suppose that I had two integers i had a and b and b were set to eight they were
假设我有两个整数，我将a和b和b设置为8，它们分别是

119
00:07:41,300 --> 00:07:45,150
set to five and then what i wanted to do is declare a reference that was set to a
设置为五，然后我想做的就是声明一个设置为

120
00:07:45,350 --> 00:07:49,379
and then maybe later on in my code i decided i actually you know what I want
然后也许以后在我的代码中我决定我实际上你知道我想要什么

121
00:07:49,579 --> 00:07:52,020
to set my reference should be referencing B can I do something like
设置我的参考应该参考B我可以做些什么吗

122
00:07:52,220 --> 00:07:55,170
that the answer is no you can't do that what will happen in this case
答案是否定的，你无法做到在这种情况下会发生什么

123
00:07:55,370 --> 00:07:59,730
is that it will effectively create a reference to a and then when you decide
是它将有效创建对a的引用，然后在您决定

124
00:07:59,930 --> 00:08:02,968
to set that tickled to be it was just that a equal
将其设置为等于

125
00:08:03,168 --> 00:08:07,230
to the value of being which is a so what you will end up with here is a being
达到存在的价值，因此您最终将得到一个存在

126
00:08:07,430 --> 00:08:12,030
equal to a and B being equal to eight that is that's it that's what you'll get
等于a和B等于8那就是你要得到的

127
00:08:12,230 --> 00:08:15,150
and of course because of that that also means that when you declare a reference
当然，因为这也意味着当您声明引用时

128
00:08:15,350 --> 00:08:19,620
you need to actually assign into something you can't just do bit consider
您需要实际分配一些您不能仅仅考虑的东西

129
00:08:19,819 --> 00:08:23,160
to be compiler will not let you do that it requires an initialize when you
被编译器不会让您这样做，它在您需要初始化时

130
00:08:23,360 --> 00:08:26,968
declare a reference you have to immediately assign it something because
声明引用，您必须立即为其分配某些内容，因为

131
00:08:27,168 --> 00:08:29,879
it has to reference something because remember it's not really a real variable
它必须引用一些东西，因为记住它不是真正的变量

132
00:08:30,079 --> 00:08:33,659
at the reference so in this example specifically what would I do if I
在参考中，因此在此示例中，如果我具体怎么办

133
00:08:33,860 --> 00:08:36,059
actually wanted this kind of functionality if I wanted to actually
如果我想真正地想要这种功能

134
00:08:36,259 --> 00:08:39,958
change what rest or the reference of well of course as I mentioned earlier
正如我前面提到的，改变其余的东西或当然是好的参考

135
00:08:40,158 --> 00:08:43,049
this isn't a real variable we would need to create some kind of variable of sorts
这不是真正的变量，我们需要创建某种类型的变量

136
00:08:43,250 --> 00:08:47,279
that we could set up 2.28 first and then later on we could switch it to point to
我们可以先设置2.28，然后再将其切换为指向

137
00:08:47,480 --> 00:08:50,729
be and of course I keep saying the word point in the hope you'll understand that
当然，我一直在说这个单词，希望您能理解

138
00:08:50,929 --> 00:08:54,719
I'm talking about a point so instead of this let's just make this a pointer we
我说的是一个要点，所以让我们将其作为指针代替我们

139
00:08:54,919 --> 00:08:57,689
can initially set it to the memory address of a and then when I wanted to
可以最初将其设置为a的内存地址，然后在我想要

140
00:08:57,889 --> 00:09:01,319
switch to it being pointing to B I can just switch it like this because
切换到它指向BI可以像这样切换它，因为

141
00:09:01,519 --> 00:09:05,909
remember to actually start assessing the value that this pointer is pointing to I
记住要真正开始评估该指针指向的值I 

142
00:09:06,110 --> 00:09:09,449
need to be reference the pointer first and then state vehicle to something like
首先需要引用指针，然后将车辆声明为类似

143
00:09:09,649 --> 00:09:14,339
that so in this case I'm setting a equal to two and in this case I'm setting B
因此，在这种情况下，我将等于2设置为B 

144
00:09:14,539 --> 00:09:21,359
equal to 1 and if I were to log both of these they should be 2 and 1 and you can
等于1，如果我要同时记录这两个值，它们应该分别为2和1，您可以

145
00:09:21,559 --> 00:09:25,199
see this AR all right that's pretty much all I want to say really simple stuff
看到这个AR好吧，这就是我想说的非常简单的东西

146
00:09:25,399 --> 00:09:28,679
will be using reference there's a lot more in the future again they're one of
将会使用参考，将来还会有更多，它们是

147
00:09:28,879 --> 00:09:33,509
those things that you just use all the time like pointers so you'll see a lot
那些经常使用的东西，例如指针，所以您会看到很多

148
00:09:33,710 --> 00:09:37,079
more examples down the road as this variant progresses enjoy this video
随着这个变体的进行，更多的示例将陆续出现，请欣赏此视频

149
00:09:37,279 --> 00:09:40,379
please hit the like button you can follow me on Twitter and Instagram and
请点击“赞”按钮，您可以在Twitter和Instagram上关注我， 

150
00:09:40,580 --> 00:09:44,008
of course if you really enjoy this video you can support me on patreon to make
当然，如果您真的喜欢这个视频，可以在patreon上支持我

151
00:09:44,208 --> 00:09:47,579
sure that I can continue this if applause series videos and make more
如果能鼓掌喝彩，请确保我可以继续做下去，并取得更多

152
00:09:47,779 --> 00:09:50,879
awesome videos in the future all right next time
未来的精彩视频下次都可以

153
00:09:51,080 --> 00:09:55,359
next time we're going to kick it up and off I'll see you guys later goodbye
下次我们要把它踢开了，我以后再见。 

154
00:09:55,559 --> 00:10:01,169
[Music]
[音乐]

155
00:10:04,120 --> 00:10:09,120
[Music]
 [音乐] 

