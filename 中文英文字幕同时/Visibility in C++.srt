﻿1
00:00:00,000 --> 00:00:03,069
Hey look guys my name is eterno and welcome back to my state wealth wealth
嘿，伙计们，我叫eterno，欢迎回到我的州

2
00:00:03,270 --> 00:00:07,089
series today I'm going to be talking all about visibility in sip of blood so
今天的系列赛我将谈论所有关于喝血的能见度

3
00:00:07,290 --> 00:00:10,630
visibility is really a concept that belongs to object-oriented programming
可见性实际上是属于面向对象编程的概念

4
00:00:10,830 --> 00:00:15,519
and really what it refers to is how visible certain members or methods of a
实际上，它所指的是

5
00:00:15,718 --> 00:00:20,409
class actually are and what I mean by visible is who can see them and who can
班级实际上是我，可见的意思是谁可以看到他们，谁可以看到

6
00:00:20,609 --> 00:00:24,429
call them and who can use them and all that stuff so right off the bat I just
打电话给他们，谁可以使用它们以及所有这些东西，所以马上我就

7
00:00:24,629 --> 00:00:28,568
want to mention that visibility is something that has absolutely no effect
想要提到可见度绝对没有任何作用

8
00:00:28,768 --> 00:00:33,159
on how your program actually runs and no effect on program performance or
有关程序实际运行方式的信息，对程序性能没有影响，或者

9
00:00:33,359 --> 00:00:36,969
anything like that it is purely something that exists in the language
类似的东西纯粹是语言中存在的东西

10
00:00:37,170 --> 00:00:41,469
for you to be able to write better code or to help you out with organizing in
使您能够编写更好的代码或帮助您组织

11
00:00:41,670 --> 00:00:46,329
code that is it so there are three basic visibility modifiers that we have in C++
就是这样的代码，因此在C ++中有三个基本的可见性修饰符

12
00:00:46,530 --> 00:00:51,579
private protected and public in other languages such as Java or C sharp there
有其他语言（例如Java或C）的私有保护和公共语言

13
00:00:51,780 --> 00:00:55,989
are other keywords in Java for example you can just not have a visibility
是Java中的其他关键字，例如，您无法看到

14
00:00:56,189 --> 00:00:59,469
modifier and that's kind of called the default visibility modifier in c-sharp
修改器，在c-sharp中称为默认可见性修改器

15
00:00:59,670 --> 00:01:03,969
there's something called internal in super boffo we just have three private
在超级花絮中有一种叫做“内部”的东西，我们只有三个私人

16
00:01:04,170 --> 00:01:07,269
protected public that's it so let's take a look at what they do so
受保护的公众就是这样，让我们​​看一下他们的做法

17
00:01:07,469 --> 00:01:12,039
in a class if I were to just define x and y as two variables like this inside
在类中，如果我只是将x和y定义为像这样的两个变量

18
00:01:12,239 --> 00:01:16,959
a class called entity for example since this is a class the default visibility
例如，称为实体的类，因为这是默认可见性的类

19
00:01:17,159 --> 00:01:20,619
would actually be private meaning that this code here would be the same as if I
实际上是私有的，这意味着这里的代码与我一样

20
00:01:20,819 --> 00:01:24,849
had actually written private explicitly though exactly the same thing is however
实际上已经明确地写成私人的，尽管完全一样的事情是

21
00:01:25,049 --> 00:01:31,149
I write a struct then it would be public by default so the fact that I've not got
我写了一个结构，然后默认情况下它是公共的，所以我还没有

22
00:01:31,349 --> 00:01:35,200
a visibility written here doesn't mean that it doesn't have a visibility it
这里写的可见性并不意味着它没有可见性

23
00:01:35,400 --> 00:01:38,980
does it's just that it implicitly gives it one either public if it's a struct or
仅仅是因为它隐式地给了它一个公共的，如果它是一个结构或

24
00:01:39,180 --> 00:01:41,799
private if it's a class you want to know more about the differences between
私人（如果是一门课），您想更多地了解两者之间的区别

25
00:01:42,000 --> 00:01:44,829
structure and classes I made a video about that so click over here and you
结构和类我为此制作了一个视频，所以请单击此处，您

26
00:01:45,030 --> 00:01:49,000
will see it actually I think it's probably over there run one so let's
会看到它，实际上我认为它可能在那边运行，所以让我们

27
00:01:49,200 --> 00:01:51,698
take the special class and leave these as private what is
参加特殊班级，将这些课程保留为私人课程

28
00:01:51,899 --> 00:01:55,808
private means private means this only this entity class can actually access
私有意味着私有，这意味着只有该实体类才能实际访问

29
00:01:56,009 --> 00:01:59,890
these variables meaning it can read them and it can write them now I should say
这些变量意味着它可以读取它们，并且现在可以写入它们，我应该说

30
00:02:00,090 --> 00:02:02,590
only asterisk because there is actually
仅星号，因为实际上

31
00:02:02,790 --> 00:02:06,518
something called a friend in C++ it's like a whole keyword and everything
在C ++中称为朋友的东西，就像整个关键字一样

32
00:02:06,718 --> 00:02:10,660
called friend and what that actually enables you to do is label a class or a
被称为朋友，而实际上使您能够做的是标记一个类或一个

33
00:02:10,860 --> 00:02:14,500
function as a friend of say this entity class and
充当说这个实体类的朋友，并且

34
00:02:14,699 --> 00:02:18,759
what a friend means is that a friend can actually access private members from
朋友的意思是，朋友实际上可以从以下位置访问私人成员

35
00:02:18,959 --> 00:02:21,580
classes we'll have a whole video on friends in the future so don't worry too
上课以后，我们会在朋友身上播放一整个视频，所以也不必担心

36
00:02:21,780 --> 00:02:26,830
much about that for now so if I just define say a constructor here I can
现在有很多事情，所以如果我在这里定义一个构造函数，我可以

37
00:02:27,030 --> 00:02:33,219
assign X to 0 or something like that however if I were to instantiate the
将X赋给0或类似的东西，但是如果我要实例化

38
00:02:33,419 --> 00:02:37,810
entity here inside main which of course is outside of this class scope I can't
在main内部的实体当然不在该类范围之内

39
00:02:38,009 --> 00:02:42,430
call yo X equal to or something like that because it's actually it's private
称yo X等于或类似，因为它实际上是私有的

40
00:02:42,629 --> 00:02:45,670
if I have a sub class of entities that here we have player which is a subclass
如果我有一个实体的子类，在这里我们有一个播放器，它是一个子类

41
00:02:45,870 --> 00:02:51,039
of entity if I have say a constructor inside player I also actually can't
实体，如果我说过玩家内部的构造函数，我实际上也不能

42
00:02:51,239 --> 00:02:55,689
access X from here because it's marked private meaning only that entity class
从此处访问X，因为将其标记为私有意味着仅该实体类

43
00:02:55,889 --> 00:03:00,700
or friends can actually access those variables the same applies to functions
或朋友实际上可以访问这些变量，对函数也是如此

44
00:03:00,900 --> 00:03:05,680
if I add a function here called print same exact thing like I can call the
如果我在这里添加一个称为print的函数，就像我可以调用

45
00:03:05,879 --> 00:03:10,030
function from within the entity class like so that's totally fine however if I
从实体类内部的功能，所以这是完全可以的，但是如果我

46
00:03:10,229 --> 00:03:15,039
try and do it from a sub class plushes player or from a different place all
尝试从子类绒毛播放器或从其他地方进行操作

47
00:03:15,239 --> 00:03:19,420
together like this I actually won't be able to call it because it's private
像这样在一起我实际上无法调用它，因为它是私有的

48
00:03:19,620 --> 00:03:22,870
ok next step up we have something called protected there is no sane next step up
好吧，下一步，我们有一个被称为保护的东西，没有理智的下一步， 

49
00:03:23,069 --> 00:03:28,659
is because protected is a little bit more visible than private less visible
是因为受保护比私有更不可见

50
00:03:28,859 --> 00:03:33,909
in public so protected means that this class of course there's empty class and
在公共场合受到如此保护意味着这堂课当然是空的

51
00:03:34,109 --> 00:03:39,129
all subclasses along the hierarchy can also access these symbols so you can see
层次结构中的所有子类也可以访问这些符号，因此您可以看到

52
00:03:39,329 --> 00:03:43,360
that now I can totally do X equal to and print inside the player club because
现在我可以完全等于X并在球员俱乐部内打印，因为

53
00:03:43,560 --> 00:03:47,920
player is a soft passive entity however I can't I can't do that from inside main
播放器是一个软的被动实体，但是我不能从主体内部做到这一点

54
00:03:48,120 --> 00:03:51,849
because it because that's completely different function is outside of the
因为那是完全不同的功能

55
00:03:52,049 --> 00:03:56,080
class it's outside of class and it's not part of a soft class and then finally I
上课是在课外，不是软课的一部分，最后我

56
00:03:56,280 --> 00:04:00,368
have public which of course means that the access for all anyone can access it
公开，这当然意味着所有人都可以访问它

57
00:04:00,568 --> 00:04:04,390
I can access it inside the entity class inside the black cloth which is an HT
我可以在HT的黑布里面的实体类中访问它

58
00:04:04,590 --> 00:04:08,800
and inside the main function which is is it is something of its own so there we
在主要功能里面是它自己的东西，所以在那里

59
00:04:09,000 --> 00:04:12,879
go that is the short answer TL DR or TL DW
简短回答TL DR或TL DW 

60
00:04:13,079 --> 00:04:17,500
I guess of what visibility actually is in C++ now let's talk a little bit about
我猜想C ++中实际上是什么能见度，现在让我们谈谈

61
00:04:17,699 --> 00:04:21,590
why you might want to use visibility where you use civility why
为什么要在使用文明的地方使用可见性，为什么

62
00:04:21,790 --> 00:04:25,939
you just make everything public yeah what's the deal with that so first of
你只是公开一切，是的，所以首先

63
00:04:26,139 --> 00:04:32,120
all making everything public is purely a bad idea for the purpose of actually you
公开所有内容纯粹是一个坏主意， 

64
00:04:32,319 --> 00:04:36,590
being a developer and writing code if it's just a matter of style it's just a
作为开发人员并编写代码（如果只是样式问题） 

65
00:04:36,790 --> 00:04:41,449
matter of how can I write code that is easy to maintain easy to understand by
如何编写易于维护，易于理解的代码

66
00:04:41,649 --> 00:04:46,520
people who are going to read this code and maybe extend the code it has nothing
将要阅读此代码并可能扩展其代码的人

67
00:04:46,720 --> 00:04:51,350
to do with performance nothing to do with this will generate different code
与性能无关，与此无关会生成不同的代码

68
00:04:51,550 --> 00:04:55,699
absolutely not visibility is not something that CPU understands is not
绝对不可见性不是CPU所理解的不是

69
00:04:55,899 --> 00:04:58,850
something your computer knows about it's just it's just something that humans
您的计算机知道的一些东西，这只是人类

70
00:04:59,050 --> 00:05:03,319
have invented in order to help other humans and themselves so when I say help
是为了帮助其他人和自己而发明的，所以当我说帮助时

71
00:05:03,519 --> 00:05:08,900
other cubans what I mean is that if you mark something as private that basically
其他古巴我的意思是，如果您将某件商品标记为私人商品， 

72
00:05:09,100 --> 00:05:13,610
tells everyone including yourself hey you shouldn't be accessing this from
告诉所有人，包括您自己，嘿，您不应该从

73
00:05:13,810 --> 00:05:18,230
another class right simple as that or some other code you should only be
像这样简单的另一个类或其他一些代码

74
00:05:18,430 --> 00:05:22,460
accessing this internally inside this class what that means is that if I've
在班级内部访问此内容，这意味着如果我已经

75
00:05:22,660 --> 00:05:27,319
never used a class before in my life and I look at what it contains I should be
我一生中从未使用过一门课，而我看它应该包含的内容

76
00:05:27,519 --> 00:05:32,480
able to say that okay I'm only allowed to actually touch the public stuff
可以说，好的，我只允许实际接触公共物品

77
00:05:32,680 --> 00:05:37,129
that's how I should be using the class that's the proper usage of this class as
这就是我应该如何使用该类的正确用法的类

78
00:05:37,329 --> 00:05:40,879
to actually is to actually call the public functions if I'm using a class
实际上是如果我正在使用一个类，则实际上是调用公共函数

79
00:05:41,079 --> 00:05:45,050
that's part of an API and I'm I look at it and I see a private function that I
这是API的一部分，我正在查看它，然后看到了一个私有函数

80
00:05:45,250 --> 00:05:49,759
want to call I know that I shouldn't be calling that private function the author
我想打电话给我，我不应该叫那个私有函数作者

81
00:05:49,959 --> 00:05:54,199
of that class probably has provided some other means to achieve the same thing
该类的人可能提供了其他方法来实现相同的目的

82
00:05:54,399 --> 00:05:58,280
because if I if I were if I were able to call that private function maybe that
因为如果我能够调用该私有函数，也许

83
00:05:58,480 --> 00:06:01,160
wouldn't give me the result I expect or maybe that would break something else
不会给我我期望的结果，否则可能会破坏其他东西

84
00:06:01,360 --> 00:06:07,759
code is of course this whole like web of complexity and by being able to specify
代码当然就像是复杂的网络，并且能够指定

85
00:06:07,959 --> 00:06:12,170
visibility we can ensure that people don't don't call other codes they
可见性，我们可以确保人们不要拨打其他代码

86
00:06:12,370 --> 00:06:16,040
shouldn't be calling and potentially break things a great little example of
不应该打电话，可能破坏事情，这是一个很好的例子

87
00:06:16,240 --> 00:06:21,020
this is a UI let's just say that I want to move the position of a button if I
这是一个用户界面，我们可以说如果我要移动按钮的位置

88
00:06:21,220 --> 00:06:26,030
just had access to the x and y of the button and I change that variable the
刚刚可以访问按钮的x和y，我更改了该变量

89
00:06:26,230 --> 00:06:29,449
button might not actually move I mean sure the X and y position might be
按钮可能实际上并未移动，我的意思是确定X和y位置可能是

90
00:06:29,649 --> 00:06:32,139
different but in the button to actually move suppose that
不同，但实际上要移动的按钮中

91
00:06:32,339 --> 00:06:36,879
we actually had to refresh our display well suddenly five is set X to five or
实际上，我们不得不突然刷新显示器，将五个X设置为五个，或者

92
00:06:37,079 --> 00:06:40,930
something like yeah the X variable has changed but the display actually doesn't
是的，X变量已更改，但显示实际上并没有

93
00:06:41,129 --> 00:06:45,040
know that it has to grab that a new value from memory if it keeps using the
知道如果继续使用，必须从内存中获取一个新值

94
00:06:45,240 --> 00:06:48,670
old one that it's gone however if I create a method inside that class called
如果我在该类中创建一个称为

95
00:06:48,870 --> 00:06:53,560
set position on set X I can do more than just a sign X to what it needs to be I
设置XI上的位置不仅可以将符号X做到我所需要的

96
00:06:53,759 --> 00:06:56,740
can also call another method called refresh or something like that that
也可以调用另一个称为“刷新”的方法或类似的方法

97
00:06:56,939 --> 00:07:01,180
actually does everything it needs to I can make that X variable itself private
实际上可以做所有我需要做的事情，我可以将X变量本身设为私有

98
00:07:01,379 --> 00:07:06,730
and then that set position or set X function public and then clearly anyone
然后设置位置或将X函数设置为public，然后显然是任何人

99
00:07:06,930 --> 00:07:10,780
who wants to use this code and who reads the code can see that I okay I shouldn't
谁想使用此代码，谁阅读该代码，可以看到我可以，我不应该

100
00:07:10,980 --> 00:07:15,370
be assigning ax directly I can just I should be calling the set axe of the
直接分配斧头，我可以打电话给我

101
00:07:15,569 --> 00:07:18,460
deposition function so that's a cute little example of why you might want to
沉积函数，这是为什么您可能想要做一个可爱的小例子

102
00:07:18,660 --> 00:07:22,060
use digital be all to help other developers and to help yourself it's
尽一切可能使用数字来帮助其他开发人员并帮助自己

103
00:07:22,259 --> 00:07:25,180
amazing how quickly you can forget code that you yourself have actually written
令人惊讶的是，您有多快能忘记自己实际编写的代码

104
00:07:25,379 --> 00:07:29,259
so don't think that okay I'm never working in a team I don't need to I
所以不要以为我永远不会在不需要的团队中工作

105
00:07:29,459 --> 00:07:32,680
don't need to deal with this disability I've spent everything public no if you
不需要处理这种残疾我已经把所有事情都花了

106
00:07:32,879 --> 00:07:35,319
look at your code in a couple months or a couple weeks through and sometimes a
在几个月或几周内查看您的代码，有时

107
00:07:35,519 --> 00:07:40,480
couple days you might forget how it's supposed to work but by using something
几天，您可能会忘记使用它的方式，但是使用某些东西

108
00:07:40,680 --> 00:07:45,370
as simple as visibility you might see what your intended way of accessing and
就像可见性一样简单，您可能会看到您打算使用的访问方式以及

109
00:07:45,569 --> 00:07:50,139
utilizing this this class actually was based on accessibility people argue
利用这个类实际上是基于人们的可及性

110
00:07:50,339 --> 00:07:53,710
about visibility way too much on the internet I don't really want to be one
关于互联网上可见度的问题太多，我真的不想成为一个

111
00:07:53,910 --> 00:07:57,490
of those people I do have my opinions and my beliefs about where you should be
在那些人中，我确实有我的看法和关于应该去哪里的信念。 

112
00:07:57,689 --> 00:08:00,520
using private where should be using public some people for example will
例如在某些人应该使用公共场所的地方使用私人

113
00:08:00,720 --> 00:08:05,439
always write variables as private always and then just have public getters and
总是总是将变量写为private，然后只有public getter和

114
00:08:05,639 --> 00:08:09,310
setters I strongly disagree with that whole approach especially be always part
我强烈不同意整个方法，尤其是始终参与其中

115
00:08:09,509 --> 00:08:13,300
because there's this always is almost never attorney here in programming at
因为这里的编程几乎从来都不是律师

116
00:08:13,500 --> 00:08:19,180
all it's almost always a point in which you actually want to use something in a
所有这几乎总是您实际上想要在

117
00:08:19,379 --> 00:08:22,810
certain way but I will save all those arguments for future videos let me know
以某种方式，但是我将保留所有这些参数以供将来的视频使用

118
00:08:23,009 --> 00:08:26,560
what your thoughts are about this whole visit or everything where you use it why
您对整个访问的想法是什么，或使用它的所有原因，为什么？ 

119
00:08:26,759 --> 00:08:30,100
you use it leave all that in the comment section below and I'll see you guys next
您使用它，将所有内容留在下面的评论部分，接下来我会再见

120
00:08:30,300 --> 00:08:32,840
time goodbye
时间再见

121
00:08:33,120 --> 00:08:36,190
[Music]
[音乐]

122
00:08:40,610 --> 00:08:45,610
[Music]
 [音乐] 

