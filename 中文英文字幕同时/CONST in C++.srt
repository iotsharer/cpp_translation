﻿1
00:00:00,000 --> 00:00:02,500
hey what's up guys my name is a channel welcome back to my state boss boss
嘿，大家好，我叫频道欢迎回到州老板

2
00:00:02,700 --> 00:00:06,129
series today when we talking all about the Const keyword and C++ a lot of
今天的系列，当我们谈论所有关于Const关键字和C ++的内容时， 

3
00:00:06,330 --> 00:00:09,100
people seem really confused by this so hopefully this video will clear things
人们似乎对此感到非常困惑，因此希望这段视频能清除一切

4
00:00:09,300 --> 00:00:13,180
up so consti is more or less what I like to call a bit of a fake keyword because
所以consti或多或少是我喜欢称呼一个假关键词的原因，因为

5
00:00:13,380 --> 00:00:18,070
it doesn't really do much in the scope of changes through the generated code
在通过生成的代码进行的更改范围内，它实际上并没有做太多事情

6
00:00:18,269 --> 00:00:23,019
what it is is kind of like visibility for classes and structs it's just a
它就像是类和结构的可见性

7
00:00:23,219 --> 00:00:27,010
mechanism we get in order to kind of make our code look a little bit cleaner
我们得到的机制是为了使我们的代码看起来更简洁

8
00:00:27,210 --> 00:00:30,789
and for certain rules on developers working without code
对于某些无需代码即可工作的开发人员规则

9
00:00:30,989 --> 00:00:34,329
Const is basically sort of like a promise that you give in which you
 const基本上有点像您给您的诺言

10
00:00:34,530 --> 00:00:38,678
promised that something will be constant that is it's not going to change however
承诺某些东西将是不变的，但是不会改变

11
00:00:38,878 --> 00:00:43,029
it's just a promise and you can bypass that whole promise and you can break
这只是一个诺言，您可以绕过整个诺言，您可以打破

12
00:00:43,229 --> 00:00:46,239
your promise just like you can in real life like when I promise to make like
你的承诺就像你在现实生活中一样

13
00:00:46,439 --> 00:00:52,299
daily videos and they're done yeah but anyway the point is that it's just it's
每天的视频，他们已经完成了，但无论如何，关键是它只是

14
00:00:52,500 --> 00:00:55,989
a promise you promised something to be constant and whether or not you keep
一个诺言，你答应要保持不变，以及你是否遵守

15
00:00:56,189 --> 00:01:00,669
that promise it's kind of up to you but again it's a promise in the sense of you
那个承诺取决于你，但再次是你的承诺

16
00:01:00,869 --> 00:01:05,528
should be keeping that promise and the reason that we want to keep constant Pro
应该遵守那个诺言以及我们要保持不变的原因Pro 

17
00:01:05,728 --> 00:01:08,799
kind of promises is because it can actually help simplify our code a lot
一种承诺是因为它实际上可以大大简化我们的代码

18
00:01:09,000 --> 00:01:12,759
and ask a lot of other benefits which we'll talk about in a minute
并询问许多其他好处，我们将在稍后讨论

19
00:01:12,959 --> 00:01:16,238
so let's just dive into some code so I can show you what I'm talking about if I
因此，让我们深入研究一些代码，以便我可以向您展示我在说什么

20
00:01:16,438 --> 00:01:19,808
declare an integer just like this I'll set it equal to five I'm free to change
像这样声明一个整数，我将其设置为等于5，我可以自由更改

21
00:01:20,009 --> 00:01:23,950
that integer to whatever I like anyway down the road however if I declare this
无论如何我都喜欢那个整数，但是如果我声明这个

22
00:01:24,150 --> 00:01:27,939
is a constant I cannot change it as you can see here so if I writing Const here
是一个常量，我无法更改它，如您在此处看到的，所以如果我在此处编写Const 

23
00:01:28,140 --> 00:01:31,599
you've kind of done a few things first of all you've seen tactically kind of
您首先在战术上已经看过一些事情， 

24
00:01:31,799 --> 00:01:34,988
specified that this a integer here is going to be a constant
指定此整数将是一个常量

25
00:01:35,188 --> 00:01:37,929
I'm not going to modify it this makes a lot of sense if you declare something
我不会修改它，如果您声明某些内容，这很有意义

26
00:01:38,129 --> 00:01:42,128
like max age and set it equal to 90 or something like that you don't really
例如最大年龄并将其设置为90或类似的值

27
00:01:42,328 --> 00:01:46,359
want this to be a variable because well it's not variable you defined a maximum
希望它是一个变量，因为它不是变量，您定义了最大值

28
00:01:46,560 --> 00:01:50,109
age and you're never going to change that that's just kind of a number that
年龄，你永远都不会改变，那只是一个数字

29
00:01:50,310 --> 00:01:53,349
you need to keep around in your program so this is probably the most simple
您需要保留程序中的内容，因此这可能是最简单的

30
00:01:53,549 --> 00:01:58,000
example for how Const can be used it's simply a way to say that I'm declaring a
有关如何使用Const的示例，它只是说我声明一个

31
00:01:58,200 --> 00:02:01,509
variable and I'm not going to modify this variable I don't really want it to
变量，并且我不会修改此变量，我真的不希望它

32
00:02:01,709 --> 00:02:04,869
be a very variable right because the term variable
是一个非常可变的权利，因为术语可变

33
00:02:05,069 --> 00:02:10,780
implies that it can change whereas Const stands for constant which means that
暗示它可以改变，而Const代表常数，这意味着

34
00:02:10,979 --> 00:02:14,260
you're basically declaring a constant instead of a variable something that
您基本上是在声明一个常量而不是一个变量

35
00:02:14,460 --> 00:02:17,620
will not change now there are several other uses for consulates talk about
现在不会改变，领事馆还有其他用途

36
00:02:17,819 --> 00:02:22,000
them the first one applies with pointers when you just bear a pointer so for now
他们第一个适用于带有指针的指针，因为您只带有指针，所以现在

37
00:02:22,199 --> 00:02:25,660
all I'm going to do is create an integer however I'll Predators integer on the
我要做的就是创建一个整数，但是我会在

38
00:02:25,860 --> 00:02:28,510
heap so that we actually get a pointer because this is declared without
堆，以便我们实际上得到一个指针，因为在没有

39
00:02:28,710 --> 00:02:32,710
constant or anything I can do two things here I can dereference a and then
常量或任何我可以做的两件事，在这里我可以取消引用a然后

40
00:02:32,909 --> 00:02:37,410
instead of equal to a value such as two and then of course to get by print a
而不是等于两个，然后通过打印得到

41
00:02:37,610 --> 00:02:41,469
I'll get two and all as well and then the other thing I can do is actually
我会同时得到两个和全部，然后我能做的另一件事是

42
00:02:41,669 --> 00:02:45,910
also reassign the actual pointer so that it points to something else like for
还重新分配实际指针，使其指向诸如

43
00:02:46,110 --> 00:02:50,590
example this max age that I've got here now to bypass this whole constant I can
我现在在这里达到的这个最大年龄可以绕过整个常数

44
00:02:50,789 --> 00:02:54,790
cast this to a normal eight pointer not something you should usually do remember
将其强制转换为普通的八指针，通常不应该记住

45
00:02:54,990 --> 00:02:58,300
how I said that you can kind of break the cost promise this is one of the ways
我怎么说你可以打破成本承诺，这是方法之一

46
00:02:58,500 --> 00:03:01,330
however if you try and do it in this case you can see we've declared a max
但是，如果您尝试在这种情况下执行此操作，则可以看到我们已声明最大

47
00:03:01,530 --> 00:03:04,810
age as an actual constant chances are the compile is actually going to treat
年龄作为编译器实际要处理的恒定机会

48
00:03:05,009 --> 00:03:07,810
that as kind of a read-only constant and if you try and dereference this and
作为只读常量，如果您尝试取消引用它，并且

49
00:03:08,009 --> 00:03:11,469
actually write to it you'll probably get a crash however for this purpose it will
实际写入它，您可能会崩溃，但是为此，它将

50
00:03:11,669 --> 00:03:15,250
still work if I have here five now you'll see that i now get ninety
如果我现在有五个人，仍然可以工作，你会看到我现在有九十岁了

51
00:03:15,449 --> 00:03:18,310
printing because what we've actually done here is reassigned the pointer so
打印，因为我们在这里实际执行的操作被重新分配了指针，因此

52
00:03:18,509 --> 00:03:22,360
we can do two things we can change the contents of the pointer so the contents
我们可以做两件事，我们可以更改指针的内容

53
00:03:22,560 --> 00:03:26,740
at bad memory address but then we can also change which memory address but
在错误的内存地址，但是我们也可以更改哪个内存地址

54
00:03:26,939 --> 00:03:30,250
kind of pointing towards now let's start adding cost everywhere so the first
现在，让我们开始在各地增加成本，因此第一个

55
00:03:30,449 --> 00:03:33,759
thing I can do is put Const just at the front here so which is a Const int
我可以做的就是将Const放在此处的最前面，这是Const int 

56
00:03:33,959 --> 00:03:37,390
pointer what does that mean that means that you cannot modify the contents of
指针是什么意思，这意味着您不能修改其中的内容

57
00:03:37,590 --> 00:03:41,020
that pointer so you can see here that I've created a pointer however when I
该指针，因此您可以在此处看到我创建了一个指针，但是当我

58
00:03:41,219 --> 00:03:45,070
when I try and dereference that pointer and change the value of a you can see
当我尝试取消对该指针的引用并更改a的值时，您会看到

59
00:03:45,270 --> 00:03:49,240
that I can't do that the value of a being the contents at that actual memory
我无法做到的是那个实际内存中内容的值

60
00:03:49,439 --> 00:03:51,969
address however reading a of course is still fine you can see I'll be
地址，但是阅读a当然仍然可以，我可以

61
00:03:52,169 --> 00:03:55,870
referencing it here printing it and I get no errors you can also notice that
在这里引用它进行打印，但我没有错误，您也可以注意到

62
00:03:56,069 --> 00:04:00,939
I'm not getting any kind of error when I try and actually change a so when I
当我尝试并实际上更改a时，我没有收到任何错误

63
00:04:01,139 --> 00:04:04,300
change the pointer aid to point to something else such as Max age that's
更改指针帮助以指向其他内容，例如最大年龄

64
00:04:04,500 --> 00:04:08,020
not a problem I just can't change the contents of that
没问题，我只是无法更改其内容

65
00:04:08,219 --> 00:04:12,099
pointer so the data at that we address the second way that I can use
指针，以便我们处理的第二种方式使用的数据

66
00:04:12,299 --> 00:04:16,749
Const is by putting it after this point assigning like this what this does is
 const是通过把它放在这一点之后，这样分配

67
00:04:16,949 --> 00:04:21,069
kind of the opposite I can change the contents of the pointer but I can't
相反，我可以更改指针的内容，但是我不能

68
00:04:21,269 --> 00:04:25,480
reassign the actual pointer itself to point to something else note that if you
重新分配实际指针本身以指向其他内容，请注意，如果您

69
00:04:25,680 --> 00:04:31,838
put Const over here right so basically it's before the point it's after the int
将Const放在这里，基本上是在int之后

70
00:04:32,038 --> 00:04:37,689
but it's before the pointer this has the exact same functionality as if I would
但它在指针之前具有与我完全相同的功能

71
00:04:37,889 --> 00:04:42,520
have written it like I did before like that right Const in pointer or in cons
像以前一样写过它，就像指针或缺点中的正确const 

72
00:04:42,720 --> 00:04:47,050
pointer they mean the same thing you just be moved constant that the key here
指针，它们表示的是同一件事，只是您将其移动的常数是此处的键

73
00:04:47,250 --> 00:04:51,730
is that it's before the pointer sign right it's before the asterisk whereas
是它在指针标志之前，在星号之前，而

74
00:04:51,930 --> 00:04:55,270
to make the actual pointer constant so that we can't reassign the pointer you
使实际的指针恒定，以便我们无法将指针重新分配给您

75
00:04:55,470 --> 00:04:59,319
need to put it after the asterisk before the variable name to make sure you
需要将其放在变量名之前的星号之后，以确保您

76
00:04:59,519 --> 00:05:02,829
remember that because sometimes you will see people with different kind of
记住，因为有时您会看到不同类型的人

77
00:05:03,029 --> 00:05:06,430
programming styles writing into Const pointer but just know that it's the same
编程风格写入Const指针，但只知道它是相同的

78
00:05:06,629 --> 00:05:11,050
as constant in pointer the difference is when you do endpoint a Const that's the
作为指针中的常量，不同之处是当您终结一个Const时

79
00:05:11,250 --> 00:05:15,040
difference so this is not possible I can't set it equal to anything else like
差异，所以这是不可能的，我不能将其设置为等于其他任何东西

80
00:05:15,240 --> 00:05:19,449
a null pointer or anything I can't erase I actually actual a but I can change the
空指针或任何我无法擦除的东西，实际上我是一个，但我可以更改

81
00:05:19,649 --> 00:05:23,800
contents of what that pointer is pointing to us and finally of course I
该指针指向我们的内容，当然，最后我

82
00:05:24,000 --> 00:05:27,910
can write Const Weiss like this which means that I cannot change the contents
可以这样写Const Weiss，这意味着我不能更改内容

83
00:05:28,110 --> 00:05:31,300
of the pointer and I can't change the actual pointer itself to point to
指针的指针，我无法将实际指针本身更改为指向

84
00:05:31,500 --> 00:05:34,329
something else so that's kind of a second usage of Const when when you're
还有其他事情，所以当您

85
00:05:34,529 --> 00:05:39,069
dealing with pointers you can be talking about the pointer itself or the contents
处理指针时，您可以谈论指针本身或内容

86
00:05:39,269 --> 00:05:43,120
of of where the point is pointing towards and where you put cost here with
指向的方向以及您在此处付出的成本

87
00:05:43,319 --> 00:05:47,290
your declaration whether it's to the left or before the asterisk or after the
您的声明是在左侧还是在星号之前或之后

88
00:05:47,490 --> 00:05:49,389
asterisk as you can see it has a different meaning
如您所见，星号具有不同的含义

89
00:05:49,589 --> 00:05:52,420
now the last meeting a cost that we're going to talk about today is to do with
现在，我们今天要讨论的最后一次会议费用是

90
00:05:52,620 --> 00:05:58,240
classes and methods so that let's write a quick class we will call it entity
类和方法，以便让我们编写一个快速的类，我们将其称为实体

91
00:05:58,439 --> 00:06:03,490
we're going to give it two variables will have my name X what about an image
我们要给它两个变量，将我的名字命名为X。 

92
00:06:03,689 --> 00:06:06,490
like these or anything is just an example and I'm going to attempt to
像这些或其他任何东西都只是一个例子，我将尝试

93
00:06:06,689 --> 00:06:10,749
write getters and setters for this now when I saw already might get X I'll just
现在我为此写吸气剂和吸气剂，当我看到已经可能得到X时， 

94
00:06:10,949 --> 00:06:15,160
make it return X I'm actually going to put Const on the right side of the
让它返回X我实际上要把Const放在右边

95
00:06:15,360 --> 00:06:18,430
method name so after the after any parameters that we might be taking on
方法名称，因此在我们可能要使用的任何参数之后

96
00:06:18,629 --> 00:06:20,970
going right the work Const so this is kind of
正确的工作const，所以这有点

97
00:06:21,170 --> 00:06:24,990
a third usage of if it comes kind of not really to do with a variable but after a
如果它不是真的与变量有关，而是在

98
00:06:25,189 --> 00:06:29,400
method name this only works in a class by the way what this means is that this
方法名只能通过以下方式在类中起作用： 

99
00:06:29,600 --> 00:06:34,259
this method is not going to modify any of the actual plus so you can see we
此方法不会修改任何实际的加号，因此您可以看到我们

100
00:06:34,459 --> 00:06:39,750
cannot modify class member variables if I try and do something like mm x equals
如果尝试执行类似x x的操作，则无法修改类成员变量

101
00:06:39,949 --> 00:06:44,850
2 I'm not going to be able to do that right I've promised that this method is
 2我将无法做到这一点，我已经保证这种方法是

102
00:06:45,050 --> 00:06:48,360
not going to modify the actual class it's just kind of a read-only method
不会修改实际的类，这只是一种只读方法

103
00:06:48,560 --> 00:06:52,290
it's just gonna read data from the US potentially but no modifying is gonna be
只是可能会从美国读取数据，但不会进行任何修改

104
00:06:52,490 --> 00:06:57,360
taking place here so it makes sense to write const with a getter however with a
发生在这里，所以用getter编写const是有意义的，但是用

105
00:06:57,560 --> 00:07:02,430
setter of course if I wanted to have a setter where I set my x value here I'm
设置器，当然，如果我想在这里设置x值的设置器中， 

106
00:07:02,629 --> 00:07:06,120
going to have to write 2x so I can't declare this as constant because
将不得不写2x，所以我不能将其声明为常量，因为

107
00:07:06,319 --> 00:07:10,829
obviously I need to write to the class so this is Const and typically you would
显然，我需要写给类，所以这是Const，通常您会

108
00:07:11,029 --> 00:07:14,790
declare this with a Const now of course if X was a pointer and you wanted it to
当然，如果X是指针，并且现在您想要它，则立即用Const声明

109
00:07:14,990 --> 00:07:18,689
be constant or round to what you could do if we just make s a pointer you could
保持不变或四舍五入，如果我们只是做一个指针，您可以做些什么

110
00:07:18,889 --> 00:07:23,490
do something like Const and the pointer Const get X constant see we've literally
做类似Const的事情，并且指针Const得到X常数，请看我们

111
00:07:23,689 --> 00:07:29,069
got concert in three times on that one line c++ man out rolls so what this
在那一行C ++上，三场音乐会获得了举办，这是怎么回事

112
00:07:29,269 --> 00:07:34,410
means is that we are returning a pointer that cannot be modified the contents of
意味着我们正在返回一个指针，该指针不能被修改的内容

113
00:07:34,610 --> 00:07:39,420
the pointer cannot be modified and this function this method promises not to
指针不能被修改，此函数保证此方法不会

114
00:07:39,620 --> 00:07:43,860
modify the actual entity class so yeah that's a lot of lot of restrictions
修改实际的实体类，是的，这有很多限制

115
00:07:44,060 --> 00:07:47,250
we've put onto this method let's revert this back to not being a pointer one
我们已经使用了此方法，让我们将其还原为不是指针

116
00:07:47,449 --> 00:07:50,340
thing I'll point out just quickly haha point out but anyway one thing I'll
我会很快指出的一件事哈哈指出，但无论如何我会指出一件事

117
00:07:50,540 --> 00:07:55,710
point out really quickly here is that by putting the pointer next to the type
真正快速指出的是，将指针放在类型旁边

118
00:07:55,910 --> 00:07:59,520
like I've done here and this actually become the points I've been my is just
就像我在这里所做的，这实际上成为了我一直以来的要点

119
00:07:59,720 --> 00:08:03,240
still an integer if you want everything to be a pointer on one line like this
如果您希望所有内容都像这样在一行上作为指针，则仍为整数

120
00:08:03,439 --> 00:08:06,449
you actually have to stick a point next to each variable just something I
您实际上必须在每个变量旁边加一个点

121
00:08:06,649 --> 00:08:09,389
thought I'd mention even though it's a bit off-topic because I'm sure people
以为我即使有一点话题也要提到，因为我确定人们

122
00:08:09,589 --> 00:08:12,600
are gonna be a little bit confused by that potentially so I'm reverting back
可能会对此感到困惑，所以我要回头

123
00:08:12,800 --> 00:08:16,949
to just having a normal get up the question is why why would I want to
只是正常起床，问题是为什么我为什么要

124
00:08:17,149 --> 00:08:20,970
declare this as Const like I get that I get that it
像我知道的那样将其声明为Const 

125
00:08:21,170 --> 00:08:25,139
kind of promises not to touch things in this function and maybe if someone else
一种承诺不要触摸此功能中的内容，如果有人

126
00:08:25,339 --> 00:08:28,829
was extending that function they would say okay cool this is not meant to write
正在扩展该功能，他们会说很酷，这不是要写

127
00:08:29,029 --> 00:08:33,929
to the class however does this actually enforce something the answer is yes it
上课，但是这确实强制了一些事情吗，答案是肯定的

128
00:08:34,129 --> 00:08:38,579
does if we had our entity over here in our main class let's just write an
如果我们在主类中有我们的实体，该怎么办，让我们只写一个

129
00:08:38,779 --> 00:08:42,299
actual practical example potentially I created my entity and then I have a
一个实际的实际例子，可能是我创建了我的实体，然后我有一个

130
00:08:42,500 --> 00:08:46,439
function which prints my entity and I wanted to access my get up so all
打印我的实体的功能，我想访问我的床单

131
00:08:46,639 --> 00:08:49,859
there's something like C out will have will pass an entity just like this for
像C这样的东西会像这样传递一个实体

132
00:08:50,059 --> 00:08:56,549
now pay or get X and we've got a pretty reasonable function here now I want to
现在支付或获得X，我们这里有一个相当合理的函数，我想

133
00:08:56,750 --> 00:09:01,049
be able to pass this by constant reference because I don't want to copy
能够通过常量引用传递此信息，因为我不想复制

134
00:09:01,250 --> 00:09:05,309
the entity class again we'll talk about copying and stuff in a future video but
在实体类中，我们将在以后的视频中再次讨论复制和填充

135
00:09:05,509 --> 00:09:08,279
basically I don't want to copy my entity class because that would potentially be
基本上我不想复制我的实体类，因为这可能是

136
00:09:08,480 --> 00:09:12,179
space lobbying in this case it's a base so it probably wouldn't be but in
在这种情况下，太空游说是一个基地，因此可能不是

137
00:09:12,379 --> 00:09:15,149
general I don't want to copy I don't want to be copying all my objects
一般我不想复制我不想复制所有对象

138
00:09:15,350 --> 00:09:18,240
because that will be slow especially for something that's read-only so I want to
因为那会很慢，特别是对于只读的东西，所以我想

139
00:09:18,440 --> 00:09:22,079
be able to pass it by cross reference now here's the thing if I pass this by
现在可以通过交叉引用传递它

140
00:09:22,279 --> 00:09:27,689
constraints it means that this entity is const so just like with pointers if this
约束意味着这个实体是const，就像指针一样

141
00:09:27,889 --> 00:09:33,059
was a pointer i can modify kind of what it's pointing towards so I can set eat a
是一个指针，我可以修改它指向的对象，因此我可以设置

142
00:09:33,259 --> 00:09:39,779
null pointer and that's fine but I cannot actually modify the contents of a
空指针，这很好，但是我实际上不能修改a的内容

143
00:09:39,980 --> 00:09:44,699
so by writing Const reference like this I have the exact same case I cannot
所以通过像这样写const引用，我有完全相同的情况，我不能

144
00:09:44,899 --> 00:09:48,750
modify the entity I can't reassign it something else because remember this
修改实体，我无法将其重新分配，因为请记住这一点

145
00:09:48,950 --> 00:09:52,500
doesn't work like it does with pointers if you reassign this reference you're
如果您重新分配此引用，则不能像使用指针那样工作

146
00:09:52,700 --> 00:09:56,129
actually changing this object not some other object there's no kind of
实际上改变这个对象而不是其他一些对象没有任何

147
00:09:56,330 --> 00:10:00,659
separation between a pointer and the contents of the pointer because with the
指针与指针内容之间的分隔，因为使用

148
00:10:00,860 --> 00:10:04,409
references you are the contents right that's all you can modify those already
引用您是对的内容，就可以修改这些内容

149
00:10:04,610 --> 00:10:08,309
referencing you are that entity even though you're a reference and so the big
引用您是那个实体，即使您是引用，也是如此

150
00:10:08,509 --> 00:10:12,659
thing is I can modify entities so if I remove this Const from this getup
事情是我可以修改实体，所以如果我从这个集合中删除这个const 

151
00:10:12,860 --> 00:10:18,000
suddenly I'm not allowed to call that get X function because this get X
突然我不被允许调用那个get X函数，因为这个get X 

152
00:10:18,200 --> 00:10:22,649
function does not guarantee that it's not going to touch the entity it could
函数不能保证它不会碰到可能的实体

153
00:10:22,850 --> 00:10:26,579
be doing stuff like this so how on earth would that work I'm not modifying the
做这样的事情，所以到底该如何工作，我没有修改

154
00:10:26,779 --> 00:10:30,019
entity directly however calling a method that does modify the
实体，但是直接调用确实会修改

155
00:10:30,220 --> 00:10:35,000
entity that's not allowed so what I have to do is Marcus Const and then what that
不允许的实体，所以我要做的是Marcus Const，然后是

156
00:10:35,200 --> 00:10:39,349
means is that when I have my const entity i can call any cost functions so
就是说，当我有我的const实体时，我可以调用任何成本函数，因此

157
00:10:39,549 --> 00:10:42,319
because of that you'll actually sometimes see two versions of a function
因此，您有时有时会看到一个函数的两个版本

158
00:10:42,519 --> 00:10:46,819
one which just returns X for example with Norah Const and one that returns X
一个只返回X，例如与Norah Const，另一个返回X 

159
00:10:47,019 --> 00:10:50,870
for the constant of course in this case it would be using the Const version or
对于这种情况下的常数，当然会使用Const版本或

160
00:10:51,070 --> 00:10:55,479
the HDX otherwise it will be the other one it looks a bit messy with having two
 HDX，否则将是另一个，它看起来有点混乱，因为有两个

161
00:10:55,679 --> 00:11:00,409
identical functions basically but that's that's how it works so because of that
基本相同的功能，但这就是它的工作原理，因为

162
00:11:00,610 --> 00:11:04,939
remember to always always mark your methods as Const if they don't actually
请记住，如果实际上没有将方法始终标记为Const 

163
00:11:05,139 --> 00:11:08,719
modify the class or if they're not supposed to modify the class because
修改类，或者不应该修改它们，因为

164
00:11:08,919 --> 00:11:11,929
otherwise you'll literally be stopping people from being able to use it if they
否则，您将阻止人们使用它，如果他们

165
00:11:12,129 --> 00:11:16,490
have a Const reference or something like that now in some occasions you do have
现在有一个Const参考或类似的东西

166
00:11:16,690 --> 00:11:19,849
something that is kind of constant you really want to mark the methodist Const
某种恒定的东西，您真的想标记卫理公会

167
00:11:20,049 --> 00:11:25,279
but for some reason you just you just needed to modify some kind of variable
但是由于某种原因，您只需要修改某种变量

168
00:11:25,480 --> 00:11:29,240
so suppose that we maybe had a Tanner a variable here which we just needed to
所以假设我们可能在这里有一个Tanner变量，我们只需要

169
00:11:29,440 --> 00:11:32,990
modify maybe it was something that's just like for debugging or like it
修改也许就像调试或类似的东西

170
00:11:33,190 --> 00:11:35,809
doesn't really affect the program like we still want to mark the methodist
并没有真正影响程序，就像我们仍然要标记卫理公会派一样

171
00:11:36,009 --> 00:11:39,709
cause but we just need to touch this variable well we can do that there's a
原因，但我们只需要很好地触摸此变量，我们就可以做到

172
00:11:39,909 --> 00:11:42,679
keyword and see what's bus called immutable immutable of course means that
关键字，看看什么叫不可变的总线，当然不可变意味着

173
00:11:42,879 --> 00:11:47,599
it's able to be changed so if we make this VAR variable mutable you can see
它可以更改，因此，如果我们使此VAR变量可变，则可以看到

174
00:11:47,799 --> 00:11:51,559
that we are modifying it even though we're inside a constant method right we
即使我们处于常量方法中，我们也正在修改它

175
00:11:51,759 --> 00:11:55,219
can't modify it if it's not mutable but if we mark it as mutable there we go we
如果它不是可变的，则无法对其进行修改，但是如果我们将其标记为可变，我们就可以进行修改

176
00:11:55,419 --> 00:11:57,589
can modify it so hopefully that's another question answered what is
可以修改它，希望这是另一个问题，答案是

177
00:11:57,789 --> 00:11:59,779
mutable that's what mutable is it allows
可变的就是允许的可变

178
00:11:59,980 --> 00:12:04,009
functions which are constant methods which are cost to modify it the variable
函数是不变的方法，需要花费一定的时间来修改变量

179
00:12:04,210 --> 00:12:06,919
anyway I hope you guys enjoyed this video if you did you can hit that like
无论如何，我希望你们喜欢这个视频，如果你能做到的话

180
00:12:07,120 --> 00:12:10,219
button to let me know that you enjoyed this video if you really like this video
按钮，如果您真的喜欢这部影片，请告诉我您喜欢这部影片

181
00:12:10,419 --> 00:12:13,399
you can help support this series on patreon by going to patreon account for
您可以通过访问patreon帐户来帮助在patreon上支持该系列

182
00:12:13,600 --> 00:12:16,549
special teacher no don't get some pretty cool rewards such as being able to
特别老师，不，不会得到一些很酷的奖励，例如能够

183
00:12:16,750 --> 00:12:20,240
contribute to the planning of these episodes and talk about stuff and get
为这些剧集的策划做出贡献，并谈论事物并获得

184
00:12:20,440 --> 00:12:23,599
these episodes early sometimes as well pretty cool stuff and of course you're
这些情节有时很酷，当然， 

185
00:12:23,799 --> 00:12:27,529
helping to support this series if you have any questions about Constance or if
如果您对康斯坦斯有任何疑问，或者

186
00:12:27,730 --> 00:12:30,209
you feel I haven't covered thing and maybe I should in a future
你觉得我还没做完，也许将来我应该做

187
00:12:30,409 --> 00:12:34,379
video just leave a comment below I'll try to answer to as many as I can and I
视频只在下面发表评论，我将尽我所能回答尽可能多的问题

188
00:12:34,580 --> 00:12:40,739
will see you guys next time goodbye [Music]
下次再见。 

189
00:12:45,149 --> 00:12:50,149
[Music]
 [音乐] 

