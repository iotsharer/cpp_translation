﻿1
00:00:00,000 --> 00:00:01,840
嘿，小伙子们，我的名字是切达乳酪，欢迎回到我的发言加系列
hey little guys my name is a cheddar and

2
00:00:02,040 --> 00:00:03,879
嘿，小伙子们，我的名字是切达乳酪，欢迎回到我的发言加系列
welcome back to my say plus plus series

3
00:00:04,080 --> 00:00:04,990
今天我要谈论的是构造成员初始化列表
today I'm going to be talking about

4
00:00:05,190 --> 00:00:07,960
今天我要谈论的是构造成员初始化列表
constructive member initializer lists

5
00:00:08,160 --> 00:00:10,780
这基本上是一个句子，意思是它，所以这是我们的一种方式
that's quite a sentence basically what

6
00:00:10,980 --> 00:00:13,210
这基本上是一个句子，意思是它，所以这是我们的一种方式
it means is it so it's a way for us to

7
00:00:13,410 --> 00:00:16,359
在构造函数中初始化我们的类成员函数，因此当我们编写一个
initialize our class member functions in

8
00:00:16,559 --> 00:00:18,220
在构造函数中初始化我们的类成员函数，因此当我们编写一个
the Constructors so when we write a

9
00:00:18,420 --> 00:00:20,050
类，然后将成员添加到该类中，我们通常需要某种初始化方式
class and we add members to that class

10
00:00:20,250 --> 00:00:21,850
类，然后将成员添加到该类中，我们通常需要某种初始化方式
we usually want some way of initializing

11
00:00:22,050 --> 00:00:24,159
这些成员，通常是在构造函数中完成的，有两种方法
those members and that's usually done in

12
00:00:24,359 --> 00:00:25,720
这些成员，通常是在构造函数中完成的，有两种方法
the constructor and there are two ways

13
00:00:25,920 --> 00:00:28,269
我们真的可以在构造函数中初始化一个类成员，让我们看一下
we can really initialize a class member

14
00:00:28,469 --> 00:00:29,949
我们真的可以在构造函数中初始化一个类成员，让我们看一下
in the constructor let's take a look so

15
00:00:30,149 --> 00:00:31,359
我这里有这个实体类，它只有一个名称字符串，而没有
I've got this entity class here which

16
00:00:31,559 --> 00:00:33,189
我这里有这个实体类，它只有一个名称字符串，而没有
just has a name string it doesn't have

17
00:00:33,390 --> 00:00:34,809
任何构造函数是的，让我们添加其中一个可能是实际上的构造函数
any constructors yes let's add one of

18
00:00:35,009 --> 00:00:36,759
任何构造函数是的，让我们添加其中一个可能是实际上的构造函数
those may be a constructor that actually

19
00:00:36,960 --> 00:00:38,858
以一个名字作为参数，我还将创建另一个构造函数
takes a name in as a parameter I'll also

20
00:00:39,058 --> 00:00:40,538
以一个名字作为参数，我还将创建另一个构造函数
create one more constructor that's just

21
00:00:40,738 --> 00:00:42,759
将会是默认的构造函数，并且不会接受任何参数，因此在此
going to be a default constructor and it

22
00:00:42,960 --> 00:00:44,320
将会是默认的构造函数，并且不会接受任何参数，因此在此
won't take any parameters so in this

23
00:00:44,520 --> 00:00:46,268
情况下，我想为M名称分配此名称的值，因为
case I want to assign M name with value

24
00:00:46,469 --> 00:00:48,428
情况下，我想为M名称分配此名称的值，因为
of this name as being passed in because

25
00:00:48,628 --> 00:00:49,928
我希望能够将我的实体设置为具有通过其传递的名称
I want to be able to set my entity to

26
00:00:50,128 --> 00:00:52,119
我希望能够将我的实体设置为具有通过其传递的名称
have the name that was passed in through

27
00:00:52,320 --> 00:00:54,549
在这种情况下，此参数也许我会做一些简单的事情，然后设置
this parameter in this case maybe I'll

28
00:00:54,750 --> 00:00:56,588
在这种情况下，此参数也许我会做一些简单的事情，然后设置
do something simple and just set the

29
00:00:56,789 --> 00:00:58,570
现在将玩家的名字未知，这似乎很好，这可能是因为
player's name to unknown now this might

30
00:00:58,770 --> 00:00:59,979
现在将玩家的名字未知，这似乎很好，这可能是因为
seem fine and it's probably the way that

31
00:01:00,179 --> 00:01:01,209
您已经在所有语言中进行了此操作，但是在C ++中，实际上
you've been doing this in all the

32
00:01:01,409 --> 00:01:03,309
您已经在所有语言中进行了此操作，但是在C ++中，实际上
languages but in C++ there's actually

33
00:01:03,509 --> 00:01:04,959
一种我们可以做到的方式，这就是为什么我记得初始化一个列表的原因
one other way we could do this and

34
00:01:05,159 --> 00:01:06,730
一种我们可以做到的方式，这就是为什么我记得初始化一个列表的原因
that's why I remember initialize a list

35
00:01:06,930 --> 00:01:08,289
首先，让我们看一下这张卡，说它正在运行并使
first of all let's just take a look at

36
00:01:08,489 --> 00:01:10,000
首先，让我们看一下这张卡，说它正在运行并使
this card and say it running and make

37
00:01:10,200 --> 00:01:11,619
确保它有效，所以我将在这里创建一个新实体，
sure it works so I'm just going to

38
00:01:11,819 --> 00:01:13,539
确保它有效，所以我将在这里创建一个新实体，
create a new entity over here and I'm

39
00:01:13,739 --> 00:01:15,039
要打印该实体的名称，我将把它拉起来更容易展示
going to print the name of that entity

40
00:01:15,239 --> 00:01:17,259
要打印该实体的名称，我将把它拉起来更容易展示
I'll pull this one easier to demonstrate

41
00:01:17,459 --> 00:01:20,289
我的第一种情况，然后是另一种，我给它起个名字
my first kind of case and then another

42
00:01:20,489 --> 00:01:22,509
我的第一种情况，然后是另一种，我给它起个名字
one with a one I'll give this one a name

43
00:01:22,709 --> 00:01:25,238
例如阴影，我们将按f5键，您可以看到在这种情况下我们可以在线打印
such as shadow and we'll hit f5 you can

44
00:01:25,438 --> 00:01:26,859
例如阴影，我们将按f5键，您可以看到在这种情况下我们可以在线打印
see in this case we get online printing

45
00:01:27,060 --> 00:01:28,599
首先在那个节目中，好极了，所以一切似乎都正常，让我们来
first in that show okay great so

46
00:01:28,799 --> 00:01:29,829
首先在那个节目中，好极了，所以一切似乎都正常，让我们来
everything seems to work let's take a

47
00:01:30,030 --> 00:01:31,209
看第二种方法，我们可以初始化它，而不是仅仅设置M名称
look at the second way we can initialize

48
00:01:31,409 --> 00:01:33,189
看第二种方法，我们可以初始化它，而不是仅仅设置M名称
this instead of us just setting M name

49
00:01:33,390 --> 00:01:35,439
等于这里的值，我们可以通过记住初始化列表来做到
equal to the value over here we can do

50
00:01:35,640 --> 00:01:36,730
等于这里的值，我们可以通过记住初始化列表来做到
it via remember initialize the list

51
00:01:36,930 --> 00:01:38,590
在您编写完或之后的B构造之后，它看起来像这样
which looks like this after B construct

52
00:01:38,790 --> 00:01:39,668
在您编写完或之后的B构造之后，它看起来像这样
after or after you write your

53
00:01:39,868 --> 00:01:41,349
构造函数和参数我们可以添加一个冒号，它可以在同一行
constructor and the parameters we can

54
00:01:41,549 --> 00:01:43,869
构造函数和参数我们可以添加一个冒号，它可以在同一行
add a colon this can be on the same line

55
00:01:44,069 --> 00:01:45,369
因为这并不重要，通常我喜欢在下一行写它
as this it doesn't really matter usually

56
00:01:45,569 --> 00:01:47,198
因为这并不重要，通常我喜欢在下一行写它
I like to write it on the next line over

57
00:01:47,399 --> 00:01:49,209
是这样发明的，然后您开始列出您所拥有的成员
here invented like this and then you

58
00:01:49,409 --> 00:01:51,219
是这样发明的，然后您开始列出您所拥有的成员
start listing off the members that you

59
00:01:51,420 --> 00:01:53,109
现在要初始化，在这种情况下，我们只有M名，所以我们只需要
want to initialize now in this case

60
00:01:53,310 --> 00:01:54,730
现在要初始化，在这种情况下，我们只有M名，所以我们只需要
we've just got M name so all we have to

61
00:01:54,930 --> 00:01:56,679
要做的是输入此变量的名称，然后给出
do is type in the name of this variable

62
00:01:56,879 --> 00:01:57,530
要做的是输入此变量的名称，然后给出
and then give

63
00:01:57,730 --> 00:01:59,509
在这种情况下某种价值是未知的，而这种替代取代了
some kind of values in this case unknown

64
00:01:59,709 --> 00:02:02,000
在这种情况下某种价值是未知的，而这种替代取代了
and that kind of replaces the need to do

65
00:02:02,200 --> 00:02:04,009
因此，这就是ich成员所在的列表，如果我们还有另一个成员的话
this so this is what a member in ich

66
00:02:04,209 --> 00:02:05,869
因此，这就是ich成员所在的列表，如果我们还有另一个成员的话
lies the list is now if we had another

67
00:02:06,069 --> 00:02:08,419
诸如分数之类的成员，我们只需添加一个逗号，然后
member such as score or something like

68
00:02:08,618 --> 00:02:10,370
诸如分数之类的成员，我们只需添加一个逗号，然后
that we would just add a comma and then

69
00:02:10,569 --> 00:02:11,990
那个人的价值，但是在这种情况下，我将其初始化为零
the value of that man but so in this

70
00:02:12,189 --> 00:02:14,150
那个人的价值，但是在这种情况下，我将其初始化为零
case I'm initializing it to zero one

71
00:02:14,349 --> 00:02:15,830
很快要注意的是，如果我们像这样定义这些变量
thing to note really quickly is that if

72
00:02:16,030 --> 00:02:17,630
很快要注意的是，如果我们像这样定义这些变量
we had defined these variables like this

73
00:02:17,830 --> 00:02:19,640
在此初始化程序列表中，您应按顺序列出所有成员，然后
in this initializer list you should be

74
00:02:19,840 --> 00:02:21,980
在此初始化程序列表中，您应按顺序列出所有成员，然后
listing off all the members in order and

75
00:02:22,180 --> 00:02:23,900
如果您未按顺序编写代码，一些编译器实际上会警告您，并且
some compilers will actually warn you if

76
00:02:24,099 --> 00:02:25,820
如果您未按顺序编写代码，一些编译器实际上会警告您，并且
you don't write the code in order and

77
00:02:26,020 --> 00:02:27,020
这很重要的原因是因为无论您如何编写初始化程序
the reason this is important is because

78
00:02:27,219 --> 00:02:29,510
这很重要的原因是因为无论您如何编写初始化程序
no matter how you write your initializer

79
00:02:29,710 --> 00:02:32,600
列出它将按照我定义的实际类成员的顺序进行初始化
list it will be initialized in the order

80
00:02:32,800 --> 00:02:34,520
列出它将按照我定义的实际类成员的顺序进行初始化
of the actual class members I defined

81
00:02:34,719 --> 00:02:36,710
因此，在这种情况下，将首先初始化整数，然后再初始化字符串
them so in this case the integer will be

82
00:02:36,909 --> 00:02:38,270
因此，在这种情况下，将首先初始化整数，然后再初始化字符串
initialized first and then the string

83
00:02:38,469 --> 00:02:39,620
即使您在初始化器列表中以其他方式编写它
even if you write it the other way

84
00:02:39,819 --> 00:02:41,390
即使您在初始化器列表中以其他方式编写它
around in the initializer list where you

85
00:02:41,590 --> 00:02:43,219
首先初始化字符串，然后再初始化整数，这样可以导致各种
initialize the string first and then the

86
00:02:43,419 --> 00:02:45,230
首先初始化字符串，然后再初始化整数，这样可以导致各种
integer so this can lead to all sorts of

87
00:02:45,430 --> 00:02:47,150
某种依赖性问题，如果您破坏顺序，那么请确保
kind of dependency problems if you if

88
00:02:47,349 --> 00:02:48,800
某种依赖性问题，如果您破坏顺序，那么请确保
you break that order so just make sure

89
00:02:49,000 --> 00:02:50,030
您始终以与变量相同的顺序初始化变量
that you always initialize your

90
00:02:50,229 --> 00:02:52,160
您始终以与变量相同的顺序初始化变量
variables in the same order that they

91
00:02:52,360 --> 00:02:53,660
在这种情况下声明为成员时声明
declare in when you declare them as

92
00:02:53,860 --> 00:02:55,880
在这种情况下声明为成员时声明
members in this situation it would look

93
00:02:56,080 --> 00:02:57,800
这样，我们就有了自己的名字，然后我们只需输入我们想要的值
like this we have our name and then we

94
00:02:58,000 --> 00:02:59,630
这样，我们就有了自己的名字，然后我们只需输入我们想要的值
just put in the value that we want to

95
00:02:59,830 --> 00:03:02,270
这样复制到这里，这就是我们如何分配它，就是您
copy from into here like this and that's

96
00:03:02,469 --> 00:03:03,830
这样复制到这里，这就是我们如何分配它，就是您
how we assign this it's just you're

97
00:03:04,030 --> 00:03:05,360
基本上用括号代替等号并将其向上移动
basically replacing the equals with

98
00:03:05,560 --> 00:03:07,130
基本上用括号代替等号并将其向上移动
parentheses and moving it up into this

99
00:03:07,330 --> 00:03:09,080
列出如果我们运行我们的代码，您会看到我们得到的结果完全相同，就这样
list if we run our code you'll see we

100
00:03:09,280 --> 00:03:11,450
列出如果我们运行我们的代码，您会看到我们得到的结果完全相同，就这样
get the exact same result okay that's it

101
00:03:11,650 --> 00:03:13,490
那是成员初始值设定项列出的内容确实很容易，但最大的问题是为什么
that's member initializer lists really

102
00:03:13,689 --> 00:03:16,670
那是成员初始值设定项列出的内容确实很容易，但最大的问题是为什么
easy the big question though is why why

103
00:03:16,870 --> 00:03:18,770
做为什么我们应该使用这只是沿海风格的东西，答案很好
do why should we use this is it just a

104
00:03:18,969 --> 00:03:21,200
做为什么我们应该使用这只是沿海风格的东西，答案很好
Coast style thing and the answer is well

105
00:03:21,400 --> 00:03:24,259
是的，没有规范可能是我喜欢编写代码的更正确答案
yes and no norm is probably the more

106
00:03:24,459 --> 00:03:26,360
是的，没有规范可能是我喜欢编写代码的更正确答案
right answer I like writing code like

107
00:03:26,560 --> 00:03:27,950
首先是因为如果您有很多会员
this first of all because if you have a

108
00:03:28,150 --> 00:03:29,900
首先是因为如果您有很多会员
lot of if you have a lot of member

109
00:03:30,099 --> 00:03:32,090
如果您在此处开始对所有变量进行分析，则这些变量可能会变得非常混乱
variables this can get really cluttered

110
00:03:32,289 --> 00:03:33,980
如果您在此处开始对所有变量进行分析，则这些变量可能会变得非常混乱
if you start analyzing them all in here

111
00:03:34,180 --> 00:03:36,110
而且可能很难看到构造函数的实际操作，因为
and it might be hard to see what the

112
00:03:36,310 --> 00:03:38,539
而且可能很难看到构造函数的实际操作，因为
constructor is actually doing because

113
00:03:38,739 --> 00:03:41,509
也许您稍后会有一些其他功能的代码，但是大多数
maybe you've got some code later on that

114
00:03:41,709 --> 00:03:43,310
也许您稍后会有一些其他功能的代码，但是大多数
does other stuff but most of your

115
00:03:43,509 --> 00:03:44,780
构造函数充满了这样的初始化变量
constructor is filled with just

116
00:03:44,979 --> 00:03:46,520
构造函数充满了这样的初始化变量
initializing variables such kind of

117
00:03:46,719 --> 00:03:48,410
您甚至可能不想要的琐碎而无聊的演员表
trivial and boring casts that you

118
00:03:48,610 --> 00:03:49,939
您甚至可能不想要的琐碎而无聊的演员表
probably don't even want you might want

119
00:03:50,139 --> 00:03:52,460
雇用他们，这就是为什么我喜欢将它们放入成员初始值设定项列表的原因
to hire them which is why I like to put

120
00:03:52,659 --> 00:03:53,870
雇用他们，这就是为什么我喜欢将它们放入成员初始值设定项列表的原因
them in the member initializer list

121
00:03:54,069 --> 00:03:55,640
因为从代码风格的角度来看，我只是喜欢，所以我更喜欢它
because just from a code style point of

122
00:03:55,840 --> 00:03:57,469
因为从代码风格的角度来看，我只是喜欢，所以我更喜欢它
view I just I do I like it more it keeps

123
00:03:57,669 --> 00:03:59,420
我在构造函数中的实际代码非常干净而且易于阅读，但是有
my actual code in my constructor very

124
00:03:59,620 --> 00:04:01,759
我在构造函数中的实际代码非常干净而且易于阅读，但是有
very clean and easy to read but there is

125
00:04:01,959 --> 00:04:03,719
实际上是功能上的差异
actually a functional difference

126
00:04:03,919 --> 00:04:05,910
实际上是功能上的差异
different supplies to classes

127
00:04:06,110 --> 00:04:08,610
特别是如果您在编写这样的代码时给我们分配了M名称
specifically if you write code such as

128
00:04:08,810 --> 00:04:10,800
特别是如果您在编写这样的代码时给我们分配了M名称
this where we assign an M name to

129
00:04:11,000 --> 00:04:12,840
这里有些未知的东西，您没有任何成员初始化程序
something here like unknown and you

130
00:04:13,039 --> 00:04:14,610
这里有些未知的东西，您没有任何成员初始化程序
don't have it any member initializer

131
00:04:14,810 --> 00:04:16,620
像这样的列表，实际发生的是这个M名称对象
list like so what will actually happen

132
00:04:16,819 --> 00:04:18,780
像这样的列表，实际发生的是这个M名称对象
is this M name object will be

133
00:04:18,980 --> 00:04:21,210
用默认构造函数构造两次，然后再用此构造一次
constructed twice once with the default

134
00:04:21,410 --> 00:04:24,600
用默认构造函数构造两次，然后再用此构造一次
constructor and then again with this

135
00:04:24,800 --> 00:04:26,189
参数未知，因为实际上发生的是这个，所以您
unknown parameter because what's

136
00:04:26,389 --> 00:04:28,949
参数未知，因为实际上发生的是这个，所以您
happening here is actually this so you

137
00:04:29,149 --> 00:04:30,480
只需记入其中的两个字符串，您就已经丢掉了
just credit two strings one of them

138
00:04:30,680 --> 00:04:32,250
只需记入其中的两个字符串，您就已经丢掉了
you've just thrown away pretty much

139
00:04:32,449 --> 00:04:34,680
马上就浪费了性能，让我们
straight away right it's a waste of

140
00:04:34,879 --> 00:04:35,968
马上就浪费了性能，让我们
performance right there let's

141
00:04:36,168 --> 00:04:37,770
演示这一点，我将在此处创建一个名为example的类
demonstrate this I'm going to make a

142
00:04:37,970 --> 00:04:39,540
演示这一点，我将在此处创建一个名为example的类
class here called example it's going to

143
00:04:39,740 --> 00:04:41,520
非常非常简单，这里只有一个公共构造函数，我们将进行这种
be very very simple just one public

144
00:04:41,720 --> 00:04:43,230
非常非常简单，这里只有一个公共构造函数，我们将进行这种
constructor here we'll make this kind of

145
00:04:43,430 --> 00:04:44,730
我想我将在这里打印创建的实体，更多的是真实示例
more of a real-world example I guess

146
00:04:44,930 --> 00:04:47,670
我想我将在这里打印创建的实体，更多的是真实示例
I'll just print created entity over here

147
00:04:47,870 --> 00:04:50,370
然后我还将创建一个示例，该示例实际上包含一个参数，
and then I'll also create one example

148
00:04:50,569 --> 00:04:52,620
然后我还将创建一个示例，该示例实际上包含一个参数，
which actually takes in a parameter and

149
00:04:52,819 --> 00:04:55,528
我将用Prince评级实体，并在那个X变量中向下滚动
I'll Prince rated entity with and in

150
00:04:55,728 --> 00:04:58,259
我将用Prince评级实体，并在那个X变量中向下滚动
that X variable I'm going to scroll down

151
00:04:58,459 --> 00:05:01,468
在这里摆脱所有这些X的东西，我将把这个示例类添加为
here get rid of all this X stuff I'm

152
00:05:01,668 --> 00:05:03,389
在这里摆脱所有这些X的东西，我将把这个示例类添加为
going to add this example class as an

153
00:05:03,589 --> 00:05:05,850
实际的班级成员，在本法院示例中，我称其为m
actual class member over here I'll call

154
00:05:06,050 --> 00:05:08,040
实际的班级成员，在本法院示例中，我称其为m
it m on this Court example to adhere to

155
00:05:08,240 --> 00:05:09,990
我的约定，这就是整个类，它有两个构造函数
my convention so this is it that's the

156
00:05:10,189 --> 00:05:11,460
我的约定，这就是整个类，它有两个构造函数
whole class it's got two constructors

157
00:05:11,660 --> 00:05:13,410
一个不带参数的参数，另一个不带参数的参数
one which takes no parameters and one

158
00:05:13,610 --> 00:05:15,150
一个不带参数的参数，另一个不带参数的参数
which takes this s and what I really

159
00:05:15,350 --> 00:05:17,460
想做的是在我的实体的默认构造函数中，我的构造函数不需要
want to do is in my default constructor

160
00:05:17,660 --> 00:05:19,439
想做的是在我的实体的默认构造函数中，我的构造函数不需要
for entity my constructor which takes no

161
00:05:19,639 --> 00:05:22,199
我只想将example设置为等于实际的example对象的参数
parameters I just want to set example

162
00:05:22,399 --> 00:05:24,990
我只想将example设置为等于实际的example对象的参数
equal to an example object that actually

163
00:05:25,189 --> 00:05:27,990
正确输入8，所以如果我来到这里，并确保我确实在创建
takes in 8 right so if I come down here

164
00:05:28,189 --> 00:05:29,730
正确输入8，所以如果我来到这里，并确保我确实在创建
and I make sure I'm actually creating

165
00:05:29,930 --> 00:05:30,870
这让我们摆脱另一个例子，我们将打印它，我们摆脱
this let's get rid of this other example

166
00:05:31,069 --> 00:05:32,338
这让我们摆脱另一个例子，我们将打印它，我们摆脱
we're going to print this and we get rid

167
00:05:32,538 --> 00:05:33,810
打印它的名字我在这里要做的只是创建一个实例
of the printing it get name all I'm

168
00:05:34,009 --> 00:05:35,610
打印它的名字我在这里要做的只是创建一个实例
doing here is just creating an instance

169
00:05:35,810 --> 00:05:37,560
这个默认构造函数的实体对象
of that entity object with this default

170
00:05:37,759 --> 00:05:38,278
这个默认构造函数的实体对象
constructor

171
00:05:38,478 --> 00:05:40,740
如果我按f5来运行代码，请看我们一次创建了两个实体
if I hit f5 to run my code look at that

172
00:05:40,939 --> 00:05:42,990
如果我按f5来运行代码，请看我们一次创建了两个实体
we created two entities once with the

173
00:05:43,189 --> 00:05:44,730
默认构造函数，不带任何参数，一次带有
default constructor the one that takes

174
00:05:44,930 --> 00:05:46,528
默认构造函数，不带任何参数，一次带有
no parameters and once with the

175
00:05:46,728 --> 00:05:48,990
用整数构造技术人员，因此我们实际上创建了两个实体
constructor the techs in an integer so

176
00:05:49,189 --> 00:05:51,150
用整数构造技术人员，因此我们实际上创建了两个实体
we've actually created two entities

177
00:05:51,350 --> 00:05:54,060
对的就是在这里创建的，就好像我们这样写对，我的意思是
right one was created up here it's as if

178
00:05:54,259 --> 00:05:57,300
对的就是在这里创建的，就好像我们这样写对，我的意思是
we wrote this like this right I mean it

179
00:05:57,500 --> 00:05:59,879
为什么在这里创建一个实体，为什么不仅仅在这里创建一个示例呢？
created an entity here why wouldn't it

180
00:06:00,079 --> 00:06:01,410
为什么在这里创建一个实体，为什么不仅仅在这里创建一个示例呢？
create an example up here just because

181
00:06:01,610 --> 00:06:03,300
处于这种成员区域并不意味着它不会运行
it's in this kind of member region

182
00:06:03,500 --> 00:06:05,790
处于这种成员区域并不意味着它不会运行
doesn't mean that it's not going to run

183
00:06:05,990 --> 00:06:06,119
这并创建，然后我们还创建了
this

184
00:06:06,319 --> 00:06:08,309
这并创建，然后我们还创建了
and created and then we've also created

185
00:06:08,509 --> 00:06:11,338
这里有一个新的示例实例，并将其分配给旧的实例，所以我们扔掉了
a new example instance here and assign

186
00:06:11,538 --> 00:06:12,660
这里有一个新的示例实例，并将其分配给旧的实例，所以我们扔掉了
it to the old one so we've thrown away

187
00:06:12,860 --> 00:06:14,338
旧的我们刚刚创建了一个示例类实例，
the old one we've just created an

188
00:06:14,538 --> 00:06:16,079
旧的我们刚刚创建了一个示例类实例，
example class instance throwing that

189
00:06:16,278 --> 00:06:17,759
走开，然后用一个新的覆盖它，我们创建了两个对象，而不是
away and just overwritten it with a new

190
00:06:17,959 --> 00:06:19,499
走开，然后用一个新的覆盖它，我们创建了两个对象，而不是
one we've created two objects instead of

191
00:06:19,699 --> 00:06:22,889
只是一项权利，而如果我们要将其移到初始化列表中
just one right whereas if we were to

192
00:06:23,088 --> 00:06:25,709
只是一项权利，而如果我们要将其移到初始化列表中
move this into the initializer list

193
00:06:25,908 --> 00:06:27,809
这里有两个选择，我们可以编写与我们完全相同的代码
there's two options for us here we could

194
00:06:28,009 --> 00:06:29,759
这里有两个选择，我们可以编写与我们完全相同的代码
either write exactly the same code as we

195
00:06:29,959 --> 00:06:30,269
以前是这样的，如果我打到五点，你会
did before

196
00:06:30,468 --> 00:06:32,549
以前是这样的，如果我打到五点，你会
like this and if I hit at five you'll

197
00:06:32,749 --> 00:06:34,528
当然，我们只是运行我们要创建的构造函数
see of course we just we just run the

198
00:06:34,728 --> 00:06:36,269
当然，我们只是运行我们要创建的构造函数
constructor that we want we create one

199
00:06:36,468 --> 00:06:38,429
st实例，或者我什至可以摆脱它，只传递参数
st instance or I could even just get rid

200
00:06:38,629 --> 00:06:40,169
st实例，或者我什至可以摆脱它，只传递参数
of this and just passing the parameters

201
00:06:40,369 --> 00:06:42,299
这样，只要点击一个文件，您就可以看到完全一样的事情
like this and just hit a file and you

202
00:06:42,499 --> 00:06:43,949
这样，只要点击一个文件，您就可以看到完全一样的事情
can see that does exactly the same thing

203
00:06:44,149 --> 00:06:45,660
所以你去了那就是你应该使用成员的权利
so there you go that's the difference

204
00:06:45,860 --> 00:06:48,119
所以你去了那就是你应该使用成员的权利
right you should be using member

205
00:06:48,319 --> 00:06:50,790
初始化程序随处可见，绝对没有理由不使用
initializer lists everywhere right

206
00:06:50,990 --> 00:06:52,679
初始化程序随处可见，绝对没有理由不使用
there's absolutely no reason not to use

207
00:06:52,879 --> 00:06:53,910
如果您不喜欢代码样式，请习惯它们，因为这不只是一种
them if you don't like the code style

208
00:06:54,110 --> 00:06:55,709
如果您不喜欢代码样式，请习惯它们，因为这不只是一种
get used to them because it's not just a

209
00:06:55,908 --> 00:06:57,569
样式问题实际上实际上是功能上的差异
matter of style it's actually there's

210
00:06:57,769 --> 00:06:58,859
样式问题实际上实际上是功能上的差异
actually a functional difference you'll

211
00:06:59,059 --> 00:07:00,778
如果您不使用它们，那实际上就是在浪费性能
be literally wasting performance if

212
00:07:00,978 --> 00:07:02,639
如果您不使用它们，那实际上就是在浪费性能
you're not using them of course not in

213
00:07:02,838 --> 00:07:04,199
在原始类型（例如整数）的情况下，所有情况都不会
all cases in the case of primitive types

214
00:07:04,399 --> 00:07:06,149
在原始类型（例如整数）的情况下，所有情况都不会
like integers you it wouldn't be

215
00:07:06,348 --> 00:07:07,919
初始化它们，直到您自己分配它们进行初始化为止，但我使用
initialized them until you initialize

216
00:07:08,119 --> 00:07:09,899
初始化它们，直到您自己分配它们进行初始化为止，但我使用
them yourself by assigning it but I use

217
00:07:10,098 --> 00:07:11,519
它无处不在我不区分类类型中的原始类
it everywhere I don't discriminate

218
00:07:11,718 --> 00:07:13,410
它无处不在我不区分类类型中的原始类
between primitive classes in class types

219
00:07:13,610 --> 00:07:15,088
您应该在任何地方都使用这样的初始化列表，但无论如何
you should just be using initialized

220
00:07:15,288 --> 00:07:16,859
您应该在任何地方都使用这样的初始化列表，但无论如何
lists like this everywhere but anyway

221
00:07:17,059 --> 00:07:18,778
这将总结我的C ++系列今天的节目，看着我​​说今天
that's gonna wrap up today's episode of

222
00:07:18,978 --> 00:07:21,149
这将总结我的C ++系列今天的节目，看着我​​说今天
my C++ series look at me saying today's

223
00:07:21,348 --> 00:07:23,069
现在好像这是每天的一集，我认为这是连续第三天
episode as if this is daily now I think

224
00:07:23,269 --> 00:07:24,629
现在好像这是每天的一集，我认为这是连续第三天
this is the third day in a row that's

225
00:07:24,829 --> 00:07:26,910
为什么如果你们欣赏我的表现很好，请打这样的
why I'm doing pretty well if you guys

226
00:07:27,110 --> 00:07:28,949
为什么如果你们欣赏我的表现很好，请打这样的
appreciate that please hit that like

227
00:07:29,149 --> 00:07:30,389
按钮，如果你想支持这个系列，可以去礼堂
button if you want to support the series

228
00:07:30,588 --> 00:07:31,920
按钮，如果你想支持这个系列，可以去礼堂
you can go over to patreon the concourse

229
00:07:32,120 --> 00:07:33,540
在rachona我设置了一些很酷的奖励，我实际上是
at rachona I've got some pretty cool

230
00:07:33,740 --> 00:07:35,459
在rachona我设置了一些很酷的奖励，我实际上是
rewards set up I'm actually kind of

231
00:07:35,658 --> 00:07:38,309
尝试一点不和谐的服务器，使patreon奖励包括您可以
trying a bit of a discord server so

232
00:07:38,509 --> 00:07:40,528
尝试一点不和谐的服务器，使patreon奖励包括您可以
patreon rewards include that you can

233
00:07:40,728 --> 00:07:41,879
当然也可以免费加入，但下面的说明中的链接当然像
also join it for free though of course

234
00:07:42,079 --> 00:07:43,528
当然也可以免费加入，但下面的说明中的链接当然像
link in the description below like a

235
00:07:43,728 --> 00:07:45,660
不和谐的事情，但在patreon上，您也可以在该服务器上获得一些不错的选择
discord thing but on patreon you get

236
00:07:45,860 --> 00:07:47,910
不和谐的事情，但在patreon上，您也可以在该服务器上获得一些不错的选择
some cool rolls on that server as well

237
00:07:48,110 --> 00:07:50,459
当它们准备好就可以尽早访问其中一些视频
as access to some of these videos early

238
00:07:50,658 --> 00:07:52,528
当它们准备好就可以尽早访问其中一些视频
when they happen to be ready early

239
00:07:52,728 --> 00:07:55,859
因为有时不是，但是在那些时候，它们可以作为您使用
because sometimes that just not but for

240
00:07:56,059 --> 00:07:57,509
因为有时不是，但是在那些时候，它们可以作为您使用
those times they are available to you as

241
00:07:57,709 --> 00:07:59,088
我基本上完成了对它们的编辑以及
soon as I basically finish editing them

242
00:07:59,288 --> 00:08:02,270
我基本上完成了对它们的编辑以及
as well as like a

243
00:08:02,470 --> 00:08:04,670
我们有一个私人不和谐/闲散的团体，我们可以在其中谈论
we've got like a private discord / slack

244
00:08:04,870 --> 00:08:07,150
我们有一个私人不和谐/闲散的团体，我们可以在其中谈论
kind of group in which we can talk about

245
00:08:07,350 --> 00:08:10,310
尽可能直接地为我们节省外部巴士站的费用
save us bus stop external just directly

246
00:08:10,509 --> 00:08:11,930
尽可能直接地为我们节省外部巴士站的费用
as well as as well as you guys can

247
00:08:12,129 --> 00:08:14,150
建议我制作新主题和视频，这就是我通常喜欢的
suggest new topics and videos for me to

248
00:08:14,350 --> 00:08:15,650
建议我制作新主题和视频，这就是我通常喜欢的
make and that's what I usually do like

249
00:08:15,850 --> 00:08:17,720
这是这里，所以下次见。再见
this one here so I'll see you guys next

250
00:08:17,920 --> 00:08:19,800
这是这里，所以下次见。再见
time good bye

251
00:08:20,000 --> 00:08:24,110
[音乐]
[Music]

252
00:08:28,519 --> 00:08:33,519
[音乐]
[Music]

