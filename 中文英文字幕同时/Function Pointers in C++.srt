﻿1
00:00:00,030 --> 00:00:01,480
嘿，大家好，我叫乔纳，欢迎回到我的州，再加上
hey what's up guys my name is Jonah

2
00:00:01,679 --> 00:00:03,009
嘿，大家好，我叫乔纳，欢迎回到我的州，再加上
welcome back to my state plus plus

3
00:00:03,209 --> 00:00:04,899
系列，所以今天我们要讨论的是C ++和
series so today we're gonna be talking

4
00:00:05,099 --> 00:00:07,629
系列，所以今天我们要讨论的是C ++和
all about function pointers in C++ and

5
00:00:07,830 --> 00:00:09,310
具体地说，我们将要讨论的是原始样式函数的种类
specifically we're gonna be talking

6
00:00:09,509 --> 00:00:11,108
具体地说，我们将要讨论的是原始样式函数的种类
about the kind of raw style function

7
00:00:11,308 --> 00:00:13,269
真正来自C的指针，所以不要以为我是
pointers that really come from C so

8
00:00:13,468 --> 00:00:14,380
真正来自C的指针，所以不要以为我是
don't think that I'm kind of being

9
00:00:14,580 --> 00:00:15,760
误导性或类似的内容，我们将进一步采用这种C ++方式
misleading or anything like that we are

10
00:00:15,960 --> 00:00:18,280
误导性或类似的内容，我们将进一步采用这种C ++方式
going to get into more this C++ way to

11
00:00:18,480 --> 00:00:19,989
在后面的视频中做函数指针以及诸如lambda之类的东西
do function pointers in a later video

12
00:00:20,189 --> 00:00:22,510
在后面的视频中做函数指针以及诸如lambda之类的东西
along with things like lambdas which are

13
00:00:22,710 --> 00:00:24,400
匿名函数，没有正式的函数定义
anonymous functions kind of without a

14
00:00:24,600 --> 00:00:26,560
匿名函数，没有正式的函数定义
formal function definition and all that

15
00:00:26,760 --> 00:00:28,269
我们将逐步了解的东西，但首先我想从善意开始
stuff we'll get into that as we go along

16
00:00:28,469 --> 00:00:30,039
我们将逐步了解的东西，但首先我想从善意开始
but first I want to start off with kind

17
00:00:30,239 --> 00:00:31,898
最原始的东西，我喜欢称之为原始
of the most primitive thing which is

18
00:00:32,098 --> 00:00:33,549
最原始的东西，我喜欢称之为原始
kind of what I like to call a raw

19
00:00:33,750 --> 00:00:35,409
函数指针又来自安全，所以什么是函数指针
function pointer which again comes from

20
00:00:35,609 --> 00:00:38,018
函数指针又来自安全，所以什么是函数指针
safe so what is a function pointer so

21
00:00:38,219 --> 00:00:39,250
如果你们还没有看到链接的视频，我们已经了解了指针
we've learned about pointers if you guys

22
00:00:39,450 --> 00:00:40,750
如果你们还没有看到链接的视频，我们已经了解了指针
haven't seen that video it's linked up

23
00:00:40,950 --> 00:00:41,858
确保您了解不一定需要的指针
there make sure you understand what

24
00:00:42,058 --> 00:00:43,599
确保您了解不一定需要的指针
pointers are you don't necessarily need

25
00:00:43,799 --> 00:00:45,309
了解内存的工作方式或C ++中的指针如何工作以达到某种使用目的
to understand how memory works or how

26
00:00:45,509 --> 00:00:47,890
了解内存的工作方式或C ++中的指针如何工作以达到某种使用目的
pointers working in C++ to kind of use

27
00:00:48,090 --> 00:00:49,989
函数指针，但是您确实确实需要观看该视频
function pointers but you definitely do

28
00:00:50,189 --> 00:00:51,338
函数指针，但是您确实确实需要观看该视频
need to watch that video so definitely

29
00:00:51,539 --> 00:00:52,178
检查出来，但基本上函数指针只是
check it out

30
00:00:52,378 --> 00:00:54,489
检查出来，但基本上函数指针只是
but basically function pointers are just

31
00:00:54,689 --> 00:00:57,509
一种将函数分配给变量的方法，因此通常我们一直在使用函数
a way to assign a function to a variable

32
00:00:57,710 --> 00:01:00,788
一种将函数分配给变量的方法，因此通常我们一直在使用函数
so functions usually as we've been using

33
00:01:00,988 --> 00:01:01,838
现在，如果您不知道函数是什么，那么我确实做了一个
them now if you don't know what a

34
00:01:02,039 --> 00:01:03,070
现在，如果您不知道函数是什么，那么我确实做了一个
function is by the way I did make a

35
00:01:03,270 --> 00:01:04,748
前段时间的视频，所以肯定也要检查一下，但是
video on that a while back so definitely

36
00:01:04,948 --> 00:01:06,849
前段时间的视频，所以肯定也要检查一下，但是
check that out as well but the way that

37
00:01:07,049 --> 00:01:08,409
到目前为止，我们一直在使用函数，就像
we've kind of been using functions so

38
00:01:08,609 --> 00:01:11,500
到目前为止，我们一直在使用函数，就像
far has just been like as something that

39
00:01:11,700 --> 00:01:14,319
我们称它不是，我们真的不喜欢用任何逻辑
we call right it's not it we can't

40
00:01:14,519 --> 00:01:17,079
我们称它不是，我们真的不喜欢用任何逻辑
really like do any sort of logic with

41
00:01:17,280 --> 00:01:21,250
功能就像一个符号，当我们想要某些东西时我们可以调用它
functions it's just like a symbol that

42
00:01:21,450 --> 00:01:23,379
功能就像一个符号，当我们想要某些东西时我们可以调用它
we can call when we want something to

43
00:01:23,579 --> 00:01:25,090
发生，我们当然可以给它提供参数，然后接收一些东西
happen and we can of course give it

44
00:01:25,290 --> 00:01:27,879
发生，我们当然可以给它提供参数，然后接收一些东西
parameters and in turn receive something

45
00:01:28,079 --> 00:01:29,469
作为回报，如果我们编写了一个函数，该函数返回一些东西并且无效，那么
in return if we write a function that

46
00:01:29,670 --> 00:01:32,168
作为回报，如果我们编写了一个函数，该函数返回一些东西并且无效，那么
returns something out and void so that's

47
00:01:32,368 --> 00:01:33,609
到目前为止，我们如何使用函数，但是您实际上可以分配
how we've been using functions so far

48
00:01:33,810 --> 00:01:36,009
到目前为止，我们如何使用函数，但是您实际上可以分配
however you can actually assign

49
00:01:36,209 --> 00:01:39,099
函数到变量，并希望从中扩展，您也可以通过
functions to variables and like to

50
00:01:39,299 --> 00:01:40,719
函数到变量，并希望从中扩展，您也可以通过
extend from that you can also pass

51
00:01:40,920 --> 00:01:42,819
函数作为参数转换为其他函数，并且有一堆
functions into other functions as

52
00:01:43,019 --> 00:01:44,619
函数作为参数转换为其他函数，并且有一堆
parameters and there's a whole bunch of

53
00:01:44,819 --> 00:01:45,759
实际上可以为某些功能创建的功能
things that you can actually do with

54
00:01:45,959 --> 00:01:47,709
实际上可以为某些功能创建的功能
functions that really creates for some

55
00:01:47,909 --> 00:01:49,750
有趣而复杂的逻辑，否则将非常混乱
interesting and complex logic that would

56
00:01:49,950 --> 00:01:51,878
有趣而复杂的逻辑，否则将非常混乱
otherwise be extremely messy to do it

57
00:01:52,078 --> 00:01:54,789
真正清除所有东西，因此是演示功能的最佳方法
really cleans all of a stuff up so the

58
00:01:54,989 --> 00:01:57,159
真正清除所有东西，因此是演示功能的最佳方法
best way to demonstrate what a function

59
00:01:57,359 --> 00:01:58,899
指针是以及如何使用它，它可以做什么显然是通过一个示例完成的，因此
pointer is and how to use it and what it

60
00:01:59,099 --> 00:02:01,238
指针是以及如何使用它，它可以做什么显然是通过一个示例完成的，因此
can do is obviously by an example so

61
00:02:01,438 --> 00:02:03,128
让我们看一下，所以我要做的第一件事是编写一个函数
let's take a look so the first thing I'm

62
00:02:03,328 --> 00:02:04,509
让我们看一下，所以我要做的第一件事是编写一个函数
going to do is write a function and this

63
00:02:04,709 --> 00:02:06,459
将会非常简单，它将成为一个void函数，它将被称为
is gonna be really simple it's gonna be

64
00:02:06,659 --> 00:02:07,659
将会非常简单，它将成为一个void函数，它将被称为
a void function it's going to be called

65
00:02:07,859 --> 00:02:09,399
你好世界，这只是要打印你好世界，我们要开始
hello world and it's just gonna print

66
00:02:09,598 --> 00:02:10,510
你好世界，这只是要打印你好世界，我们要开始
hello world we're gonna start

67
00:02:10,710 --> 00:02:13,000
真的很简单，好吧，你好，世界去了
real simple here okay so hello world

68
00:02:13,199 --> 00:02:13,719
真的很简单，好吧，你好，世界去了
there we go

69
00:02:13,919 --> 00:02:15,670
然后我要做的当然就是正常调用
and then what I'm going to do is of

70
00:02:15,870 --> 00:02:17,439
然后我要做的当然就是正常调用
course just call it normally right

71
00:02:17,639 --> 00:02:19,480
这就是函数的工作方式，我们称其为无参数，因此
that's how functions work we call it

72
00:02:19,680 --> 00:02:21,070
这就是函数的工作方式，我们称其为无参数，因此
this one doesn't have any parameters so

73
00:02:21,270 --> 00:02:22,240
我们不需要输入任何参数，如果我运行程序，我当然会
we don't need to put any parameters in

74
00:02:22,439 --> 00:02:24,520
我们不需要输入任何参数，如果我运行程序，我当然会
and if I run my program of course I'm

75
00:02:24,719 --> 00:02:25,680
将获得文本hello world打印，您可以在此处看到我做的
going to get the text hello world

76
00:02:25,879 --> 00:02:29,260
将获得文本hello world打印，您可以在此处看到我做的
printing and you can see here that I do

77
00:02:29,460 --> 00:02:30,730
好吧，非常完美，现在让我们将此功能分配给一些
okay great perfect

78
00:02:30,930 --> 00:02:33,070
好吧，非常完美，现在让我们将此功能分配给一些
now let's assign this function to some

79
00:02:33,270 --> 00:02:35,230
现在我们了解了auto关键字，并且auto关键字是一个
kind of variable now we learn about the

80
00:02:35,430 --> 00:02:37,180
现在我们了解了auto关键字，并且auto关键字是一个
auto keyword and the auto keyword is one

81
00:02:37,379 --> 00:02:38,560
在功能方面真正有用的东西
of those things that is actually really

82
00:02:38,759 --> 00:02:40,300
在功能方面真正有用的东西
useful with things like function

83
00:02:40,500 --> 00:02:41,710
指针，我们以后也一定会涉及到，但让我们
pointers and we'll definitely get into

84
00:02:41,909 --> 00:02:43,840
指针，我们以后也一定会涉及到，但让我们
that in the future as well but let's

85
00:02:44,039 --> 00:02:45,430
只是给自动拍摄一下，看看我们是否可以分配hello world来订购一些想要的
just give Auto a shot and see if we can

86
00:02:45,629 --> 00:02:48,310
只是给自动拍摄一下，看看我们是否可以分配hello world来订购一些想要的
assign hello world to order some want to

87
00:02:48,509 --> 00:02:50,290
创建一个称为函数的变量，我现在将其设置为等于hello world
create a variable called function I'm

88
00:02:50,490 --> 00:02:51,939
创建一个称为函数的变量，我现在将其设置为等于hello world
going to set it equal to hello world now

89
00:02:52,139 --> 00:02:53,860
马上就可以看到这不起作用，因为我们无法推断出“自动”
it's right away you can see this isn't

90
00:02:54,060 --> 00:02:56,230
马上就可以看到这不起作用，因为我们无法推断出“自动”
working because we can't deduce Auto

91
00:02:56,430 --> 00:02:58,090
键入，因为真正的hello world返回虚空，而我们在这里所做的是
type because really hello world returns

92
00:02:58,289 --> 00:03:00,100
键入，因为真正的hello world返回虚空，而我们在这里所做的是
void and what we're doing here is we're

93
00:03:00,300 --> 00:03:02,950
实际调用该函数，但是如果我们正确删除了括号
actually calling the function but if we

94
00:03:03,150 --> 00:03:05,350
实际调用该函数，但是如果我们正确删除了括号
get rid of the parentheses right

95
00:03:05,550 --> 00:03:07,270
突然我们实际上并没有调用我们实际上正在获取的函数
suddenly we're not actually calling the

96
00:03:07,469 --> 00:03:09,040
突然我们实际上并没有调用我们实际上正在获取的函数
function we're actually getting the

97
00:03:09,240 --> 00:03:11,410
函数指针，具体地说，它看起来像是带有“＆”号的
function pointer and specifically this

98
00:03:11,610 --> 00:03:13,060
函数指针，具体地说，它看起来像是带有“＆”号的
would look like this with an ampersand

99
00:03:13,259 --> 00:03:14,620
所以我们有点想获得该函数的内存地址
so we're kind of getting the memory

100
00:03:14,819 --> 00:03:17,500
所以我们有点想获得该函数的内存地址
address of that function now functions

101
00:03:17,699 --> 00:03:20,380
当然只是CPU指令，它们以某种方式存储在我们的二进制文件中
are of course just CPU instructions and

102
00:03:20,580 --> 00:03:22,000
当然只是CPU指令，它们以某种方式存储在我们的二进制文件中
they're stored some way in our binary

103
00:03:22,199 --> 00:03:24,160
当我们将来实际编译代码时，我们可能会花很多时间
when we actually compile our code in the

104
00:03:24,360 --> 00:03:25,719
当我们将来实际编译代码时，我们可能会花很多时间
future we might actually take a deep

105
00:03:25,919 --> 00:03:27,820
深入研究二进制文件并查看我们的实际CPU指令以及所有这些
dive into the binary file and see our

106
00:03:28,020 --> 00:03:29,800
深入研究二进制文件并查看我们的实际CPU指令以及所有这些
actual CPU instructions and how all that

107
00:03:30,000 --> 00:03:32,920
可以工作，但现在想象一下在编译代码时
works but for now just imagine that

108
00:03:33,120 --> 00:03:34,450
可以工作，但现在想象一下在编译代码时
literally when you compile your code

109
00:03:34,650 --> 00:03:37,120
每个函数都被编译成CPU指令，它们位于某个地方
every single function gets compiled into

110
00:03:37,319 --> 00:03:39,010
每个函数都被编译成CPU指令，它们位于某个地方
CPU instructions and they are somewhere

111
00:03:39,210 --> 00:03:41,170
在可执行文件中的二进制文件中，所以我们在这里所做的
in our binary in our executable file

112
00:03:41,370 --> 00:03:43,510
在可执行文件中的二进制文件中，所以我们在这里所做的
right so what we're doing over here with

113
00:03:43,710 --> 00:03:45,310
这个“＆”号是在说这个可执行文件中的“嘿”
this ampersand is we're saying hey in

114
00:03:45,509 --> 00:03:47,800
这个“＆”号是在说这个可执行文件中的“嘿”
this executable file let's find this

115
00:03:48,000 --> 00:03:49,630
Hello world函数，让我们获取这些CPU指令的内存地址
hello world function and let's get the

116
00:03:49,830 --> 00:03:51,580
Hello world函数，让我们获取这些CPU指令的内存地址
memory address of those CPU instructions

117
00:03:51,780 --> 00:03:53,620
基本上这就是对的，所以我们正在做的是检索
that's basically what this is right so

118
00:03:53,819 --> 00:03:54,790
基本上这就是对的，所以我们正在做的是检索
what we're doing is we're retrieving the

119
00:03:54,990 --> 00:03:56,590
调用此函数时要执行的指令的位置，依此类推
location of the instructions to execute

120
00:03:56,789 --> 00:03:59,350
调用此函数时要执行的指令的位置，依此类推
when this function gets called and so

121
00:03:59,550 --> 00:04:01,060
现在我们实际上不必使用此＆符号，因为有一个
now we don't actually have to use this

122
00:04:01,259 --> 00:04:02,530
现在我们实际上不必使用此＆符号，因为有一个
ampersand by the way because there is an

123
00:04:02,729 --> 00:04:04,180
稍后我们将介绍隐式转换，因此您可以正确执行此操作
implicit conversion which we'll see in a

124
00:04:04,379 --> 00:04:06,250
稍后我们将介绍隐式转换，因此您可以正确执行此操作
minute so you can just do this right

125
00:04:06,449 --> 00:04:08,230
你好，世界，就是这样，现在我可以做的是，我实际上可以称呼我的
hello world and that's it so now what I

126
00:04:08,430 --> 00:04:10,000
你好，世界，就是这样，现在我可以做的是，我实际上可以称呼我的
can do is I can actually call my

127
00:04:10,199 --> 00:04:11,230
这样的功能，让我们实际调用它两次，我按f5键，您可以
function like this and let's actually

128
00:04:11,430 --> 00:04:14,200
这样的功能，让我们实际调用它两次，我按f5键，您可以
call it twice I'll hit f5 and you can

129
00:04:14,400 --> 00:04:15,700
看到我把它弄好了两次，所以我们已经很酷了
see that I get hollow it well printing

130
00:04:15,900 --> 00:04:16,810
看到我把它弄好了两次，所以我们已经很酷了
twice so that's pretty cool we've

131
00:04:17,009 --> 00:04:18,968
实际上设法将函数分配给变量，现在这是顺序，但是
actually managed to assign a function to

132
00:04:19,168 --> 00:04:21,550
实际上设法将函数分配给变量，现在这是顺序，但是
a variable now this is order but what

133
00:04:21,750 --> 00:04:22,780
如果我们将鼠标悬停在上面，类型实际上是很好
type is this

134
00:04:22,980 --> 00:04:25,660
如果我们将鼠标悬停在上面，类型实际上是很好
actually well if we hover our mouse over

135
00:04:25,860 --> 00:04:27,939
看看我们得到什么，我们得到空，然后像一个星号，然后起作用
this look at what we get we get void and

136
00:04:28,139 --> 00:04:30,490
看看我们得到什么，我们得到空，然后像一个星号，然后起作用
then like an asterisk and then function

137
00:04:30,689 --> 00:04:31,900
和一些括号看起来有点怪异，所以我们实际写出这种类型
and some parentheses looks a bit weird

138
00:04:32,100 --> 00:04:33,939
和一些括号看起来有点怪异，所以我们实际写出这种类型
so let's actually write out that type so

139
00:04:34,139 --> 00:04:36,400
我们变得虚无，那么我们需要给我们一个名字，因为我们是
we get void then we need to give us some

140
00:04:36,600 --> 00:04:38,259
我们变得虚无，那么我们需要给我们一个名字，因为我们是
kind of name in our case because we are

141
00:04:38,459 --> 00:04:39,819
创建一个变量，所以在我们调用它之前，让我们再次调用它
creating a variable so before we called

142
00:04:40,019 --> 00:04:41,740
创建一个变量，所以在我们调用它之前，让我们再次调用它
it function let's call it function again

143
00:04:41,939 --> 00:04:44,350
所以在括号中，我要在星号后加上我的名字
so in parentheses I'm putting an

144
00:04:44,550 --> 00:04:46,360
所以在括号中，我要在星号后加上我的名字
asterisk followed by the name of my

145
00:04:46,560 --> 00:04:49,750
变量，然后是函数可能要使用的所有参数，因此如果使用
variable and then any parameters that my

146
00:04:49,949 --> 00:04:51,040
变量，然后是函数可能要使用的所有参数，因此如果使用
function might take so if it took an

147
00:04:51,240 --> 00:04:53,590
整数我会像这样将int放入字符串中
integer I would put in int like that if

148
00:04:53,790 --> 00:04:55,120
整数我会像这样将int放入字符串中
it took a string I would put in string

149
00:04:55,319 --> 00:04:56,980
现在我们什么也没拿，因为您可以看到这里只是没有参数
now we're not taking anything as you can

150
00:04:57,180 --> 00:04:59,230
现在我们什么也没拿，因为您可以看到这里只是没有参数
see this is just no no parameters here

151
00:04:59,430 --> 00:05:00,759
所以我可以像这样结束它，这是实际的类型
so I can just kind of end it like that

152
00:05:00,959 --> 00:05:03,790
所以我可以像这样结束它，这是实际的类型
this is the actual type now this does

153
00:05:03,990 --> 00:05:07,300
看起来很奇怪，因为这很好，这最终是正确的类型
look weird because this well this is

154
00:05:07,500 --> 00:05:09,639
看起来很奇怪，因为这很好，这最终是正确的类型
ultimately the type kind of right it's

155
00:05:09,839 --> 00:05:10,900
只是我们需要像其他变量和函数一样给它起一个名字
just that we need to give it a name like

156
00:05:11,100 --> 00:05:12,579
只是我们需要像其他变量和函数一样给它起一个名字
any other variable and function just

157
00:05:12,779 --> 00:05:13,930
碰巧是这个名字，可以高呼，可以是任何您想要的
happens to be the name this can be

158
00:05:14,129 --> 00:05:15,490
碰巧是这个名字，可以高呼，可以是任何您想要的
chanted this can be anything you want

159
00:05:15,689 --> 00:05:17,770
这只是一个名字，然后我们在这里可以做的只是一个提示。
it's just a name and then what we can

160
00:05:17,970 --> 00:05:20,560
这只是一个名字，然后我们在这里可以做的只是一个提示。
kind of do here is just a sign churner

161
00:05:20,759 --> 00:05:22,569
说你好世界或类似的东西，你可以看到它像
to say hello world or something like

162
00:05:22,769 --> 00:05:23,590
说你好世界或类似的东西，你可以看到它像
that and you can see that it works like

163
00:05:23,790 --> 00:05:25,270
任何其他变量，如果我只是写的话，当然也可以在这里做
any other variable and you can of course

164
00:05:25,470 --> 00:05:28,240
任何其他变量，如果我只是写的话，当然也可以在这里做
do that here as well if I just write

165
00:05:28,439 --> 00:05:31,060
你好，世界也像那样工作，好吧，看起来确实很令人困惑
hello world that also works like that

166
00:05:31,259 --> 00:05:33,910
你好，世界也像那样工作，好吧，看起来确实很令人困惑
okay it does look very confusing that's

167
00:05:34,110 --> 00:05:36,160
就像我叫频道一样成为第一步
gonna be step one like I've called the

168
00:05:36,360 --> 00:05:36,730
就像我叫频道一样成为第一步
channel

169
00:05:36,930 --> 00:05:39,009
当然，如果我按了五个，就好像是一个功能一样
a cold shot over here like as if it was

170
00:05:39,209 --> 00:05:40,930
当然，如果我按了五个，就好像是一个功能一样
a function if I hit up five of course it

171
00:05:41,129 --> 00:05:43,150
仍然有效，这看起来确实令人困惑，我完全同意，这就是为什么人们
still works this does look confusing I

172
00:05:43,350 --> 00:05:45,430
仍然有效，这看起来确实令人困惑，我完全同意，这就是为什么人们
totally agree which is why what people

173
00:05:45,629 --> 00:05:47,110
倾向于使用这样的函数指针，要么是使用作者，要么是我的那种
tend to do with function pointers like

174
00:05:47,310 --> 00:05:50,560
倾向于使用这样的函数指针，要么是使用作者，要么是我的那种
this is either use author or my kind of

175
00:05:50,759 --> 00:05:52,389
首选方法实际上是使用def键入所有内容，因此我们基本上是在创建
preferred way is actually to type def at

176
00:05:52,589 --> 00:05:53,889
首选方法实际上是使用def键入所有内容，因此我们基本上是在创建
all using it so we're basically creating

177
00:05:54,089 --> 00:05:55,990
它的别名，所以我的意思是代替这种奇怪的代码
an alias for it so what I mean by that

178
00:05:56,189 --> 00:05:57,850
它的别名，所以我的意思是代替这种奇怪的代码
is instead of this kind of weird code

179
00:05:58,050 --> 00:06:01,000
我们实际上可以做的是说def类型，然后在这里输入实际类型，我们可以
what we can actually do is say type def

180
00:06:01,199 --> 00:06:05,319
我们实际上可以做的是说def类型，然后在这里输入实际类型，我们可以
then this actual type here and we can

181
00:06:05,519 --> 00:06:08,110
像我们不知道你好世界函数之类的称呼它
call it like our I don't know hello

182
00:06:08,310 --> 00:06:10,509
像我们不知道你好世界函数之类的称呼它
world function or something like that

183
00:06:10,709 --> 00:06:12,610
是的，当然，这个hello world函数名称是因为
right and of course this hello world

184
00:06:12,810 --> 00:06:14,650
是的，当然，这个hello world函数名称是因为
function name goes into here because

185
00:06:14,850 --> 00:06:16,389
这是我们的实际名称，因此基本上是类型，但我们从
that's our actual name so it's basically

186
00:06:16,589 --> 00:06:17,710
这是我们的实际名称，因此基本上是类型，但我们从
the type but we put a start out at the

187
00:06:17,910 --> 00:06:19,509
前面的意思是，如果我想实际使用它，我可以
front what that now means is that if I

188
00:06:19,709 --> 00:06:21,250
前面的意思是，如果我想实际使用它，我可以
want to actually use this I can just

189
00:06:21,449 --> 00:06:22,960
输入hello world函数，然后像function这样的变量名就等于hello
type in hello world function then a

190
00:06:23,160 --> 00:06:25,540
输入hello world函数，然后像function这样的变量名就等于hello
variable name like function equals hello

191
00:06:25,740 --> 00:06:27,490
世界，然后我当然可以像这样调用函数，好吧，让这个
world and then of course I can just call

192
00:06:27,689 --> 00:06:31,600
世界，然后我当然可以像这样调用函数，好吧，让这个
function like that okay let's make this

193
00:06:31,800 --> 00:06:33,069
更令人兴奋的是，我将在此处添加一个参数
a little bit more exciting I'm gonna add

194
00:06:33,269 --> 00:06:34,389
更令人兴奋的是，我将在此处添加一个参数
a parameter here it's gonna be an

195
00:06:34,589 --> 00:06:35,360
整数将被称为a，然后在世界的尽头，我是
integer is gonna be called

196
00:06:35,560 --> 00:06:37,490
整数将被称为a，然后在世界的尽头，我是
a and then at the end of hello world I'm

197
00:06:37,689 --> 00:06:40,460
实际会打印出价值，然后像这样，哎呀，让我们把它放在
actually going to print value and then a

198
00:06:40,660 --> 00:06:43,100
实际会打印出价值，然后像这样，哎呀，让我们把它放在
just like that whoops let's put this on

199
00:06:43,300 --> 00:06:46,218
右边好吧，所以现在您当然可以使用这个功能了
the right side all right so now this

200
00:06:46,418 --> 00:06:48,079
右边好吧，所以现在您当然可以使用这个功能了
hello well function of course we get an

201
00:06:48,279 --> 00:06:49,730
错误，我们无法分配它，因为hello world是一个接受函数
error here we can't assign it because

202
00:06:49,930 --> 00:06:51,468
错误，我们无法分配它，因为hello world是一个接受函数
hello world is a function that takes in

203
00:06:51,668 --> 00:06:53,600
一个整数，所以我要做的就是将函数定义更改为
an integer so what I'm going to do is

204
00:06:53,800 --> 00:06:56,180
一个整数，所以我要做的就是将函数定义更改为
change my function definition here to

205
00:06:56,379 --> 00:06:57,770
实际上包含一个整数作为参数，现在您可以看到它的工作原理
actually include an integer as a

206
00:06:57,970 --> 00:06:59,449
实际上包含一个整数作为参数，现在您可以看到它的工作原理
parameter and now you can see this works

207
00:06:59,649 --> 00:07:01,460
当我们在这里调用此函数时，我们现在需要
and when we call this function which

208
00:07:01,660 --> 00:07:02,840
当我们在这里调用此函数时，我们现在需要
we're doing over here we now need to

209
00:07:03,040 --> 00:07:04,730
指定一个整数，以便输入8，然后按f5键运行我的代码，
specify an integer so I'm going to type

210
00:07:04,930 --> 00:07:07,100
指定一个整数，以便输入8，然后按f5键运行我的代码，
in 8 and then hit f5 to run my code and

211
00:07:07,300 --> 00:07:08,930
您现在可以看到，我得到了价值8的印刷品，所以我现在已经完成了
you can see now I get the value 8

212
00:07:09,129 --> 00:07:10,730
您现在可以看到，我得到了价值8的印刷品，所以我现在已经完成了
printing so what I've kind of done now

213
00:07:10,930 --> 00:07:12,829
我有一个可以使用这样的变量名称和调用的函数
is I have a function that I can call

214
00:07:13,029 --> 00:07:15,199
我有一个可以使用这样的变量名称和调用的函数
using a variable name like this and of

215
00:07:15,399 --> 00:07:17,240
当然，它已经参数化，所以我可以在这里放任何想要的东西，它将
course it's parameterised so I can put

216
00:07:17,439 --> 00:07:19,100
当然，它已经参数化，所以我可以在这里放任何想要的东西，它将
anything I wanted to here and it will

217
00:07:19,300 --> 00:07:20,870
用不同的值调用该函数，这再次很酷，所以
call that function with different values

218
00:07:21,069 --> 00:07:23,840
用不同的值调用该函数，这再次很酷，所以
which again is pretty cool so that's the

219
00:07:24,040 --> 00:07:25,610
那是它如何运作的基础，应该足以让您了解
that's the basics of how it works that

220
00:07:25,810 --> 00:07:27,800
那是它如何运作的基础，应该足以让您了解
should probably show you enough to use

221
00:07:28,000 --> 00:07:29,360
它几乎具有全部功能，但现在让我们看一下
it in pretty much all of its capacity

222
00:07:29,560 --> 00:07:31,639
它几乎具有全部功能，但现在让我们看一下
but now let's take a look at an actually

223
00:07:31,839 --> 00:07:33,680
为什么可能首先要使用函数指针的有用示例
useful example of why you might want to

224
00:07:33,879 --> 00:07:35,600
为什么可能首先要使用函数指针的有用示例
use function pointers in the first place

225
00:07:35,800 --> 00:07:38,420
所以假设我有一个向量，只是在这里包括一个向量
so suppose that I have a vector and just

226
00:07:38,620 --> 00:07:39,740
所以假设我有一个向量，只是在这里包括一个向量
include a vector over here

227
00:07:39,939 --> 00:07:41,930
不要删除此代码，而事实上所有这些我都有一个向量
don't delete this code as well and all

228
00:07:42,129 --> 00:07:44,449
不要删除此代码，而事实上所有这些我都有一个向量
of this in fact I have a vector of

229
00:07:44,649 --> 00:07:47,569
整数，我将其称为此值，而我将不使用这种
integers I'll call this values I'm just

230
00:07:47,769 --> 00:07:48,920
整数，我将其称为此值，而我将不使用这种
going to cyan't using this kind of

231
00:07:49,120 --> 00:07:52,009
像这样在这里初始化一个列表，我想创建某种
initialize a list here like that okay

232
00:07:52,209 --> 00:07:54,139
像这样在这里初始化一个列表，我想创建某种
and I want to create some kind of

233
00:07:54,339 --> 00:07:55,670
迭代所有这些功能并执行某种操作的功能，因此
function which iterates through all of

234
00:07:55,870 --> 00:07:57,770
迭代所有这些功能并执行某种操作的功能，因此
this and perform some kind of action so

235
00:07:57,970 --> 00:07:59,689
当然，每个循环基本上都是我在这里写的，但让我们假装
of course a for each loop is basically

236
00:07:59,889 --> 00:08:01,040
当然，每个循环基本上都是我在这里写的，但让我们假装
what I'm writing here but let's pretend

237
00:08:01,240 --> 00:08:02,689
我希望它可以存在于函数中，因为也许做了其他一些事情
that I wanted it to be in a function

238
00:08:02,889 --> 00:08:04,218
我希望它可以存在于函数中，因为也许做了其他一些事情
because maybe did some other things as

239
00:08:04,418 --> 00:08:06,860
好，那只是示例，所以我要写的是
well and that's just the example so what

240
00:08:07,060 --> 00:08:07,968
好，那只是示例，所以我要写的是
I'm going to do is I'm going to write a

241
00:08:08,168 --> 00:08:10,370
对于每个函数，我将其拉为H，它将采用此标准
for each function I'll pull it for H

242
00:08:10,569 --> 00:08:12,920
对于每个函数，我将其拉为H，它将采用此标准
it's going to take in this standard

243
00:08:13,120 --> 00:08:16,670
整数的向量称为值，然后它将要做一些事情，所以我将
vector of integers called values and

244
00:08:16,870 --> 00:08:18,770
整数的向量称为值，然后它将要做一些事情，所以我将
then it's going to do something so I'll

245
00:08:18,970 --> 00:08:22,069
只是说说int值，然后我们实际上想做些什么
just say for int value in the values and

246
00:08:22,269 --> 00:08:24,189
只是说说int值，然后我们实际上想做些什么
then we want to actually do something so

247
00:08:24,389 --> 00:08:28,910
让我们继续称之为O H值现在我们如何告诉该函数我们
let's go ahead and call this Oh H values

248
00:08:29,110 --> 00:08:32,000
让我们继续称之为O H值现在我们如何告诉该函数我们
now how do we tell this function what we

249
00:08:32,200 --> 00:08:34,069
希望它能够切实地为每个价值观做好我们将要做的事情
want it to actually do for each of these

250
00:08:34,269 --> 00:08:37,629
希望它能够切实地为每个价值观做好我们将要做的事情
values well the way we're gonna do it is

251
00:08:37,830 --> 00:08:39,769
只是通过本质上这样传入一个函数，我将
just by passing in a function

252
00:08:39,969 --> 00:08:41,809
只是通过本质上这样传入一个函数，我将
essentially so up here I'm going to

253
00:08:42,009 --> 00:08:43,579
创建一个函数，我要调用此打印值，它将使用
create a function I want to call this

254
00:08:43,779 --> 00:08:45,559
创建一个函数，我要调用此打印值，它将使用
print value it's going to take in a

255
00:08:45,759 --> 00:08:47,709
值，几乎要做的是先打印出来，然后再打印值
value and almost going to do is

256
00:08:47,909 --> 00:08:52,409
值，几乎要做的是先打印出来，然后再打印值
we print it so value and then the value

257
00:08:52,610 --> 00:08:55,389
非常简单的功能吧，所以我要在这里做的是每个功能
very simple function right and so what

258
00:08:55,590 --> 00:08:58,479
非常简单的功能吧，所以我要在这里做的是每个功能
I'm going to do here is inside for each

259
00:08:58,679 --> 00:09:01,269
我实际上将在这里再写一个参数
I'm actually going to write one more

260
00:09:01,470 --> 00:09:03,849
我实际上将在这里再写一个参数
parameter here that actually is this

261
00:09:04,049 --> 00:09:06,189
函数，所以它将成为空，我要调用的名称是func，然后
function so it's going to be void the

262
00:09:06,389 --> 00:09:09,009
函数，所以它将成为空，我要调用的名称是func，然后
name I'm going to call is func and then

263
00:09:09,210 --> 00:09:10,389
当然，它需要一个参数，它是一个整数，所以我要写
of course it takes one parameter which

264
00:09:10,590 --> 00:09:11,740
当然，它需要一个参数，它是一个整数，所以我要写
is an integer so I'm going to write in

265
00:09:11,940 --> 00:09:14,019
这样，然后在每个循环中我实际上只是要写
like that and then inside this for each

266
00:09:14,220 --> 00:09:15,579
这样，然后在每个循环中我实际上只是要写
loop I'm actually just going to write

267
00:09:15,779 --> 00:09:18,219
func，然后使用value作为参数进行调用，因为那是
func and then call it with value as the

268
00:09:18,419 --> 00:09:19,689
func，然后使用value作为参数进行调用，因为那是
parameter of course because that's the

269
00:09:19,889 --> 00:09:21,370
for循环的当前迭代，然后在里面是4小时，我正在传递
current iteration of the for loop and

270
00:09:21,570 --> 00:09:23,289
for循环的当前迭代，然后在里面是4小时，我正在传递
then inside is 4-h I'm passing in the

271
00:09:23,490 --> 00:09:24,849
值当然是我们的向量，我也将通过此函数
values of course which is our vector and

272
00:09:25,049 --> 00:09:26,409
值当然是我们的向量，我也将通过此函数
I'm also going to pass in this function

273
00:09:26,610 --> 00:09:28,329
这就是所谓的打印值，所以现在基本上我们有了一个函数
which is called print value so now

274
00:09:28,529 --> 00:09:30,250
这就是所谓的打印值，所以现在基本上我们有了一个函数
basically what we've got is a function

275
00:09:30,450 --> 00:09:32,319
我们可以传入整数向量，并为每个整数传递
into which we can pass in a vector of

276
00:09:32,519 --> 00:09:35,199
我们可以传入整数向量，并为每个整数传递
integers and for each of these integers

277
00:09:35,399 --> 00:09:37,059
在此向量中，它将执行正在发生的功能
inside this vector it's going to execute

278
00:09:37,259 --> 00:09:39,399
在此向量中，它将执行正在发生的功能
this function that's what's happening

279
00:09:39,600 --> 00:09:40,659
在这里，您可以看到外观，所以如果我按f5键，您可以看到
over here and you can see what that

280
00:09:40,860 --> 00:09:43,359
在这里，您可以看到外观，所以如果我按f5键，您可以看到
looks like so if I hit f5 you can see

281
00:09:43,559 --> 00:09:45,579
我在这里得到的所有这些整数我得到了一五四二
what I get here I get all these integers

282
00:09:45,779 --> 00:09:47,679
我在这里得到的所有这些整数我得到了一五四二
that I've got one five four two and

283
00:09:47,879 --> 00:09:50,199
我在这里的列表中定义的三个，我正在获得此打印值功能
three as I defined with my list here and

284
00:09:50,399 --> 00:09:51,789
我在这里的列表中定义的三个，我正在获得此打印值功能
I'm getting this print value function

285
00:09:51,990 --> 00:09:53,799
使用这些整数中的每一个作为参数执行，这很酷
executed with each of these integers as

286
00:09:54,000 --> 00:09:55,539
使用这些整数中的每一个作为参数执行，这很酷
a parameter so that's pretty cool it's a

287
00:09:55,740 --> 00:09:56,649
我们可以告诉我们功能的方式嘿，我希望您在
way that we can kind of tell our

288
00:09:56,850 --> 00:09:59,679
我们可以告诉我们功能的方式嘿，我希望您在
function hey I want you to do this at a

289
00:09:59,879 --> 00:10:02,740
现在特别的时候，这似乎有点过大了，所以我给你看一个
particular time now this might seem a

290
00:10:02,940 --> 00:10:04,389
现在特别的时候，这似乎有点过大了，所以我给你看一个
little bit overkill so I'll show you one

291
00:10:04,590 --> 00:10:05,949
更多的方法可以完成此操作，这将在下一个视频中进行
more way to kind of do this and this

292
00:10:06,149 --> 00:10:07,240
更多的方法可以完成此操作，这将在下一个视频中进行
will kind of late into the next video

293
00:10:07,440 --> 00:10:08,469
我们有很多东西，而不是那种定义这个功能的东西
that we have so instead of kind of

294
00:10:08,669 --> 00:10:09,939
我们有很多东西，而不是那种定义这个功能的东西
defining this function it's a little bit

295
00:10:10,139 --> 00:10:11,349
凌乱的只是有一个额外的功能浮动，特别是如果我们只
messy just have an extra function

296
00:10:11,549 --> 00:10:12,639
凌乱的只是有一个额外的功能浮动，特别是如果我们只
floating around especially if we only

297
00:10:12,840 --> 00:10:14,799
打算在4-h函数中使用它，所以我们可以做的就是使用
intend for it to be used inside this 4-h

298
00:10:15,000 --> 00:10:16,389
打算在4-h函数中使用它，所以我们可以做的就是使用
function so what we can do is use

299
00:10:16,590 --> 00:10:18,789
所谓的lambda和lambda基本上是一个正常函数，除了
something called a lambda and lambda is

300
00:10:18,990 --> 00:10:21,219
所谓的lambda和lambda基本上是一个正常函数，除了
essentially a normal function except

301
00:10:21,419 --> 00:10:23,109
它没有像这样被声明为普通函数，只是被声明了
it's not declared as a normal function

302
00:10:23,309 --> 00:10:24,639
它没有像这样被声明为普通函数，只是被声明了
like this it's kind of just declared

303
00:10:24,840 --> 00:10:26,019
在我们的代码中，它是一个一次性函数，实际上不是
during our code and it's kind of a

304
00:10:26,220 --> 00:10:27,939
在我们的代码中，它是一个一次性函数，实际上不是
throwaway function that isn't actually a

305
00:10:28,139 --> 00:10:29,919
真正的功能，它是一个匿名功能，也是我们可以声明的方式
real function it's an anonymous function

306
00:10:30,120 --> 00:10:31,839
真正的功能，它是一个匿名功能，也是我们可以声明的方式
and the way that we can kind of declare

307
00:10:32,039 --> 00:10:33,819
通过使用像这样的括号这样的方括号将带你
that is by using square brackets like

308
00:10:34,019 --> 00:10:36,939
通过使用像这样的括号这样的方括号将带你
this parenthesis like this will take you

309
00:10:37,139 --> 00:10:38,709
现在我们需要在此处输入的实际参数是值，然后
now actual parameter that we need to

310
00:10:38,909 --> 00:10:40,689
现在我们需要在此处输入的实际参数是值，然后
take in here which is out value and then

311
00:10:40,889 --> 00:10:42,339
带有大括号，我们基本上告诉它我们想要执行的操作，就好像它是
with curly brackets we basically tell it

312
00:10:42,539 --> 00:10:44,829
带有大括号，我们基本上告诉它我们想要执行的操作，就好像它是
what we want it to do so as if it was

313
00:10:45,029 --> 00:10:46,479
任何其他函数，因此我们将保存当前值，然后保存实际值
any other function so we'll save current

314
00:10:46,679 --> 00:10:50,289
任何其他函数，因此我们将保存当前值，然后保存实际值
value and then the actual value that we

315
00:10:50,490 --> 00:10:52,389
具有将是值，这是我们传入的参数
have which is going to be value which is

316
00:10:52,590 --> 00:10:54,339
具有将是值，这是我们传入的参数
the parameter that we passed in okay

317
00:10:54,539 --> 00:10:56,589
就像这些非常简单的东西一样，让我们​​按f5键，您可以看到我明白了
just like that pretty simple stuff let's

318
00:10:56,789 --> 00:10:58,839
就像这些非常简单的东西一样，让我们​​按f5键，您可以看到我明白了
hit f5 and you can see that I get my

319
00:10:59,039 --> 00:11:00,789
像以前一样打印值，但是我们没有X
values printing like before but we don't

320
00:11:00,990 --> 00:11:01,219
像以前一样打印值，但是我们没有X
have an X

321
00:11:01,419 --> 00:11:02,389
函数，我们实际上只是将这些方括号写成内联
function we've literally just kind of

322
00:11:02,590 --> 00:11:04,490
函数，我们实际上只是将这些方括号写成内联
written an inline these square brackets

323
00:11:04,690 --> 00:11:06,799
我被称为CAPTCHA方法，这就是我们如何从
I was called CAPTCHA method so that's

324
00:11:07,000 --> 00:11:08,449
我被称为CAPTCHA方法，这就是我们如何从
how we pass variables in from the

325
00:11:08,649 --> 00:11:11,029
外界，我们的参数在这里和内部进行，就像任何其他
outside world our parameters going here

326
00:11:11,230 --> 00:11:12,769
外界，我们的参数在这里和内部进行，就像任何其他
and inside and this is just like any

327
00:11:12,970 --> 00:11:14,449
其他函数体，如果您愿意，我们甚至可以像这样运行它
other function body we could even run it

328
00:11:14,649 --> 00:11:16,519
其他函数体，如果您愿意，我们甚至可以像这样运行它
like this if you wanted to and you can

329
00:11:16,720 --> 00:11:18,109
看到这是实际的功能，好吧，我们将进一步讨论lambdas
see that's the actual function okay

330
00:11:18,309 --> 00:11:19,879
看到这是实际的功能，好吧，我们将进一步讨论lambdas
we're gonna talk way more about lambdas

331
00:11:20,080 --> 00:11:22,250
在专门针对lambda的视频中，我们还将讨论一些东西
in a video dedicated to lambdas we're

332
00:11:22,450 --> 00:11:23,269
在专门针对lambda的视频中，我们还将讨论一些东西
also going to talk about something

333
00:11:23,470 --> 00:11:25,250
称为STD函数，它是标准函数和标准绑定等
called STD function which is standard

334
00:11:25,450 --> 00:11:27,049
称为STD函数，它是标准函数和标准绑定等
function and standard binds and all that

335
00:11:27,250 --> 00:11:28,669
东西，所以还有很多要讨论的关于函数指针的事情
stuff so there's a lot more to actually

336
00:11:28,870 --> 00:11:30,919
东西，所以还有很多要讨论的关于函数指针的事情
discuss to do with function pointers

337
00:11:31,120 --> 00:11:32,419
这只是一个简短的介绍，以便您可以看到
this is just kind of a gentle

338
00:11:32,620 --> 00:11:33,799
这只是一个简短的介绍，以便您可以看到
introduction so that you can see what

339
00:11:34,000 --> 00:11:36,109
它们是以及我们如何使用它们以及所有这些以及原始方法
they are and how we can use them and all

340
00:11:36,309 --> 00:11:37,669
它们是以及我们如何使用它们以及所有这些以及原始方法
of that and also the primitive way of

341
00:11:37,870 --> 00:11:40,129
它们在C中的存在方式就像这个空白，然后
how they exist in C which is just kind

342
00:11:40,330 --> 00:11:42,740
它们在C中的存在方式就像这个空白，然后
of like this void and then it's just the

343
00:11:42,940 --> 00:11:44,839
签名真的很奇怪，几乎没有人在实际的C ++中使用它
the signature is really really weird and

344
00:11:45,039 --> 00:11:47,209
签名真的很奇怪，几乎没有人在实际的C ++中使用它
almost no one uses this in actual C++

345
00:11:47,409 --> 00:11:49,819
了，但一定要意识到这一点，因为您会看到很多类似的代码
anymore but definitely be aware of it

346
00:11:50,019 --> 00:11:51,049
了，但一定要意识到这一点，因为您会看到很多类似的代码
because you will see a lot of code like

347
00:11:51,250 --> 00:11:53,149
无论如何，只是希望在您的编程生涯中，大家喜欢
that just in your programming life

348
00:11:53,350 --> 00:11:54,469
无论如何，只是希望在您的编程生涯中，大家喜欢
anyway I hope you guys enjoyed this

349
00:11:54,669 --> 00:11:56,209
视频，如果您点击了该按钮，则可以帮助您支持该系列
video if you did you can hit that like

350
00:11:56,409 --> 00:11:58,099
视频，如果您点击了该按钮，则可以帮助您支持该系列
button you can help support this series

351
00:11:58,299 --> 00:12:00,349
通过访问patreon.com/scishow Cherno，可以获得一些非常酷的奖励
by going to patreon.com/scishow Cherno

352
00:12:00,549 --> 00:12:01,879
通过访问patreon.com/scishow Cherno，可以获得一些非常酷的奖励
there are some really cool rewards that

353
00:12:02,080 --> 00:12:03,500
您将到达那里以帮助支持本系列影片，例如及早获取视频
you'll get there for helping to support

354
00:12:03,700 --> 00:12:05,569
您将到达那里以帮助支持本系列影片，例如及早获取视频
this series such as getting videos early

355
00:12:05,769 --> 00:12:07,909
就像我们每月进行的一次环聊会议一样
and like monthly kind of hangout

356
00:12:08,110 --> 00:12:10,429
就像我们每月进行的一次环聊会议一样
sessions which we do and access to

357
00:12:10,629 --> 00:12:12,199
我所有其他系列的源代码以及所有类似的好东西，谢谢
source code from all my other series and

358
00:12:12,399 --> 00:12:13,759
我所有其他系列的源代码以及所有类似的好东西，谢谢
all that kind of cool stuff thank you

359
00:12:13,960 --> 00:12:15,259
一如既往地向所有赞助人表示感谢，本系列不会
huge thank you as always to all the

360
00:12:15,460 --> 00:12:17,209
一如既往地向所有赞助人表示感谢，本系列不会
patreon supporters this series would not

361
00:12:17,409 --> 00:12:19,759
在没有你的情况下在这里，非常感谢你，下次我再见
be here without you so thank you so much

362
00:12:19,960 --> 00:12:22,879
在没有你的情况下在这里，非常感谢你，下次我再见
I will see you guys next time goodbye

363
00:12:23,080 --> 00:12:28,080
[音乐]
[Music]

