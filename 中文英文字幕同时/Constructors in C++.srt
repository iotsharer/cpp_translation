﻿1
00:00:00,000 --> 00:00:03,069
it was look guys my name is the china rose and back to my sip of blood series
看起来我的名字叫瓷玫瑰，回到我的鲜血系列

2
00:00:03,270 --> 00:00:05,859
today we're going to continue on our whole object-oriented programming
今天，我们将继续整个面向对象的编程

3
00:00:06,059 --> 00:00:09,790
classes or that doesn't want to talk about constructors in C++
类或不想谈论C ++中的构造函数

4
00:00:09,990 --> 00:00:13,750
so what is a constructor a constructor is basically a special type of method
所以什么是构造函数构造函数基本上是一种特殊的方法

5
00:00:13,949 --> 00:00:17,800
which runs every time we instantiate an object so I think the best described
每次实例化对象时都会运行，所以我认为最好的描述是

6
00:00:18,000 --> 00:00:21,460
with an example suppose that we want to create an empty class let's actually
举一个例子，假设我们要创建一个空类，实际上

7
00:00:21,660 --> 00:00:25,870
dive into some code daybook we want to create an entity class here which has
深入研究一些代码日程，我们要在此处创建一个实体类

8
00:00:26,070 --> 00:00:31,030
two members perhaps it has a flirt X and a flirt Y which is basically just
两个成员也许有一个调情X和一个调情Y，基本上

9
00:00:31,230 --> 00:00:35,288
describing the position of the entity if I try and create this entity and then
描述实体的位置（如果我尝试创建该实体，然后

10
00:00:35,488 --> 00:00:39,549
perhaps maybe give it a print function so that it can output what it is into
也许给它一个打印功能，以便它可以将其输出到

11
00:00:39,750 --> 00:00:45,159
the console I'll just print X and have a nice little separator comma between the
在控制台上，我将只打印X，并且在

12
00:00:45,359 --> 00:00:50,288
two then why I'm instantiating the entity here and then I'm going to call
两个然后为什么我要在这里实例化实体然后我要调用

13
00:00:50,488 --> 00:00:54,998
the print function if I find on this code you can see that it will work
如果我在此代码上找到打印功能，则可以看到它可以工作

14
00:00:55,198 --> 00:01:00,279
however I get seemingly random values for the position of this entity and that
但是我似乎获得了该实体位置的随机值，并且

15
00:01:00,479 --> 00:01:04,629
is because when we instantiated this empty and allocated memory for it we
是因为当我们实例化此空的并为其分配内存时， 

16
00:01:04,829 --> 00:01:08,409
didn't actually initialize that memory meaning that we got whatever was left
实际上并没有初始化内存，这意味着我们剩下了一切

17
00:01:08,609 --> 00:01:12,849
over in that in that memory space what we probably want to do is actually
在那个存储空间中，我们可能要做的实际上是

18
00:01:13,049 --> 00:01:16,659
initialize the memory and set it to zero or something like that so that our
初始化内存并将其设置为零或类似的东西，以便我们

19
00:01:16,859 --> 00:01:21,039
position is zero by default we don't specify it or set a position in the
默认情况下，position为零，我们不指定它或在

20
00:01:21,239 --> 00:01:24,129
future I'm going to make an in-depth video about initialization for classes
未来，我将制作有关类初始化的深入视频

21
00:01:24,329 --> 00:01:27,549
and all of that so I'm going to keep it brief here but another great example is
所有这些，所以我将在这里简单介绍一下，但是另一个很好的例子是

22
00:01:27,750 --> 00:01:31,689
if we decide to manually print x and y they're public so we can go ahead and
如果我们决定手动打印x和y，它们是公开的，那么我们可以继续

23
00:01:31,890 --> 00:01:36,609
try and write code which prints out X for example P dot X if we try and
尝试编写可打印出X的代码，例如P点X。 

24
00:01:36,810 --> 00:01:40,119
compile this code you can see that we get an uninitialized local variable e
编译这段代码，您可以看到我们得到了一个未初始化的局部变量e 

25
00:01:40,319 --> 00:01:44,709
used error message so in other words it will not even compile because we're
使用了错误消息，换句话说，它甚至无法编译，因为我们

26
00:01:44,909 --> 00:01:48,488
trying to use memory that has not been initialized this print function does
尝试使用尚未初始化的内存，此打印功能可以

27
00:01:48,688 --> 00:01:51,789
compile however it doesn't work the way that we expect it to of course because
当然，它不能像我们期望的那样工作

28
00:01:51,989 --> 00:01:55,869
it's printing x and y however they are set to seemingly random values so we've
它正在打印x和y，但是它们被设置为看似随机的值，因此我们已经

29
00:01:56,069 --> 00:02:00,278
already got a need for some kind of initialization we need a way to every
已经需要某种初始化，我们需要一种方法

30
00:02:00,478 --> 00:02:04,719
time we construct an entity we want to be able to set x and y to zero unless
当我们构造一个实体时，我们希望能够将x和y设置为零，除非

31
00:02:04,920 --> 00:02:08,610
we've specified some you so what you might want to do to do
我们已经指定了一些人，所以您可能想做什么

32
00:02:08,810 --> 00:02:12,150
that is create an init method so I'll create a method called internet it'll
那是创建一个init方法，所以我将创建一个称为internet的方法

33
00:02:12,349 --> 00:02:17,160
just be void and it will just take the job of setting ax and y to zero now what
只是无效，它将只需要将ax和y设置为零的工作现在

34
00:02:17,360 --> 00:02:21,320
I can do is when my entity gets constructed I can call a dot in it and
我可以做的是，当我的实体构造好后，我可以在其中调用一个点

35
00:02:21,520 --> 00:02:26,340
then if I try and print my event my values you can see that I get zero and
然后，如果我尝试将事件的值打印为我的值，就会看到我得到零， 

36
00:02:26,539 --> 00:02:30,660
then when I call print I get zero comma zero printing meaning that x and y I set
然后当我调用print时，我得到零逗号零打印，这意味着我设置了x和y 

37
00:02:30,860 --> 00:02:34,530
to zero wonderful however this is quite a bit of code that we've written extra
归零，但这是我们编写的很多代码

38
00:02:34,729 --> 00:02:38,310
we've had to define this in this method and every time we want to create an
我们必须在此方法中定义它，每次我们想要创建一个

39
00:02:38,509 --> 00:02:42,390
entity in our code it means that we have to actually run that init function that
实体在我们的代码中，这意味着我们必须实际运行该init函数

40
00:02:42,590 --> 00:02:46,200
is quite a lot of extra code and definitely not clean so whether it was a
有很多额外的代码，而且绝对不干净，因此无论它是

41
00:02:46,400 --> 00:02:50,550
way for us to run this initialization code one we construct an empty in comes
我们运行初始化代码的一种方法，我们在其中构造一个空

42
00:02:50,750 --> 00:02:54,480
the constructor so the constructor is a special type of method which is
构造函数，所以构造函数是一种特殊的方法类型

43
00:02:54,680 --> 00:02:59,219
basically this if the message that gets called every time you construct an
基本上，如果您每次构造一个

44
00:02:59,419 --> 00:03:02,880
object to define it we define it like any other method however it does not
对象来定义它，我们像定义其他方法一样定义它，但是它没有

45
00:03:03,080 --> 00:03:07,410
have a return type and its name must match the name of the class if I want to
有一个返回类型，如果我想的话，其名称必须与该类的名称匹配

46
00:03:07,610 --> 00:03:11,039
construct it for entity I just type in the name of the class which is entity I
为实体构造它，只需输入实体I的类的名称

47
00:03:11,239 --> 00:03:14,880
can optionally give it parameters which we'll talk about in a second or at live
可以选择为其提供参数，我们将在稍后或现场讨论

48
00:03:15,080 --> 00:03:19,350
blog and then I can just give it a body and in this case I can set X to be 0 and
博客，然后我可以给它一个正文，在这种情况下，我可以将X设置为0，然后

49
00:03:19,550 --> 00:03:23,520
Y should be 0 as well we can get rid of this igneous method and if we scroll
 Y也应该为0，我们可以摆脱这种火成岩的方法，如果我们滚动

50
00:03:23,719 --> 00:03:28,590
down here we don't need to call in it anymore at all if I run my code now you
在这里，如果我现在运行我的代码，我们根本不需要调用它

51
00:03:28,789 --> 00:03:32,460
can see that we get an identical result to what we had when we ran the init
可以看到我们得到的结果与运行init时得到的结果相同

52
00:03:32,659 --> 00:03:35,670
method however now we'll need the init method it's handled for us by the
方法，但是现在我们需要init方法由我们为它处理

53
00:03:35,870 --> 00:03:39,750
constructor if you don't specify constructor you still have a constructor
如果不指定构造函数，则仍然有一个构造函数

54
00:03:39,949 --> 00:03:43,200
it's just something called a default constructor it's actually provided for
这只是所谓的默认构造函数，实际上是为

55
00:03:43,400 --> 00:03:46,650
you by default however that constructor doesn't actually do anything it's
您默认情况下，但是构造函数实际上不执行任何操作

56
00:03:46,849 --> 00:03:50,189
basically equivalent to this it does nothing so those things that is
基本上等同于此，它什么也不做，所以那些事情是

57
00:03:50,389 --> 00:03:53,730
initializes your variable at all in languages such as Java primitive types
用Java基本类型之类的语言完全初始化变量

58
00:03:53,930 --> 00:03:58,110
like int or floats are automatically initialized and set to 0 that is not the
如int或floats会自动初始化并设置为0，而不是

59
00:03:58,310 --> 00:04:02,280
case in simple slots you have to manually initialize all of your
在简单的插槽中，您必须手动初始化所​​有

60
00:04:02,479 --> 00:04:05,670
primitive types otherwise they will be set to whatever
基本类型，否则它们将被设置为任何

61
00:04:05,870 --> 00:04:08,880
was left over in that memory so very very important that you don't forget
留在那记忆中，非常重要，你不要忘记

62
00:04:09,080 --> 00:04:11,939
about analyzation again I'm going to talk more about initialization in a
再次讨论分析，我将在

63
00:04:12,139 --> 00:04:15,490
future video and strategies and ways to initialize memory
未来的视频以及初始化内存的策略和方法

64
00:04:15,689 --> 00:04:19,150
correctly so definitely check that out when ever I get around to making that
正确，因此绝对可以在我做任何事情时检查一下

65
00:04:19,350 --> 00:04:22,629
let's take a look at a constructor with parameters so I can write as many
让我们看一下带有参数的构造函数，这样我就可以写很多

66
00:04:22,829 --> 00:04:25,660
constructors as I want of course provided they have different parameters
我当然想要的构造函数，只要它们具有不同的参数

67
00:04:25,860 --> 00:04:29,920
are the same as if I was writing methods with the same name
就像我在写具有相同名称的方法一样

68
00:04:30,120 --> 00:04:33,129
however providing different overloads for them so basically different versions
但是为它们提供了不同的重载，因此基本上是不同的版本

69
00:04:33,329 --> 00:04:37,000
of the same method with different parameters I'm going to add X and y as a
相同方法的不同参数，我将X和y添加为

70
00:04:37,199 --> 00:04:41,410
parameter here and I'm going to assign X and y to x and y so basically I'm
参数在这里，我要为x和y分配X和y，所以基本上

71
00:04:41,610 --> 00:04:46,780
assigning my parameter to my member variable here I now have the option of
将参数分配给成员变量，现在我可以选择

72
00:04:46,980 --> 00:04:51,160
constructing entity with parameters so I simply write whatever my I want my
用参数构造实体，所以我只要写我想要的任何东西

73
00:04:51,360 --> 00:04:55,420
values to be such as 10 and 5 I'll get rid of this extra print and if I run
值，例如10和5，我将摆脱这种多余的打印，如果我运行

74
00:04:55,620 --> 00:05:00,189
this I should have called get 10 and 5 printing to the console like so all
我应该把get 10 and 5打印到控制台上

75
00:05:00,389 --> 00:05:03,879
right cool that's constructors pretty much covered constructors of course will
不错，构造函数几乎可以涵盖所有构造函数

76
00:05:04,079 --> 00:05:08,468
not run if you do not instantiate an object so if you just use static methods
如果不实例化对象，则不会运行，因此，如果您仅使用静态方法

77
00:05:08,668 --> 00:05:13,090
from a class it would run we haven't talked about heap allocation yet we
从一个可以运行的类开始，我们还没有讨论堆分配，但是我们

78
00:05:13,290 --> 00:05:16,210
definitely will in a future video probably really really soon but of
肯定会在未来的视频中真的会很快，但是

79
00:05:16,410 --> 00:05:20,680
course when you use the new keyword and create an object instance it will also
当然，当您使用new关键字并创建对象实例时，它也会

80
00:05:20,879 --> 00:05:23,889
call the constructor there are ways to remove a constructor as well if for
调用构造函数还有一些方法可以删除构造函数

81
00:05:24,089 --> 00:05:28,930
example you had a log class which you only had static methods in so for
例如，您有一个日志类，其中只有静态方法，因此

82
00:05:29,129 --> 00:05:33,310
example I had my static void log here let's call it something like right
例子我在这里有我的静态无效日志，让我们称它为正确

83
00:05:33,509 --> 00:05:37,540
instead of monkey silly and I only wanted to allow people to use my class
而不是傻傻的猴子，我只想允许人们使用我的课程

84
00:05:37,740 --> 00:05:41,050
like Thoreau I did not want people creating instances there's two different
像梭罗一样，我不希望人们创建实例，但有两个不同

85
00:05:41,250 --> 00:05:45,278
strategies for sorting that out we can either hide the constructor by making it
整理策略，我们可以通过隐藏构造函数

86
00:05:45,478 --> 00:05:48,790
private like so as you can see that I get an error here because I cannot
就像您可以看到我在这里收到错误消息一样，因为我无法

87
00:05:48,990 --> 00:05:52,300
access the constructor if I don't do something like that you can see it
如果我不做类似的事情，请访问构造函数，您可以看到它

88
00:05:52,500 --> 00:05:55,540
obviously allows the construction of this object because they both lost
显然允许构造该对象，因为它们都丢失了

89
00:05:55,740 --> 00:05:59,439
applied the default constructor for us however we can tell the compiler no I
为我们应用了默认构造函数，但是我们可以告诉编译器否

90
00:05:59,639 --> 00:06:03,759
don't want that default constructor and we can do so by simply writing log which
不需要那个默认的构造函数，我们可以通过简单地写log 

91
00:06:03,959 --> 00:06:09,040
is a default constructor equals delete and if you look over here we cannot call
是默认的构造函数，等于删除，如果您查看此处，我们将无法调用

92
00:06:09,240 --> 00:06:13,210
love like this because the default constructor doesn't actually exist it's
之所以喜欢这样，是因为默认构造函数实际上并不存在

93
00:06:13,410 --> 00:06:16,509
delete it there are also special types of constructor such as the copy
删除它还有一些特殊类型的构造函数，例如复制

94
00:06:16,709 --> 00:06:19,129
constructor and the move constructor they're going to be set
构造函数和将要设置的move构造函数

95
00:06:19,329 --> 00:06:22,579
videos on each of those because they're rather complicated but to the basic use
每个视频，因为它们相当复杂，但基本用途

96
00:06:22,779 --> 00:06:26,088
that is what a constructor is a special method which one whenever you create an
那就是构造函数是一种特殊的方法，只要您创建一个

97
00:06:26,288 --> 00:06:30,920
instance of a class the primary use for this is to initialize that class make
类的实例，其主要用途是初始化该类的make 

98
00:06:31,120 --> 00:06:34,338
sure that you neutralize all of your memory and do any kind of setups that
确保中和所有内存并进行任何类型的设置

99
00:06:34,538 --> 00:06:37,519
you need to do whenever you creating your object instance process we've got
每当您创建对象实例流程时，我们都需要做

100
00:06:37,718 --> 00:06:41,319
so much stuff to cover I'll see you guys next time goodbye
有很多东西要掩盖，下次再见

101
00:06:41,519 --> 00:06:46,699
[Music]
[音乐]

102
00:06:49,649 --> 00:06:54,649
[Music]
 [音乐] 

