﻿1
00:00:00,000 --> 00:00:03,129
hey what's up guys my name is Ayana and welcome back to my safe loss class
嘿，大家好，我叫Ayana，欢迎回到我的安全损失课程

2
00:00:03,330 --> 00:00:07,299
series so today building off of what we talked about last time with function
系列，所以今天以我们上次讨论的功能为基础

3
00:00:07,500 --> 00:00:10,600
pointers if you haven't seen that video definitely click up there to check it
指针（如果您还没有看过该视频的话）一定要点击那里查看

4
00:00:10,800 --> 00:00:14,290
out today we're gonna be talking about talking about Landers and lambdas are
今天我们要谈论的是关于Landers和Lambda 

5
00:00:14,490 --> 00:00:18,850
essentially a way to a way for us to define something that I like to call in
本质上是一种方法，使我们可以定义一些我喜欢调用的东西

6
00:00:19,050 --> 00:00:22,870
an anonymous function so basically it's a way for us to create a function
匿名函数，所以基本上这是我们创建函数的一种方式

7
00:00:23,070 --> 00:00:26,710
without actually having to physically create a function just like a quick
无需像快速一样实际创建功能

8
00:00:26,910 --> 00:00:30,010
disposable function if we want to suck if we want to demonstrate some kind of
一次性功能，如果我们想吸吮，如果我们想演示某种

9
00:00:30,210 --> 00:00:34,239
code that needs to be run but we we want to trade it more like a variable and
需要运行的代码，但我们希望更像一个变量进行交易， 

10
00:00:34,439 --> 00:00:39,189
less than an actual formal function that exists as like a symbol in our actual
小于一个实际的形式功能，在我们的实际中像符号一样存在

11
00:00:39,390 --> 00:00:43,029
compiled code if that makes sense and obviously when we jump into some
编译的代码是否有意义，很明显，当我们跳入某些代码时

12
00:00:43,229 --> 00:00:46,869
examples if that didn't make sense hopefully they'll clear it up okay so
例子，如果那没有希望，他们会把它清除好，所以

13
00:00:47,070 --> 00:00:51,640
first of all a lambda right what is it used for it's one thing to understand
首先是lambda，对它的用途是一回事

14
00:00:51,840 --> 00:00:54,489
what it is but it's obviously a completely different thing to understand
是什么，但显然这是完全不同的东西

15
00:00:54,689 --> 00:00:58,329
how to use it and when to use it and the answer to that is essentially whenever
如何使用它以及何时使用它，答案基本上是在任何时候

16
00:00:58,530 --> 00:01:04,418
you have a function pointer you can use a lambda in C++ that is how that works
您有一个函数指针，可以在C ++中使用lambda，它的工作方式是

17
00:01:04,618 --> 00:01:10,329
right so a lambda is just a way for us to literally define a function without
对，因此lambda只是我们从字面上定义函数的一种方式

18
00:01:10,530 --> 00:01:14,859
having to define a function so the usage of al and are therefore is wherever we
必须定义一个函数，所以al的使用因此在任何地方

19
00:01:15,060 --> 00:01:20,379
would normally set a function pointer to a function we can set it to a lambda
通常会设置指向函数的函数指针，我们可以将其设置为lambda 

20
00:01:20,579 --> 00:01:27,099
instead right so really to talk about why lambdas are useful and where you can
而是说真的，为什么要说lambda有用，以及在哪里可以

21
00:01:27,299 --> 00:01:31,719
use them you need to understand where you would use a function pointer and we
使用它们，您需要了解在哪里使用函数指针，我们

22
00:01:31,920 --> 00:01:34,480
definitely did talk about that in the function pointers video but when we
确实在函数指针视频中谈到了这一点，但是当我们

23
00:01:34,680 --> 00:01:38,799
actually start getting into concrete examples in C++ and in this series and
实际上开始进入C ++和本系列的具体示例

24
00:01:39,000 --> 00:01:43,058
especially the game engine series where we actually build something that's when
特别是游戏引擎系列，我们实际上是在

25
00:01:43,259 --> 00:01:46,750
you'll really see me use everything that I described in this series so just keep
您会真正看到我使用了本系列中介绍的所有内容，因此请继续

26
00:01:46,950 --> 00:01:50,409
that in mind this is more to be treated as like a reference and like oh I don't
请记住，这更像是参考，就像哦，我不

27
00:01:50,609 --> 00:01:54,878
know what this is at all or I don't know the syntax for this actual thing or how
根本不知道这是什么，或者我不知道这件事的语法或方式

28
00:01:55,078 --> 00:01:57,969
it works right so let's jump in and we're going to build off of what we
它工作正常，所以让我们开始吧，我们将基于我们建立的基础

29
00:01:58,170 --> 00:02:02,409
covered in the function pointers video to actually demonstrate the usage of a
视频中的功能指针视频进行了介绍，以实际演示

30
00:02:02,609 --> 00:02:05,230
lambda and the different things that we can do with it okay so this is the code
 lambda以及我们可以用它做的不同的事情好了，所以这是代码

31
00:02:05,430 --> 00:02:08,679
that we had from the last video we basically just built a little for each
从上一个视频中获得的，我们基本上只是为每个视频制作了一些

32
00:02:08,878 --> 00:02:12,670
function which took in a function pointer and in fact last video I even
该函数接收了一个函数指针，实际上我上一个视频

33
00:02:12,870 --> 00:02:15,069
said that we did user lambda this is a lambda
说我们做了用户lambda这是一个lambda 

34
00:02:15,269 --> 00:02:20,019
right over here we did essentially define our own function in line with the
在这里，我们确实根据定义了自己的功能

35
00:02:20,218 --> 00:02:24,459
rest of our code that just printed out whatever value we needed and this
我们剩下的代码只是打印出我们需要的任何值，而这

36
00:02:24,658 --> 00:02:29,530
function pointer essentially defined what the lambda actually needed to be so
函数指针本质上定义了lambda实际上需要做什么

37
00:02:29,729 --> 00:02:32,800
in other words we know that it returns void and we know that it takes one
换句话说，我们知道它返回空并且我们知道它需要一个

38
00:02:33,000 --> 00:02:36,219
parameter which is an integer and that's why you see this function as you can see
参数是一个整数，这就是为什么您看到此函数的原因

39
00:02:36,419 --> 00:02:40,060
returns nothing it just prints out a line of text and then we have this
不返回任何内容，只打印出一行文本，然后我们得到

40
00:02:40,259 --> 00:02:43,840
integer value parameter which is defined because we've said that we need to take
定义整数值参数是因为我们说过我们需要

41
00:02:44,039 --> 00:02:47,610
in an integer and in this case the integer that we pass into this function
在一个整数中，在这种情况下，我们传入该函数的整数

42
00:02:47,810 --> 00:02:52,689
ends up being the value that work currently iterating over right so you
最终成为当前迭代正确的值，因此您

43
00:02:52,889 --> 00:02:57,009
can see that literally this func value is actually calling this code that we've
可以看到，这个func值实际上是在调用我们已经

44
00:02:57,209 --> 00:03:01,118
defined here in our main function and that really is the usage of a lambda
在我们的主要功能中定义，这实际上是lambda的用法

45
00:03:01,318 --> 00:03:05,379
lets us do cool things like this one of the biggest examples I can ever give is
让我们做这样的很酷的事情，我可以举的最大的例子之一是

46
00:03:05,579 --> 00:03:11,170
we want to be able to pass in a function to an API so that at some point in the
我们希望能够将函数传递给API，以便在

47
00:03:11,370 --> 00:03:17,590
future it can call that function for us because we don't know we can't call the
将来它可以为我们调用该函数，因为我们不知道我们无法调用

48
00:03:17,789 --> 00:03:20,890
function now because either it doesn't have the data it needs or we just don't
现在运行，因为它没有所需的数据，或者我们只是没有

49
00:03:21,090 --> 00:03:24,879
want to we want to kind of defer the calling of a function and if we want to
想要，我们想推迟一个函数的调用，如果我们想

50
00:03:25,079 --> 00:03:28,360
do something like that then of course we need to tell it what function to call
做类似的事情，那么我们当然需要告诉它要调用什么函数

51
00:03:28,560 --> 00:03:32,230
when it gets up to the stage if I now want to call a function and lambdas are
当我现在想调用一个函数并且lambdas是什么时候

52
00:03:32,430 --> 00:03:35,649
just a really good way of specifying that function specifying code that you
只是指定该功能的一种非常好的方法，它指定了您所需要的代码

53
00:03:35,848 --> 00:03:40,090
want to run sometime in the future like we have here with is for H where we
想在未来的某个时间像我们在这里运行的那样

54
00:03:40,289 --> 00:03:43,929
actually run that code when we're iterating over the element and when it
当我们遍历元素时以及当元素迭代时，实际上运行该代码

55
00:03:44,128 --> 00:03:48,129
becomes time for us to call it with the specific parameter as we're doing here
到了我们在这里使用特定参数来调用它的时候了

56
00:03:48,329 --> 00:03:51,730
so let's dissect the syntax of this Lambert a little bit I'm actually going
所以让我们剖析这个Lambert的语法

57
00:03:51,930 --> 00:03:55,749
to kind of get rid of this and assign it to an order variable so order lambda
要摆脱这种情况，并将其分配给订单变量，以便订购lambda 

58
00:03:55,949 --> 00:04:01,450
we'll say equals our lambda and then we'll pass in the lambda into here all
我们将说等于我们的lambda，然后我们将lambda传递给所有

59
00:04:01,650 --> 00:04:05,469
right cool so first of all we start off a lambda with these square brackets let
不错，所以首先我们用这些方括号开始一个lambda 

60
00:04:05,669 --> 00:04:08,649
me make some room we start up this lambda with the square brackets what is
我腾出一些空间，用方括号启动此lambda，这是什么

61
00:04:08,848 --> 00:04:12,459
that now I will point out first of all that you don't really have to take my
现在，我首先要指出的是，您实际上并不需要接受

62
00:04:12,658 --> 00:04:16,088
word for it or this video for it cuz sometimes videos are a bit annoying for
这个词或这个视频，因为有时视频有点烦人

63
00:04:16,288 --> 00:04:20,110
things like this you can actually go over to cpp reference calm as I've got
像这样的事情，您实际上可以转到cpp参考冷静

64
00:04:20,310 --> 00:04:24,689
open over here and there is an there's a page on lambda expressions you
在这里打开，有一个关于lambda表达式的页面

65
00:04:24,889 --> 00:04:28,468
can see they exist in C++ 11 and they've actually got the syntax of all the
可以看到它们存在于C ++ 11中，并且它们实际上具有所有

66
00:04:28,668 --> 00:04:31,528
things you can do and you can see that what of course the first part is the
您可以做的事情，您可以看到第一部分当然是

67
00:04:31,728 --> 00:04:34,949
captures and of course it describes everything over here if you've got the
捕获的内容，当然，如果您有

68
00:04:35,149 --> 00:04:39,300
explanation you can see the captures are a comma separated list of zero or more
解释您可以看到捕获是逗号分隔的零个或多个列表

69
00:04:39,500 --> 00:04:43,410
captures CPB reference comm is my favorite superclass reference website
捕获CPB参考通信是我最喜欢的超类参考网站

70
00:04:43,610 --> 00:04:47,160
there are many others and you can use all of them really they're all useful
还有很多其他的，您可以全部使用，它们真的很有用

71
00:04:47,360 --> 00:04:52,170
but this one's actually quite clear and refer to this often right I'll leave a
但是这个其实很清楚，经常提到这个，我会留下一个

72
00:04:52,370 --> 00:04:54,838
link to the lambda page in the description below but in general if
链接到以下说明中的lambda页面，但通常

73
00:04:55,038 --> 00:04:57,959
you're not sure about something like lambdas or I wonder what goes into that
您不确定lambda之类的东西，或者我想知道其中的原因

74
00:04:58,158 --> 00:05:02,850
capture thing or what the possible values are right if you try to figure
捕获事物或可能的值正确，如果您尝试计算

75
00:05:03,050 --> 00:05:07,439
out stuff like that just look it up in the reference ok so inside here you can
这样的东西只要在参考中查找就可以了，所以在这里你可以

76
00:05:07,639 --> 00:05:09,899
see the captions we have a comma separated list of zero or more captures
看到字幕，我们用逗号分隔的零个或多个捕获列表

77
00:05:10,098 --> 00:05:13,709
so I'll explain what this is in a minute but basically you can see that we can
所以我会在几分钟内解释这是什么，但是基本上您可以看到我们可以

78
00:05:13,908 --> 00:05:19,020
pass in like variables essentially a would be captured by a copy or a value
传递类似的变量，本质上a将被副本或值捕获

79
00:05:19,220 --> 00:05:23,129
and B is captured by reference here we can pass in this we can pass in just an
 B是通过引用捕获的，在这里我们可以通过，我们可以仅通过

80
00:05:23,329 --> 00:05:26,850
ampersand which captures everything by reference just an equal sign which
 “＆”号仅通过等号捕获所有内容

81
00:05:27,050 --> 00:05:30,480
captures everything by value or by copy and then that captures nothing so what
通过值或副本捕获所有内容，然后不捕获任何内容，那么

82
00:05:30,680 --> 00:05:35,100
is this whole capturing thing well consider this as an example what if we
是整个捕获的东西吗，以这个为例，如果我们

83
00:05:35,300 --> 00:05:40,230
want to put outside variables into the instructions that are inside our lambda
想要将外部变量放入lambda内部的指令中

84
00:05:40,430 --> 00:05:44,278
function what happens then right because remember what we're doing is we're
函数会发生什么，然后正确，因为记住我们在做的是

85
00:05:44,478 --> 00:05:46,910
constructing a function that will then get called later
构造一个函数，稍后再调用

86
00:05:47,110 --> 00:05:52,499
so if we use variables that are outside of that lambda like for example outside
因此，如果我们使用lambda之外的变量，例如outside 

87
00:05:52,699 --> 00:05:58,170
of this function so maybe I have an aunt a over here that's equal to 5 what if I
这个功能，所以也许我有一个阿姨在这里等于5，如果我

88
00:05:58,370 --> 00:06:02,129
use that here what if the pop will instead of printing value or something I
在这里使用它，如果流行音乐会代替打印值或我会做什么

89
00:06:02,329 --> 00:06:05,550
want to print a well how is that gonna work because this is outside and
想要打印一口井，那将如何工作，因为这在外面，并且

90
00:06:05,750 --> 00:06:10,439
obviously what I'm doing is calling this code from over here inside this for each
显然，我正在做的是在每个内部从此处调用此代码

91
00:06:10,639 --> 00:06:14,910
loop so how does it have access to a well there's two ways that we can pass
循环，那么它如何获得油井，我们可以通过两种方式

92
00:06:15,110 --> 00:06:18,509
this a variable around and this is the exact same as if we hadn't made our own
这是一个变量，就像我们没有自己做一样

93
00:06:18,709 --> 00:06:23,759
function we can pass it by value or we can pass it by reference and that is
函数，我们可以按值传递它，也可以按引用传递它，即

94
00:06:23,959 --> 00:06:28,439
what that capture group is for write that first at square brackets that lets
该捕获组的目的是首先在方括号中写出

95
00:06:28,639 --> 00:06:32,100
us say how we want to pass vary apples in now in this case we're not
我们说我们现在想要通过各种苹果的方式不是这样

96
00:06:32,300 --> 00:06:35,100
passing anything in which is why we're getting an error here because well it's
通过任何东西，这就是为什么我们在这里出现错误的原因，因为这是

97
00:06:35,300 --> 00:06:39,088
an enclosing function we can't pass an A but what we can do is we can either
一个封闭的函数，我们不能传递A，但是我们可以做的是

98
00:06:39,288 --> 00:06:45,449
write equals which means pass everything in by value or we can write an ampersand
写等于意味着按值传递所有内容，或者我们可以写一个“＆”号

99
00:06:45,649 --> 00:06:50,699
like this which means pass everything in by reference or we can actually write
这样意味着通过引用传递所有内容，或者我们实际上可以编写

100
00:06:50,899 --> 00:06:55,649
individual variables like this so I want to pass a by value or with an ampersand
像这样的单个变量，所以我想传递一个按值或与符号

101
00:06:55,848 --> 00:06:59,129
a by reference now in this case we actually get an error when we try and
现在通过引用，在这种情况下，当我们尝试

102
00:06:59,329 --> 00:07:02,309
pass something in well actually if we capture anything whether it be by value
实际上，如果我们捕获任何东西，无论是通过价值还是通过，都会很好地传递一些东西

103
00:07:02,509 --> 00:07:05,879
or by reference we're going to get an error over here in 4h and this is just
或参考，我们将在4h内出现错误，这仅仅是

104
00:07:06,079 --> 00:07:09,660
in this specific example because we're just using a roll function pointer if we
在此特定示例中，因为如果我们仅使用roll函数指针

105
00:07:09,860 --> 00:07:14,459
convert this to an STD function like we will right now so this is gonna be of
像我们现在将其转换为STD函数一样，这将是

106
00:07:14,658 --> 00:07:18,269
course returning for taking in one hint parameter and we'll call it func I'll
当然返回一个提示参数，我们称它为func 

107
00:07:18,468 --> 00:07:22,769
include functional so that we have access to that then that's gonna go away
包括功能，以便我们可以访问它，那么那将消失

108
00:07:22,968 --> 00:07:26,369
okay so what we're doing here is we're passing everything in by value which
好吧，所以我们在这里所做的是，我们正在按价值传递一切

109
00:07:26,569 --> 00:07:29,999
means it's just gonna copy the value and pass it in but you can also use an
意味着它将只是复制值并将其传递给您，但您也可以使用

110
00:07:30,199 --> 00:07:32,879
ampersand if you want to capture something by reference so maybe if it's
 “＆”号，如果您想通过引用捕获某些内容，那么也许是

111
00:07:33,079 --> 00:07:37,199
like a class or a struct that you don't want to copy or this lambda is actually
例如您不想复制的类或结构，或者该lambda实际上是

112
00:07:37,399 --> 00:07:41,790
intended to modify the code if you're not sure or to modify that object or
打算在不确定时修改代码，或者修改该对象，或者

113
00:07:41,990 --> 00:07:45,778
variable you're not sure what I mean by passing by reference check out both my
变量，您不确定通过引用传递我的意思是检查我的

114
00:07:45,978 --> 00:07:48,749
video and functions I think covers a little bit of that but the references
我认为视频和功能涵盖了其中的一部分，但参考资料

115
00:07:48,949 --> 00:07:52,468
video and the pointers video is also related to that all that stuff if we go
视频和指针视频也与所有相关的东西，如果我们去

116
00:07:52,668 --> 00:07:56,369
back to C++ reference we you can also say that we have the next step which is
回到C ++参考，我们也可以说下一步就是

117
00:07:56,569 --> 00:07:59,819
the parameters right those are the parameters that our function takes in we
正确的参数是我们函数接收的参数

118
00:08:00,019 --> 00:08:03,480
have an optional specifier such as mutable which allows the body to modify
有一个可选的说明符，例如mutable，它允许主体进行修改

119
00:08:03,680 --> 00:08:07,468
parameters captured by copy that's another thing that's kind of useful you
复制捕获的参数对您很有用

120
00:08:07,668 --> 00:08:10,769
can see here that we're copying the a variable but if we try and assign a to
可以在这里看到我们正在复制变量，但是如果我们尝试将分配给

121
00:08:10,968 --> 00:08:14,309
something like that it's not gonna let us which is a bit weird because we're
这样的事情不会让我们有点奇怪，因为我们

122
00:08:14,509 --> 00:08:17,550
just passing by value and of course if we were passing something into a normal
只是通过价值传递，当然，如果我们将某种东西传递给正常

123
00:08:17,750 --> 00:08:21,329
function by value of course we can we can reassign it into whatever we want
按价值功能当然可以将其重新分配到我们想要的任何内容中

124
00:08:21,528 --> 00:08:24,778
and to fix that you basically just out the word mutable here it's a bit weird
并要解决的是，您基本上只是可​​变的一词，这有点奇怪

125
00:08:24,978 --> 00:08:28,829
but hey stuff like that you probably wouldn't even realize exists if you
但是嘿，像这样的东西，如果您可能甚至不会意识到存在

126
00:08:29,028 --> 00:08:32,370
don't look at things like the reference and see what's possible because you can
不要看参考之类的东西，而是要看看有什么可能，因为你可以

127
00:08:32,570 --> 00:08:37,019
see that over here we have our kind of parameters and whatever which is one of
看到在这里，我们有我们自己的参数，而其中任何一个

128
00:08:37,219 --> 00:08:41,218
them is that that beautiful optional kind of sequence of specifies Const
它们是指定Const的那种漂亮的可选序列

129
00:08:41,418 --> 00:08:45,059
expression is another one as well anyway that's essentially how
无论如何，表达也是另一种表达方式

130
00:08:45,259 --> 00:08:48,539
long does work it's pretty simple I don't want to drag this video on because
可以正常工作很简单，我不想拖拉该视频，因为

131
00:08:48,740 --> 00:08:53,939
it makes sense I hope to most people it is fairly simple the usages of it are
我希望对大多数人来说这很简单，它的用法很简单

132
00:08:54,139 --> 00:08:57,870
the most important part we're gonna look at one use for why you might want to use
我们将要探讨的最重要的部分是为什么您可能想使用

133
00:08:58,070 --> 00:09:01,379
a lambda in something called STD find F which is part of the
称为STD的F中的lambda查找F 

134
00:09:01,580 --> 00:09:05,009
algorithm pedophile and it's basically something that we can use to find a
算法恋童癖，这基本上是我们可以用来寻找

135
00:09:05,210 --> 00:09:10,589
value inside of some kind of iterator so over here we do have this vector of
某种迭代器内部的值，所以在这里我们确实有

136
00:09:10,789 --> 00:09:15,029
values I've included algorithm over here at the top then I'm going to type in STD
我已经在顶部的算法中包含了一些值，然后我将输入STD 

137
00:09:15,230 --> 00:09:18,990
find if so this function is basically just going to accept some kind of
查找是否可以，此功能基本上只是接受某种

138
00:09:19,190 --> 00:09:22,859
iterator so we'll give it values begin and values and so between the beginning
迭代器，所以我们给它赋值begin和values，以此类推

139
00:09:23,059 --> 00:09:29,129
and end of this vector of values that we have here we can basically say hey
我们在这里拥有的价值向量的结尾，我们基本上可以说嘿

140
00:09:29,330 --> 00:09:34,319
returned for me an iterator that is the first element that matches whatever kind
为我返回了一个迭代器，该迭代器是匹配任何种类的第一个元素

141
00:09:34,519 --> 00:09:38,669
of predicate I pass in so in other words what we can do here is write a function
我传入的谓词，换句话说，我们在这里可以做的就是编写一个函数

142
00:09:38,870 --> 00:09:42,620
is going to take in this is a vector of integers so we'll take an int value and
将要接受的是一个整数向量，所以我们将一个int值

143
00:09:42,820 --> 00:09:46,379
then it has to return a boolean which basically says whether or not it
那么它必须返回一个布尔值，该布尔值基本上表明是否

144
00:09:46,580 --> 00:09:50,819
fulfills our condition so we'll say maybe you value is greater than three or
满足我们的条件，所以我们可能会说您的价值大于三或

145
00:09:51,019 --> 00:09:55,259
something like that let's just return that so return value grid entry so it's
像这样的东西，让我们只返回它，所以返回值网格条目

146
00:09:55,460 --> 00:09:59,219
going to look through this vector for numbers for integers that are larger
将通过此向量查找较大整数的数字

147
00:09:59,419 --> 00:10:03,870
than three and return to us an iterator of that which will essentially be just
而不是三个，然后返回给我们一个基本上只是

148
00:10:04,070 --> 00:10:08,969
the first element so what it should do hopefully is return five for us so if we
第一个元素，所以它应该做的是为我们返回五个，所以如果我们

149
00:10:09,169 --> 00:10:15,569
assign this order iterator equals a city find F and then we'll print out we'll do
分配此迭代器等于城市找到F，然后我们将打印出

150
00:10:15,769 --> 00:10:18,750
reference that iterator and print it up we should get the value five printing
引用该迭代器并打印出来，我们应该得到五个打印值

151
00:10:18,950 --> 00:10:23,219
why because we've specified that we want a value greater than three using a
为什么，因为我们指定要使用一个大于3的值

152
00:10:23,419 --> 00:10:27,990
lambda and five happens to be the first number that is greater than three inside
 lambda和五个碰巧是第一个大于内部三个的数字

153
00:10:28,190 --> 00:10:32,789
this list of values that we have so if I just kind of put a breakpoint somewhere
我们拥有的这些值列表如果我只是在某个地方放置断点

154
00:10:32,990 --> 00:10:36,779
here and run my code you'll see that we get the value five printing as we
在这里并运行我的代码，您将看到我们获得了五个打印值

155
00:10:36,980 --> 00:10:40,740
expected and we've managed to do that fairly easily by just specifying a nice
期望，并且我们已经成功地做到了这一点，只需指定一个

156
00:10:40,940 --> 00:10:45,240
little quick function that we want this court this code to call so what this is
我们希望此代码调用此法院的快速函数，这是什么

157
00:10:45,440 --> 00:10:48,599
going to do is loop through our list kind of like what I've got here in 4h
要做的是遍历我们的列表，就像我在4小时内到这里一样

158
00:10:48,799 --> 00:10:53,379
and it's as if we just added a little if value greater than five
好像我们只是添加了一点，如果值大于5 

159
00:10:53,580 --> 00:10:58,120
and return kind of that value that's basically what this is doing for us but
并返回那种价值，这基本上就是我们要做的，但是

160
00:10:58,320 --> 00:11:02,889
we were able to specify this if statement condition there's bullying by
我们可以指定此if语句条件被欺负

161
00:11:03,090 --> 00:11:05,919
just using a lambda really quickly like that anyway I hope you guys enjoyed this
无论如何，我真的非常快地使用了lambda，我希望你们喜欢

162
00:11:06,120 --> 00:11:09,009
video if you did you can hit the like button that's pretty much all there is
视频（如果有的话），您可以点击“赞”按钮

163
00:11:09,210 --> 00:11:12,669
to lambdas thank you huge thank you as always to all of my patreon supporters
向lambdas表示感谢，非常感谢我所有的patreon支持者

164
00:11:12,870 --> 00:11:16,509
you can head on over to patreon my home 4/2 channel and help support all the
您可以前往patreon我的家庭4/2频道并帮助支持所有

165
00:11:16,710 --> 00:11:21,579
videos that I make care of my channel next time I don't even know what we're
下次我会照顾我的频道的视频我什至不知道我们是什么

166
00:11:21,779 --> 00:11:23,949
gonna cover next time to be honest probably we haven't even covered
坦白说，下次我们可能会掩盖

167
00:11:24,149 --> 00:11:27,339
standard function yet but I feel like these function pointers I just gonna
标准函数，但我觉得这些函数指针

168
00:11:27,539 --> 00:11:31,089
drag on for a while and I think you guys all understand them if you don't
拖一会儿，我想你们大家都理解他们，如果不

169
00:11:31,289 --> 00:11:35,379
definitely leave a comment below but anyway I will see you guys next time
肯定会在下面留下评论，但是无论如何，下次我会再见到你们

170
00:11:35,580 --> 00:11:42,169
goodbye [Music]
再见 

