﻿1
00:00:00,030 --> 00:00:03,849
hey what's up guys my name is the Cherno and welcome back to my safe loss blog
嘿，大家好，我叫Cherno，欢迎回到我的安全损失博客

2
00:00:04,049 --> 00:00:08,509
series so you've decided to use a Mac
系列，因此您决定使用Mac 

3
00:00:08,660 --> 00:00:12,910
there are plenty of different tools you can use to build typical applications on
您可以使用许多不同的工具来构建典型的应用程序

4
00:00:13,109 --> 00:00:16,929
Mac but the one that I'm going to recommend is Xcode Xcode is an idea
 Mac，但我要推荐的是Xcode Xcode是一个主意

5
00:00:17,129 --> 00:00:23,739
written by Apple it's not too bad it can be slow on large projects for example we
由Apple撰写，还不错，在大型项目中可能会很慢，例如我们

6
00:00:23,939 --> 00:00:29,318
use Xcode over at EA to build games for iOS and I can tell you that some of the
在EA上使用Xcode来为iOS构建游戏，我可以告诉你一些

7
00:00:29,518 --> 00:00:34,268
compile times of upwards of 20 minutes to games like Mesa speed no limits it's
 Mesa速度这类游戏的编译时间长达20分钟， 

8
00:00:34,469 --> 00:00:38,108
also a nightmare to actually write code because everything is lagging like crazy
实际编写代码也是一场噩梦，因为一切都像疯了一样滞后

9
00:00:38,308 --> 00:00:42,909
when you've got a project that large however so small projects like this and
当您有一个如此大但如此小的项目时， 

10
00:00:43,109 --> 00:00:47,529
to learn C++ I still think Xcode is probably the best idea so let's go ahead
学习C ++，我仍然认为Xcode可能是最好的主意，所以让我们继续吧

11
00:00:47,729 --> 00:00:52,268
and install it okay so on your Mac we're going to go to the App Store we're going
并正确安装它，因此在Mac上，我们将转到App Store 

12
00:00:52,469 --> 00:00:57,608
to type in Xcode and then we're going to install it and of course this will take
输入Xcode，然后我们将其安装，这当然需要

13
00:00:57,808 --> 00:01:01,628
some time depending on how fast your internet is alright so once execute is
一段时间取决于您互联网的运行速度，因此一旦执行

14
00:01:01,829 --> 00:01:04,988
installed let's go ahead and open it we'll accept all the terms and
安装完毕后，请继续打开它，我们将接受所有条款并

15
00:01:05,188 --> 00:01:10,480
conditions Xcode is going to go ahead and install some components for us
条件Xcode将继续为我们安装一些组件

16
00:01:10,680 --> 00:01:14,560
okay so once that's done we're going to create a new Xcode project for the
好的，一旦完成，我们将为该对象创建一个新的Xcode项目。 

17
00:01:14,760 --> 00:01:18,129
template we're just going to click on mac OS and then choose command line tool
模板，我们只需要单击mac OS，然后选择命令行工具

18
00:01:18,329 --> 00:01:23,140
we'll give it a name so hello world in this case and then under the language
在这种情况下，我们会给它起一个名字，让世界打个招呼，然后用语言

19
00:01:23,340 --> 00:01:26,230
we're going to select C++ you'll have to put in some kind of organization
我们将选择C ++，您将不得不采用某种组织方式

20
00:01:26,430 --> 00:01:30,128
identifier like comm dot your company's the suggested one so you can do
像comm这样的标识符，您的公司就是建议的公司，因此您可以

21
00:01:30,328 --> 00:01:32,859
something like comm dot whatever domain name you own it doesn't really matter
诸如comm点之类的名称无论您拥有什么域名都没有关系

22
00:01:33,060 --> 00:01:35,709
this is just supposed to be some kind of unique identifier it doesn't really
这只是一个唯一的标识符，实际上并没有

23
00:01:35,909 --> 00:01:40,628
matter what it is just put something in there and hit next then you're going to
不管是什么，只需在其中放一些东西，然后点击下一步，就可以

24
00:01:40,828 --> 00:01:45,308
want to select a location for this I like to store things inside my user
想要为此选择一个位置，我希望将其存储在用户内部

25
00:01:45,509 --> 00:01:48,759
directory and then in a folder called dev
目录，然后在名为dev的文件夹中

26
00:01:48,959 --> 00:01:53,829
so I'll make a folder called dev and then just click create in there alright
所以我将创建一个名为dev的文件夹，然后单击其中的创建

27
00:01:54,030 --> 00:01:56,859
so there we go Xcode would like to access your contact
所以我们去了Xcode想访问您的联系人

28
00:01:57,060 --> 00:02:01,269
we don't really need that for this this is basically what an Xcode project looks
我们实际上并不需要它，基本上这就是Xcode项目的外观

29
00:02:01,469 --> 00:02:04,750
like there's a few project settings that you'll find inside the actual you click
例如，您可以在实际点击中找到一些项目设置

30
00:02:04,950 --> 00:02:08,169
on the actual name of the project you've got all these build settings and build
在项目的实际名称上，您具有所有这些构建设置和构建

31
00:02:08,368 --> 00:02:11,170
phases for the most part we don't really need to touch anything so you'll see
在大多数情况下，我们实际上不需要触摸任何阶段，因此您会看到

32
00:02:11,370 --> 00:02:15,219
that we've all what we've already got a main dot CPP file and we've already got
我们已经拥有了一个主要的点CPP文件，并且我们已经拥有了

33
00:02:15,419 --> 00:02:17,740
a bunch of code but I'm going to type it out again just so that it matches the
一堆代码，但我要再次键入它，以使其与

34
00:02:17,939 --> 00:02:26,460
other videos so basically we wanted to include iostream into main st DC out
其他视频，所以基本上我们想将iostream包含在主st DC中

35
00:02:26,659 --> 00:02:37,780
hello world and then we'll do our STD cin get save that and then all you have
你好，世界，然后我们将做我们的STD cin保存，然后所有

36
00:02:37,979 --> 00:02:43,390
to do to actually build it is you can hit product and then build now will
要真正构建它，您可以点击产品，然后立即构建

37
00:02:43,590 --> 00:02:46,420
compile it for you there you go build succeeded and then if you want to run it
为您编译它，然后构建成功，然后如果您要运行它

38
00:02:46,620 --> 00:02:49,780
you can just hit this play button that will also build it as well you'll have
您只要按一下这个播放按钮，它也会建立它

39
00:02:49,979 --> 00:02:57,730
to enable developer mode if this is your first time running Xcode and off we go
如果这是您第一次运行Xcode并关闭，则启用开发人员模式

40
00:02:57,930 --> 00:03:01,960
so the build succeeded and you should see a console pop out in the bottom here
因此构建成功，您应该在此处底部看到一个控制台弹出窗口

41
00:03:02,159 --> 00:03:06,009
with your output what does he enter and you can see the program terminates
用您的输出他输入了什么，您可以看到程序终止

42
00:03:06,209 --> 00:03:09,910
successfully and that's it we're ready to learn C++ on Mac alright you've got
成功了，就是这样，我们已经准备好在Mac上学习C ++了

43
00:03:10,110 --> 00:03:13,990
Xcode up and running you're finally ready to learn some C++ over the next
 Xcode启动并运行，您终于可以在下一个学习C ++了。 

44
00:03:14,189 --> 00:03:17,349
two videos we're going to start learning how C++ actually works because that's
两个视频，我们将开始学习C ++的实际工作原理，因为

45
00:03:17,549 --> 00:03:20,319
pretty much the key to writing it properly until then make sure you follow
正确编写它的关键在于确保您遵循

46
00:03:20,519 --> 00:03:24,340
me on Twitter and Instagram and if you really like this series you can support
我在Twitter和Instagram上，如果您真的喜欢这个系列，可以支持

47
00:03:24,539 --> 00:03:31,310
it on patreon well I'll see you guys next time good bye war
在patreon上，好吧，下次再见。 

