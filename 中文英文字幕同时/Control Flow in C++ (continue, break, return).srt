﻿1
00:00:00,000 --> 00:00:01,869
伙计们，我的名字叫休会或欢迎回到我安全的失恋系列，所以
as guys my name is adjourn or welcome

2
00:00:02,069 --> 00:00:04,118
伙计们，我的名字叫休会或欢迎回到我安全的失恋系列，所以
back to my safe lost love series so

3
00:00:04,318 --> 00:00:04,960
今天我们将要讨论控制流语句，实际上
today we're going to be talking about

4
00:00:05,160 --> 00:00:06,609
今天我们将要讨论控制流语句，实际上
control flow statements and really this

5
00:00:06,809 --> 00:00:08,439
将是上周剧集的延续，所以如果您还没有看过
is going to be a continuation of last

6
00:00:08,638 --> 00:00:09,880
将是上周剧集的延续，所以如果您还没有看过
week's episode so if you haven't seen

7
00:00:10,080 --> 00:00:12,399
关于循环的那集肯定会检查它们是否会成为
that episode about loop definitely check

8
00:00:12,599 --> 00:00:13,390
关于循环的那集肯定会检查它们是否会成为
that out they'll be a link in the

9
00:00:13,589 --> 00:00:14,769
下面的描述控制流语句与循环一起使用，因此在其他
description below control flow

10
00:00:14,968 --> 00:00:16,390
下面的描述控制流语句与循环一起使用，因此在其他
statements work with loop so in other

11
00:00:16,589 --> 00:00:17,949
这些词使我们对这些循环的方式有了更多的控制
words they kind of give us a little bit

12
00:00:18,149 --> 00:00:19,900
这些词使我们对这些循环的方式有了更多的控制
more control as to how these loops

13
00:00:20,100 --> 00:00:21,760
实际运行中，我们有三种主要的控制流语句，我们可以
actually run we've kind of got three

14
00:00:21,960 --> 00:00:23,499
实际运行中，我们有三种主要的控制流语句，我们可以
main control flow statements that we can

15
00:00:23,699 --> 00:00:26,350
使用继续失败和返回，他们做不同的事情继续只能
use continues break and return and they

16
00:00:26,550 --> 00:00:28,449
使用继续失败和返回，他们做不同的事情继续只能
do different things continue can only be

17
00:00:28,649 --> 00:00:30,129
在循环内使用，基本上继续说去下一次迭代
used inside a loop and basically

18
00:00:30,329 --> 00:00:32,619
在循环内使用，基本上继续说去下一次迭代
continue says go to the next iteration

19
00:00:32,820 --> 00:00:34,989
如果有一个循环，否则它将只是循环终止
of this loop if there is one otherwise

20
00:00:35,189 --> 00:00:37,599
如果有一个循环，否则它将只是循环终止
it'll just end loop break is primarily

21
00:00:37,799 --> 00:00:39,759
在循环中使用，但是它也出现在switch语句中，并且基本上会中断
used in loops however it also appears in

22
00:00:39,960 --> 00:00:41,829
在循环中使用，但是它也出现在switch语句中，并且基本上会中断
switch statements and basically breaks

23
00:00:42,030 --> 00:00:45,219
只是说走出循环，循环结束，最后返回可能是
just says get out of the loop the end to

24
00:00:45,420 --> 00:00:47,049
只是说走出循环，循环结束，最后返回可能是
loop and finally return is probably the

25
00:00:47,250 --> 00:00:48,369
如果您想以这种方式考虑，那将是最强大的，因为回报会
most powerful one if you want to

26
00:00:48,570 --> 00:00:50,739
如果您想以这种方式考虑，那将是最强大的，因为回报会
consider it that way because return will

27
00:00:50,939 --> 00:00:52,719
只是完全退出您的功能，所以如果您在某个功能中并且碰到一个
just get out of your function entirely

28
00:00:52,920 --> 00:00:54,939
只是完全退出您的功能，所以如果您在某个功能中并且碰到一个
so if you're in a function and you hit a

29
00:00:55,140 --> 00:00:57,070
return关键字，您当然会退出该功能
return keyword you will exit the

30
00:00:57,270 --> 00:00:58,748
return关键字，您当然会退出该功能
function of course the functions which

31
00:00:58,948 --> 00:01:00,248
当您输入return时，您实际上需要返回一个值
actually need to return a value when you

32
00:01:00,448 --> 00:01:01,448
当您输入return时，您实际上需要返回一个值
type in return you're conscious have

33
00:01:01,649 --> 00:01:03,428
本身返回本身返回仅适用于void函数，如果您的函数
returned by itself return by itself is

34
00:01:03,628 --> 00:01:04,840
本身返回本身返回仅适用于void函数，如果您的函数
only for void functions if your function

35
00:01:05,040 --> 00:01:06,609
键入return时需要返回一个值，您需要为其提供一个
needs to return a value when you type in

36
00:01:06,810 --> 00:01:07,929
键入return时需要返回一个值，您需要为其提供一个
return you need to supply it with a

37
00:01:08,129 --> 00:01:09,819
珍惜所有权利，无需多说，真的，让我们跳进去，
value all right not much else to say

38
00:01:10,019 --> 00:01:11,019
珍惜所有权利，无需多说，真的，让我们跳进去，
really let's just jump in and take a

39
00:01:11,219 --> 00:01:12,488
看一些例子，这是我们在上一集中的程序
look at some examples so this is the

40
00:01:12,688 --> 00:01:14,859
看一些例子，这是我们在上一集中的程序
program that we had in the last episode

41
00:01:15,060 --> 00:01:16,808
请运行一下，您会看到我们打个招呼世界，
please give that a run you'll see we

42
00:01:17,009 --> 00:01:18,129
请运行一下，您会看到我们打个招呼世界，
have hello world printing a bunch of

43
00:01:18,329 --> 00:01:19,899
时间，然后当然，我们有一段时间的达芙妮循环
times and then of course we had a bit of

44
00:01:20,099 --> 00:01:21,459
时间，然后当然，我们有一段时间的达芙妮循环
a while loop which Daphnia do-while loop

45
00:01:21,659 --> 00:01:22,629
它什么也没做，我只是摆脱其中的一些，因为我们不是真的
it does nothing I'll just get rid of

46
00:01:22,829 --> 00:01:23,769
它什么也没做，我只是摆脱其中的一些，因为我们不是真的
some of these because we don't really

47
00:01:23,969 --> 00:01:25,209
需要他们，我在上一个程序中唯一保留的就是
need them the only one that I'll keep

48
00:01:25,409 --> 00:01:26,769
需要他们，我在上一个程序中唯一保留的就是
from our previous program is pretty much

49
00:01:26,969 --> 00:01:29,980
那是for循环，所以我们要看的第一个控制流语句是continue
that for loop so the first control flow

50
00:01:30,180 --> 00:01:31,689
那是for循环，所以我们要看的第一个控制流语句是continue
statement that we'll look at is continue

51
00:01:31,890 --> 00:01:33,488
继续，我们将跳至for循环的下一个迭代
continue we'll skip to the next

52
00:01:33,688 --> 00:01:34,539
继续，我们将跳至for循环的下一个迭代
iteration of the for loop

53
00:01:34,739 --> 00:01:37,569
因此，如果我像这样写在这里继续，实际上不会产生影响
so if I write continue over here just

54
00:01:37,769 --> 00:01:39,609
因此，如果我像这样写在这里继续，实际上不会产生影响
like this it will actually not impact

55
00:01:39,810 --> 00:01:40,959
for循环的行为，因为这已经结束了
the behavior of the for loop at all

56
00:01:41,159 --> 00:01:43,209
for循环的行为，因为这已经结束了
because that is already the end of the

57
00:01:43,409 --> 00:01:45,159
该行将继续进行下一次迭代的for循环
for loop when it is that line is going

58
00:01:45,359 --> 00:01:46,899
该行将继续进行下一次迭代的for循环
to continue to the next iteration anyway

59
00:01:47,099 --> 00:01:48,640
这根本不会改变行为，但是如果我做类似的事情
that this doesn't modify the behavior at

60
00:01:48,840 --> 00:01:51,689
这根本不会改变行为，但是如果我做类似的事情
all however if I do something like if I

61
00:01:51,890 --> 00:01:56,378
mod 2等于0，然后在那里继续并运行我的程序，您将看到
mod 2 equals 0 and then have a continue

62
00:01:56,578 --> 00:01:59,649
mod 2等于0，然后在那里继续并运行我的程序，您将看到
there and run my program you'll see the

63
00:01:59,849 --> 00:02:00,730
基本上发生的是它从第二次迭代开始就跳过
basically what's happening is it's

64
00:02:00,930 --> 00:02:03,278
基本上发生的是它从第二次迭代开始就跳过
skipping every second iteration starting

65
00:02:03,478 --> 00:02:05,319
与第一个更好的方式来说明这一点可能是包括
with the first one so a better way to

66
00:02:05,519 --> 00:02:07,480
与第一个更好的方式来说明这一点可能是包括
illustrate this is probably to include

67
00:02:07,680 --> 00:02:09,729
某种值，但我们不仅可以记录世界，您还可以记录
some kind of values but instead of just

68
00:02:09,929 --> 00:02:11,770
某种值，但我们不仅可以记录世界，您还可以记录
logging hello world we can also log the

69
00:02:11,969 --> 00:02:12,410
实际的I变量以查看什么是索引
actual

70
00:02:12,610 --> 00:02:14,030
实际的I变量以查看什么是索引
I variable to see what index is

71
00:02:14,229 --> 00:02:16,160
目前在，如果我写的话，我按f5键，您可以看到我们的世界
currently at and if I write that and I

72
00:02:16,360 --> 00:02:17,960
目前在，如果我写的话，我按f5键，您可以看到我们的世界
hit f5 you can see that our hello world

73
00:02:18,159 --> 00:02:18,500
段仅在其他段中使用奇数迭代运行
segment

74
00:02:18,699 --> 00:02:20,240
段仅在其他段中使用奇数迭代运行
only runs with odd iterations in other

75
00:02:20,439 --> 00:02:22,520
当我为零时，它不会运行，因为课程0 mod 0 PS 0因此
words when I is zero it's not going to

76
00:02:22,719 --> 00:02:25,490
当我为零时，它不会运行，因为课程0 mod 0 PS 0因此
run because of course 0 mod 0 PS 0 so

77
00:02:25,689 --> 00:02:27,200
它会继续，因为它是守护进程，否则当我为1时是真的
it's going to continue because it's

78
00:02:27,400 --> 00:02:29,270
它会继续，因为它是守护进程，否则当我为1时是真的
daemon it's true otherwise when I is 1

79
00:02:29,469 --> 00:02:31,370
当我不知道时，我们将获取日志，当我们3岁时，我们将获取日志，然后当我
we're going to get log when I to we're

80
00:02:31,569 --> 00:02:34,010
当我不知道时，我们将获取日志，当我们3岁时，我们将获取日志，然后当我
not and when is 3 we are and then when I

81
00:02:34,210 --> 00:02:35,450
看到我们不是，然后我从没碰到5个数字，而碰到了5个，我们没有
saw we're not and then of course I never

82
00:02:35,650 --> 00:02:37,760
看到我们不是，然后我从没碰到5个数字，而碰到了5个，我们没有
hits 5 figures when it hits 5 we do not

83
00:02:37,960 --> 00:02:39,830
执行for循环，因为此语句表明我必须小于客户端
do the for loops because this statement

84
00:02:40,030 --> 00:02:41,960
执行for循环，因为此语句表明我必须小于客户端
says that I must be less than clients

85
00:02:42,159 --> 00:02:43,370
我们也将其更改为更简单的内容，例如我更大时
we've also changed this to something a

86
00:02:43,569 --> 00:02:45,980
我们也将其更改为更简单的内容，例如我更大时
bit more simple such as if I is greater

87
00:02:46,180 --> 00:02:48,080
而不是继续，所以基本上应该在这里当我为0时
than to continue so basically what

88
00:02:48,280 --> 00:02:50,810
而不是继续，所以基本上应该在这里当我为0时
should happen here is when I is 0 and I

89
00:02:51,009 --> 00:02:53,210
是1，我是对的，它应该打印出我们的日志以及我的身份
is 1 and I is true it should print out

90
00:02:53,409 --> 00:02:54,770
是1，我是对的，它应该打印出我们的日志以及我的身份
our log as well as what I is

91
00:02:54,969 --> 00:02:58,370
但是当我3岁或4岁的时候不应该这样，如果我们跑步，你会看到我们打招呼
however when I is 3 or 4 it shouldn't so

92
00:02:58,569 --> 00:02:59,930
但是当我3岁或4岁的时候不应该这样，如果我们跑步，你会看到我们打招呼
if we run that you'll see we get hello

93
00:03:00,129 --> 00:03:02,090
好打印3倍，所以差不多可以继续了，我是说所有
well printing 3 times great so that's

94
00:03:02,289 --> 00:03:03,950
好打印3倍，所以差不多可以继续了，我是说所有
pretty much it to continue I mean all

95
00:03:04,150 --> 00:03:05,600
它要做的是，当continue被击中时，它将进入下一个迭代
it's going to do is when continue gets

96
00:03:05,800 --> 00:03:07,400
它要做的是，当continue被击中时，它将进入下一个迭代
hit it will give to the next iteration

97
00:03:07,599 --> 00:03:09,020
for循环（如果我们实际上通过在此处放置断点来调试它）将
the for loop if we actually debug this

98
00:03:09,219 --> 00:03:10,789
for循环（如果我们实际上通过在此处放置断点来调试它）将
by putting a breakpoint here this will

99
00:03:10,989 --> 00:03:12,020
给你更多的见解，所以你可以在这里看到我是0，当然不是
give you some more insights so you can

100
00:03:12,219 --> 00:03:13,820
给你更多的见解，所以你可以在这里看到我是0，当然不是
see here I is 0 which of course is not

101
00:03:14,020 --> 00:03:15,140
将触发此事件，因此它将记录下来，并记录所有内容
going to trigger this so it's going to

102
00:03:15,340 --> 00:03:16,819
将触发此事件，因此它将记录下来，并记录所有内容
go down to log and log and everything

103
00:03:17,019 --> 00:03:17,390
当我是1和我2时也一样
will be fine

104
00:03:17,590 --> 00:03:20,120
当我是1和我2时也一样
same for when I is 1 and I 2 however as

105
00:03:20,319 --> 00:03:22,759
当我看到3 + 3大于2时，我们将继续
soon as I see s 3 + 3 is greater than 2

106
00:03:22,959 --> 00:03:24,770
当我看到3 + 3大于2时，我们将继续
we're going to go to continue which

107
00:03:24,969 --> 00:03:26,360
表示如果我现在点击f10而不是移至博客，
means that if I hit f10 right now

108
00:03:26,560 --> 00:03:28,370
表示如果我现在点击f10而不是移至博客，
instead of moving down to the blog you

109
00:03:28,569 --> 00:03:29,750
可以看到它实际上跳回到了以下内容的开头，它将
can see it actually jumps back up to the

110
00:03:29,949 --> 00:03:31,280
可以看到它实际上跳回到了以下内容的开头，它将
start of the follows and it will it will

111
00:03:31,479 --> 00:03:33,259
我加号，它会评估我们的病情，然后跳回此处和
do I plus plus and it will evaluate our

112
00:03:33,459 --> 00:03:35,450
我加号，它会评估我们的病情，然后跳回此处和
condition and then jump back here and of

113
00:03:35,650 --> 00:03:36,950
当然，在这种情况下，我会是完全相同的行为
course in this case it's going to be the

114
00:03:37,150 --> 00:03:38,870
当然，在这种情况下，我会是完全相同的行为
exact same behavior all right class I

115
00:03:39,069 --> 00:03:40,819
认为这是继续进行的整理，让我们稍作休息，所以如果我只替换一下
think that's continued sorted let's take

116
00:03:41,019 --> 00:03:42,620
认为这是继续进行的整理，让我们稍作休息，所以如果我只替换一下
a little break so if I just replace this

117
00:03:42,819 --> 00:03:44,930
代码会破坏它的作用，就像您看到我们得到的一样
code would break it's going to appear to

118
00:03:45,129 --> 00:03:47,300
代码会破坏它的作用，就像您看到我们得到的一样
act the same way you can see that we get

119
00:03:47,500 --> 00:03:50,240
0 1 2而且我们什么也没得到，但是如果我回到第一个例子
0 1 2 and we don't get anything else

120
00:03:50,439 --> 00:03:52,310
0 1 2而且我们什么也没得到，但是如果我回到第一个例子
however if I go back to my first example

121
00:03:52,509 --> 00:03:56,060
如果我在2上等于0并按f5，则可以看到我们实际上得到了绝对
of if I'm on 2 equals 0 and hit f5 you

122
00:03:56,259 --> 00:03:57,410
如果我在2上等于0并按f5，则可以看到我们实际上得到了绝对
can see we actually get absolutely

123
00:03:57,610 --> 00:03:59,120
现在什么都没打印，也许这里有一个更好的例子是
nothing printing now perhaps a little

124
00:03:59,319 --> 00:04:00,860
现在什么都没打印，也许这里有一个更好的例子是
bit of a better example here is to

125
00:04:01,060 --> 00:04:03,230
实际打印第一个值，所以我会再加1等于2，如果我们
actually print the first value so I'll

126
00:04:03,430 --> 00:04:05,150
实际打印第一个值，所以我会再加1等于2，如果我们
just do I plus 1 more 2 equals 0 if we

127
00:04:05,349 --> 00:04:06,950
快速替换将继续，这样您就可以看到发生了什么
quickly replaces would continue to just

128
00:04:07,150 --> 00:04:08,480
快速替换将继续，这样您就可以看到发生了什么
so you can see what's happening here you

129
00:04:08,680 --> 00:04:09,770
可以看到，现在不再是奇数，而是偶数
can see that instead of going

130
00:04:09,969 --> 00:04:11,509
可以看到，现在不再是奇数，而是偶数
odd numbers now it's going even number

131
00:04:11,709 --> 00:04:14,719
所以我们得到0 2来打印，所以如果我现在用break替换它，那是什么
so we get 0 2 in for printing so if I

132
00:04:14,919 --> 00:04:16,848
所以我们得到0 2来打印，所以如果我现在用break替换它，那是什么
replace this now with break what's

133
00:04:17,048 --> 00:04:19,000
实际上将要发生的是它只是打印第一个
actually going to happen is

134
00:04:19,199 --> 00:04:21,460
实际上将要发生的是它只是打印第一个
it's just going to print that first one

135
00:04:21,660 --> 00:04:23,530
因为一旦击中中断声明，卢克就结束了
because as soon as it hits that break

136
00:04:23,730 --> 00:04:25,689
因为一旦击中中断声明，卢克就结束了
statement that said that Luke is over

137
00:04:25,889 --> 00:04:27,939
如果我们在此处放置一个断点并按f5键，则该循环结束
game over for that loop if we place a

138
00:04:28,139 --> 00:04:31,150
如果我们在此处放置一个断点并按f5键，则该循环结束
breakpoint over here and we hit f5

139
00:04:31,350 --> 00:04:32,740
您当然会第一次看到他们的陈述不正确，所以
you'll see the first time of course

140
00:04:32,939 --> 00:04:34,569
您当然会第一次看到他们的陈述不正确，所以
their statements not going to be true so

141
00:04:34,769 --> 00:04:36,310
我们没有中断，但是第二次我们通过这个for循环，这是
we don't break but then the second time

142
00:04:36,509 --> 00:04:38,170
我们没有中断，但是第二次我们通过这个for循环，这是
we run through this for loop and this is

143
00:04:38,370 --> 00:04:40,329
当我们击中休息时为真，猜想如果我现在转弯时会直跳
true when we hit break guess what if I

144
00:04:40,529 --> 00:04:42,160
当我们击中休息时为真，猜想如果我现在转弯时会直跳
hit a turn right now we jump straight

145
00:04:42,360 --> 00:04:43,900
从for循环中移至下一行代码，这是我们的跟踪中断
out of the for loop into the next line

146
00:04:44,100 --> 00:04:47,199
从for循环中移至下一行代码，这是我们的跟踪中断
of code that's a trace break our follows

147
00:04:47,399 --> 00:04:48,730
完全，当然，循环内的这类控制流语句
completely and of course these kind of

148
00:04:48,930 --> 00:04:50,020
完全，当然，循环内的这类控制流语句
control flow statements inside loops

149
00:04:50,220 --> 00:04:52,120
适用于所有循环，因此for循环while循环执行while循环，它们的工作原理相同
apply to all loops so for loops while

150
00:04:52,319 --> 00:04:54,160
适用于所有循环，因此for循环while循环执行while循环，它们的工作原理相同
loops do while loops they work the same

151
00:04:54,360 --> 00:04:56,650
在所有循环中都可以，最后一个是返回，现在这是一个函数
in all loops all right and the final one

152
00:04:56,850 --> 00:04:59,530
在所有循环中都可以，最后一个是返回，现在这是一个函数
we have is return now this is a function

153
00:04:59,730 --> 00:05:01,689
它必须返回一个整数，所以我们不能只是输入回车，我们必须
that has to return an integer so we

154
00:05:01,889 --> 00:05:03,699
它必须返回一个整数，所以我们不能只是输入回车，我们必须
can't just type in return we have to

155
00:05:03,899 --> 00:05:05,290
返回我们尝试执行的操作，然后在此处控制七个
return something we try and do something

156
00:05:05,490 --> 00:05:07,420
返回我们尝试执行的操作，然后在此处控制七个
like this and we here controller seven

157
00:05:07,620 --> 00:05:09,670
它会失败，因为您可以看到告诉我们main函数必须
it's going to fail because you can see

158
00:05:09,870 --> 00:05:12,160
它会失败，因为您可以看到告诉我们main函数必须
is telling us that main function must

159
00:05:12,360 --> 00:05:14,500
返回一个值，因为声明声明它具有一定的主动性，所以我
return a value because the declaration

160
00:05:14,699 --> 00:05:16,449
返回一个值，因为声明声明它具有一定的主动性，所以我
states it has certain initiative so I

161
00:05:16,649 --> 00:05:19,060
可以做类似返回0到0的事情，当然是有效的整数，如果我运行
could do something like return 0 to 0 of

162
00:05:19,259 --> 00:05:21,340
可以做类似返回0到0的事情，当然是有效的整数，如果我运行
course is a valid integer and if I run

163
00:05:21,540 --> 00:05:23,980
该代码实际上将立即显示以关闭我们的程序
this code it's actually going to

164
00:05:24,180 --> 00:05:25,449
该代码实际上将立即显示以关闭我们的程序
immediately appear to close our program

165
00:05:25,649 --> 00:05:27,250
因为我们当然从来没有去过性病看狗狗得到这是
because of course we never get to the

166
00:05:27,449 --> 00:05:29,620
因为我们当然从来没有去过性病看狗狗得到这是
STD see in dogs get statement which is

167
00:05:29,819 --> 00:05:31,540
该声明使我们的窗口保持打开状态，因为它期望我们提供
the statement that keeps our window open

168
00:05:31,740 --> 00:05:33,040
该声明使我们的窗口保持打开状态，因为它期望我们提供
because it's expecting us to provide

169
00:05:33,240 --> 00:05:35,110
输入，所以如果我再次在此处放置断点并运行程序，您将看到
input so if I again put a breakpoint

170
00:05:35,310 --> 00:05:37,030
输入，所以如果我再次在此处放置断点并运行程序，您将看到
here and run my program you'll see the

171
00:05:37,230 --> 00:05:38,560
第一次没关系，它将记录您好世界，而实际上我们确实陷入了困境
first time it's fine it'll log hello

172
00:05:38,759 --> 00:05:40,300
第一次没关系，它将记录您好世界，而实际上我们确实陷入了困境
world and we do actually get that hollow

173
00:05:40,500 --> 00:05:42,370
世界在这里登录，然后第二次我点击return 0就是这样
world logging up here then a second time

174
00:05:42,569 --> 00:05:44,530
世界在这里登录，然后第二次我点击return 0就是这样
when I hit return 0 that's it look at

175
00:05:44,730 --> 00:05:46,210
由于该功能结束，因此它会右跳到该大括号
that it jumps right to that end curly

176
00:05:46,410 --> 00:05:48,759
由于该功能结束，因此它会右跳到该大括号
bracket because this function is over

177
00:05:48,959 --> 00:05:50,560
现在，在return语句中需要注意的另一件事是您没有
now one other thing to note with the

178
00:05:50,759 --> 00:05:52,300
现在，在return语句中需要注意的另一件事是您没有
return statement is that you don't have

179
00:05:52,500 --> 00:05:53,980
在循环内使用return的方式与继续和中断相同
to use return inside a loop the same way

180
00:05:54,180 --> 00:05:55,660
在循环内使用return的方式与继续和中断相同
that you have to with continue and break

181
00:05:55,860 --> 00:05:57,400
返回值可以写在任何地方，所以我可以做类似v更大的事情
returns can be written anywhere so I

182
00:05:57,600 --> 00:05:59,710
返回值可以写在任何地方，所以我可以做类似v更大的事情
could do something like if v is greater

183
00:05:59,910 --> 00:06:02,949
比8返回零，在这种情况下，我当然必须提供一个值，因为
than 8 return zero and of course I have

184
00:06:03,149 --> 00:06:04,420
比8返回零，在这种情况下，我当然必须提供一个值，因为
to provide a value in this case because

185
00:06:04,620 --> 00:06:06,550
此函数返回整数，因此不一定在循环内返回
this function returns an integer so

186
00:06:06,750 --> 00:06:08,560
此函数返回整数，因此不一定在循环内返回
return not necessarily inside a loop can

187
00:06:08,759 --> 00:06:10,569
绝对在代码中的任何地方，甚至都不需要
be absolutely anywhere in your code you

188
00:06:10,769 --> 00:06:11,740
绝对在代码中的任何地方，甚至都不需要
also don't even have to have an if

189
00:06:11,939 --> 00:06:13,180
声明你可能会有这样的事情，但是在这种情况下当然有
statement you could have something like

190
00:06:13,379 --> 00:06:15,670
声明你可能会有这样的事情，但是在这种情况下当然有
this but of course in this case there is

191
00:06:15,870 --> 00:06:17,680
在任何情况下都不会触发此行，因此实际上
no circumstance under which this line

192
00:06:17,879 --> 00:06:18,819
在任何情况下都不会触发此行，因此实际上
would get triggered so it is actually

193
00:06:19,019 --> 00:06:21,100
被称为Det代码，某些编译器（尤其是其他语言）将
known as Det code and some compilers

194
00:06:21,300 --> 00:06:22,389
被称为Det代码，某些编译器（尤其是其他语言）将
especially in other languages will

195
00:06:22,589 --> 00:06:24,040
实际上限制了您编写这样的代码，因为我的意思是
actually restrict you from writing code

196
00:06:24,240 --> 00:06:25,180
实际上限制了您编写这样的代码，因为我的意思是
like this because I mean at this point

197
00:06:25,379 --> 00:06:26,560
您不妨删除此行，因为它永远不会被调用，所以
you might as well just delete this line

198
00:06:26,759 --> 00:06:27,939
您不妨删除此行，因为它永远不会被调用，所以
it's never going to get called so that's

199
00:06:28,139 --> 00:06:29,740
就控制流语句而言
pretty much it as far as control flow

200
00:06:29,939 --> 00:06:30,468
就控制流语句而言
statements go

201
00:06:30,668 --> 00:06:31,968
turn可以在任何地方使用，基本上可以退出该功能
turn can be used anywhere and it would

202
00:06:32,168 --> 00:06:33,290
turn可以在任何地方使用，基本上可以退出该功能
basically just exit the function

203
00:06:33,490 --> 00:06:34,999
当然要记住，如果您的函数必须返回一个值，则返回必须
remember of course if your function has

204
00:06:35,199 --> 00:06:37,910
当然要记住，如果您的函数必须返回一个值，则返回必须
to return a value then return has to

205
00:06:38,110 --> 00:06:39,889
给它一个值以返回这些控制流语句，基本上构成基础
give it a value to return these control

206
00:06:40,089 --> 00:06:41,660
给它一个值以返回这些控制流语句，基本上构成基础
flow statements basically form the basis

207
00:06:41,860 --> 00:06:44,239
流如何通过循环和if语句进入填充区
of how how your flow goes the pad with

208
00:06:44,439 --> 00:06:46,999
流如何通过循环和if语句进入填充区
loops and if statements and like all of

209
00:06:47,199 --> 00:06:48,499
这些控制流语句本质上是编程的逻辑
these control flow statements that is

210
00:06:48,699 --> 00:06:50,269
这些控制流语句本质上是编程的逻辑
essentially the logic of programming

211
00:06:50,468 --> 00:06:52,639
这些是您可以用来控制程序流程的工具
those are the tools you have access to

212
00:06:52,839 --> 00:06:54,499
这些是您可以用来控制程序流程的工具
to control the flow of your program

213
00:06:54,699 --> 00:06:56,838
基本上，接下来执行哪条线的是那些工具，如Salmons和
basically which line gets executed next

214
00:06:57,038 --> 00:06:59,929
基本上，接下来执行哪条线的是那些工具，如Salmons和
right those tools if Salmons and

215
00:07:00,129 --> 00:07:03,528
这样的条件循环和控制流语句就是
conditionals like that loops and control

216
00:07:03,728 --> 00:07:05,809
这样的条件循环和控制流语句就是
flow statements that is it that is the

217
00:07:06,009 --> 00:07:06,860
实际上唯一可以改变程序行为的东西
only thing that can modify the behavior

218
00:07:07,060 --> 00:07:09,139
实际上唯一可以改变程序行为的东西
of your program short from you actually

219
00:07:09,339 --> 00:07:10,399
写入指令指针可能不会如系列中那样
writing into the instruction pointer

220
00:07:10,598 --> 00:07:12,980
写入指令指针可能不会如系列中那样
don't do that maybe as in series

221
00:07:13,180 --> 00:07:14,718
进展，我们开始实际编写应用程序，我将引用所有
progresses and we start actually writing

222
00:07:14,918 --> 00:07:17,449
进展，我们开始实际编写应用程序，我将引用所有
applications I'll be referencing all of

223
00:07:17,649 --> 00:07:18,679
这些控制流语句始终存在，并向您显示示例
these control flow statements all the

224
00:07:18,879 --> 00:07:20,149
这些控制流语句始终存在，并向您显示示例
time and show you examples of where

225
00:07:20,348 --> 00:07:21,949
它们的使用方式以及如何使用它们以及可能向您展示示例
they're used and how I can use them as

226
00:07:22,149 --> 00:07:23,509
它们的使用方式以及如何使用它们以及可能向您展示示例
well as probably showing you examples

227
00:07:23,709 --> 00:07:25,879
关于如何编写相同的代码但不使用它们，或者我们正在使用
about how I can write the same code but

228
00:07:26,079 --> 00:07:27,350
关于如何编写相同的代码但不使用它们，或者我们正在使用
without using them or we're using a

229
00:07:27,550 --> 00:07:29,269
不同的陈述，所以如果您想要一些更具体的示例
different statement so if you want some

230
00:07:29,468 --> 00:07:30,319
不同的陈述，所以如果您想要一些更具体的示例
more concrete examples of how to

231
00:07:30,519 --> 00:07:32,028
实际的工作只是关注这个系列以及以后的情节
actually work just stay tuned to this

232
00:07:32,228 --> 00:07:33,709
实际的工作只是关注这个系列以及以后的情节
series and in future episodes when we

233
00:07:33,908 --> 00:07:35,329
实际上开始写东西，我们肯定会覆盖这件事，因为
actually start writing stuff we're

234
00:07:35,528 --> 00:07:36,709
实际上开始写东西，我们肯定会覆盖这件事，因为
definitely going to cover this because

235
00:07:36,908 --> 00:07:38,509
再次，这是写作
again this is one of the fundamental

236
00:07:38,709 --> 00:07:39,619
再次，这是写作
building blocks of writing an

237
00:07:39,819 --> 00:07:41,420
应用程序，但是无论如何，我希望你们喜欢这个情节
application but anyway I hope you guys

238
00:07:41,620 --> 00:07:43,610
应用程序，但是无论如何，我希望你们喜欢这个情节
enjoyed this episode if you did please

239
00:07:43,810 --> 00:07:45,230
点击类似按钮，然后在Twitter和Instagram链接上关注我
hit that like button and follow me on

240
00:07:45,430 --> 00:07:46,968
点击类似按钮，然后在Twitter和Instagram链接上关注我
Twitter and Instagram link in the

241
00:07:47,168 --> 00:07:48,499
如果您真的喜欢这部影片，并想显示
description below if you really enjoyed

242
00:07:48,699 --> 00:07:49,399
如果您真的喜欢这部影片，并想显示
this video and you want to show your

243
00:07:49,598 --> 00:07:50,899
支持并确保制作更多这些很棒的视频，以便您可以支持
support and make sure that more of these

244
00:07:51,098 --> 00:07:52,939
支持并确保制作更多这些很棒的视频，以便您可以支持
awesome videos get made you can support

245
00:07:53,139 --> 00:07:55,189
我在patreon图标4/2 Cherno上的链接再次链接将在下面的描述中
me on patreon icon 4/2 Cherno again link

246
00:07:55,389 --> 00:07:56,899
我在patreon图标4/2 Cherno上的链接再次链接将在下面的描述中
will be in the description below next

247
00:07:57,098 --> 00:08:00,439
希望我下次能再见一集
time hope i've got quite the episode for

248
00:08:00,639 --> 00:08:03,990
希望我下次能再见一集
you guys next time goodbye

249
00:08:05,459 --> 00:08:09,099
[音乐]
[Music]

250
00:08:11,199 --> 00:08:15,410
[音乐]你
[Music]

251
00:08:15,610 --> 00:08:20,610
[音乐]你
you

