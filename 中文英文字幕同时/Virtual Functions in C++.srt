﻿1
00:00:00,000 --> 00:00:03,758
hey what's up guys my name is HMO and welcome back to my syphilis gloss series
嘿，大家好，我叫HMO，欢迎回到我的梅毒光泽系列

2
00:00:03,959 --> 00:00:08,080
today I'm going to be talking about virtual functions in c++ over the last
今天，我将在最后一讲C ++中的虚函数

3
00:00:08,279 --> 00:00:10,899
two episodes we've been talking about classes and object-oriented programming
我们一直在谈论两集，涉及类和面向对象的编程

4
00:00:11,099 --> 00:00:14,889
and inheritance and all that stuff and virtual functions are really really
和继承以及所有这些东西和虚函数

5
00:00:15,089 --> 00:00:19,060
important to that whole concept virtual functions allow up to override methods
对于整个概念很重要，虚函数允许最多重写方法

6
00:00:19,260 --> 00:00:23,919
in subclasses so for example let's say we've got two classes a and B B is
在子类中，例如，假设我们有两个类a和BB是

7
00:00:24,118 --> 00:00:28,600
derived from K many would be the subclass of a if we create a method in
如果我们在中创建方法，则从K派生的许多将是a的子类。 

8
00:00:28,800 --> 00:00:32,409
the a class and marketers virtual we have the option of overriding that
虚拟的阶级和营销者，我们可以选择覆盖

9
00:00:32,609 --> 00:00:36,669
method in the B class to get it to do something else as always this is best
 B类中的方法让它像往常一样做其他事情，这是最好的

10
00:00:36,869 --> 00:00:39,909
explained by an example so let's take a look and we're to create two clauses
通过一个例子解释，让我们看一下，我们将创建两个子句

11
00:00:40,109 --> 00:00:43,750
here one's going to be called anti Z which is going to be our base class the
这里将被称为反Z，这将是我们的基类

12
00:00:43,950 --> 00:00:48,309
only thing this empty class is going to have is a public method called get a
这个空类只有一个叫get a的公共方法。 

13
00:00:48,509 --> 00:00:51,759
name which is going to return a string because this is just an entity we're
名称，它将返回一个字符串，因为这只是我们的一个实体

14
00:00:51,960 --> 00:00:55,479
just going to return the word entity next we're going to create another class
只是要返回实体这个词，接下来我们将创建另一个类

15
00:00:55,679 --> 00:00:59,439
player which is going to be a subclass of the entity class we're going to add a
播放器，它将是实体类的子类，我们将添加一个

16
00:00:59,640 --> 00:01:02,969
little bit more of this class firstly we're going to actually store a name
首先，我们将实际存储一个名称

17
00:01:03,170 --> 00:01:09,810
then I'm going to provide a constructor which allows us to specify a name and
那么我将提供一个构造函数，该构造函数允许我们指定名称并

18
00:01:10,010 --> 00:01:14,319
then we're going to give it a method called get name which in this case is
那么我们要给它一个叫做get name的方法，在这种情况下是

19
00:01:14,519 --> 00:01:19,090
going to return this name the name that is the member okay cool so let's take a
将要返回该名称的名称是该成员的名字，很酷，所以让我们

20
00:01:19,290 --> 00:01:22,689
look at how we might use this set up let's see whether we create an ASCII
看看如何使用此设置，让我们看看是否创建ASCII 

21
00:01:22,890 --> 00:01:27,789
here and I'm going to try and print the get name from that FC then I'm going to
在这里，我将尝试从该FC打印获取名称，然后

22
00:01:27,989 --> 00:01:33,308
create a player I'll call this player turn on and I'm going to print the
创建一个播放器，我将其称为打开播放器，然后打印

23
00:01:33,509 --> 00:01:37,179
player's name as well and I'm not going to bother deleting these objects because
玩家的名字，我也不想删除这些对象，因为

24
00:01:37,379 --> 00:01:41,590
this program tonus anyway there's no just no use in doing that let's get at 5
这个程序无论如何都没有用，让我们从5开始

25
00:01:41,790 --> 00:01:44,769
to write our code and if we look at the results alright cool looks pretty good
编写我们的代码，如果我们看一下结果，好酷看起来还不错

26
00:01:44,969 --> 00:01:49,480
well as you've got show no printing everything seems fine however everything
以及您没有显示任何打印内容，但一切似乎都很好

27
00:01:49,680 --> 00:01:54,459
that we've written here so far will crumble if we decide to use a concept of
如果我们决定使用以下概念，那么到目前为止我们在这里写的内容都会崩溃

28
00:01:54,659 --> 00:02:00,069
polymorphism if I start referring to the player as if it was an entity that's
多态性，如果我开始指的是玩家，就好像它是

29
00:02:00,269 --> 00:02:05,500
where we run into problem so for example if I create a variable here called
我们遇到问题的地方，例如，如果我在这里创建一个变量

30
00:02:05,700 --> 00:02:08,800
empty which is actually going to be assigned to T which is of course a
空，实际上将要分配给T，这当然是a 

31
00:02:09,000 --> 00:02:13,450
pointer to a player type right player however now I'm just referring to
指向玩家类型的指针，但是现在我只是指的是玩家

32
00:02:13,650 --> 00:02:19,630
it as an entity if I print this so I'll do entity get name if I run micro here
如果我打印它，它将作为一个实体，所以如果我在这里运行micro，我将做实体获得名称

33
00:02:19,830 --> 00:02:23,320
you can see we get entity printing however we would have course expect
您可以看到我们得到了实体打印，但是我们当然会有期望

34
00:02:23,520 --> 00:02:27,340
player because even though we're referring to this entity as an entity
玩家，因为即使我们将此实体称为实体

35
00:02:27,539 --> 00:02:32,800
pointer it is actually a player it's an instance of the player class possibly a
指针，它实际上是一个播放器，它是播放器类的一个实例，可能是

36
00:02:33,000 --> 00:02:37,990
better example is if we had a prince named function which took in an entity
更好的例子是，如果我们有一个名为function的王子，它接受了一个实体

37
00:02:38,189 --> 00:02:44,200
and then here we just called see out and see get name and I'll replace these with
然后在这里，我们只是打电话给see out，看看获得名称，我将其替换为

38
00:02:44,400 --> 00:02:51,400
print name a and print name P so now we've got seemingly one function which
打印名称a和打印名称P，所以现在我们有了一个似乎可以

39
00:02:51,599 --> 00:02:54,789
takes in any kind of entity of course so you can see that we don't get any
当然可以接受任何种类的实体，因此您可以看到我们没有得到任何实体

40
00:02:54,989 --> 00:03:00,310
compile errors when we try to pass in P because P is an entity right player is
当我们尝试传递P时会编译错误，因为P是一个实体，玩家是

41
00:03:00,509 --> 00:03:03,789
an entity and all we're doing here is calling the entities get name function
一个实体，我们在这里要做的就是调用实体get name函数

42
00:03:03,989 --> 00:03:08,500
which we would expect to be this get name for our entity and then this get
我们希望它成为我们实体的名称，然后得到

43
00:03:08,699 --> 00:03:12,819
name for our player however if we run our code you consider to get entity
玩家的名字，但是如果我们运行我们的代码，您会考虑获得实体

44
00:03:13,019 --> 00:03:16,180
printing twice why is that that's there's incorrect there instances
打印两次为什么那是不正确的实例

45
00:03:16,379 --> 00:03:19,030
happening is because if we just declare our functions normally our methods
发生的原因是，如果我们只是正常地声明我们的函数，我们的方法

46
00:03:19,229 --> 00:03:23,920
normally inside our classes then when it comes time to call a method is going to
通常在我们的类中，那么当需要调用方法时

47
00:03:24,120 --> 00:03:28,509
call whatever method belongs to the type and of course if we look at this print
调用属于该类型的任何方法，当然，如果我们看一下此打印， 

48
00:03:28,709 --> 00:03:31,960
main function it takes in an entity points off meaning that when we call the
它接受一个实体的主要功能指向的意思是当我们调用

49
00:03:32,159 --> 00:03:36,129
get name function inside entity it's going to look at entity and just call
在实体内部获取名称函数，它将查看实体并调用

50
00:03:36,329 --> 00:03:41,020
get name that's it however we want C++ to somehow realize that hang on a minute
得到它的名字，但是我们希望C ++以某种方式意识到这一点

51
00:03:41,219 --> 00:03:45,670
the entity that I passed into here is actually a player so please call this
我传入这里的实体实际上是玩家，因此请致电此人

52
00:03:45,870 --> 00:03:50,920
get name function that is where virtual functions come in virtual functions that
获取名称函数，即虚函数出现在虚函数中

53
00:03:51,120 --> 00:03:54,219
reduce something called dynamic dispatch which compile is typically implemented
减少通常称为“动态调度”的编译

54
00:03:54,419 --> 00:03:58,210
by our V table a V table is basically table which contains a mapping for all
根据我们的V表，V表基本上是包含所有映射的表

55
00:03:58,409 --> 00:04:02,349
the virtual functions but our base class so that we can actually map them to the
虚拟函数，但我们的基类，以便我们实际上可以将它们映射到

56
00:04:02,549 --> 00:04:05,710
correct overwritten function at runtime in the future I'm going to do a whole in
将来在运行时纠正覆盖的函数，我将做一个整体

57
00:04:05,909 --> 00:04:09,159
depth video on how we tabled work and all that so stick around for that if
关于我们如何安排工作的深度视频，所有这些都坚持下去，如果

58
00:04:09,359 --> 00:04:12,069
you're interested but to keep it simple all you basically need to know is that
您很感兴趣，但为了简单起见，您基本上需要知道的是

59
00:04:12,269 --> 00:04:15,179
if you want to override function you have to mark the base
如果要覆盖功能，则必须标记基准

60
00:04:15,378 --> 00:04:19,020
function in the base bar as virtual let's go back to our code and I'm just
在基础栏中作为虚拟功能运行让我们回到我们的代码，我只是

61
00:04:19,220 --> 00:04:23,910
simply going to add the word virtual to the front of this get name function that
只需将单词virtual添加到此get name函数的前面即可

62
00:04:24,110 --> 00:04:27,930
is in our base up in our entity class it might not seem like much but this
在我们实体类的基础上，看起来似乎不多，但这

63
00:04:28,129 --> 00:04:32,160
basically tells the compiler hey generate a V table for this function so
基本上告诉编译器嘿为此函数生成一个V表，所以

64
00:04:32,360 --> 00:04:35,850
that if it's overwritten you can point to the correct function with this change
如果它被覆盖，您可以通过此更改指向正确的功能

65
00:04:36,050 --> 00:04:38,400
let's hit f5 to run our code and look at that
让我们按f5来运行我们的代码并查看该代码

66
00:04:38,600 --> 00:04:42,028
we've got NC and shown on printing correctly now another thing that we can
我们已经有了NC并可以正确打印，现在又可以

67
00:04:42,228 --> 00:04:45,990
do that was introduced in tables of 11 is actually marked this overridden
表11中引入的操作实际上被标记为已覆盖

68
00:04:46,189 --> 00:04:51,060
function with the keyword override right over here this isn't required of course
在这里使用关键字覆盖功能，当然这不是必需的

69
00:04:51,259 --> 00:04:54,480
you can see that we just ran our code without that and it worked fine however
您可以看到我们只是在没有代码的情况下运行了代码，但是效果很好

70
00:04:54,680 --> 00:04:57,540
you should still do this it goes first of all it makes it a little bit more
您仍然应该这样做，首先它会使它变得更多一点

71
00:04:57,740 --> 00:05:00,990
readable since we now know this is actually an overridden function but also
可读，因为我们现在知道这实际上是一个重写的函数

72
00:05:01,189 --> 00:05:03,930
it just helps us a little bit with preventing bugs tutors like spelling
它仅对防止拼写错误等错误教师有所帮助

73
00:05:04,129 --> 00:05:06,870
mistakes and all that like for example five types get a name with a lowercase
错误和所有类似的东西，例如五种类型都使用小写字母命名

74
00:05:07,069 --> 00:05:10,560
and you can see we get an error because there's no such function in the base but
您会看到我们收到一个错误，因为在基础中没有这样的功能，但是

75
00:05:10,759 --> 00:05:14,310
for us to override or if we try an override function that isn't marked as
供我们覆盖，或者尝试使用未标记为的覆盖函数

76
00:05:14,509 --> 00:05:17,699
virtual you can see it also gives us an error so it's just something that helps
虚拟的，您可以看到它也给我们带来了一个错误，因此这只是有帮助

77
00:05:17,899 --> 00:05:21,210
us out okay so that's basically what a virtual function is virtual functions
我们出去吧，所以基本上这就是虚拟功能

78
00:05:21,410 --> 00:05:24,660
aren't free though unfortunately there are two runtime costs associated with
不是免费的，尽管不幸的是，有两个与

79
00:05:24,860 --> 00:05:28,410
virtual functions firstly we have the additional memory that is required in
虚拟函数首先，我们需要在

80
00:05:28,610 --> 00:05:31,500
order for us to store that be table so that we can dispatch to the correct
以便我们存储该表，以便我们可以分派到正确的位置

81
00:05:31,699 --> 00:05:36,509
function that includes a member pointer in the actual base path that points to
该函数在实际的基本路径中包含一个指向的成员指针

82
00:05:36,709 --> 00:05:40,110
the V table and secondly every time we call a virtual function we have to go
 V表，其次是每次调用虚函数时

83
00:05:40,310 --> 00:05:43,949
through that table to determine which function to actually map to which is an
通过该表确定实际映射到哪个功能的

84
00:05:44,149 --> 00:05:47,639
additional performance penalty and because of these costs some people just
额外的性能损失，由于这些成本，有些人只是

85
00:05:47,839 --> 00:05:51,360
prefer not to use virtual functions at all honestly in my experience I've never
根据我的经验，我不希望老实说完全不使用虚拟函数

86
00:05:51,560 --> 00:05:57,750
encountered this to be so costly that that it would make any difference if I
遇到这样的事情是如此昂贵，以至于如果我

87
00:05:57,949 --> 00:06:00,899
stopped using virtual functions so personally I use them all the time
停止使用虚拟功能，所以我个人一直都在使用它们

88
00:06:01,098 --> 00:06:05,389
without any issue maybe if you are on some embedded platform which has
如果您使用的是具有

89
00:06:05,589 --> 00:06:10,588
absolutely terrible performance and every CPU slice accounts maybe then
绝对糟糕的性能，然后每个CPU分片都可能

90
00:06:10,788 --> 00:06:15,660
avoid virtual functions but otherwise I really can't tell you not to use them
避免使用虚函数，否则我真的无法告诉您不要使用它们

91
00:06:15,860 --> 00:06:19,319
because of performance because because it's just such a minimal impact that you
因为性能，因为它对您的影响很小

92
00:06:19,519 --> 00:06:22,560
probably won't notice it at all hope you guys enjoyed this video on virtual
可能根本不会注意到它，希望你们喜欢虚拟视频

93
00:06:22,759 --> 00:06:25,709
functions if you have an expression comment just leave them below and I'll
函数，如果您有表达式注释，只需将它们留在下面，我会

94
00:06:25,908 --> 00:06:28,680
see you guys next time goodbye [Applause]
下次见，再见[鼓掌] 

95
00:06:28,879 --> 00:06:34,060
[Music]
[音乐]

96
00:06:37,029 --> 00:06:42,029
[Music]
 [音乐] 

