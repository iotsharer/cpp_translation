﻿1
00:00:00,000 --> 00:00:01,360
嘿，大家好，我叫中国回到我的房地产半衰期
hey what's up guys my name is a China

2
00:00:01,560 --> 00:00:02,948
嘿，大家好，我叫中国回到我的房地产半衰期
welcome back to my estate bust loss

3
00:00:03,149 --> 00:00:04,859
系列，所以首先让我说声谢谢，谢谢大家
series so first of all let me just say

4
00:00:05,059 --> 00:00:07,448
系列，所以首先让我说声谢谢，谢谢大家
huge thank you to all of you for helping

5
00:00:07,649 --> 00:00:10,089
我吸引了100,000个订阅者，这在YouTube最初是很漫长的旅程
me reach 100,000 subscribers it's been

6
00:00:10,289 --> 00:00:11,769
我吸引了100,000个订阅者，这在YouTube最初是很漫长的旅程
quite a journey at first on YouTube in

7
00:00:11,968 --> 00:00:14,530
2011年，我想最认真的是2012年，我可能更认真地对待
2011 and I guess most seriously in 2012

8
00:00:14,730 --> 00:00:16,359
2011年，我想最认真的是2012年，我可能更认真地对待
I'm even more seriously probably at the

9
00:00:16,559 --> 00:00:17,710
去年年初，当我停在cipa Plus系列和OpenGL系列时
start of last year when I stopped at the

10
00:00:17,910 --> 00:00:20,169
去年年初，当我停在cipa Plus系列和OpenGL系列时
cipa Plus series and the OpenGL serie

11
00:00:20,368 --> 00:00:22,449
因为那是一段漫长的旅程，所以我真的感到非常感谢和非常
since has been quite a journey and I

12
00:00:22,649 --> 00:00:24,370
因为那是一段漫长的旅程，所以我真的感到非常感谢和非常
really do feel very thankful and very

13
00:00:24,570 --> 00:00:25,720
幸运的是有十万人跟随这个频道
blessed to have a hundred thousand

14
00:00:25,920 --> 00:00:28,629
幸运的是有十万人跟随这个频道
people following this channel so a huge

15
00:00:28,829 --> 00:00:30,429
谢谢大家，今天我们要谈论的只是善意的
thank you to all of you and today what

16
00:00:30,629 --> 00:00:31,870
谢谢大家，今天我们要谈论的只是善意的
we're going to talk about is just kind

17
00:00:32,070 --> 00:00:33,549
简短的视觉工作室技巧，只是一般的技巧
of a bit of a quick visual studio tip

18
00:00:33,750 --> 00:00:34,899
简短的视觉工作室技巧，只是一般的技巧
and just a tip in general for

19
00:00:35,100 --> 00:00:36,820
开发和调试，这将是断点，但不是
development and for debugging and that's

20
00:00:37,020 --> 00:00:38,320
开发和调试，这将是断点，但不是
going to be about breakpoints but not

21
00:00:38,520 --> 00:00:40,059
关于断点，如果您不知道断点肯定是什么
just about breakpoints if you don't know

22
00:00:40,259 --> 00:00:41,140
关于断点，如果您不知道断点肯定是什么
what a breakpoint is definitely watch

23
00:00:41,340 --> 00:00:42,849
该视频，但这不只是正常的断点，而是
that video but this isn't just about

24
00:00:43,049 --> 00:00:44,559
该视频，但这不只是正常的断点，而是
normal breakpoints it's going to be

25
00:00:44,759 --> 00:00:46,989
关于可以应用于断点的条件和动作，例如条件或
about conditions and actions that we can

26
00:00:47,189 --> 00:00:49,448
关于可以应用于断点的条件和动作，例如条件或
apply to breakpoints so by conditions or

27
00:00:49,649 --> 00:00:51,189
有条件的断点，我的意思是，我们实际上可以告诉他们把
conditional breakpoints what I mean is

28
00:00:51,390 --> 00:00:53,439
有条件的断点，我的意思是，我们实际上可以告诉他们把
we can actually tell them to put the

29
00:00:53,640 --> 00:00:54,969
调试器我想在此处放置一个断点，但我只希望该断点
debugger I want to place a breakpoint

30
00:00:55,170 --> 00:00:57,549
调试器我想在此处放置一个断点，但我只希望该断点
here but I only want the breakpoint to

31
00:00:57,750 --> 00:00:59,158
在一定条件下触发内存中的某些东西基本上是
trigger under a certain condition

32
00:00:59,359 --> 00:01:01,838
在一定条件下触发内存中的某些东西基本上是
something in memory basically that's

33
00:01:02,039 --> 00:01:04,358
如果是这样的话，将符合我的标准，请触发此断点
going to meet my criteria if that's the

34
00:01:04,558 --> 00:01:06,278
如果是这样的话，将符合我的标准，请触发此断点
case then please trigger this breakpoint

35
00:01:06,478 --> 00:01:08,829
动作断点的可行断点仅仅是
and an actionable break points of action

36
00:01:09,030 --> 00:01:10,750
动作断点的可行断点仅仅是
breakpoints are just something that will

37
00:01:10,950 --> 00:01:12,129
允许我们采取某种措施，通常在控制台上打印一些内容
allow us to take some kind of action

38
00:01:12,329 --> 00:01:14,259
允许我们采取某种措施，通常在控制台上打印一些内容
generally print something to the console

39
00:01:14,459 --> 00:01:17,409
当击中断点时，我猜有两种类型的动作
when a breakpoint is hit so there's kind

40
00:01:17,609 --> 00:01:19,450
当击中断点时，我猜有两种类型的动作
of two I guess types of action

41
00:01:19,650 --> 00:01:21,429
断点，让您可以继续执行
breakpoints there's one that let the

42
00:01:21,629 --> 00:01:23,200
断点，让您可以继续执行
lets you kind of just continue execution

43
00:01:23,400 --> 00:01:24,879
在打印所需内容时，例如，假设我要登录
while printing what you want so for

44
00:01:25,079 --> 00:01:26,709
在打印所需内容时，例如，假设我要登录
example let's just say I wanted to log

45
00:01:26,909 --> 00:01:29,019
鼠标位置每当鼠标位置为
the mouse position what I could do is

46
00:01:29,219 --> 00:01:30,640
鼠标位置每当鼠标位置为
every time the mouse position is

47
00:01:30,840 --> 00:01:32,500
之所以记录下来，是因为就像每次发生鼠标移动事件一样，例如
recorded because like every time I mouse

48
00:01:32,700 --> 00:01:34,840
之所以记录下来，是因为就像每次发生鼠标移动事件一样，例如
move move event happens for example I

49
00:01:35,040 --> 00:01:37,539
可能让该断点实际将某些内容打印到控制台，但保持
could have that breakpoint actually

50
00:01:37,739 --> 00:01:39,549
可能让该断点实际将某些内容打印到控制台，但保持
print something to the console but keep

51
00:01:39,750 --> 00:01:41,259
我正在运行的驱动程序，或者我可以将其打印到控制台上
the driver I'm running or I could get it

52
00:01:41,459 --> 00:01:42,609
我正在运行的驱动程序，或者我可以将其打印到控制台上
to like print something to the console

53
00:01:42,810 --> 00:01:44,019
但仍然会中断程序，它们仍会暂停执行
but still break the program's they're

54
00:01:44,219 --> 00:01:46,269
但仍然会中断程序，它们仍会暂停执行
still gonna pause execution of the

55
00:01:46,469 --> 00:01:48,609
程序，以便我们可以检查内存中的其他内容，以便再次检查断点
program so that we can inspect other

56
00:01:48,810 --> 00:01:51,459
程序，以便我们可以检查内存中的其他内容，以便再次检查断点
things in memory so again breakpoints in

57
00:01:51,659 --> 00:01:53,168
一般而言，请确保您观看了该视频，因为如果您处于断点，
general make sure you watch that video

58
00:01:53,368 --> 00:01:55,238
一般而言，请确保您观看了该视频，因为如果您处于断点，
because if you're in order break point

59
00:01:55,438 --> 00:01:58,619
这不会真的有多大帮助，但动作断点和有条件的
is this isn't gonna really help much but

60
00:01:58,819 --> 00:02:00,429
这不会真的有多大帮助，但动作断点和有条件的
action breakpoints and conditional

61
00:02:00,629 --> 00:02:02,469
断点作为一种进一步的调试工具确实非常有用
breakpoints are really really useful as

62
00:02:02,670 --> 00:02:05,189
断点作为一种进一步的调试工具确实非常有用
kind of a further debugging tool now

63
00:02:05,390 --> 00:02:06,939
这些断点可以在代码中实际完成的所有操作
everything that you can do with these

64
00:02:07,140 --> 00:02:08,350
这些断点可以在代码中实际完成的所有操作
breakpoints you can actually do in code

65
00:02:08,550 --> 00:02:10,990
好吧，我可以只写一个if语句，但是检查一下
okay I could just write an if statement

66
00:02:11,189 --> 00:02:13,360
好吧，我可以只写一个if语句，但是检查一下
for example but check something

67
00:02:13,560 --> 00:02:15,399
然后显然在if语句中插入一个断点
and then obviously just put a breakpoint

68
00:02:15,598 --> 00:02:17,050
然后显然在if语句中插入一个断点
inside that if statement and if they've

69
00:02:17,250 --> 00:02:18,399
七个是正确的，那么断点将被绊倒，我什至没有
seven is true then the break point will

70
00:02:18,598 --> 00:02:20,080
七个是正确的，那么断点将被绊倒，我什至没有
be tripped all right I don't even have

71
00:02:20,280 --> 00:02:21,939
设置一个断点，我什至可以编写调试中断或类似的内容
to put a break point I could just even

72
00:02:22,139 --> 00:02:23,590
设置一个断点，我什至可以编写调试中断或类似的内容
write debug break or something like that

73
00:02:23,789 --> 00:02:25,390
某种编译器内在函数，如果它是
some kind of compiler intrinsic that

74
00:02:25,590 --> 00:02:27,219
某种编译器内在函数，如果它是
actually will break the debugger if it's

75
00:02:27,419 --> 00:02:29,110
附加在给定的代码行上，类似于我们对断言的处理
attached at that given line of code kind

76
00:02:29,310 --> 00:02:30,670
附加在给定的代码行上，类似于我们对断言的处理
of like what we do with with assertion

77
00:02:30,870 --> 00:02:34,330
映射器是一种可能性，并且具有动作断点再次打印
mappers that's one possibility and with

78
00:02:34,530 --> 00:02:35,618
映射器是一种可能性，并且具有动作断点再次打印
action break points again printing

79
00:02:35,818 --> 00:02:36,939
控制台的某些事情显然可以在代码中完成，但是
something to the console obviously you

80
00:02:37,139 --> 00:02:38,500
控制台的某些事情显然可以在代码中完成，但是
can do that in code but where this

81
00:02:38,699 --> 00:02:41,200
真正派上用场的地方是我们可能会将程序编成某种形式
really comes in handy is where we might

82
00:02:41,400 --> 00:02:42,939
真正派上用场的地方是我们可能会将程序编成某种形式
have gotten our program into some kind

83
00:02:43,139 --> 00:02:46,140
状态可能是一个错误，我们想检查某种
of state that is a bug perhaps and we

84
00:02:46,340 --> 00:02:49,840
状态可能是一个错误，我们想检查某种
want to inspect certain kind of

85
00:02:50,039 --> 00:02:51,700
属性来找出导致此错误的原因是什么或我为什么
properties to figure out what what like

86
00:02:51,900 --> 00:02:53,650
属性来找出导致此错误的原因是什么或我为什么
what's caused this bug or like why am I

87
00:02:53,849 --> 00:02:55,240
在这种状态下，或者甚至是白天，我的东西是什么
in this state or what even is this

88
00:02:55,439 --> 00:02:57,580
在这种状态下，或者甚至是白天，我的东西是什么
daylight right like what is my stuff set

89
00:02:57,780 --> 00:02:59,500
为什么会发生这种情况？如果我们真的要
to why have why is this happened and

90
00:02:59,699 --> 00:03:03,640
为什么会发生这种情况？如果我们真的要
obviously if we were to actually if we

91
00:03:03,840 --> 00:03:05,349
实际上是决定将代码添加到我们的代码库中，但必须
were to actually kind of decide to add

92
00:03:05,549 --> 00:03:06,910
实际上是决定将代码添加到我们的代码库中，但必须
code to our code base but have to

93
00:03:07,110 --> 00:03:08,530
终止应用程序，然后重新编译它，那将是一个
terminate the application and then

94
00:03:08,729 --> 00:03:10,060
终止应用程序，然后重新编译它，那将是一个
recompile it and then that would be an

95
00:03:10,259 --> 00:03:11,590
问题，因为也许我们无法达到规定的要求，也许是
issue because maybe we couldn't get it

96
00:03:11,789 --> 00:03:12,819
问题，因为也许我们无法达到规定的要求，也许是
to the stated yeah maybe it was really

97
00:03:13,019 --> 00:03:15,310
很难陈述，我的意思是通常这只会减慢我们
difficult to get to state and I mean

98
00:03:15,509 --> 00:03:17,080
很难陈述，我的意思是通常这只会减慢我们
generally it will just slow down our

99
00:03:17,280 --> 00:03:18,819
工作流，如果我们必须停止在代码中进行协调
workflow if we have to keep kind of

100
00:03:19,019 --> 00:03:21,099
工作流，如果我们必须停止在代码中进行协调
stopping reconciling out in code like

101
00:03:21,299 --> 00:03:22,629
然后上升到导致为什么喜欢什么的状态
and then getting up to the state that

102
00:03:22,829 --> 00:03:24,250
然后上升到导致为什么喜欢什么的状态
led up to like why were like what

103
00:03:24,449 --> 00:03:25,719
正是我们要调试，所有这些都是一个很大的问题，这确实
exactly we want to debug and all of that

104
00:03:25,919 --> 00:03:28,990
正是我们要调试，所有这些都是一个很大的问题，这确实
so it's a huge issue and this really

105
00:03:29,189 --> 00:03:30,400
确实对我们有很大帮助，所以让我们深入研究一些代码
does help us out a lot so let's just

106
00:03:30,599 --> 00:03:32,110
确实对我们有很大帮助，所以让我们深入研究一些代码
dive into some code whatever here

107
00:03:32,310 --> 00:03:34,840
实际上是Sparky的源代码，这是我制作的游戏引擎
actually is the source code to Sparky

108
00:03:35,039 --> 00:03:36,280
实际上是Sparky的源代码，这是我制作的游戏引擎
which is a game engine that I made about

109
00:03:36,479 --> 00:03:39,368
三年前，然后我想做的就是开始发布
three years ago and then what I want to

110
00:03:39,568 --> 00:03:40,509
三年前，然后我想做的就是开始发布
do is I'll just launch and kind of show

111
00:03:40,709 --> 00:03:41,710
你们我们正在处理的只是一个示例项目，可能是
you guys what we're dealing with this is

112
00:03:41,909 --> 00:03:43,030
你们我们正在处理的只是一个示例项目，可能是
just an example project it could be

113
00:03:43,229 --> 00:03:44,590
什么都没有，但我认为我实际上会使用某种实际项目而不是
anything but I thought I'd actually use

114
00:03:44,789 --> 00:03:46,360
什么都没有，但我认为我实际上会使用某种实际项目而不是
kind of a real-world project instead of

115
00:03:46,560 --> 00:03:49,509
只是做类似我不知道我们某种奇怪的Hello World应用程序的事情
just doing something like I don't know

116
00:03:49,709 --> 00:03:51,819
只是做类似我不知道我们某种奇怪的Hello World应用程序的事情
our some kind of weird hello world app

117
00:03:52,019 --> 00:03:53,319
可能不会显示太多，所以我们在这里所拥有的仅仅是
that probably wouldn't show you much so

118
00:03:53,519 --> 00:03:55,000
可能不会显示太多，所以我们在这里所拥有的仅仅是
all we have here is really just is this

119
00:03:55,199 --> 00:03:57,909
场景中有一些图形，当我终于打开时，我们有了
scene which has some graphics in and

120
00:03:58,109 --> 00:04:00,368
场景中有一些图形，当我终于打开时，我们有了
when I finally opens and then we have

121
00:04:00,568 --> 00:04:03,610
就像您知道一些粗糙度不同的金属和塑料球
like you know some metal and plastic

122
00:04:03,810 --> 00:04:05,110
就像您知道一些粗糙度不同的金属和塑料球
balls of various roughnesses and all

123
00:04:05,310 --> 00:04:06,580
实际上，我认为OpenGL版本或Sparky不太好
that and in fact I think the OpenGL

124
00:04:06,780 --> 00:04:09,009
实际上，我认为OpenGL版本或Sparky不太好
version or Sparky is a bit not as good

125
00:04:09,209 --> 00:04:10,330
作为DirectX版本，因此可能会有一些瑕疵，但无论如何，关键是
as the DirectX version so there might be

126
00:04:10,530 --> 00:04:11,710
作为DirectX版本，因此可能会有一些瑕疵，但无论如何，关键是
some artifacts but anyway the point is

127
00:04:11,909 --> 00:04:13,810
我们这里有一个3D应用，我想做的是在运行时
we have kind of a 3d app here and what I

128
00:04:14,009 --> 00:04:15,550
我们这里有一个3D应用，我想做的是在运行时
want to do is while this is running

129
00:04:15,750 --> 00:04:17,800
没有真正关闭它，我要决定要调试
without actually closing it I'm gonna

130
00:04:18,000 --> 00:04:19,810
没有真正关闭它，我要决定要调试
I've just decided that I want to debug

131
00:04:20,009 --> 00:04:21,158
鼠标的位置，因为鼠标的位置似乎有点不对
the mouse position because the position

132
00:04:21,358 --> 00:04:22,360
鼠标的位置，因为鼠标的位置似乎有点不对
the mouse just seems to be a bit wrong

133
00:04:22,560 --> 00:04:24,100
所以我要做的是转到
so what I'll do is I'll go to the code

134
00:04:24,300 --> 00:04:24,439
所以我要做的是转到
that

135
00:04:24,639 --> 00:04:26,778
在这种情况下，一定要记录金额位置开关，并且用win32输入
surely record amounts position switch in

136
00:04:26,978 --> 00:04:29,420
在这种情况下，一定要记录金额位置开关，并且用win32输入
this case is with win32 input does every

137
00:04:29,620 --> 00:04:31,429
是的，我只是在正确的视野中关闭，以腾出更多空间，然后我
be I'm just closes in the right view to

138
00:04:31,629 --> 00:04:33,949
是的，我只是在正确的视野中关闭，以腾出更多空间，然后我
make some more space and then what I'm

139
00:04:34,149 --> 00:04:35,929
我们要做的就是找到嘴巴，让我们说正确的按钮回调，
going to do is find the mouth let's just

140
00:04:36,129 --> 00:04:37,278
我们要做的就是找到嘴巴，让我们说正确的按钮回调，
say the button callback right so every

141
00:04:37,478 --> 00:04:38,509
一次单击按钮就会被调用，我们有这种鼠标
time a button is clicked this gets

142
00:04:38,709 --> 00:04:41,179
一次单击按钮就会被调用，我们有这种鼠标
called and we have this kind of mouse

143
00:04:41,379 --> 00:04:43,399
现在在这里设置位置x和y我要调试它，所以我要做的是
position x and y that gets set here now

144
00:04:43,598 --> 00:04:45,410
现在在这里设置位置x和y我要调试它，所以我要做的是
I want to debug that so what I'll do is

145
00:04:45,610 --> 00:04:47,540
我将跳到这里的结尾，实际上我当然不会添加
I'll jump to the end of here and

146
00:04:47,740 --> 00:04:49,639
我将跳到这里的结尾，实际上我当然不会添加
actually of course I'm not gonna add

147
00:04:49,839 --> 00:04:51,230
我要做的只是编写代码，在该行上放置一个断点
code all I'm really gonna do is I'm just

148
00:04:51,430 --> 00:04:52,790
我要做的只是编写代码，在该行上放置一个断点
gonna put a breakpoint like on this line

149
00:04:52,990 --> 00:04:54,920
例如，我要右键单击该断点，然后转到
for example I'm gonna right click on

150
00:04:55,120 --> 00:04:56,299
例如，我要右键单击该断点，然后转到
this break point and then go to

151
00:04:56,499 --> 00:04:59,600
条件还不错，或者实际上很好，让我们先登录即可，我们可以转到
conditions okay or in fact well let's

152
00:04:59,800 --> 00:05:00,769
条件还不错，或者实际上很好，让我们先登录即可，我们可以转到
just log at first so we can go to

153
00:05:00,968 --> 00:05:02,809
可以采取行动，条件和行动都在这里，因此我们可以实际使用
actions okay and conditions and actions

154
00:05:03,009 --> 00:05:04,550
可以采取行动，条件和行动都在这里，因此我们可以实际使用
are both here so we can actually use a

155
00:05:04,750 --> 00:05:06,588
条件和动作一起，就必须先单击动作，然后
condition and an action together right

156
00:05:06,788 --> 00:05:08,509
条件和动作一起，就必须先单击动作，然后
you have to click on actions first and

157
00:05:08,709 --> 00:05:10,879
然后为其添加条件，否则我们必须实际添加一些内容
then add a condition to it otherwise it

158
00:05:11,079 --> 00:05:11,959
然后为其添加条件，否则我们必须实际添加一些内容
well we have to actually add something

159
00:05:12,158 --> 00:05:14,569
然后它会让我们有点同时做，但无论如何，让我们从一个
and then it will let us kind of um do

160
00:05:14,769 --> 00:05:16,309
然后它会让我们有点同时做，但无论如何，让我们从一个
both but anyway let's start with an

161
00:05:16,509 --> 00:05:18,439
操作，以便将某些内容记录到Apple窗口中，然后我们决定
action so we'll log something to the

162
00:05:18,639 --> 00:05:20,300
操作，以便将某些内容记录到Apple窗口中，然后我们决定
Apple window and we will decide to

163
00:05:20,500 --> 00:05:22,338
继续执行，以便在有所需变量的情况下执行此操作
continue execution so the way that we do

164
00:05:22,538 --> 00:05:23,718
继续执行，以便在有所需变量的情况下执行此操作
this if we have variables that we want

165
00:05:23,918 --> 00:05:25,399
做，这实际上向您展示了一些语法，我们可以做的就是使用
to do and this actually shows you a bit

166
00:05:25,598 --> 00:05:28,369
做，这实际上向您展示了一些语法，我们可以做的就是使用
at the syntax what we can do is just use

167
00:05:28,569 --> 00:05:29,809
大括号和大括号内，我们可以放入变量名
curly brackets and inside the curly

168
00:05:30,009 --> 00:05:31,519
大括号和大括号内，我们可以放入变量名
brackets we can put in a variable name

169
00:05:31,718 --> 00:05:34,579
例如X或输入管理器arrow mem下划线鼠标位置X或其他内容
such as X or input manager arrow mem

170
00:05:34,778 --> 00:05:36,649
例如X或输入管理器arrow mem下划线鼠标位置X或其他内容
underscore mouse position X or something

171
00:05:36,848 --> 00:05:38,149
像这样X显然很容易编写
like that X is just obviously easier to

172
00:05:38,348 --> 00:05:38,600
像这样X显然很容易编写
write

173
00:05:38,800 --> 00:05:40,939
因此我们将执行X，并且您可以执行诸如cast之类的操作，因此如果我们
so we'll do X and you can do things like

174
00:05:41,139 --> 00:05:42,829
因此我们将执行X，并且您可以执行诸如cast之类的操作，因此如果我们
cast so it will cost to a float if we

175
00:05:43,028 --> 00:05:44,749
希望我们不必在这种情况下要做的是将它变成整数Mouse
want we don't have to do in this case

176
00:05:44,949 --> 00:05:45,920
希望我们不必在这种情况下要做的是将它变成整数Mouse
it's just going to be an integer Mouse

177
00:05:46,120 --> 00:05:48,468
位置，所以我们会说值8正确，但我将其浮动
position so we'd say the value 8 away

178
00:05:48,668 --> 00:05:50,088
位置，所以我们会说值8正确，但我将其浮动
properly but I'll cause it to a float is

179
00:05:50,288 --> 00:05:52,850
很高兴向您展示，您可以按x和y表示，然后按X，然后用逗号Y，您可以
so fun to show you that you can so x and

180
00:05:53,050 --> 00:05:55,759
很高兴向您展示，您可以按x和y表示，然后按X，然后用逗号Y，您可以
y okay so X and then comma Y and you can

181
00:05:55,959 --> 00:05:57,709
看到我有点把这些变量名包含在大括号内，
see that I'm kind of encompassing these

182
00:05:57,908 --> 00:05:59,929
看到我有点把这些变量名包含在大括号内，
variable names inside curly brackets and

183
00:06:00,129 --> 00:06:01,910
我可以知道添加其他文本，例如鼠标的位置是可以的，
I can you know add some other text like

184
00:06:02,110 --> 00:06:07,999
我可以知道添加其他文本，例如鼠标的位置是可以的，
the mouse position is that okay and

185
00:06:08,199 --> 00:06:10,399
到这里，我们已经关闭，应用程序仍在运行
there we go we have that let's hit close

186
00:06:10,598 --> 00:06:11,809
到这里，我们已经关闭，应用程序仍在运行
and the application is still running

187
00:06:12,009 --> 00:06:13,218
显然，所以我要做的就是单击，如果我们进入
obviously so what I'm gonna do is I'm

188
00:06:13,418 --> 00:06:15,379
显然，所以我要做的就是单击，如果我们进入
just gonna click and if we go into our

189
00:06:15,579 --> 00:06:17,300
输出窗口，您会看到它实际上告诉我们鼠标的内容
output window you'll see that it

190
00:06:17,500 --> 00:06:18,619
输出窗口，您会看到它实际上告诉我们鼠标的内容
actually tells us what our mouse

191
00:06:18,819 --> 00:06:22,009
位置是，如果我再拖动一点，以便你们看到，我回去
position is and if I drag this up a bit

192
00:06:22,209 --> 00:06:23,959
位置是，如果我再拖动一点，以便你们看到，我回去
more so you guys can see and I go back

193
00:06:24,158 --> 00:06:26,088
到我的程序中，我只需单击所有位置，然后我就可以移动它了
to my program and I just click all over

194
00:06:26,288 --> 00:06:27,649
到我的程序中，我只需单击所有位置，然后我就可以移动它了
the place let me move this in fact I'll

195
00:06:27,848 --> 00:06:30,559
调整它的大小使其变小，您可以看到我在整个地方单击，并且
resize it make it smaller you can see

196
00:06:30,759 --> 00:06:32,300
调整它的大小使其变小，您可以看到我在整个地方单击，并且
I'm clicking all over the place and it's

197
00:06:32,500 --> 00:06:35,629
告诉我鼠标的位置还可以，整个盒子看起来像
telling me what my mouse position is

198
00:06:35,829 --> 00:06:37,129
告诉我鼠标的位置还可以，整个盒子看起来像
okay and looks like that whole box being

199
00:06:37,329 --> 00:06:38,270
反正因某种原因两次打电话你可以说我有数学
called twice for some reason

200
00:06:38,470 --> 00:06:41,540
反正因某种原因两次打电话你可以说我有数学
anyway you can say that I have the math

201
00:06:41,740 --> 00:06:42,860
在我的应用程序运行时实时打印位置
position being printed in real time

202
00:06:43,060 --> 00:06:44,480
在我的应用程序运行时实时打印位置
while my application is running and I

203
00:06:44,680 --> 00:06:45,560
猜想最大的收获显然是我可以添加代码
guess the big takeaway from this is

204
00:06:45,759 --> 00:06:47,300
猜想最大的收获显然是我可以添加代码
obviously I could just add code that

205
00:06:47,500 --> 00:06:48,860
在应用程序的实际源代码中打印出来，但是很酷的是我
prints that in the actual source code of

206
00:06:49,060 --> 00:06:51,259
在应用程序的实际源代码中打印出来，但是很酷的是我
the application but the cool thing is I

207
00:06:51,459 --> 00:06:52,520
还没有停止我的申请权，实际上我没有必要
haven't have stopped my application

208
00:06:52,720 --> 00:06:53,900
还没有停止我的申请权，实际上我没有必要
right I haven't actually had to

209
00:06:54,100 --> 00:06:55,670
重新编译任何代码或仅在我最初的应用程序是
recompile any code or anything this is

210
00:06:55,870 --> 00:06:57,319
重新编译任何代码或仅在我最初的应用程序是
just while my initial application was

211
00:06:57,519 --> 00:06:59,060
运行中，我设法以其他方式将其他数据打印到控制台
running I've managed to print additional

212
00:06:59,259 --> 00:07:01,939
运行中，我设法以其他方式将其他数据打印到控制台
data to the console the other way of

213
00:07:02,139 --> 00:07:03,290
这样做将只是添加常规断点单击，然后查看
doing this would be to just add a

214
00:07:03,490 --> 00:07:05,900
这样做将只是添加常规断点单击，然后查看
regular breakpoint click and then look

215
00:07:06,100 --> 00:07:07,939
当您知道损坏的程序时，它会暂停并查看变量
at you know the broken program like well

216
00:07:08,139 --> 00:07:09,290
当您知道损坏的程序时，它会暂停并查看变量
it's paused and look at the variables

217
00:07:09,490 --> 00:07:10,639
但这太长了吧，是的，我可以点击整个地方
but that would be way too long right

218
00:07:10,839 --> 00:07:12,500
但这太长了吧，是的，我可以点击整个地方
yeah I can just click all over the place

219
00:07:12,699 --> 00:07:14,360
从字面上看，您会看到我将立即获取所需的所有数据，因此
literally and you can see I'll just get

220
00:07:14,560 --> 00:07:16,340
从字面上看，您会看到我将立即获取所需的所有数据，因此
all the data I need immediately so it's

221
00:07:16,540 --> 00:07:19,850
现在非常强大，可以测试条件，让我们说我想要我的鼠标
really powerful now to test conditions

222
00:07:20,050 --> 00:07:21,579
现在非常强大，可以测试条件，让我们说我想要我的鼠标
let's just say that I want my mouse

223
00:07:21,779 --> 00:07:24,110
在7的条件下打印给他的位置，所以如果X大于
position to him to be printed under a 7

224
00:07:24,310 --> 00:07:25,879
在7的条件下打印给他的位置，所以如果X大于
condition so maybe if X is greater than

225
00:07:26,079 --> 00:07:27,470
500之类的东西，因为我知道它仅发生在屏幕的右侧
500 or something because I know it only

226
00:07:27,670 --> 00:07:29,300
500之类的东西，因为我知道它仅发生在屏幕的右侧
happens on the right side of the screen

227
00:07:29,500 --> 00:07:32,389
所以我将按照条件进行操作，您会看到我已经启用了条件和
so I'll go to my conditions and you can

228
00:07:32,589 --> 00:07:33,980
所以我将按照条件进行操作，您会看到我已经启用了条件和
see I've enabled both conditions and

229
00:07:34,180 --> 00:07:35,930
现在我要说的是X平方，而不是500平方好，就是5,000 X
actions here now I'll just say X square

230
00:07:36,129 --> 00:07:39,439
现在我要说的是X平方，而不是500平方好，就是5,000 X
than 500 okay that's 5,000 X greater

231
00:07:39,639 --> 00:07:42,439
大于500好的条件表达式可以是任何布尔语句
than 500 okay just a conditional

232
00:07:42,639 --> 00:07:44,439
大于500好的条件表达式可以是任何布尔语句
expression can be any boolean statement

233
00:07:44,639 --> 00:07:48,319
然后我将其关闭，但将其关闭或将其关闭
and then I'll just close that but

234
00:07:48,519 --> 00:07:50,780
然后我将其关闭，但将其关闭或将其关闭
hitting close or just by closing it

235
00:07:50,980 --> 00:07:53,060
在那里，如果我返回，您将看到如果我单击左侧屏幕
there and if I go back you'll see that

236
00:07:53,259 --> 00:07:54,500
在那里，如果我返回，您将看到如果我单击左侧屏幕
if I click on the left side screen

237
00:07:54,699 --> 00:07:56,300
什么都没有打印出来，但是当我单击屏幕右侧时
nothing gets printed but it soon as I

238
00:07:56,500 --> 00:07:57,439
什么都没有打印出来，但是当我单击屏幕右侧时
click on the right side of screen it

239
00:07:57,639 --> 00:08:00,079
可以打印我的值，所以这是我们仅在以下条件下评估断点的一种方式
prints my value okay so that's a way for

240
00:08:00,279 --> 00:08:02,569
可以打印我的值，所以这是我们仅在以下条件下评估断点的一种方式
us to only evaluate breakpoints under a

241
00:08:02,769 --> 00:08:04,520
一定的条件，当然，如果我回到这里去行动
certain condition and of course if I

242
00:08:04,720 --> 00:08:06,410
一定的条件，当然，如果我回到这里去行动
come back over here and go to actions

243
00:08:06,610 --> 00:08:09,199
再次，我可以取消选中继续执行，这意味着当我实际执行
again I can uncheck continue execution

244
00:08:09,399 --> 00:08:11,270
再次，我可以取消选中继续执行，这意味着当我实际执行
which means that when I actually do

245
00:08:11,470 --> 00:08:13,040
单击左侧的尺寸框，屏幕右侧没有任何反应
click on the left size frame nothing

246
00:08:13,240 --> 00:08:14,090
单击左侧的尺寸框，屏幕右侧没有任何反应
happens on the right side of screen

247
00:08:14,290 --> 00:08:17,150
它会显示我的值，然后您还可以看到我的程序现在已暂停，因此
it'll print my value and then also you

248
00:08:17,350 --> 00:08:19,490
它会显示我的值，然后您还可以看到我的程序现在已暂停，因此
can see my program is now paused so that

249
00:08:19,689 --> 00:08:21,590
如果我真的想好的话，我可以看看这里需要的所有值，这就是
I can look at all the values I need over

250
00:08:21,790 --> 00:08:24,139
如果我真的想好的话，我可以看看这里需要的所有值，这就是
here if I really want to okay so that's

251
00:08:24,339 --> 00:08:26,449
这就是行动的条件，当我需要
it that's conditions of actions I use

252
00:08:26,649 --> 00:08:27,889
这就是行动的条件，当我需要
these all the time when I need to

253
00:08:28,089 --> 00:08:29,210
因为它们真的很强大，真的很不错
because it's they're just really

254
00:08:29,410 --> 00:08:30,740
因为它们真的很强大，真的很不错
powerful and they're just really good

255
00:08:30,939 --> 00:08:32,240
为了再次加快工作流程，您无法通过其他方式做任何事情，但是
for speeding up your workflow again

256
00:08:32,440 --> 00:08:35,179
为了再次加快工作流程，您无法通过其他方式做任何事情，但是
nothing you can't do via other means but

257
00:08:35,379 --> 00:08:36,559
只是当您处在复杂的事物之中时，尤其是当您处于
it's just if you're in the middle of

258
00:08:36,759 --> 00:08:38,059
只是当您处在复杂的事物之中时，尤其是当您处于
something complex especially if you're

259
00:08:38,259 --> 00:08:40,009
在大型应用程序上工作，您会知道将其全部关闭重新编译
working on a large application you know

260
00:08:40,210 --> 00:08:41,990
在大型应用程序上工作，您会知道将其全部关闭重新编译
that shutting it all down recompiling

261
00:08:42,190 --> 00:08:43,429
kuroda的回归将永远持续下去，甚至可能无法吸引您
the kuroda going back it's gonna take

262
00:08:43,629 --> 00:08:45,769
kuroda的回归将永远持续下去，甚至可能无法吸引您
forever and it may not even get you to

263
00:08:45,970 --> 00:08:47,689
您可能会遇到该错误的地方
the point where you have that bug in

264
00:08:47,889 --> 00:08:49,059
您可能会遇到该错误的地方
likely you

265
00:08:49,259 --> 00:08:51,039
该程序处于该错误的可复制状态，所以这是
that program in that reproducible state

266
00:08:51,240 --> 00:08:53,199
该程序处于该错误的可复制状态，所以这是
for the bug so this is something that is

267
00:08:53,399 --> 00:08:54,729
非常有用，每个人都应该使用一件事，我会说的是
incredibly useful and everyone should be

268
00:08:54,929 --> 00:08:58,269
非常有用，每个人都应该使用一件事，我会说的是
using one thing I will say is the

269
00:08:58,470 --> 00:09:00,159
如果有更新，条件对于诸如循环之类的事情非常有用
conditions are very very useful for

270
00:09:00,360 --> 00:09:02,589
如果有更新，条件对于诸如循环之类的事情非常有用
things like loops if you have an update

271
00:09:02,789 --> 00:09:05,259
像这样是正确的循环游戏，所以我有一个正在运行60的更新循环
loop right like this is a game right so

272
00:09:05,460 --> 00:09:07,089
像这样是正确的循环游戏，所以我有一个正在运行60的更新循环
I have an update loop that's running 60

273
00:09:07,289 --> 00:09:08,859
每秒的次数，而我想缓存几乎
times per second and I want to cache a

274
00:09:09,059 --> 00:09:10,959
每秒的次数，而我想缓存几乎
particular condition that's almost

275
00:09:11,159 --> 00:09:12,909
不可能与断点有关，例如我只是在遍历
impossible to do with breakpoints like

276
00:09:13,110 --> 00:09:15,099
不可能与断点有关，例如我只是在遍历
maybe I'm just kind of traversing this

277
00:09:15,299 --> 00:09:16,959
我游戏中所有实体的整个节点层次结构，我有5000个实体
whole node hierarchy of all my entities

278
00:09:17,159 --> 00:09:19,569
我游戏中所有实体的整个节点层次结构，我有5000个实体
in my game and I have like 5000 entities

279
00:09:19,769 --> 00:09:21,819
我只想打破一个叫做玩家或那个
and I just want to break on the one

280
00:09:22,019 --> 00:09:23,589
我只想打破一个叫做玩家或那个
that's called player or the one that's

281
00:09:23,789 --> 00:09:27,009
叫敌人405对，因为那是一个问题
called enemy 405 right because that's

282
00:09:27,210 --> 00:09:28,719
叫敌人405对，因为那是一个问题
the problem one well that's going to be

283
00:09:28,919 --> 00:09:30,490
手动操作如此困难，您将放置一个断点，并持续按f5直到
so difficult to do manually you'll put a

284
00:09:30,690 --> 00:09:33,519
手动操作如此困难，您将放置一个断点，并持续按f5直到
breakpoint you'll keep hitting f5 until

285
00:09:33,720 --> 00:09:35,439
您到达了实体名称等于您知道敌人405的地步
you get to the point where the name of

286
00:09:35,639 --> 00:09:38,319
您到达了实体名称等于您知道敌人405的地步
the entity equals you know enemy 405

287
00:09:38,519 --> 00:09:40,029
是的，但是如果您放下一个条件，说我只会激活它
right but if you put a condition down

288
00:09:40,230 --> 00:09:41,740
是的，但是如果您放下一个条件，说我只会激活它
which says I'd only activate this

289
00:09:41,940 --> 00:09:44,409
断点（如果名字是敌人）405猜猜你刚刚做了什么
breakpoint if name is enemy 405 well

290
00:09:44,610 --> 00:09:45,729
断点（如果名字是敌人）405猜猜你刚刚做了什么
guess what you've just done that

291
00:09:45,929 --> 00:09:48,159
瞬间没有任何工作，我会说，这会使您的程序变慢
instantly with absolutely no work I will

292
00:09:48,360 --> 00:09:49,899
瞬间没有任何工作，我会说，这会使您的程序变慢
say that this slows down your program a

293
00:09:50,100 --> 00:09:51,789
因此，如果您继续放下条件断点和实际
lot so if you keep putting down

294
00:09:51,990 --> 00:09:53,229
因此，如果您继续放下条件断点和实际
conditional breakpoints and actual

295
00:09:53,429 --> 00:09:55,149
断点之类的东西，就像在实时应用程序中一样，
breakpoints and all this like in a

296
00:09:55,350 --> 00:09:56,679
断点之类的东西，就像在实时应用程序中一样，
real-time application you'll probably

297
00:09:56,879 --> 00:09:58,569
下降到5 fps或类似的速度会滞后，这就是他们
drop to like 5 fps or something like

298
00:09:58,769 --> 00:10:00,219
下降到5 fps或类似的速度会滞后，这就是他们
that it will lag that's just how they

299
00:10:00,419 --> 00:10:02,409
工作很慢，但是显然有调试工具，所以通常
work there's slow but obviously there

300
00:10:02,610 --> 00:10:03,879
工作很慢，但是显然有调试工具，所以通常
are debugging tools so usually that's

301
00:10:04,080 --> 00:10:05,469
如果您担心性能，不会对您造成太大影响，那么
not going to affect you too much if you

302
00:10:05,669 --> 00:10:06,969
如果您担心性能，不会对您造成太大影响，那么
are worried about performance then just

303
00:10:07,169 --> 00:10:08,769
停止您的应用程序，并使用一些为您处理的代码重新编译它
stop your application recompile it with

304
00:10:08,970 --> 00:10:10,599
停止您的应用程序，并使用一些为您处理的代码重新编译它
some code which handles us for you

305
00:10:10,799 --> 00:10:12,250
而不是使用调试器，因为附加了调试器并
instead of using the debugger for that

306
00:10:12,450 --> 00:10:13,779
而不是使用调试器，因为附加了调试器并
because having the debugger attached and

307
00:10:13,980 --> 00:10:15,939
无论如何，这样做还是很慢的，我希望你们喜欢这个视频
doing this is quite slow anyway I hope

308
00:10:16,139 --> 00:10:17,259
无论如何，这样做还是很慢的，我希望你们喜欢这个视频
you guys enjoyed this video if you did

309
00:10:17,460 --> 00:10:19,000
您可以点击“赞”按钮，您可以在Twitter和Instagram上关注我
you can hit the like button you can

310
00:10:19,200 --> 00:10:20,559
您可以点击“赞”按钮，您可以在Twitter和Instagram上关注我
follow me on Twitter and Instagram as

311
00:10:20,759 --> 00:10:22,599
很好地获取有关我所有视频和类似内容的更新
well to get updates about all my videos

312
00:10:22,799 --> 00:10:23,379
很好地获取有关我所有视频和类似内容的更新
and stuff like that

313
00:10:23,580 --> 00:10:25,149
詹纳（Jenner）之后的父权制课程是支持本系列和本系列的最佳方法
patriarchal course after Jenner is the

314
00:10:25,350 --> 00:10:26,769
詹纳（Jenner）之后的父权制课程是支持本系列和本系列的最佳方法
best way to support this series and this

315
00:10:26,970 --> 00:10:28,479
YouTube频道，非常感谢所有制作此系列影片的顾客
YouTube channel so thank you so much to

316
00:10:28,679 --> 00:10:30,399
YouTube频道，非常感谢所有制作此系列影片的顾客
all of my patrons which make this series

317
00:10:30,600 --> 00:10:32,289
在下一个视频中我们可能会谈论更多的视觉效果
possible in the next video we're going

318
00:10:32,490 --> 00:10:33,549
在下一个视频中我们可能会谈论更多的视觉效果
to be talking about some more visual

319
00:10:33,750 --> 00:10:35,799
我想介绍一下工作室调试功能，因为我们会
studio debugging features that I want to

320
00:10:36,000 --> 00:10:37,329
我想介绍一下工作室调试功能，因为我们会
kind of introduce because we're gonna be

321
00:10:37,529 --> 00:10:40,209
即将开始其他系列的比赛，规模会很大，我希望所有人
starting some other series soon it's

322
00:10:40,409 --> 00:10:41,649
即将开始其他系列的比赛，规模会很大，我希望所有人
gonna be pretty big and I want everyone

323
00:10:41,850 --> 00:10:43,029
对Visual Studio感到有点自在，
to be kind of comfortable with Visual

324
00:10:43,230 --> 00:10:44,439
对Visual Studio感到有点自在，
Studio and what's it all spots to the

325
00:10:44,639 --> 00:10:46,299
我们可以有效调试代码的地方，谢谢大家的关注
point where we can debug our code

326
00:10:46,500 --> 00:10:48,219
我们可以有效调试代码的地方，谢谢大家的关注
effectively thank you guys for watching

327
00:10:48,419 --> 00:10:50,699
下次见。[音乐]
I'll see you next time goodbye

328
00:10:50,899 --> 00:10:55,899
下次见。[音乐]
[Music]

