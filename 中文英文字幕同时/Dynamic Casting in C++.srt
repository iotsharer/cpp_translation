﻿1
00:00:00,030 --> 00:00:01,509
嘿，大家好，我叫Archana，欢迎回到另一个C ++视频
hey what's up guys my name is Archana

2
00:00:01,709 --> 00:00:04,000
嘿，大家好，我叫Archana，欢迎回到另一个C ++视频
welcome back to another C++ video it's

3
00:00:04,200 --> 00:00:05,799
自上一个版本以来已经有很长一段时间了
been ages since the last one gonna get

4
00:00:06,000 --> 00:00:08,380
自上一个版本以来已经有很长一段时间了
back into it now though dynamic casting

5
00:00:08,580 --> 00:00:09,430
这就是今天我们要谈论的，你们不知道是什么演员
that's what we're gonna be talking about

6
00:00:09,630 --> 00:00:11,530
这就是今天我们要谈论的，你们不知道是什么演员
today you guys don't know what casting

7
00:00:11,730 --> 00:00:13,870
通常在C ++中，我有一个有关此的视频，所以一定要检查一下
is in general in C++ I've got a video

8
00:00:14,070 --> 00:00:15,849
通常在C ++中，我有一个有关此的视频，所以一定要检查一下
about that so definitely check that out

9
00:00:16,048 --> 00:00:17,289
并在开始观看此视频之前先熟悉一下
and familiarize yourself with that

10
00:00:17,489 --> 00:00:20,440
并在开始观看此视频之前先熟悉一下
before you move on to this video if

11
00:00:20,640 --> 00:00:21,999
您通常不熟悉类型系统，我有一些视频
you're not familiar with the type system

12
00:00:22,199 --> 00:00:23,620
您通常不熟悉类型系统，我有一些视频
in general I have a few videos about

13
00:00:23,820 --> 00:00:25,690
同样，因为类型转换与类型转换有关，这也是我们的一种方式
that as well because casting is to do

14
00:00:25,890 --> 00:00:27,789
同样，因为类型转换与类型转换有关，这也是我们的一种方式
with types casting is a way for us to

15
00:00:27,989 --> 00:00:30,310
在我们在C ++中使用的类型之间进行转换，因此您需要确保自己
convert between the types that we use in

16
00:00:30,510 --> 00:00:32,198
在我们在C ++中使用的类型之间进行转换，因此您需要确保自己
C++ so you need to make sure that you

17
00:00:32,399 --> 00:00:34,089
了解类型系统是如何工作的，等等。
kind of understand how the type system

18
00:00:34,289 --> 00:00:37,538
了解类型系统是如何工作的，等等。
works and more so maybe the point that

19
00:00:37,738 --> 00:00:39,459
类型系统不是在C ++中特别强加的东西，而是
the type system isn't something that is

20
00:00:39,659 --> 00:00:41,739
类型系统不是在C ++中特别强加的东西，而是
particularly enforced in C++ it's

21
00:00:41,939 --> 00:00:43,628
销售类提供给我们的一种保护代码的方式，但这是
something that sales class kind of gives

22
00:00:43,829 --> 00:00:46,479
销售类提供给我们的一种保护代码的方式，但这是
us as a way to protect our code but it's

23
00:00:46,679 --> 00:00:47,500
不是我们必须坚持或使用的东西，因为我们
not something that we have to

24
00:00:47,700 --> 00:00:49,358
不是我们必须坚持或使用的东西，因为我们
necessarily stick to or use because we

25
00:00:49,558 --> 00:00:52,178
可以在任何类型的类型之间自由转换，例如如果我们选择但动态的
can freely cast between types of any

26
00:00:52,378 --> 00:00:54,669
可以在任何类型的类型之间自由转换，例如如果我们选择但动态的
types like if we so choose but dynamic

27
00:00:54,869 --> 00:00:57,399
铸件提供给我们的就像是真正的安全网
casting is provided to us as some

28
00:00:57,600 --> 00:01:00,428
铸件提供给我们的就像是真正的安全网
somewhat of like a safety net really for

29
00:01:00,628 --> 00:01:02,079
当我们想进行特定类型的铸造时，我经历了
when we want to do a specific type of

30
00:01:02,280 --> 00:01:04,028
当我们想进行特定类型的铸造时，我经历了
casting now I've gone through kind of

31
00:01:04,228 --> 00:01:06,340
C ++样式转换有两种方式进行转换，c样式转换是
C++ style casts there's two kind of ways

32
00:01:06,540 --> 00:01:08,829
C ++样式转换有两种方式进行转换，c样式转换是
to cast there's a c-style cast which is

33
00:01:09,030 --> 00:01:10,569
只是当您将新类型写在变量之前的方括号中，然后
just when you write your new type in

34
00:01:10,769 --> 00:01:12,969
只是当您将新类型写在变量之前的方括号中，然后
brackets before a variable but then

35
00:01:13,170 --> 00:01:14,980
还有最简单的我会像静态演员一样的康斯特演员
there's also the simplest I'll cast like

36
00:01:15,180 --> 00:01:16,659
还有最简单的我会像静态演员一样的康斯特演员
static cast Const casts I'll make

37
00:01:16,859 --> 00:01:18,969
可能有更多关于此的视频，但动态转换是C ++样式的转换，
probably more videos about that but

38
00:01:19,170 --> 00:01:22,119
可能有更多关于此的视频，但动态转换是C ++样式的转换，
dynamic cast is a C++ style cast that

39
00:01:22,319 --> 00:01:24,879
仅适用于C ++，不适用于C，并且不需要额外的工作来确保
only works in C++ doesn't work in C and

40
00:01:25,079 --> 00:01:26,799
仅适用于C ++，不适用于C，并且不需要额外的工作来确保
it doesn't extra work to make sure that

41
00:01:27,000 --> 00:01:29,079
就像我们实际施放正确的方式一样，这是一个，它实际上是一个
well like the way that we actually cast

42
00:01:29,280 --> 00:01:31,869
就像我们实际施放正确的方式一样，这是一个，它实际上是一个
right it's some it's a it's actually a

43
00:01:32,069 --> 00:01:33,849
有效的转换，我们不只是在两种类型之间转换，无论发生什么
valid cast we don't just cast between

44
00:01:34,049 --> 00:01:35,918
有效的转换，我们不只是在两种类型之间转换，无论发生什么
two types and and whatever happens

45
00:01:36,118 --> 00:01:37,719
事实并非如此，它实际上是在为我们做一些验证以确保
happens it's not like that it actually

46
00:01:37,920 --> 00:01:39,668
事实并非如此，它实际上是在为我们做一些验证以确保
does some validation for us to ensure

47
00:01:39,868 --> 00:01:43,689
现在无论您是否使用动态投射，投射都有效
that cast is valid now whether or not

48
00:01:43,890 --> 00:01:46,299
现在无论您是否使用动态投射，投射都有效
you use dynamic cast is again totally up

49
00:01:46,500 --> 00:01:47,859
给你，我想给你一些要点，我想教育你
to you I want to give you some points to

50
00:01:48,060 --> 00:01:51,039
给你，我想给你一些要点，我想教育你
kind of I guess educate you to a sense

51
00:01:51,239 --> 00:01:53,230
您是否想使用它，如果那是您想做的事情
of whether or not you should be using

52
00:01:53,430 --> 00:01:54,549
您是否想使用它，如果那是您想做的事情
that if that's something you want to do

53
00:01:54,750 --> 00:01:58,480
但更重要的是，动态转换是一种更像是
but more so a dynamic cast is is quite a

54
00:01:58,680 --> 00:02:00,099
但更重要的是，动态转换是一种更像是
it's it's a tool that's more like a

55
00:02:00,299 --> 00:02:01,840
功能，重要的是要意识到它不仅像完成的转换一样
function it's important to realize that

56
00:02:02,040 --> 00:02:04,480
功能，重要的是要意识到它不仅像完成的转换一样
it's not just like a cast that is done

57
00:02:04,680 --> 00:02:08,050
在像编译时一样在运行时进行评估，因此它确实具有
at like compile time it's evaluated at

58
00:02:08,250 --> 00:02:09,880
在像编译时一样在运行时进行评估，因此它确实具有
runtime and because of that it does have

59
00:02:10,080 --> 00:02:11,980
相关的运行时间成本，我什至可以在
an Associated runtime cost I might even

60
00:02:12,180 --> 00:02:13,270
相关的运行时间成本，我什至可以在
make a video in the

61
00:02:13,469 --> 00:02:15,700
将来比较这种正常类型的静态转换与
future comparing the performance of this

62
00:02:15,900 --> 00:02:17,439
将来比较这种正常类型的静态转换与
normal kind of static cast versus that

63
00:02:17,639 --> 00:02:19,900
动态转换的原因当然是因为动态转换会做额外的工作
dynamic cast because of course since the

64
00:02:20,099 --> 00:02:22,540
动态转换的原因当然是因为动态转换会做额外的工作
dynamic cast does extra work it does

65
00:02:22,740 --> 00:02:24,160
一种性能成本较低，这很正常，但动态
kind of come with a small performance

66
00:02:24,360 --> 00:02:27,310
一种性能成本较低，这很正常，但动态
cost and that's normal but what dynamic

67
00:02:27,509 --> 00:02:30,670
强制转换确实是专门用于沿继承层次结构进行强制转换
cast does is specifically it's used for

68
00:02:30,870 --> 00:02:32,500
强制转换确实是专门用于沿继承层次结构进行强制转换
casts along the inheritance hierarchy

69
00:02:32,699 --> 00:02:34,300
现在，如果您不知道公民拼图中的继承是什么，
now if you don't know what inheritance

70
00:02:34,500 --> 00:02:36,550
现在，如果您不知道公民拼图中的继承是什么，
is in civil puzzle what classes are in

71
00:02:36,750 --> 00:02:38,950
C ++查看我关于继承，类和所有东西的视频
C++ check out my video on inheritance

72
00:02:39,150 --> 00:02:40,240
C ++查看我关于继承，类和所有东西的视频
and on classes and all that stuff

73
00:02:40,439 --> 00:02:42,760
因为那会帮助你，但实际上这意味着如果我有
because that will help you out but

74
00:02:42,960 --> 00:02:44,350
因为那会帮助你，但实际上这意味着如果我有
essentially what that means is if I have

75
00:02:44,550 --> 00:02:46,390
一个是另一个类的子类的类，我想强制转换为
a class that is a subclass of another

76
00:02:46,590 --> 00:02:48,460
一个是另一个类的子类的类，我想强制转换为
class and I want to either cast to the

77
00:02:48,659 --> 00:02:50,620
基本类型或从基本类型到派生类型的位置
base type or from the base type to a

78
00:02:50,819 --> 00:02:53,050
基本类型或从基本类型到派生类型的位置
derived type that's where I can actually

79
00:02:53,250 --> 00:02:55,540
使用动态转换权限，所以我们只说我们有一个实体，我们有一个空的
use a dynamic cast right so let's just

80
00:02:55,740 --> 00:02:57,580
使用动态转换权限，所以我们只说我们有一个实体，我们有一个空的
say we have an entity we have an empty

81
00:02:57,780 --> 00:02:59,020
类，类似于游戏中所有实体种类的类，
class which is like the class for kind

82
00:02:59,219 --> 00:03:01,210
类，类似于游戏中所有实体种类的类，
of all of our entities in our game and

83
00:03:01,409 --> 00:03:02,890
那么我们就像一个玩家阶级，也许像一个敌人阶级，
then we have like a player class and

84
00:03:03,090 --> 00:03:04,270
那么我们就像一个玩家阶级，也许像一个敌人阶级，
maybe like an enemy class and they're

85
00:03:04,469 --> 00:03:06,250
这两个实体，所以如果我只想从实体类派生它们
both entities so they derive from the

86
00:03:06,449 --> 00:03:09,610
这两个实体，所以如果我只想从实体类派生它们
entity class right if I just want to

87
00:03:09,810 --> 00:03:12,340
将玩家对象转换回实体对象，这很容易，我是说玩家
cast a player object back to an entity

88
00:03:12,539 --> 00:03:14,620
将玩家对象转换回实体对象，这很容易，我是说玩家
object that that's easy I mean a player

89
00:03:14,819 --> 00:03:17,289
已经具有类型实体，因此可以隐式完成，无需强制转换
already has the type entity so that that

90
00:03:17,489 --> 00:03:19,120
已经具有类型实体，因此可以隐式完成，无需强制转换
can be done implicitly no casting is

91
00:03:19,319 --> 00:03:21,160
必要的，尽管如果我们愿意，我们仍然可以使用动态强制转换
necessary although if we wanted to we

92
00:03:21,360 --> 00:03:23,020
必要的，尽管如果我们愿意，我们仍然可以使用动态强制转换
could still use dynamic casts to do it

93
00:03:23,219 --> 00:03:24,009
现在另一种方法是解决问题
now

94
00:03:24,209 --> 00:03:25,900
现在另一种方法是解决问题
the other way around is where things get

95
00:03:26,099 --> 00:03:27,640
稍微复杂一点，换句话说，如果我们有一个
a little bit more complicated so by the

96
00:03:27,840 --> 00:03:29,439
稍微复杂一点，换句话说，如果我们有一个
other way around I mean if we have an

97
00:03:29,639 --> 00:03:32,200
实体实例，我们想将其投射到我们无法得知的玩家
entity instance and we want to cast it

98
00:03:32,400 --> 00:03:34,930
实体实例，我们想将其投射到我们无法得知的玩家
to a player we have no way of knowing

99
00:03:35,129 --> 00:03:37,030
是否是玩家对，据我们所知它是一个实体指针
whether or it's a player right it's as

100
00:03:37,229 --> 00:03:39,340
是否是玩家对，据我们所知它是一个实体指针
far as we know it is an entity pointer

101
00:03:39,539 --> 00:03:41,230
对，它是一个实体实例，对，是一个实体对象的实例，所以
right it's an entity instance is an

102
00:03:41,430 --> 00:03:44,050
对，它是一个实体实例，对，是一个实体对象的实例，所以
instance of an entity object right so

103
00:03:44,250 --> 00:03:46,719
什么是实体对象，它可能是实体类型，可能只是一个
what is an entity object well it could

104
00:03:46,919 --> 00:03:48,580
什么是实体对象，它可能是实体类型，可能只是一个
be of type entity could just be an

105
00:03:48,780 --> 00:03:50,710
实体既不是玩家权也不是敌人权，只能是MC，但是
entity could be neither player right or

106
00:03:50,909 --> 00:03:53,800
实体既不是玩家权也不是敌人权，只能是MC，但是
enemy right could just be an MC but it

107
00:03:54,000 --> 00:03:57,039
也可能是玩家，或者可能是完全合法的敌人，我们可能拥有
also could be a player or it could be an

108
00:03:57,239 --> 00:03:59,259
也可能是玩家，或者可能是完全合法的敌人，我们可能拥有
enemy that's totally valid we might have

109
00:03:59,459 --> 00:04:00,700
使其成为玩家，我们可能使其成为敌人，也可能成为小偷
made it a player we might have made it

110
00:04:00,900 --> 00:04:02,500
使其成为玩家，我们可能使其成为敌人，也可能成为小偷
an enemy or it could be a thief it could

111
00:04:02,699 --> 00:04:04,569
如果我们要说实际上我要花这两种，请成为三种类型之一
be one of three types if we want to say

112
00:04:04,769 --> 00:04:06,340
如果我们要说实际上我要花这两种，请成为三种类型之一
that actually I'm gonna cost this two

113
00:04:06,539 --> 00:04:09,400
播放器，我们不知道它是否是播放器
player we don't know if it is a player

114
00:04:09,599 --> 00:04:11,200
播放器，我们不知道它是否是播放器
or not the compiler has to either

115
00:04:11,400 --> 00:04:13,660
相信我们，如果我们错了，那就说这是一个敌人，然后我们尝试
believe us and if we're wrong let's just

116
00:04:13,860 --> 00:04:14,950
相信我们，如果我们错了，那就说这是一个敌人，然后我们尝试
say it was an enemy and we try and

117
00:04:15,150 --> 00:04:17,050
访问播放器独有的数据或修改对此疑问的功能
access data that is unique to player or

118
00:04:17,250 --> 00:04:18,819
访问播放器独有的数据或修改对此疑问的功能
a function that modifies doubt of this

119
00:04:19,019 --> 00:04:20,379
独特的两个播放器，我们的程序可能会崩溃，就像
unique two player our program will

120
00:04:20,579 --> 00:04:21,639
独特的两个播放器，我们的程序可能会崩溃，就像
probably crash and it'll be like a

121
00:04:21,839 --> 00:04:25,480
灾难，因此由于实际使用了动态成本
disaster so because of that dynamic cost

122
00:04:25,680 --> 00:04:27,189
灾难，因此由于实际使用了动态成本
is actually used

123
00:04:27,389 --> 00:04:29,410
验证这一点，如果我们有一个实体实例
to validate that so what happens is if

124
00:04:29,610 --> 00:04:32,410
验证这一点，如果我们有一个实体实例
we have an entity instance that is

125
00:04:32,610 --> 00:04:35,290
实际上是敌人的权利，但我们尝试使用以下方法将其投射给玩家
actually an enemy right but we cast it

126
00:04:35,490 --> 00:04:36,910
实际上是敌人的权利，但我们尝试使用以下方法将其投射给玩家
we try and cast it to a player using

127
00:04:37,110 --> 00:04:39,369
动态转换会失败，并且动态转换将返回null
dynamic cast that cast will fail and

128
00:04:39,569 --> 00:04:41,410
动态转换会失败，并且动态转换将返回null
that dynamic cast will return a null

129
00:04:41,610 --> 00:04:43,600
指针将只返回零，然后我们可以验证该结果或执行任何操作
pointer will just return zero and then

130
00:04:43,800 --> 00:04:45,129
指针将只返回零，然后我们可以验证该结果或执行任何操作
we can validate that or do whatever we

131
00:04:45,329 --> 00:04:46,389
想要这样，基本上我们可以用它来检查对象是否给定
want so basically we could use it to

132
00:04:46,589 --> 00:04:48,910
想要这样，基本上我们可以用它来检查对象是否给定
check to see if an object is a given

133
00:04:49,110 --> 00:04:51,369
输入，换句话说，我可以尝试对该实体对象执行动态投射
type so in other words I can try and do

134
00:04:51,569 --> 00:04:53,379
输入，换句话说，我可以尝试对该实体对象执行动态投射
a dynamic cast on that entity object to

135
00:04:53,579 --> 00:04:55,269
将其转换为播放器，然后检查返回是否不返回
convert it to player and then check to

136
00:04:55,468 --> 00:04:56,709
将其转换为播放器，然后检查返回是否不返回
see if that returns not with it returns

137
00:04:56,908 --> 00:04:58,629
否，这不是玩家，这是很多东西，更容易
null that it's not a player right this

138
00:04:58,829 --> 00:05:00,189
否，这不是玩家，这是很多东西，更容易
is a lot of stuff is more easily

139
00:05:00,389 --> 00:05:01,269
解释鼓励，所以让我们来看看基本的设置
explained encouraged so let's just dive

140
00:05:01,468 --> 00:05:03,639
解释鼓励，所以让我们来看看基本的设置
in take a look at a basic kind of setup

141
00:05:03,839 --> 00:05:05,800
其中可能有用，以及如何实际使用动态转换
in which this might be useful and how we

142
00:05:06,000 --> 00:05:08,468
其中可能有用，以及如何实际使用动态转换
could actually use dynamic casting okay

143
00:05:08,668 --> 00:05:10,269
那么这里的空白C ++项目是什么，因为我真的很想展示一下
so what does a blank C++ project here

144
00:05:10,468 --> 00:05:12,278
那么这里的空白C ++项目是什么，因为我真的很想展示一下
because I really wanted to kind of show

145
00:05:12,478 --> 00:05:13,959
你们一切，所以如果我们做我刚才描述的事情，如果我们像一个实体
you guys everything so if we do what I

146
00:05:14,158 --> 00:05:15,910
你们一切，所以如果我们做我刚才描述的事情，如果我们像一个实体
just described if we have like an entity

147
00:05:16,110 --> 00:05:19,059
对，如果我们有一个玩家，它是一个MC，所以是一个公共实体，那么我们还将
right if we have a player which is an MC

148
00:05:19,259 --> 00:05:23,499
对，如果我们有一个玩家，它是一个MC，所以是一个公共实体，那么我们还将
so a public entity and then we'll also

149
00:05:23,699 --> 00:05:26,139
有我谈论过的那个敌方阶级也将成为苏
have that enemy class that I talked

150
00:05:26,338 --> 00:05:27,879
有我谈论过的那个敌方阶级也将成为苏
about which is also going to be an su

151
00:05:28,079 --> 00:05:29,379
现在就这些数据而言
right now

152
00:05:29,579 --> 00:05:30,879
现在就这些数据而言
in terms of the data that these will

153
00:05:31,079 --> 00:05:35,139
如果甚至不需要我们实际添加任何数据，那么我可能
have if it's not even required for us to

154
00:05:35,338 --> 00:05:37,300
如果甚至不需要我们实际添加任何数据，那么我可能
actually add any data so I probably

155
00:05:37,500 --> 00:05:39,449
我只是想保持简短，我想因为这真的不值得
wanted just to keep this brief I think

156
00:05:39,649 --> 00:05:41,739
我只是想保持简短，我想因为这真的不值得
because it's not really worth going into

157
00:05:41,939 --> 00:05:43,569
这些东西，但基本上的想法是我们有三种不同的类型
that stuff but basically the idea is we

158
00:05:43,769 --> 00:05:45,040
这些东西，但基本上的想法是我们有三种不同的类型
have three different types that they're

159
00:05:45,240 --> 00:05:46,449
可能填充了功能，这些功能填充了其唯一的数据
probably filled with functions filled

160
00:05:46,649 --> 00:05:47,980
可能填充了功能，这些功能填充了其唯一的数据
with data that is unique to their

161
00:05:48,180 --> 00:05:50,019
具体类型我如何使一个实体好
specific types how do I make an entity

162
00:05:50,218 --> 00:05:51,129
具体类型我如何使一个实体好
well let's just say I want to make a

163
00:05:51,329 --> 00:05:53,319
我可以输入此视频的播放器现在等于此视频的新播放器
player I can just type in player player

164
00:05:53,519 --> 00:05:55,329
我可以输入此视频的播放器现在等于此视频的新播放器
equals new player now for this video

165
00:05:55,528 --> 00:05:57,069
我们将使用原始指针，将来我可能会制作视频
we're just gonna be using raw pointers I

166
00:05:57,269 --> 00:05:58,269
我们将使用原始指针，将来我可能会制作视频
might make a video in the future about

167
00:05:58,468 --> 00:06:00,759
使用智能指针进行动态投射，但此视频无法涵盖
dynamic casting with smart pointers but

168
00:06:00,959 --> 00:06:02,079
使用智能指针进行动态投射，但此视频无法涵盖
this video just can't cover kind of the

169
00:06:02,278 --> 00:06:04,449
与原始指针一起使用的原始动态类型转换，因此我们有一个播放器
original dynamic cast that is used with

170
00:06:04,649 --> 00:06:06,879
与原始指针一起使用的原始动态类型转换，因此我们有一个播放器
the raw pointers so we have a player

171
00:06:07,079 --> 00:06:09,429
现在的事情是，这个玩家已经拥有两种类型，并且拥有两个实体
right now the thing is this player

172
00:06:09,629 --> 00:06:12,009
现在的事情是，这个玩家已经拥有两种类型，并且拥有两个实体
already has two types it has both entity

173
00:06:12,209 --> 00:06:14,439
和与之相关的播放器实际上，它甚至像类型列表一样
and player associated with it it's

174
00:06:14,639 --> 00:06:16,119
和与之相关的播放器实际上，它甚至像类型列表一样
actually even got like a type list thing

175
00:06:16,319 --> 00:06:17,319
以及我们稍后可能要讨论的内容，但是这两个类型和T
as well which we might talk about later

176
00:06:17,519 --> 00:06:19,869
以及我们稍后可能要讨论的内容，但是这两个类型和T
but it's got the those two types and T

177
00:06:20,069 --> 00:06:21,759
和玩家，所以我可以轻松地将其更改为实体，对吧，哦，我
and player so what I could do is easily

178
00:06:21,959 --> 00:06:23,800
和玩家，所以我可以轻松地将其更改为实体，对吧，哦，我
just change this to entity right oh I

179
00:06:24,000 --> 00:06:26,649
可以使实体II成为人事参与者权利的隐性成本，但是一旦
can make an entity II a people player

180
00:06:26,848 --> 00:06:29,709
可以使实体II成为人事参与者权利的隐性成本，但是一旦
right implicit cost however once I've

181
00:06:29,908 --> 00:06:32,050
做到了，如果我想回到播放器中，我无法轻松地做到这一点，因此我可以输入
done that if I want to go back to player

182
00:06:32,250 --> 00:06:35,170
做到了，如果我想回到播放器中，我无法轻松地做到这一点，因此我可以输入
I can't easily do that so I can type in

183
00:06:35,370 --> 00:06:37,899
玩家P等于E对，这会给我一个错误
player P equals E right that's gonna

184
00:06:38,098 --> 00:06:38,949
玩家P等于E对，这会给我一个错误
give me an error

185
00:06:39,149 --> 00:06:41,049
为什么，因为我必须使用不同的类型，原因是
why because well I have to

186
00:06:41,249 --> 00:06:43,179
为什么，因为我必须使用不同的类型，原因是
the different types the reason is that

187
00:06:43,379 --> 00:06:45,399
这很容易成为敌人，你知道如果我这样做
this could have easily been an enemy you

188
00:06:45,598 --> 00:06:46,269
这很容易成为敌人，你知道如果我这样做
know what if I do

189
00:06:46,468 --> 00:06:49,959
MC容易一个或一个等于新敌人的东西好吧这里发生了什么
MC easy one or whatever equals new enemy

190
00:06:50,158 --> 00:06:52,449
MC容易一个或一个等于新敌人的东西好吧这里发生了什么
all right what happens here what about

191
00:06:52,649 --> 00:06:54,728
如果我正确地做一个事情会发生什么，我们不仍然是一个实体，但是
what happens if I do a one right

192
00:06:54,928 --> 00:06:56,889
如果我正确地做一个事情会发生什么，我们不仍然是一个实体，但是
we don't it's still an entity but is it

193
00:06:57,088 --> 00:06:59,019
玩家是我们目前尚不了解的敌人，因此
a player is it an enemy we don't really

194
00:06:59,218 --> 00:07:01,809
玩家是我们目前尚不了解的敌人，因此
know at this at this point so because of

195
00:07:02,009 --> 00:07:04,389
该系统，我们必须通过说“嘿，它是一个
that system we have to actually reassure

196
00:07:04,588 --> 00:07:06,519
该系统，我们必须通过说“嘿，它是一个
the compiler by saying that hey it is a

197
00:07:06,718 --> 00:07:09,669
玩家现在在这种情况下这很危险
player now in this case there this is

198
00:07:09,869 --> 00:07:10,389
玩家现在在这种情况下这很危险
dangerous

199
00:07:10,588 --> 00:07:13,478
因为E人实际上是一个敌人，所以如果我们将其投给玩家，这将
because E one's actually an enemy and so

200
00:07:13,678 --> 00:07:14,889
因为E人实际上是一个敌人，所以如果我们将其投给玩家，这将
if we cast it to a player this will

201
00:07:15,088 --> 00:07:17,228
最初是起作用的，实际上，如果敌人具有像玩家那样的功能，或者
initially work and in fact if enemy has

202
00:07:17,428 --> 00:07:19,419
最初是起作用的，实际上，如果敌人具有像玩家那样的功能，或者
a function that like player also has or

203
00:07:19,619 --> 00:07:21,369
MC也有，我们尝试运行该功能，它可能会工作，我们可能
MC also has and we try and run that

204
00:07:21,569 --> 00:07:22,718
MC也有，我们尝试运行该功能，它可能会工作，我们可能
function it'll probably work we probably

205
00:07:22,918 --> 00:07:24,549
甚至不会注意到任何问题，但是如果我们尝试做一些
won't even notice any issues with it but

206
00:07:24,749 --> 00:07:26,410
甚至不会注意到任何问题，但是如果我们尝试做一些
if we try and do something that is

207
00:07:26,610 --> 00:07:28,269
敌人没有的玩家唯一的信息，例如访问某些数据成员
unique to player that enemy doesn't have

208
00:07:28,468 --> 00:07:30,788
敌人没有的玩家唯一的信息，例如访问某些数据成员
such as access certain data members that

209
00:07:30,988 --> 00:07:33,848
唯一的玩家没有敌人，因为底层类型实际上是我们的敌人
only player has a not enemy since the

210
00:07:34,048 --> 00:07:36,579
唯一的玩家没有敌人，因为底层类型实际上是我们的敌人
underlying type is actually enemy our

211
00:07:36,778 --> 00:07:38,559
程序可能会崩溃或做一些我们真正不喜欢的事情
program will likely crash or do some

212
00:07:38,759 --> 00:07:40,569
程序可能会崩溃或做一些我们真正不喜欢的事情
really weird things that we really don't

213
00:07:40,769 --> 00:07:44,739
希望它这样做，因为我们可以再次代替使用R或强制类型转换或
want it to do so because of that we can

214
00:07:44,939 --> 00:07:46,749
希望它这样做，因为我们可以再次代替使用R或强制类型转换或
again instead of using R or casts or

215
00:07:46,949 --> 00:07:48,699
甚至就像一个静态类，因为您仍然可以使用，您知道
even like a static class because you can

216
00:07:48,899 --> 00:07:50,439
甚至就像一个静态类，因为您仍然可以使用，您知道
still use you know a static cast with

217
00:07:50,639 --> 00:07:52,239
这样，我的意思是说没关系，我们可以做的是交换
this like that I mean that's fine

218
00:07:52,439 --> 00:07:54,429
这样，我的意思是说没关系，我们可以做的是交换
instead about what we can do is swap

219
00:07:54,629 --> 00:07:57,098
如果我们尝试以这种速度执行此操作，那么现在就进行动态投射
this out for a dynamic cast right now if

220
00:07:57,298 --> 00:07:58,598
如果我们尝试以这种速度执行此操作，那么现在就进行动态投射
we try to do that at this rate it's

221
00:07:58,798 --> 00:07:59,829
会告诉我们，它需要在课堂外放置聚合物，因为
gonna tell us that it needs to have a

222
00:08:00,028 --> 00:08:01,239
会告诉我们，它需要在课堂外放置聚合物，因为
polymer off the class site because

223
00:08:01,439 --> 00:08:03,338
动态类型转换仅适用于类类型的聚合物。
dynamic cast only works with polymer for

224
00:08:03,538 --> 00:08:04,689
动态类型转换仅适用于类类型的聚合物。
class types how do we do that well we

225
00:08:04,889 --> 00:08:06,609
基本上需要有某种虚拟功能表，实际上
basically need to have some kind of

226
00:08:06,809 --> 00:08:08,889
基本上需要有某种虚拟功能表，实际上
virtual function table that actually

227
00:08:09,088 --> 00:08:12,369
告诉我们它实际上是一个多态类类型，因此我们可以使它成为我们需要的
tell us that it is in fact a polymorphic

228
00:08:12,569 --> 00:08:14,199
告诉我们它实际上是一个多态类类型，因此我们可以使它成为我们需要的
class type so we can make this we need

229
00:08:14,399 --> 00:08:16,929
说虚拟虚空，您知道打印名称或类似名称并不是真的
to say virtual void you know print name

230
00:08:17,129 --> 00:08:18,459
说虚拟虚空，您知道打印名称或类似名称并不是真的
or something like this doesn't really

231
00:08:18,658 --> 00:08:20,468
无关紧要，但关键是我们将使此类具有V表，因此
matter but the point is that we'll make

232
00:08:20,668 --> 00:08:22,359
无关紧要，但关键是我们将使此类具有V表，因此
this class have a V table and thus

233
00:08:22,559 --> 00:08:23,799
有东西要覆盖，这意味着它是多态类型，我们
there's stuff to override which means

234
00:08:23,999 --> 00:08:25,838
有东西要覆盖，这意味着它是多态类型，我们
that is it's a polymorphic type and we

235
00:08:26,038 --> 00:08:27,549
如果这是一个真实的实体类，可以再次使用动态类型转换
can use dynamic casting with it again if

236
00:08:27,749 --> 00:08:30,069
如果这是一个真实的实体类，可以再次使用动态类型转换
this was a real entity class it would

237
00:08:30,269 --> 00:08:31,629
肯定有虚函数，所以这只是使它更多一点
definitely have virtual functions so

238
00:08:31,829 --> 00:08:33,549
肯定有虚函数，所以这只是使它更多一点
this is just making it a little bit more

239
00:08:33,750 --> 00:08:36,968
也很现实，所以我们可以正确地表达它，所以我们得到了e
realistic as well so we can cast it

240
00:08:37,168 --> 00:08:38,588
也很现实，所以我们可以正确地表达它，所以我们得到了e
right so we've got our e which is a

241
00:08:38,788 --> 00:08:41,229
玩家，或者我们可以使用一个实际上是敌人的人，所以我们
player or we can use a one which is that

242
00:08:41,429 --> 00:08:42,998
玩家，或者我们可以使用一个实际上是敌人的人，所以我们
and actually actually an enemy so we

243
00:08:43,198 --> 00:08:45,159
可以说实际上玩家只是为了使这一点更加清楚，然后我们
could say actually player just to make

244
00:08:45,360 --> 00:08:47,198
可以说实际上玩家只是为了使这一点更加清楚，然后我们
this a little bit more clear and then we

245
00:08:47,399 --> 00:08:50,529
可以看到它说实际上是敌人，所以，如果我们设法让我们的敌人
could see it say actually enemy all

246
00:08:50,730 --> 00:08:53,289
可以看到它说实际上是敌人，所以，如果我们设法让我们的敌人
right so if we try and get our our enemy

247
00:08:53,490 --> 00:08:54,879
进入玩家会失败，但是如果我们尝试
into a player this

248
00:08:55,080 --> 00:08:57,429
进入玩家会失败，但是如果我们尝试
would fail right but if we try and

249
00:08:57,629 --> 00:09:00,549
将此玩家转换为即使他们都在
convert this player to a player that

250
00:09:00,750 --> 00:09:02,319
将此玩家转换为即使他们都在
should work even though they're both at

251
00:09:02,519 --> 00:09:04,870
他们现在都是实体，所以让我们看看这是什么
them at the moment they're both entities

252
00:09:05,070 --> 00:09:08,169
他们现在都是实体，所以让我们看看这是什么
okay so let's take a look at what this

253
00:09:08,370 --> 00:09:10,299
如果我们要执行代码，它将在此处放置一个断点并按f5键
code will do if we were to execute it

254
00:09:10,500 --> 00:09:12,009
如果我们要执行代码，它将在此处放置一个断点并按f5键
I'll put a breakpoint here and hit f5

255
00:09:12,210 --> 00:09:13,899
当然，您应该确保我的名字是不同的变量
and of course you should make sure that

256
00:09:14,100 --> 00:09:15,429
当然，您应该确保我的名字是不同的变量
my name is variables different things

257
00:09:15,629 --> 00:09:17,709
好吧，这个种姓应该失败，因为这实际上是一个敌人，
okay so this caste should fail right

258
00:09:17,909 --> 00:09:20,289
好吧，这个种姓应该失败，因为这实际上是一个敌人，
because this is actually an enemy and

259
00:09:20,490 --> 00:09:21,759
我们正在尝试将其投放到玩家，因此f10，您会看到此返回null
we're trying to cast it to a player so

260
00:09:21,960 --> 00:09:23,799
我们正在尝试将其投放到玩家，因此f10，您会看到此返回null
f10 and you can see this returns null

261
00:09:24,000 --> 00:09:25,899
投射不起作用，但是如果我尝试在这里进行操作，您会看到p1在
the cast didn't work however if I try

262
00:09:26,100 --> 00:09:28,389
投射不起作用，但是如果我尝试在这里进行操作，您会看到p1在
and do it here you can see that p1 is in

263
00:09:28,590 --> 00:09:31,000
实际上是一个有效的实体，它能够成功地将其投放到玩家中，因为
fact a valid entity it successfully was

264
00:09:31,200 --> 00:09:33,399
实际上是一个有效的实体，它能够成功地将其投放到玩家中，因为
able to cast it into a player because it

265
00:09:33,600 --> 00:09:35,500
实际上是一个球员，如果有的话，基本上就是所有的动态施法者
actually was a player and that is

266
00:09:35,700 --> 00:09:38,079
实际上是一个球员，如果有的话，基本上就是所有的动态施法者
essentially all the dynamic casters if

267
00:09:38,279 --> 00:09:41,620
强制转换有效，然后返回您要播放指针的值
the cast is valid then it returns the

268
00:09:41,820 --> 00:09:43,569
强制转换有效，然后返回您要播放指针的值
value that you want to play a pointer

269
00:09:43,769 --> 00:09:46,509
但是如果它无效，因为它不是您给定的类型，
but if it is not valid because it's not

270
00:09:46,710 --> 00:09:47,889
但是如果它无效，因为它不是您给定的类型，
the given type that you've kind of

271
00:09:48,090 --> 00:09:49,779
声称就是那样，它只会给你带来的不便是动态投射
claimed that it is then it will just

272
00:09:49,980 --> 00:09:51,639
声称就是那样，它只会给你带来的不便是动态投射
give you not that's what dynamic cast

273
00:09:51,840 --> 00:09:54,039
简而言之，您应该从中得到的问题是但是如何
does in a nutshell now the question that

274
00:09:54,240 --> 00:09:56,169
简而言之，您应该从中得到的问题是但是如何
you should have from this is but how

275
00:09:56,370 --> 00:09:58,899
它知道我没有写任何东西吗，也不像C ++或某些托管
does it know I haven't written anything

276
00:09:59,100 --> 00:10:02,199
它知道我没有写任何东西吗，也不像C ++或某些托管
and it's not like C++ or some managed

277
00:10:02,399 --> 00:10:04,539
诸如C Sharp或Java之类的语言，如何知道
language like C sharp or Java or

278
00:10:04,740 --> 00:10:06,969
诸如C Sharp或Java之类的语言，如何知道
anything like that how how does it know

279
00:10:07,169 --> 00:10:10,359
该玩家实际上是一个玩家，它如何知道我所拥有的这个实体
that player is actually a player like

280
00:10:10,559 --> 00:10:11,709
该玩家实际上是一个玩家，它如何知道我所拥有的这个实体
how does it know that this entity that I

281
00:10:11,909 --> 00:10:13,389
拥有的实际上是一个玩家而不是一个敌人，它的行为方式是
have is actually a player and not an

282
00:10:13,590 --> 00:10:15,789
拥有的实际上是一个玩家而不是一个敌人，它的行为方式是
enemy what the way that it does that is

283
00:10:15,990 --> 00:10:17,529
它实际上存储了运行时类型信息，特别是所谓的
it actually stores runtime type

284
00:10:17,730 --> 00:10:18,939
它实际上存储了运行时类型信息，特别是所谓的
information especially what it's called

285
00:10:19,139 --> 00:10:19,719
我们存储的TTI运行时类型信息
our TTI

286
00:10:19,919 --> 00:10:22,269
我们存储的TTI运行时类型信息
runtime type information it stores

287
00:10:22,470 --> 00:10:24,399
有关我们所有类型的运行时类型信息，这确实增加了开销，但
runtime type information about all of

288
00:10:24,600 --> 00:10:27,549
有关我们所有类型的运行时类型信息，这确实增加了开销，但
our types this does add an overhead but

289
00:10:27,750 --> 00:10:29,139
它可以让您执行动态转换之类的操作，因此有两件事需要
it lets you do things like dynamic

290
00:10:29,340 --> 00:10:31,449
它可以让您执行动态转换之类的操作，因此有两件事需要
casting so there's two things to

291
00:10:31,649 --> 00:10:33,789
首先在这里考虑我们的TTI增加了开销，因为突然键入
consider here first of all our TTI adds

292
00:10:33,990 --> 00:10:36,939
首先在这里考虑我们的TTI增加了开销，因为突然键入
overhead because suddenly types type

293
00:10:37,139 --> 00:10:38,259
类型需要存储有关自身的更多信息
types need to store more information

294
00:10:38,460 --> 00:10:40,299
类型需要存储有关自身的更多信息
about themselves than were otherwise

295
00:10:40,500 --> 00:10:45,429
当前和大型或动态投放也需要时间，因为我们需要
present and big or to dynamic cast is

296
00:10:45,629 --> 00:10:47,409
当前和大型或动态投放也需要时间，因为我们需要
also also takes time because we need to

297
00:10:47,610 --> 00:10:49,539
实际检查类型信息是否与该实体匹配
actually check to see does the type

298
00:10:49,740 --> 00:10:51,609
实际检查类型信息是否与该实体匹配
information match is this entity

299
00:10:51,809 --> 00:10:55,299
实际上是敌人还是玩家，我们必须这样做
actually an enemy or is it a player what

300
00:10:55,500 --> 00:10:56,469
实际上是敌人还是玩家，我们必须这样做
type is that we have to do that

301
00:10:56,669 --> 00:10:57,519
任何时候在运行时进行验证我们都会动态产生成本，因此
validation at runtime

302
00:10:57,720 --> 00:11:00,039
任何时候在运行时进行验证我们都会动态产生成本，因此
anytime we dynamic cost so because of

303
00:11:00,240 --> 00:11:03,759
现在确实增加了开销，我们在这里的代码中实际可以做的是
that it does add that overhead now what

304
00:11:03,960 --> 00:11:05,620
现在确实增加了开销，我们在这里的代码中实际可以做的是
we can actually do in our code here is

305
00:11:05,820 --> 00:11:07,899
如果不需要，请关闭运行时类型信息
turn runtime type information off if we

306
00:11:08,100 --> 00:11:08,479
如果不需要，请关闭运行时类型信息
don't need it

307
00:11:08,679 --> 00:11:10,039
因此，如果我们转到属性，它是不同的最喜欢的编译器，但是
so if we go to our properties and it's

308
00:11:10,240 --> 00:11:11,688
因此，如果我们转到属性，它是不同的最喜欢的编译器，但是
different favorite compiler but over

309
00:11:11,889 --> 00:11:14,448
在安全的c ++和语言以及Visual Studio中，您可以看到我们有这个
here inside safe c++ and language and

310
00:11:14,649 --> 00:11:16,219
在安全的c ++和语言以及Visual Studio中，您可以看到我们有这个
visual studio you can see we have this

311
00:11:16,419 --> 00:11:18,469
如果要关闭运行时类型信息，然后启用
enable runtime type information if we

312
00:11:18,669 --> 00:11:20,479
如果要关闭运行时类型信息，然后启用
were to switch that off and then we

313
00:11:20,679 --> 00:11:24,048
尝试编译我们的代码，您会发现它实际上会给我们一个
attempted to compile our code you would

314
00:11:24,249 --> 00:11:26,839
尝试编译我们的代码，您会发现它实际上会给我们一个
see that it actually would give us a

315
00:11:27,039 --> 00:11:28,668
警告说动态类用于多态类型实体
warning which says dynamic class used on

316
00:11:28,869 --> 00:11:32,089
警告说动态类用于多态类型实体
polymorphic type entity with this just

317
00:11:32,289 --> 00:11:33,378
表示我们已经关闭了运行时类型信息，
means that we've switched off runtime

318
00:11:33,578 --> 00:11:34,578
表示我们已经关闭了运行时类型信息，
type information so with that with our

319
00:11:34,778 --> 00:11:37,459
TTI关闭给我们带来了无法预测的行为，如果我们尝试运行此代码，我们
TTI off gives us unpredictable behavior

320
00:11:37,659 --> 00:11:40,938
TTI关闭给我们带来了无法预测的行为，如果我们尝试运行此代码，我们
and if we try and run this code and we

321
00:11:41,139 --> 00:11:43,639
按下f10键即可越过这个无效的动态类型，您可以看到我们实际上获得了
hit f10 to get past this invalid dynamic

322
00:11:43,839 --> 00:11:45,139
按下f10键即可越过这个无效的动态类型，您可以看到我们实际上获得了
cast you can see we actually get an

323
00:11:45,339 --> 00:11:48,168
访问冲突，所以实际上会为我们抛出一个错误，所以它不会
access violation so that actually throws

324
00:11:48,369 --> 00:11:50,508
访问冲突，所以实际上会为我们抛出一个错误，所以它不会
an error for us all right so it doesn't

325
00:11:50,708 --> 00:11:51,738
不给我们null，因为它不能，因为它不具有该类型
doesn't give us null because it can't

326
00:11:51,938 --> 00:11:53,089
不给我们null，因为它不能，因为它不具有该类型
because it doesn't have that type

327
00:11:53,289 --> 00:11:55,159
信息，因此请确保您了解
information so make sure that you are

328
00:11:55,360 --> 00:11:57,139
信息，因此请确保您了解
aware of the kind of implications the

329
00:11:57,339 --> 00:12:00,139
动态类型转换实际上是因为它们做一些额外的事情，并且它们确实
dynamic casts actually do because they

330
00:12:00,339 --> 00:12:02,628
动态类型转换实际上是因为它们做一些额外的事情，并且它们确实
do kind of do extra things and they do

331
00:12:02,828 --> 00:12:05,948
要求在大多数情况下我们的TTI处于开启状态，例如我们想要进行演员
require that our TTI is on in most cases

332
00:12:06,149 --> 00:12:08,779
要求在大多数情况下我们的TTI处于开启状态，例如我们想要进行演员
if we were to like for example do a cast

333
00:12:08,980 --> 00:12:11,149
从玩家到实体都是隐含的，这很有趣，不会崩溃
from player to entity which is implicit

334
00:12:11,350 --> 00:12:13,519
从玩家到实体都是隐含的，这很有趣，不会崩溃
anyway it would be fun it wouldn't crash

335
00:12:13,720 --> 00:12:15,498
但是如果我们尝试做这样的事情，您会发现它只是崩溃了
but if we try and do something like this

336
00:12:15,698 --> 00:12:17,628
但是如果我们尝试做这样的事情，您会发现它只是崩溃了
you can see it just just it just crashes

337
00:12:17,828 --> 00:12:20,149
甚至根本不允许我们这样做，而启用TTI是非常典型的，但是
doesn't even allow us to it at all and

338
00:12:20,350 --> 00:12:23,149
甚至根本不允许我们这样做，而启用TTI是非常典型的，但是
having our TTI on is pretty typical but

339
00:12:23,350 --> 00:12:25,068
也确实增加了一些开销，所以请记住这一点，我最后一件事
also does add a bit of an overhead so

340
00:12:25,269 --> 00:12:27,738
也确实增加了一些开销，所以请记住这一点，我最后一件事
just keep that in mind one last thing I

341
00:12:27,938 --> 00:12:29,688
想要向您展示的是因为动态，因为我们拥有动态演员表
want to show you is that because of

342
00:12:29,889 --> 00:12:31,368
想要向您展示的是因为动态，因为我们拥有动态演员表
dynamic because we have dynamic casts

343
00:12:31,568 --> 00:12:33,378
你知道我们可以做我提到的验证，我们可以做类似
you know we can do that validation that

344
00:12:33,578 --> 00:12:34,969
你知道我们可以做我提到的验证，我们可以做类似
I mentioned we can do something like if

345
00:12:35,169 --> 00:12:37,758
p0现在与管理C Sharp或Java这样的语言非常相似
p0 now this is very similar to manage

346
00:12:37,958 --> 00:12:40,308
p0现在与管理C Sharp或Java这样的语言非常相似
languages like C sharp or Java where we

347
00:12:40,509 --> 00:12:43,099
基本上可以尝试检查什么是真正的东西，所以这实际上
can basically try and check what is what

348
00:12:43,299 --> 00:12:44,839
基本上可以尝试检查什么是真正的东西，所以这实际上
something actually is so this actually

349
00:12:45,039 --> 00:12:47,209
你知道的敌人，就像c锐一样，如果敌人真的是一个
enemy you know in like c-sharp we could

350
00:12:47,409 --> 00:12:50,899
你知道的敌人，就像c锐一样，如果敌人真的是一个
do if if is if actually enemy is a

351
00:12:51,100 --> 00:12:53,868
播放器，然后我们就可以在Java中完成所有操作，只需使用
player then we can do it all over in

352
00:12:54,068 --> 00:12:56,719
播放器，然后我们就可以在Java中完成所有操作，只需使用
Java we have instance of so by using

353
00:12:56,919 --> 00:12:58,639
这种动态转换的东西，我们基本上可以实现
this kind of dynamic cast thing we can

354
00:12:58,839 --> 00:13:00,409
这种动态转换的东西，我们基本上可以实现
basically achieve the same thing we can

355
00:13:00,610 --> 00:13:03,469
如果说动态角色扮演者实际上是敌人，那么如果实际上敌人是玩家，
say if dynamic cosplayer actually enemy

356
00:13:03,669 --> 00:13:05,478
如果说动态角色扮演者实际上是敌人，那么如果实际上敌人是玩家，
right if actually enemy is a player

357
00:13:05,678 --> 00:13:07,339
实例，那么我们可以执行以下代码，如果我完成此操作，您将
instance then we can do the following

358
00:13:07,539 --> 00:13:08,808
实例，那么我们可以执行以下代码，如果我完成此操作，您将
code and if I just complete this you'll

359
00:13:09,009 --> 00:13:10,639
看到那是完全正确的权利，但显然我们想对它进行强制转换
see that that's perfectly valid right

360
00:13:10,839 --> 00:13:13,188
看到那是完全正确的权利，但显然我们想对它进行强制转换
but obviously we want to kind of cast it

361
00:13:13,389 --> 00:13:15,409
再次使用它可能还可以，因此我们可能会做一些事情
again and use it probably in that okay

362
00:13:15,610 --> 00:13:17,659
再次使用它可能还可以，因此我们可能会做一些事情
so therefore we'd probably do something

363
00:13:17,860 --> 00:13:20,328
像这样的代码很典型，您一直都在看
like this this kind of code is pretty

364
00:13:20,528 --> 00:13:22,250
像这样的代码很典型，您一直都在看
typical you see it all the time

365
00:13:22,450 --> 00:13:24,740
但请注意，动态投放会产生一定的成本，因此，如果您只想
but just be aware that dynamic casting

366
00:13:24,940 --> 00:13:27,829
但请注意，动态投放会产生一定的成本，因此，如果您只想
does incur a cost so if you're all about

367
00:13:28,029 --> 00:13:29,299
如果您尝试使用非常快速的代码编写代码，则可能需要进行优化
optimization if you try and write with

368
00:13:29,500 --> 00:13:31,250
如果您尝试使用非常快速的代码编写代码，则可能需要进行优化
really fast code you'll probably want to

369
00:13:31,450 --> 00:13:33,169
避免这种情况，希望大家喜欢这个视频，您可以按“赞”按钮
avoid that and I hope you guys enjoyed

370
00:13:33,370 --> 00:13:34,699
避免这种情况，希望大家喜欢这个视频，您可以按“赞”按钮
that video you can hit the like button

371
00:13:34,899 --> 00:13:36,139
您还可以帮助支持我在patreon上所做的一切-Cherno
you can also help support everything

372
00:13:36,340 --> 00:13:38,750
您还可以帮助支持我在patreon上所做的一切-Cherno
that I do on patreon for - the Cherno

373
00:13:38,950 --> 00:13:41,319
下次再见。[音乐]
and I'll see you next time goodbye

374
00:13:41,519 --> 00:13:46,519
下次再见。[音乐]
[Music]

