﻿1
00:00:00,000 --> 00:00:01,269
嘿，小伙子们，我的名字叫卢西亚诺。
hey little guys my name is Luciano

2
00:00:01,469 --> 00:00:03,609
嘿，小伙子们，我的名字叫卢西亚诺。
welcome to another video today what are

3
00:00:03,810 --> 00:00:05,740
我们谈论的是函数和C ++，我梦见的一个星期中的视频是Jono
we talking about functions and C++ what

4
00:00:05,940 --> 00:00:08,680
我们谈论的是函数和C ++，我梦见的一个星期中的视频是Jono
a midweek video am i dreaming has Jono

5
00:00:08,880 --> 00:00:11,080
也许发疯了，但这不是我只想说声谢谢的意思
gone crazy maybe but that's not the

6
00:00:11,279 --> 00:00:12,729
也许发疯了，但这不是我只想说声谢谢的意思
point I just want to say a huge thank

7
00:00:12,929 --> 00:00:13,930
谢谢那些一直在画这个系列的人，我真的很高兴你们
you for everyone who's been drawing this

8
00:00:14,130 --> 00:00:15,160
谢谢那些一直在画这个系列的人，我真的很高兴你们
series I'm really glad you guys are

9
00:00:15,359 --> 00:00:16,810
享受这一点，因为如果您想告诉我如何，这对我来说真是一个爆炸
enjoying this because it's been a real

10
00:00:17,010 --> 00:00:18,640
享受这一点，因为如果您想告诉我如何，这对我来说真是一个爆炸
blast to me if you want to show me how

11
00:00:18,839 --> 00:00:20,260
非常感谢我的视频，您可以在Instagram链接上关注我
much you appreciate my videos you can

12
00:00:20,460 --> 00:00:21,909
非常感谢我的视频，您可以在Instagram链接上关注我
follow me on Instagram link in the

13
00:00:22,109 --> 00:00:23,230
下面的描述最近我真的很喜欢摄影，
description below lately I've been

14
00:00:23,429 --> 00:00:24,519
下面的描述最近我真的很喜欢摄影，
really into photography and it would

15
00:00:24,719 --> 00:00:25,780
如果你们可以跟随我，对我来说就是世界
mean the world to me if you guys could

16
00:00:25,980 --> 00:00:26,379
如果你们可以跟随我，对我来说就是世界
follow me

17
00:00:26,579 --> 00:00:28,210
就像现在暂停此视频一样，只要跟随我就可以回到
just like pause this video right now and

18
00:00:28,410 --> 00:00:30,669
就像现在暂停此视频一样，只要跟随我就可以回到
just go and follow me anyway back to

19
00:00:30,868 --> 00:00:33,189
函数，所以什么才是函数函数基本上是代码块
functions so what exactly are functions

20
00:00:33,390 --> 00:00:34,899
函数，所以什么才是函数函数基本上是代码块
functions are basically blocks of code

21
00:00:35,100 --> 00:00:36,309
我们编写的旨在在以后执行特定任务的代码
that we write that are designed to

22
00:00:36,509 --> 00:00:38,829
我们编写的旨在在以后执行特定任务的代码
perform a specific task later when we

23
00:00:39,030 --> 00:00:40,899
上课时，这些块称为方法，但是当我说函数时，
get to classes those blocks are called

24
00:00:41,100 --> 00:00:43,178
上课时，这些块称为方法，但是当我说函数时，
methods but when I say functions I'm

25
00:00:43,378 --> 00:00:44,858
明确地谈论不属于班级的事情，这很普遍
explicitly talking about something that

26
00:00:45,058 --> 00:00:46,959
明确地谈论不属于班级的事情，这很普遍
isn't part of a class it's pretty common

27
00:00:47,159 --> 00:00:48,669
让我们拆分功能以防止代码重复，我们不想成为
for us to split up functions to prevent

28
00:00:48,869 --> 00:00:50,439
让我们拆分功能以防止代码重复，我们不想成为
code duplication we don't want to be

29
00:00:50,640 --> 00:00:52,059
当然，如果我们除了
running the same code multiple times

30
00:00:52,259 --> 00:00:54,309
当然，如果我们除了
because of course if we did apart from

31
00:00:54,509 --> 00:00:55,838
复制并粘贴大量代码，最后以巨大的混乱结束
copying and pasting a lot of code and

32
00:00:56,039 --> 00:00:59,378
复制并粘贴大量代码，最后以巨大的混乱结束
just ending up with this huge mess it

33
00:00:59,579 --> 00:01:01,268
这也意味着，如果我们决定更改某些代码，则必须全部更改
also means that if we decide to change

34
00:01:01,469 --> 00:01:03,158
这也意味着，如果我们决定更改某些代码，则必须全部更改
some code we have to change it in all

35
00:01:03,359 --> 00:01:04,959
我们粘贴原始代码的那些地方
those places where we pasted the

36
00:01:05,159 --> 00:01:06,278
我们粘贴原始代码的那些地方
original code and that's just going to

37
00:01:06,478 --> 00:01:08,289
维护是一场灾难，所以我们能做的只是写一点点
be a disaster to maintain so what we can

38
00:01:08,489 --> 00:01:10,028
维护是一场灾难，所以我们能做的只是写一点点
do instead is just write a nice little

39
00:01:10,228 --> 00:01:11,769
函数执行我们想要的功能，然后可以多次调用它
function that does what we want it to

40
00:01:11,969 --> 00:01:13,450
函数执行我们想要的功能，然后可以多次调用它
and then we can call it multiple times

41
00:01:13,650 --> 00:01:15,429
在我们的代码中，如果需要的话，您可以想到具有输入和输入的功能
in our code if we need to you can think

42
00:01:15,629 --> 00:01:17,319
在我们的代码中，如果需要的话，您可以想到具有输入和输入的功能
of functions of having an input and an

43
00:01:17,519 --> 00:01:18,849
输出，尽管它们不一定需要我们可以提供功能
output although they don't necessarily

44
00:01:19,049 --> 00:01:21,009
输出，尽管它们不一定需要我们可以提供功能
need to we can provide the function with

45
00:01:21,209 --> 00:01:23,200
某些参数和函数可以为我们返回值，因此假设我们
certain parameters and the function can

46
00:01:23,400 --> 00:01:25,238
某些参数和函数可以为我们返回值，因此假设我们
return a value for us so suppose that we

47
00:01:25,438 --> 00:01:27,129
想要将两个数字相乘，我们想编写一个函数
wanted to multiply two numbers together

48
00:01:27,329 --> 00:01:28,869
想要将两个数字相乘，我们想编写一个函数
and we wanted to write a function that

49
00:01:29,069 --> 00:01:30,429
做到这一点，所以我要在这里写的第一件事是所谓的回报
did that so the first thing I'm going to

50
00:01:30,629 --> 00:01:31,929
做到这一点，所以我要在这里写的第一件事是所谓的回报
write here is something called a return

51
00:01:32,129 --> 00:01:34,058
这是该函数将返回的类型的值，因为我们
value that is this is the type that this

52
00:01:34,259 --> 00:01:35,799
这是该函数将返回的类型的值，因为我们
function will return since we're

53
00:01:36,000 --> 00:01:37,418
当然，将两个整数相乘会得到一个整数，所以我们的返回值
multiplying two integers that of course

54
00:01:37,618 --> 00:01:39,369
当然，将两个整数相乘会得到一个整数，所以我们的返回值
will result in an integer so our return

55
00:01:39,569 --> 00:01:40,959
类型将是int在这种情况下，我将给函数命名
type is going to be int I'm going to

56
00:01:41,159 --> 00:01:42,308
类型将是int在这种情况下，我将给函数命名
give the function a name in this case

57
00:01:42,509 --> 00:01:44,829
乘数，它需要两个参数，实际上是
multiplier and it's going to take two

58
00:01:45,030 --> 00:01:46,539
乘数，它需要两个参数，实际上是
parameters these are actually the

59
00:01:46,739 --> 00:01:47,528
我们想要相乘的数字，我将其称为a和B
numbers that we want to multiply

60
00:01:47,728 --> 00:01:49,418
我们想要相乘的数字，我将其称为a和B
together I'll just call them a and B

61
00:01:49,618 --> 00:01:51,579
我给这个函数一个body，所有要做的就是返回一个B
I'll give the function a body and all

62
00:01:51,780 --> 00:01:54,488
我给这个函数一个body，所有要做的就是返回一个B
this is going to do is return a times B

63
00:01:54,688 --> 00:01:55,808
所以您可以看到我们这里有一个函数，它同时包含两个参数
so you can see that we've got a function

64
00:01:56,009 --> 00:01:57,698
所以您可以看到我们这里有一个函数，它同时包含两个参数
here that takes in two parameters both

65
00:01:57,899 --> 00:02:00,159
整数并简单地返回我们没有的那两个数字的乘积
integers and simply returns the product

66
00:02:00,359 --> 00:02:01,628
整数并简单地返回我们没有的那两个数字的乘积
of those two numbers we don't

67
00:02:01,828 --> 00:02:03,488
一定要提供参数，例如，我无法提供任何参数
necessarily have to provide parameters

68
00:02:03,688 --> 00:02:05,378
一定要提供参数，例如，我无法提供任何参数
for example I could just not provide any

69
00:02:05,578 --> 00:02:07,209
参数并返回类似八乘五的东西，这仍然是
parameters and return something like

70
00:02:07,409 --> 00:02:09,009
参数并返回类似八乘五的东西，这仍然是
five times eight this is still a

71
00:02:09,209 --> 00:02:10,449
返回整数但不带任何整数的函数
function that returns

72
00:02:10,649 --> 00:02:12,879
返回整数但不带任何整数的函数
integer but it is just not taking any

73
00:02:13,079 --> 00:02:14,560
参数，我们还可以告诉函数我们不希望它返回
parameters we could also tell the

74
00:02:14,759 --> 00:02:16,030
参数，我们还可以告诉函数我们不希望它返回
function that we don't want it to return

75
00:02:16,229 --> 00:02:17,920
任何事情，我们通过将void编写为返回类型来实现，当然void
anything and we do that by writing void

76
00:02:18,120 --> 00:02:19,929
任何事情，我们通过将void编写为返回类型来实现，当然void
as its return type void of course means

77
00:02:20,128 --> 00:02:21,310
没什么，反而这可以像爱的结果那样做
nothing so instead this could do

78
00:02:21,509 --> 00:02:22,868
没什么，反而这可以像爱的结果那样做
something like love the result to the

79
00:02:23,068 --> 00:02:27,429
控制台，所以让我们回到此处的原始示例，其中有na和int B
console so let's go back to our original

80
00:02:27,628 --> 00:02:30,159
控制台，所以让我们回到此处的原始示例，其中有na和int B
example here where we had n a and int B

81
00:02:30,359 --> 00:02:32,770
我们返回了这两个整数的乘积，所以我们怎么称呼这个函数
and we returned the product of those two

82
00:02:32,969 --> 00:02:34,780
我们返回了这两个整数的乘积，所以我们怎么称呼这个函数
integers so how do we call this function

83
00:02:34,979 --> 00:02:37,060
调用函数非常简单，让我们继续尝试并打印
well calling a function is pretty simple

84
00:02:37,259 --> 00:02:39,039
调用函数非常简单，让我们继续尝试并打印
let's go ahead and try and print the

85
00:02:39,239 --> 00:02:40,840
乘法的结果我要首先创建一个变量
result of a multiplication I'm going to

86
00:02:41,039 --> 00:02:42,610
乘法的结果我要首先创建一个变量
first of all make a variable which holds

87
00:02:42,810 --> 00:02:44,319
这个结果，所以我将输入最终结果等于乘法，我们将使用3和
this result so I'll type end result

88
00:02:44,519 --> 00:02:47,230
这个结果，所以我将输入最终结果等于乘法，我们将使用3和
equals multiply and we'll go with 3 and

89
00:02:47,430 --> 00:02:49,750
2所以要做的就是用这两个调用这个乘法函数
2 so what this is going to do is call

90
00:02:49,949 --> 00:02:51,550
2所以要做的就是用这两个调用这个乘法函数
this multiply function with these two

91
00:02:51,750 --> 00:02:53,560
参数，然后存储返回时间B的返回值
parameters and then store the return

92
00:02:53,759 --> 00:02:56,110
参数，然后存储返回时间B的返回值
value that is this result of a times B

93
00:02:56,310 --> 00:02:58,750
在这个结果整数中，我们可以将结果输出到控制台
in this result integer we can then

94
00:02:58,949 --> 00:03:00,909
在这个结果整数中，我们可以将结果输出到控制台
output that result to the console

95
00:03:01,109 --> 00:03:03,129
让我们按f5键运行我们的程序，然后我们的水果模型可以看到我们得到6
let's hit f5 to run our program and our

96
00:03:03,329 --> 00:03:04,629
让我们按f5键运行我们的程序，然后我们的水果模型可以看到我们得到6
fruit builds you can see that we get 6

97
00:03:04,829 --> 00:03:06,250
当然，那是三乘二，所以让我们假设这是一个档次
which of course is what three times two

98
00:03:06,449 --> 00:03:08,618
当然，那是三乘二，所以让我们假设这是一个档次
is so let's kick this up a notch suppose

99
00:03:08,818 --> 00:03:09,700
我想做一堆乘法，我想记录所有
that I want to do a bunch of

100
00:03:09,900 --> 00:03:12,009
我想做一堆乘法，我想记录所有
multiplications and I want to log all of

101
00:03:12,209 --> 00:03:13,509
他们到控制台，如果我做这样的事情没有功能，那么它
them to the console if I do something

102
00:03:13,709 --> 00:03:15,280
他们到控制台，如果我做这样的事情没有功能，那么它
like that without a function then it

103
00:03:15,479 --> 00:03:16,960
看起来很乱，例如，我需要重复这段代码，让我们开始吧
would look pretty messy so for example I

104
00:03:17,159 --> 00:03:18,759
看起来很乱，例如，我需要重复这段代码，让我们开始吧
need to repeat this code so let's go

105
00:03:18,959 --> 00:03:21,129
向前复制并粘贴几次，我将其称为“结果2结果”
ahead and copy and paste a few time I'll

106
00:03:21,329 --> 00:03:22,899
向前复制并粘贴几次，我将其称为“结果2结果”
call it something like result two result

107
00:03:23,098 --> 00:03:27,360
三个会执行八次，五次执行90次，执行45次，如果我运行程序，然后挂起
three will do eight times five 90 times

108
00:03:27,560 --> 00:03:31,330
三个会执行八次，五次执行90次，执行45次，如果我运行程序，然后挂起
45 and if I run my program and oh hang

109
00:03:31,530 --> 00:03:32,409
一分钟，为什么我到处都得到相同的值，哦，当我复制时
on a minute why am I getting the same

110
00:03:32,609 --> 00:03:34,209
一分钟，为什么我到处都得到相同的值，哦，当我复制时
value everywhere oh look when I copied

111
00:03:34,408 --> 00:03:35,920
并粘贴了这段代码，我忘记了更改变量，现在您可能已经想到了
and pasted this code I forgot to change

112
00:03:36,120 --> 00:03:37,539
并粘贴了这段代码，我忘记了更改变量，现在您可能已经想到了
the variable now you may have thought

113
00:03:37,739 --> 00:03:38,980
我是偶然地这样做的，但是我实际上是故意证明
that I did that by accident but I

114
00:03:39,180 --> 00:03:40,300
我是偶然地这样做的，但是我实际上是故意证明
actually did it on purpose to prove

115
00:03:40,500 --> 00:03:43,539
实际上，人们一直在复制和粘贴
something this actually happens all the

116
00:03:43,739 --> 00:03:45,700
实际上，人们一直在复制和粘贴
time people copy and paste blocks of

117
00:03:45,900 --> 00:03:48,219
代码，然后忘记更改一个较小的细节，在某些情况下，您
code and then forget change one minor

118
00:03:48,419 --> 00:03:50,560
代码，然后忘记更改一个较小的细节，在某些情况下，您
detail and in certain situations you

119
00:03:50,759 --> 00:03:52,090
可能实际上只是运行您的程序，甚至没有注意到它不起作用
might actually just run your program and

120
00:03:52,289 --> 00:03:53,349
可能实际上只是运行您的程序，甚至没有注意到它不起作用
not even notice that it's not working

121
00:03:53,549 --> 00:03:56,590
正确，直到它折断了某处，但像这样的东西可以
correctly until it breaks somewhere down

122
00:03:56,789 --> 00:03:58,390
正确，直到它折断了某处，但像这样的东西可以
the line and yet something like this can

123
00:03:58,590 --> 00:03:59,830
如果您只是为此创建一个函数，那么实际上就很容易修复
actually be fixed really easily if you

124
00:04:00,030 --> 00:04:01,270
如果您只是为此创建一个函数，那么实际上就很容易修复
just create a function for it let's go

125
00:04:01,469 --> 00:04:02,800
并通过实际打印结果二和三来解决此问题（如果我运行此方法）
ahead and fix this by actually printing

126
00:04:03,000 --> 00:04:04,689
并通过实际打印结果二和三来解决此问题（如果我运行此方法）
out results two and three if I run this

127
00:04:04,889 --> 00:04:06,429
我们会得到正确的结果，这很好，但是您可以看到我在
we will get our correct results which is

128
00:04:06,628 --> 00:04:07,810
我们会得到正确的结果，这很好，但是您可以看到我在
great however you can see that I'm

129
00:04:08,009 --> 00:04:09,849
实际多次调用它，就像这样有点烦人
actually calling this multiple times and

130
00:04:10,049 --> 00:04:11,950
实际多次调用它，就像这样有点烦人
it's just a little bit annoying like for

131
00:04:12,150 --> 00:04:13,810
如果我决定替换此乘法函数，示例会更进一步
example further down the road if I

132
00:04:14,009 --> 00:04:15,759
如果我决定替换此乘法函数，示例会更进一步
decide to replace this multiply function

133
00:04:15,959 --> 00:04:17,709
仅仅通过做类似八乘五的事情就可以了
by simply from doing something like

134
00:04:17,908 --> 00:04:19,899
仅仅通过做类似八乘五的事情就可以了
eight times five look at this I have to

135
00:04:20,098 --> 00:04:22,180
在每个地方替换它两次两次
replace it in every single place three

136
00:04:22,379 --> 00:04:23,230
在每个地方替换它两次两次
times two

137
00:04:23,430 --> 00:04:25,870
90乘以45这就是我想要处理的，因此乘法
90 times 45 that's I want to have to

138
00:04:26,069 --> 00:04:27,218
90乘以45这就是我想要处理的，因此乘法
deal with that so this multiplication

139
00:04:27,418 --> 00:04:29,620
然后记录结果，让我们继续进行功能
and then logging the result let's go

140
00:04:29,819 --> 00:04:30,819
然后记录结果，让我们继续进行功能
ahead and make the function for that

141
00:04:31,019 --> 00:04:32,499
它将是空的，因为它并不会真的将任何东西退还给我们，只是
it'll be void because it's not really

142
00:04:32,699 --> 00:04:34,150
它将是空的，因为它并不会真的将任何东西退还给我们，只是
going to return anything to us it's just

143
00:04:34,350 --> 00:04:36,189
要执行我们要求它执行的操作，我们将其称为乘法
going to perform what we asked it to do

144
00:04:36,389 --> 00:04:37,718
要执行我们要求它执行的操作，我们将其称为乘法
we'll call it something like multiply

145
00:04:37,918 --> 00:04:39,819
并登录，然后让我们看一下我们可能想要的参数，那么
and log and then let's take a look at

146
00:04:40,019 --> 00:04:41,680
并登录，然后让我们看一下我们可能想要的参数，那么
which parameters we might want so what

147
00:04:41,879 --> 00:04:43,809
实际上在这三个代码块之间改变了我们
actually changes between these three

148
00:04:44,009 --> 00:04:45,490
实际上在这三个代码块之间改变了我们
blocks of code the values that we

149
00:04:45,689 --> 00:04:47,139
其实就是这样，所以这些成为我们的参数
actually multiplied that's it

150
00:04:47,339 --> 00:04:49,449
其实就是这样，所以这些成为我们的参数
so those become the parameters for our

151
00:04:49,649 --> 00:04:51,999
功能这些医生探针之间实际发生了什么变化
function what actually changes between

152
00:04:52,199 --> 00:04:53,468
功能这些医生探针之间实际发生了什么变化
these doctor probe what needs to be

153
00:04:53,668 --> 00:04:54,968
为执行此功能而指定的功能，让我们继续写
specified for this function to perform

154
00:04:55,168 --> 00:04:56,980
为执行此功能而指定的功能，让我们继续写
its job let's go ahead and write in our

155
00:04:57,180 --> 00:04:58,300
参数，所以我们将要使用两个整数并使用一个
parameters so we're going to be taking

156
00:04:58,500 --> 00:05:00,189
参数，所以我们将要使用两个整数并使用一个
in two integers and be using a really

157
00:05:00,389 --> 00:05:01,569
给他们冷却任何您想要的MV明智的方法，我们将复制并粘贴一个
cool them anything you want that MV

158
00:05:01,769 --> 00:05:03,129
给他们冷却任何您想要的MV明智的方法，我们将复制并粘贴一个
seems sensible we'll copy and paste one

159
00:05:03,329 --> 00:05:04,870
这些功能中的这些块中，这看起来不错，我当然会替换
of these blocks into this function this

160
00:05:05,069 --> 00:05:06,160
这些功能中的这些块中，这看起来不错，我当然会替换
looks pretty good of course I'll replace

161
00:05:06,360 --> 00:05:08,050
三个和两个与我们的参数，以便我们使用参数
three and two with our parameters so

162
00:05:08,250 --> 00:05:09,160
三个和两个与我们的参数，以便我们使用参数
that we're using the parameters we

163
00:05:09,360 --> 00:05:11,050
指定此函数以执行将与之相乘的乘法
specify into this function to perform

164
00:05:11,250 --> 00:05:12,819
指定此函数以执行将与之相乘的乘法
the multiplication against which will

165
00:05:13,019 --> 00:05:14,528
导致时间B在这里倍增，然后我们将记录我们的
cause a times B to get multiplied here

166
00:05:14,728 --> 00:05:15,879
导致时间B在这里倍增，然后我们将记录我们的
and then we're going to be logging our

167
00:05:16,079 --> 00:05:17,889
现在就进行咨询，而不是做那么多次，
results the consult so now instead of

168
00:05:18,089 --> 00:05:20,770
现在就进行咨询，而不是做那么多次，
doing this so many times all I have to

169
00:05:20,970 --> 00:05:24,670
要做的只是用我的参数调用乘法并记录，所以三个和两个用于
do is simply call multiply and log with

170
00:05:24,870 --> 00:05:26,290
要做的只是用我的参数调用乘法并记录，所以三个和两个用于
my parameters so three and two for

171
00:05:26,490 --> 00:05:28,899
例子，然后我们有八个和五个，然后我们有90和45，仅此而已
example and then we have eight and five

172
00:05:29,098 --> 00:05:36,100
例子，然后我们有八个和五个，然后我们有90和45，仅此而已
and then we have 90 and 45 and that's it

173
00:05:36,300 --> 00:05:37,660
看看我可以摆脱所有这些代码，这就是我们最终得到的
look at that I can get rid of all this

174
00:05:37,860 --> 00:05:39,430
看看我可以摆脱所有这些代码，这就是我们最终得到的
code and this is what we end up with a

175
00:05:39,629 --> 00:05:42,040
干净整洁，易于阅读的程序，如果我启动程序，您会看到我们
nice clean and easy to read program if I

176
00:05:42,240 --> 00:05:44,379
干净整洁，易于阅读的程序，如果我启动程序，您会看到我们
launch my program you can see that we

177
00:05:44,579 --> 00:05:46,810
在这里获取正确的值，所以这是一个非常简单的示例，但我认为这是
get the correct values here so this is a

178
00:05:47,009 --> 00:05:48,459
在这里获取正确的值，所以这是一个非常简单的示例，但我认为这是
pretty simple example but I think it's

179
00:05:48,658 --> 00:05:49,600
证明功能真的很重要
effective in demonstrating that

180
00:05:49,800 --> 00:05:50,980
证明功能真的很重要
functions are really really important

181
00:05:51,180 --> 00:05:52,540
您应该旨在将您的代码分成许多功能，但是
you should be aiming to split up your

182
00:05:52,740 --> 00:05:54,699
您应该旨在将您的代码分成许多功能，但是
code into many many functions however

183
00:05:54,899 --> 00:05:56,230
我要强调的一件事是不要太过分，您不需要功能
one thing that I want to stress is don't

184
00:05:56,430 --> 00:05:58,028
我要强调的一件事是不要太过分，您不需要功能
go overboard you don't need a function

185
00:05:58,228 --> 00:06:00,249
对于绝对不会对任何人有利的每一行代码
for absolutely every line of code that's

186
00:06:00,449 --> 00:06:02,649
对于绝对不会对任何人有利的每一行代码
not going to be good for anyone it's

187
00:06:02,848 --> 00:06:04,300
很难维护您的代码将变得凌乱和混乱，并且
going to be hard to maintain your code

188
00:06:04,500 --> 00:06:05,829
很难维护您的代码将变得凌乱和混乱，并且
is going to look messy and cluttered and

189
00:06:06,029 --> 00:06:07,088
每次我们调用一个函数时，这实际上会使您的程序变慢
it's actually going to make your program

190
00:06:07,288 --> 00:06:09,338
每次我们调用一个函数时，这实际上会使您的程序变慢
slower every time we call a function

191
00:06:09,538 --> 00:06:11,588
星号编译器生成一个调用指令，它的基本含义是
asterisk the compiler generates a call

192
00:06:11,788 --> 00:06:13,600
星号编译器生成一个调用指令，它的基本含义是
instruction what it basically means is

193
00:06:13,800 --> 00:06:16,240
在运行的程序中，为了让我们调用需要创建的函数
that in a running program in order for

194
00:06:16,439 --> 00:06:18,310
在运行的程序中，为了让我们调用需要创建的函数
us to call a function we need to create

195
00:06:18,509 --> 00:06:20,290
该函数的整个堆栈框架意味着我们必须推送诸如
the entire stack frame for the function

196
00:06:20,490 --> 00:06:21,730
该函数的整个堆栈框架意味着我们必须推送诸如
meaning we have to push things like the

197
00:06:21,930 --> 00:06:23,259
参数放到堆栈上，我们还必须推送一个称为return的东西
parameters onto the stack we have to

198
00:06:23,459 --> 00:06:24,520
参数放到堆栈上，我们还必须推送一个称为return的东西
also push something called a return

199
00:06:24,720 --> 00:06:26,290
地址到堆栈上，然后我们实际上是跳到另一个
address onto the stack and then what we

200
00:06:26,490 --> 00:06:27,790
地址到堆栈上，然后我们实际上是跳到另一个
do is we actually jump to a different

201
00:06:27,990 --> 00:06:30,338
我们二进制文件的一部分，以便开始执行我们的指令
part of our binary in order to start

202
00:06:30,538 --> 00:06:32,079
我们二进制文件的一部分，以便开始执行我们的指令
executing the instructions from our

203
00:06:32,278 --> 00:06:33,879
函数和我们推动的返回值，我们需要回到我们的位置
function and that return value that we

204
00:06:34,079 --> 00:06:35,439
函数和我们推动的返回值，我们需要回到我们的位置
push we need to get back to where we

205
00:06:35,639 --> 00:06:36,910
最初是在我们调用函数之前，所以这就像跳跃
originally were before we call the

206
00:06:37,110 --> 00:06:38,528
最初是在我们调用函数之前，所以这就像跳跃
function so this is called like jumping

207
00:06:38,728 --> 00:06:40,120
在内存周围以便执行功能指令，以及所有这些
around memory in order to execute

208
00:06:40,319 --> 00:06:41,500
在内存周围以便执行功能指令，以及所有这些
function instructions and all of that

209
00:06:41,699 --> 00:06:43,838
需要时间，所以现在放慢了我们的程序的速度我早些时候说星号的原因
takes time so it slows down our program

210
00:06:44,038 --> 00:06:45,610
需要时间，所以现在放慢了我们的程序的速度我早些时候说星号的原因
now the reason I said asterisk earlier

211
00:06:45,810 --> 00:06:47,170
因为所有这些都是假设编译器决定保留我们的
was because this is all assuming that

212
00:06:47,370 --> 00:06:48,430
因为所有这些都是假设编译器决定保留我们的
the compiler decides to keep our

213
00:06:48,629 --> 00:06:50,019
作为实际功能使用，并且不内联，我们将在后面讨论
function as an actual function and

214
00:06:50,218 --> 00:06:51,730
作为实际功能使用，并且不内联，我们将在后面讨论
doesn't inline it we're going to talk in

215
00:06:51,930 --> 00:06:53,410
深入介绍未来的视频，所以我说这一切的原因是
depth about inlining in a future video

216
00:06:53,610 --> 00:06:54,879
深入介绍未来的视频，所以我说这一切的原因是
so the reason I'm saying all this is

217
00:06:55,079 --> 00:06:56,439
因为您不想仅仅继续创建每个函数
because you don't want to just go ahead

218
00:06:56,639 --> 00:06:58,120
因为您不想仅仅继续创建每个函数
and create a functions absolutely every

219
00:06:58,319 --> 00:06:59,800
这行代码并不荒谬，它需要一点经验
line of code don't be ridiculous about

220
00:07:00,000 --> 00:07:01,060
这行代码并不荒谬，它需要一点经验
it it takes a little bit of experience

221
00:07:01,259 --> 00:07:02,920
实现您需要的功能，但是基本上，如果您看到自己在做什么
to realize what you need a function for

222
00:07:03,120 --> 00:07:04,660
实现您需要的功能，但是基本上，如果您看到自己在做什么
but basically if you see yourself doing

223
00:07:04,860 --> 00:07:05,769
多次执行一项常见任务
a common task

224
00:07:05,968 --> 00:07:08,139
多次执行一项常见任务
multiple times create a function for

225
00:07:08,339 --> 00:07:10,360
函数的主要目的是防止代码重复，我们不会
that the primary point of functions is

226
00:07:10,560 --> 00:07:12,850
函数的主要目的是防止代码重复，我们不会
to prevent code duplication we don't

227
00:07:13,050 --> 00:07:15,240
只想在各处复制和粘贴代码
want to just be copying and pasting code

228
00:07:15,439 --> 00:07:16,420
只想在各处复制和粘贴代码
everywhere

229
00:07:16,620 --> 00:07:18,040
现在，如果我们再回到代码一秒钟，您可能已经注意到了
now if we go back to our code for just a

230
00:07:18,240 --> 00:07:19,480
现在，如果我们再回到代码一秒钟，您可能已经注意到了
second you might have noticed something

231
00:07:19,680 --> 00:07:20,829
这个主函数有点奇怪，它说它是返回值
a little bit odd about this main

232
00:07:21,028 --> 00:07:23,230
这个主函数有点奇怪，它说它是返回值
function it says that it's return value

233
00:07:23,430 --> 00:07:25,569
是int，但是找不到return关键字，我显然
is int however the return keyword is

234
00:07:25,769 --> 00:07:28,180
是int，但是找不到return关键字，我显然
nowhere to be found and I'm obviously

235
00:07:28,379 --> 00:07:30,879
不返回任何东西，所以如果我指定一个返回值，我是否真的需要
not returning anything so if I specify a

236
00:07:31,079 --> 00:07:32,800
不返回任何东西，所以如果我指定一个返回值，我是否真的需要
return value do I actually need to

237
00:07:33,000 --> 00:07:34,778
返回一些内容，让我们继续尝试并在此乘法中什么也不做
return something let's go ahead and try

238
00:07:34,978 --> 00:07:36,189
返回一些内容，让我们继续尝试并在此乘法中什么也不做
and just do nothing in this multiply

239
00:07:36,389 --> 00:07:37,838
功能，我将按Control键F7来编译我的文件
function I'll hit control f7 to compile

240
00:07:38,038 --> 00:07:38,379
功能，我将按Control键F7来编译我的文件
my file

241
00:07:38,579 --> 00:07:39,519
看看这个，我得到一个错误，告诉我乘法必须返回一个
look at this I'm getting an error

242
00:07:39,718 --> 00:07:41,079
看看这个，我得到一个错误，告诉我乘法必须返回一个
telling me that multiply must return a

243
00:07:41,278 --> 00:07:41,680
值，返回类型的函数也是如此
value

244
00:07:41,879 --> 00:07:43,180
值，返回类型的函数也是如此
so do functions with the return type

245
00:07:43,379 --> 00:07:44,980
实际需要返回值的答案是，它们可以执行主要功能
actually need to return values the

246
00:07:45,180 --> 00:07:47,740
实际需要返回值的答案是，它们可以执行主要功能
answer is yes they do the main function

247
00:07:47,939 --> 00:07:49,990
实际上是一个特殊的功能main函数，只有main函数是
is actually a special function the main

248
00:07:50,189 --> 00:07:52,209
实际上是一个特殊的功能main函数，只有main函数是
function and only the main function is

249
00:07:52,408 --> 00:07:54,730
如果不指定返回值，则必须从该类型返回，否则必须返回一个值
exempt from this kind of must return a

250
00:07:54,930 --> 00:07:56,560
如果不指定返回值，则必须从该类型返回，否则必须返回一个值
value if you don't specify a return

251
00:07:56,759 --> 00:07:58,540
值，它将自动假设您返回的是零，因此我们将
value it will automatically assume that

252
00:07:58,740 --> 00:07:59,889
值，它将自动假设您返回的是零，因此我们将
you're returning zero so we'll be

253
00:08:00,089 --> 00:08:01,959
与我写过的文字完全相同，这只是现代CNC的功能，但已丢失
identical to if I had written this this

254
00:08:02,158 --> 00:08:04,088
与我写过的文字完全相同，这只是现代CNC的功能，但已丢失
is just a feature of modern CNC but lost

255
00:08:04,288 --> 00:08:06,100
版本可以使您的代码保持整洁，
versions that lets you just keep your

256
00:08:06,300 --> 00:08:07,718
版本可以使您的代码保持整洁，
code a little bit cleaner and just as

257
00:08:07,918 --> 00:08:09,490
有趣的是，这必须返回一个有价值的东西，实际上只是
fun know that this must return a value

258
00:08:09,689 --> 00:08:11,499
有趣的是，这必须返回一个有价值的东西，实际上只是
thing is actually something that only

259
00:08:11,699 --> 00:08:13,480
在调试模式下应用，在发布模式下编译，在这里您会看到我们
applies in debug mode we compile in

260
00:08:13,680 --> 00:08:15,129
在调试模式下应用，在发布模式下编译，在这里您会看到我们
release mode here you'll see that we

261
00:08:15,329 --> 00:08:16,749
实际上没有得到一个错误，不是说我们在这里做的是
actually don't get an error that's not

262
00:08:16,949 --> 00:08:17,920
实际上没有得到一个错误，不是说我们在这里做的是
to say that what we're doing here is

263
00:08:18,120 --> 00:08:20,050
正确，因为如果我们确实捕获了该返回值，那么
correct because if we actually do

264
00:08:20,250 --> 00:08:21,819
正确，因为如果我们确实捕获了该返回值，那么
capture that return value so to do

265
00:08:22,019 --> 00:08:23,139
我们会得到未定义的行为，就像编译器实际上不会
something we will get undefined behavior

266
00:08:23,338 --> 00:08:24,999
我们会得到未定义的行为，就像编译器实际上不会
it's just as the compiler won't actually

267
00:08:25,199 --> 00:08:25,930
但是在调试模式下通过某些调试对我们大喊
yell at us

268
00:08:26,129 --> 00:08:28,060
但是在调试模式下通过某些调试对我们大喊
however in debug mode with certain debug

269
00:08:28,259 --> 00:08:31,509
启用了编译标志，我们将得到一个错误，它将仅帮助我们调试我们的
compilation flags enabled we will get an

270
00:08:31,709 --> 00:08:33,399
启用了编译标志，我们将得到一个错误，它将仅帮助我们调试我们的
error which will just help us debug our

271
00:08:33,599 --> 00:08:35,349
代码，因为您绝对不应编写函数
code because at no point should you be

272
00:08:35,549 --> 00:08:36,688
代码，因为您绝对不应编写函数
writing a function

273
00:08:36,889 --> 00:08:37,918
它会返回一些东西但是不行，所以差不多
it's going to return something but

274
00:08:38,119 --> 00:08:40,139
它会返回一些东西但是不行，所以差不多
doesn't okay so that's pretty much a

275
00:08:40,339 --> 00:08:41,668
函数的基本介绍函数将非常有用
basic introduction to functions

276
00:08:41,869 --> 00:08:43,708
函数的基本介绍函数将非常有用
functions are really useful I'm going to

277
00:08:43,908 --> 00:08:45,358
将来会编写更多这样的程序，每个程序都是由
be writing a lot more of them in the

278
00:08:45,558 --> 00:08:47,548
将来会编写更多这样的程序，每个程序都是由
future every program is built from a

279
00:08:47,749 --> 00:08:49,438
函数集合，所以如果您不这样做，这将是非常重要的东西
collection of functions so this is

280
00:08:49,639 --> 00:08:50,758
函数集合，所以如果您不这样做，这将是非常重要的东西
really important stuff if you don't

281
00:08:50,958 --> 00:08:52,258
认为您完全了解函数的工作原理不必担心，因为在此过程中
think you fully understand how functions

282
00:08:52,458 --> 00:08:54,118
认为您完全了解函数的工作原理不必担心，因为在此过程中
work don't worry because throughout this

283
00:08:54,318 --> 00:08:55,618
系列我们将要编写许多功能，您将要获得
series we're going to be writing so many

284
00:08:55,818 --> 00:08:57,089
系列我们将要编写许多功能，您将要获得
functions that you're just going to get

285
00:08:57,289 --> 00:08:58,589
习惯了，最好的学习方式当然是练习
used to it and the best way to learn is

286
00:08:58,789 --> 00:09:00,358
习惯了，最好的学习方式当然是练习
of course to practice we also commonly

287
00:09:00,558 --> 00:09:02,578
将函数分解为声明和定义，所以我们通常使用声明
break up functions into declarations and

288
00:09:02,778 --> 00:09:04,558
将函数分解为声明和定义，所以我们通常使用声明
definitions so declarations we usually

289
00:09:04,759 --> 00:09:06,149
存储在头文件中，然后在翻译单元中写入定义
store in header file and then

290
00:09:06,350 --> 00:09:08,068
存储在头文件中，然后在翻译单元中写入定义
definitions we write in translation unit

291
00:09:08,269 --> 00:09:09,779
或CPP文件，所以现在我要制作一个专用于头文件的完整视频
or CPP files so now I'm going to make a

292
00:09:09,980 --> 00:09:11,728
或CPP文件，所以现在我要制作一个专用于头文件的完整视频
whole video dedicated to header files

293
00:09:11,928 --> 00:09:13,468
我们将在其中的头文件中介绍函数声明
and we're going to cover function

294
00:09:13,668 --> 00:09:15,298
我们将在其中的头文件中介绍函数声明
declarations in a header file in that

295
00:09:15,499 --> 00:09:17,308
视频，所以无论如何，我希望大家喜欢这个情节，请点击
video so anyway guys I hope you enjoyed

296
00:09:17,509 --> 00:09:18,959
视频，所以无论如何，我希望大家喜欢这个情节，请点击
this episode if you did please hit the

297
00:09:19,159 --> 00:09:20,399
像按钮一样，别忘了在Twitter和Instagram上关注我，如果您真的
like button don't forget to follow me on

298
00:09:20,600 --> 00:09:22,498
像按钮一样，别忘了在Twitter和Instagram上关注我，如果您真的
Twitter and Instagram and if you really

299
00:09:22,698 --> 00:09:23,758
欣赏这部影片，您想尽早查看未来影片的草稿，以及
enjoy this video and you want to see

300
00:09:23,958 --> 00:09:26,128
欣赏这部影片，您想尽早查看未来影片的草稿，以及
drafts of future videos early as well as

301
00:09:26,328 --> 00:09:27,628
讨论这些视频中的实际内容，您可以在patreon上为我提供支持
discuss what actually goes into these

302
00:09:27,828 --> 00:09:29,728
讨论这些视频中的实际内容，您可以在patreon上为我提供支持
videos you can support me on patreon

303
00:09:29,928 --> 00:09:31,258
链接将在下面的描述中，直到下次再见
link will be in description below but

304
00:09:31,458 --> 00:09:34,269
链接将在下面的描述中，直到下次再见
until next time guys goodbye

305
00:09:34,470 --> 00:09:39,470
[音乐]
[Music]

