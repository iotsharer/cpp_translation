﻿1
00:00:00,000 --> 00:00:01,540
所以，我的名字叫安娜，欢迎回到我说的老板老板系列，所以今天
and so guys my name is Ana and welcome

2
00:00:01,740 --> 00:00:03,728
所以，我的名字叫安娜，欢迎回到我说的老板老板系列，所以今天
back to my say boss boss series so today

3
00:00:03,928 --> 00:00:05,019
我将再谈一点关于内存的知识，特别是我们如何能
I'm gonna be talking a little bit more

4
00:00:05,219 --> 00:00:07,780
我将再谈一点关于内存的知识，特别是我们如何能
about memory and specifically how we can

5
00:00:07,980 --> 00:00:10,450
跟踪分配和内存分配现在关于内存的事情是
track allocation and the allocation of

6
00:00:10,650 --> 00:00:12,519
跟踪分配和内存分配现在关于内存的事情是
memory now the thing about memory is

7
00:00:12,718 --> 00:00:15,250
重要的是我要永远谈论记忆
that it's important I'm gonna keep

8
00:00:15,449 --> 00:00:17,140
重要的是我要永远谈论记忆
talking about memory probably forever

9
00:00:17,339 --> 00:00:18,789
因为这就是计算机的工作方式，计算机与计算机紧密相连
because that's how computers work

10
00:00:18,989 --> 00:00:21,190
因为这就是计算机的工作方式，计算机与计算机紧密相连
computers are very very closely tied in

11
00:00:21,390 --> 00:00:23,079
有内存，如果您只有一个CPU，而您没有任何RAM
with memory if you just have a CPU and

12
00:00:23,278 --> 00:00:25,179
有内存，如果您只有一个CPU，而您没有任何RAM
you don't have any RAM you don't have

13
00:00:25,379 --> 00:00:27,280
任何内存，那么什么都不会发生，因为它是如此
any memory then nothing really is going

14
00:00:27,480 --> 00:00:29,019
任何内存，那么什么都不会发生，因为它是如此
to happen and because it's such a

15
00:00:29,219 --> 00:00:31,239
讨论的热门话题很多人有时会有错误的主意
popular topic for discussion a lot of

16
00:00:31,439 --> 00:00:33,459
讨论的热门话题很多人有时会有错误的主意
people sometimes have the wrong idea

17
00:00:33,659 --> 00:00:36,159
关于记忆，我认为这是由于2020年而变得更加复杂
about memory and I think this is further

18
00:00:36,359 --> 00:00:38,739
关于记忆，我认为这是由于2020年而变得更加复杂
complicated by the fact that it's 2020

19
00:00:38,939 --> 00:00:41,619
我们真的没有像20年这样大的记忆问题
we don't really have that big of an

20
00:00:41,820 --> 00:00:43,570
我们真的没有像20年这样大的记忆问题
issue with memory like we did 20 years

21
00:00:43,770 --> 00:00:44,108
以前我们有GB和GB的
ago

22
00:00:44,308 --> 00:00:46,149
以前我们有GB和GB的
we have gigabytes and gigabytes of

23
00:00:46,350 --> 00:00:48,128
内存，我的计算机中有32 GB的内存，大多数笔记本电脑都带有8 GB的内存
memory I have like 32 gigabytes in my

24
00:00:48,329 --> 00:00:50,500
内存，我的计算机中有32 GB的内存，大多数笔记本电脑都带有8 GB的内存
computer most laptops come with like 8

25
00:00:50,700 --> 00:00:53,409
或16 GB的内存，编写这样的C ++程序有点困难
or 16 gigabytes of memory it's kind of

26
00:00:53,609 --> 00:00:55,119
或16 GB的内存，编写这样的C ++程序有点困难
difficult to write a C++ program that's

27
00:00:55,320 --> 00:00:57,608
要使用更多或更多的数量，我的意思不是那么困难，并且绝对取决于
gonna use that amount or more I mean not

28
00:00:57,808 --> 00:00:59,378
要使用更多或更多的数量，我的意思不是那么困难，并且绝对取决于
that difficult and definitely depends on

29
00:00:59,579 --> 00:01:02,829
您在做什么，但仍然很高兴世界不会使用太多内存
what you're doing but it's still hello

30
00:01:03,030 --> 00:01:04,659
您在做什么，但仍然很高兴世界不会使用太多内存
world is not gonna use too much memory

31
00:01:04,859 --> 00:01:06,399
因此，无论如何，我的意思是有时了解何时
so anyway my point with all of this is

32
00:01:06,599 --> 00:01:08,799
因此，无论如何，我的意思是有时了解何时
that sometimes it's useful to know when

33
00:01:09,000 --> 00:01:10,480
您的程序专门在堆上分配内存，当然是
your program allocates memory

34
00:01:10,680 --> 00:01:12,219
您的程序专门在堆上分配内存，当然是
specifically on the heap of course is

35
00:01:12,420 --> 00:01:13,629
如果您知道程序在哪里分配内存，我们在说什么
what we're talking about if you know

36
00:01:13,829 --> 00:01:15,789
如果您知道程序在哪里分配内存，我们在说什么
where your program allocates memory you

37
00:01:15,989 --> 00:01:18,129
可以减少它，因此有可能优化您的
can potentially work to reduce it

38
00:01:18,329 --> 00:01:20,918
可以减少它，因此有可能优化您的
therefore potentially optimizing your

39
00:01:21,118 --> 00:01:23,349
程序并使其运行更快，因为不是在堆上分配内存
program and making it run faster because

40
00:01:23,549 --> 00:01:25,750
程序并使其运行更快，因为不是在堆上分配内存
allocating memory on the heap is is not

41
00:01:25,950 --> 00:01:27,340
最重要的是，尤其是在性能关键的代码中
the best thing to do especially in

42
00:01:27,540 --> 00:01:29,439
最重要的是，尤其是在性能关键的代码中
performance critical code on top of that

43
00:01:29,640 --> 00:01:31,569
能够查看分配内存的位置还可以帮助您发现
being able to see where memory is being

44
00:01:31,769 --> 00:01:33,878
能够查看分配内存的位置还可以帮助您发现
allocated can also help you discover a

45
00:01:34,078 --> 00:01:35,709
即使您编写了所有代码，也了解程序的工作原理
little bit more about how your program

46
00:01:35,909 --> 00:01:38,439
即使您编写了所有代码，也了解程序的工作原理
works even if you wrote all of the code

47
00:01:38,640 --> 00:01:40,539
您当前正在运行，如果我迷路了，我们就想忘记这一点
that you're currently running and we

48
00:01:40,739 --> 00:01:42,278
您当前正在运行，如果我迷路了，我们就想忘记这一点
just like forget about this if I lost on

49
00:01:42,478 --> 00:01:43,869
LSU库可以保存您在生命项目中可能看到的所有代码
a library LSU saves all of your code

50
00:01:44,069 --> 00:01:46,238
LSU库可以保存您在生命项目中可能看到的所有代码
looking over a life project that you may

51
00:01:46,438 --> 00:01:47,829
已经工作了一段时间，并且能够看到好吧，你知道我的
have been working on for a while and

52
00:01:48,030 --> 00:01:50,500
已经工作了一段时间，并且能够看到好吧，你知道我的
being able to see okay you know my

53
00:01:50,700 --> 00:01:52,028
程序在这里分配内存我忘了那件事
program allocates memory here I forgot

54
00:01:52,228 --> 00:01:53,769
程序在这里分配内存我忘了那件事
about that well that was something that

55
00:01:53,969 --> 00:01:56,289
是很久以前的有用信息
was from a long time ago that's useful

56
00:01:56,489 --> 00:01:57,079
是很久以前的有用信息
information

57
00:01:57,280 --> 00:01:58,579
我们将讨论如何才能准确了解每个分配的来源
and we're going to talk about how we can

58
00:01:58,780 --> 00:02:00,950
我们将讨论如何才能准确了解每个分配的来源
see exactly where each allocation comes

59
00:02:01,150 --> 00:02:02,989
并跟踪应用程序中内存的整体使用情况
from as well as track the overall usage

60
00:02:03,189 --> 00:02:05,808
并跟踪应用程序中内存的整体使用情况
of memory inside our application by just

61
00:02:06,009 --> 00:02:07,730
编写代码以做到不依赖任何其他工具
writing the code to do that not relying

62
00:02:07,930 --> 00:02:09,618
编写代码以做到不依赖任何其他工具
on any other tools whatsoever

63
00:02:09,818 --> 00:02:11,030
我还要感谢Skillshare为您提供的视频赞助
and I also want to thank Skillshare for

64
00:02:11,229 --> 00:02:12,618
我还要感谢Skillshare为您提供的视频赞助
sponsoring this video for those of you

65
00:02:12,818 --> 00:02:14,570
谁不知道Shey是什么技能？Scotia是一个在线学习社区，
who don't know what skill Shey is Scotia

66
00:02:14,770 --> 00:02:16,250
谁不知道Shey是什么技能？Scotia是一个在线学习社区，
is an online learning community for

67
00:02:16,449 --> 00:02:17,660
数以百万计的广告素材聚集在一起，迈出下一步的广告素材
creatives where millions come together

68
00:02:17,860 --> 00:02:19,670
数以百万计的广告素材聚集在一起，迈出下一步的广告素材
to take the next step in their creative

69
00:02:19,870 --> 00:02:21,920
旅程Skillshare提供了成千上万的启发性课程，用于创意和
journey Skillshare offers thousands of

70
00:02:22,120 --> 00:02:23,630
旅程Skillshare提供了成千上万的启发性课程，用于创意和
inspiring classes for creative and

71
00:02:23,830 --> 00:02:25,730
好奇的人，对包括插画设计摄影视频在内的话题
curious people on topics including

72
00:02:25,930 --> 00:02:28,070
好奇的人，对包括插画设计摄影视频在内的话题
illustration design photography video

73
00:02:28,270 --> 00:02:30,590
自由职业者，他们平台上还有很多东西可以帮助您
freelancing and more they've got tons of

74
00:02:30,789 --> 00:02:32,480
自由职业者，他们平台上还有很多东西可以帮助您
stuff on their platform to help you

75
00:02:32,680 --> 00:02:34,700
学习一种新技能，而我最喜欢的技能共享功能之一就是
learn a new skill and one of my favorite

76
00:02:34,900 --> 00:02:36,530
学习一种新技能，而我最喜欢的技能共享功能之一就是
features about skill share is that their

77
00:02:36,729 --> 00:02:38,870
视频非常简洁，这意味着我可以按照自己的时间表进行调整，
videos are quite concise which means

78
00:02:39,069 --> 00:02:41,390
视频非常简洁，这意味着我可以按照自己的时间表进行调整，
that I can fit them into my schedule and

79
00:02:41,590 --> 00:02:43,250
我有时间实际使用它们来学习新知识
I have time to actually use them to

80
00:02:43,449 --> 00:02:44,210
我有时间实际使用它们来学习新知识
learn something new

81
00:02:44,409 --> 00:02:46,160
我特别喜欢他们的很多插图课，因为我一直
a lot of their illustration classes I

82
00:02:46,360 --> 00:02:48,080
我特别喜欢他们的很多插图课，因为我一直
particularly like because I'm always

83
00:02:48,280 --> 00:02:50,630
有兴趣提高我的艺术水平，他们也有很多课程可以帮助您
interested in improving my art and they

84
00:02:50,830 --> 00:02:52,160
有兴趣提高我的艺术水平，他们也有很多课程可以帮助您
also have plenty of classes to get you

85
00:02:52,360 --> 00:02:54,410
开始制作视频，使您一年赚二十二十
started on how to make videos make

86
00:02:54,610 --> 00:02:56,000
开始制作视频，使您一年赚二十二十
twenty twenty a year in which you

87
00:02:56,199 --> 00:02:57,770
探索新技能可加深现有激情，并迷失于创造力
explore new skills deepen existing

88
00:02:57,969 --> 00:03:00,259
探索新技能可加深现有激情，并迷失于创造力
passions and get lost in creativity with

89
00:03:00,459 --> 00:03:02,360
学校交易会在线课程，每年订阅费用低于10美元
school fairs online classes and with an

90
00:03:02,560 --> 00:03:04,189
学校交易会在线课程，每年订阅费用低于10美元
annual subscription of less than $10 a

91
00:03:04,389 --> 00:03:06,319
一个月的技能分享是完成所有这些工作的好方法，而最好的部分是
month Skillshare is a great way to do

92
00:03:06,519 --> 00:03:08,210
一个月的技能分享是完成所有这些工作的好方法，而最好的部分是
all of that and the best part is the

93
00:03:08,409 --> 00:03:10,310
技能共享可为使用该链接中的链接进行注册的前500人
skill share is offering the first 500

94
00:03:10,509 --> 00:03:12,319
技能共享可为使用该链接中的链接进行注册的前500人
people who sign up using the link in the

95
00:03:12,519 --> 00:03:14,689
以下介绍了三个月的技能分享溢价，因此请继续执行
description below to three months of

96
00:03:14,889 --> 00:03:16,700
以下介绍了三个月的技能分享溢价，因此请继续执行
skill share premium so go ahead and do

97
00:03:16,900 --> 00:03:18,349
尽快感谢您，再次感谢Skillshare的赞助
that as soon as possible and thank you

98
00:03:18,549 --> 00:03:19,910
尽快感谢您，再次感谢Skillshare的赞助
as always to Skillshare for sponsoring

99
00:03:20,110 --> 00:03:21,770
这部影片我也要感谢大家订阅和观看我的
this video I also want to thank all of

100
00:03:21,969 --> 00:03:23,420
这部影片我也要感谢大家订阅和观看我的
you for subscribing and watching my

101
00:03:23,620 --> 00:03:25,189
视频，因为我们在该频道吸引了20万订阅者
videos because we've hit 200 thousand

102
00:03:25,389 --> 00:03:26,599
视频，因为我们在该频道吸引了20万订阅者
subscribers on the channel which is

103
00:03:26,799 --> 00:03:28,550
我认为我要做的一个很酷的里程碑是做一些质量检查
quite a cool milestone what I thought I

104
00:03:28,750 --> 00:03:30,740
我认为我要做的一个很酷的里程碑是做一些质量检查
would do for that is do a bit of a QA

105
00:03:30,939 --> 00:03:35,270
我有五年没有做过问答视频
I have not done a Q&A video in like five

106
00:03:35,469 --> 00:03:35,960
我有五年没有做过问答视频
years

107
00:03:36,159 --> 00:03:37,969
可能是这样，如果你们对我有疑问，请在评论中删除
probably so if you guys have questions

108
00:03:38,169 --> 00:03:39,920
可能是这样，如果你们对我有疑问，请在评论中删除
for me then drop them in the comment

109
00:03:40,120 --> 00:03:41,120
下面的部分将介绍该视频的评论，以了解我
section below I'll be looking at the

110
00:03:41,319 --> 00:03:42,620
下面的部分将介绍该视频的评论，以了解我
comments of this video to see what I

111
00:03:42,819 --> 00:03:45,439
应该包含在“问答”中，请不要要求我修复您的卡，让我们开始吧
should include in the Q&A please don't

112
00:03:45,639 --> 00:03:47,719
应该包含在“问答”中，请不要要求我修复您的卡，让我们开始吧
ask me to fix your card now let's dive

113
00:03:47,919 --> 00:03:49,580
放入卡片中，看看您如何更清楚地知道在哪里
into some card and take a look at how

114
00:03:49,780 --> 00:03:51,950
放入卡片中，看看您如何更清楚地知道在哪里
exactly you can be more aware of where

115
00:03:52,150 --> 00:03:54,050
您正在分配内存，所以在这里我们有一个完全空白的C ++程序
you're allocating memory so here we have

116
00:03:54,250 --> 00:03:56,240
您正在分配内存，所以在这里我们有一个完全空白的C ++程序
a completely blank C++ program I'm gonna

117
00:03:56,439 --> 00:03:58,670
以此为基础向大家展示所有这些东西
use this as the foundation for showing

118
00:03:58,870 --> 00:04:01,099
以此为基础向大家展示所有这些东西
you guys all of this stuff keep in mind

119
00:04:01,299 --> 00:04:03,500
虽然总的来说，我今天要在这里向您展示的所有内容
though that in general everything I'm

120
00:04:03,699 --> 00:04:05,140
虽然总的来说，我今天要在这里向您展示的所有内容
going to show you here today you can

121
00:04:05,340 --> 00:04:07,179
轻松地插入您现有的应用程序中，就像我
easily plug into an existing application

122
00:04:07,378 --> 00:04:08,469
轻松地插入您现有的应用程序中，就像我
of yours that's kind of the way that I

123
00:04:08,669 --> 00:04:11,170
我打算让它发生，也许将来我们会看看
am intending for it to happen maybe in

124
00:04:11,370 --> 00:04:12,340
我打算让它发生，也许将来我们会看看
the future we'll take a look at this

125
00:04:12,539 --> 00:04:14,740
在一个真实的项目中，但现在我要剥离所有这些
inside a real project but for now I'm

126
00:04:14,939 --> 00:04:16,449
在一个真实的项目中，但现在我要剥离所有这些
just gonna strip back all of that kind

127
00:04:16,649 --> 00:04:18,249
精打细算所有根本不相关的额外内容，我们
of craft all of that extra stuff that

128
00:04:18,449 --> 00:04:20,650
精打细算所有根本不相关的额外内容，我们
isn't really relevant at all and we're

129
00:04:20,850 --> 00:04:22,629
要看一下核心基础知识，我要做的第一件事就是写一个
gonna take a look at the core basics the

130
00:04:22,829 --> 00:04:24,340
要看一下核心基础知识，我要做的第一件事就是写一个
first thing I'm gonna do here is write a

131
00:04:24,540 --> 00:04:26,290
小物体，因为这可能是一个很好的例子
little object because this is probably

132
00:04:26,490 --> 00:04:28,480
小物体，因为这可能是一个很好的例子
going to be a good example of a

133
00:04:28,680 --> 00:04:30,430
实际用例中，我可能有一个对象，让我们说它包含一个XY
real-world use case I might have an

134
00:04:30,629 --> 00:04:33,310
实际用例中，我可能有一个对象，让我们说它包含一个XY
object let's just say it contains an X Y

135
00:04:33,509 --> 00:04:35,468
Z现在如果在堆栈上创建三个整数，我可能会
Z we have three integers in here now if

136
00:04:35,668 --> 00:04:36,790
Z现在如果在堆栈上创建三个整数，我可能会
I create it on the stack I'll probably

137
00:04:36,990 --> 00:04:38,170
最终得到这样的东西，但是如果我在堆上创建这个
end up with something like this

138
00:04:38,370 --> 00:04:40,810
最终得到这样的东西，但是如果我在堆上创建这个
however if I create this on the heap by

139
00:04:41,009 --> 00:04:43,329
然后调用一个新对象，这当然会导致堆分配和
calling a new object then of course that

140
00:04:43,529 --> 00:04:45,310
然后调用一个新对象，这当然会导致堆分配和
will result in a heap allocation and the

141
00:04:45,509 --> 00:04:47,170
今天这部影片的重点是我们如何才能像这样
point of this video today is how can we

142
00:04:47,370 --> 00:04:49,270
今天这部影片的重点是我们如何才能像这样
detect things like this the way we're

143
00:04:49,470 --> 00:04:51,460
通过覆盖新的运算符来做到这一点，特别是
gonna do that is by overriding the new

144
00:04:51,660 --> 00:04:53,170
通过覆盖新的运算符来做到这一点，特别是
operator and specifically we're

145
00:04:53,370 --> 00:04:55,569
全局重写此运算符，以做到这一点，我们将输入void指针
overriding this operator globally so to

146
00:04:55,769 --> 00:04:57,189
全局重写此运算符，以做到这一点，我们将输入void指针
do that we'll type in void pointer

147
00:04:57,389 --> 00:05:00,730
运算符裸色，然后尺寸t叹气，所以这是新运算符this
operator nude and then size t sighs so

148
00:05:00,930 --> 00:05:03,100
运算符裸色，然后尺寸t叹气，所以这是新运算符this
what this is is the new operator this

149
00:05:03,300 --> 00:05:05,800
这里的new关键字实际上是一个具有特定大小的函数
new keyword here is actually a function

150
00:05:06,000 --> 00:05:08,410
这里的new关键字实际上是一个具有特定大小的函数
that gets called with a particular size

151
00:05:08,610 --> 00:05:10,150
以及可能还有其他参数，通过在此处编写此代码，我们是在说
and potentially other arguments as well

152
00:05:10,350 --> 00:05:12,310
以及可能还有其他参数，通过在此处编写此代码，我们是在说
by writing this code here we're saying

153
00:05:12,509 --> 00:05:14,319
不要使用标准库中的new运算符使用此运算符
do not use the operator new that is in

154
00:05:14,519 --> 00:05:16,270
不要使用标准库中的new运算符使用此运算符
the standard library use this one

155
00:05:16,470 --> 00:05:18,278
相反，链接器实际上将链接此函数，而不是现在
instead the linker will actually link in

156
00:05:18,478 --> 00:05:20,710
相反，链接器实际上将链接此函数，而不是现在
this function instead now of course it's

157
00:05:20,910 --> 00:05:22,660
在这里实际分配内存很好，因为这就是您的重点
good to actually allocate memory here

158
00:05:22,860 --> 00:05:24,189
在这里实际分配内存很好，因为这就是您的重点
because that's the point of this your

159
00:05:24,389 --> 00:05:25,810
如果您不这样做，则程序可能无法正常工作
program probably won't work if you don't

160
00:05:26,009 --> 00:05:28,180
如果您不这样做，则程序可能无法正常工作
of course you could retrieve the memory

161
00:05:28,379 --> 00:05:29,740
从该函数的任何地方返回一个空指针
from anywhere the point of this function

162
00:05:29,939 --> 00:05:31,960
从该函数的任何地方返回一个空指针
is to return a void pointer which is

163
00:05:32,160 --> 00:05:33,850
只是一个内存地址，所以因为我们真的不想影响行为
just a memory address so because we

164
00:05:34,050 --> 00:05:35,740
只是一个内存地址，所以因为我们真的不想影响行为
don't really want to affect the behavior

165
00:05:35,939 --> 00:05:37,360
在我们的程序中，我们只需键入返回的malloc大小，那么它将做什么
of our program we'll simply type in

166
00:05:37,560 --> 00:05:39,670
在我们的程序中，我们只需键入返回的malloc大小，那么它将做什么
return malloc size so what does it'll do

167
00:05:39,870 --> 00:05:42,310
为我们分配了适当的内存并返回一个指针
is allocate the appropriate amount of

168
00:05:42,509 --> 00:05:44,439
为我们分配了适当的内存并返回一个指针
memory for us and return a pointer to

169
00:05:44,639 --> 00:05:46,600
该内存，因此此更改是我运行时所做的唯一更改
that memory so with this change being

170
00:05:46,800 --> 00:05:48,490
该内存，因此此更改是我运行时所做的唯一更改
the only change that I've made if we run

171
00:05:48,689 --> 00:05:50,528
通常这不会改变，但是这个新关键字现在会一直运行
this normally nothing will change but

172
00:05:50,728 --> 00:05:52,509
通常这不会改变，但是这个新关键字现在会一直运行
this new keyword will now run through

173
00:05:52,709 --> 00:05:54,399
这个功能非常好，因为我可以轻松输入
this function which is quite nice

174
00:05:54,598 --> 00:05:56,528
这个功能非常好，因为我可以轻松输入
because I could easily type in something

175
00:05:56,728 --> 00:06:02,649
例如分配大小字节，现在它将实际打印到
like allocating size bytes for example

176
00:06:02,848 --> 00:06:04,270
例如分配大小字节，现在它将实际打印到
and now it will actually print that to

177
00:06:04,470 --> 00:06:05,889
控制台，也许另一个好处是只需编写类似
the console and perhaps the other

178
00:06:06,089 --> 00:06:07,870
控制台，也许另一个好处是只需编写类似
benefit to simply writing something like

179
00:06:08,069 --> 00:06:10,088
这是因为我现在可以轻松在此处粘贴断点，如果运行此
this is that I can now easily stick a

180
00:06:10,288 --> 00:06:12,278
这是因为我现在可以轻松在此处粘贴断点，如果运行此
breakpoint here and if I run this

181
00:06:12,478 --> 00:06:14,290
程序，您会看到我的程序在此行中断，我
program you'll see that my program

182
00:06:14,490 --> 00:06:16,160
程序，您会看到我的程序在此行中断，我
breaks on this line and I

183
00:06:16,360 --> 00:06:17,809
并查看调用堆栈，我可以看到它来自此行
and look at the call stack and I can see

184
00:06:18,009 --> 00:06:20,239
并查看调用堆栈，我可以看到它来自此行
that okay it's coming from this line of

185
00:06:20,439 --> 00:06:22,399
当然，在这种情况下，现在的代码非常明显，但是如果我们使
code now of course in this case that's

186
00:06:22,598 --> 00:06:24,920
当然，在这种情况下，现在的代码非常明显，但是如果我们使
super obvious but what if we made

187
00:06:25,120 --> 00:06:26,809
像字符串这样的东西，所以我应该说我们现在具有字符串文化
something like a string so I should say

188
00:06:27,009 --> 00:06:28,879
像字符串这样的东西，所以我应该说我们现在具有字符串文化
we have a string culture now now this is

189
00:06:29,079 --> 00:06:30,800
一个小的字符串，因此它不会在堆上分配内存来存储这些
a small string so it weren't allocate

190
00:06:31,000 --> 00:06:32,239
一个小的字符串，因此它不会在堆上分配内存来存储这些
memory on the heap to store these

191
00:06:32,439 --> 00:06:34,249
字符，但是我们处于调试模式，因此如果我们将其保留在内存中，
characters however we are in debug mode

192
00:06:34,449 --> 00:06:36,679
字符，但是我们处于调试模式，因此如果我们将其保留在内存中，
so it still will outcast in memory if we

193
00:06:36,879 --> 00:06:38,660
现在启动它，您可以看到此分配来自此行
launch this now you can see that this

194
00:06:38,860 --> 00:06:41,119
现在启动它，您可以看到此分配来自此行
allocation is coming from this line of

195
00:06:41,319 --> 00:06:43,040
代码，我们实际上可以通过字符串在煤袋中一直跟踪
code and we can actually track that all

196
00:06:43,240 --> 00:06:45,050
代码，我们实际上可以通过字符串在煤袋中一直跟踪
the way in the coalsack through string

197
00:06:45,250 --> 00:06:48,379
并说好吧，我们在这里将运算符称为new，并分配8个字节的
and say that okay we call operator new

198
00:06:48,579 --> 00:06:50,899
并说好吧，我们在这里将运算符称为new，并分配8个字节的
here and we allocate eight bytes of

199
00:06:51,098 --> 00:06:53,119
内存，这也是您可以看到我们的控制台反映的内容，以防万一
memory which is also you can see what

200
00:06:53,319 --> 00:06:55,639
内存，这也是您可以看到我们的控制台反映的内容，以防万一
our console reflects just in case it's

201
00:06:55,839 --> 00:06:58,218
并非100％清楚我还想指出，这不仅会影响到您
not 100% clear I also want to point out

202
00:06:58,418 --> 00:07:00,170
并非100％清楚我还想指出，这不仅会影响到您
that this does not just affect you

203
00:07:00,370 --> 00:07:02,778
如果您使用智能指针，则明确调用您，例如，如果这是一个
explicitly calling you if you use smart

204
00:07:02,978 --> 00:07:05,420
如果您使用智能指针，则明确调用您，例如，如果这是一个
pointers so for example if this was a

205
00:07:05,620 --> 00:07:08,389
唯一的指针，然后让我们对其进行更改以使我成为对象
unique pointer and then let's change

206
00:07:08,589 --> 00:07:12,739
唯一的指针，然后让我们对其进行更改以使我成为对象
this to make you me with object

207
00:07:12,939 --> 00:07:16,249
在这里寻址清除内存，然后显然内存仍然会得到
addressing clear memory up here then

208
00:07:16,449 --> 00:07:18,019
在这里寻址清除内存，然后显然内存仍然会得到
obviously memory will still get

209
00:07:18,218 --> 00:07:20,660
如果我运行此代码，则分配在这里，将跳过这里的字符串到下一个
allocated here if I run this code will

210
00:07:20,860 --> 00:07:22,670
如果我运行此代码，则分配在这里，将跳过这里的字符串到下一个
skip past the string here to the next

211
00:07:22,870 --> 00:07:24,110
分配，您可以看到这是在make unique内部发生的，因为
allocation and you can see that this

212
00:07:24,310 --> 00:07:26,300
分配，您可以看到这是在make unique内部发生的，因为
happens from within make unique because

213
00:07:26,500 --> 00:07:28,759
当然，唯一的当然是通过调用new来在堆上创建对象的
unique actually of course creates the

214
00:07:28,959 --> 00:07:30,769
当然，唯一的当然是通过调用new来在堆上创建对象的
object on the heap by calling new so

215
00:07:30,968 --> 00:07:32,778
希望您已经知道这有多大用处了，可以贴断点
hopefully you can already see how useful

216
00:07:32,978 --> 00:07:34,550
希望您已经知道这有多大用处了，可以贴断点
this is you can stick a breakpoint

217
00:07:34,750 --> 00:07:36,709
在该运算符的新函数中并精确追溯到那些内存的位置
inside that operator new function and

218
00:07:36,908 --> 00:07:39,139
在该运算符的新函数中并精确追溯到那些内存的位置
trace back exactly where those memory

219
00:07:39,338 --> 00:07:40,999
分配来自另一个超级好的例子
allocations are coming from another

220
00:07:41,199 --> 00:07:43,100
分配来自另一个超级好的例子
really good example where this is super

221
00:07:43,300 --> 00:07:44,809
有用的是，如果您正在开发游戏引擎或游戏时
useful is if you're working on a game

222
00:07:45,009 --> 00:07:47,329
有用的是，如果您正在开发游戏引擎或游戏时
engine or a game when the games actually

223
00:07:47,528 --> 00:07:49,459
在渲染帧时运行，比如说加载完成
running when you're rendering frames

224
00:07:49,658 --> 00:07:51,139
在渲染帧时运行，比如说加载完成
like let's just say you finished loading

225
00:07:51,338 --> 00:07:52,879
您完成了所有的初始化工作，就在演奏中
you finished initialization all of that

226
00:07:53,079 --> 00:07:54,588
您完成了所有的初始化工作，就在演奏中
you're in your level you're just playing

227
00:07:54,788 --> 00:07:57,379
您要逐帧进行游戏，以最大程度地减少分配数量
your game frame to frame you want to

228
00:07:57,579 --> 00:07:59,059
您要逐帧进行游戏，以最大程度地减少分配数量
minimize the number of allocations you

229
00:07:59,259 --> 00:08:01,069
因为它们会对性能产生重大影响，所以
have because they that can have a quite

230
00:08:01,269 --> 00:08:03,079
因为它们会对性能产生重大影响，所以
significant performance impact so what

231
00:08:03,278 --> 00:08:04,910
你可以做，而我过去所做的就是你可以坚持一个断点
you can do and what I have done in the

232
00:08:05,110 --> 00:08:06,739
你可以做，而我过去所做的就是你可以坚持一个断点
past is you can just stick a breakpoint

233
00:08:06,939 --> 00:08:09,468
在游戏循环实际运行时在该新功能内部运行
inside that operating new function while

234
00:08:09,668 --> 00:08:11,360
在游戏循环实际运行时在该新功能内部运行
your game loop is actually running and

235
00:08:11,560 --> 00:08:13,639
精确跟踪那些内存分配从帧到哪里的位置
kind of trace exactly where those memory

236
00:08:13,838 --> 00:08:15,588
精确跟踪那些内存分配从帧到哪里的位置
allocations are coming from frame to

237
00:08:15,788 --> 00:08:17,389
框架，老实说，我可能会像字符串操作一样
frame and to be honest a lot of them I'm

238
00:08:17,588 --> 00:08:18,920
框架，老实说，我可能会像字符串操作一样
probably gonna be like string operations

239
00:08:19,120 --> 00:08:20,718
诸如此类的东西，但有时您可以为
and stuff like that but sometimes you

240
00:08:20,918 --> 00:08:23,028
诸如此类的东西，但有时您可以为
can you can make smarter decisions for

241
00:08:23,228 --> 00:08:24,550
例如，也许您有一个不断调整大小的矢量，也许您
example maybe you have a vector

242
00:08:24,750 --> 00:08:26,259
例如，也许您有一个不断调整大小的矢量，也许您
that keeps getting all resized maybe you

243
00:08:26,459 --> 00:08:28,420
可以提前定义该尺寸或使用不完全相同的尺寸
can define that size ahead of time or

244
00:08:28,620 --> 00:08:29,470
可以提前定义该尺寸或使用不完全相同的尺寸
use something that doesn't quite

245
00:08:29,670 --> 00:08:31,870
分配尽可能多的内存，或者您可以编写一个内存竞技场或类似的内容
allocate as much memory or maybe you can

246
00:08:32,070 --> 00:08:33,968
分配尽可能多的内存，或者您可以编写一个内存竞技场或类似的内容
write a memory arena or something like

247
00:08:34,168 --> 00:08:37,958
给定系统或操作或游戏循环中的某些内容，以便您
that for a given system or operation or

248
00:08:38,158 --> 00:08:39,699
给定系统或操作或游戏循环中的某些内容，以便您
something in your game loop so that you

249
00:08:39,899 --> 00:08:41,919
可以减少实际的内存分配，而这些内存分配总是会碰到堆
can reduce actual memory allocations

250
00:08:42,120 --> 00:08:43,539
可以减少实际的内存分配，而这些内存分配总是会碰到堆
that keep hitting the heap it's all

251
00:08:43,740 --> 00:08:45,490
非常有用的东西，如果我们回到我们的这个小例子，我们也可以
seriously useful stuff if we go back to

252
00:08:45,690 --> 00:08:47,589
非常有用的东西，如果我们回到我们的这个小例子，我们也可以
this little example of ours we can also

253
00:08:47,789 --> 00:08:49,899
编写匹配的删除操作符，看起来就像这样，我们有操作符
write the matching delete operator which

254
00:08:50,100 --> 00:08:51,789
编写匹配的删除操作符，看起来就像这样，我们有操作符
just looks like this we have operator

255
00:08:51,990 --> 00:08:53,979
先删除，然后再指向我们要删除的内存的指针，然后我们可以
delete and then a pointer to the memory

256
00:08:54,179 --> 00:08:55,569
先删除，然后再指向我们要删除的内存的指针，然后我们可以
that we want to delete and what we can

257
00:08:55,769 --> 00:08:58,539
这里要做的只是输入空闲内存，如果
do here is just type in free memory and

258
00:08:58,740 --> 00:09:01,029
这里要做的只是输入空闲内存，如果
that's really all there is to it now if

259
00:09:01,230 --> 00:09:03,099
我们在此处放置一个断点，也许我们可以使用这个唯一的指针并将其放入
we put a breakpoint here and we maybe

260
00:09:03,299 --> 00:09:06,069
我们在此处放置一个断点，也许我们可以使用这个唯一的指针并将其放入
take this unique pointer and put it into

261
00:09:06,269 --> 00:09:08,079
这里应该看到的一个小范围是该运算符的删除被称为
a little scope here what you should see

262
00:09:08,279 --> 00:09:10,179
这里应该看到的一个小范围是该运算符的删除被称为
is this operator delete being called

263
00:09:10,379 --> 00:09:12,459
从这个主要功能中删除唯一的指针后，您可以
from this main function after this

264
00:09:12,659 --> 00:09:14,199
从这个主要功能中删除唯一的指针后，您可以
unique pointer is destroyed and you can

265
00:09:14,399 --> 00:09:15,849
看到这是此唯一指针的实际析构函数，即
see that is the actual destructor of

266
00:09:16,049 --> 00:09:17,439
看到这是此唯一指针的实际析构函数，即
this unique pointer here that is

267
00:09:17,639 --> 00:09:19,839
如果我们回到这里，现在实际上删除我们的基础原始指针
actually deleting our underlying raw

268
00:09:20,039 --> 00:09:21,429
如果我们回到这里，现在实际上删除我们的基础原始指针
pointer now if we go back here you'll

269
00:09:21,629 --> 00:09:22,569
了解到，这当然并不包含有关
learn is that this of course doesn't

270
00:09:22,769 --> 00:09:24,519
了解到，这当然并不包含有关
actually contain information about the

271
00:09:24,720 --> 00:09:26,289
尺寸，所以我们不能真正打印出这样的东西，我是说我们可以
size so we can't really print anything

272
00:09:26,490 --> 00:09:28,269
尺寸，所以我们不能真正打印出这样的东西，我是说我们可以
like this well I mean we can we just

273
00:09:28,470 --> 00:09:30,549
必须添加该参数，因为在核心上，此删除运算符不会
have to add that parameter in because at

274
00:09:30,750 --> 00:09:31,839
必须添加该参数，因为在核心上，此删除运算符不会
the core this delete operator doesn't

275
00:09:32,039 --> 00:09:33,879
确实需要尺寸信息，但是您可以选择通过以下方式获取该信息
really need size information but you can

276
00:09:34,080 --> 00:09:35,829
确实需要尺寸信息，但是您可以选择通过以下方式获取该信息
optionally obtain that information by

277
00:09:36,029 --> 00:09:37,809
只是覆盖这个特定的函数签名，所以现在让我们看一下
just overriding this specific function

278
00:09:38,009 --> 00:09:39,819
只是覆盖这个特定的函数签名，所以现在让我们看一下
signature so now if we take a look at

279
00:09:40,019 --> 00:09:41,379
这个，我们让我们的程序自然运行，确保我实际上
this and we just let our program run

280
00:09:41,580 --> 00:09:43,629
这个，我们让我们的程序自然运行，确保我实际上
naturally making sure that I actually

281
00:09:43,830 --> 00:09:45,969
写一些类似释放的内容，您可以看到现在我们分配了12个字节，释放了12个字节
write something like freeing you can see

282
00:09:46,169 --> 00:09:47,949
写一些类似释放的内容，您可以看到现在我们分配了12个字节，释放了12个字节
that now we allocate 12 bytes we free 12

283
00:09:48,149 --> 00:09:49,659
个字节，我们再分配8个字节，释放8个字节，因此现在使用这两个字节
bytes we allocate another 8 bytes and we

284
00:09:49,860 --> 00:09:52,779
个字节，我们再分配8个字节，释放8个字节，因此现在使用这两个字节
free 8 bytes so now using these two

285
00:09:52,980 --> 00:09:54,939
我们可以创建一些可以维护的分配跟踪器的功能
functions we can create some sort of

286
00:09:55,139 --> 00:09:57,329
我们可以创建一些可以维护的分配跟踪器的功能
allocation tracker we can maintain

287
00:09:57,529 --> 00:09:59,679
分配指标，我们可以准确查明正在使用多少内存
allocation metrics we can find out

288
00:09:59,879 --> 00:10:01,269
分配指标，我们可以准确查明正在使用多少内存
exactly how much memory is being used

289
00:10:01,470 --> 00:10:02,859
分配了多少？释放了多少东西？
how much is being allocated how much is

290
00:10:03,059 --> 00:10:04,659
分配了多少？释放了多少东西？
being freed all of that stuff which of

291
00:10:04,860 --> 00:10:06,399
课程真的很有用，所以我要做的是我会来到这里，我会
course becomes really useful so what

292
00:10:06,600 --> 00:10:07,659
课程真的很有用，所以我要做的是我会来到这里，我会
I'll do is I'll come up here and I'll

293
00:10:07,860 --> 00:10:10,179
编写一个称为分配指标的结构，它将包含一个UN 32t
write a struct called allocation metrics

294
00:10:10,379 --> 00:10:13,509
编写一个称为分配指标的结构，它将包含一个UN 32t
this will contain a UN 32t which will be

295
00:10:13,710 --> 00:10:15,969
我们分配的总内存以及释放的总内存
our total allocated memory as well as

296
00:10:16,169 --> 00:10:18,789
我们分配的总内存以及释放的总内存
our total freed memory

297
00:10:18,990 --> 00:10:20,199
我还将在此处编写一个小函数来告诉我们当前的用法，
I'll also write a little function here

298
00:10:20,399 --> 00:10:23,889
我还将在此处编写一个小函数来告诉我们当前的用法，
that tells us our current usage and this

299
00:10:24,089 --> 00:10:27,159
只会分配乌龟-乌龟3我会创建一个静态的
will just be turtle allocated - turtle 3

300
00:10:27,360 --> 00:10:29,069
只会分配乌龟-乌龟3我会创建一个静态的
I'll create a little static

301
00:10:29,269 --> 00:10:32,219
此结构的实例，然后再打印整个
instance of this struct here and then

302
00:10:32,419 --> 00:10:33,359
此结构的实例，然后再打印整个
instead of printing this whole

303
00:10:33,559 --> 00:10:35,159
分配的事情，我只会说s分配指标
allocation thing I'll just say s

304
00:10:35,360 --> 00:10:37,919
分配的事情，我只会说s分配指标
allocation metrics turtle allocated plus

305
00:10:38,120 --> 00:10:40,319
由内而外相等的大小操作您，然后我将为
equal size inside out operating you and

306
00:10:40,519 --> 00:10:43,109
由内而外相等的大小操作您，然后我将为
then I'll do the same thing for the

307
00:10:43,309 --> 00:10:44,849
删除运算符，但那当然是我们的乌龟品种，所以现在我可以做的是
delete operator but of course that'll be

308
00:10:45,049 --> 00:10:47,219
删除运算符，但那当然是我们的乌龟品种，所以现在我可以做的是
our turtle breed so now what I can do is

309
00:10:47,419 --> 00:10:49,789
也许编写一个称为打印使用率区域性和内存使用率的函数
maybe write a function called print

310
00:10:49,990 --> 00:10:53,609
也许编写一个称为打印使用率区域性和内存使用率的函数
usage culture and memory usage so small

311
00:10:53,809 --> 00:10:56,669
清除，这只会输出我们的内存使用情况，即s分配
clear and this will simply output our

312
00:10:56,870 --> 00:11:00,689
清除，这只会输出我们的内存使用情况，即s分配
memory usage which will be s allocation

313
00:11:00,889 --> 00:11:06,029
度量当前使用情况的字节数，很酷，所以现在我可以在
metrics current usage bytes ok cool so

314
00:11:06,230 --> 00:11:08,129
度量当前使用情况的字节数，很酷，所以现在我可以在
now I can print my memory usage at the

315
00:11:08,330 --> 00:11:10,229
开始，我可以将其打印出来以分配该对象，我可以将该字符串向上移动
start I can print it off to allocating

316
00:11:10,429 --> 00:11:12,209
开始，我可以将其打印出来以分配该对象，我可以将该字符串向上移动
this object I might move this string up

317
00:11:12,409 --> 00:11:13,709
让它变得更有趣，我只是将这些
to make it a little bit more interesting

318
00:11:13,909 --> 00:11:15,479
让它变得更有趣，我只是将这些
and I'll just I'll just put these

319
00:11:15,679 --> 00:11:16,649
到处都让我们运行该程序，现在您可以在开始时看到
everywhere let's just run this program

320
00:11:16,850 --> 00:11:18,179
到处都让我们运行该程序，现在您可以在开始时看到
so now you can see that at the beginning

321
00:11:18,379 --> 00:11:20,370
我们没有使用任何内存，那么我们为此实际字符串分配了8个字节
we're not using any memory then we've

322
00:11:20,570 --> 00:11:22,649
我们没有使用任何内存，那么我们为此实际字符串分配了8个字节
allocated 8 bytes for this actual string

323
00:11:22,850 --> 00:11:24,959
那么我们为这三个整数又分配了12个字节，因为
then we've allocated another 12 bytes

324
00:11:25,159 --> 00:11:26,639
那么我们为这三个整数又分配了12个字节，因为
for these three integers because we have

325
00:11:26,840 --> 00:11:28,019
三个整数3乘以4等于12，最后我们又退缩了
three integers three times four is

326
00:11:28,220 --> 00:11:30,000
三个整数3乘以4等于12，最后我们又退缩了
twelve and then finally we're back down

327
00:11:30,200 --> 00:11:31,889
范围结束后，将其保留为八个字节，因为我们已经释放了唯一的指针
to eight bytes after the scope ends

328
00:11:32,090 --> 00:11:34,079
范围结束后，将其保留为八个字节，因为我们已经释放了唯一的指针
because we've freed our unique pointer

329
00:11:34,279 --> 00:11:36,029
无论如何，如果您发现这个有用的东西，那么很酷的东西继续抛出
pretty cool stuff anyway if you guys

330
00:11:36,230 --> 00:11:37,829
无论如何，如果您发现这个有用的东西，那么很酷的东西继续抛出
found this helpful go ahead and throw

331
00:11:38,029 --> 00:11:40,319
将其放入您自己的程序中，然后进行实际测试并查看实际效果
this into your own programs and actually

332
00:11:40,519 --> 00:11:42,029
将其放入您自己的程序中，然后进行实际测试并查看实际效果
test it out and see it in action

333
00:11:42,230 --> 00:11:43,979
我确实要说明一下，您当然可以为此使用工具
I do want to make a note that of course

334
00:11:44,179 --> 00:11:46,620
我确实要说明一下，您当然可以为此使用工具
you could use tools for this you don't

335
00:11:46,820 --> 00:11:48,359
必须编写自己执行此操作的代码，然后才能使用视觉工作室
have to write code that does this

336
00:11:48,559 --> 00:11:49,949
必须编写自己执行此操作的代码，然后才能使用视觉工作室
yourself you could use visual studios

337
00:11:50,149 --> 00:11:52,109
内置的内存分配跟踪性能分析工具
built-in kind of memory allocation

338
00:11:52,309 --> 00:11:53,639
内置的内存分配跟踪性能分析工具
tracking profiling tools whatever

339
00:11:53,840 --> 00:11:54,269
他们被称为valgrind也有一些不错的工具
they're called

340
00:11:54,470 --> 00:11:57,449
他们被称为valgrind也有一些不错的工具
valgrind also has some nice tools for

341
00:11:57,649 --> 00:11:59,219
跟踪内存并查看内存来自哪里，就像分配来自
tracking memory and seeing where it's

342
00:11:59,419 --> 00:12:01,079
跟踪内存并查看内存来自哪里，就像分配来自
coming from like allocations coming from

343
00:12:01,279 --> 00:12:02,879
所有这些东西，所以那里有可用的工具，我可能
all of that kind of stuff so there are

344
00:12:03,080 --> 00:12:04,289
所有这些东西，所以那里有可用的工具，我可能
tools available out there and I might

345
00:12:04,490 --> 00:12:05,909
将来在它们上制作一些视频，但对我个人而言，这是一个非常
make some videos on them in the future

346
00:12:06,110 --> 00:12:09,359
将来在它们上制作一些视频，但对我个人而言，这是一个非常
but for me personally this is a very

347
00:12:09,559 --> 00:12:11,789
快速而肮脏的方式来实际查看实际情况并进行跟踪
quick and dirty way to actually see what

348
00:12:11,990 --> 00:12:13,979
快速而肮脏的方式来实际查看实际情况并进行跟踪
is actually going on and also trace it

349
00:12:14,179 --> 00:12:15,509
回到您的代码中，而不必使用其他工具
back within your code without having to

350
00:12:15,710 --> 00:12:17,849
回到您的代码中，而不必使用其他工具
kind of use other tools they are

351
00:12:18,049 --> 00:12:20,579
绝对有用，我并不是说您不应该全部使用它们
definitely useful and I'm not saying

352
00:12:20,779 --> 00:12:21,959
绝对有用，我并不是说您不应该全部使用它们
that you shouldn't use them all I'm

353
00:12:22,159 --> 00:12:24,089
有时候写起来会更容易，更简单
saying is that sometimes it's just a lot

354
00:12:24,289 --> 00:12:26,129
有时候写起来会更容易，更简单
easier and more simple to just write

355
00:12:26,330 --> 00:12:28,109
这样的事情，可以立即看到那些
something like this and instantly be

356
00:12:28,309 --> 00:12:29,490
这样的事情，可以立即看到那些
able to see exactly where those

357
00:12:29,690 --> 00:12:31,409
分配来自，这绝对是我在我使用的东西
allocations are coming from and this is

358
00:12:31,610 --> 00:12:33,120
分配来自，这绝对是我在我使用的东西
definitely something that I use in my

359
00:12:33,320 --> 00:12:35,069
一直拥有自己的技术，实际上是在更大的游戏引擎中
own technology all the time and in fact

360
00:12:35,269 --> 00:12:37,339
一直拥有自己的技术，实际上是在更大的游戏引擎中
in a bigger kind of game engine for

361
00:12:37,539 --> 00:12:39,889
您可能会有自己的歌剧新操作员来领导所有这一切
you would probably have your own kind of

362
00:12:40,090 --> 00:12:41,449
您可能会有自己的歌剧新操作员来领导所有这一切
opera new operator to lead all that

363
00:12:41,649 --> 00:12:44,029
东西，因为除了必须跟踪指标并确保
stuff because aside from having to just

364
00:12:44,230 --> 00:12:45,649
东西，因为除了必须跟踪指标并确保
track metrics and make sure that that

365
00:12:45,850 --> 00:12:47,929
在所有平台上都可以使用，您可能还想控制确切的位置
works across all platforms you also may

366
00:12:48,129 --> 00:12:49,819
在所有平台上都可以使用，您可能还想控制确切的位置
want to control exactly where that

367
00:12:50,019 --> 00:12:51,439
记忆来自任何地方，希望你们喜欢这个视频
memory is coming from anyway hope you

368
00:12:51,639 --> 00:12:52,669
记忆来自任何地方，希望你们喜欢这个视频
guys enjoyed this video if you did

369
00:12:52,870 --> 00:12:54,409
请点击类似按钮，别忘了留下您可能有的任何问题
please hit that like button don't forget

370
00:12:54,610 --> 00:12:55,669
请点击类似按钮，别忘了留下您可能有的任何问题
to leave any questions that you may have

371
00:12:55,870 --> 00:12:57,319
在下面的评论部分为我提供了200k特殊问答视频，我将
for me in the comment section below for

372
00:12:57,519 --> 00:13:00,979
在下面的评论部分为我提供了200k特殊问答视频，我将
that 200k special Q&A video and I will

373
00:13:01,179 --> 00:13:03,509
下次再见[音乐]
see you next time goodbye

374
00:13:03,710 --> 00:13:08,710
下次再见[音乐]
[Music]

