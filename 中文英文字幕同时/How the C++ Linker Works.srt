﻿1
00:00:00,030 --> 00:00:02,468
hey little guys my name is Deshawn oh and today we're going to talk about
嘿，小伙子们，我的名字叫Deshawn哦，今天我们要谈的是

2
00:00:02,669 --> 00:00:07,509
linking so what is linking what does the simple spot linker actually do linking
链接，那么链接是什么？简单的Spot链接器实际执行的链接是什么

3
00:00:07,710 --> 00:00:11,559
is a process that we go through when we go from our source surface plus files to
是从源代码表面加上文件到

4
00:00:11,759 --> 00:00:15,490
our actual executable binary so the first stage is actually compiling our
我们实际的可执行二进制文件，因此第一步实际上是在编译我们的

5
00:00:15,689 --> 00:00:18,579
source files and I actually made an entire video on that so go check that
源文件，实际上我制作了一个完整的视频，所以请检查一下

6
00:00:18,778 --> 00:00:22,089
out link in the description below once we've compiled our files we need to
编译完文件后，需要在下面的说明中找到链接，我们需要

7
00:00:22,289 --> 00:00:26,710
go through a process called linking now the primary focus of linking is to find
经历一个称为链接的过程，现在链接的主要重点是查找

8
00:00:26,910 --> 00:00:32,079
where each symbol and function is and link them together remember each file is
每个符号和功能都在哪里并将它们链接在一起记住每个文件是

9
00:00:32,279 --> 00:00:36,279
compiled into a separate object file as a translation unit and they have no
编译成一个单独的目标文件作为翻译单元，它们没有

10
00:00:36,479 --> 00:00:40,119
relation to each other those files can't actually interact so if we decide to
彼此之间的关系这些文件实际上无法交互，因此如果我们决定

11
00:00:40,320 --> 00:00:45,428
split our program across multiple C++ files which is of course very common we
将我们的程序分为多个C ++文件，这当然很常见

12
00:00:45,628 --> 00:00:50,108
need a way to actually link those files together into one program and that is
需要一种将这些文件实际链接到一个程序中的方法，那就是

13
00:00:50,308 --> 00:00:53,469
the primary purpose of what the linker does even if you don't have functions in
链接器的主要目的是即使您没有在其中的功能

14
00:00:53,670 --> 00:00:56,739
external files like for example you've written your entire program in one file
外部文件，例如您已将整个程序编写到一个文件中

15
00:00:56,939 --> 00:01:00,669
the application still needs to know where the entry point is so in other
应用程序仍然需要知道入口点在其他位置

16
00:01:00,869 --> 00:01:04,238
words where the main function is so that when you actually run your application
主要功能所在的词，以便您实际运行应用程序时

17
00:01:04,438 --> 00:01:07,539
the C runtime library can say hey here's the main function I'm going to jump
 C运行时库可以说嘿，这是我要跳转的主要功能

18
00:01:07,739 --> 00:01:10,329
there and start executing code from there and that is effectively what
从那里开始执行代码，这就是有效的方法

19
00:01:10,530 --> 00:01:13,450
starts with your application so it still needs to link the main function and
从您的应用程序开始，因此它仍然需要链接主要功能，并且

20
00:01:13,650 --> 00:01:16,028
everything like that even if you don't have all the files the best way to
即使您没有所有文件，这样的事情也是最好的方法

21
00:01:16,228 --> 00:01:19,299
explain this is by showing some examples so let's jump over and take a look so
通过显示一些示例来说明这一点，让我们跳过并看一下

22
00:01:19,500 --> 00:01:22,299
here in Visual Studio we've got a very simple project that just contains one
在Visual Studio中，我们有一个非常简单的项目，其中仅包含一个

23
00:01:22,500 --> 00:01:27,129
source file master tbp and inside there we have two functions log and multiply
源文件主文件tbp，里面有两个日志和乘法功能

24
00:01:27,329 --> 00:01:30,399
the multiply function actually calls the log function prints out the word
乘法函数实际上会调用log函数打印出单词

25
00:01:30,599 --> 00:01:34,929
multiply to the console and then returns a times B pretty simple stuff however
乘以控制台，然后返回时间B非常简单的内容

26
00:01:35,129 --> 00:01:38,019
this isn't an actual application since of course it doesn't contain a main
这不是实际的应用程序，因为它当然不包含主程序

27
00:01:38,219 --> 00:01:41,528
function the first thing that you have to realize is that there are those two
功能首先要意识到的是，有这两个

28
00:01:41,728 --> 00:01:44,619
stages of compilation right there's compiling and there's linking and
编译的各个阶段，那里有编译，链接和

29
00:01:44,819 --> 00:01:47,649
there's actually a way that you can differentiate between the two individual
实际上，您可以区分两个人

30
00:01:47,849 --> 00:01:51,418
studio if you press ctrl f7 or if you press the compile button only
工作室，如果您按Ctrl F7或仅按“编译”按钮

31
00:01:51,618 --> 00:01:54,789
compilation will happen no linking will ever happen
编译将不会发生任何链接

32
00:01:54,989 --> 00:01:59,590
however if you build your project or if you hit f5 to run your project it will
但是，如果您构建项目或按f5键运行项目，它将

33
00:01:59,790 --> 00:02:04,599
actually compile and then link so if I just hit ctrl f7 you'll see that I
实际编译然后链接，所以如果我只是按ctrl f7键，您会看到我

34
00:02:04,799 --> 00:02:09,039
actually get no errors everything's fine because the compilation was successful
实际上没有任何错误，因为编译成功，所以一切都很好

35
00:02:09,239 --> 00:02:12,670
it generated that math dot obj file the object file and everything's great
它生成了数学点obj文件和目标文件，一切都很好

36
00:02:12,870 --> 00:02:16,450
however if I right-click on my project and hit build you'll see that I actually
但是，如果我右键单击我的项目并点击构建，您将看到我实际上

37
00:02:16,650 --> 00:02:20,830
get a linking error entry-point must be defined and again that's because I'm
得到一个链接错误入口点必须被定义，这再次是因为我

38
00:02:21,030 --> 00:02:24,850
missing my entry point my main function so because our compilation is divided
缺少我的主要功能的入口点，因为我们的编译是分开的

39
00:02:25,050 --> 00:02:28,750
into those two stages compiling and linking we actually get different types
在这两个阶段进行编译和链接，我们实际上得到了不同的类型

40
00:02:28,949 --> 00:02:32,980
of error messages associated with each stage if I make a syntax error for
与每个阶段相关的错误消息，如果我为

41
00:02:33,180 --> 00:02:36,610
example which of course is something the compiler has to deal with if I compile
例如，如果我进行编译，那当然是编译器必须处理的事情

42
00:02:36,810 --> 00:02:40,569
my code you'll see that it tells me that I actually get an error which is called
我的代码中，您会看到它告诉我实际上我收到一个错误，称为

43
00:02:40,769 --> 00:02:44,800
c2 143 and then it says syntax error of course so this is the error code for
 c2 143，然后它说的是语法错误，所以这是错误代码

44
00:02:45,000 --> 00:02:48,280
this type of error and you'll notice that it actually starts with the letter
这类错误，您会注意到它实际上以字母开头

45
00:02:48,479 --> 00:02:52,899
C this tells us that it's an error that occurred in the compiling stage if I fix
 C告诉我们，如果我修复，这是在编译阶段发生的错误

46
00:02:53,098 --> 00:02:57,969
that and then build my entire project you'll see the error code listed here
然后构建我的整个项目，您将看到此处列出的错误代码

47
00:02:58,169 --> 00:03:02,020
begins with the letters Ln K which of course stamps a link and it even tells
以字母Ln K开头，它当然会标记一个链接，甚至告诉

48
00:03:02,219 --> 00:03:05,770
us over here that this happened during the link stage it's really important
我们在这里，这发生在链接阶段，这非常重要

49
00:03:05,969 --> 00:03:09,730
that you know what kind of error you get whether it's a compiling error or a
知道是哪种错误，无论是编译错误还是

50
00:03:09,930 --> 00:03:13,090
linking error because of course you need to know that so that you can fix it
链接错误，因为您当然需要知道这一点，以便可以对其进行修复

51
00:03:13,289 --> 00:03:16,810
properly so in this case we get an error which tells us the entry point must be
正确，因此在这种情况下，我们会收到一条错误消息，告诉我们入口点必须是

52
00:03:17,009 --> 00:03:20,710
defined again that is because we are compiling this as an application if we
再次定义是因为如果我们将其编译为应用程序

53
00:03:20,909 --> 00:03:24,939
go to our properties and we take a look at what configuration type we have set
转到我们的属性，然后看一下我们设置的配置类型

54
00:03:25,139 --> 00:03:29,770
here you'll see it is data application or exe and every exe file has to have
在这里，您将看到它是数据应用程序或exe，每个exe文件都必须具有

55
00:03:29,969 --> 00:03:34,000
some kind of entry point if we go over here into the linker settings and into
如果我们在这里进入链接器设置并进入

56
00:03:34,199 --> 00:03:38,289
advanced you'll actually see that we can specify a custom entry point the entry
高级，您实际上会看到我们可以指定一个自定义入口点

57
00:03:38,489 --> 00:03:41,950
point doesn't have to be the main function there just has to be an entry
点不必是主要功能，只需输入一个

58
00:03:42,150 --> 00:03:46,840
point now normally it is the main function and for pretty much anything
现在，它通常是主要功能，几乎可以用于任何事情

59
00:03:47,039 --> 00:03:50,200
you do it probably will be the main function but just so you know entry
您可能会执行此操作，但这可能是主要功能，只是您知道输入

60
00:03:50,400 --> 00:03:54,009
point doesn't necessarily have to be a function called main it can be really
点不一定必须是称为main的函数

61
00:03:54,209 --> 00:03:57,939
anything so if we back out of here and actually write that main function I'll
任何事情，如果我们回到这里并实际编写该主要功能，我会

62
00:03:58,139 --> 00:04:03,849
just write int main and then I'll build my project again you'll see that we no
只需编写int main，然后我将再次构建我的项目，您会看到我们没有

63
00:04:04,049 --> 00:04:07,539
longer get that linking error and that we were successfully able to generate
不再得到该链接错误，并且我们能够成功生成

64
00:04:07,739 --> 00:04:10,629
that exe file all right so now that we've established that let's go ahead
该exe文件就可以了，现在我们已经确定了，让我们继续吧

65
00:04:10,829 --> 00:04:17,319
and print out the value of that multiply function so we'll multiply 5 and 8
并打印出该乘法函数的值，所以我们将5和8相乘

66
00:04:17,519 --> 00:04:20,030
together
一起

67
00:04:20,410 --> 00:04:25,410
so what we should see is this message being logged and then the value 40 being
所以我们应该看到的是此消息被记录，然后值40 

68
00:04:25,610 --> 00:04:29,960
printed let's also add a CNCs so that our console doesn't close immediately
打印后，我们还要添加一个CNC，以便不会立即关闭控制台

69
00:04:30,160 --> 00:04:33,530
then I'll just click on this local windows debugger button to run this and
然后我只需单击此本地Windows调试器按钮即可运行

70
00:04:33,730 --> 00:04:37,710
you can see we get multiplied and 40 so our application seems to be running
您可以看到我们乘以40，所以我们的应用程序似乎正在运行

71
00:04:37,910 --> 00:04:42,720
correctly great now suppose I had these in multiple files for example this log
现在正确地很好，假设我在多个文件中拥有这些文件，例如此日志

72
00:04:42,920 --> 00:04:46,680
doesn't really need to be in this mass file because of course this just logs
确实不需要在此海量文件中，因为当然这只是记录

73
00:04:46,879 --> 00:04:49,829
the message so why don't I have a separate file that actually contains all
消息，所以为什么我没有一个单独的文件实际上包含所有

74
00:04:50,029 --> 00:04:52,680
of my logging related functions I'm going to right click on source files and
与日志记录相关的功能中，我将右键单击源文件，然后

75
00:04:52,879 --> 00:04:58,530
add a new C++ file called lock they'll typically and then I'll click Add I'm
添加一个通常称为lock的新C ++文件，然后单击“添加” 

76
00:04:58,730 --> 00:05:02,460
going to grab that log function from here and move it into my log Delta VP
将从这里获取该日志功能，并将其移至我的日志Delta VP中

77
00:05:02,660 --> 00:05:07,500
file if I go back to master CBP and try and compile this code I'll get an error
文件，如果我回到主CBP并尝试编译此代码，则会收到错误消息

78
00:05:07,699 --> 00:05:10,650
and you'll know that this is a compile error because the error code begins with
而且您会知道这是一个编译错误，因为错误代码以

79
00:05:10,850 --> 00:05:14,579
let us see telling me the log is not found because this file has no knowledge
让我们看看告诉我找不到日志，因为该文件不知道

80
00:05:14,779 --> 00:05:19,050
that a function called log exists at all so we'll go ahead and grab this first
根本没有一个叫做log的函数，所以我们先来抢这个

81
00:05:19,250 --> 00:05:22,470
line of the log function which is the signature and we'll add this so that we
日志功能的一行是签名，我们将其添加为

82
00:05:22,670 --> 00:05:26,730
have a declaration of the log function in this map dot CPP file no ctrl f7 and
在此地图点CPP文件ctrl f7中没有log函数的声明，并且

83
00:05:26,930 --> 00:05:29,850
you can see it as compiling worked now let's go ahead and build our entire
您可以将其视为编译工作，让我们继续构建整个

84
00:05:30,050 --> 00:05:34,230
project we get several errors here compile errors telling us the CR is not
项目，我们在这里遇到几个错误编译错误告诉我们CR不是

85
00:05:34,430 --> 00:05:40,500
found because we need to actually include iostream once we've done that
发现，因为一旦完成，我们实际上需要包括iostream 

86
00:05:40,699 --> 00:05:43,710
let's build our entire project alright great it seemed to work
让我们构建我们的整个项目，好极了，它似乎可以正常工作

87
00:05:43,910 --> 00:05:47,220
successfully now let's take a look at one type of linking error that we might
现在成功地让我们看一下我们可能会遇到的一种类型的链接错误

88
00:05:47,420 --> 00:05:51,960
get this one called unresolved external symbol and this is what happens when the
得到这个称为未解决的外部符号，这就是当

89
00:05:52,160 --> 00:05:56,759
linker can't find something that it needs so we'll go back over here and in
链接器找不到所需的内容，因此我们将回到此处和

90
00:05:56,959 --> 00:06:00,449
the log file I'm going to change this to say something else for example I'm just
日志文件，我要更改它以表示其他内容，例如，我只是

91
00:06:00,649 --> 00:06:04,439
going to add an R here so that we say logger if I go back to master CPP I
将在此处添加一个R，这样我们回到主CPP时就说记录器

92
00:06:04,639 --> 00:06:08,550
still left my declaration as log so it still does expect the function to be
仍然将我的声明保留为日志，因此它仍然希望该函数为

93
00:06:08,750 --> 00:06:13,139
called log so this file will still compile of course because this does no
称为日志，因此该文件当然仍会编译，因为这样做不会

94
00:06:13,339 --> 00:06:17,189
linking so all it's doing is checking to make sure everything here compiled
链接，所以它所做的就是检查以确保此处的所有内容均已编译

95
00:06:17,389 --> 00:06:21,270
correctly it believes that there is a log function somewhere but it's going to
正确的是，它认为某个地方有一个日志函数，但是它将

96
00:06:21,470 --> 00:06:25,800
be the job of the linking stage to actually find that log function so if I
是链接阶段的工作，实际上是找到该日志功能，所以如果我

97
00:06:26,000 --> 00:06:29,460
build my entire project now you'll see we actually get an error this is a
现在构建我的整个项目，您会看到我们实际上收到一个错误，这是一个

98
00:06:29,660 --> 00:06:31,750
linking error because if you can see that it begins
链接错误，因为如果您可以看到它开始

99
00:06:31,949 --> 00:06:35,740
with the LNK letters and the RS as unresolved external symbol now he tells
他告诉LNK字母和RS作为未解决的外部符号

100
00:06:35,939 --> 00:06:39,850
us exactly what symbol is missing it's that log function it even tells us where
我们确切地缺少了什么符号，它甚至是告诉我们日志功能的位置

101
00:06:40,050 --> 00:06:43,600
we reference it that we're referencing it in a function called multiply so here
我们引用它是在一个叫做乘法的函数中引用它，所以这里

102
00:06:43,800 --> 00:06:47,680
it is in multiply we're calling log and it cannot actually find which function
它正好是乘法，我们正在调用log，但实际上无法找到哪个函数

103
00:06:47,879 --> 00:06:51,670
to link it to so of course it has to give up an error because when we land on
链接到它，所以当然要放弃一个错误，因为当我们降落在

104
00:06:51,870 --> 00:06:55,030
that code at runtime what is it supposed to do when it tries
该代码在运行时尝试时应该做什么

105
00:06:55,230 --> 00:06:58,509
to call the log function it doesn't know where the log function is now if I go
调用log函数，如果我去的话，它不知道log函数现在在哪里

106
00:06:58,709 --> 00:07:01,990
over here and I comment out this log function so that we actually never call
在这里，我注释掉此日志功能，以便我们实际上从不调用

107
00:07:02,189 --> 00:07:06,730
it if I try and build this we get no errors the reason this happens because I
如果我尝试构建它，就不会出错，因为我

108
00:07:06,930 --> 00:07:09,939
never call the log function so the linker doesn't have to link this
从不调用log函数，因此链接器不必链接此函数

109
00:07:10,139 --> 00:07:13,689
function call to actually call the log function because we never call the log
函数调用以实际调用日志函数，因为我们从不调用日志

110
00:07:13,889 --> 00:07:17,500
function another interesting note is that if I do call the log function here
函数另一个有趣的注意事项是，如果我在这里调用log函数

111
00:07:17,699 --> 00:07:21,910
and multiply however I comment this line out so that I never call multiply which
并乘以，但是我将这一行注释掉，这样我就再也不会乘以

112
00:07:22,110 --> 00:07:26,829
in turn never calls log if I build my project now you'll see that I still get
反过来，如果我现在构建项目，则永远不会调用日志，您会看到我仍然得到

113
00:07:27,029 --> 00:07:30,069
a linking error and you might be like what but why is that happening
链接错误，您可能会喜欢，但是为什么会这样

114
00:07:30,269 --> 00:07:34,930
I'm not calling multiply anywhere why is it complaining about a linking error
我不在任何地方调用乘法，为什么会抱怨链接错误

115
00:07:35,129 --> 00:07:38,139
Trinette have just removed a function entirely since this potentially dead
 Trinette刚刚完全删除了一个功能，因为该功能可能已死

116
00:07:38,339 --> 00:07:42,160
code that's never used wrong because whilst we're not using the
从未使用过的代码，因为虽然我们没有使用

117
00:07:42,360 --> 00:07:46,270
multiply function in this file we actually could technically use it in
在此文件中使用乘法功能，实际上我们可以在其中使用它

118
00:07:46,470 --> 00:07:50,110
another file and so the linker does actually need to link that if we could
另一个文件，因此链接器实际上确实需要链接该文件

119
00:07:50,310 --> 00:07:54,250
somehow tell the compiler that hey this function multiplied I'm only ever going
不知何故告诉编译器，嘿，这个函数成倍增加，我只会

120
00:07:54,449 --> 00:07:58,300
to use it inside this file then of course we could remove that linking
在文件中使用它，那么我们当然可以删除该链接

121
00:07:58,500 --> 00:08:01,990
necessity since this multiplies never called it never needs to call log oh
的必要性，因为这个乘数永远不会被调用，所以它永远不需要调用log哦

122
00:08:02,189 --> 00:08:05,620
wait there is a way we could do that if we come over here and write the word
等一下，如果我们过来这里写下这个词，我们有办法做到这一点

123
00:08:05,819 --> 00:08:09,520
static in front of multiply that basically means that this multiply
乘法前面的static基本上意味着该乘法

124
00:08:09,720 --> 00:08:13,660
function is only declared for this translation unit which is this math dot
仅为此翻译单元（此数学点）声明函数

125
00:08:13,860 --> 00:08:18,610
CPP file in our case and since multiply is never called inside this file if I
在我们的情况下，CPP文件由于在我的文件中从未调用过乘法

126
00:08:18,810 --> 00:08:23,290
build we won't get any linking errors if I bring back my comment though and then
构建，如果我回想一下，我们将不会收到任何链接错误

127
00:08:23,490 --> 00:08:27,100
I build of course we will get a linking error in this case we actually ended up
我建立的当然会出现链接错误，在这种情况下，我们实际上结束了

128
00:08:27,300 --> 00:08:30,579
modifying the name of the function however it's not just the name of the
修改函数名称，但不仅仅是函数名称

129
00:08:30,779 --> 00:08:34,120
function that matters if I bring back to function I'll call it log again I'll
如果我返回功能很重要的功能，我将其再次命名为“ log” 

130
00:08:34,320 --> 00:08:39,459
build my project we won't get any errors but then I for example change the return
建立我的项目，我们不会收到任何错误，但是例如，我更改了收益

131
00:08:39,659 --> 00:08:43,929
type to be int and then I'll just return 0 or something like
输入要为int的类型，然后我将返回0或类似的值

132
00:08:44,129 --> 00:08:49,659
if I build my project now you'll see that we get an error because in master
如果我现在建立我的专案，您会看到我们收到错误讯息，因为

133
00:08:49,860 --> 00:08:53,948
CBP we specify that this log function was a void function and so because of
 CBP我们指定此log函数为void函数，因此由于

134
00:08:54,149 --> 00:08:58,359
that is going to look for a function called log that returns void and also
这将寻找一个名为log的函数，该函数返回void并且

135
00:08:58,559 --> 00:09:02,139
takes in this one parameter if I go back to log and I might change this back to
如果我返回日志，则接受这个参数，我可能会将其更改回

136
00:09:02,339 --> 00:09:06,909
avoid get rid of this return zero and build it everything works fine but then
避免摆脱此返回零并建立它一切正常，但然后

137
00:09:07,110 --> 00:09:11,049
I might add another parameter for example the level if I now build this
如果现在构建此参数，我可能会添加另一个参数，例如级别

138
00:09:11,250 --> 00:09:15,128
we'll get a linking error once again because the log function that it expects
我们将再次收到链接错误，因为它期望的日志功能

139
00:09:15,328 --> 00:09:19,539
does not have another parameter you'll see if we go down here into the linking
没有其他参数，如果我们进入链接，您将看到

140
00:09:19,740 --> 00:09:23,589
error message it actually expects a function which returns void which has
错误消息，它实际上需要一个返回void的函数，该函数具有

141
00:09:23,789 --> 00:09:27,609
this calling convention it's called log and it has to have just one parameter
这种调用约定称为log，它必须只有一个参数

142
00:09:27,809 --> 00:09:33,399
which is a constant that's it if they cannot find exactly that then you're
如果他们找不到确切的常量，那就是常数

143
00:09:33,600 --> 00:09:36,669
going to get a linking error let's go back over here to our log file and just
会出现链接错误，让我们回到这里回到日志文件， 

144
00:09:36,870 --> 00:09:39,998
remove this level so that our program works again and if I build it of course
删除此级别，以便我们的程序可以再次运行，如果我构建它，当然

145
00:09:40,198 --> 00:09:43,779
we shouldn't get any linking errors great okay so the other type of linking
我们应该不会出现任何链接错误，所以其他类型的链接

146
00:09:43,980 --> 00:09:47,529
error that's pretty common is when we have to look at symbols so in other
常见的错误是当我们不得不看符号时

147
00:09:47,730 --> 00:09:51,789
words we have functions or variables which have the same name and the same
我们具有相同名称和相同功能的函数或变量

148
00:09:51,990 --> 00:09:56,049
signature so two identically named functions which have the same return
签名，因此两个名称相同的函数具有相同的返回值

149
00:09:56,250 --> 00:09:59,589
value and the same parameters if that happens we're in trouble
值和相同的参数（如果发生这种情况），我们会遇到麻烦

150
00:09:59,789 --> 00:10:02,498
the reason we're in trouble is because the linker doesn't know which one to
我们遇到麻烦的原因是因为链接器不知道要选择哪个

151
00:10:02,698 --> 00:10:07,058
link to it's ambiguous so back in our code if I was for example to write
链接到它是模棱两可的，所以回到我们的代码中，例如，如果我要编写

152
00:10:07,259 --> 00:10:10,659
another version of dysfunction so I'll just literally copy and paste this
另一个版本的功能障碍，所以我将直接复制并粘贴此功能

153
00:10:10,860 --> 00:10:13,959
function and try and build my code you'll know that we actually get a
函数并尝试构建我的代码，您将知道我们实际上得到了一个

154
00:10:14,159 --> 00:10:17,859
compile error because this already has a body and the compiler can kind of tell
编译错误，因为它已经有一个主体，并且编译器可以告诉您

155
00:10:18,059 --> 00:10:22,179
us that yeah okay since I'm compiling this file I can obviously see that
我们，是的，因为我正在编译此文件，所以我显然可以看到

156
00:10:22,379 --> 00:10:26,258
you've made a mistake this code just isn't valid so this is an example of
您犯了一个错误，该代码无效，所以这是一个示例

157
00:10:26,458 --> 00:10:30,068
having to look at symbols where the compiler can actually save us because
必须查看符号，编译器实际上可以拯救我们，因为

158
00:10:30,269 --> 00:10:33,729
this all happens in one file and no linking actually needs to happen to see
这一切都发生在一个文件中，实际上不需要进行任何链接即可查看

159
00:10:33,929 --> 00:10:38,438
that we've got an error however if I was to move this into a different file for
这是一个错误，但是如果我要将其移动到另一个文件中

160
00:10:38,639 --> 00:10:42,938
example I'll move it back into our math file right over here so we still have
示例，我将其移回此处的数学文件中，因此我们仍然有

161
00:10:43,139 --> 00:10:46,179
our declaration I can leave that there that's fine that's just a declaration
我们的声明，我可以离开，那很好，那只是一个声明

162
00:10:46,379 --> 00:10:49,448
where they have one definition of log in this file so it's not going to give us a
他们在此文件中有一个日志的定义，因此不会给我们一个

163
00:10:49,649 --> 00:10:53,378
compiling error if I had control f7 just to compile this you can see it's totally
如果我有控制f7只是为了编译它，则会出现编译错误，您可以看到它完全是

164
00:10:53,578 --> 00:10:56,719
fine but now if I build we get a linking error
很好，但是现在如果我构建，我们会收到链接错误

165
00:10:56,919 --> 00:10:59,719
and you can see that the one we get it tells us that we have this log function
您会看到我们得到的那个告诉我们我们有此日志功能

166
00:10:59,919 --> 00:11:05,120
already defined in log obj one or more multiply defined symbols found so in
已经在log obj中定义了一个或多个乘法定义的符号，因此在

167
00:11:05,320 --> 00:11:08,149
this case link it doesn't know which log function to link to does the link to the
在这种情况下，链接不知道要链接到哪个日志功能

168
00:11:08,350 --> 00:11:12,559
one in map specifically or does link to the one in log dot CBP it doesn't know
具体在地图中的一个或确实链接到它不知道的对数点CBP中的一个

169
00:11:12,759 --> 00:11:15,529
now you might think that this type of arrow is not something that would happen
现在您可能会认为这种箭头不会发生

170
00:11:15,730 --> 00:11:19,878
often and that you're smarter than that however this can creep up on you so I'll
通常，你比那更聪明，但是这可能会蔓延到你身上，所以我会

171
00:11:20,078 --> 00:11:22,039
show you a few ways as to how that can happen
向您展示如何发生这种情况的几种方法

172
00:11:22,240 --> 00:11:25,549
all right firstly let's just remove this extra log definition we have here so
好吧，首先让我们删除这里的额外日志定义

173
00:11:25,750 --> 00:11:29,149
that our project builds successfully now let's create a header file I'm going to
现在我们的项目成功构建了，让我们创建一个头文件

174
00:11:29,350 --> 00:11:32,719
right click on the header file select add new item going to be a header file
右键单击头文件，选择添加新项目，将其作为头文件

175
00:11:32,919 --> 00:11:37,309
I'm going to call this file log H and click Add now inside here I'm going to
我将这个文件命名为H，然后点击“立即添加到此处” 

176
00:11:37,509 --> 00:11:41,870
grab this log function and make sure that I'm declaring it inside this header
获取此日志功能，并确保我在此标头中声明了它

177
00:11:42,070 --> 00:11:46,188
file if I go back to log CPP I'll write some kind of other function for example
文件，如果我返回日志CPP，我会写一些其他功能，例如

178
00:11:46,389 --> 00:11:51,109
in its log that's just going to call the log function and say that it's
在它的日志中，只是要调用log函数，并说它是

179
00:11:51,309 --> 00:11:55,279
initialized the log of course if we try and compile now we're going to get an
如果我们现在尝试编译的话，当然会初始化日志，我们将获得一个

180
00:11:55,480 --> 00:12:01,729
error because we need that log function so I'll include log back over here in
错误，因为我们需要该日志功能，因此我将在此处重新登录

181
00:12:01,929 --> 00:12:04,758
master zbp instead of having this declaration here I'm also going to
大师zbp而不是在这里有这个声明，我也要

182
00:12:04,958 --> 00:12:11,089
include log so great we're calling a log function from both the multiply function
包括日志太好了，我们从两个乘法函数中都调用了log函数

183
00:12:11,289 --> 00:12:15,229
inside the master zerah key file as well as the net log inside the log of CBP
在主zerah密钥文件中以及CBP日志中的net日志中

184
00:12:15,429 --> 00:12:18,139
file it doesn't really matter that I'm not calling this function so don't worry
文件，我没有调用此函数并不重要，所以不用担心

185
00:12:18,339 --> 00:12:21,529
about that I'm just going to build my project okay check this out I get an
关于那我将要建立我的项目好吧，检查一下，我得到了

186
00:12:21,730 --> 00:12:26,240
error telling me the log is already defined in local obj so we get a one or
告诉我日志已在本地obj中定义的错误，因此我们得到一个或

187
00:12:26,440 --> 00:12:29,269
more multiply divine symbol sound so we're getting a duplicate symbols error
更多倍增神圣的符号声音，所以我们得到了重复的符号错误

188
00:12:29,470 --> 00:12:32,419
message however you can see that I've really only got one definition of log
消息，但是您可以看到我真的只有一个日志定义

189
00:12:32,620 --> 00:12:36,439
it's inside this log dot H file why is it complaining about multiple symbols
它在此日志点H文件中，为什么它抱怨多个符号

190
00:12:36,639 --> 00:12:40,549
now this comes back to how the include statement works remember when we include
现在，这又回到了include语句的工作方式

191
00:12:40,750 --> 00:12:44,389
a header file we're just taking the contents of that header file and putting
头文件，我们只是获取该头文件的内容并放入

192
00:12:44,589 --> 00:12:49,519
it where our include statement is so what's actually happened is it's taking
它是我们的include语句所在的位置，所以实际上发生的是

193
00:12:49,720 --> 00:12:55,698
this log function popped it over here like so into logged up CBP and then also
该日志功能将其弹出到这里，就像登录CBP，然后

194
00:12:55,899 --> 00:13:00,289
over here and now you can see that we of course do have to log functions so how
在这里，现在您可以看到我们当然必须记录功能，如何

195
00:13:00,490 --> 00:13:04,008
do we fix this well we've got a few options here if we undo all of this so
如果我们撤消所有这些，我们是否可以解决这个问题呢？ 

196
00:13:04,208 --> 00:13:07,729
that we are including log again we could mark this log function
我们再次包含日志，我们可以标记此日志功能

197
00:13:07,929 --> 00:13:11,179
static that means that the linking that should happen to this log function
静态的，这意味着应该执行此日志功能的链接

198
00:13:11,379 --> 00:13:15,199
should only be internal which means that this log function when it gets included
应该只在内部，这意味着该日志功能在包含时

199
00:13:15,399 --> 00:13:18,709
into log dog therapy and master therapy is going to be just internal to this
进入原木狗疗法和大师疗法将仅在此内部

200
00:13:18,909 --> 00:13:22,759
file kind of like what we did with multiply so basically log and math will
文件有点像我们做乘法的东西，所以基本上对数和数学将

201
00:13:22,960 --> 00:13:26,599
have their own versions of this function called log and it won't be visible to
具有此函数的自己的版本，称为log，它对

202
00:13:26,799 --> 00:13:30,529
any other object files so if we just compile this now you'll see that we
任何其他目标文件，所以如果我们现在编译它，您将看到我们

203
00:13:30,730 --> 00:13:33,649
won't get any linking errors another thing that we could do to this is make
不会出现任何链接错误我们可以做的另一件事是make 

204
00:13:33,850 --> 00:13:37,189
it inline of course in line just means that it's going to take our actual
它当然是在线的，这意味着它将采用我们实际的

205
00:13:37,389 --> 00:13:41,870
function body and replace the call with it so in this case this log initialized
函数体并用它替换调用，因此在这种情况下，此日志已初始化

206
00:13:42,070 --> 00:13:48,349
log would just become that it saw we were to do something like that and build
日志将变成它看到我们要做类似的事情并构建

207
00:13:48,549 --> 00:13:51,620
you'll see that we get no errors either now there's one other way that we could
您会发现我们现在没有错误，或者还有另一种方法

208
00:13:51,820 --> 00:13:54,889
fix this and that's probably what I would do in this situation and that is
解决这个问题，这可能是我在这种情况下要做的，那就是

209
00:13:55,090 --> 00:13:58,639
just move the definition of this into one translation unit because right now
只需将其定义移到一个翻译单元中，因为现在

210
00:13:58,840 --> 00:14:02,599
what's happening is this log function is being included in two translation units
发生的情况是此日志功能包含在两个翻译单元中

211
00:14:02,799 --> 00:14:05,599
love does EBP and maps are therapy that's what's causing the error in the
 EBP和地图确实是一种疗法，正是这种疗法导致了错误

212
00:14:05,799 --> 00:14:09,379
first place so we could move it into a third translation unit or we could put
第一名，所以我们可以将其移至第三翻译单元，也可以放

213
00:14:09,580 --> 00:14:13,219
this log definition into one of these existing translation units since this
将此日志定义放入这些现有翻译单元之一中，因为

214
00:14:13,419 --> 00:14:16,009
function is called log and it's related to logging I'm actually going to put it
该函数称为日志，它与日志记录有关，我实际上将把它

215
00:14:16,210 --> 00:14:20,479
into log dot CBP so I'll grab this function I'll copy it into logical
到对数点CBP中，因此我将获取此功能，然后将其复制到逻辑

216
00:14:20,679 --> 00:14:24,319
sweetie I'll get rid of the in line and then I'll come back to my log door age
亲爱的，我将摆脱排队，然后回到木门时代

217
00:14:24,519 --> 00:14:28,549
and just leave the Declaration here again without the in line of course so
然后就顺其自然地再次离开宣言

218
00:14:28,750 --> 00:14:32,629
now this header file just has the declaration for log the actual function
现在，此头文件仅具有用于记录实际功能的声明

219
00:14:32,830 --> 00:14:37,399
to link to is included inside logo CBP once in one translation unit in our
链接到徽标CBP包含在我们的一个翻译单元中一次

220
00:14:37,600 --> 00:14:42,649
project and then main we'll call that so if I build this we get no linking errors
项目，然后再调用main，所以如果我构建它，则不会出现链接错误

221
00:14:42,850 --> 00:14:46,399
and our project was able to be linked successfully so that's it that's pretty
并且我们的项目能够成功链接，就是这样

222
00:14:46,600 --> 00:14:50,599
much a quick crash course on linking and how linking works remember at the end of
有关链接以及链接工作原理的快速速成课程，请记住

223
00:14:50,799 --> 00:14:53,509
the day the linker needs to take all of our objects files that were generated
链接器需要获取生成的所有目标文件的那天

224
00:14:53,710 --> 00:14:57,709
during compilation and link them all together it will also pull in any other
在编译过程中并将它们链接在一起，它还会拉入其他任何一个

225
00:14:57,909 --> 00:15:01,669
libraries that we may be using for example the C runtime library the
我们可能正在使用的库，例如C运行时库

226
00:15:01,870 --> 00:15:06,199
sippers law standard library our platform api is necessary and a whole
吸管者法律标准库，我们的平台api是必要的，也是一个整体

227
00:15:06,399 --> 00:15:09,349
lot of other stuff it's very common to be linking from many different places
很多其他东西是很常见的，可以从许多不同的地方链接

228
00:15:09,549 --> 00:15:12,500
there's also different types of linking we have static linking and we have
还有不同类型的链接，我们有静态链接，我们有

229
00:15:12,700 --> 00:15:16,279
dynamic linking but I'll save that for another video anyway thanks for watching
动态链接，但无论如何我都会将其保存为另一个视频，感谢您的观看

230
00:15:16,480 --> 00:15:19,370
I hope you enjoyed this video if you have any other questions just leave them
如果您还有其他问题，希望您喜欢这部影片

231
00:15:19,570 --> 00:15:21,669
in the comments and below and I'll try my best to answer
在下面的评论中，我会尽力回答

232
00:15:21,870 --> 00:15:25,120
them be sure to follow me on Twitter and Instagram and if you really like this
他们一定要在Twitter和Instagram上关注我，如果您真的喜欢这个

233
00:15:25,320 --> 00:15:27,879
video you can support me on patreon that'll help me make more of these
您可以在patreon上支持我的视频，可以帮助我更多地利用这些视频

234
00:15:28,080 --> 00:15:31,990
videos and by doing so you'll also get access to early draft videos and be
视频，这样做还可以访问早期草稿视频并成为

235
00:15:32,190 --> 00:15:35,199
involved in the planning process but as always I'll see you guys next time
参与了计划过程，但我将一如既往地再次见到你们

236
00:15:35,399 --> 00:15:40,399
goodbye fool
再见傻瓜

