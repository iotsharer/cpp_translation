﻿1
00:00:00,950 --> 00:00:04,229
[音乐]大家好，我叫陈哦
[Music]

2
00:00:04,429 --> 00:00:06,669
[音乐]大家好，我叫陈哦
hey what's up guys my name is Chen oh

3
00:00:06,870 --> 00:00:09,339
欢迎今天回到我的C ++系列，我们将讨论所有关于线程的内容
welcome back to my c++ series today

4
00:00:09,539 --> 00:00:11,290
欢迎今天回到我的C ++系列，我们将讨论所有关于线程的内容
we're gonna be talking all about threads

5
00:00:11,490 --> 00:00:13,780
总的来说，我们如何能使眼睛平行这是一个很难的词，我们如何制造东西
and in general how we can parallel eyes

6
00:00:13,980 --> 00:00:16,780
总的来说，我们如何能使眼睛平行这是一个很难的词，我们如何制造东西
that's a hard word how we can make stuff

7
00:00:16,980 --> 00:00:19,030
并行发生是因为大多数计算机，进程或设备
happen in parallel because most of the

8
00:00:19,230 --> 00:00:20,739
并行发生是因为大多数计算机，进程或设备
computers or processes or devices that

9
00:00:20,939 --> 00:00:23,199
如今我们正在编程，具有多个逻辑处理线程
we're programming for nowadays have more

10
00:00:23,399 --> 00:00:25,870
如今我们正在编程，具有多个逻辑处理线程
than one logical thread of processing

11
00:00:26,070 --> 00:00:29,470
到目前为止，他们实际上可以完成我们用c ++编写的所有代码
that they can actually do so far all the

12
00:00:29,670 --> 00:00:31,060
到目前为止，他们实际上可以完成我们用c ++编写的所有代码
code that we've been writing in the c++

13
00:00:31,260 --> 00:00:33,070
系列和opengl系列，如果你们正在观看
series and the opengl series as well if

14
00:00:33,270 --> 00:00:36,070
系列和opengl系列，如果你们正在观看
you guys are watching that has been on a

15
00:00:36,270 --> 00:00:38,049
单线程，这意味着当我们实际为这些情节编写代码时
single thread which means that when we

16
00:00:38,250 --> 00:00:40,239
单线程，这意味着当我们实际为这些情节编写代码时
actually write code for those episodes

17
00:00:40,439 --> 00:00:42,279
对于这些系列，我们实际上只是让计算机将一件事情做
and for those series we're literally

18
00:00:42,479 --> 00:00:44,049
对于这些系列，我们实际上只是让计算机将一件事情做
just making the computer do things one

19
00:00:44,250 --> 00:00:46,869
一次只对一条指令执行一次的代码行
kind of line of code at a time on one

20
00:00:47,070 --> 00:00:49,140
一次只对一条指令执行一次的代码行
instruction at a time which is fine a

21
00:00:49,340 --> 00:00:50,979
我们写的很多东西显然不是特别重要
lot of the things that we've written

22
00:00:51,179 --> 00:00:52,538
我们写的很多东西显然不是特别重要
have obviously not been particularly

23
00:00:52,738 --> 00:00:55,029
速度快，或者不需要真正优化或真正优化
fast or that they haven't needed to be

24
00:00:55,229 --> 00:00:57,009
速度快，或者不需要真正优化或真正优化
really like well optimized or really

25
00:00:57,210 --> 00:00:58,570
快速，因为我们并没有真正编写任何繁重的代码
quick because we haven't really written

26
00:00:58,770 --> 00:01:00,820
快速，因为我们并没有真正编写任何繁重的代码
any heavy code any code that has that

27
00:01:01,020 --> 00:01:02,320
实际上会花费很长时间来计算设备，这非常简单
will actually take the device a long

28
00:01:02,520 --> 00:01:04,388
实际上会花费很长时间来计算设备，这非常简单
time to compute it's been really simple

29
00:01:04,588 --> 00:01:06,159
东西，但是当我们进入更复杂的程序时，对我们来说非常有益
stuff but as we get into more complex

30
00:01:06,359 --> 00:01:10,000
东西，但是当我们进入更复杂的程序时，对我们来说非常有益
programs it's very beneficial for us to

31
00:01:10,200 --> 00:01:12,250
实际上将某些工作转移到不同的执行线程上
actually move off certain work to

32
00:01:12,450 --> 00:01:14,649
实际上将某些工作转移到不同的执行线程上
different threads of execution not just

33
00:01:14,849 --> 00:01:16,719
为了某种表现，也为了我们能做到的
for the sake of kind of performance but

34
00:01:16,920 --> 00:01:17,859
为了某种表现，也为了我们能做到的
also for the sake of what we can

35
00:01:18,060 --> 00:01:20,349
实际上，例如，我们使用sed CN get来请求输入
actually do with that for example we've

36
00:01:20,549 --> 00:01:23,590
实际上，例如，我们使用sed CN get来请求输入
used sed CN get to request input from

37
00:01:23,790 --> 00:01:27,129
用户进入控制台，但是我们在等待时无法执行任何操作
the user into the console however we

38
00:01:27,329 --> 00:01:28,480
用户进入控制台，但是我们在等待时无法执行任何操作
can't do anything while we're waiting

39
00:01:28,680 --> 00:01:30,340
对于输入，我们真的可以等待接收输入，这是一种
for input can we we're literally just

40
00:01:30,540 --> 00:01:32,799
对于输入，我们真的可以等待接收输入，这是一种
waiting to receive input and that's kind

41
00:01:33,000 --> 00:01:34,090
就像一个真正的循环，它会无限循环，直到我们按
of like a while true loop that's just

42
00:01:34,290 --> 00:01:37,209
就像一个真正的循环，它会无限循环，直到我们按
going on infinitely and until we press

43
00:01:37,409 --> 00:01:39,099
ENTER，就像我们的线程被阻塞一样，但是如果我们可以做的话
ENTER that's it like our thread is

44
00:01:39,299 --> 00:01:40,808
ENTER，就像我们的线程被阻塞一样，但是如果我们可以做的话
blocked but what if we could maybe do

45
00:01:41,009 --> 00:01:42,159
诸如将某些内容打印到控制台或仅记录某些内容或
something else like print something to

46
00:01:42,359 --> 00:01:43,869
诸如将某些内容打印到控制台或仅记录某些内容或
the console or just log something or

47
00:01:44,069 --> 00:01:45,819
在实际上等待用户输入的同时编写文件或其他内容
write a file or whatever while we're

48
00:01:46,019 --> 00:01:47,558
在实际上等待用户输入的同时编写文件或其他内容
actually waiting for user input we still

49
00:01:47,759 --> 00:01:48,969
希望发生其他事情，这可能是一个非常简单的例子
want other things to happen that's one

50
00:01:49,170 --> 00:01:51,128
希望发生其他事情，这可能是一个非常简单的例子
example that's probably really simple to

51
00:01:51,328 --> 00:01:52,750
理解，但无论如何，我们只是直接深入代码中，看看我们
understand but anyway we're just gonna

52
00:01:52,950 --> 00:01:55,509
理解，但无论如何，我们只是直接深入代码中，看看我们
dive right into the code and see what we

53
00:01:55,709 --> 00:01:57,878
真的可以做的很好，所以我要做的第一件事就是向您展示好心
can actually do alright so the first

54
00:01:58,078 --> 00:01:59,319
真的可以做的很好，所以我要做的第一件事就是向您展示好心
thing I'm going to do is show you kind

55
00:01:59,519 --> 00:02:00,698
线程如何处理我们可以使用它们的方法，所以我将包括
of how threads work on what we can do

56
00:02:00,899 --> 00:02:02,378
线程如何处理我们可以使用它们的方法，所以我将包括
with them so I'm going to include this

57
00:02:02,578 --> 00:02:04,509
我们在这里拥有的线程标头基本上包括线程支持
thread header that we have here which

58
00:02:04,709 --> 00:02:07,000
我们在这里拥有的线程标头基本上包括线程支持
includes the threading support basically

59
00:02:07,200 --> 00:02:08,289
进入此文件，以便我们可以使用线程类以及所有内容，然后键入
into this file so that we can use a

60
00:02:08,489 --> 00:02:09,969
进入此文件，以便我们可以使用线程类以及所有内容，然后键入
thread class and everything and I type

61
00:02:10,169 --> 00:02:11,980
在sed线程中，我将其命名为某事，因此我将其称为工作
in sed thread I'm gonna call it

62
00:02:12,180 --> 00:02:13,480
在sed线程中，我将其命名为某事，因此我将其称为工作
something so I'll call this a work

63
00:02:13,680 --> 00:02:14,950
因为它就像我们的工作者线程，然后我的意思是你是什么
because it's gonna be like our worker

64
00:02:15,150 --> 00:02:16,719
因为它就像我们的工作者线程，然后我的意思是你是什么
thread and then what I mean what are you

65
00:02:16,919 --> 00:02:18,069
要做的是传入一个函数以及它可以选择的任何类型的参数
to do is pass in a function and

66
00:02:18,269 --> 00:02:19,929
要做的是传入一个函数以及它可以选择的任何类型的参数
optionally any kind of arguments that it

67
00:02:20,128 --> 00:02:22,480
可能需要，所以让我们做一个叫做do work的函数，这就是这个词
may need so let's make a function called

68
00:02:22,680 --> 00:02:24,909
可能需要，所以让我们做一个叫做do work的函数，这就是这个词
do work this is going to be the word

69
00:02:25,109 --> 00:02:25,959
这将是实际执行我们想要发生的事情的功能
this is going to be the function that

70
00:02:26,158 --> 00:02:27,670
这将是实际执行我们想要发生的事情的功能
actually performs what we want to happen

71
00:02:27,870 --> 00:02:30,819
在另一个执行线程上，所以我将把它传递给这里
on another thread of execution so I'm

72
00:02:31,019 --> 00:02:32,590
在另一个执行线程上，所以我将把它传递给这里
going to pass it into here by just

73
00:02:32,789 --> 00:02:34,480
打字和工作，如果你们还没有看到我的功能指针视频
typing and do work if you guys haven't

74
00:02:34,680 --> 00:02:36,159
打字和工作，如果你们还没有看到我的功能指针视频
seen my function pointers video

75
00:02:36,359 --> 00:02:37,569
一定要检查一下，因为这是它如何工作的一个函数
definitely check that up because this is

76
00:02:37,769 --> 00:02:39,519
一定要检查一下，因为这是它如何工作的一个函数
how this works it takes in a function

77
00:02:39,718 --> 00:02:41,709
指针，这就是为什么我们只是传递而没有括号或其他任何东西的原因
pointer that's why we just pass in do

78
00:02:41,908 --> 00:02:43,149
指针，这就是为什么我们只是传递而没有括号或其他任何东西的原因
work without parentheses or anything

79
00:02:43,348 --> 00:02:44,769
就像那个视频链接将在那儿，我们编写此实际代码后
like that video link will be up there

80
00:02:44,968 --> 00:02:46,719
就像那个视频链接将在那儿，我们编写此实际代码后
and as soon as we write this actual code

81
00:02:46,919 --> 00:02:48,610
它会立即启动该线程，并会执行任何操作
it's going to immediately kick off that

82
00:02:48,810 --> 00:02:50,289
它会立即启动该线程，并会执行任何操作
thread and it's going to do whatever is

83
00:02:50,489 --> 00:02:51,700
在这里，它将一直运行，直到我们等待它退出，这样
in here and it's going to keep running

84
00:02:51,900 --> 00:02:54,640
在这里，它将一直运行，直到我们等待它退出，这样
until we wait for it to exit so the way

85
00:02:54,840 --> 00:02:56,289
我们实际上可以等待某些事情完成或等待线程完成
that we can actually wait for something

86
00:02:56,489 --> 00:02:58,149
我们实际上可以等待某些事情完成或等待线程完成
to finish or wait for a thread to

87
00:02:58,348 --> 00:03:00,730
完成工作是通过打入工人点联接，所以这是我们的
complete its work is by tacking is by

88
00:03:00,930 --> 00:03:03,969
完成工作是通过打入工人点联接，所以这是我们的
typing in worker dot join so this is our

89
00:03:04,169 --> 00:03:05,500
当然是线程对象，然后这个连接函数实际上将
thread object of course and then this

90
00:03:05,699 --> 00:03:07,750
当然是线程对象，然后这个连接函数实际上将
join function is essentially going to

91
00:03:07,949 --> 00:03:09,550
只需等待该线程被加入，现在线程加入是另一种
just wait for this thread to be joined

92
00:03:09,750 --> 00:03:11,830
只需等待该线程被加入，现在线程加入是另一种
now thread joining is a another kind of

93
00:03:12,030 --> 00:03:14,170
潜在的复杂主题，可能值得更详细地讨论
portent potentially complex topic that

94
00:03:14,370 --> 00:03:15,580
潜在的复杂主题，可能值得更详细地讨论
might be worth discussing in more detail

95
00:03:15,780 --> 00:03:18,189
但是基本上就像在c-sharp和更现代的编程中一样
but basically in like in c-sharp and

96
00:03:18,389 --> 00:03:19,569
但是基本上就像在c-sharp和更现代的编程中一样
kind of more modern programming it's

97
00:03:19,769 --> 00:03:22,060
只是叫“等待”或“等待退出”，或者只是我离开而已，这可能是合理的
just called wait or wait for exit or

98
00:03:22,259 --> 00:03:23,849
只是叫“等待”或“等待退出”，或者只是我离开而已，这可能是合理的
just me away it is probably reasonable

99
00:03:24,049 --> 00:03:26,259
基本上所有这一切只是说嘿，您可以等一下当前
basically all of this does is it just

100
00:03:26,459 --> 00:03:29,170
基本上所有这一切只是说嘿，您可以等一下当前
says hey can you wait on the current

101
00:03:29,370 --> 00:03:31,420
该线程要完成其工作，因此请阻塞当前线程，直到
thread for this thread to finish its

102
00:03:31,620 --> 00:03:33,390
该线程要完成其工作，因此请阻塞当前线程，直到
work so block the current thread until

103
00:03:33,590 --> 00:03:36,340
这个其他线程已经完成，所以因为这个东西正在运行
this other thread has completed so

104
00:03:36,539 --> 00:03:37,629
这个其他线程已经完成，所以因为这个东西正在运行
because this stuff is running in

105
00:03:37,829 --> 00:03:40,060
并行，我们有一个主线程，它从一个工作线程开始，
parallel we have our main thread which

106
00:03:40,259 --> 00:03:42,159
并行，我们有一个主线程，它从一个工作线程开始，
starts off a worker thread which does

107
00:03:42,359 --> 00:03:44,649
它的工作，然后最终通过编写这种join调用来完成我们的工作
its work and then eventually what we're

108
00:03:44,848 --> 00:03:46,599
它的工作，然后最终通过编写这种join调用来完成我们的工作
doing by writing this join kind of call

109
00:03:46,799 --> 00:03:48,610
这是我们在主线程上说的，等待该工作线程完成
here is we're saying on the main thread

110
00:03:48,810 --> 00:03:50,890
这是我们在主线程上说的，等待该工作线程完成
wait for that worker thread to finish

111
00:03:51,090 --> 00:03:53,980
在我们继续之前的所有执行都与主线程无关
all of its execution before we continue

112
00:03:54,180 --> 00:03:55,959
在我们继续之前的所有执行都与主线程无关
on was with whatever our main thread

113
00:03:56,158 --> 00:03:58,149
手，所以这意味着该CN点获取代码行将不会运行
hands so what that means is that this CN

114
00:03:58,348 --> 00:04:00,550
手，所以这意味着该CN点获取代码行将不会运行
dot get line of code will not run

115
00:04:00,750 --> 00:04:01,929
因为这是此后的下一行代码，直到一切都不会运行
because it's the next line of code after

116
00:04:02,128 --> 00:04:04,719
因为这是此后的下一行代码，直到一切都不会运行
this it will not run until everything

117
00:04:04,919 --> 00:04:06,610
该功能已完成，我们将在亲和祈祷中讲
that is in this function has finished

118
00:04:06,810 --> 00:04:08,170
该功能已完成，我们将在亲和祈祷中讲
and we'll say this in pro and prayer

119
00:04:08,370 --> 00:04:10,179
公民也将在短短的一秒钟内练习一下，例如我要去的事
will citizen practice in just a second

120
00:04:10,378 --> 00:04:12,099
公民也将在短短的一秒钟内练习一下，例如我要去的事
as well so as an example what I'm going

121
00:04:12,299 --> 00:04:14,409
现在要做的就是让我们深入了解它，我将演示
to do now is just let's just dive right

122
00:04:14,609 --> 00:04:16,569
现在要做的就是让我们深入了解它，我将演示
into it and I'll demonstrate that

123
00:04:16,769 --> 00:04:18,968
我用沙猫制作的示例，如果我们只是编写一个普通程序，也许
example that I made with sand cat if we

124
00:04:19,168 --> 00:04:20,920
我用沙猫制作的示例，如果我们只是编写一个普通程序，也许
just write a normal program maybe we

125
00:04:21,120 --> 00:04:22,718
希望完成将内容打印到控制台的工作，我们会说
want work to be done that prints

126
00:04:22,918 --> 00:04:24,699
希望完成将内容打印到控制台的工作，我们会说
something to the console we'll say it

127
00:04:24,899 --> 00:04:26,410
像这样打印到理事会的工作，这将
prints working to

128
00:04:26,610 --> 00:04:29,110
像这样打印到理事会的工作，这将
council just like that and it's going to

129
00:04:29,310 --> 00:04:31,300
永远保持运行状态我们本质上想永远运行
kind of keep running forever we

130
00:04:31,500 --> 00:04:32,790
永远保持运行状态我们本质上想永远运行
essentially wanted to just run forever

131
00:04:32,990 --> 00:04:35,410
所以如果我们想写这样的东西，我会把它放在这里
so I'll put this into here if we were to

132
00:04:35,610 --> 00:04:37,899
所以如果我们想写这样的东西，我会把它放在这里
want to write something like this that

133
00:04:38,098 --> 00:04:39,639
执行发生的任何类型的工作，直到我们告诉它停止为止
performs any kind of work that happens

134
00:04:39,839 --> 00:04:41,980
执行发生的任何类型的工作，直到我们告诉它停止为止
until we tell it to stop by maybe

135
00:04:42,180 --> 00:04:43,809
点击进入我们将如何做的很好，我们知道我们
hitting enter how would we go about

136
00:04:44,009 --> 00:04:45,850
点击进入我们将如何做的很好，我们知道我们
doing this well we know that what we

137
00:04:46,050 --> 00:04:48,730
可以做的是，我们可以说Manuela Casey，但是不明白，也许我们想要这个
could do is we could say Manuela Casey

138
00:04:48,930 --> 00:04:50,949
可以做的是，我们可以说Manuela Casey，但是不明白，也许我们想要这个
and don't get so maybe we want this

139
00:04:51,149 --> 00:04:53,429
while循环继续进行，直到说得到实际返回的东西，因为说
while loop to continue until saying get

140
00:04:53,629 --> 00:04:55,838
while循环继续进行，直到说得到实际返回的东西，因为说
actually return something because saying

141
00:04:56,038 --> 00:04:58,088
得到我们只是等我们基本上按Enter键，所以我们要写一个
get we'll just wait for us to basically

142
00:04:58,288 --> 00:05:00,369
得到我们只是等我们基本上按Enter键，所以我们要写一个
press Enter so we want to write a

143
00:05:00,569 --> 00:05:04,569
等到我们按ENTER的程序，但是等到我们的代码行
program that waits until we press ENTER

144
00:05:04,769 --> 00:05:07,869
等到我们按ENTER的程序，但是等到我们的代码行
but the line of code that waits until we

145
00:05:08,069 --> 00:05:08,709
按Enter阻止执行，因为它正在等待
press Enter

146
00:05:08,908 --> 00:05:11,379
按Enter阻止执行，因为它正在等待
blocks execution because it's waiting

147
00:05:11,579 --> 00:05:12,790
让我们按ENTER键，使其实际上无法连续打印
for us to press ENTER so it can't

148
00:05:12,990 --> 00:05:15,100
让我们按ENTER键，使其实际上无法连续打印
actually print working continuously

149
00:05:15,300 --> 00:05:16,899
因为它正在等待我们按ENTER，所以在一个线程上这不会
because it's waiting for us to press

150
00:05:17,098 --> 00:05:18,879
因为它正在等待我们按ENTER，所以在一个线程上这不会
ENTER so on one thread this doesn't

151
00:05:19,079 --> 00:05:20,379
真正的工作，我们需要能够同时做两件事
really work we need to be able to do two

152
00:05:20,579 --> 00:05:22,088
真正的工作，我们需要能够同时做两件事
things at the same time we want to be

153
00:05:22,288 --> 00:05:24,730
能够等待用户按下ENTER并反复检查以查看是否有该用户
able to wait for the user to press ENTER

154
00:05:24,930 --> 00:05:26,709
能够等待用户按下ENTER并反复检查以查看是否有该用户
and repeatedly check to see has the user

155
00:05:26,908 --> 00:05:29,230
按ENTER键，然后我们也想将工作记录到控制台，所以让我们看一下
press ENTER but then we also want to log

156
00:05:29,430 --> 00:05:31,629
按ENTER键，然后我们也想将工作记录到控制台，所以让我们看一下
working to the console so let's look at

157
00:05:31,829 --> 00:05:33,278
一种我们可以解决这个问题的方法，所以我要做的是编写一个静态
one way that we might solve this so what

158
00:05:33,478 --> 00:05:35,050
一种我们可以解决这个问题的方法，所以我要做的是编写一个静态
I'm going to do is write a static

159
00:05:35,250 --> 00:05:39,910
布尔值在这里称为s工作，或者好吧，我们称其为完成，我将
boolean here called s working and or

160
00:05:40,110 --> 00:05:42,249
布尔值在这里称为s工作，或者好吧，我们称其为完成，我将
well we'll call it s finished and I'll

161
00:05:42,449 --> 00:05:44,139
将其设置为false，因为我们还没有完成，然后进行while循环
set it equal to false because we haven't

162
00:05:44,338 --> 00:05:45,910
将其设置为false，因为我们还没有完成，然后进行while循环
finished yet and then this while loop

163
00:05:46,110 --> 00:05:48,189
将一直运行直到完成将其设置为true为止，因此在未完成时继续打印
will run until finished has been set to

164
00:05:48,389 --> 00:05:50,619
将一直运行直到完成将其设置为true为止，因此在未完成时继续打印
true so while not finished keep printing

165
00:05:50,819 --> 00:05:53,259
工作，然后我要在这里做的是Pesta D集团
out working and then what I'm going to

166
00:05:53,459 --> 00:05:57,519
工作，然后我要在这里做的是Pesta D集团
do over here is Pesta D syndicate which

167
00:05:57,718 --> 00:05:59,949
阻止此线程，直到我们按Enter并按ENTER之后我就进入了
blocks this thread until we press ENTER

168
00:06:00,149 --> 00:06:02,259
阻止此线程，直到我们按Enter并按ENTER之后我就进入了
and after we press ENTER I'm on a set

169
00:06:02,459 --> 00:06:04,028
完成到真实，然后我要去等待线程上的这项工作
finish to true and then I'm gonna go

170
00:06:04,228 --> 00:06:05,199
完成到真实，然后我要去等待线程上的这项工作
just wait for this work at thread to

171
00:06:05,399 --> 00:06:07,329
加入，所以如果我们同时观察这种情况以及如何
join so what's happening here if we look

172
00:06:07,528 --> 00:06:08,829
加入，所以如果我们同时观察这种情况以及如何
at this kind of concurrently and how

173
00:06:09,028 --> 00:06:10,899
它实际上正在运行，这是在控制台上以最快的速度进行打印
it's actually running this is printing

174
00:06:11,098 --> 00:06:12,850
它实际上正在运行，这是在控制台上以最快的速度进行打印
working as fast as I can to the console

175
00:06:13,050 --> 00:06:15,850
这就是该线程正在等待我们按下Enter键的全部
and that's all it's doing this thread is

176
00:06:16,050 --> 00:06:17,468
这就是该线程正在等待我们按下Enter键的全部
waiting for us to press the Enter key

177
00:06:17,668 --> 00:06:20,739
当我们提前到第19行时，下一个就完成了
and when we do in advances to line 19

178
00:06:20,939 --> 00:06:22,838
当我们提前到第19行时，下一个就完成了
which that's finished it true the next

179
00:06:23,038 --> 00:06:24,910
当此检查检查完成状态时，将看到已设置
time this checks checks the state of

180
00:06:25,110 --> 00:06:27,519
当此检查检查完成状态时，将看到已设置
finished it will see that it's been set

181
00:06:27,718 --> 00:06:30,429
为真，这意味着我已经完成，所以它将超越这一步
to true which means that I'm finished so

182
00:06:30,629 --> 00:06:32,110
为真，这意味着我已经完成，所以它将超越这一步
it's going to advance past this while

183
00:06:32,310 --> 00:06:33,879
循环并在函数的末尾，因此线程已完成，这是
loop and at the end of the function thus

184
00:06:34,079 --> 00:06:35,769
循环并在函数的末尾，因此线程已完成，这是
that thread has finished and this is

185
00:06:35,968 --> 00:06:37,569
将确保我们直到那个线程都看不到
going to make sure that we don't do see

186
00:06:37,769 --> 00:06:40,120
将确保我们直到那个线程都看不到
and get until that thread

187
00:06:40,319 --> 00:06:42,699
实际上已经完成了执行，所以如果我们按f5并说出
has actually finished its execution so

188
00:06:42,899 --> 00:06:44,530
实际上已经完成了执行，所以如果我们按f5并说出
if we just hit f5 and say what this

189
00:06:44,730 --> 00:06:46,240
实际上，您是否可以在这里看到重复打印步行以及何时
actually does you can see over here that

190
00:06:46,439 --> 00:06:47,860
实际上，您是否可以在这里看到重复打印步行以及何时
is repeatedly printing walking and when

191
00:06:48,060 --> 00:06:50,230
我按ENTER就是这样，它将停止打印，如果我按ENTER
I press ENTER that's it it stops

192
00:06:50,430 --> 00:06:51,520
我按ENTER就是这样，它将停止打印，如果我按ENTER
printing working and if I press ENTER

193
00:06:51,720 --> 00:06:53,020
再次它将终止我们的程序，因为我们还有另一个场景
again it's going to terminate our

194
00:06:53,220 --> 00:06:54,730
再次它将终止我们的程序，因为我们还有另一个场景
program because we have another scene

195
00:06:54,930 --> 00:06:56,259
没有结束在这里等待，直到它关闭我们的程序对不起
not get over here which waits for that

196
00:06:56,459 --> 00:06:58,990
没有结束在这里等待，直到它关闭我们的程序对不起
before it just closes our program sorry

197
00:06:59,189 --> 00:07:00,430
也许与其说打印速度尽可能快，不如说
maybe instead of printing working as

198
00:07:00,629 --> 00:07:02,050
也许与其说打印速度尽可能快，不如说
fast as possible we might want to say

199
00:07:02,250 --> 00:07:04,270
好吧，让我们稍等一下，多线程是
well actually let's wait for a bit

200
00:07:04,470 --> 00:07:06,009
好吧，让我们稍等一下，多线程是
the thing with multi-threading is that

201
00:07:06,209 --> 00:07:08,110
如果您持续不断地做这样的事情，那就好了
if you continually do things as fast as

202
00:07:08,310 --> 00:07:10,300
如果您持续不断地做这样的事情，那就好了
possible like this it is going to kind

203
00:07:10,500 --> 00:07:12,460
导致该线程的CPU使用率达到100％，这不是很好
of result in like a hundred percent CPU

204
00:07:12,660 --> 00:07:14,139
导致该线程的CPU使用率达到100％，这不是很好
usage for that thread which isn't great

205
00:07:14,339 --> 00:07:15,730
所以我们能做的是因为他们会等一会儿，而我要去做
so what we can do is because they will

206
00:07:15,930 --> 00:07:17,889
所以我们能做的是因为他们会等一会儿，而我要去做
wait for a bit and what I'm going to do

207
00:07:18,089 --> 00:07:20,770
这只是告诉当前线程正在睡眠，所以我要写睡眠
here is just tell this current thread to

208
00:07:20,970 --> 00:07:22,449
这只是告诉当前线程正在睡眠，所以我要写睡眠
sleep so I'm going to write asleep

209
00:07:22,649 --> 00:07:25,629
之前然后是一个代表一秒钟的秒，以便编写代码
before and then one s which stands for

210
00:07:25,829 --> 00:07:27,370
之前然后是一个代表一秒钟的秒，以便编写代码
one second and in order to write code

211
00:07:27,569 --> 00:07:28,600
这样，我将不得不使用命名空间Pro知道字面量，所以
like that I'm gonna have to using

212
00:07:28,800 --> 00:07:31,810
这样，我将不得不使用命名空间Pro知道字面量，所以
namespace Pro know literals okay so

213
00:07:32,009 --> 00:07:33,400
基本上我只是说在您打印此行代码之后
basically I'm just saying after you

214
00:07:33,600 --> 00:07:35,410
基本上我只是说在您打印此行代码之后
print this working line of code just

215
00:07:35,610 --> 00:07:37,810
睡眠一秒钟，然后返回while循环并继续打印
sleep for a second before you go back up

216
00:07:38,009 --> 00:07:39,879
睡眠一秒钟，然后返回while循环并继续打印
to this while loop and continue printing

217
00:07:40,079 --> 00:07:43,300
工作直到完成设置为true，所以如果我现在按f5，我们应该开始工作
working until finished is set to true so

218
00:07:43,500 --> 00:07:45,370
工作直到完成设置为true，所以如果我现在按f5，我们应该开始工作
if I hit f5 now we should get working

219
00:07:45,569 --> 00:07:47,560
每秒打印一次到控制台，然后我们就可以看到
printing to the console once every

220
00:07:47,759 --> 00:07:49,329
每秒打印一次到控制台，然后我们就可以看到
second and there we go you can see that

221
00:07:49,529 --> 00:07:51,430
像工作中一样滴答作响，如果我按ENTER，就这样
it's ticking like that working working

222
00:07:51,629 --> 00:07:54,400
像工作中一样滴答作响，如果我按ENTER，就这样
and if I press ENTER then that's it it's

223
00:07:54,600 --> 00:07:56,079
完成，实际上，我们也许只写一些东西来表明它是
done and in fact let's just maybe write

224
00:07:56,279 --> 00:07:58,120
完成，实际上，我们也许只写一些东西来表明它是
something to just indicate that it's

225
00:07:58,319 --> 00:08:00,759
这样，在工作邻接之后，我将完成打印，让我们看看它的外观
done so after work adjoin I'll print

226
00:08:00,959 --> 00:08:05,710
这样，在工作邻接之后，我将完成打印，让我们看看它的外观
finished and let's see what that looks

227
00:08:05,910 --> 00:08:07,090
像这样，我们开始进行ENTER工作，您可以看到我们已经
like so there we go working working

228
00:08:07,290 --> 00:08:08,860
像这样，我们开始进行ENTER工作，您可以看到我们已经
working ENTER and you can see we've

229
00:08:09,060 --> 00:08:10,480
完成，这是我们执行的结束，所以这是一个非常基本的示例
finished and that is the end of our

230
00:08:10,680 --> 00:08:13,210
完成，这是我们执行的结束，所以这是一个非常基本的示例
execution so that's a very basic example

231
00:08:13,410 --> 00:08:15,730
关于线程如何工作的说法可能毫无用处，但是这段视频的重点是
of how threads work arguably fairly

232
00:08:15,930 --> 00:08:17,439
关于线程如何工作的说法可能毫无用处，但是这段视频的重点是
useless but the point of this video is

233
00:08:17,639 --> 00:08:18,910
首先向您展示如何在C ++ API中访问线程
just to show you first of all how you

234
00:08:19,110 --> 00:08:20,920
首先向您展示如何在C ++ API中访问线程
can access threads in the C++ API

235
00:08:21,120 --> 00:08:22,750
当然，这最终会涉及到平台特定的代码，但是您只需要
of course this ends up boiling down to

236
00:08:22,949 --> 00:08:24,939
当然，这最终会涉及到平台特定的代码，但是您只需要
platform specific code but you just need

237
00:08:25,139 --> 00:08:26,920
包括线程，以及当您通过输入城市来创建线程对象时
to include thread and when you just make

238
00:08:27,120 --> 00:08:29,110
包括线程，以及当您通过输入城市来创建线程对象时
a thread object by typing in a city

239
00:08:29,310 --> 00:08:30,850
线程，然后从中创建变量，然后将函数传递给
thread and then you make a variable out

240
00:08:31,050 --> 00:08:32,169
线程，然后从中创建变量，然后将函数传递给
of it and you pass in a function to the

241
00:08:32,370 --> 00:08:34,028
构造函数，该线程立即启动该线程以完成您所做的任何工作
constructor that immediately kicks off

242
00:08:34,229 --> 00:08:35,769
构造函数，该线程立即启动该线程以完成您所做的任何工作
that thread doing whatever work you've

243
00:08:35,969 --> 00:08:38,468
到达它，该线程将继续直到完成为止，您始终可以像
made it to and that thread will continue

244
00:08:38,668 --> 00:08:41,079
到达它，该线程将继续直到完成为止，您始终可以像
until it's done you can always use like

245
00:08:41,279 --> 00:08:43,750
该线程加入以实际等待启动的当前线程
that thread join to actually wait for

246
00:08:43,950 --> 00:08:45,609
该线程加入以实际等待启动的当前线程
the current thread that's kicked off

247
00:08:45,809 --> 00:08:48,009
第二个线程实际上完成了执行，以防您需要
that second thread to actually finish

248
00:08:48,210 --> 00:08:50,049
第二个线程实际上完成了执行，以防您需要
its execution in case you need to do

249
00:08:50,250 --> 00:08:51,699
进行某种清理或等待实际程序终止
some kind of clean up or wait like this

250
00:08:51,899 --> 00:08:53,809
进行某种清理或等待实际程序终止
for the actual program to terminate

251
00:08:54,009 --> 00:08:57,589
在所有线程完成之后，就API而言，实际上并没有
after all the threads are done that's as

252
00:08:57,789 --> 00:08:59,449
在所有线程完成之后，就API而言，实际上并没有
far as the API goes there's really not a

253
00:08:59,649 --> 00:09:01,069
您可以做的很多事情不需要做很多事情
lot of things you can do there's not a

254
00:09:01,269 --> 00:09:02,419
您可以做的很多事情不需要做很多事情
lot of things you need to do the concept

255
00:09:02,620 --> 00:09:03,679
的线程确实非常简单，但是却变得非常复杂
of threads is really really simple but

256
00:09:03,879 --> 00:09:05,359
的线程确实非常简单，但是却变得非常复杂
it gets really complicated really

257
00:09:05,559 --> 00:09:07,819
就像许多其他事情一样，很快就可以在C ++中执行该线程操作
quickly like a lot of things do in C++

258
00:09:08,019 --> 00:09:10,250
就像许多其他事情一样，很快就可以在C ++中执行该线程操作
the sed this thread thing as well can

259
00:09:10,450 --> 00:09:15,709
只要确保好，如果我只是在这里使用了这个线程对象
just make sure well there's also a this

260
00:09:15,909 --> 00:09:17,689
只要确保好，如果我只是在这里使用了这个线程对象
thread object that I used here if I just

261
00:09:17,889 --> 00:09:19,549
再次拿起我的笔记本电脑，我们有这个sed这个线程，您可以用来
grab my laptop again we have this sed

262
00:09:19,750 --> 00:09:21,169
再次拿起我的笔记本电脑，我们有这个sed这个线程，您可以用来
this thread which you can use to

263
00:09:21,370 --> 00:09:23,419
实际上基本上是向当前线程提供命令，因为您可以说
actually basically give commands to the

264
00:09:23,620 --> 00:09:24,769
实际上基本上是向当前线程提供命令，因为您可以说
current thread because you can say that

265
00:09:24,970 --> 00:09:26,149
即使在这个线程中，我也无法访问该线程对象
in this thread I don't have access to

266
00:09:26,350 --> 00:09:28,189
即使在这个线程中，我也无法访问该线程对象
this thread object even though I'm

267
00:09:28,389 --> 00:09:29,929
实际上就可以了，例如，我可以做的只是打印线程ID
actually on it so what I could do for

268
00:09:30,129 --> 00:09:32,299
实际上就可以了，例如，我可以做的只是打印线程ID
example is just print the thread ID that

269
00:09:32,500 --> 00:09:35,299
我正在通话，所以我可能会说CDCR之类的话
I'm on so I might say something like s

270
00:09:35,500 --> 00:09:36,169
我正在通话，所以我可能会说CDCR之类的话
CDCR

271
00:09:36,370 --> 00:09:40,549
开始的线程ID等于，然后输入以下内容来打印实际的线程ID
started thread ID equals and then I'll

272
00:09:40,750 --> 00:09:42,529
开始的线程ID等于，然后输入以下内容来打印实际的线程ID
print the actual thread ID by typing in

273
00:09:42,730 --> 00:09:46,339
一个城市，这个线程获取ID，然后我将复制该行代码并打印
a city this thread get ID and then I'll

274
00:09:46,539 --> 00:09:49,329
一个城市，这个线程获取ID，然后我将复制该行代码并打印
copy that line of code and also print it

275
00:09:49,529 --> 00:09:52,809
也许我们在这里结束之后，这些机构应该有所不同
maybe after we finish over here and

276
00:09:53,009 --> 00:09:55,159
也许我们在这里结束之后，这些机构应该有所不同
these authorities should be different

277
00:09:55,360 --> 00:09:56,120
因为它们运行在不同的线程上，所以如果我按f5键运行程序
because they're running on different

278
00:09:56,320 --> 00:09:58,549
因为它们运行在不同的线程上，所以如果我按f5键运行程序
threads so if I hit f5 to run my program

279
00:09:58,750 --> 00:10:00,949
您可以看到我们从线程ID 8 3 4 8开始，然后如果我按Enter键，您可以看到
you can see we start at thread ID 8 3 4

280
00:10:01,149 --> 00:10:02,870
您可以看到我们从线程ID 8 3 4 8开始，然后如果我按Enter键，您可以看到
8 and then if I hit enter you can see

281
00:10:03,070 --> 00:10:04,639
这是一个完全不同的ID，因为它们实际上位于
it's a completely different ID because

282
00:10:04,840 --> 00:10:06,319
这是一个完全不同的ID，因为它们实际上位于
they're actually on different threads of

283
00:10:06,519 --> 00:10:08,029
执行中，我们创建了另一个线程以将我们的工作内容打印到
execution we've created another thread

284
00:10:08,230 --> 00:10:10,429
执行中，我们创建了另一个线程以将我们的工作内容打印到
to print our working stuff to the

285
00:10:10,629 --> 00:10:12,949
控制台，所以线程非常重要，我们将要讨论的是
console so yeah threads are really

286
00:10:13,149 --> 00:10:14,539
控制台，所以线程非常重要，我们将要讨论的是
important we're going to be talking a

287
00:10:14,740 --> 00:10:15,769
将来会有更多关于它们的信息，它们对于加速速度确实很有用
lot more about them in the future

288
00:10:15,970 --> 00:10:17,269
将来会有更多关于它们的信息，它们对于加速速度确实很有用
they're really useful for speeding up

289
00:10:17,470 --> 00:10:19,069
您的程序线程的主要目的是优化，而不仅仅是您
your program the primary purpose of

290
00:10:19,269 --> 00:10:21,919
您的程序线程的主要目的是优化，而不仅仅是您
threads is optimisation but not only you

291
00:10:22,120 --> 00:10:23,389
也可以使用它们来完成我们今天无法做到的事情
can also use them to do things like we

292
00:10:23,590 --> 00:10:25,399
也可以使用它们来完成我们今天无法做到的事情
did today that isn't really possible to

293
00:10:25,600 --> 00:10:27,379
这样做是因为我们实际上需要能够同时做两件事
do because we literally need to be able

294
00:10:27,580 --> 00:10:28,429
这样做是因为我们实际上需要能够同时做两件事
to do two things at once

295
00:10:28,629 --> 00:10:30,979
如果您确实有自己的发送狗门功能，那么您当然可以选择
if you did have your own send dog gate

296
00:10:31,179 --> 00:10:32,449
如果您确实有自己的发送狗门功能，那么您当然可以选择
function you could of course alternate

297
00:10:32,649 --> 00:10:34,279
在工作之间，我的意思是让我们看看用户是否按Enter
between work what I mean by that is

298
00:10:34,480 --> 00:10:36,319
在工作之间，我的意思是让我们看看用户是否按Enter
let's see if the users press ENTER if

299
00:10:36,519 --> 00:10:38,359
不要让我们在控制台上打印工作，然后像您可以的那样再次进行操作
not let's print working to the console

300
00:10:38,559 --> 00:10:40,579
不要让我们在控制台上打印工作，然后像您可以的那样再次进行操作
then we do that again like you can do it

301
00:10:40,779 --> 00:10:42,349
在一个您看不到的线程上，因为它阻塞了整个线程，但是
on one thread you can't with seen yet

302
00:10:42,549 --> 00:10:44,659
在一个您看不到的线程上，因为它阻塞了整个线程，但是
because it blocks the entire thread but

303
00:10:44,860 --> 00:10:46,129
如果您有自己的方法，那么没有理由您不能这样做，那就是
if you had your own method there's no

304
00:10:46,330 --> 00:10:47,809
如果您有自己的方法，那么没有理由您不能这样做，那就是
reason why you couldn't do that so it's

305
00:10:48,009 --> 00:10:48,889
不像事物是完全不可能的，它只是比
not like things are completely

306
00:10:49,090 --> 00:10:50,870
不像事物是完全不可能的，它只是比
impossible it's just more or less than

307
00:10:51,070 --> 00:10:52,759
如果您实际使用线程，则某些事情会容易得多，并且速度也更快
some things a lot easier and a lot

308
00:10:52,960 --> 00:10:55,250
如果您实际使用线程，则某些事情会容易得多，并且速度也更快
faster if you actually utilize threading

309
00:10:55,450 --> 00:10:56,870
这是一个巨大的话题，我们将来会继续探索吗？
and that's a huge topic which we will

310
00:10:57,070 --> 00:10:58,969
这是一个巨大的话题，我们将来会继续探索吗？
continue to explore in the future do you

311
00:10:59,169 --> 00:11:00,139
像这个视频的家伙，您可以点击“赞”按钮，也可以帮助支持
guys like this video you can hit the

312
00:11:00,340 --> 00:11:01,279
像这个视频的家伙，您可以点击“赞”按钮，也可以帮助支持
like button you can also help support

313
00:11:01,480 --> 00:11:02,719
该系列文章转到patreon.com/scishow，说明中将没有链接。
this series by go to patreon.com/scishow

314
00:11:02,919 --> 00:11:05,329
该系列文章转到patreon.com/scishow，说明中将没有链接。
no link will be in the description huge

315
00:11:05,529 --> 00:11:07,078
一如既往地感谢所有赞助该系列产品的顾客
thank you as always to all the patron

316
00:11:07,278 --> 00:11:09,269
一如既往地感谢所有赞助该系列产品的顾客
support this series they would not be

317
00:11:09,470 --> 00:11:12,269
在没有你的情况下，我要离开这片森林，直到它变得黑暗而又黑暗
here without you I'm gonna get out of

318
00:11:12,470 --> 00:11:14,308
在没有你的情况下，我要离开这片森林，直到它变得黑暗而又黑暗
this forest before it gets too dark and

319
00:11:14,509 --> 00:11:17,699
下次再见。[音乐]
I'll see you guys next time goodbye

320
00:11:17,899 --> 00:11:22,899
下次再见。[音乐]
[Music]

