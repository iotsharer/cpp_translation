﻿1
00:00:00,000 --> 00:00:03,009
hey what's up guys my name is eterno and welcome back to my safe last class
嘿，大家好，我叫eterno，欢迎回到我安全的上课

2
00:00:03,209 --> 00:00:06,069
series last time we talked about virtual functions and today I'm going to be
上一次我们讨论虚拟函数的系列，今天我将成为

3
00:00:06,269 --> 00:00:09,700
talking about specific types of virtual functions only called a pure virtual
谈论特定类型的虚函数，仅称为纯虚函数

4
00:00:09,900 --> 00:00:13,660
function a pure virtual function in C++ is essentially the same as an abstract
函数C ++中的纯虚函数本质上与抽象函数相同

5
00:00:13,859 --> 00:00:17,199
method or an interface in other languages such as Java or C sharp
其他语言（例如Java或C Sharp）的方法或接口

6
00:00:17,399 --> 00:00:21,850
basically a pure virtual function allows us to define a function in a base class
基本上，纯虚函数允许我们在基类中定义函数

7
00:00:22,050 --> 00:00:26,829
that does not have an implementation and then for subclasses to actually
没有实现，然后让子类实际

8
00:00:27,028 --> 00:00:30,370
implement that function if we take a look at our example from the previous
如果我们看一下前面的例子，可以实现该功能

9
00:00:30,570 --> 00:00:33,698
video which is about virtual functions you can see that we had a virtual
有关虚拟功能的视频，您可以看到我们有一个虚拟的

10
00:00:33,899 --> 00:00:38,229
getname function inside the entity class and then we had an override for that
实体类中的getname函数，然后对此进行了覆盖

11
00:00:38,429 --> 00:00:41,890
function inside our player class by having this get named function inside
通过在此播放器类中获取命名函数来实现该功能

12
00:00:42,090 --> 00:00:46,239
our base class with an actual body means that overriding it in a subclass such as
我们的带有实际主体的基类意味着将其覆盖在子类中，例如

13
00:00:46,439 --> 00:00:50,439
player is entirely optional if we didn't override it then we could still call
播放器是完全可选的，如果我们不覆盖它，那么我们仍然可以调用

14
00:00:50,640 --> 00:00:54,579
play it off get name however it would of course return the anti string however in
播放掉获取名称，但是它当然会返回反字符串

15
00:00:54,780 --> 00:00:59,349
some cases it doesn't make sense for us to provide this default implementation
在某些情况下，对我们而言，提供此默认实现是没有意义的

16
00:00:59,549 --> 00:01:02,858
we actually might want to force the subclass to actually provide its own
我们实际上可能想强制子类实际提供自己的子类

17
00:01:03,058 --> 00:01:06,909
definition for a certain function in object-oriented programming is actually
面向对象编程中某个功能的定义实际上是

18
00:01:07,109 --> 00:01:10,539
quite common for us to create a class that consists only of unimplemented
对于我们来说，创建一个只包含未实现的类的类非常普遍

19
00:01:10,739 --> 00:01:14,230
methods and then force a subclass to actually implement them this is
方法，然后强制子类实际实现它们，这是

20
00:01:14,430 --> 00:01:17,649
something that often referred to as an interface so in interstates being a
通常被称为接口的东西，因此在州际

21
00:01:17,849 --> 00:01:21,519
class that only consists of unimplemented methods and acting as a
类仅包含未实现的方法，并充当

22
00:01:21,719 --> 00:01:25,480
template of sorts and since this interface class doesn't actually contain
模板，并且由于此接口类实际上不包含

23
00:01:25,680 --> 00:01:30,459
method implementations it's not actually possible for us to instantiate that
方法实现，实际上我们无法实例化

24
00:01:30,659 --> 00:01:34,390
class let's see if we can make this get main function inside and see a pure
该类让我们看看是否可以使它在内部获得主函数并看到一个纯

25
00:01:34,590 --> 00:01:37,689
virtual function to do that I'm just going to get rid of the body and instead
虚拟函数可以做到这一点，我将摆脱身体，取而代之

26
00:01:37,890 --> 00:01:41,890
of the body just write equal zero keep in mind that it still has to be
的身体只是写等于零，请记住，它仍然必须是

27
00:01:42,090 --> 00:01:46,448
defined as virtual but the equals zero is essentially making it a pure virtual
定义为虚拟但等于0本质上使其成为纯虚拟

28
00:01:46,649 --> 00:01:50,980
function meaning that it has to be implemented in a soft class if you want
函数的意思是，如果需要，它必须在一个软类中实现

29
00:01:51,180 --> 00:01:54,609
to be able to instantiate that class so by doing this a few things have actually
能够实例化该类，因此通过执行此操作，实际上

30
00:01:54,810 --> 00:01:58,390
happened first of all if you look at main you can see that we now no longer
首先，如果您查看主要事件，您会发现我们现在不再

31
00:01:58,590 --> 00:02:03,039
have the ability to actually instantiate that entity class we must give it some
有能力实例化该实体类，我们必须给它一些

32
00:02:03,239 --> 00:02:07,659
kind of subclass that actually has that function implemented so in this case
实际上实现了该功能的一种子类，因此在这种情况下

33
00:02:07,859 --> 00:02:11,010
player for example and of course when to provide some kind of string
例如播放器，当然还有何时提供某种字符串

34
00:02:11,210 --> 00:02:14,610
as well you can see the player works fine however that's only because we've
您也可以看到播放器运行正常，但这仅仅是因为我们已经

35
00:02:14,810 --> 00:02:19,140
actually implemented that getname function if I decide to comment out that
如果我决定将其注释掉，实际上实现了该getname函数

36
00:02:19,340 --> 00:02:24,000
implementation you can see this we can't instantiate player Evo so essentially
实施过程中，您可以看到我们无法从本质上实例化播放器Evo 

37
00:02:24,199 --> 00:02:28,650
you can only instantiate the class if it actually has all those pure virtual
您只能实例化该类，如果它实际上具有所有这些纯虚拟的

38
00:02:28,849 --> 00:02:31,890
functions implemented or a class was further up in the hierarchy it's the for
实现的功能或类在层次结构中更进一步

39
00:02:32,090 --> 00:02:35,880
example player war to be a soft class of another class which did implement that
举例来说，玩家之战就是实现了该目标的另一个职业的软阶级

40
00:02:36,080 --> 00:02:40,500
get name function that would also be fine but the idea is that get name that
获取名称功能也可以，但是这个想法是获取名称

41
00:02:40,699 --> 00:02:44,460
pure virtual function has to actually be implemented in order for us to be able
为了使我们能够真正实现纯虚拟功能

42
00:02:44,659 --> 00:02:47,340
to create an instance of this class all right let's take a look at a better
创建此类的实例好吧，让我们看一个更好的

43
00:02:47,539 --> 00:02:50,880
example I'm going to undo everything that we had done here
例子我要撤消我们在这里所做的一切

44
00:02:51,080 --> 00:02:53,640
all right that was pretty good suppose that we wanted to write a function that
很好，很好，假设我们要编写一个函数

45
00:02:53,840 --> 00:02:57,660
would print the class name of every class that we have here so what I'll do
将打印我们在这里拥有的每个班级的班级名称，所以我该怎么做

46
00:02:57,860 --> 00:03:02,280
is outside void print as my parameter I'm going to want to take in some kind
在void print之外，这是我要采用的某种参数

47
00:03:02,479 --> 00:03:07,650
of class type that will call obj for example and then what I'm going to want
类类型，它将例如调用obj，然后是我想要的

48
00:03:07,849 --> 00:03:12,180
to do is actually print the class's name so maybe we have something like obj dot
要做的实际上是打印类的名称，所以也许我们有类似obj dot之类的东西

49
00:03:12,379 --> 00:03:18,180
get class name so what I actually need here is a type that guarantees that we
得到类名，所以我在这里实际需要的是一种可以确保我们

50
00:03:18,379 --> 00:03:22,620
actually have a get class name function we need a type that provides that get
实际上有一个get类名函数，我们需要一个提供get 

51
00:03:22,819 --> 00:03:26,580
class name function and that's exactly what an interface is let's go ahead and
类名函数，而这正是接口的作用，然后继续

52
00:03:26,780 --> 00:03:30,960
call this type principal and we'll set it off so at the very top here I'll make
称这个类型为principal，我们将其关闭，所以在这里，我将使

53
00:03:31,159 --> 00:03:34,950
a new file called printable and the only thing this is going to have I'm gonna
一个名为printable的新文件，这是我要拥有的唯一东西

54
00:03:35,150 --> 00:03:41,160
make a public virtual string function the return of string here get class name
制作一个公共的虚拟字符串函数，在这里返回字符串，获取类名

55
00:03:41,360 --> 00:03:45,210
and it's going to be pure virtual so I'm going to set it equal to zero then I'm
而且它将是纯虚拟的，所以我将其设置为零，然后

56
00:03:45,409 --> 00:03:49,860
going to make entity implement that interstate notice that player is already
将使实体实施该州际通知，玩家已经

57
00:03:50,060 --> 00:03:54,210
an entity so we don't have to implement printable however if it wasn't for
一个实体，因此我们不必实现可打印，但是如果不是

58
00:03:54,409 --> 00:03:57,600
whatever reason we could just add a comma and then implement an intersect
不管什么原因，我们可以只添加一个逗号然后实现一个相交

59
00:03:57,800 --> 00:04:01,680
like so gives mind that I keep calling as an interface but of course it is just
像这样让我记住，我一直在调用作为接口，但是当然

60
00:04:01,879 --> 00:04:05,670
a class there is not even at the name interface series because all it has is
一个名称接口系列甚至都没有的类，因为它所拥有的只是

61
00:04:05,870 --> 00:04:09,810
that pure virtual function and that is it other languages actually do have an
纯粹的虚函数，就是其他语言确实有一个

62
00:04:10,009 --> 00:04:13,680
interface keyword instead of class but these likewise does not interfaces are
接口关键字而不是类，但是这些同样不是接口

63
00:04:13,879 --> 00:04:17,250
just classes in a set of laws so now everything has to have this get class
只是按照一套法律分类，所以现在所有的东西都必须有这个分类

64
00:04:17,449 --> 00:04:21,030
name function if it does not you can't actually instantiate it let's go ahead
名称函数（如果没有）实际上无法实例化，让我们继续

65
00:04:21,230 --> 00:04:25,259
and add a get clasp and function forenski now if
并添加获取扣和功能forenski现在如果

66
00:04:25,459 --> 00:04:30,000
we were to do what we did here in sight player I'll just write stds string and
我们要做的就是在视线播放器中做的事情，我只写stds字符串， 

67
00:04:30,199 --> 00:04:34,650
get class name return entity then we actually have a few issues first of all
获取类名返回实体，那么实际上我们首先遇到了一些问题

68
00:04:34,850 --> 00:04:37,710
our problem of not being able to instantiate entity is solved of course
我们无法实例化实体的问题当然已经解决了

69
00:04:37,910 --> 00:04:41,220
because we have provided that function however you'll notice that we haven't
因为我们提供了该功能，但是您会注意到我们没有

70
00:04:41,420 --> 00:04:45,270
provided one for player so if I go down to print let's make this a pointer and
为玩家提供了一个，所以如果我要打印的话，让我们将其作为指针

71
00:04:45,470 --> 00:04:51,689
actually call print with both player and entity we'll get rid of print names that
实际上使用玩家和实体来调用打印，我们将摆脱打印名称， 

72
00:04:51,889 --> 00:04:55,560
we're not confused here here five of course you can see that we get entity
我们在这里不感到困惑，当然有五个您可以看到我们得到了实体

73
00:04:55,759 --> 00:04:58,439
printing twice because we haven't actually provided a definition in player
打印两次，因为我们实际上尚未在播放器中提供定义

74
00:04:58,639 --> 00:05:02,550
however if we do so if I just try and copy this get class name which of course
但是，如果我们这样做，那么我尝试复制此获取类名，这当然是

75
00:05:02,750 --> 00:05:07,199
is an override let's not forget that if I pop that down here and rename this to
是一个替代，我们不要忘记，如果我在此处将其弹出并重命名为

76
00:05:07,399 --> 00:05:10,920
player I can hit up five here to run my code and you can see that I now get the
玩家，我可以在这里加5来运行我的代码，您可以看到我现在得到了

77
00:05:11,120 --> 00:05:14,759
appropriate class name printing and all of that is coming from that one print
适当的类名打印，所有这些都来自该一张打印

78
00:05:14,959 --> 00:05:18,600
function that takes in a printable object it doesn't really care what class
接受可打印对象的函数，它实际上并不在乎什么类

79
00:05:18,800 --> 00:05:23,879
it is we could create a completely different class here such as a that is a
我们可以在这里创建一个完全不同的类，例如a 

80
00:05:24,079 --> 00:05:28,829
printable class meaning that it has to have that function if it does not have
可打印类，意味着它必须具有该功能（如果没有） 

81
00:05:29,029 --> 00:05:33,990
that get class name function then you won't be able to instantiate an instance
获得类名功能，那么您将无法实例化实例

82
00:05:34,189 --> 00:05:37,290
of this class if we do something like this I created a brand new class that
如果我们做这样的事情，我创建了一个全新的课程

83
00:05:37,490 --> 00:05:40,829
made of principle meaning that I have implemented this function I can now call
从原则上讲，我已经实现了此功能，现在可以调用

84
00:05:41,029 --> 00:05:44,850
print like this the course don't write this code exactly because the memory
这样打印当然不会因为内存而写此代码

85
00:05:45,050 --> 00:05:48,449
leaks but for testing its own if I do something like this you can see that we
泄漏，但是如果我自己做这样的测试，可以看到我们

86
00:05:48,649 --> 00:05:53,250
now get that a class printing here and because it is a principal it is
现在在这里得到一个类打印，因为它是一个主体，所以

87
00:05:53,449 --> 00:05:56,490
guaranteed to have that gap class name function that's how this whole thing
保证具有间隙类名称功能，这就是整个过程

88
00:05:56,689 --> 00:06:00,210
works because it knows that anything that is printable how the guest Laughing
之所以起作用，是因为它知道任何可打印的内容，来宾的笑声如何

89
00:06:00,410 --> 00:06:05,189
function for this to actually call if you do not implement that function then
如果您未实现该函数，则此函数将实际调用

90
00:06:05,389 --> 00:06:08,819
you will not be able to instantiate the class so that's it that is what an
您将无法实例化该类，这就是

91
00:06:09,019 --> 00:06:13,020
interface isn't it bus process what a pure virtual function is in C++ very
接口不是总线过程，而是C ++中的纯虚函数

92
00:06:13,220 --> 00:06:17,340
very useful stuff the number one reason is really used is the situations like
非常有用的东西，真正使用的第一个原因是

93
00:06:17,540 --> 00:06:21,420
this where you want to be able to guarantee that if I have a certain
在这里您希望能够保证如果我有一个

94
00:06:21,620 --> 00:06:24,660
function so that you can pass it to a fairly generic method that we'll just
函数，以便您可以将其传递给我们将要使用的相当通用的方法

95
00:06:24,860 --> 00:06:28,439
call that function or do whatever it needs to do hope you guys enjoy this
调用该函数或执行所需的操作，希望大家喜欢

96
00:06:28,639 --> 00:06:31,290
video as always you can leave any questions or comments you have in the
像往常一样，您可以在视频中留下任何问题或评论

97
00:06:31,490 --> 00:06:32,968
comment section below I try to reply toes
下面的评论部分，我尝试回复脚趾

98
00:06:33,168 --> 00:06:37,819
comments as I can and I will see you guys next time goodbye
尽我所能，我下次再见。 

99
00:06:38,019 --> 00:06:43,199
[Music]
[音乐]

100
00:06:46,149 --> 00:06:51,149
[Music]
 [音乐] 

