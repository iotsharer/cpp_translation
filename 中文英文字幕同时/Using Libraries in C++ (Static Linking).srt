﻿1
00:00:00,000 --> 00:00:03,428
hey what's up guys my name is a chana welcome back to my say plus plus series
嘿，大家好我的名字是chana欢迎回到我的发言加系列

2
00:00:03,629 --> 00:00:06,639
festival haircut second of all today I'm gonna be talking
我今天要讲的第二个节日发型

3
00:00:06,839 --> 00:00:10,540
all about libraries in C++ and specifically how we can use external
关于C ++库的所有知识，特别是我们如何使用外部库

4
00:00:10,740 --> 00:00:14,740
libraries in our project so if you used to other languages such as Java or C
库中的项目，因此，如果您习惯于其他语言，例如Java或C 

5
00:00:14,939 --> 00:00:18,490
sharp or Python or something like that adding libraries is a pretty trivial
锐利的或Python的或类似的东西，添加库非常简单

6
00:00:18,690 --> 00:00:21,940
task you might be using a package manager you might not be but either way
您可能正在使用包管理器的任务，但是可能不是

7
00:00:22,140 --> 00:00:26,499
it's usually pretty straightforward when it comes to C++ everyone seems to have
对于每个人似乎都拥有的C ++，这通常非常简单

8
00:00:26,699 --> 00:00:30,818
problems and back in the day I had issues with this as well and I don't
问题，回到过去我也有问题，但我没有

9
00:00:31,018 --> 00:00:34,149
even know why it's there actually really simple and hopefully this video will
甚至不知道为什么它在那里真的非常简单，希望这段视频能够

10
00:00:34,350 --> 00:00:36,909
kind of help clear it up for you but basically there are a few strategies
可以帮助您解决问题，但是基本上有一些策略

11
00:00:37,109 --> 00:00:40,839
that we can actually take when we're dealing with libraries in C++ and I'm
当我们用C ++处理库时，我实际上可以接受的

12
00:00:41,039 --> 00:00:44,919
gonna kind of show you my way as well as maybe discuss some of the other ones but
不仅可以向您展示我的方式，还可以讨论其他一些方式，但是

13
00:00:45,119 --> 00:00:50,409
first of all I hate the idea of package managers and I hate the idea of linking
首先，我讨厌包管理器的想法，也讨厌链接的想法

14
00:00:50,609 --> 00:00:56,198
to kind of other repositories or all that stuff my my ideal project setup is
某种其他存储库或所有这些东西，我理想的项目设置是

15
00:00:56,399 --> 00:01:00,788
that if you check out my repository my code repository from github or whatever
如果您从github或其他地方签出我的存储库我的代码存储库

16
00:01:00,988 --> 00:01:05,319
you should have everything you need in that repository just straight away fee
您应该立即在该存储库中拥有所需的一切， 

17
00:01:05,519 --> 00:01:09,009
for you to actually be able to compile and run the application or the project
使您实际上能够编译和运行应用程序或项目

18
00:01:09,209 --> 00:01:13,329
or whatever right there should be no kind of syncing up with package managers
或其他任何权利，都不应与包管理器进行同步

19
00:01:13,530 --> 00:01:17,168
downloading other libraries all that stuff I hate all of that I just want
下载其他所有我讨厌的东西我讨厌的东西

20
00:01:17,368 --> 00:01:21,099
especially with C++ there are some package managers for other kind of
尤其是对于C ++，有些软件包管理器可用于其他类型的

21
00:01:21,299 --> 00:01:24,250
languages which makes sense I'm specifically obviously talking about C++
有意义的语言我显然是在谈论C ++ 

22
00:01:24,450 --> 00:01:30,308
this is the C++ series but for C++ I just want stuff to work I just want to
这是C ++系列，但对于C ++，我只希望某些东西能正常工作， 

23
00:01:30,509 --> 00:01:34,689
be able to clone the repository and I want it to compile and run so for that
能够克隆存储库，并且我希望它能够编译并运行

24
00:01:34,890 --> 00:01:40,090
reason I tend to actually keep versions of my dependencies all the libraries
我倾向于实际上保留所有库的依赖版本的原因

25
00:01:40,290 --> 00:01:45,429
that I'm using in the actual solution in the actual project folder so I've
我在实际项目文件夹中的实际解决方案中使用的

26
00:01:45,629 --> 00:01:48,909
actually got copies of those that visit those physical binaries or code
实际上获得了访问那些物理二进制文件或代码的副本

27
00:01:49,109 --> 00:01:52,959
depending on which way I go without as well in my actual kind of working
取决于我在实际工作中没有去哪一种方式

28
00:01:53,159 --> 00:01:55,929
directory of my solution that kinda brings me to another point
我的解决方案的目录，这有点让我想到另一点

29
00:01:56,129 --> 00:01:59,859
should I be compiling these myself or should I just be linking against
我应该自己编译这些文件还是应该链接反对

30
00:02:00,060 --> 00:02:05,018
pre-built binaries now for most serious projects I would absolutely recommend
现在，我绝对推荐大多数严肃项目的预编译二进制文件

31
00:02:05,218 --> 00:02:08,529
actually building the source code if you're using Visual Studio you can just
实际构建源代码（如果您使用的是Visual Studio） 

32
00:02:08,729 --> 00:02:12,439
add another project which contains your kind of source code of your
添加另一个项目，其中包含您的源代码类型

33
00:02:12,639 --> 00:02:15,680
dependency of your library and then compiled that into either a static or
库的依赖项，然后将其编译为静态或静态

34
00:02:15,879 --> 00:02:20,180
dynamic library however if you don't have access to the source code or if
动态库，但是如果您无权访问源代码或

35
00:02:20,379 --> 00:02:22,430
you're planning just this is a quick project I don't want to spend time
您正在计划这只是一个快速的项目，我不想花时间

36
00:02:22,629 --> 00:02:25,790
setting this up because it's kind of a throwaway thing or it's just a it's
设置它是因为它是一个一次性的东西，或者仅仅是一个

37
00:02:25,990 --> 00:02:29,540
nothing serious then I would probably lean against the binaries because it's
没什么严重的，那么我可能会依赖二进制文件，因为它是

38
00:02:29,740 --> 00:02:32,330
just gonna be quicker and easier so today specifically we're going to be
只会变得更快，更轻松，所以今天特别是我们将

39
00:02:32,530 --> 00:02:35,660
linking against binaries we're not going to be grabbing the source code of an
链接二进制文件，我们将不会获取源代码

40
00:02:35,860 --> 00:02:38,840
actual kind of dependency of an actual library and compiling it ourselves I
实际库的实际依赖关系并自己编译

41
00:02:39,039 --> 00:02:41,480
might save that for another video if you're interested in that let me know
如果您对此感兴趣，可以将其保存为另一个视频，请告诉我

42
00:02:41,680 --> 00:02:44,780
leave a comment below but today we're just gonna be dealing with binaries and
在下面发表评论，但今天我们将要处理二进制文件， 

43
00:02:44,979 --> 00:02:48,350
specifically what we're going to be dealing with is a library called GL FW
具体来说，我们要处理的是一个名为GL FW的库

44
00:02:48,550 --> 00:02:52,130
now my open GL series which is all about OpenGL and graphics program you saw like
现在我的开放式GL系列与您所看到的OpenGL和图形程序有关

45
00:02:52,330 --> 00:02:55,580
that in episode 2 which I've linked up there as well we actually did download
在第2集中，我也在那里进行了链接，实际上我们确实下载了

46
00:02:55,780 --> 00:02:58,880
gel of W and Lincoln so you can take a look at that if you'd like maybe I show
 W和林肯的凝胶，所以如果您愿意，我可以看看

47
00:02:59,080 --> 00:03:02,660
some extras some extra things there but today we're actually going to go through
还有一些额外的东西，但今天我们实际上要经历

48
00:03:02,860 --> 00:03:06,350
it slowly and take a look at everything but if you kind of want to see it in a
慢慢看一看，但如果您想在一个

49
00:03:06,550 --> 00:03:10,610
context of a real application then check that series out another thing to note is
实际应用程序的上下文，然后检查一系列要注意的事项是

50
00:03:10,810 --> 00:03:14,150
the binaries might not be available for your actual project right for your
二进制文件可能不适用于您的实际项目

51
00:03:14,349 --> 00:03:17,810
actual library that you want to link so you may actually be forced to build it
您想要链接的实际库，因此您可能实际上被迫构建它

52
00:03:18,009 --> 00:03:21,740
yourself this is especially true with like Mac and Linux because for UNIX
您自己，对于Mac和Linux来说尤其如此，因为对于UNIX 

53
00:03:21,939 --> 00:03:25,849
systems people usually just love building code for Windows a lot of
人们通常只喜欢为Windows构建代码的系统

54
00:03:26,049 --> 00:03:29,689
people who use windows just want stuff to work like like me I just want things
使用Windows的人只是希望事物像我一样工作，我只是想要事物

55
00:03:29,889 --> 00:03:33,379
to work so that's why preview binaries exist and again I'm just saying that in
起作用，这就是为什么存在预览二进制文件的原因，我再次说

56
00:03:33,579 --> 00:03:36,950
a more kind of professional a lot of project in in a space where I actually
在我实际所在的空间中，更专业的很多项目

57
00:03:37,150 --> 00:03:40,819
had time I would for sure compile it myself because it helps with debugging
有时间我肯定会自己编译，因为它有助于调试

58
00:03:41,019 --> 00:03:44,569
it helps with if you want to modify the library and change it a little bit I do
如果您想修改库并对其稍作更改，它会有所帮助

59
00:03:44,769 --> 00:03:48,200
that quite frequently I just love having it having the source code right there in
我经常喜欢在源代码中添加它

60
00:03:48,400 --> 00:03:52,370
the visual studio solution but today let's talk about binaries so ever here
 Visual Studio解决方案，但今天让我们在这里谈论二进制文件

61
00:03:52,569 --> 00:03:56,990
we have Chillicothe org which contains the which is the gfw website there are a
我们有Chillicothe组织，其中包含gfw网站， 

62
00:03:57,189 --> 00:03:59,300
few downloads here if you click this download button and we'll download the
如果您点击此下载按钮，此处下载次数很少，我们将下载

63
00:03:59,500 --> 00:04:02,870
source code for gfw but instead we're going to go to download and you can see
 gfw的源代码，但相反，我们要去下载，您可以看到

64
00:04:03,069 --> 00:04:06,469
that we have precompiled binaries for Windows another great example here you
我们已经为Windows预编译了二进制文件另一个很好的例子

65
00:04:06,669 --> 00:04:10,009
can see for Linux and Mac OS you have to compile it yourself but for Windows we
可以看到对于Linux和Mac OS，您必须自己进行编译，但是对于Windows，我们

66
00:04:10,209 --> 00:04:12,650
have these binaries now this brings us to our first question
现在有了这些二进制文件，这使我们想到了第一个问题

67
00:04:12,849 --> 00:04:17,000
do I want 32-bit binaries or 64-bit binaries this has nothing to do with
我要32位二进制文​​件还是64位二进制文​​件与此无关

68
00:04:17,199 --> 00:04:21,408
your actual operating system if you're running Windows 10 64 bit like I am that
您的实际操作系统（如果您正在运行Windows 10 64位操作系统） 

69
00:04:21,608 --> 00:04:25,100
doesn't mean that you should be grabbing the 64-bit binaries what that means or
并不意味着您应该获取64位二进制文​​件，这意味着或

70
00:04:25,300 --> 00:04:28,369
like the way that you choose the ones you want is for your target for like for
就像您选择想要的方式是针对您的目标一样

71
00:04:28,569 --> 00:04:32,718
your application target so if I'm compiling my application as an x86
您的应用程序目标，因此，如果我将我的应用程序编译为x86 

72
00:04:32,918 --> 00:04:37,699
application as a win32 application then I would want the 32-bit binaries if I'm
应用程序作为Win32应用程序，那么如果我要使用32位二进制文​​件

73
00:04:37,899 --> 00:04:41,329
compiling a 64-bit application I was 64-bit binaries make sure you match them
编译64位应用程序我是64位二进制文​​件，请确保您匹配它们

74
00:04:41,529 --> 00:04:45,199
together because if you don't they won't link so in this case I'm going to just
在一起，因为如果您不这样做，它们将不会链接，因此在这种情况下，我将

75
00:04:45,399 --> 00:04:48,230
be making a 32-bit application so I'm going to grab the 32-bit Windows
正在制作一个32位应用程序，所以我将抓住32位Windows 

76
00:04:48,430 --> 00:04:51,528
binaries and download them once I've downloaded and extracted them we'll have
二进制文件并下载它们，一旦我下载并提取了它们，我们将拥有

77
00:04:51,728 --> 00:04:55,069
this folder over here inside there we have a bunch of folders and some other
这个文件夹在里面，我们有一堆文件夹，还有一些

78
00:04:55,269 --> 00:04:58,759
text files and this is kind of the typical layout like yes all the typical
文本文件，这是一种典型的布局，是的，所有典型

79
00:04:58,959 --> 00:05:02,718
organizational structure that we have with libraries in C++ there are two
我们在C ++库中拥有的组织结构有两种

80
00:05:02,918 --> 00:05:08,449
parts to a library usually includes and libraries right so an include kind of
库的一部分通常包含和库，因此包含一种

81
00:05:08,649 --> 00:05:12,379
directory and a library directory or a Lib directory so what the include
目录和库目录或Lib目录，那么包括

82
00:05:12,579 --> 00:05:16,189
directory is is a bunch of header files if you don't know how linking works in C
如果您不知道链接在C语言中的工作方式，则目录是一堆头文件

83
00:05:16,389 --> 00:05:19,278
bottles or what I even mean by link you check out the house if it was linking
瓶子，或什至是链接的意思，如果房子在链接，您要检查房子

84
00:05:19,478 --> 00:05:23,240
works video that I've made in the top right corner there it covers this stuff
我在右上角制作的作品视频涵盖了这些内容

85
00:05:23,439 --> 00:05:26,360
in depth but basically the include directory has a bunch of header files
深入，但基本上include目录中有一堆头文件

86
00:05:26,560 --> 00:05:30,170
that we need to use so that we can actually use functions that are in the
我们需要使用的功能，以便我们可以实际使用

87
00:05:30,370 --> 00:05:33,410
pre-built binaries and then the lint directory has those pre-built binaries
预构建的二进制文件，然后lint目录中包含那些预构建的二进制文件

88
00:05:33,610 --> 00:05:37,968
and there are usually two phases to this right there's usually dynamic libraries
这个权利通常有两个阶段，通常是动态库

89
00:05:38,168 --> 00:05:41,749
and Static libraries not all libraries it's actually surprised not all
和静态库不是所有库，实际上并不是所有

90
00:05:41,949 --> 00:05:45,170
libraries apply both of them for you jello W does though and you can choose
 jello W会为您应用这两个库，您可以选择

91
00:05:45,370 --> 00:05:48,829
if you want to link statically or dynamically we're going to talk a lot
如果您想静态或动态链接，我们将进行很多讨论

92
00:05:49,028 --> 00:05:52,338
about the differences between static and dynamic linking in another video
关于另一个视频中静态链接和动态链接之间的区别

93
00:05:52,538 --> 00:05:58,338
specifically about that but I'll cover the differences for you kind of simply
特别是关于这一点，但我会为您简单介绍这些差异

94
00:05:58,538 --> 00:06:02,899
here static linking means that the library actually gets basically put into
这里的静态链接意味着该库实际上已被放入

95
00:06:03,098 --> 00:06:07,399
your executable so just it's inside your DX a file or whatever your executable is
您的可执行文件，因此它仅位于DX文件中或任何可执行文件中

96
00:06:07,598 --> 00:06:11,180
for your operating system whereas a dynamic library gets actually linked at
用于您的操作系统，而动态库实际上在

97
00:06:11,379 --> 00:06:15,468
runtime so you still do have some kind of linkage you can choose to alert a
运行时，因此您仍然有某种联系可以选择来提醒

98
00:06:15,668 --> 00:06:19,610
dynamic library kind of just on the fly literally there's a function called load
动态库有点儿飞速，实际上有一个叫做load的函数

99
00:06:19,810 --> 00:06:23,389
library which you can use in the Windows API as an example and that will load you
您可以在Windows API中用作示例的库，它将为您加载

100
00:06:23,589 --> 00:06:27,290
like your dynamic library and you can pull function pointers out of data stalk
就像您的动态库一样，您可以将函数指针移出数据跟踪

101
00:06:27,490 --> 00:06:30,569
calling functions away but you can also have a kind of at
调用函数，但您也可以在

102
00:06:30,769 --> 00:06:35,699
the application launch it loads your DLL file which is your dynamic link library
应用程序启动后将加载您的DLL文件，这是您的动态链接库

103
00:06:35,899 --> 00:06:38,549
and what I'm going to talk too much in depth about this but there's just that
我将对此进行过多的讨论，但仅此而已

104
00:06:38,749 --> 00:06:42,119
that difference of whether or not your library actually gets compiled into your
您的库是否实际被编译到您的库中的区别

105
00:06:42,319 --> 00:06:47,160
exe file or links into your sa file and actually having a separate file for your
 exe档案或连结至您的sa档案，实际上为您有一个单独的档案

106
00:06:47,360 --> 00:06:51,239
library at runtime which you need to have alongside your Exe file or just
库在运行时需要与Exe文件一起存储

107
00:06:51,439 --> 00:06:55,199
somewhere so the UAA file can actually load that different stuff because of
某个位置，因此UAA文件实际上可以加载不同的内容，因为

108
00:06:55,399 --> 00:07:00,028
that kind of dependency of you now need to have a DLL file as well as your exa
这种依赖关系现在需要一个DLL文件以及exa 

109
00:07:00,228 --> 00:07:05,009
file usually I like to do static linking whenever possible static linking is also
文件，通常我喜欢做静态链接，只要有可能，静态链接也是

110
00:07:05,209 --> 00:07:08,610
technically faster because the compiler or the link can actually perform kind of
从技术上讲更快，因为编译器或链接实际上可以执行某种

111
00:07:08,810 --> 00:07:12,360
link time optimisation and all that stuff linking statically can technically
链接时间优化以及所有静态链接在技术上都可以

112
00:07:12,560 --> 00:07:16,949
result in a faster application because there are several optimizations that can
导致更快的应用程序，因为可以进行多种优化

113
00:07:17,149 --> 00:07:21,028
be applied since we actually know the function that we're linking kind of add
被应用，因为我们实际上知道我们要链接的add类型的功能

114
00:07:21,228 --> 00:07:25,350
add actual about link time whereas with dynamic libraries we all know what's
添加实际的链接时间，而使用动态库，我们都知道

115
00:07:25,550 --> 00:07:28,439
gonna happen we have to leave it intact and when the library actually gets
将会发生，我们必须保持原样，并且当图书馆真正

116
00:07:28,639 --> 00:07:31,410
bloated by our application of runtime that's one that parts kind of get
被我们的运行时应用程序肿，这是部分获得

117
00:07:31,610 --> 00:07:36,329
patched up so usually static linking is the way to go but for this video I'm
修补，所以通常要使用静态链接，但是对于这个视频， 

118
00:07:36,528 --> 00:07:39,778
going to demonstrate both of those strategies so again we have the include
将演示这两种策略，因此我们再次包含

119
00:07:39,978 --> 00:07:43,079
files and then we have the library files to kind of components that we need is
文件，然后将库文件放入需要的某种组件中

120
00:07:43,278 --> 00:07:47,160
set up so for our compiler in our visual studio project we have to point it to
进行设置，以便在Visual Studio项目中为我们的编译器指定指向

121
00:07:47,360 --> 00:07:50,699
the header files to the include file so that we actually know which functions we
头文件到包含文件，以便我们实际上知道我们要使用的功能

122
00:07:50,899 --> 00:07:54,509
have available to us and so that we have those function declarations that really
提供给我们，所以我们有那些真正的功能声明

123
00:07:54,709 --> 00:07:57,660
there's symbol declarations because they could also be variables I'm just using
有符号声明，因为它们也可能是我正在使用的变量

124
00:07:57,860 --> 00:08:02,429
functions as an example and then we also have to point our linker to the library
以功能为例，然后我们还必须将链接器指向库

125
00:08:02,629 --> 00:08:04,920
files and I'm waving my hands around love that that's just how I talks to
文件，我挥舞着我的手，那就是我说话的方式

126
00:08:05,120 --> 00:08:09,028
deal with it and then we also need to tell our linker this is my library file
处理它，然后我们还需要告诉链接器这是我的库文件

127
00:08:09,228 --> 00:08:12,509
I want you to link this so that we actually get the function definitions
我希望您链接此链接，以便我们实际获得功能定义

128
00:08:12,709 --> 00:08:15,778
linked correctly let's go ahead and do that for both static and dynamic
正确链接，让我们继续进行静态和动态操作

129
00:08:15,978 --> 00:08:19,468
libraries so I bought this hollow world project here in Visual Studio it's very
库，所以我在Visual Studio的这里购买了这个空心世界项目， 

130
00:08:19,668 --> 00:08:23,489
basic stuff but what I'm actually going to do is inside the solution directory
基本的东西，但是我实际上要做的是在解决方案目录中

131
00:08:23,689 --> 00:08:27,959
make a folder called dependencies this is how I actually manage my dependencies
制作一个名为dependencies的文件夹，这就是我实际管理依赖项的方式

132
00:08:28,158 --> 00:08:32,099
in my libraries inside dependencies I'll make another folder called GFW and
在依赖关系库中，我将创建另一个名为GFW的文件夹， 

133
00:08:32,299 --> 00:08:36,448
inside there I'm going to put the files that I actually downloaded which are the
在里面，我将实际下载的文件放在

134
00:08:36,649 --> 00:08:41,028
GLW library so include and live CC 2015 you can see here that they've actually
 GLW库，因此包含并直播CC 2015，您可以在此处看到他们实际上已经

135
00:08:41,229 --> 00:08:45,019
appended kind of which compiled they used to actually compile this this
附加了那种他们曾经实际编译过的东西

136
00:08:45,220 --> 00:08:50,449
library files form in gwgw 64 and then basically all these versions of Visual
库文件形式在gwgw 64中，然后基本上是所有这些版本的Visual 

137
00:08:50,649 --> 00:08:54,169
Studio will just pick the latest one it doesn't really matter in me at the end
 Studio会选择最新的一首，最后对我而言这并不重要

138
00:08:54,370 --> 00:08:57,589
of the day they are just kind of compiled binaries and they will work
一天，它们只是一种已编译的二进制文件，它们会工作

139
00:08:57,789 --> 00:09:01,250
with kind of either one but we want to use the one that's most compatible with
两者之一，但我们想使用与以下产品最兼容的一种

140
00:09:01,450 --> 00:09:06,349
our current tool chain which is BC 2015 because we're using video studio 2017 so
我们当前的工具链是BC 2015，因为我们正在使用video studio 2017，因此

141
00:09:06,549 --> 00:09:11,269
I'll copy that and the include into this folder here include you can see has a
我将其复制并将包含到此文件夹中，此处包含您可以看到有一个

142
00:09:11,470 --> 00:09:15,709
gel FLV folder and then a bunch of paedophiles perfect and Lib has three
凝胶FLV文件夹，然后是一堆完美的恋童癖者和Lib有三个

143
00:09:15,909 --> 00:09:20,719
files for us gow3 dll is the one time kind of dynamic link library that we
对我们来说，gow3 dll文件是一种动态链接库， 

144
00:09:20,919 --> 00:09:25,519
actually use if we're linking dynamically at runtime and gow3 DLL dot
如果在运行时动态链接和gow3 DLL点，则实际使用

145
00:09:25,720 --> 00:09:30,078
Lib is actually kind of the static library that we use with the DLL so that
 Lib实际上是一种我们与DLL一起使用的静态库，因此

146
00:09:30,278 --> 00:09:34,129
we don't have to actually ask the DLL hey can I please have a bunch of
我们不必真正询问DLL嘿，我可以请一堆

147
00:09:34,330 --> 00:09:37,429
function pointers to all of these functions the idea is that this Lib file
所有这些函数的函数指针，想法是该Lib文件

148
00:09:37,629 --> 00:09:41,659
actually contains all of the locations of the functions and symbols inside jela
实际上包含了jela中函数和符号的所有位置

149
00:09:41,860 --> 00:09:46,339
w3 DLL so that we can actually link against them at compile time so in
 w3 DLL，以便我们实际上可以在编译时与它们链接

150
00:09:46,539 --> 00:09:49,609
comparison if we didn't have this Lib file we could still use this stuff how
比较，如果我们没有这个Lib文件，我们仍然可以使用这个东西

151
00:09:49,809 --> 00:09:54,078
we have to ask the DLL file for each function by name and actually be like
我们必须按名称询问每个函数的DLL文件，实际上就像

152
00:09:54,278 --> 00:09:58,008
hey I want jillyfell B in it or whatever but this live file already kind of
嘿，我想在其中插入jillyfell B，但此实时文件已经有点

153
00:09:58,208 --> 00:10:00,919
contains all of those locations for us so the linker can just link to them
包含我们所有的这些位置，因此链接器可以仅链接到它们

154
00:10:01,120 --> 00:10:04,279
immediately gelatin b3 to live which you can see is significantly bigger than the
立即可见，明胶b3的存活率明显大于

155
00:10:04,480 --> 00:10:07,849
other ones is the static library so this is what we linked if we don't want
其他的是静态库，所以这是我们链接的内容，如果我们不想

156
00:10:08,049 --> 00:10:11,839
compile time linking and if we do that we don't need this DLL file to be
编译时链接，如果我们这样做，则不需要此DLL文件

157
00:10:12,039 --> 00:10:16,159
without Exe file at runtime okay cool so let's actually link this stuff I'm going
在运行时没有Exe文件就好了，所以让我们实际链接一下我要去的东西

158
00:10:16,360 --> 00:10:19,699
to right click on my hello world project hit properties and the first thing I'll
右键单击我的hello world项目命中属性，然后我要做的第一件事

159
00:10:19,899 --> 00:10:24,139
actually do go to C C++ general and then in additional include directories I'm
实际上去C C ++常规，然后在其他包含目录中

160
00:10:24,339 --> 00:10:28,339
going to specify my additional include directory now note your configuration
现在要指定我的其他包含目录，请注意您的配置

161
00:10:28,539 --> 00:10:31,729
and platform make sure you're editing the right ones I obviously want this to
和平台确保您编辑的是正确的，我显然希望这样做

162
00:10:31,929 --> 00:10:34,819
apply for all configurations so I'll just hit all configurations now the
申请所有配置，所以我现在就点击所有配置

163
00:10:35,019 --> 00:10:38,269
include directory is the actual directory that goes each of this include
包含目录是每个包含该目录的实际目录

164
00:10:38,470 --> 00:10:42,559
folder so that's it I could just copy this directory but obviously you can see
文件夹，就是这样，我可以只复制此目录，但显然您可以看到

165
00:10:42,759 --> 00:10:46,939
that it's in C uses yarn documents very specific to my computer if someone ends
如果有人结束，它在C语言中使用的纱线文档非常特定于我的计算机

166
00:10:47,139 --> 00:10:50,059
up cloning this from github or something like that they're not gonna have this
从github或类似的东西克隆它们不会有这个

167
00:10:50,259 --> 00:10:53,870
path and that code isn't going to compile what I actually want to do is
路径，该代码将无法编译我真正想要做的是

168
00:10:54,070 --> 00:10:59,179
have a path that is relative to this actual directory so hollow world is the
具有相对于此实际目录的路径，因此空心世界是

169
00:10:59,379 --> 00:11:02,269
directory with my solution right everyone's going to have that if they
目录与我的解决方案，如果每个人都拥有它， 

170
00:11:02,470 --> 00:11:06,829
check out my repository because this is my repository so inside it so starting
签出我的存储库，因为这是我的存储库，所以在其中，所以开始

171
00:11:07,029 --> 00:11:12,109
from this solution file which I can do by typing in solution this is a macro
从这个解决方案文件中，我可以通过输入解决方案来做到这一点，这是一个宏

172
00:11:12,309 --> 00:11:16,609
that you can use in Visual Studio if I go under here into edit you can see that
如果我进入这里进行编辑，则可以在Visual Studio中使用它，您会看到

173
00:11:16,809 --> 00:11:21,289
it actually evaluates to this value which is the solution directory if I hit
它实际上评估为该值，如果我点击则为解决方案目录

174
00:11:21,490 --> 00:11:25,729
macros over here I can see all of my macros and I can type in like solution
我可以在这里看到所有宏，也可以键入类似的解决方案

175
00:11:25,929 --> 00:11:29,209
de as an example and you can see what it is there's also things like project
以de为例，您可以看到其中的内容，例如project 

176
00:11:29,409 --> 00:11:32,419
directory which is the directory of the project you can see that it's inside
目录，这是项目的目录，您可以看到它位于其中

177
00:11:32,620 --> 00:11:36,079
another hello world folder but the one that we care about it's virgin directory
另一个hello world文件夹，但我们关心的是原始目录

178
00:11:36,279 --> 00:11:40,399
so we'll start off there and then we'll basically get that relative path which
所以我们将从那里开始，然后我们基本上会得到相对路径

179
00:11:40,600 --> 00:11:46,159
in this case is dependencies GL FW and include so I can just grab that and
在这种情况下，依赖项是GL FW并包含在内，所以我可以抓住它， 

180
00:11:46,360 --> 00:11:49,909
really just copy the end of that path starting in my solution directory folder
真的只是从我的解决方案目录文件夹中复制该路径的末尾

181
00:11:50,110 --> 00:11:53,809
I'll paste it into here hit enter and that's it I'm done what I
我将其粘贴到此处，按回车键，就是我已经完成了

182
00:11:54,009 --> 00:11:58,578
can now do is basically just include files relative to this directory so this
现在可以做的基本上只是包含相对于此目录的文件，因此

183
00:11:58,778 --> 00:12:01,939
directory contains the GL FW folder which has a file called JAL it'll be
目录包含GL FW文件夹，其中有一个名为JAL的文件

184
00:12:02,139 --> 00:12:06,199
three dot H so guess what I can do I can just come in here and type it include GL
三点H，所以我应该怎么办，我可以输入此处并输入GL 

185
00:12:06,399 --> 00:12:11,779
f w /j love w3 dot h just like that and that will actually work if I hit ctrl f7
 fw / j喜欢w3点h就像那样，如果我按ctrl f7键，那实际上可以工作

186
00:12:11,980 --> 00:12:15,769
to compile my code you can see that I get no errors now because this is a
编译我的代码，您可以看到我现在没有错误，因为这是一个

187
00:12:15,970 --> 00:12:20,179
compiler specified in fluid path I can also use angular brackets and this kind
在流体路径中指定的编译器我也可以使用尖括号和此类

188
00:12:20,379 --> 00:12:24,439
of comes to the debate of do I use angular brackets or do I just use quotes
涉及辩论是使用尖括号还是仅使用引号

189
00:12:24,639 --> 00:12:28,399
Mike there's no difference because quotes will actually check the relative
迈克（Mike）没有区别，因为引号实际上会检查相对

190
00:12:28,600 --> 00:12:31,370
paths first and if it doesn't find anything relative to this file so
首先路径，如果找不到与此文件相关的任何内容，那么

191
00:12:31,570 --> 00:12:35,628
relative to the main dot CPP file it will actually look for the compile it'll
相对于主要点CPP文件，它实际上会寻找它将编译的文件

192
00:12:35,828 --> 00:12:40,490
look through the compiler include parts now the way that I use this is basically
查看编译器的包含部分，现在我使用它的方式基本上是

193
00:12:40,690 --> 00:12:47,240
if this source file is in Visual Studio so if gow3 or H is actually in in inside
如果此源文件在Visual Studio中，则gow3或H实际上在内部

194
00:12:47,440 --> 00:12:50,389
my solution somewhere maybe it's in a different project I don't care but it's
我的解决方案可能在某个地方，我不在乎，但这是

195
00:12:50,589 --> 00:12:54,469
inside this solution it's included in the solution then I will use quotes if
在此解决方案中，它包含在解决方案中，如果有，我将使用引号

196
00:12:54,669 --> 00:12:57,409
it's an it's absolutely kind of external dependency
这绝对是一种外部依赖

197
00:12:57,610 --> 00:13:01,639
or external library that isn't in I'm not compiling it with Visual Studio with
或不在其中的外部库，我无法使用Visual Studio对其进行编译

198
00:13:01,839 --> 00:13:05,000
this actual solution then I'll use angular brackets to kind of indicate
这个实际的解决方案，然后我将使用尖括号来表示

199
00:13:05,200 --> 00:13:08,870
that it's actually external so for this case I will write my code like this now
它实际上是外部的，因此在这种情况下，我现在将像这样编写我的代码

200
00:13:09,070 --> 00:13:11,809
I haven't actually linked against the libraries at all so I've told the
我实际上并没有链接到任何库，所以我告诉了

201
00:13:12,009 --> 00:13:16,279
compiler that hey there's a bunch of functions if I go to jail w3h you'll
嘿，如果我去监狱w3h，会有很多函数

202
00:13:16,480 --> 00:13:19,189
actually see that there's a bunch of stuff here I have a function for example
实际看到这里有很多东西，例如，我有一个函数

203
00:13:19,389 --> 00:13:23,240
called Jill up top you in it so let's try calling this over here my code I'll
叫吉尔在上面，所以让我们尝试在这里调用我的代码

204
00:13:23,440 --> 00:13:27,169
type in into a or something equals jail toluene is I don't know why my curly
输入a或等于监狱甲苯的东西是我不知道为什么我的卷发

205
00:13:27,370 --> 00:13:29,659
brackets a a black that is the worst idea ever
括号AA黑色是有史以来最糟糕的想法

206
00:13:29,860 --> 00:13:34,099
I'm gonna fix that after this video anyway you can see that if I do gel 12
我要修复这个视频之后，无论如何，我都能看到胶凝12 

207
00:13:34,299 --> 00:13:36,019
unit like this it looks like everything's fine
这样的单位看起来一切都很好

208
00:13:36,220 --> 00:13:39,439
I can hit ctrl f7 to compile my code you can see I don't get any compiler errors
我可以按ctrl f7来编译我的代码，可以看到我没有得到任何编译器错误

209
00:13:39,639 --> 00:13:42,740
but of course if I try and actually build my project and run it all just hit
但是当然，如​​果我尝试并实际构建我的项目并运行它，一切都将成功

210
00:13:42,940 --> 00:13:46,549
build it's not gonna link correctly and you'll get an error that says unresolved
构建它无法正确链接，您将收到一条错误消息，指出未解决

211
00:13:46,750 --> 00:13:50,149
external simple Jill it'll be eaten it this is the cause of so many problems
外部简单的吉尔会被吃掉，这是造成许多问题的原因

212
00:13:50,350 --> 00:13:55,729
for people what this means is that you haven't linked your actual library the
对于人们来说，这意味着您尚未链接实际库， 

213
00:13:55,929 --> 00:13:59,870
linker can't find this jello double unit function if we actually go to the header
如果我们实际转到标题，链接器将找不到此jello double单元函数

214
00:14:00,070 --> 00:14:03,139
file again and we take a look it's just got a semicolon here this doesn't tell
再次提交文件，我们看一看这里只是分号，这并不说明

215
00:14:03,339 --> 00:14:06,439
us what the function actually does it just tells us that the function exists
我们实际上该函数的作用只是告诉我们该函数存在

216
00:14:06,639 --> 00:14:09,889
what we need to do is actually find a definition for that function
我们需要做的实际上是找到该功能的定义

217
00:14:10,089 --> 00:14:13,250
now we could provide one I'll just show you I'm showing you this so that you
现在我们可以提供一个，我只给你看，我给你看，这样你

218
00:14:13,450 --> 00:14:17,089
know how it works so we know that U of W in here takes no parameters and returns
知道它是如何工作的，所以我们知道这里的W的U不带任何参数并返回

219
00:14:17,289 --> 00:14:20,299
an integer I could literally write chillip double unit here and it's like
一个整数，我可以在这里写chillip double单位，就像

220
00:14:20,500 --> 00:14:24,529
returns zero or something like that and then now if I build this and compile it
返回零或类似的东西，然后如果我构建它并编译它

221
00:14:24,730 --> 00:14:28,490
you can see that it is going to successfully generate my hello world exe
您会看到它将成功生成我的世界Hello exe 

222
00:14:28,690 --> 00:14:33,139
file and the build will succeed because I'm actually providing a definition for
文件，构建将成功，因为我实际上是为

223
00:14:33,339 --> 00:14:36,319
this function now obviously we don't want this definition we want the one
这个功能现在显然我们不想要这个定义，我们想要一个

224
00:14:36,519 --> 00:14:39,859
that's actually in the library so we need to link against the library
那实际上在图书馆里，所以我们需要链接图书馆

225
00:14:40,059 --> 00:14:43,849
hopefully that clears things up so I'll right click on holo world hit properties
希望这可以解决问题，所以我将右键单击“热门世界”属性

226
00:14:44,049 --> 00:14:46,969
to make sure again I'm in all configurations and on the right platform
再次确保我处于所有配置和正确的平台上

227
00:14:47,169 --> 00:14:53,029
go to linker and in general now inside link input is where I actually want to
转到链接器，通常现在链接输入是我真正想要的位置

228
00:14:53,230 --> 00:14:58,370
include that chillip w3 don't live file right I actually want to include it over
包括chillip w3不要实时保存文件，我实际上想将其包括在内

229
00:14:58,570 --> 00:15:02,659
here however I don't want to have to specify the entire path so again I could
但是在这里，我不想指定整个路径，所以我可以

230
00:15:02,860 --> 00:15:06,078
do like you know solution directory slash blah blah I just want to keep this
像您知道解决方案目录一样斜杠等等，我只想保留这个

231
00:15:06,278 --> 00:15:09,679
clean and just have we don't live here so if I want to do
干净，只是我们不住在这里，所以如果我想做

232
00:15:09,879 --> 00:15:13,609
that I actually need to go back to general and set my additional library
我实际上需要回到一般状态并设置我的附加库

233
00:15:13,809 --> 00:15:18,169
directory just like I did in C++ general with my additional include directory so
就像我在C ++ general中所做的那样，使用附加的include目录，因此

234
00:15:18,370 --> 00:15:22,609
this should be the root directory which contains my shell w3 don't live library
这应该是包含我的外壳程序的根目录w3不要运行库

235
00:15:22,809 --> 00:15:27,559
file so I cannot go over here into my Java folder go back here to live and
文件，所以我不能在这里进入我的Java文件夹。 

236
00:15:27,759 --> 00:15:31,429
then this is the directory I care about so I'm going to grab this and copy it
然后这是我关心的目录，所以我要抓取并复制它

237
00:15:31,629 --> 00:15:36,318
and then I'm going to come back to the library directories type in solution and
然后我要回到解决方案中的库目录类型

238
00:15:36,519 --> 00:15:40,698
then paste in that path just like that okay so I've specified a library
然后像这样粘贴在该路径中，所以我指定了一个库

239
00:15:40,899 --> 00:15:44,809
directory and then I've also specify the name of a library file that is relative
目录，然后我还要指定相对的库文件的名称

240
00:15:45,009 --> 00:15:50,448
to that library directory so if I hit OK and I try and build this now you can see
到该库目录，因此，如果我单击确定，然后尝试构建它，您现在可以看到

241
00:15:50,649 --> 00:15:54,378
that this works fine and we don't actually get any errors and if I was to
可以正常工作，实际上我们没有任何错误，如果我要

242
00:15:54,578 --> 00:15:58,399
for example run this let's just print the result of this to the console here 5
例如运行此命令，让我们将其结果打印到控制台5 

243
00:15:58,600 --> 00:16:01,758
you can see that we actually get one printing here which basically means that
您可以看到我们实际上在这里得到了一份印刷品，这基本上意味着

244
00:16:01,958 --> 00:16:05,479
you'll have W initialized successfully so everything is working fine now
您将成功初始化了W，所以现在一切正常

245
00:16:05,679 --> 00:16:09,679
similarly to how I showed you how I actually manually added a G lfwe th
类似于我向您展示如何实际手动添加G 

246
00:16:09,879 --> 00:16:13,909
function definition the same kind of applies for a header file right if I get
函数定义，如果得到的话，同样适用于头文件

247
00:16:14,110 --> 00:16:16,938
rid of this include or I don't have my header files I just don't know that
摆脱这个包含或我没有我的头文件，我只是不知道

248
00:16:17,139 --> 00:16:20,089
there's a function called G let's all be eating it actually available if I try
有一个叫做G的函数，如果我尝试的话，我们都会吃掉它实际可用的

249
00:16:20,289 --> 00:16:22,969
and compile this code it's not it's not that it's not gonna link it's just not
并编译此代码不是不是不是要链接它就是

250
00:16:23,169 --> 00:16:27,229
gonna compile because there is no such function the compiler can't see a
会编译，因为没有这样的函数，编译器看不到

251
00:16:27,429 --> 00:16:30,919
function called shelf double-unit however if I provide the actual
函数称为架子双单位，但是如果我提供实际的

252
00:16:31,120 --> 00:16:34,578
declaration here by typing in G left I'll be eating it and just returning
在这里输入G声明，我会吃掉它，然后返回

253
00:16:34,778 --> 00:16:39,649
nothing like that and hit f5 to run my code the idea of this will actually work
像这样，然后按f5来运行我的代码，这样的想法实际上可以工作

254
00:16:39,850 --> 00:16:44,149
however in this specific case we get an error because G of W is actually a C
但是在这种情况下，我们会得到一个错误，因为W的G实际上是C 

255
00:16:44,350 --> 00:16:48,109
library and what we're doing here is mangling the name with C++ I'll have a
库，我们在这里正在用C ++修改名称，我将

256
00:16:48,309 --> 00:16:51,948
video is specifically about kind of using C with C + + + name mangling and
视频专门涉及将C与C ++ +名称修饰和

257
00:16:52,149 --> 00:16:55,688
actually what's going on here but to cut this short we need to actually add
实际上发生了什么，但是要缩短时间，我们需要实际添加

258
00:16:55,889 --> 00:17:00,198
extern C to this function declaration which basically just says that preserve
这个函数声明的extern C基本上只是说保留

259
00:17:00,399 --> 00:17:03,679
this name as it is because you'll be linking against a library that was built
之所以使用这个名称，是因为您将链接到已建立的库

260
00:17:03,879 --> 00:17:07,669
in C so now if we compile this code you can see that it works successfully and
在C中，所以现在如果我们编译此代码，您可以看到它可以成功运行，并且

261
00:17:07,869 --> 00:17:12,378
if I run it as well we still get one same result that we got when we had the
如果我也运行它，我们仍然会得到与拥有

262
00:17:12,578 --> 00:17:15,319
header file so just remember those header files are kind of just best that
头文件，所以请记住那些头文件是最好的

263
00:17:15,519 --> 00:17:17,159
you have to do this stuff yourself it's not
你必须自己做这个东西，不是

264
00:17:17,359 --> 00:17:20,459
like the library linking is magically includes the magic or anything like that
就像库链接神奇地包含了魔术之类的东西

265
00:17:20,659 --> 00:17:24,029
it's just all components of a system to kind of go together
只是系统的所有组件都可以一起使用

266
00:17:24,230 --> 00:17:26,818
so the header files kind of just tell us which functions we have available by
所以头文件只是告诉我们我们可以使用哪些功能

267
00:17:27,019 --> 00:17:30,000
providing us with declarations and then that library file provides us with
为我们提供声明，然后该库文件为我们提供

268
00:17:30,200 --> 00:17:34,799
definitions so that we actually can link to those functions and execute the
定义，以便我们实际上可以链接到这些函数并执行

269
00:17:35,000 --> 00:17:38,549
appropriate code when we call the functions in our C++ code
在我们的C ++代码中调用函数时的适当代码

270
00:17:38,750 --> 00:17:41,430
okay I'm gonna wrap up the video there I know we did talk about dynamic libraries
好吧，我要整理视频，我知道我们确实在谈论动态库

271
00:17:41,630 --> 00:17:43,919
but this video is just been trailing I actually thought this would be pretty
但是这段视频只是在拖尾，我实际上以为这很漂亮

272
00:17:44,119 --> 00:17:46,889
trivial to explain and it would take like five minutes but as always I've
琐碎的解释，大概需要五分钟，但我一如既往

273
00:17:47,089 --> 00:17:50,309
just gone pretty in-depth and just talked about this a lot which i think is
刚刚深入，并谈论了很多，我认为是

274
00:17:50,509 --> 00:17:53,250
a good thing we're gonna talk about dynamic libraries in another video
我们将在另一个视频中谈论动态库是一件好事

275
00:17:53,450 --> 00:17:56,549
though and specifically with that I'll show you how to also kind of pull them
不过，我将特别向您展示如何拉动它们

276
00:17:56,750 --> 00:17:59,490
out if you don't have a static library to go along with them like we do with
如果您没有像我们这样的静态库，可以将它们淘汰

277
00:17:59,690 --> 00:18:02,848
yellow to view some otherwise I'll show you how to pull out functions and do all
黄色以查看其他内容，否则我将向您展示如何提取功能并完成所有操作

278
00:18:03,048 --> 00:18:08,219
that kind of actual runtime linking as well as linking against an actual DL but
这种实际的运行时链接以及与实际DL的链接，但是

279
00:18:08,419 --> 00:18:11,519
at compile time as well because there's a few kind of caveats with that as well
在编译时也是如此，因为还有一些警告

280
00:18:11,720 --> 00:18:14,279
I hope you guys enjoy this video if you did you can have that like button you
我希望你们喜欢这个视频，如果您能拥有喜欢按钮

281
00:18:14,480 --> 00:18:16,559
can also help support the series on patreon we're going to
还可以帮助支持我们将在patreon上进行的系列赛

282
00:18:16,759 --> 00:18:19,919
patreon.com/scishow know you'll get episodes earlier there are some pretty
 patreon.com/scishow知道您会早些获得剧集，其中有些很漂亮

283
00:18:20,119 --> 00:18:23,938
cool new perks as well which you should check out I will see you guys next time
不错的新福利，你应该看看我下次见

284
00:18:24,138 --> 00:18:31,140
goodbye [Music]
再见 

