﻿1
00:00:00,000 --> 00:00:01,720
嘿，大家好，我叫HMO，欢迎回到我的州，再加上
hey what's up guys my name is HMO and

2
00:00:01,919 --> 00:00:03,218
嘿，大家好，我叫HMO，欢迎回到我的州，再加上
welcome back to my state plus plus

3
00:00:03,419 --> 00:00:05,290
今天的系列我们将被告知所有关于新关键字的信息，它是老板和老板
series today we'll be told me all about

4
00:00:05,490 --> 00:00:07,359
今天的系列我们将被告知所有关于新关键字的信息，它是老板和老板
the new keyword it's a boss boss and man

5
00:00:07,559 --> 00:00:09,939
男人碰面是在谈论你们是否昨天错过了我的视频
man does this meet talking about if you

6
00:00:10,138 --> 00:00:11,620
男人碰面是在谈论你们是否昨天错过了我的视频
guys missed my video yesterday about how

7
00:00:11,820 --> 00:00:14,470
用C ++创建对象以及正确强调对象的正确方法
to create objects in C++ and the correct

8
00:00:14,669 --> 00:00:16,120
用C ++创建对象以及正确强调对象的正确方法
way to accentuate objects and definitely

9
00:00:16,320 --> 00:00:18,640
检查那里的鳕鱼的链接，否则让我们谈论新的，所以
check that out cod that link there but

10
00:00:18,839 --> 00:00:20,589
检查那里的鳕鱼的链接，否则让我们谈论新的，所以
otherwise let's talk about new so the

11
00:00:20,789 --> 00:00:22,390
新关键字很有趣，因为实际上它吸引了很多人
new keyword is interesting because it's

12
00:00:22,589 --> 00:00:25,600
新关键字很有趣，因为实际上它吸引了很多人
it's actually quite deep a lot of people

13
00:00:25,800 --> 00:00:26,560
写新书，他们并没有真正考虑，但是有很多事情要做
write new and they don't really think

14
00:00:26,760 --> 00:00:28,149
写新书，他们并没有真正考虑，但是有很多事情要做
about it but there's a lot that goes on

15
00:00:28,349 --> 00:00:29,620
了解这一点非常重要，尤其是因为
and it's a really important that you

16
00:00:29,820 --> 00:00:31,269
了解这一点非常重要，尤其是因为
understand that especially since you're

17
00:00:31,469 --> 00:00:33,070
使用C ++进行编程的事实是您的原型制造了总线，这意味着您
programming in C++ the fact that your

18
00:00:33,270 --> 00:00:34,628
使用C ++进行编程的事实是您的原型制造了总线，这意味着您
prototype makes a bus bus means that you

19
00:00:34,829 --> 00:00:36,640
应该关心诸如内存，性能和优化之类的事情
should be caring about things like

20
00:00:36,840 --> 00:00:39,309
应该关心诸如内存，性能和优化之类的事情
memory and performance and optimization

21
00:00:39,509 --> 00:00:41,858
所有这一切，因为真的，如果不这样做，为什么要编写C ++
all and all that because really if you

22
00:00:42,058 --> 00:00:44,018
所有这一切，因为真的，如果不这样做，为什么要编写C ++
don't why are you writing C++ there are

23
00:00:44,219 --> 00:00:45,549
还有那么多其他语言，您可以在2017年特别做到
so many other languages out there that

24
00:00:45,750 --> 00:00:47,588
还有那么多其他语言，您可以在2017年特别做到
you can do especially now in 2017

25
00:00:47,789 --> 00:00:49,448
除非您特别需要，为什么要写公交车卡
why would you be writing to a bus bus

26
00:00:49,649 --> 00:00:50,979
除非您特别需要，为什么要写公交车卡
card unless you specifically need

27
00:00:51,179 --> 00:00:52,659
性能，或者您特别关心发生的一切，
performance or you specifically care

28
00:00:52,859 --> 00:00:54,549
性能，或者您特别关心发生的一切，
about everything that goes on and

29
00:00:54,750 --> 00:00:58,209
了解新知识非常重要，特别是如果您来自
understanding new is very very important

30
00:00:58,409 --> 00:00:59,948
了解新知识非常重要，特别是如果您来自
especially if you're coming from a

31
00:01:00,149 --> 00:01:02,198
Java或C Sharp等托管语言，其中首先要清理内存
managed language like Java or C sharp

32
00:01:02,399 --> 00:01:04,869
Java或C Sharp等托管语言，其中首先要清理内存
where memory is first of all cleaned up

33
00:01:05,069 --> 00:01:07,000
自动为您服务，但是当您选择时，您却找不到太多选择
automatically for you but also you don't

34
00:01:07,200 --> 00:01:08,980
自动为您服务，但是当您选择时，您却找不到太多选择
get anywhere near as many choices when

35
00:01:09,180 --> 00:01:11,528
就像您在C ++中一样涉及到内存，因此对于那些确实了解Java或C的人来说
it comes to memory as you do in C++ so

36
00:01:11,728 --> 00:01:12,879
就像您在C ++中一样涉及到内存，因此对于那些确实了解Java或C的人来说
for those who do know Java or C sharp

37
00:01:13,079 --> 00:01:14,950
您一直都在使用新公交车，当您要去看公交车时
you're used to using new all the time

38
00:01:15,150 --> 00:01:17,500
您一直都在使用新公交车，当您要去看公交车时
and when you're going to see bus bus

39
00:01:17,700 --> 00:01:19,480
您可能会认为我是的，我可以看到简单的斑点，但不是很难，我是一样的
you'll probably think I yeah I can see

40
00:01:19,680 --> 00:01:21,549
您可能会认为我是的，我可以看到简单的斑点，但不是很难，我是一样的
simple spots not that hard is the same I

41
00:01:21,750 --> 00:01:22,629
意味着有一种新的关键方式，我将创建一个新的班级，了解我的
mean there's a new key way I'll just

42
00:01:22,829 --> 00:01:25,959
意味着有一种新的关键方式，我将创建一个新的班级，了解我的
create a new class know check out my

43
00:01:26,159 --> 00:01:27,399
有关如何进行视频分享的视频（如果您还没有的话）以及主要目的
video on how this and share classes if

44
00:01:27,599 --> 00:01:29,019
有关如何进行视频分享的视频（如果您还没有的话）以及主要目的
you haven't already so the main purpose

45
00:01:29,219 --> 00:01:32,009
new的方法是在堆上分配内存，特别是您要编写new，然后
of new is to allocate memory on the heap

46
00:01:32,209 --> 00:01:34,539
new的方法是在堆上分配内存，特别是您要编写new，然后
specifically you write new and then you

47
00:01:34,739 --> 00:01:36,189
写出您的数据，说明是类，原始类型还是数组
write your data say whether that be a

48
00:01:36,390 --> 00:01:38,829
写出您的数据，说明是类，原始类型还是数组
class or a primitive type or an array

49
00:01:39,030 --> 00:01:40,539
根据您所写的内容，确定所需的尺寸
based on what you've written it

50
00:01:40,739 --> 00:01:42,668
根据您所写的内容，确定所需的尺寸
determines the necessary size of the

51
00:01:42,868 --> 00:01:44,649
以字节为单位的分配，例如，如果我编写了一个新的int
allocation in bytes for example if I

52
00:01:44,849 --> 00:01:46,689
以字节为单位的分配，例如，如果我编写了一个新的int
write a new int that is going to have to

53
00:01:46,890 --> 00:01:49,299
请求有四个字节的内存后，立即分配4个字节的内存
request four bytes of memory allocate 4

54
00:01:49,500 --> 00:01:51,488
请求有四个字节的内存后，立即分配4个字节的内存
bytes of memory once it has that number

55
00:01:51,688 --> 00:01:53,890
它问操作系统，我应该说什么C标准库
it goes and it asks the operating system

56
00:01:54,090 --> 00:01:55,569
它问操作系统，我应该说什么C标准库
what the I should say C standard library

57
00:01:55,769 --> 00:01:58,299
我需要4个字节的内存，请给我，这就是乐趣的开始
I need 4 bytes of memory please give it

58
00:01:58,500 --> 00:02:00,340
我需要4个字节的内存，请给我，这就是乐趣的开始
to me and that's where the fun begins

59
00:02:00,540 --> 00:02:04,058
现在我们需要找到一个包含4个字节的内存的连续块，现在当然是4
now we need to find a contiguous block

60
00:02:04,259 --> 00:02:06,759
现在我们需要找到一个包含4个字节的内存的连续块，现在当然是4
of 4 bytes of memory now of course 4

61
00:02:06,959 --> 00:02:08,110
内存字节非常非常容易找到，所以会有Radek
bytes of memory is very very easy to

62
00:02:08,310 --> 00:02:09,779
内存字节非常非常容易找到，所以会有Radek
find so there'll be Radek

63
00:02:09,979 --> 00:02:12,509
快速分配，但仍然需要在内存中找到一个地址
quick allocation but it still needs to

64
00:02:12,709 --> 00:02:15,209
快速分配，但仍然需要在内存中找到一个地址
find an address in memory where you have

65
00:02:15,408 --> 00:02:17,700
完成操作后，将连续的完整字节返回到该内存地址的指针
full bytes in a row once it does that it

66
00:02:17,900 --> 00:02:20,130
完成操作后，将连续的完整字节返回到该内存地址的指针
returns a pointer to that memory address

67
00:02:20,330 --> 00:02:22,590
这样您就可以开始使用您的数据并在其中存储数据并读入我
so that you can begin using your data

68
00:02:22,789 --> 00:02:24,360
这样您就可以开始使用您的数据并在其中存储数据并读入我
and storing data there and read me in

69
00:02:24,560 --> 00:02:25,740
可以进行阅读和写作以及所有有趣的事情，因此您可以了解我
access of reading and writing and doing

70
00:02:25,939 --> 00:02:27,719
可以进行阅读和写作以及所有有趣的事情，因此您可以了解我
all that fun stuff so you see how I just

71
00:02:27,919 --> 00:02:29,789
像列出五个步骤之类的东西
like list it off like five steps or

72
00:02:29,989 --> 00:02:31,649
像列出五个步骤之类的东西
something like that yeah

73
00:02:31,848 --> 00:02:33,360
这是当您打电话给新客户时的主要收获，现在我需要一些时间
that's the primary takeaway from this

74
00:02:33,560 --> 00:02:36,090
这是当您打电话给新客户时的主要收获，现在我需要一些时间
when you call new it takes time now I

75
00:02:36,289 --> 00:02:38,039
确实说我们必须从字面上寻找连续内存的四个字节
did say that we had to literally look

76
00:02:38,239 --> 00:02:40,740
确实说我们必须从字面上寻找连续内存的四个字节
for four bytes of contiguous memory it's

77
00:02:40,939 --> 00:02:43,349
不是很喜欢它，不是真的，它只是在一个字面上搜索我们的记忆
not really like it's not like it it

78
00:02:43,549 --> 00:02:46,410
不是很喜欢它，不是真的，它只是在一个字面上搜索我们的记忆
literally searches our memory just in a

79
00:02:46,610 --> 00:02:48,480
像激光一样的行像餐饮，我们有四个字节的空闲时间，不，我会看一下
row like a laser being like a cater we

80
00:02:48,680 --> 00:02:50,550
像激光一样的行像餐饮，我们有四个字节的空闲时间，不，我会看一下
have four bytes free no I'll look on the

81
00:02:50,750 --> 00:02:52,439
下一个插槽有一个称为“空闲列表”的东西，实际上可以维护
next slot there's something called a

82
00:02:52,639 --> 00:02:54,179
下一个插槽有一个称为“空闲列表”的东西，实际上可以维护
free list which actually maintains

83
00:02:54,378 --> 00:02:58,110
没有字节的地址不是另一个视频的故事，但不是
addresses that have bytes free not a

84
00:02:58,310 --> 00:03:00,000
没有字节的地址不是另一个视频的故事，但不是
story for another video but it's not

85
00:03:00,199 --> 00:03:02,039
好像它可能没有您认为的那么慢
like it might not be as slow as you

86
00:03:02,239 --> 00:03:05,069
好像它可能没有您认为的那么慢
think it's obviously written to be

87
00:03:05,269 --> 00:03:07,319
相当聪明，但仍然很慢，但这是主要要点
rather intelligent but it is still quite

88
00:03:07,519 --> 00:03:09,090
相当聪明，但仍然很慢，但这是主要要点
slow but that's the primary takeaway

89
00:03:09,289 --> 00:03:11,550
知道基本上找到了一块足以容纳我们的记忆
knew basically finds a block of memory

90
00:03:11,750 --> 00:03:13,349
知道基本上找到了一块足以容纳我们的记忆
that is big enough to accommodate our

91
00:03:13,549 --> 00:03:15,450
需要，然后给我们提供指向该内存块的指针，让我们看一下
needs and then gives us a pointer to

92
00:03:15,650 --> 00:03:17,009
需要，然后给我们提供指向该内存块的指针，让我们看一下
that block of memory let's take a look

93
00:03:17,209 --> 00:03:18,509
在某些卡片上，我在这里得到的是一个非常基础的课程，只是有了一个字符串
at some cards what I've got over here is

94
00:03:18,709 --> 00:03:20,730
在某些卡片上，我在这里得到的是一个非常基础的课程，只是有了一个字符串
a very basic class has just got a string

95
00:03:20,930 --> 00:03:22,439
就是这样，我将在此处向下滚动，我们将看看
name that's it I'm going to scroll down

96
00:03:22,639 --> 00:03:23,789
就是这样，我将在此处向下滚动，我们将看看
over here and we'll take a look at the

97
00:03:23,989 --> 00:03:25,800
就像我们通常通过创建整数来创建关键字一样
new keyword first and foremost just like

98
00:03:26,000 --> 00:03:27,810
就像我们通常通过创建整数来创建关键字一样
we create integers normally by doing

99
00:03:28,009 --> 00:03:29,700
我们还可以选择实际动态分配该内存，
this we can also choose to actually

100
00:03:29,900 --> 00:03:31,680
我们还可以选择实际动态分配该内存，
dynamically allocate that memory and

101
00:03:31,879 --> 00:03:33,118
通过使用new关键字获得堆上的信用，因此我们可以写的是int
credit on the heap by using the new

102
00:03:33,318 --> 00:03:35,640
通过使用new关键字获得堆上的信用，因此我们可以写的是int
keyword so what we can write is int

103
00:03:35,840 --> 00:03:38,640
指针B，因为记得我说过new会返回一个指向内存的指针
pointer B because remember as I said new

104
00:03:38,840 --> 00:03:40,649
指针B，因为记得我说过new会返回一个指向内存的指针
returns a pointer to the memory that

105
00:03:40,848 --> 00:03:43,080
您分配的值等于new int，仅此一个字节
you've allocated equals new int and

106
00:03:43,280 --> 00:03:45,000
您分配的值等于new int，仅此一个字节
that's it that is a single for byte

107
00:03:45,199 --> 00:03:47,879
在堆上分配的整数，并且如果我，则此B正在存储其内存地址
integer allocated on the heap and this B

108
00:03:48,079 --> 00:03:49,920
在堆上分配的整数，并且如果我，则此B正在存储其内存地址
is storing its memory address if I

109
00:03:50,120 --> 00:03:52,259
想分配一个数组，而不是然后问括号放在哪里，然后键入
wanted to allocate an array instead then

110
00:03:52,459 --> 00:03:54,210
想分配一个数组，而不是然后问括号放在哪里，然后键入
I would ask where brackets and just type

111
00:03:54,409 --> 00:03:56,340
我想要多少个元素，在这种情况下为50，这意味着我们需要200个字节的
in how many elements I wanted so in this

112
00:03:56,539 --> 00:03:59,159
我想要多少个元素，在这种情况下为50，这意味着我们需要200个字节的
case 50 which means we need 200 bytes of

113
00:03:59,359 --> 00:04:02,099
如果要分配，则内存为每个整数的大小的50乘以4
memory 50 times 4 for being the size of

114
00:04:02,299 --> 00:04:04,050
如果要分配，则内存为每个整数的大小的50乘以4
each integer if we wanted to allocate

115
00:04:04,250 --> 00:04:06,960
我们的NC类在堆上是通过新键编写的，我们在其中编写这样的代码或
our NC class on the heap by the new key

116
00:04:07,159 --> 00:04:08,640
我们的NC类在堆上是通过新键编写的，我们在其中编写这样的代码或
where we were write code like this or

117
00:04:08,840 --> 00:04:10,740
这或者我们不需要为此使用括号作为默认值
this alternatively we don't need to use

118
00:04:10,939 --> 00:04:12,539
这或者我们不需要为此使用括号作为默认值
parentheses for this the default

119
00:04:12,739 --> 00:04:14,340
构造函数，但如果可以，我通常会这样做，但我们需要一个条目数组
constructor but we can and I usually do

120
00:04:14,539 --> 00:04:16,199
构造函数，但如果可以，我通常会这样做，但我们需要一个条目数组
if we wanted an array of entries instead

121
00:04:16,399 --> 00:04:16,879
一周只需使用方括号即可
week

122
00:04:17,079 --> 00:04:18,288
一周只需使用方括号即可
do that by just using square brackets

123
00:04:18,488 --> 00:04:20,509
然后我们基本上就是新权利，这就是您使用new关键字的方式
and there we go that's basically new

124
00:04:20,709 --> 00:04:22,429
然后我们基本上就是新权利，这就是您使用new关键字的方式
right that's how you use the new keyword

125
00:04:22,629 --> 00:04:24,290
让我们在核课程中多讲一点这两个是什么
let's talk about it a little bit more

126
00:04:24,490 --> 00:04:26,509
让我们在核课程中多讲一点这两个是什么
with classes the nuclear what does two

127
00:04:26,709 --> 00:04:28,489
事情不只是看实体，在这种情况下，她有多大
things it doesn't just look at entity

128
00:04:28,689 --> 00:04:30,559
事情不只是看实体，在这种情况下，她有多大
see how big an she is in this case it's

129
00:04:30,759 --> 00:04:32,689
一个字符串，所以我认为像28个字节之类的东西不要引用我
a string so that's I think like 28 bytes

130
00:04:32,889 --> 00:04:34,160
一个字符串，所以我认为像28个字节之类的东西不要引用我
or something like that don't quote me on

131
00:04:34,360 --> 00:04:36,528
我只是从内存中猜测它会乘以50，并要求
that I'm just guessing from memory it

132
00:04:36,728 --> 00:04:38,629
我只是从内存中猜测它会乘以50，并要求
multiplies that by 50 and it asks for a

133
00:04:38,829 --> 00:04:40,939
在数组的情况下，那么多字节的块，尽管您实际上会得到一个
block of that many bytes in the case of

134
00:04:41,139 --> 00:04:42,319
在数组的情况下，那么多字节的块，尽管您实际上会得到一个
an array though you'll actually get a

135
00:04:42,519 --> 00:04:45,588
50个实体的块只是连续地在内存中，所以这实际上是
block of 50 entities just continuously

136
00:04:45,788 --> 00:04:47,749
50个实体的块只是连续地在内存中，所以这实际上是
in memory so this is actually kind of

137
00:04:47,949 --> 00:04:49,790
就像连续在堆栈上分配50个实体一样
like allocating 50 entities on the stack

138
00:04:49,990 --> 00:04:51,769
就像连续在堆栈上分配50个实体一样
just in a row it's a little bit

139
00:04:51,968 --> 00:04:53,179
不同，因为您仍然分配在堆上，但是其中的每个实体
different because you're still allocated

140
00:04:53,379 --> 00:04:54,920
不同，因为您仍然分配在堆上，但是其中的每个实体
on the heap but each entity in this

141
00:04:55,120 --> 00:04:57,350
这个例子不会真的在另一个记忆里
example won't really be in another

142
00:04:57,550 --> 00:04:58,939
这个例子不会真的在另一个记忆里
memory breast you have your block of

143
00:04:59,139 --> 00:05:00,920
内存是50个实体，如果我们回到两个，则只是连续
memory which is 50 entity it's just just

144
00:05:01,120 --> 00:05:03,740
内存是50个实体，如果我们回到两个，则只是连续
in a row if we go back to two this just

145
00:05:03,939 --> 00:05:06,139
成为堆分配的单个实体对象，然后编写此新关键字
being a heap allocated single entity

146
00:05:06,339 --> 00:05:08,149
成为堆分配的单个实体对象，然后编写此新关键字
object then by writing this new keyword

147
00:05:08,348 --> 00:05:10,189
我们不仅在堆上分配了足够的内存来存储此文件，还看到了
we not only allocate enough memory on

148
00:05:10,389 --> 00:05:12,170
我们不仅在堆上分配了足够的内存来存储此文件，还看到了
the heap to store this anta see we also

149
00:05:12,370 --> 00:05:14,119
称构造器是核工作的另一重要内容
call the constructor that's the other

150
00:05:14,319 --> 00:05:15,889
称构造器是核工作的另一重要内容
important thing that the nuclear work

151
00:05:16,089 --> 00:05:18,410
它不仅分配内存，还调用后面的构造函数
does it not only allocates the memory it

152
00:05:18,610 --> 00:05:20,509
它不仅分配内存，还调用后面的构造函数
also calls the constructor now behind

153
00:05:20,709 --> 00:05:22,819
所有新场景都是真实的，您可以看到我在这里所做的就是
the scenes all new really is and you can

154
00:05:23,019 --> 00:05:24,860
所有新场景都是真实的，您可以看到我在这里所做的就是
see what I've done here is I've just if

155
00:05:25,060 --> 00:05:26,449
您右键单击您可以转到定义，您将看到这个圆点
you right-click on you you can go to

156
00:05:26,649 --> 00:05:29,028
您右键单击您可以转到定义，您将看到这个圆点
definition and you'll see what this dot

157
00:05:29,228 --> 00:05:30,980
首先是new运算符，首先您会发现这是new运算符
operator new actually is first of all

158
00:05:31,180 --> 00:05:33,290
首先是new运算符，首先您会发现这是new运算符
you'll see that it's an operator new is

159
00:05:33,490 --> 00:05:36,259
就像加号或减号或等于号一样的运算符
just an operator just like plus or minus

160
00:05:36,459 --> 00:05:39,139
就像加号或减号或等于号一样的运算符
or equals it's an operator which means

161
00:05:39,338 --> 00:05:40,910
您实际上会使操作员超负荷并改变我们将要讨论的行为
that you can actually overload the

162
00:05:41,110 --> 00:05:43,369
您实际上会使操作员超负荷并改变我们将要讨论的行为
operator and changes behavior we'll talk

163
00:05:43,569 --> 00:05:45,170
关于操作员超载的链接很快就会在描述卡上链接
about overloading operators very very

164
00:05:45,370 --> 00:05:47,959
关于操作员超载的链接很快就会在描述卡上链接
soon link in the description card on the

165
00:05:48,158 --> 00:05:50,449
屏幕上所有的爵士乐，但您可以看到的第二个只是
screen all that jazz but second of all

166
00:05:50,649 --> 00:05:51,800
屏幕上所有的爵士乐，但您可以看到的第二个只是
you can see that is literally just

167
00:05:52,000 --> 00:05:53,569
函数，这就是它需要的大小，即它分配的大小
function and this is the size that it

168
00:05:53,769 --> 00:05:55,309
函数，这就是它需要的大小，即它分配的大小
takes that's how much it allocates it

169
00:05:55,509 --> 00:05:57,588
返回一个空指针，我们可能会在单独的视频中讨论空指针
returns a void pointer we might talk

170
00:05:57,788 --> 00:05:59,088
返回一个空指针，我们可能会在单独的视频中讨论空指针
about void pointers in a separate video

171
00:05:59,288 --> 00:06:01,100
但基本上是空指针，只是没有时间的指针
but basically a void pointer is just

172
00:06:01,300 --> 00:06:03,920
但基本上是空指针，只是没有时间的指针
it's a pointer with no time a pointer is

173
00:06:04,120 --> 00:06:05,718
只是一个内存地址，所以当然就像指针真的需要一个类型一样
just a memory address so of course it's

174
00:06:05,918 --> 00:06:08,119
只是一个内存地址，所以当然就像指针真的需要一个类型一样
like pointers really need a type it

175
00:06:08,319 --> 00:06:09,468
需要一种类型以便您能够以可能的方式对其进行操作
needs a type for you to be able to

176
00:06:09,668 --> 00:06:11,300
需要一种类型以便您能够以可能的方式对其进行操作
manipulate it probably the way that you

177
00:06:11,500 --> 00:06:14,119
想要，但指针的核心只是一个内存地址，它只是一个数字
want to but at its core a pointer is

178
00:06:14,319 --> 00:06:15,860
想要，但指针的核心只是一个内存地址，它只是一个数字
just a memory address it's just a number

179
00:06:16,060 --> 00:06:17,389
所以为什么它需要一个特定的类型，例如int或double或entity，但是无论如何
so why would it need a specific type

180
00:06:17,588 --> 00:06:19,519
所以为什么它需要一个特定的类型，例如int或double或entity，但是无论如何
like int or double or entity but anyway

181
00:06:19,718 --> 00:06:20,778
您可以看到我们返回了一个空指针，所以它返回了一个指针
you can see that we return a void

182
00:06:20,978 --> 00:06:22,939
您可以看到我们返回了一个空指针，所以它返回了一个指针
pointer so it returns a pointer it so

183
00:06:23,139 --> 00:06:25,020
接受大小，并返回指向该内存分配块的点，但是
takes in a size and it returns a point

184
00:06:25,220 --> 00:06:26,790
接受大小，并返回指向该内存分配块的点，但是
to that allocate block of memory but

185
00:06:26,990 --> 00:06:28,889
幕后的实际操作和策略实际上是什么
what new actually does behind the scenes

186
00:06:29,089 --> 00:06:30,420
幕后的实际操作和策略实际上是什么
and strategies speaking this is actually

187
00:06:30,620 --> 00:06:31,949
取决于国家加上图书馆，所以局自己来救我们
dependent on the state plus plus library

188
00:06:32,149 --> 00:06:33,329
取决于国家加上图书馆，所以局自己来救我们
so of course Bureau your own save us

189
00:06:33,529 --> 00:06:34,680
加上您对我们诚实的损失库，因为理论上
plus compiler with your honesty of us

190
00:06:34,879 --> 00:06:36,000
加上您对我们诚实的损失库，因为理论上
loss library because theoretically make

191
00:06:36,199 --> 00:06:38,960
它可以做任何您想做的事，但通常拉动您会叫
it do anything you wanted but usually

192
00:06:39,160 --> 00:06:41,759
它可以做任何您想做的事，但通常拉动您会叫
usually pulling you will call the

193
00:06:41,959 --> 00:06:43,770
用于内存分配的基础状态函数malloc及其
underlying state function malloc which

194
00:06:43,970 --> 00:06:45,420
用于内存分配的基础状态函数malloc及其
that's for memory allocate and what this

195
00:06:45,620 --> 00:06:47,879
您实际上会注意到，我们正在考虑我们想要进行多少战斗，
will actually do you'll note is taking a

196
00:06:48,079 --> 00:06:50,189
您实际上会注意到，我们正在考虑我们想要进行多少战斗，
size of how many fights we want and

197
00:06:50,389 --> 00:06:53,040
返回一个空指针，这实际上就是它的全部工作，因此被说成这段代码
return a void pointer so that's really

198
00:06:53,240 --> 00:06:55,680
返回一个空指针，这实际上就是它的全部工作，因此被说成这段代码
all it does so that being said this code

199
00:06:55,879 --> 00:06:58,259
实际上相当于如果我们只写了malloc sizeof实体，例如
is actually kind of equivalent to if we

200
00:06:58,459 --> 00:07:02,100
实际上相当于如果我们只写了malloc sizeof实体，例如
just written malloc sizeof entity like

201
00:07:02,300 --> 00:07:03,840
然后，当然要为此付出代价，直到看到我们不会
that and then of course cost this back

202
00:07:04,040 --> 00:07:05,280
然后，当然要为此付出代价，直到看到我们不会
into an end to see something we wouldn't

203
00:07:05,480 --> 00:07:08,189
不得不在C中做，但是我们在C ++中做，但是这两行之间的区别
have had to do in C but we do in C++ but

204
00:07:08,389 --> 00:07:09,930
不得不在C中做，但是我们在C ++中做，但是这两行之间的区别
the difference between these two lines

205
00:07:10,129 --> 00:07:11,699
这两行代码之间的唯一区别是，
of code the only difference between

206
00:07:11,899 --> 00:07:14,040
这两行代码之间的唯一区别是，
these two lines of code is the fact that

207
00:07:14,240 --> 00:07:15,960
这实际上将调用实体构造函数，而这将是
this will actually call the entity

208
00:07:16,160 --> 00:07:17,759
这实际上将调用实体构造函数，而这将是
constructor whereas what this will do is

209
00:07:17,959 --> 00:07:20,040
纯粹分配内存，然后给我们一个指向该内存的指针，而不是调用
purely allocate the memory and then give

210
00:07:20,240 --> 00:07:21,870
纯粹分配内存，然后给我们一个指向该内存的指针，而不是调用
us a pointer to that memory not calling

211
00:07:22,069 --> 00:07:24,060
构造函数，您不应该在那里分配内存表
the constructor you should not be

212
00:07:24,259 --> 00:07:26,610
构造函数，您不应该在那里分配内存表
allocating memory tables like this there

213
00:07:26,810 --> 00:07:28,860
在某些情况下，您可能想要这样做，我们可能会谈论它们
are some situations in which you might

214
00:07:29,060 --> 00:07:31,259
在某些情况下，您可能想要这样做，我们可能会谈论它们
want to do that we might talk about them

215
00:07:31,459 --> 00:07:34,439
等一下，但是现在就看你，因为你不看这部影片
later but for you right now if you're

216
00:07:34,639 --> 00:07:35,639
等一下，但是现在就看你，因为你不看这部影片
watching this video because you don't

217
00:07:35,839 --> 00:07:37,680
知道我刚用的是什么，因为这当然不会
know what I pretty new is using you

218
00:07:37,879 --> 00:07:39,329
知道我刚用的是什么，因为这当然不会
because this of course won't call the

219
00:07:39,529 --> 00:07:41,939
构造函数，这也是糟糕的代码，更难阅读，这是
constructor and it's also way poor code

220
00:07:42,139 --> 00:07:43,800
构造函数，这也是糟糕的代码，更难阅读，这是
and it's just harder to read and this is

221
00:07:44,000 --> 00:07:45,060
确实是您应该做的方式，这是我要做的最后一件事
really the way that you should be doing

222
00:07:45,259 --> 00:07:46,439
确实是您应该做的方式，这是我要做的最后一件事
it the last thing that I'm going to

223
00:07:46,639 --> 00:07:48,240
今天提到的new是，当您使用new关键字时，您必须
mention about new for today is that when

224
00:07:48,439 --> 00:07:50,370
今天提到的new是，当您使用new关键字时，您必须
you do use the new keyword you have to

225
00:07:50,569 --> 00:07:52,740
请记住，您必须使用delete，所以一旦我们分配了所有这些变量
remember that you must use delete so

226
00:07:52,939 --> 00:07:54,540
请记住，您必须使用delete，所以一旦我们分配了所有这些变量
once we allocate all these variables

227
00:07:54,740 --> 00:07:57,090
像B和E一样，我们必须使用delete关键字，如果您将它也用作运算符
like B and E we have to use the delete

228
00:07:57,290 --> 00:07:59,189
像B和E一样，我们必须使用delete关键字，如果您将它也用作运算符
keyword which is also an operator if you

229
00:07:59,389 --> 00:08:01,350
转到X的定义，它也是内存块中的运算符文本
go to the definition for that X is also

230
00:08:01,550 --> 00:08:02,850
转到X的定义，它也是内存块中的运算符文本
an operator text in a block of memory

231
00:08:03,050 --> 00:08:05,460
大小，它只是一个常规函数，实际上会免费调用C函数
size it's just a regular function which

232
00:08:05,660 --> 00:08:07,710
大小，它只是一个常规函数，实际上会免费调用C函数
calls the C function free and actually

233
00:08:07,910 --> 00:08:09,420
释放曾经是malakut的内存块，这很重要，因为当
frees the block of memory that was

234
00:08:09,620 --> 00:08:11,278
释放曾经是malakut的内存块，这很重要，因为当
malakut this is important because when

235
00:08:11,478 --> 00:08:13,590
我们使用new关键字未释放内存，它未标记为空闲，
we use the new keyword the memory is not

236
00:08:13,790 --> 00:08:15,600
我们使用new关键字未释放内存，它未标记为空闲，
released it's not marked as free and

237
00:08:15,800 --> 00:08:17,160
它不会放回该空闲列表中，以便我们可以调用new并对其进行分配
it's not put back into that free list so

238
00:08:17,360 --> 00:08:18,930
它不会放回该空闲列表中，以便我们可以调用new并对其进行分配
that we can call new and allocate it

239
00:08:19,129 --> 00:08:21,329
再次，直到我们调用删除，我们必须这样做
again until we call delete we have to do

240
00:08:21,529 --> 00:08:21,550
再次，直到我们调用删除，我们必须这样做
that

241
00:08:21,750 --> 00:08:23,620
手动当然，在简单的地方有很多策略可以自动化
manually there are of course a lot of

242
00:08:23,819 --> 00:08:25,900
手动当然，在简单的地方有很多策略可以自动化
strategies in simple spots to automate

243
00:08:26,100 --> 00:08:28,480
这以某种形式存在，并且有一些简单的策略，例如基于范围的
this in some form and there are simple

244
00:08:28,680 --> 00:08:30,220
这以某种形式存在，并且有一些简单的策略，例如基于范围的
strategies like kind of scope based

245
00:08:30,420 --> 00:08:32,079
指针和高级策略（例如引用计数），我们将深入探讨
pointers and advanced strategies like

246
00:08:32,279 --> 00:08:34,208
指针和高级策略（例如引用计数），我们将深入探讨
reference counting we'll get into all of

247
00:08:34,408 --> 00:08:36,309
我们将来的东西，但是请记住，如果您这样错误地使用我
our stuff in the future but just keep in

248
00:08:36,509 --> 00:08:38,229
我们将来的东西，但是请记住，如果您这样错误地使用我
mind if you use me like this in a wrong

249
00:08:38,429 --> 00:08:38,500
你必须使用删除的方式，我确实计划
way

250
00:08:38,700 --> 00:08:40,870
你必须使用删除的方式，我确实计划
you have to use delete and I do plan on

251
00:08:41,070 --> 00:08:42,399
谈论未来所有这些内存管理的复杂性
talking about the intricacies of all

252
00:08:42,599 --> 00:08:43,958
谈论未来所有这些内存管理的复杂性
this memory management in the future

253
00:08:44,158 --> 00:08:46,419
一旦我们真正开始从事项目并编写代码，
once we actually start working on a

254
00:08:46,620 --> 00:08:48,309
一旦我们真正开始从事项目并编写代码，
project and writing code which is

255
00:08:48,509 --> 00:08:49,779
实际上很快就会变得如此激动，一切都会被揭示出来
actually going to be very soon so get

256
00:08:49,980 --> 00:08:51,699
实际上很快就会变得如此激动，一切都会被揭示出来
excited everything will be revealed I'm

257
00:08:51,899 --> 00:08:53,620
当然，您会看到很多B的例子，因为这是由
sure and you'll see many examples in the

258
00:08:53,820 --> 00:08:55,599
当然，您会看到很多B的例子，因为这是由
case of B because this was allocated by

259
00:08:55,799 --> 00:08:57,399
使用信用额度时，请记住这种新方式
using the array of credit keep in mind

260
00:08:57,600 --> 00:08:59,889
使用信用额度时，请记住这种新方式
by the way that this new that takes in

261
00:09:00,090 --> 00:09:01,449
佩雷塔阵列实际上是一个稍微不同的功能，如果我们
an array of peretta is actually a

262
00:09:01,649 --> 00:09:03,069
佩雷塔阵列实际上是一个稍微不同的功能，如果我们
slightly different function if we

263
00:09:03,269 --> 00:09:05,109
使用带有方括号的new进行分配，我们应该调用带有方括号的delete
allocate using new with square brackets

264
00:09:05,309 --> 00:09:08,019
使用带有方括号的new进行分配，我们应该调用带有方括号的delete
we should be calling delete with square

265
00:09:08,220 --> 00:09:09,339
这样的括号，因为有一个带方括号的delete运算符
brackets like this because there is a

266
00:09:09,539 --> 00:09:10,870
这样的括号，因为有一个带方括号的delete运算符
delete operator with square brackets

267
00:09:11,070 --> 00:09:12,939
这样就可以了，如果您使用带有new的square进行分配，那就是另一条规则
like that okay so that's another rule if

268
00:09:13,139 --> 00:09:14,919
这样就可以了，如果您使用带有new的square进行分配，那就是另一条规则
you allocate using new with square

269
00:09:15,120 --> 00:09:16,179
括号，因为您已使用方括号分配了数组删除
brackets because you've allocated an

270
00:09:16,379 --> 00:09:18,759
括号，因为您已使用方括号分配了数组删除
array delete using the square brackets

271
00:09:18,960 --> 00:09:20,889
如果您不这样做，则只需使用方括号即可，
if you don't then just use whatever

272
00:09:21,090 --> 00:09:22,629
如果您不这样做，则只需使用方括号即可，
square brackets one more thing that new

273
00:09:22,830 --> 00:09:24,309
实际上支持的是在U中称为“展示位置”的位置
actually supports is something called a

274
00:09:24,509 --> 00:09:25,899
实际上支持的是在U中称为“展示位置”的位置
placement in U and that is where you

275
00:09:26,100 --> 00:09:28,299
真正决定内存的来源，所以您不是
actually get to decide kind of where the

276
00:09:28,500 --> 00:09:29,589
真正决定内存的来源，所以您不是
memory comes from so you're not really

277
00:09:29,789 --> 00:09:31,629
在这种情况下分配内存，您只是在调用构造函数，
allocating memory in this case you're

278
00:09:31,830 --> 00:09:33,789
在这种情况下分配内存，您只是在调用构造函数，
just calling the constructor and

279
00:09:33,990 --> 00:09:36,429
在特定的内存地址中初始化您的实体以及您的方式
initializing your entity in a specific

280
00:09:36,629 --> 00:09:38,289
在特定的内存地址中初始化您的实体以及您的方式
memory address and the way you do that

281
00:09:38,490 --> 00:09:40,809
就是这样写括号，然后指定一个内存地址
is just by writing parenthesis like this

282
00:09:41,009 --> 00:09:41,979
就是这样写括号，然后指定一个内存地址
and then specifying a memory address

283
00:09:42,179 --> 00:09:45,189
在这种情况下，例如B井，我的意思是理论上可行，因为我
such as well B in this case I mean it

284
00:09:45,389 --> 00:09:46,389
在这种情况下，例如B井，我的意思是理论上可行，因为我
would theoretically work because I'm

285
00:09:46,589 --> 00:09:47,919
假设实体小于200个字节肯定很好，因为
assuming that entity is gonna be less

286
00:09:48,120 --> 00:09:49,569
假设实体小于200个字节肯定很好，因为
than 200 bytes definitely well because

287
00:09:49,769 --> 00:09:52,539
这只是一个字符串，是的，这会像所有这些代码一样混乱，
it's just a string but yeah this it's

288
00:09:52,740 --> 00:09:53,979
这只是一个字符串，是的，这会像所有这些代码一样混乱，
gonna confuse like all this code and

289
00:09:54,179 --> 00:09:55,870
真的，真的，我们将在以后更详细地讨论新的展示位置
really really we'll talk about placement

290
00:09:56,070 --> 00:09:57,849
真的，真的，我们将在以后更详细地讨论新的展示位置
new in the future in more detail on how

291
00:09:58,049 --> 00:09:59,529
您实际上可以使用很多优化代码，但是现在我只想
you can actually use at optimizing a

292
00:09:59,730 --> 00:10:02,439
您实际上可以使用很多优化代码，但是现在我只想
code quite a bit but for now I just want

293
00:10:02,639 --> 00:10:04,000
向您展示语法的外观，但无论如何我希望您
to show you what the syntax kind of

294
00:10:04,200 --> 00:10:05,620
向您展示语法的外观，但无论如何我希望您
looks like in however anyway I hope you

295
00:10:05,820 --> 00:10:06,879
大家喜欢这部影片，如果您愿意，可以按赞按钮
guys enjoyed this video if you did you

296
00:10:07,080 --> 00:10:08,259
大家喜欢这部影片，如果您愿意，可以按赞按钮
can have that like button you can also

297
00:10:08,460 --> 00:10:09,849
帮助支持本系列节目，并确保通过
help support this series and make sure

298
00:10:10,049 --> 00:10:11,949
帮助支持本系列节目，并确保通过
more awesome episodes are made by going

299
00:10:12,149 --> 00:10:13,779
要注册客户或搜索频道，我有一个不一致的服务器，会有一个
to patreon account or search the channel

300
00:10:13,980 --> 00:10:16,089
要注册客户或搜索频道，我有一个不一致的服务器，会有一个
I've got a discord server there'll be a

301
00:10:16,289 --> 00:10:17,500
下面的描述中的链接我们可以谈论所有这些东西
link in description below we can talk

302
00:10:17,700 --> 00:10:18,250
下面的描述中的链接我们可以谈论所有这些东西
about all this stuff

303
00:10:18,450 --> 00:10:20,559
顾客会获得特殊的角色，例如服务器角色，还有一个特殊的渠道，
patrons get special like server roles

304
00:10:20,759 --> 00:10:22,059
顾客会获得特殊的角色，例如服务器角色，还有一个特殊的渠道，
and also a special channel where we

305
00:10:22,259 --> 00:10:24,219
实际谈论这些视频并计划下一个
actually talk about these videos and

306
00:10:24,419 --> 00:10:24,959
实际谈论这些视频并计划下一个
plan the next

307
00:10:25,159 --> 00:10:26,609
有点像在幕后，非常细节非常酷
it's kind of like right behind the

308
00:10:26,809 --> 00:10:28,438
有点像在幕后，非常细节非常酷
scenes and very details very cool

309
00:10:28,639 --> 00:10:29,878
所以一定要签出并成为一个图片家庭，并帮助支持该系列
so definitely check that out and become

310
00:10:30,078 --> 00:10:31,919
所以一定要签出并成为一个图片家庭，并帮助支持该系列
a pic home and help support this series

311
00:10:32,120 --> 00:10:33,659
我会在下一个视频再见
I will see you guys in the next video

312
00:10:33,860 --> 00:10:35,509
我会在下一个视频再见
goodbye

313
00:10:35,710 --> 00:10:39,830
[音乐]
[Music]

314
00:10:44,240 --> 00:10:49,240
[音乐]
[Music]

