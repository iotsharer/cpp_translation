﻿1
00:00:00,000 --> 00:00:01,660
嘿，大家好，我叫作chato，欢迎回到我的C ++系列
hey what's up guys my name is the chato

2
00:00:01,860 --> 00:00:04,719
嘿，大家好，我叫作chato，欢迎回到我的C ++系列
and welcome back to my C++ series I

3
00:00:04,919 --> 00:00:06,790
不知道今天这手东西是什么我要谈论你的一切
don't know what this hand stuff is today

4
00:00:06,990 --> 00:00:08,020
不知道今天这手东西是什么我要谈论你的一切
I'm gonna be talking all about how you

5
00:00:08,220 --> 00:00:10,060
应该用C ++创建对象，因为C ++给了我们一些不同的东西
should be creating your objects in C++

6
00:00:10,259 --> 00:00:12,550
应该用C ++创建对象，因为C ++给了我们一些不同的东西
because C++ gives us a few different

7
00:00:12,750 --> 00:00:14,859
如果您不知道什么是对象，我们现在可以实际创建对象的方式
ways we can actually create an object

8
00:00:15,058 --> 00:00:16,179
如果您不知道什么是对象，我们现在可以实际创建对象的方式
now if you don't know what an object is

9
00:00:16,379 --> 00:00:17,740
还是您肯定想看一堂什么课，看看我遇到的视频
or what a class is you definitely want

10
00:00:17,940 --> 00:00:18,909
还是您肯定想看一堂什么课，看看我遇到的视频
to check out the video that I met on

11
00:00:19,109 --> 00:00:20,440
屏幕上会显示卡片或以下说明中的链接，但
that there'll be a card on the screen or

12
00:00:20,640 --> 00:00:21,820
屏幕上会显示卡片或以下说明中的链接，但
a link in the description below but

13
00:00:22,019 --> 00:00:23,470
基本上，当我们写完一堂课之后，该是我们真正开始学习的时候了
basically when we've written a class and

14
00:00:23,670 --> 00:00:25,568
基本上，当我们写完一堂课之后，该是我们真正开始学习的时候了
it comes time for us to actually start

15
00:00:25,768 --> 00:00:27,550
使用我们创建的类，我们通常需要实例化它，除非
using the class that we've created we

16
00:00:27,750 --> 00:00:29,710
使用我们创建的类，我们通常需要实例化它，除非
need to instantiate it usually unless

17
00:00:29,910 --> 00:00:31,419
就像是完全静态的，但我在说的是，我们需要
it's like completely static but what I'm

18
00:00:31,618 --> 00:00:32,948
就像是完全静态的，但我在说的是，我们需要
talking about that we need to

19
00:00:33,149 --> 00:00:35,169
实例化我们的课程我们该如何做，我们基本上有两个选择
instantiate our class how do we do it we

20
00:00:35,369 --> 00:00:37,299
实例化我们的课程我们该如何做，我们基本上有两个选择
basically have two choices here and the

21
00:00:37,500 --> 00:00:39,038
选择之间的差异是内存来自哪里
difference between the choices is where

22
00:00:39,238 --> 00:00:41,320
选择之间的差异是内存来自哪里
the memory comes from which memory were

23
00:00:41,520 --> 00:00:43,059
当我们在C ++中创建对象时，实际上将在创建对象
actually going to be creating our object

24
00:00:43,259 --> 00:00:46,000
当我们在C ++中创建对象时，实际上将在创建对象
in when we create an object in C++ it

25
00:00:46,200 --> 00:00:48,250
即使我们编写一个完全为空的类也需要占用一些内存
needs to occupy some memory even if we

26
00:00:48,450 --> 00:00:50,108
即使我们编写一个完全为空的类也需要占用一些内存
write a class that is completely empty

27
00:00:50,308 --> 00:00:52,448
没有成员，没有阶级成员，或者没有类似的东西
no members no class members or nothing

28
00:00:52,649 --> 00:00:52,959
没有成员，没有阶级成员，或者没有类似的东西
like that

29
00:00:53,159 --> 00:00:55,358
它必须要占用至少一个字节的内存，但这通常是我们的
it has to it has to occupy at least one

30
00:00:55,558 --> 00:00:56,739
它必须要占用至少一个字节的内存，但这通常是我们的
byte of memory but that's usually our

31
00:00:56,939 --> 00:00:58,268
案例中我们班上有很多成员，他们需要存储
case we have a lot of members in our

32
00:00:58,469 --> 00:01:00,939
案例中我们班上有很多成员，他们需要存储
classes and they need to be stored

33
00:01:01,140 --> 00:01:02,619
当我们决定要开始使用该对象时，我将创建一个
somewhere when we decide I want to start

34
00:01:02,820 --> 00:01:03,969
当我们决定要开始使用该对象时，我将创建一个
using this object I'm gonna create a

35
00:01:04,170 --> 00:01:05,379
一堆变量对象有一些我们需要分配的变量
bunch of variables the object has a

36
00:01:05,579 --> 00:01:07,179
一堆变量对象有一些我们需要分配的变量
bunch of variables we need to allocate

37
00:01:07,379 --> 00:01:09,640
存储在计算机中的某个位置，以便我们实际上可以记住什么
memory somewhere in our computer so that

38
00:01:09,840 --> 00:01:12,369
存储在计算机中的某个位置，以便我们实际上可以记住什么
we can actually remember what what what

39
00:01:12,569 --> 00:01:13,778
将变量设置为，并将它们的应用添加为两个
the variables are set to and their

40
00:01:13,978 --> 00:01:15,278
将变量设置为，并将它们的应用添加为两个
application is kind of added into two

41
00:01:15,478 --> 00:01:17,019
内存的主要部分堆栈和堆现在还有其他部分
main sections of memory the stack and

42
00:01:17,219 --> 00:01:19,539
内存的主要部分堆栈和堆现在还有其他部分
the heap now there are other sections of

43
00:01:19,739 --> 00:01:22,359
内存，例如我们的源代码所在的区域，因此，这是机器
memory such as the area where our source

44
00:01:22,560 --> 00:01:24,759
内存，例如我们的源代码所在的区域，因此，这是机器
code lives so by this point it's machine

45
00:01:24,959 --> 00:01:26,649
代码，所以还有内存的其他部分，我们稍后再讨论
code so there's other sections of memory

46
00:01:26,849 --> 00:01:28,689
代码，所以还有内存的其他部分，我们稍后再讨论
we're gonna talk about them later they

47
00:01:28,890 --> 00:01:30,039
现在并不重要，只需将其视为堆栈和
don't really matter right now just think

48
00:01:30,239 --> 00:01:31,539
现在并不重要，只需将其视为堆栈和
of it as the stack and the heap that's

49
00:01:31,739 --> 00:01:33,549
我们现在关心的一切，我将制作一个关于以下内容的深入视频
all we care about right now I am going

50
00:01:33,750 --> 00:01:36,250
我们现在关心的一切，我将制作一个关于以下内容的深入视频
to make an in-depth video about what the

51
00:01:36,450 --> 00:01:37,808
堆栈是什么，并且此堆栈总线中的热湾在运行，并且所有这些
stack is and what the heat Bay is in

52
00:01:38,009 --> 00:01:39,488
堆栈是什么，并且此堆栈总线中的热湾在运行，并且所有这些
this stack bus is behave and all that

53
00:01:39,688 --> 00:01:41,738
东西，如果我在观看此视频时已经做到的话，
stuff if I've already made it when

54
00:01:41,938 --> 00:01:42,819
东西，如果我在观看此视频时已经做到的话，
you're watching this video there'll be a

55
00:01:43,019 --> 00:01:44,799
如果没有的话就在那里，所以请紧紧抓住C ++，
card there if not it's coming soon so

56
00:01:45,000 --> 00:01:46,750
如果没有的话就在那里，所以请紧紧抓住C ++，
just hold on tight in C++ we get to

57
00:01:46,950 --> 00:01:49,959
选择我们的对象是在堆栈上还是在热量上创建
choose where it goes whether our object

58
00:01:50,159 --> 00:01:52,359
选择我们的对象是在堆栈上还是在热量上创建
gets created on the stack or on the heat

59
00:01:52,560 --> 00:01:53,799
并且它们具有不同的功能差异，用于
and they kind of have different

60
00:01:54,000 --> 00:01:56,168
并且它们具有不同的功能差异，用于
functional differences stack objects for

61
00:01:56,368 --> 00:01:58,778
例如具有自动寿命，其寿命实际上是由
example have an automatic lifespan right

62
00:01:58,978 --> 00:02:01,000
例如具有自动寿命，其寿命实际上是由
their lifetime is actually controlled by

63
00:02:01,200 --> 00:02:02,829
他们声明的范围以及该变量消失后尽快
the scope that they declared and as soon

64
00:02:03,030 --> 00:02:04,509
他们声明的范围以及该变量消失后尽快
as you as soon as that variable goes out

65
00:02:04,709 --> 00:02:05,489
内存可用的范围是
of scope

66
00:02:05,689 --> 00:02:07,560
内存可用的范围是
that's it that the memory is free

67
00:02:07,760 --> 00:02:10,230
因为当该作用域结束时，堆栈弹出，并且其中的任何内容
because when that scope ends the stack

68
00:02:10,430 --> 00:02:12,719
因为当该作用域结束时，堆栈弹出，并且其中的任何内容
pops and anything that was in that in

69
00:02:12,919 --> 00:02:14,460
该堆栈框架中的那个作用域框架现在变得磨损了
that scope frame in that stack frame

70
00:02:14,659 --> 00:02:17,189
该堆栈框架中的那个作用域框架现在变得磨损了
that gets that gets frayed now the heap

71
00:02:17,389 --> 00:02:19,530
不同的是，堆是这个大而又大的神秘地方，一旦你
is is different the heap is this big big

72
00:02:19,729 --> 00:02:21,510
不同的是，堆是这个大而又大的神秘地方，一旦你
big mysterious place where once you've

73
00:02:21,710 --> 00:02:23,370
在那个臀部分配了一个对象，实际上您已经在
allocated an object in that hip and

74
00:02:23,569 --> 00:02:25,170
在那个臀部分配了一个对象，实际上您已经在
you've actually created an object on the

75
00:02:25,370 --> 00:02:27,750
堆它会坐在那儿，直到你决定我不再需要它为止
heap it's gonna sit there until you

76
00:02:27,949 --> 00:02:29,850
堆它会坐在那儿，直到你决定我不再需要它为止
decide I no longer need it I want to

77
00:02:30,050 --> 00:02:31,200
释放该对象，让它随心所欲地做任何事情，让我们来看看
free that object do whatever you like

78
00:02:31,400 --> 00:02:32,969
释放该对象，让它随心所欲地做任何事情，让我们来看看
with that memory so let's take a look at

79
00:02:33,169 --> 00:02:34,469
这两种在上面创建对象的方法的代码是什么样的
what the code looks like for both of

80
00:02:34,669 --> 00:02:36,120
这两种在上面创建对象的方法的代码是什么样的
those methods of creating objects over

81
00:02:36,319 --> 00:02:38,070
这里我有一个叫做NC的类，它只有一个字符串，现在这个字符串是
here I've got a class called NC which

82
00:02:38,270 --> 00:02:39,780
这里我有一个叫做NC的类，它只有一个字符串，现在这个字符串是
just has a string now this string is

83
00:02:39,979 --> 00:02:41,730
只是一个最聪明的Raina在这里用了一点点，只是为了简化这段代码
just simply a sanest Raina put a little

84
00:02:41,930 --> 00:02:43,500
只是一个最聪明的Raina在这里用了一点点，只是为了简化这段代码
using up here just to simplify this code

85
00:02:43,699 --> 00:02:44,700
一点点，这样我们就不会到处写STD字符串了
a little bit so that we're not writing

86
00:02:44,900 --> 00:02:47,400
一点点，这样我们就不会到处写STD字符串了
STD string everywhere I usually do this

87
00:02:47,599 --> 00:02:49,260
因为我不喜欢使用命名空间STD，所以我知道很多人问我为什么不这样做
because I don't like using namespace STD

88
00:02:49,460 --> 00:02:51,660
因为我不喜欢使用命名空间STD，所以我知道很多人问我为什么不这样做
I know a lot of you ask me why I don't

89
00:02:51,860 --> 00:02:54,270
就像使用命名空间STD一样，我很快就会制作视频，所以请稍等
like using namespace STD I'm gonna make

90
00:02:54,469 --> 00:02:57,420
就像使用命名空间STD一样，我很快就会制作视频，所以请稍等
a video on that very soon so just wait

91
00:02:57,620 --> 00:02:59,430
为此，但基本上我们有一类，其中一个成员是一个字符串，然后
for it but basically we've got a class

92
00:02:59,629 --> 00:03:01,860
为此，但基本上我们有一类，其中一个成员是一个字符串，然后
with one member is a string and then

93
00:03:02,060 --> 00:03:03,420
我们有一个不带任何参数的构造函数
we've got one constructor which doesn't

94
00:03:03,620 --> 00:03:04,920
我们有一个不带任何参数的构造函数
take any parameters another constructor

95
00:03:05,120 --> 00:03:06,900
它确实将字符串作为参数，然后我们只设置名称
which does take in a string as a

96
00:03:07,099 --> 00:03:08,550
它确实将字符串作为参数，然后我们只设置名称
parameter and then we just set the name

97
00:03:08,750 --> 00:03:10,469
无论参数是什么，最后我们只有一个简单的吸气剂
to whatever the parameter is and then

98
00:03:10,669 --> 00:03:12,120
无论参数是什么，最后我们只有一个简单的吸气剂
finally we just have a simple getter

99
00:03:12,319 --> 00:03:13,740
东西的名字，所以我将像一个小小的虚拟类，现在让我们尝试
thing the name so I'll just made like a

100
00:03:13,939 --> 00:03:15,539
东西的名字，所以我将像一个小小的虚拟类，现在让我们尝试
little dummy class now let's try and

101
00:03:15,739 --> 00:03:17,370
在主函数中创建它我们该如何做好呢？
create it in the main function how do we

102
00:03:17,569 --> 00:03:19,620
在主函数中创建它我们该如何做好呢？
do that well the first option which is

103
00:03:19,819 --> 00:03:21,630
在堆栈上创建它非常简单
creating it on the stack is very very

104
00:03:21,830 --> 00:03:22,140
在堆栈上创建它非常简单
simple

105
00:03:22,340 --> 00:03:24,270
我们基本上输入要实例化的类的类型
we basically type in the type of the

106
00:03:24,469 --> 00:03:26,520
我们基本上输入要实例化的类的类型
class that we want to instantiate then

107
00:03:26,719 --> 00:03:28,350
我们点击空格键，然后给它取一个名字，我称这个实体为
we hit the spacebar and then we give it

108
00:03:28,550 --> 00:03:30,150
我们点击空格键，然后给它取一个名字，我称这个实体为
a name someone I call this entity and

109
00:03:30,349 --> 00:03:32,100
这就是它的全部内容，因为我们已经这样写了它实际上
that's all there is to it now because

110
00:03:32,300 --> 00:03:33,539
这就是它的全部内容，因为我们已经这样写了它实际上
we've written it like this it's actually

111
00:03:33,739 --> 00:03:35,700
正确调用默认构造函数，此代码可能看起来有些奇怪
calling the default constructor right

112
00:03:35,900 --> 00:03:37,950
正确调用默认构造函数，此代码可能看起来有些奇怪
this code might look a little bit weird

113
00:03:38,150 --> 00:03:39,509
如果您来自C Sharp或Java之类的语言，实际上
to you if you're coming from a language

114
00:03:39,709 --> 00:03:41,610
如果您来自C Sharp或Java之类的语言，实际上
like C sharp or Java in fact you might

115
00:03:41,810 --> 00:03:43,230
认为这会导致称为空指针异常或
think this this leads to something

116
00:03:43,430 --> 00:03:44,850
认为这会导致称为空指针异常或
called a null pointer exception or a

117
00:03:45,050 --> 00:03:46,770
空引用异常，因为它看起来像我们还没有初始化
null reference exception because it

118
00:03:46,969 --> 00:03:48,689
空引用异常，因为它看起来像我们还没有初始化
appears like we just haven't initialized

119
00:03:48,889 --> 00:03:51,300
我们的对象，但是只要我们在这里有这个默认构造函数，我们就可以了
our object but we have right as long as

120
00:03:51,500 --> 00:03:52,890
我们的对象，但是只要我们在这里有这个默认构造函数，我们就可以了
we have this default constructor here

121
00:03:53,090 --> 00:03:55,770
这是完全有效的代码，我们现在可以调用并查看未命名的内容，以及什么
this is totally valid code we can now

122
00:03:55,969 --> 00:03:58,259
这是完全有效的代码，我们现在可以调用并查看未命名的内容，以及什么
call and see don't get named and what

123
00:03:58,459 --> 00:04:02,039
我们实际上会得到很好，我们会得到名字
we'll actually get is well we'll get the

124
00:04:02,239 --> 00:04:02,520
我们实际上会得到很好，我们会得到名字
name

125
00:04:02,719 --> 00:04:04,020
在这种情况下，我们将变得未知，因为这是默认构造函数所做的
we'll get unknown in this case because

126
00:04:04,219 --> 00:04:05,430
在这种情况下，我们将变得未知，因为这是默认构造函数所做的
that's what the default constructor did

127
00:04:05,629 --> 00:04:05,900
所以将其打印到控制台上，看看
so

128
00:04:06,099 --> 00:04:07,490
所以将其打印到控制台上，看看
print this out to the console and see

129
00:04:07,689 --> 00:04:09,170
我们到达那里的原因未知，因为我们在这里所做的实际上是
what we get there we have it unknown

130
00:04:09,370 --> 00:04:10,820
我们到达那里的原因未知，因为我们在这里所做的实际上是
because what we've done here is actually

131
00:04:11,020 --> 00:04:12,260
不同于此代码在Java或C语言中的处理方式
different to what this code would have

132
00:04:12,460 --> 00:04:14,600
不同于此代码在Java或C语言中的处理方式
done in Java or C sharp and kind of will

133
00:04:14,800 --> 00:04:15,949
如果想指定一个参数，我们可以在一分钟内到达
kind of get there in a minute if we

134
00:04:16,149 --> 00:04:18,110
如果想指定一个参数，我们可以在一分钟内到达
wanted to specify a parameter all we

135
00:04:18,310 --> 00:04:20,270
需要做的就是打开括号，并给一个名字，例如Cherno
need to do is just open our parentheses

136
00:04:20,470 --> 00:04:22,460
需要做的就是打开括号，并给一个名字，例如Cherno
and give a name such as Cherno you can

137
00:04:22,660 --> 00:04:24,470
也可以这样做，我们可以写等于然后是构造函数的类型
also do it this way we can write equals

138
00:04:24,670 --> 00:04:26,600
也可以这样做，我们可以写等于然后是构造函数的类型
and then the type that's our constructor

139
00:04:26,800 --> 00:04:29,000
做得好，如果我们加起来运行五个程序，这次我们会变得chat不休，
done right and if we hit up five to run

140
00:04:29,199 --> 00:04:30,860
做得好，如果我们加起来运行五个程序，这次我们会变得chat不休，
our program we get chatter this time and

141
00:04:31,060 --> 00:04:32,960
一切都很好，所以什么时候我们要创建自己的东西呢？
everything's great so what's the deal

142
00:04:33,160 --> 00:04:34,819
一切都很好，所以什么时候我们要创建自己的东西呢？
with this when do we want to create our

143
00:04:35,019 --> 00:04:37,009
如果您可以创建一个这样的对象，答案几乎一直都是
objects like this the answer is pretty

144
00:04:37,209 --> 00:04:39,500
如果您可以创建一个这样的对象，答案几乎一直都是
much all the time if you can create an

145
00:04:39,699 --> 00:04:41,810
这样的对象确实会创建这样的对象，这基本上是规则
object like this do create an object

146
00:04:42,009 --> 00:04:43,850
这样的对象确实会创建这样的对象，这基本上是规则
like this that's basically the rule

147
00:04:44,050 --> 00:04:47,000
因为这是C ++中最快的方法，也是C ++中管理最多的方法
because this is the fastest way in C++

148
00:04:47,199 --> 00:04:49,910
因为这是C ++中最快的方法，也是C ++中管理最多的方法
and the most managed way in C++ to

149
00:04:50,110 --> 00:04:52,160
现在实际实例化对象现在让我们谈谈为什么会有
actually instantiate objects now let's

150
00:04:52,360 --> 00:04:54,110
现在实际实例化对象现在让我们谈谈为什么会有
talk about why why there would be

151
00:04:54,310 --> 00:04:56,150
原因是您无法做到这一点的原因之一是
reasons where you can't do this one of

152
00:04:56,350 --> 00:04:58,189
原因是您无法做到这一点的原因之一是
the reasons is if you actually want this

153
00:04:58,389 --> 00:05:00,710
如果我们还有其他功能，则生活在该功能的生命之外
to live outside of the life of this

154
00:05:00,910 --> 00:05:03,170
如果我们还有其他功能，则生活在该功能的生命之外
function if we had another function over

155
00:05:03,370 --> 00:05:05,540
在这里，我们在该功能中创建了实体
here and we created our entity over in

156
00:05:05,740 --> 00:05:07,699
在这里，我们在该功能中创建了实体
that function then as soon as we reached

157
00:05:07,899 --> 00:05:09,920
一旦我们达到这个目标，这个实体就结束了
the end as soon as we reach this this

158
00:05:10,120 --> 00:05:13,040
一旦我们达到这个目标，这个实体就结束了
this end curly bracket this entity gets

159
00:05:13,240 --> 00:05:14,990
从内存中销毁，因为发生的是当我们调用函数堆栈时
destroyed from memory because what

160
00:05:15,189 --> 00:05:17,720
从内存中销毁，因为发生的是当我们调用函数堆栈时
happens is when we call function a stack

161
00:05:17,920 --> 00:05:19,220
框架为此功劳功劳，其中包含所有局部变量
frame gets credit for this function

162
00:05:19,420 --> 00:05:21,170
框架为此功劳功劳，其中包含所有局部变量
which contains all the local variables

163
00:05:21,370 --> 00:05:23,060
我们声明其中包括原始类型，也包括我们的类和对象
that we declare which includes primitive

164
00:05:23,259 --> 00:05:25,850
我们声明其中包括原始类型，也包括我们的类和对象
types but also our classes our objects

165
00:05:26,050 --> 00:05:28,189
当该函数结束时，堆栈帧被破坏，这意味着
and when this function ends that stack

166
00:05:28,389 --> 00:05:29,990
当该函数结束时，堆栈帧被破坏，这意味着
frame gets destroyed which means that

167
00:05:30,189 --> 00:05:31,520
我们在堆栈上拥有的所有内存我们创建的所有变量都是
all of the memory that we had on the

168
00:05:31,720 --> 00:05:33,650
我们在堆栈上拥有的所有内存我们创建的所有变量都是
stack all the variables we created are

169
00:05:33,850 --> 00:05:36,259
消失了，让我们写一些实际上会使作用域失效的代码不一定
gone so let's write some code that would

170
00:05:36,459 --> 00:05:38,840
消失了，让我们写一些实际上会使作用域失效的代码不一定
actually fail scopes don't necessarily

171
00:05:39,040 --> 00:05:40,579
需要成为函数，如果语句为循环甚至为空的话
need to be functions they could be if

172
00:05:40,779 --> 00:05:42,230
需要成为函数，如果语句为循环甚至为空的话
statements for loops or even empty

173
00:05:42,430 --> 00:05:43,790
如果我已经阅读过一个
scopes in which we just have curly

174
00:05:43,990 --> 00:05:45,889
如果我已经阅读过一个
brackets like this if I've read an

175
00:05:46,089 --> 00:05:47,720
有趣的一点，现在这基本上是一个变量，它指向
interesting point uh now this is this is

176
00:05:47,920 --> 00:05:50,120
有趣的一点，现在这基本上是一个变量，它指向
basically a variable which points to an

177
00:05:50,319 --> 00:05:52,340
在这里，我将实体分配给所有Barents的内存地址
entity over here I'm going to assign it

178
00:05:52,540 --> 00:05:54,379
在这里，我将实体分配给所有Barents的内存地址
to the memory address all Barents the

179
00:05:54,579 --> 00:05:55,879
我们已经记入堆栈的对象，我也要
object that we've credit on the stack

180
00:05:56,079 --> 00:05:57,350
我们已经记入堆栈的对象，我也要
right over here I'm also going to

181
00:05:57,550 --> 00:05:58,819
简化此步骤，因为我通常会编写类似以下的代码
simplify this because I usually write

182
00:05:59,019 --> 00:06:00,290
简化此步骤，因为我通常会编写类似以下的代码
code that looks like this I'm going to

183
00:06:00,490 --> 00:06:02,360
在此行上按f9只是设置一个断点，我们将检查它
hit f9 on this line just to set a

184
00:06:02,560 --> 00:06:04,310
在此行上按f9只是设置一个断点，我们将检查它
breakpoint and we'll we'll inspect this

185
00:06:04,509 --> 00:06:06,139
所以在这里，我们创建一个新的实体对象等级，名称为
so right over here we create a new

186
00:06:06,339 --> 00:06:07,809
所以在这里，我们创建一个新的实体对象等级，名称为
entity object grade it's got the name

187
00:06:08,009 --> 00:06:09,939
当我按f10键向下移动时，一切都很棒，您可以看到我们已经设置好了
everything's great when I hit f10 to

188
00:06:10,139 --> 00:06:11,559
当我按f10键向下移动时，一切都很棒，您可以看到我们已经设置好了
move down you can see that we've now set

189
00:06:11,759 --> 00:06:14,468
我们的8.0是正确的，因此，如果我们将鼠标悬停在它上面，则实际上指向此正确
our 8.0 right so if we hover over a it

190
00:06:14,668 --> 00:06:16,569
我们的8.0是正确的，因此，如果我们将鼠标悬停在它上面，则实际上指向此正确
is in fact pointing to this correct

191
00:06:16,769 --> 00:06:18,009
名称中的内存地址是切达干酪，很棒
memory address in the name is cheddar

192
00:06:18,209 --> 00:06:18,879
名称中的内存地址是切达干酪，很棒
that's great

193
00:06:19,079 --> 00:06:22,028
但是，如果我们再次按f10键，然后前进到这条线，则可能
however if we hit f10 again and we

194
00:06:22,228 --> 00:06:23,379
但是，如果我们再次按f10键，然后前进到这条线，则可能
advance to this line and then maybe the

195
00:06:23,579 --> 00:06:25,480
下一行，我们将鼠标悬停在该行上，它仍指向同一地址
next line and we hover over look at that

196
00:06:25,680 --> 00:06:26,889
下一行，我们将鼠标悬停在该行上，它仍指向同一地址
it's still pointing to that same address

197
00:06:27,088 --> 00:06:29,379
该名称消失了，因为该对象被释放了，它被销毁了
that the name is gone because that

198
00:06:29,579 --> 00:06:31,989
该名称消失了，因为该对象被释放了，它被销毁了
object was was freed it was destroyed

199
00:06:32,189 --> 00:06:34,480
消失了，此频道高清不再存在，我们已经老化到堆栈中
gone this channel HD doesn't exist

200
00:06:34,680 --> 00:06:35,799
消失了，此频道高清不再存在，我们已经老化到堆栈中
anymore we were aged into the stack

201
00:06:35,999 --> 00:06:37,660
框架消失了，那是中国的终结，那是震颤的终结
frame it's gone that's the end of China

202
00:06:37,860 --> 00:06:39,699
框架消失了，那是中国的终结，那是震颤的终结
that's the end of that tremor go on

203
00:06:39,899 --> 00:06:41,619
蜂窝电话是很好的权利，因此，如果我们希望该频道以某种方式直播
cellular that's good right so if we

204
00:06:41,819 --> 00:06:43,600
蜂窝电话是很好的权利，因此，如果我们希望该频道以某种方式直播
wanted this channel to somehow live

205
00:06:43,800 --> 00:06:45,639
在范围之外，我们无法将其分配到堆栈上，因此我们不得不求助于
outside the scope we couldn't allocate

206
00:06:45,838 --> 00:06:46,929
在范围之外，我们无法将其分配到堆栈上，因此我们不得不求助于
it on the stack we would have to resort

207
00:06:47,129 --> 00:06:48,939
堆分配另一个我们可能不想要的原因，我们将能够
to heap allocation the other reason why

208
00:06:49,139 --> 00:06:50,439
堆分配另一个我们可能不想要的原因，我们将能够
we might not want to we'll be able to

209
00:06:50,639 --> 00:06:52,629
分配人员是因为如果这个实体的规模实际上也太大
allocate on the staff is because if the

210
00:06:52,829 --> 00:06:54,519
分配人员是因为如果这个实体的规模实际上也太大
size of this entity is actually too

211
00:06:54,718 --> 00:06:56,769
大我们可能有太多实体，我们可能没有足够的空间来实际
large we maybe have too many entities we

212
00:06:56,968 --> 00:06:58,179
大我们可能有太多实体，我们可能没有足够的空间来实际
might not have enough room to actually

213
00:06:58,379 --> 00:06:59,709
在堆栈上分配，因为堆栈通常很小，通常是一个
allocate on the stack because the stack

214
00:06:59,908 --> 00:07:01,389
在堆栈上分配，因为堆栈通常很小，通常是一个
is usually quite small it's usually one

215
00:07:01,588 --> 00:07:03,129
2兆字节，具体取决于您的平台和
megabyte two megabytes it kind of

216
00:07:03,329 --> 00:07:04,778
2兆字节，具体取决于您的平台和
depends on your platform and your

217
00:07:04,978 --> 00:07:07,359
编译器，但是如果您有这个巨型类，或者您想拥有一千个
compiler but if you have this giant

218
00:07:07,559 --> 00:07:09,790
编译器，但是如果您有这个巨型类，或者您想拥有一千个
class or you want to have a thousand of

219
00:07:09,990 --> 00:07:11,439
这些类，您可能在堆栈上没有足够的空间，因此您可能必须
these classes you might not have enough

220
00:07:11,639 --> 00:07:12,910
这些类，您可能在堆栈上没有足够的空间，因此您可能必须
room on the stack so you might have to

221
00:07:13,110 --> 00:07:14,588
在堆上分配让我们看一下堆分配是什么样的
allocate upon the heap let's take a look

222
00:07:14,788 --> 00:07:15,999
在堆上分配让我们看一下堆分配是什么样的
at what heap allocation looked like

223
00:07:16,199 --> 00:07:17,649
因此，如果我们想将当前电流转换为实际分配在堆上
so if we wanted to convert this current

224
00:07:17,848 --> 00:07:19,749
因此，如果我们想将当前电流转换为实际分配在堆上
here to actually allocate on the heap

225
00:07:19,949 --> 00:07:22,119
我们要做的是首先需要更改类型
what we would do is we would first of

226
00:07:22,319 --> 00:07:24,369
我们要做的是首先需要更改类型
all need to change the type the type is

227
00:07:24,569 --> 00:07:26,350
现在不再是实体，该类型是实体指针以及我们分配给的对象
now no longer entity the type is an

228
00:07:26,550 --> 00:07:28,778
现在不再是实体，该类型是实体指针以及我们分配给的对象
entity pointer and what we assigned to

229
00:07:28,978 --> 00:07:31,869
这里的实体是新实体，现在最大的区别实际上不是
entity here is new entity now the

230
00:07:32,069 --> 00:07:34,269
这里的实体是新实体，现在最大的区别实际上不是
biggest difference here is actually not

231
00:07:34,468 --> 00:07:35,889
很多人注意到它是新关键字的指针新关键字是
the pointer which a lot of people notice

232
00:07:36,088 --> 00:07:38,829
很多人注意到它是新关键字的指针新关键字是
it's the new keyword the new keyword is

233
00:07:39,028 --> 00:07:41,499
K，很快就会在新关键字上播放视频
K and that's gonna be video on that new

234
00:07:41,699 --> 00:07:43,899
K，很快就会在新关键字上播放视频
keyword very soon maybe even tomorrow

235
00:07:44,098 --> 00:07:46,088
我们将看到，当我们调用new时，看到实际发生的事情是我们分配
we'll see and when we call new and see

236
00:07:46,288 --> 00:07:48,309
我们将看到，当我们调用new时，看到实际发生的事情是我们分配
what actually happens is we allocate

237
00:07:48,509 --> 00:07:49,959
堆上的内存我们称为构造函数，而这个新实体实际上
memory on the heap we call the

238
00:07:50,158 --> 00:07:52,718
堆上的内存我们称为构造函数，而这个新实体实际上
constructor and this new entity actually

239
00:07:52,918 --> 00:07:55,269
返回一个实体指针，它返回该实体在堆上的位置
returns an entity pointer it returns the

240
00:07:55,468 --> 00:07:58,359
返回一个实体指针，它返回该实体在堆上的位置
location on the heap where this entity

241
00:07:58,559 --> 00:08:00,278
实际上已经分配了，这就是为什么我们必须将我们分配给HD指针的原因
has actually been allocated which is why

242
00:08:00,478 --> 00:08:02,079
实际上已经分配了，这就是为什么我们必须将我们分配给HD指针的原因
we have to assign us to an HD pointer

243
00:08:02,278 --> 00:08:05,679
这是您了解Java和C-sharp的人们在这里的
and this this is where you people who

244
00:08:05,879 --> 00:08:08,259
这是您了解Java和C-sharp的人们在这里的
know Java and c-sharp this is this is

245
00:08:08,459 --> 00:08:10,088
您将要编写的Java或C语言中的代码实际上是什么样的
what the code actually looks like in

246
00:08:10,288 --> 00:08:11,829
您将要编写的Java或C语言中的代码实际上是什么样的
Java or C sharp you would be writing

247
00:08:12,028 --> 00:08:14,920
这样的代码通常在您使用C ++时可能会
code like this so usually when you come

248
00:08:15,120 --> 00:08:17,739
这样的代码通常在您使用C ++时可能会
to C++ your instinct would be probably

249
00:08:17,939 --> 00:08:19,939
只是为了改变时间，突然将Java代码粉碎为c-sharp代码，
just to change the time and suddenly

250
00:08:20,139 --> 00:08:22,430
只是为了改变时间，突然将Java代码粉碎为c-sharp代码，
smashes Java code in c-sharp code and

251
00:08:22,629 --> 00:08:24,740
您是对的，这与Java和c-sharp代码无关，我们只是有一个额外的
you're right it does matter Java and

252
00:08:24,939 --> 00:08:26,480
您是对的，这与Java和c-sharp代码无关，我们只是有一个额外的
c-sharp code we just have an extra

253
00:08:26,680 --> 00:08:28,430
C ++中的选项，因此我们可以在堆栈上进行分配，而实际上在Java中无法做到这一点
option in C++ so we can allocate on the

254
00:08:28,629 --> 00:08:30,889
C ++中的选项，因此我们可以在堆栈上进行分配，而实际上在Java中无法做到这一点
stack you can't actually do that in Java

255
00:08:31,089 --> 00:08:34,129
或C-sharp中的C shop，您可以在c-sharp中有一个叫做struct的东西
or C shop in c-sharp you can in c-sharp

256
00:08:34,330 --> 00:08:35,599
或C-sharp中的C shop，您可以在c-sharp中有一个叫做struct的东西
there's something called a struct and

257
00:08:35,799 --> 00:08:37,519
这是一种基于值的类型，因此实际上是在
that's a value-based type and so that

258
00:08:37,720 --> 00:08:39,199
这是一种基于值的类型，因此实际上是在
actually is kind of allocated on the

259
00:08:39,399 --> 00:08:40,759
即使您使用了new关键字，但是在Java中，所有内容都在
stack even though you use the new

260
00:08:40,960 --> 00:08:43,309
即使您使用了new关键字，但是在Java中，所有内容都在
keyword but in Java everything's on the

261
00:08:43,509 --> 00:08:45,919
堆，这是一家商店，所有类都讨厌struct关键字，
heap and it's a shop all classes on the

262
00:08:46,120 --> 00:08:47,899
堆，这是一家商店，所有类都讨厌struct关键字，
hate the struct keyword is a bit

263
00:08:48,100 --> 00:08:49,639
与之不同的是，这是一个加号，也是我最大的问题之一
different than it isn't it's a plus plus

264
00:08:49,840 --> 00:08:51,349
与之不同的是，这是一个加号，也是我最大的问题之一
and one of the biggest problems that I

265
00:08:51,549 --> 00:08:54,079
看到的是所有来自诸如Java或C之类的托管语言的人
see is that everyone who comes over from

266
00:08:54,279 --> 00:08:55,870
看到的是所有来自诸如Java或C之类的托管语言的人
a managed language like Java or C sharp

267
00:08:56,070 --> 00:08:58,459
只是在C ++中到处都使用new关键字，因此您不应该这样做
just uses the new keyword everywhere in

268
00:08:58,659 --> 00:09:01,490
只是在C ++中到处都使用new关键字，因此您不应该这样做
C++ and you shouldn't be doing that for

269
00:09:01,690 --> 00:09:02,990
这两个原因以及我们将在该新视频中讨论的那些原因
two reasons and those reasons we're

270
00:09:03,190 --> 00:09:04,039
这两个原因以及我们将在该新视频中讨论的那些原因
going to talk about in that new video

271
00:09:04,240 --> 00:09:05,419
因为如果我谈论绝对的一切，这部影片将会很大
because this video is going to be huge

272
00:09:05,620 --> 00:09:06,919
因为如果我谈论绝对的一切，这部影片将会很大
if I talk about absolutely everything

273
00:09:07,120 --> 00:09:09,459
但是要缩短它，分配给堆的性能所需的时间比
but to cut it short performance

274
00:09:09,659 --> 00:09:11,689
但是要缩短它，分配给堆的性能所需的时间比
allocated on the heap takes longer than

275
00:09:11,889 --> 00:09:13,429
在堆栈上进行分配，并在堆上进行婚礼分配，您必须
allocating on the stack and also wedding

276
00:09:13,629 --> 00:09:14,629
在堆栈上进行分配，并在堆上进行婚礼分配，您必须
allocate on the heap you have to

277
00:09:14,830 --> 00:09:17,029
手动释放您分配的内存，以便一旦我们执行类似的操作
manually free that memory that you've

278
00:09:17,230 --> 00:09:19,129
手动释放您分配的内存，以便一旦我们执行类似的操作
allocated so once we do something like

279
00:09:19,330 --> 00:09:21,559
在我们安全的us +代码中，我们实际上负责释放该代码
this in our safe us+ code we are

280
00:09:21,759 --> 00:09:23,899
在我们安全的us +代码中，我们实际上负责释放该代码
actually responsible for freeing that

281
00:09:24,100 --> 00:09:25,579
记忆里说加老板不会突然决定好了
memory say plus boss is not going to

282
00:09:25,779 --> 00:09:27,709
记忆里说加老板不会突然决定好了
suddenly decide okay you're done with

283
00:09:27,909 --> 00:09:29,120
这个实体对象，它不知道我们已经完成了它，我们必须告诉它
this entity object it doesn't know that

284
00:09:29,320 --> 00:09:31,099
这个实体对象，它不知道我们已经完成了它，我们必须告诉它
we're done with it we have to tell it

285
00:09:31,299 --> 00:09:34,159
此内存是免费的，我们执行此操作的方式是调用delete，然后
this memory is free and the way that we

286
00:09:34,360 --> 00:09:37,879
此内存是免费的，我们执行此操作的方式是调用delete，然后
do that is we call delete and then the

287
00:09:38,080 --> 00:09:40,159
变量名称，所以如果您使用new关键字，则删除实体，请使用delete
variable name so delete entity if you

288
00:09:40,360 --> 00:09:42,469
变量名称，所以如果您使用new关键字，则删除实体，请使用delete
use the new keyword you use the delete

289
00:09:42,669 --> 00:09:44,870
清理自己的关键字，这就是简单的调味料的作用，因此在我们的
keyword to clean up after yourself that

290
00:09:45,070 --> 00:09:47,299
清理自己的关键字，这就是简单的调味料的作用，因此在我们的
is how simple sauce works so in our

291
00:09:47,500 --> 00:09:49,669
示例，因为如果我们确实必须调用delete来释放该对象
example since we do have to actually

292
00:09:49,870 --> 00:09:51,620
示例，因为如果我们确实必须调用delete来释放该对象
call delete to free this object if we

293
00:09:51,820 --> 00:09:53,779
被删除删除在这里也许甚至即使这个发送得到我们
were to remove delete down over here

294
00:09:53,980 --> 00:09:56,120
被删除删除在这里也许甚至即使这个发送得到我们
maybe even after this send get we're

295
00:09:56,320 --> 00:09:58,159
将此实体分配给e，这样我们就可以摆脱这个应用程序了，我们不需要
assigning this entity on to e so we'll

296
00:09:58,360 --> 00:09:59,750
将此实体分配给e，这样我们就可以摆脱这个应用程序了，我们不需要
get rid of this apps and we don't need

297
00:09:59,950 --> 00:10:02,329
因为实体已经是一个指针，而NC是一个指针
it since entity is already a pointer and

298
00:10:02,529 --> 00:10:04,729
因为实体已经是一个指针，而NC是一个指针
since NC is a pointer we actually have

299
00:10:04,929 --> 00:10:07,459
先取消引用，然后调用获取名称，或者我们可以使用
to either dereference this first and

300
00:10:07,659 --> 00:10:09,620
先取消引用，然后调用获取名称，或者我们可以使用
then call get name or we can use the

301
00:10:09,820 --> 00:10:11,359
错误运算符，为我们做到这一点，将在箭头运算符上显示一个视频
error operator which does that for us

302
00:10:11,559 --> 00:10:13,240
错误运算符，为我们做到这一点，将在箭头运算符上显示一个视频
gonna have a video on the arrow operator

303
00:10:13,440 --> 00:10:15,559
当然，我已经厌倦了这样做，但是更多的视频，如果我们失败了
certain I'm getting tired of doing this

304
00:10:15,759 --> 00:10:18,979
当然，我已经厌倦了这样做，但是更多的视频，如果我们失败了
but yet more videos and if we go down

305
00:10:19,179 --> 00:10:20,329
在这里，我们只是将延迟的实体更改为延迟的，所以这就是
over here we'll just change the late

306
00:10:20,529 --> 00:10:22,069
在这里，我们只是将延迟的实体更改为延迟的，所以这就是
entity to be delayed a so this is what

307
00:10:22,269 --> 00:10:23,839
我们的代码看起来像现在我们在分配的堆上创建一个实体，并在其中创建C
our code looks like now we create an

308
00:10:24,039 --> 00:10:26,359
我们的代码看起来像现在我们在分配的堆上创建一个实体，并在其中创建C
entity on the heap we assign and C to

309
00:10:26,559 --> 00:10:28,039
这是我们在这里没有复制任何数据
this we're not copying any data here

310
00:10:28,240 --> 00:10:28,599
这是我们在这里没有复制任何数据
really what

311
00:10:28,799 --> 00:10:30,279
要做的是在复制内存时是否存储存储着幻想
doing is whether storing the memory

312
00:10:30,480 --> 00:10:31,689
要做的是在复制内存时是否存储存储着幻想
dress the fantasy while copying the

313
00:10:31,889 --> 00:10:34,000
实际的高清对象只是内存地址，如果我按f5键，则会被窃听
actual HD object just the memory address

314
00:10:34,200 --> 00:10:36,639
实际的高清对象只是内存地址，如果我按f5键，则会被窃听
and if I hit f5 will be bugged this will

315
00:10:36,839 --> 00:10:38,439
按f10键，您会看到它设置正确，我们将其命名为Sugar。
hit f10 you can see that it gets set

316
00:10:38,639 --> 00:10:40,479
按f10键，您会看到它设置正确，我们将其命名为Sugar。
correctly we have our name sugar I'll

317
00:10:40,679 --> 00:10:42,159
再次获得f10，我将转到场景，将鼠标悬停在
get f10 again I'll go to I'll go to the

318
00:10:42,360 --> 00:10:44,469
再次获得f10，我将转到场景，将鼠标悬停在
scene get I'll hop on my mouse over in

319
00:10:44,669 --> 00:10:45,549
您会看到它仍然命名为Chennai，因为它只会
you can see it still has the name

320
00:10:45,750 --> 00:10:48,099
您会看到它仍然命名为Chennai，因为它只会
Chennai right because it only gets

321
00:10:48,299 --> 00:10:50,589
删除，它在这里免费，这是我们可以创建的两种方法
deleted and it gets free here and those

322
00:10:50,789 --> 00:10:52,089
删除，它在这里免费，这是我们可以创建的两种方法
are the two ways that we can create

323
00:10:52,289 --> 00:10:54,279
C ++中的对象并在两者之间进行选择是我的对象的问题
objects in C++ and choosing between the

324
00:10:54,480 --> 00:10:56,979
C ++中的对象并在两者之间进行选择是我的对象的问题
two is a matter of is my objects really

325
00:10:57,179 --> 00:10:58,839
真的很大，还是我想明确控制自己的生命周期
really really big or do I want to

326
00:10:59,039 --> 00:11:00,578
真的很大，还是我想明确控制自己的生命周期
explicitly control the lifetime of my

327
00:11:00,778 --> 00:11:02,919
如果您对我在堆栈上找到的两个问题都回答否，则反对
object if you answered no to both of

328
00:11:03,120 --> 00:11:04,568
如果您对我在堆栈上找到的两个问题都回答否，则反对
those questions I locate on the stack

329
00:11:04,769 --> 00:11:06,490
正确地创建或进入堆栈，这将使它更容易实现自动化，并且
right create or get on the stack it's

330
00:11:06,690 --> 00:11:09,969
正确地创建或进入堆栈，这将使它更容易实现自动化，并且
way easier it's automated and it's

331
00:11:10,169 --> 00:11:11,979
更快，而在堆上分配则需要您手动调用delete
faster whereas allocated on the heap

332
00:11:12,179 --> 00:11:13,870
更快，而在堆上分配则需要您手动调用delete
requires you to manually called delete

333
00:11:14,070 --> 00:11:15,909
如果您忘记调用delete可能会导致内存泄漏我的意思是
which can lead to memory leaks if you

334
00:11:16,110 --> 00:11:17,919
如果您忘记调用delete可能会导致内存泄漏我的意思是
forget to call delete I mean it's a bit

335
00:11:18,120 --> 00:11:19,419
比忘记很多人更难想清楚
harder than just like forgetting a lot

336
00:11:19,620 --> 00:11:20,828
比忘记很多人更难想清楚
of people think well how often you

337
00:11:21,028 --> 00:11:22,419
忘了打电话给服务员，人们知道你可以错过它，有时我
forget to call the waiter people knew

338
00:11:22,620 --> 00:11:24,578
忘了打电话给服务员，人们知道你可以错过它，有时我
you can you can miss it sometimes I

339
00:11:24,778 --> 00:11:26,409
猜它很好，它变得复杂以及发现您可以
guess it's good it gets complicated as

340
00:11:26,610 --> 00:11:28,419
猜它很好，它变得复杂以及发现您可以
well as well as will discover you can

341
00:11:28,620 --> 00:11:29,589
还使用一种称为智能指针的东西，我们会去那些
also use something called a smart

342
00:11:29,789 --> 00:11:31,750
还使用一种称为智能指针的东西，我们会去那些
pointer we're kind of gonna get to those

343
00:11:31,950 --> 00:11:33,819
最终和林肯，我什至不知道我真的很厌倦
eventually and Lincoln that I don't even

344
00:11:34,019 --> 00:11:35,349
最终和林肯，我什至不知道我真的很厌倦
know I'm getting really tired of doing

345
00:11:35,549 --> 00:11:36,639
我不会以想回来的方式来追踪它，不是
this I'm not gonna keep track of this by

346
00:11:36,839 --> 00:11:38,229
我不会以想回来的方式来追踪它，不是
the way I want to come back it's not

347
00:11:38,429 --> 00:11:39,279
就像我几个月后要回到这部影片一样
like I'm gonna come back to this video

348
00:11:39,480 --> 00:11:40,599
就像我几个月后要回到这部影片一样
in a few months and be like oh yeah

349
00:11:40,799 --> 00:11:42,309
最好填写所有这些即时贴，所以只要在我的频道中搜索这些视频即可
better fill in all those cards so just

350
00:11:42,509 --> 00:11:43,628
最好填写所有这些即时贴，所以只要在我的频道中搜索这些视频即可
search my channel for those videos if

351
00:11:43,828 --> 00:11:44,740
他们在那里，他们是北面板，但有了智能指针，我们可以
they're there that they're for the north

352
00:11:44,940 --> 00:11:46,599
他们在那里，他们是北面板，但有了智能指针，我们可以
panel but with smart pointers we can

353
00:11:46,799 --> 00:11:48,429
实际上仍然分配在堆上并仍然具有这种大小
actually kind of still allocate on the

354
00:11:48,629 --> 00:11:50,919
实际上仍然分配在堆上并仍然具有这种大小
heap and still get that kind of size

355
00:11:51,120 --> 00:11:53,859
优势，但是当
advantage but also have our objects be

356
00:11:54,059 --> 00:11:55,899
优势，但是当
automatically deleted when either the

357
00:11:56,100 --> 00:11:58,809
指针超出范围或可能在共享指针的情况下
pointer goes out of scope or maybe like

358
00:11:59,009 --> 00:12:00,669
指针超出范围或可能在共享指针的情况下
in the case of shared pointers when

359
00:12:00,870 --> 00:12:02,500
没有更多的参考资料将讨论将来最古老的内存内容
there are no more references will talk

360
00:12:02,700 --> 00:12:05,229
没有更多的参考资料将讨论将来最古老的内存内容
about oldest memory stuff in the future

361
00:12:05,429 --> 00:12:06,399
我现在不想参与其中，但是如果我们只是在谈论某种
I don't want to get into it right now

362
00:12:06,600 --> 00:12:08,319
我现在不想参与其中，但是如果我们只是在谈论某种
but if we're just talking about kind of

363
00:12:08,519 --> 00:12:10,479
原始C ++使对象堆栈和堆积的两种方法
primitive C++ two ways to make objects

364
00:12:10,679 --> 00:12:11,769
原始C ++使对象堆栈和堆积的两种方法
stack and heap

365
00:12:11,970 --> 00:12:13,959
那些是我最近在堆栈上传播的，除非您绝对不能做你们
those are my recent spread on the stack

366
00:12:14,159 --> 00:12:15,698
那些是我最近在堆栈上传播的，除非您绝对不能做你们
unless you absolutely can't do you guys

367
00:12:15,899 --> 00:12:16,809
欣赏这部影片，然后您可以按一下喜欢的按钮，就可以支持该影片
enjoy this video then you can hit that

368
00:12:17,009 --> 00:12:18,339
欣赏这部影片，然后您可以按一下喜欢的按钮，就可以支持该影片
like button and you can support this

369
00:12:18,539 --> 00:12:20,589
在patreon.com上的此类椅子系列哦，您基本上会很早得到一集
series on patreon.com for such the chair

370
00:12:20,789 --> 00:12:22,569
在patreon.com上的此类椅子系列哦，您基本上会很早得到一集
oh you'll get episodes early basically

371
00:12:22,769 --> 00:12:24,399
当我完成编辑后，这些情节便会发布给所有顾客
as soon as I'm done editing the episodes

372
00:12:24,600 --> 00:12:25,959
当我完成编辑后，这些情节便会发布给所有顾客
get released to all the patrons which is

373
00:12:26,159 --> 00:12:28,448
很酷，您还可以讨论和讨论这些视频中包含的内容
pretty cool you can also discuss and

374
00:12:28,649 --> 00:12:30,219
很酷，您还可以讨论和讨论这些视频中包含的内容
talk about what goes into these videos

375
00:12:30,419 --> 00:12:31,839
并为新视频和所有有趣的内容提供建议，您也可以提供帮助
and make suggestions for new videos and

376
00:12:32,039 --> 00:12:34,328
并为新视频和所有有趣的内容提供建议，您也可以提供帮助
all that fun stuff and you also help

377
00:12:34,528 --> 00:12:35,829
支持这个系列，并确保下次我很漂亮时继续制作视频
support this series and make sure that I

378
00:12:36,029 --> 00:12:37,689
支持这个系列，并确保下次我很漂亮时继续制作视频
keep making videos next time I'm pretty

379
00:12:37,889 --> 00:12:38,769
确保我们要谈论新关键字，这样应该会令人兴奋
sure we're gonna talk about the new

380
00:12:38,970 --> 00:12:40,479
确保我们要谈论新关键字，这样应该会令人兴奋
keyword so that should be exciting

381
00:12:40,679 --> 00:12:42,309
一定会看那个视频，这将是在哈哈，我告别了
definitely watch that video it'll be in

382
00:12:42,509 --> 00:12:46,730
一定会看那个视频，这将是在哈哈，我告别了
the haha I'm done goodbye

383
00:12:53,399 --> 00:12:58,399
您
you

