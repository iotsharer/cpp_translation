﻿1
00:00:00,030 --> 00:00:02,290
大家好，我的名字是chana欢迎回到我的C ++系列，所以今天我们
hello guys my name is the chana welcome

2
00:00:02,490 --> 00:00:04,868
大家好，我的名字是chana欢迎回到我的C ++系列，所以今天我们
back to my C++ series so today we're

3
00:00:05,068 --> 00:00:06,189
将要讨论的全部是C ++中的结构化绑定，这是C ++特有的
gonna be talking all about structured

4
00:00:06,389 --> 00:00:09,540
将要讨论的全部是C ++中的结构化绑定，这是C ++特有的
bindings in C++ this is specific to C++

5
00:00:09,740 --> 00:00:12,550
17个结构化绑定是一项新功能，可让我们处理多次返回
17 structured bindings are a new feature

6
00:00:12,750 --> 00:00:14,559
17个结构化绑定是一项新功能，可让我们处理多次返回
that let us deal with multiple return

7
00:00:14,759 --> 00:00:16,629
值好一点了，现在我做了一段有关如何应对的视频
values a little bit better now I did

8
00:00:16,829 --> 00:00:18,190
值好一点了，现在我做了一段有关如何应对的视频
make a video about how to deal with

9
00:00:18,390 --> 00:00:21,010
如果您还没有C ++中的多个返回值，请签出该视频
multiple return values in C++ check out

10
00:00:21,210 --> 00:00:22,269
如果您还没有C ++中的多个返回值，请签出该视频
that video if you haven't already I'll

11
00:00:22,469 --> 00:00:24,579
将其链接到那里，这是对它的一种扩展
have it linked up there and this is kind

12
00:00:24,778 --> 00:00:26,350
将其链接到那里，这是对它的一种扩展
of extending upon that with kind of a

13
00:00:26,550 --> 00:00:28,870
我们如何处理这个问题的新方法，特别是我们如何处理元组
new way of how we can deal with this

14
00:00:29,070 --> 00:00:30,909
我们如何处理这个问题的新方法，特别是我们如何处理元组
specifically how we can deal with tuples

15
00:00:31,109 --> 00:00:33,070
和配对，并返回类似的结果，因为结构化绑定只是让我们
and pairs and returning things like that

16
00:00:33,270 --> 00:00:34,959
和配对，并返回类似的结果，因为结构化绑定只是让我们
because structured bindings just let us

17
00:00:35,159 --> 00:00:36,669
某种程度上简化了我们的代码，使其比过去更加干净
kind of simplify our code make it a lot

18
00:00:36,869 --> 00:00:39,159
某种程度上简化了我们的代码，使其比过去更加干净
cleaner than what it was in the past in

19
00:00:39,359 --> 00:00:40,419
关于如何在C ++中处理多个值的视频
that video about how to deal with

20
00:00:40,619 --> 00:00:42,518
关于如何在C ++中处理多个值的视频
multiple values in C++ I did

21
00:00:42,719 --> 00:00:44,739
特别提到我喜欢结构，并且我喜欢基本上返回实例
specifically mention that I like structs

22
00:00:44,939 --> 00:00:47,709
特别提到我喜欢结构，并且我喜欢基本上返回实例
and I like to return basically instances

23
00:00:47,909 --> 00:00:49,989
包含我实际想要的数据成员的结构，这就是我的方式
of structs which contain the members of

24
00:00:50,189 --> 00:00:51,788
包含我实际想要的数据成员的结构，这就是我的方式
data that I actually want that's how I

25
00:00:51,988 --> 00:00:53,709
我个人喜欢与可能有潜在
personally like dealing with multiple

26
00:00:53,909 --> 00:00:56,498
我个人喜欢与可能有潜在
return guys that could have potentially

27
00:00:56,698 --> 00:00:58,658
通过引入结构化绑定进行了更改，实际上，它具有
changed with this introduction of

28
00:00:58,859 --> 00:01:00,369
通过引入结构化绑定进行了更改，实际上，它具有
structured bindings and in fact it has

29
00:01:00,570 --> 00:01:02,320
发生了变化，因为在过去的一两年中，我确实以自己的代码
changed because over the past like year

30
00:01:02,520 --> 00:01:04,569
发生了变化，因为在过去的一两年中，我确实以自己的代码
or two really in my own code I've kind

31
00:01:04,769 --> 00:01:06,759
的人注意到我一直在使用元组，喜欢成对和轮胎，而我一直在
of noticed that I've been using tuples

32
00:01:06,959 --> 00:01:09,310
的人注意到我一直在使用元组，喜欢成对和轮胎，而我一直在
and like pairs and tires and I've been

33
00:01:09,510 --> 00:01:11,379
使用那种基本上具有多个返回值的东西
using that kind of stuff basically

34
00:01:11,579 --> 00:01:13,269
使用那种基本上具有多个返回值的东西
having multiple return value soaked into

35
00:01:13,469 --> 00:01:15,009
像元组一样，我经常使用它，因为结构化绑定
like a tuple I've been using that a lot

36
00:01:15,209 --> 00:01:16,689
像元组一样，我经常使用它，因为结构化绑定
more often because structured bindings

37
00:01:16,890 --> 00:01:19,028
帮助我实际上使我的代码仍然易于管理，因为我以前绝对
help me actually make my code still

38
00:01:19,228 --> 00:01:20,679
帮助我实际上使我的代码仍然易于管理，因为我以前绝对
manageable because I used to absolutely

39
00:01:20,879 --> 00:01:22,689
讨厌以前的样子实际上让我们看一下它的样子
hate what it was like before in fact

40
00:01:22,890 --> 00:01:24,250
讨厌以前的样子实际上让我们看一下它的样子
let's take a look at what it was like

41
00:01:24,450 --> 00:01:26,439
在此之前，我要写一个非常简单的示例，从头开始运行它
before so I'm gonna write a very simple

42
00:01:26,640 --> 00:01:27,789
在此之前，我要写一个非常简单的示例，从头开始运行它
example just running it from scratch

43
00:01:27,989 --> 00:01:29,259
在这里使其变得非常明显，我在这里要做的就是写一个
here to make it really really obvious

44
00:01:29,459 --> 00:01:30,759
在这里使其变得非常明显，我在这里要做的就是写一个
and what I'll do here is just write a

45
00:01:30,959 --> 00:01:33,009
现在创建一个人的函数是一个很好的例子，因为您
function that creates a person now a

46
00:01:33,209 --> 00:01:34,778
现在创建一个人的函数是一个很好的例子，因为您
person is a nice example because you

47
00:01:34,978 --> 00:01:36,488
可能想存储有关某个人的更多详细信息，而不仅仅是他们的姓名
might want to store more details about a

48
00:01:36,688 --> 00:01:38,289
可能想存储有关某个人的更多详细信息，而不仅仅是他们的姓名
person than just for example their name

49
00:01:38,489 --> 00:01:40,329
在这种情况下，我们将处理他们的姓名和年龄，因此我们将编写一个函数
in this case we'll deal with their name

50
00:01:40,530 --> 00:01:42,069
在这种情况下，我们将处理他们的姓名和年龄，因此我们将编写一个函数
and their age so we'll write a function

51
00:01:42,269 --> 00:01:45,099
称为创建人，我们需要设置一个返回类型，将其设置为SCD
called create person we'll need to set a

52
00:01:45,299 --> 00:01:47,230
称为创建人，我们需要设置一个返回类型，将其设置为SCD
return type which we will set to SCD

53
00:01:47,430 --> 00:01:49,659
元组，它将包含多种返回值的实际种类
tuple and this is gonna contain the

54
00:01:49,859 --> 00:01:51,369
元组，它将包含多种返回值的实际种类
actual kind of multiple return values

55
00:01:51,569 --> 00:01:53,230
在这种情况下，我们要处理的是名称为
that we want to deal with in this case

56
00:01:53,430 --> 00:01:55,808
在这种情况下，我们要处理的是名称为
we'll deal with a string for the name of

57
00:01:56,009 --> 00:01:58,268
这个人，然后是他们年龄的整数，我们将在此处包括字符串
the person and then an integer for their

58
00:01:58,468 --> 00:02:00,399
这个人，然后是他们年龄的整数，我们将在此处包括字符串
age and we'll include string up here as

59
00:02:00,599 --> 00:02:03,009
好吧，当然我们不需要只写年龄，所以我们
well and of course we don't need to

60
00:02:03,209 --> 00:02:05,140
好吧，当然我们不需要只写年龄，所以我们
actually write age just the type so we

61
00:02:05,340 --> 00:02:07,509
在这里有一个字符串和int我们将简单地返回，在这种情况下会很好
have a string and int over here we'll

62
00:02:07,709 --> 00:02:09,580
在这里有一个字符串和int我们将简单地返回，在这种情况下会很好
simply return in this case will be nice

63
00:02:09,780 --> 00:02:11,600
很简单，我们只需返回名称，然后为我的年龄手表
and simple we'll just simply return

64
00:02:11,800 --> 00:02:14,660
很简单，我们只需返回名称，然后为我的年龄手表
as the name and then an age watch for my

65
00:02:14,860 --> 00:02:16,760
年龄是24岁，所以我们现在基本上有一种机制可以返回两个
age which is the 24 so now we have

66
00:02:16,960 --> 00:02:19,700
年龄是24岁，所以我们现在基本上有一种机制可以返回两个
basically a mechanism to return two

67
00:02:19,900 --> 00:02:21,710
不同类型的数据，一个字符串和一个INT，而不必像创建一个
different types of data a string and an

68
00:02:21,909 --> 00:02:23,450
不同类型的数据，一个字符串和一个INT，而不必像创建一个
INT without having to like create a

69
00:02:23,650 --> 00:02:24,890
struct之类的东西，或者就像通过引用传递参数或
struct or anything like that or just

70
00:02:25,090 --> 00:02:26,840
struct之类的东西，或者就像通过引用传递参数或
like pass parameters by reference or

71
00:02:27,039 --> 00:02:29,120
就像作为一个指针一样好，简单，干净，在这种情况下，您可以
like as a pointer nice and simple and

72
00:02:29,319 --> 00:02:30,590
就像作为一个指针一样好，简单，干净，在这种情况下，您可以
clean of course in this case you could

73
00:02:30,789 --> 00:02:32,120
只需使用一对，因为实际数据有两个变量
just use a pair because there are two

74
00:02:32,319 --> 00:02:33,650
只需使用一对，因为实际数据有两个变量
variables that this actual data

75
00:02:33,849 --> 00:02:35,330
结构保持不变，但是通过元组，您当然可以将其扩展为
structure is holding however with the

76
00:02:35,530 --> 00:02:37,100
结构保持不变，但是通过元组，您当然可以将其扩展为
tuple you can of course expand this to

77
00:02:37,300 --> 00:02:39,020
容器基本上可以根据需要获取尽可能多的值，因此在以前的版本中
containers basically as many values as

78
00:02:39,219 --> 00:02:41,990
容器基本上可以根据需要获取尽可能多的值，因此在以前的版本中
you want so the in previous versions of

79
00:02:42,189 --> 00:02:44,120
使用C ++，您可以选择几种方式来处理这些问题
C++ you had a few options for how you

80
00:02:44,319 --> 00:02:45,590
使用C ++，您可以选择几种方式来处理这些问题
would deal with this well write some

81
00:02:45,789 --> 00:02:47,150
代码来表明这种情况，因此我将编写STD元组字符串，然后您可以看到
code to kind of show that so I'll write

82
00:02:47,349 --> 00:02:50,150
代码来表明这种情况，因此我将编写STD元组字符串，然后您可以看到
STD tuple string and and you can see

83
00:02:50,349 --> 00:02:51,650
该类型已经有点混乱，但是我们将在这里让我们的人
already the type is a little bit messy

84
00:02:51,849 --> 00:02:53,480
该类型已经有点混乱，但是我们将在这里让我们的人
but we'll have our person here then

85
00:02:53,680 --> 00:02:55,070
我们将其设置为等于现在创建人员，以简化此操作
we'll set that equal to create person

86
00:02:55,270 --> 00:02:56,870
我们将其设置为等于现在创建人员，以简化此操作
now to simplify this a little bit more

87
00:02:57,069 --> 00:02:58,730
您实际上可以在这里使用订单，这就是我可能要做的
you can just use order here in fact

88
00:02:58,930 --> 00:03:00,260
您实际上可以在这里使用订单，这就是我可能要做的
that's what I probably would do in this

89
00:03:00,460 --> 00:03:02,270
情况，但是现在我们有访问数据的任务，这就是它的位置
case but now we have the task of

90
00:03:02,469 --> 00:03:04,310
情况，但是现在我们有访问数据的任务，这就是它的位置
accessing the data and this is where it

91
00:03:04,509 --> 00:03:07,040
变得非常烦人，基本上就是这样，你不能只是做人
gets really really annoying so basically

92
00:03:07,240 --> 00:03:08,689
变得非常烦人，基本上就是这样，你不能只是做人
the way that you can't just do person

93
00:03:08,889 --> 00:03:11,480
点，您知道名称，就好像它是一个结构一样，您必须使用STD get，然后将其用作
dot you know name as if it was a struct

94
00:03:11,680 --> 00:03:14,630
点，您知道名称，就好像它是一个结构一样，您必须使用STD get，然后将其用作
you have to use STD get and then as a

95
00:03:14,830 --> 00:03:16,939
例如，模板参数要获取的数据索引为零
template argument the index of the data

96
00:03:17,139 --> 00:03:18,259
例如，模板参数要获取的数据索引为零
you want to get so zero for example

97
00:03:18,459 --> 00:03:21,259
将返回名称的第一个变量是字符串，然后如果我使用一个
would return the name the first variable

98
00:03:21,459 --> 00:03:23,810
将返回名称的第一个变量是字符串，然后如果我使用一个
which is a string and then if I used one

99
00:03:24,009 --> 00:03:25,189
在这里，这将返回年龄，基本上可以得到这个，然后您将
here that would return the age so

100
00:03:25,389 --> 00:03:26,600
在这里，这将返回年龄，基本上可以得到这个，然后您将
basically to get this and you put the

101
00:03:26,800 --> 00:03:29,540
这里将实际变量作为参数，以便获取实际字符串
actual variable as a parameter here so

102
00:03:29,740 --> 00:03:31,670
这里将实际变量作为参数，以便获取实际字符串
to get the actual string which is the

103
00:03:31,870 --> 00:03:33,800
名称，我必须编写这样的代码，否则您当然可以使用auto，然后
name I'd have to write code like this or

104
00:03:34,000 --> 00:03:35,210
名称，我必须编写这样的代码，否则您当然可以使用auto，然后
you can use auto of course and then the

105
00:03:35,409 --> 00:03:37,160
年龄也看起来有点神秘，基本上是
age as well would look a little bit

106
00:03:37,360 --> 00:03:39,110
年龄也看起来有点神秘，基本上是
cryptic and it would basically be the

107
00:03:39,310 --> 00:03:41,120
与sed一样得到一个，这有点好，这只是不好
same with sed get one and this is a

108
00:03:41,319 --> 00:03:43,430
与sed一样得到一个，这有点好，这只是不好
little bit well it's it's just not nice

109
00:03:43,629 --> 00:03:45,410
在这种情况下，我可能永远不会使用三元组
I probably would never use a triple for

110
00:03:45,610 --> 00:03:46,790
在这种情况下，我可能永远不会使用三元组
this case now there is something called

111
00:03:46,990 --> 00:03:49,550
STD领带比实际创建的领带要好一些
STD tie which is a little bit nicer than

112
00:03:49,750 --> 00:03:51,620
STD领带比实际创建的领带要好一些
this you still have to actually create

113
00:03:51,819 --> 00:03:55,130
真正的变量，因此名称和年龄在这里，但是您实际上可以做的就是
true variables so name and age over here

114
00:03:55,330 --> 00:03:57,290
真正的变量，因此名称和年龄在这里，但是您实际上可以做的就是
but what you can actually do is just

115
00:03:57,490 --> 00:03:59,689
通过他们在这里，他们通过参考名称年龄传递，然后只是
pass them in here they get passed in by

116
00:03:59,889 --> 00:04:01,759
通过他们在这里，他们通过参考名称年龄传递，然后只是
a reference name an age and then just

117
00:04:01,959 --> 00:04:04,280
将其设置为等于创建人，这比所有这些要好一点
set this equal to create person and this

118
00:04:04,479 --> 00:04:06,170
将其设置为等于创建人，这比所有这些要好一点
is a little bit nicer than all of this

119
00:04:06,370 --> 00:04:07,400
当然我们没有实际的人变量，因为我们不在那里
of course we don't have our actual

120
00:04:07,599 --> 00:04:09,200
当然我们没有实际的人变量，因为我们不在那里
person variable because we don't there

121
00:04:09,400 --> 00:04:10,759
没有人是对的不是像它是一个结构，不是像它自己的类型
is no person right it's not like it's a

122
00:04:10,959 --> 00:04:12,830
没有人是对的不是像它是一个结构，不是像它自己的类型
struct it's not like it's its own type

123
00:04:13,030 --> 00:04:15,289
在这种情况下，它只是一个元组的容器，它容纳了我们想要的
it is just a container a tuple in this

124
00:04:15,489 --> 00:04:17,629
在这种情况下，它只是一个元组的容器，它容纳了我们想要的
case that holds what we want which is a

125
00:04:17,829 --> 00:04:20,538
字符串和整数，所以看起来确实更好，我可能是
string and an int so this does look a

126
00:04:20,738 --> 00:04:23,270
字符串和整数，所以看起来确实更好，我可能是
bit nicer and I would probably be a

127
00:04:23,470 --> 00:04:24,590
多一点倾向于这样做
little bit more inclined to do it this

128
00:04:24,790 --> 00:04:25,340
多一点倾向于这样做
way how

129
00:04:25,540 --> 00:04:27,230
它是否仍然占用三行代码，看起来还是不
whether it still takes up like three

130
00:04:27,430 --> 00:04:28,759
它是否仍然占用三行代码，看起来还是不
lines of code it still does not look

131
00:04:28,959 --> 00:04:31,250
很好，我仍然可能会使用一个结构，基本上只是创建一个
nice and I still probably would use a

132
00:04:31,449 --> 00:04:33,790
很好，我仍然可能会使用一个结构，基本上只是创建一个
struct and just basically create a

133
00:04:33,990 --> 00:04:36,740
人，这样我就可以返回它，然后显然可以通过
person so that then I could return that

134
00:04:36,939 --> 00:04:38,509
人，这样我就可以返回它，然后显然可以通过
and then obviously just access it by

135
00:04:38,709 --> 00:04:40,819
我在该C ++视频中显示的多个人的姓名和年龄
person name and person till age as I

136
00:04:41,019 --> 00:04:43,220
我在该C ++视频中显示的多个人的姓名和年龄
showed in that C++ video about multiple

137
00:04:43,420 --> 00:04:44,990
现在返回类型，这是您一直在等待的部分，这是
return types now this is the part that

138
00:04:45,189 --> 00:04:46,340
现在返回类型，这是您一直在等待的部分，这是
you've been waiting for this is where

139
00:04:46,540 --> 00:04:48,259
C ++ 17为我们带来了一个新的功能，称为结构化绑定，可以解决所有问题
C++ 17 brings us a new feature called

140
00:04:48,459 --> 00:04:50,210
C ++ 17为我们带来了一个新的功能，称为结构化绑定，可以解决所有问题
structured bindings that solves all

141
00:04:50,410 --> 00:04:51,889
这些问题，使我们的代码看起来非常不错，而不是全部完成
these problems and makes our code look

142
00:04:52,089 --> 00:04:53,990
这些问题，使我们的代码看起来非常不错，而不是全部完成
really nice instead of doing all of this

143
00:04:54,189 --> 00:04:57,290
我会在这里举起我的人，而不是做所有这些，我们仍然保持
I will raise my person up here instead

144
00:04:57,490 --> 00:04:59,000
我会在这里举起我的人，而不是做所有这些，我们仍然保持
of doing all this we still keep this as

145
00:04:59,199 --> 00:05:00,980
一个元组，我们不需要STD领带或类似我们需要做的所有事情
a tuple we don't need STD tie or

146
00:05:01,180 --> 00:05:03,259
一个元组，我们不需要STD领带或类似我们需要做的所有事情
anything like that all we need to do we

147
00:05:03,459 --> 00:05:04,430
可以摆脱这两行代码，我们要做的就是使用单词author
can get rid of these two lines of code

148
00:05:04,629 --> 00:05:06,949
可以摆脱这两行代码，我们要做的就是使用单词author
all we need to do is use the word author

149
00:05:07,149 --> 00:05:10,100
其次是我们给变量的两个名称，因此我们可以给它们任何
followed by the two names that we give

150
00:05:10,300 --> 00:05:12,290
其次是我们给变量的两个名称，因此我们可以给它们任何
to our variable so we can give these any

151
00:05:12,490 --> 00:05:15,110
我们想要的名称，因此在这种情况下名称和年龄会很好，然后我们
names we want whatsoever so name and age

152
00:05:15,310 --> 00:05:16,970
我们想要的名称，因此在这种情况下名称和年龄会很好，然后我们
would be good in this case and then we

153
00:05:17,170 --> 00:05:18,650
只需将其设置为等于创建人，这是一个字符串，这是一个
just set it equal to create person and

154
00:05:18,850 --> 00:05:21,290
只需将其设置为等于创建人，这是一个字符串，这是一个
that is it this is a string this is an

155
00:05:21,490 --> 00:05:23,540
int它为我们完美地完成了所有工作，如果我们想喜欢我们可以打印
int it does everything for us perfectly

156
00:05:23,740 --> 00:05:25,220
int它为我们完美地完成了所有工作，如果我们想喜欢我们可以打印
and if we wanted to like we could print

157
00:05:25,420 --> 00:05:26,870
我们可以用这些变量做任何我们想做的事情，因为现在它们是
this we could do anything we want with

158
00:05:27,069 --> 00:05:28,759
我们可以用这些变量做任何我们想做的事情，因为现在它们是
these variables because now they're

159
00:05:28,959 --> 00:05:30,800
完全在这个范围内，我们现在完全可以访问，请记住这一点
totally just in this scope and totally

160
00:05:31,000 --> 00:05:32,900
完全在这个范围内，我们现在完全可以访问，请记住这一点
accessible to us now keep in mind this

161
00:05:33,100 --> 00:05:35,629
仅在C ++ 17和更高版本的say plus plus中具有此功能，因此如果
feature is only in C++ 17 and newer

162
00:05:35,829 --> 00:05:37,370
仅在C ++ 17和更高版本的say plus plus中具有此功能，因此如果
versions of say plus plus so if this

163
00:05:37,569 --> 00:05:39,199
不会为您编译，请确保您没有一次编译一次plus 14或
does not compile for you make sure

164
00:05:39,399 --> 00:05:40,970
不会为您编译，请确保您没有一次编译一次plus 14或
you're not compiling once a plus 14 or

165
00:05:41,170 --> 00:05:42,860
11或类似的名称，请确保您将销售加版本切换为
11 or anything like that make sure you

166
00:05:43,060 --> 00:05:44,810
11或类似的名称，请确保您将销售加版本切换为
switch your sales plus version to say

167
00:05:45,009 --> 00:05:46,280
再加上Visual Studio中的十七个，我们可以在这里进入
plus plus seventeen specifically in

168
00:05:46,480 --> 00:05:48,319
再加上Visual Studio中的十七个，我们可以在这里进入
Visual Studio we can go over here into

169
00:05:48,519 --> 00:05:50,930
属性，然后输入C ++语言，然后确保
properties and then go into say C++

170
00:05:51,129 --> 00:05:53,090
属性，然后输入C ++语言，然后确保
language and then make sure that our

171
00:05:53,290 --> 00:05:54,800
如果已将语言标准设置为Sables plus 17标准
language standard is actually set to

172
00:05:55,000 --> 00:05:57,259
如果已将语言标准设置为Sables plus 17标准
Sables plus 17 standard if it's set on

173
00:05:57,459 --> 00:05:59,480
默认情况下可能无法正常工作，例如，如果我确实将其切换为默认值，然后尝试
default it may not work so for example

174
00:05:59,680 --> 00:06:01,400
默认情况下可能无法正常工作，例如，如果我确实将其切换为默认值，然后尝试
if I do switch it to default and I try

175
00:06:01,600 --> 00:06:03,020
并编译此代码，您会看到它将无法编译，我们将收到错误消息
and compile this code you can see that

176
00:06:03,220 --> 00:06:05,060
并编译此代码，您会看到它将无法编译，我们将收到错误消息
it will not compile we will get an error

177
00:06:05,259 --> 00:06:06,230
说这个名字是未联系的标识符，实际上我们会得到一个
saying that name is uncontacted

178
00:06:06,430 --> 00:06:08,120
说这个名字是未联系的标识符，实际上我们会得到一个
identifier and in fact we will get an

179
00:06:08,319 --> 00:06:09,530
告诉我们需要做什么的错误，它说明了结构化的语言功能
error that tells us what we need to do

180
00:06:09,730 --> 00:06:11,120
告诉我们需要做什么的错误，它说明了结构化的语言功能
it says the language features structured

181
00:06:11,319 --> 00:06:14,300
绑定需要编译器标志c ++ 17，因此只需确保将其切换到
bindings requires compiler flag c++ 17

182
00:06:14,500 --> 00:06:16,069
绑定需要编译器标志c ++ 17，因此只需确保将其切换到
so to switch it to that just make sure

183
00:06:16,269 --> 00:06:18,079
您进入属性并将样本语言标准设置为c ++
you go into your properties and set your

184
00:06:18,279 --> 00:06:21,050
您进入属性并将样本语言标准设置为c ++
sample stuff language standard to be c++

185
00:06:21,250 --> 00:06:23,030
当然，如果您使用的是其他IDE或其他版本，则现在为17
17 now of course if you're using a

186
00:06:23,230 --> 00:06:24,560
当然，如果您使用的是其他IDE或其他版本，则现在为17
different IDE or a different build

187
00:06:24,759 --> 00:06:26,270
系统而不是像我在这种情况下那样的Visual Studio，那么您必须
system than Visual Studio like I am in

188
00:06:26,470 --> 00:06:28,069
系统而不是像我在这种情况下那样的Visual Studio，那么您必须
this case then you'll have to do the

189
00:06:28,269 --> 00:06:30,050
等同于此，但请保存我们再加上17，这是您需要的结构
equivalent for that but save us plus 17

190
00:06:30,250 --> 00:06:32,090
等同于此，但请保存我们再加上17，这是您需要的结构
is what you need to have structured

191
00:06:32,290 --> 00:06:34,009
绑定以使用这样的结构化绑定，我想再给大家展示一下
bindings to use structured bindings like

192
00:06:34,209 --> 00:06:35,480
绑定以使用这样的结构化绑定，我想再给大家展示一下
this I want to show you guys one more

193
00:06:35,680 --> 00:06:37,189
例子，所以我在这里是从
example so what I've got here is some

194
00:06:37,389 --> 00:06:37,910
例子，所以我在这里是从
code from the

195
00:06:38,110 --> 00:06:40,009
OpenGL系列特别是着色器编译的代码，我确实谈到了
OpenGL series specifically the shader

196
00:06:40,209 --> 00:06:42,410
OpenGL系列特别是着色器编译的代码，我确实谈到了
compilation' code I did talk about the

197
00:06:42,610 --> 00:06:44,210
在该视频中使用元组的可能性以及我可能会在
possibility of using tuples in that

198
00:06:44,410 --> 00:06:46,400
在该视频中使用元组的可能性以及我可能会在
video as well which I might link in the

199
00:06:46,600 --> 00:06:48,590
右上角，但是基本上我们最终要做的是我们需要通过
top right corner but basically what we

200
00:06:48,790 --> 00:06:50,270
右上角，但是基本上我们最终要做的是我们需要通过
ended up doing was we need to pass a

201
00:06:50,470 --> 00:06:52,189
我们在文件路径层中使用的着色器，然后我们只读取该文件
shader which we took in a file path

202
00:06:52,389 --> 00:06:53,629
我们在文件路径层中使用的着色器，然后我们只读取该文件
floor and then we just read that file

203
00:06:53,829 --> 00:06:55,100
并且此函数的结果是将该着色器文件分为两个
and the result of this function was

204
00:06:55,300 --> 00:06:56,569
并且此函数的结果是将该着色器文件分为两个
splitting up that shader file into two

205
00:06:56,769 --> 00:06:58,520
基于着色器类型的不同字符串，因此我们做了哪些工作来支持该着色器
different strings based on the shader

206
00:06:58,720 --> 00:07:01,160
基于着色器类型的不同字符串，因此我们做了哪些工作来支持该着色器
type and so what we did to support that

207
00:07:01,360 --> 00:07:02,870
我们是否返回了一个结构，如您所见，如果我们查看该结构是什么，
is we returned a struct as you can see

208
00:07:03,069 --> 00:07:04,579
我们是否返回了一个结构，如您所见，如果我们查看该结构是什么，
if we look at what that struct is we

209
00:07:04,779 --> 00:07:06,379
有顶酱和碎酱，所以我要做的就是改变这个
have a vertex sauce and a fragment sauce

210
00:07:06,579 --> 00:07:08,150
有顶酱和碎酱，所以我要做的就是改变这个
so what I'm going to do is change this

211
00:07:08,350 --> 00:07:10,160
使用结构化绑定，这样我们就根本不需要此结构
to use structured bindings so that we

212
00:07:10,360 --> 00:07:11,540
使用结构化绑定，这样我们就根本不需要此结构
don't need this struct at all so alt

213
00:07:11,740 --> 00:07:13,550
删除结构将在这里进入我们的路径着色器
delete the struct will come over here

214
00:07:13,750 --> 00:07:16,009
删除结构将在这里进入我们的路径着色器
into our path shader we're not going to

215
00:07:16,209 --> 00:07:17,810
在这里使用一对，我们将使用元组，原因是理论上这可以
use a pair here we'll use a tuple the

216
00:07:18,009 --> 00:07:20,000
在这里使用一对，我们将使用元组，原因是理论上这可以
reason is that this theoretically could

217
00:07:20,199 --> 00:07:22,460
除了片段和顶点着色器之外，还支持更多类型
support more types than just fragment

218
00:07:22,660 --> 00:07:23,930
除了片段和顶点着色器之外，还支持更多类型
and vertex shaders which is what we've

219
00:07:24,129 --> 00:07:25,699
现在到达那里，所以我要做的就是再次使用三重
got there right now so what I'll do is

220
00:07:25,899 --> 00:07:27,319
现在到达那里，所以我要做的就是再次使用三重
just use the triple again you could use

221
00:07:27,519 --> 00:07:28,879
一对，然后将其更改为元组，它不应破坏任何代码，而只是
a pair then later change it into tuple

222
00:07:29,079 --> 00:07:30,829
一对，然后将其更改为元组，它不应破坏任何代码，而只是
it shouldn't break any code but just

223
00:07:31,029 --> 00:07:32,210
给出为什么我在这种情况下用于构建父表的原因
giving a reason behind why I'm using to

224
00:07:32,410 --> 00:07:33,259
给出为什么我在这种情况下用于构建父表的原因
build parent table in this case

225
00:07:33,459 --> 00:07:35,600
相同的结果好吧，所以我们将在这里介绍，我们还将包括
identical results okay so we'll come

226
00:07:35,800 --> 00:07:37,430
相同的结果好吧，所以我们将在这里介绍，我们还将包括
over here and we'll also include that

227
00:07:37,629 --> 00:07:41,329
元组，现在我将跳入我的公交车代码，将其更改为
tuple and now I will hop over into my a

228
00:07:41,529 --> 00:07:43,579
元组，现在我将跳入我的公交车代码，将其更改为
bus bus code I will change this to be

229
00:07:43,779 --> 00:07:45,439
同样很酷的事情是，这个return语句不会在
the same thing the cool thing is that

230
00:07:45,639 --> 00:07:47,329
同样很酷的事情是，这个return语句不会在
this return statement does not change at

231
00:07:47,529 --> 00:07:49,310
都是因为我们仍然以完全相同的方式构造元组，但是如果我们来
all because we still construct the tuple

232
00:07:49,509 --> 00:07:51,560
都是因为我们仍然以完全相同的方式构造元组，但是如果我们来
in exactly the same way but if we come

233
00:07:51,759 --> 00:07:53,540
在这里进入我们实际使用此代码的位置，而不是此处
over here into where we actually use

234
00:07:53,740 --> 00:07:55,819
在这里进入我们实际使用此代码的位置，而不是此处
this code which is up here instead of

235
00:07:56,019 --> 00:07:57,680
使用此着色器程序源类型，然后执行源类型顶点或
using this shader program source type

236
00:07:57,879 --> 00:07:59,329
使用此着色器程序源类型，然后执行源类型顶点或
and then doing source type vertex or

237
00:07:59,529 --> 00:08:01,129
源表碎片源，而不是源表，现在我们只能使用order
source table fragments source instead of

238
00:08:01,329 --> 00:08:03,530
源表碎片源，而不是源表，现在我们只能使用order
that we can now just use order followed

239
00:08:03,730 --> 00:08:07,009
通过顶点源和片段调味料，然后将其设置为等于传递标头
by vertex source and fragment sauce and

240
00:08:07,209 --> 00:08:09,980
通过顶点源和片段调味料，然后将其设置为等于传递标头
then just set that equal to pass header

241
00:08:10,180 --> 00:08:12,079
然后进入创建函数，它也消失了，只是变得
and then into the create a function this

242
00:08:12,279 --> 00:08:14,000
然后进入创建函数，它也消失了，只是变得
also disappears and just simply becomes

243
00:08:14,199 --> 00:08:16,699
在这种情况下，您现在可以看到顶点酱和碎片酱
a vertex sauce and fragments sauce now

244
00:08:16,899 --> 00:08:18,410
在这种情况下，您现在可以看到顶点酱和碎片酱
in this case you can see that this does

245
00:08:18,610 --> 00:08:20,270
无法编译，因为我们需要将其切换为当前的C ++ 17
not compile because we need to switch

246
00:08:20,470 --> 00:08:23,090
无法编译，因为我们需要将其切换为当前的C ++ 17
this to be C++ 17 which it currently is

247
00:08:23,290 --> 00:08:25,009
并非如此，让我们在这里做到这是语言标准，我们将其设置为
not so let's just do that here it is

248
00:08:25,209 --> 00:08:26,900
并非如此，让我们在这里做到这是语言标准，我们将其设置为
language standard and we'll set that to

249
00:08:27,100 --> 00:08:29,600
C ++ 17现在您可以看到错误消失了，这就是我们剩下的。
C++ 17 now you can see the error goes

250
00:08:29,800 --> 00:08:31,880
C ++ 17现在您可以看到错误消失了，这就是我们剩下的。
away and this is what we're left with so

251
00:08:32,080 --> 00:08:34,429
非常简单，非常干净的代码，如果我们返回头文件，您会看到
really simple really clean code if we go

252
00:08:34,629 --> 00:08:35,839
非常简单，非常干净的代码，如果我们返回头文件，您会看到
back to our header file you can see we

253
00:08:36,039 --> 00:08:37,370
完全摆脱了着色器酱结构，我们知道少一种
got rid of that shader sauce struct

254
00:08:37,570 --> 00:08:39,289
完全摆脱了着色器酱结构，我们知道少一种
altogether we know how one less type

255
00:08:39,490 --> 00:08:40,939
漂浮在其中更容易清洁得多，因为这确实是
floating around which is a lot cleaner a

256
00:08:41,139 --> 00:08:42,919
漂浮在其中更容易清洁得多，因为这确实是
lot easier because this is really the

257
00:08:43,120 --> 00:08:45,379
在我们的代码中，我们实际上要使用该着色器的唯一情况是看到了struct this
only case in our code where we actually

258
00:08:45,580 --> 00:08:48,529
在我们的代码中，我们实际上要使用该着色器的唯一情况是看到了struct this
want to use that shader saw struct this

259
00:08:48,730 --> 00:08:50,339
在我们的所有代码库中都被使用过，您不想将其用于
was used all around our code base

260
00:08:50,539 --> 00:08:51,568
在我们的所有代码库中都被使用过，您不想将其用于
you wouldn't want to wear this into a

261
00:08:51,769 --> 00:08:53,128
元组并使用这样的结构化绑定，但在这种情况下，它是如此
tuple and use structured bindings like

262
00:08:53,328 --> 00:08:54,929
元组并使用这样的结构化绑定，但在这种情况下，它是如此
this but in this case it just makes so

263
00:08:55,129 --> 00:08:57,870
从某种意义上说，为什么有一个只使用一次的类型，而我真的不喜欢
much sense why have a type that you only

264
00:08:58,070 --> 00:09:00,419
从某种意义上说，为什么有一个只使用一次的类型，而我真的不喜欢
use once that I don't really like the

265
00:09:00,620 --> 00:09:02,008
之所以这样，是因为它只会使您的代码基础混乱
idea of that because it just clutters up

266
00:09:02,208 --> 00:09:03,419
之所以这样，是因为它只会使您的代码基础混乱
your code base you have an extra type

267
00:09:03,620 --> 00:09:05,729
不需要浮动，您可以使用它的结构化绑定和
floating around not necessary you can

268
00:09:05,929 --> 00:09:06,990
不需要浮动，您可以使用它的结构化绑定和
just use it structured bindings and

269
00:09:07,190 --> 00:09:09,389
元组或成对的情况，希望你们喜欢这个视频，
tuples or pairs in this case and I hope

270
00:09:09,589 --> 00:09:10,828
元组或成对的情况，希望你们喜欢这个视频，
you guys enjoyed this video and that you

271
00:09:11,028 --> 00:09:12,058
如果可以的话，可以从查看真实示例中受益
kind of benefited from seeing a

272
00:09:12,259 --> 00:09:13,859
如果可以的话，可以从查看真实示例中受益
real-world example if you did you can

273
00:09:14,059 --> 00:09:15,508
在下方添加一个赞，您也可以通过赞助人来帮助支持该频道
drop a like below you can also help

274
00:09:15,708 --> 00:09:17,490
在下方添加一个赞，您也可以通过赞助人来帮助支持该频道
support this channel by going to patron

275
00:09:17,690 --> 00:09:19,679
斜线之家表示衷心感谢，所有向来的顾客
home for slash that show huge thank you

276
00:09:19,879 --> 00:09:21,419
斜线之家表示衷心感谢，所有向来的顾客
as always to all the patrons that made

277
00:09:21,620 --> 00:09:22,740
该视频可能会在下面对您的评论发表评论
this video possible

278
00:09:22,940 --> 00:09:24,389
该视频可能会在下面对您的评论发表评论
drop a comment below on what you would

279
00:09:24,589 --> 00:09:26,279
希望接下来看到C ++涵盖的内容我确实有很长的清单，我认为
like to see covered in C++ next I do

280
00:09:26,480 --> 00:09:28,469
希望接下来看到C ++涵盖的内容我确实有很长的清单，我认为
have a very long list I think there's

281
00:09:28,669 --> 00:09:30,089
像20种物品，这些都是我要通过的东西，我要尝试把
like 20 items are sort of what I'm gonna

282
00:09:30,289 --> 00:09:32,068
像20种物品，这些都是我要通过的东西，我要尝试把
get through I'm gonna try and put out a

283
00:09:32,269 --> 00:09:34,349
尽可能多地为您提供视频，因此请确保您订阅了
video for you as often as possible so

284
00:09:34,549 --> 00:09:35,549
尽可能多地为您提供视频，因此请确保您订阅了
make sure that you subscribe if you

285
00:09:35,750 --> 00:09:37,289
还没有，我下次见
haven't already and I will see you next

286
00:09:37,490 --> 00:09:39,500
还没有，我下次见
time goodbye

287
00:09:39,700 --> 00:09:44,700
[音乐]
[Music]

