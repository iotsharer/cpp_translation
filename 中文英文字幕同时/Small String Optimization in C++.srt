﻿1
00:00:00,000 --> 00:00:01,420
大家好，我叫Archana，欢迎回到我说的老板老板系列
hello guys my name is Archana and

2
00:00:01,620 --> 00:00:03,188
大家好，我叫Archana，欢迎回到我说的老板老板系列
welcome back to my say boss boss series

3
00:00:03,388 --> 00:00:04,750
所以今天我们将要讨论字符串，特别是一些东西
so today we're going to be talking about

4
00:00:04,950 --> 00:00:06,368
所以今天我们将要讨论字符串，特别是一些东西
strings and specifically something

5
00:00:06,569 --> 00:00:09,700
简称为小字符串优化或SSR，这意味着我们为什么
called small string optimization or SSR

6
00:00:09,900 --> 00:00:12,309
简称为小字符串优化或SSR，这意味着我们为什么
for short what does it mean why do we

7
00:00:12,509 --> 00:00:15,250
护理，它如何改善我们的弦乐体验，现在弦乐非常
care and how does it improve our string

8
00:00:15,449 --> 00:00:17,169
护理，它如何改善我们的弦乐体验，现在弦乐非常
experience now strings are a very

9
00:00:17,368 --> 00:00:18,129
我认为有趣的话题是在Sabo的失落或受伤中
interesting topic

10
00:00:18,329 --> 00:00:20,140
我认为有趣的话题是在Sabo的失落或受伤中
I think in Sabo's lost or injured just

11
00:00:20,339 --> 00:00:21,850
一般而言，在任何语言中，因为它们具有所有这种负面的污名
in any language in general because they

12
00:00:22,050 --> 00:00:24,730
一般而言，在任何语言中，因为它们具有所有这种负面的污名
have all of this kind of negative stigma

13
00:00:24,929 --> 00:00:26,739
我想与他们相关联的是他们的速度较慢，
I guess associated with them they have

14
00:00:26,939 --> 00:00:29,789
我想与他们相关联的是他们的速度较慢，
this reputation of being slower and

15
00:00:29,989 --> 00:00:32,259
甚至不要开始与C ++程序员谈论性能，因为
don't even start talking about

16
00:00:32,460 --> 00:00:35,669
甚至不要开始与C ++程序员谈论性能，因为
performance to a C++ programmer because

17
00:00:35,869 --> 00:00:40,779
男人，如果C ++的运行速度较慢，那么查看某人的代码真的很容易
man if something's slower in C++ it's

18
00:00:40,979 --> 00:00:42,250
男人，如果C ++的运行速度较慢，那么查看某人的代码真的很容易
really easy to look at someone's code

19
00:00:42,450 --> 00:00:44,140
看到它们时，您会皱眉，然后立即使用字符串
see them using strings in a way that you

20
00:00:44,340 --> 00:00:45,399
看到它们时，您会皱眉，然后立即使用字符串
just frown upon and then immediately

21
00:00:45,600 --> 00:00:47,529
认为他们不是一个好的程序员，或者他们所做的事情
think that they're either not a good

22
00:00:47,729 --> 00:00:49,119
认为他们不是一个好的程序员，或者他们所做的事情
programmer or they're doing things that

23
00:00:49,320 --> 00:00:51,219
愚蠢的慢速方式，与字符串相关的负面因素太多
stupid slower way there's just so much

24
00:00:51,420 --> 00:00:53,529
愚蠢的慢速方式，与字符串相关的负面因素太多
negativity associated with strings

25
00:00:53,729 --> 00:00:55,989
相信我，但我在这里是要告诉你，这并不全都是坏事，事实上有时候
believe me but I'm here to tell you that

26
00:00:56,189 --> 00:00:58,599
相信我，但我在这里是要告诉你，这并不全都是坏事，事实上有时候
it's not all bad and in fact sometimes

27
00:00:58,799 --> 00:01:00,779
当您看到有人使用字符串时，可能还不如您想像的糟
when you do see someone using a string

28
00:01:00,979 --> 00:01:03,518
当您看到有人使用字符串时，可能还不如您想像的糟
maybe it's not as bad as you think

29
00:01:03,719 --> 00:01:04,869
现在一般而言，我想我会继续
now strings in general I think I

30
00:01:05,069 --> 00:01:05,950
现在一般而言，我想我会继续
something they're just gonna continue

31
00:01:06,150 --> 00:01:07,629
时不时地谈论这个频道，无法进行编程
talking about every now and then on this

32
00:01:07,829 --> 00:01:09,039
时不时地谈论这个频道，无法进行编程
channel it's impossible to program

33
00:01:09,239 --> 00:01:10,719
没有字符串，但另一方面减少了您对字符串的使用
without strings but on the other hand

34
00:01:10,920 --> 00:01:13,238
没有字符串，但另一方面减少了您对字符串的使用
reducing your usage of strings in your

35
00:01:13,438 --> 00:01:14,980
代码可以显着加快速度，具体取决于您实际使用它们的方式
code can significantly speed it up

36
00:01:15,180 --> 00:01:16,750
代码可以显着加快速度，具体取决于您实际使用它们的方式
depending on how in fact you do use them

37
00:01:16,950 --> 00:01:19,959
因此，希望这个话题继续存在，但是今天我们要特别关注的是
so expect this topic to stay around but

38
00:01:20,159 --> 00:01:21,129
因此，希望这个话题继续存在，但是今天我们要特别关注的是
today specifically we're going to be

39
00:01:21,329 --> 00:01:23,109
看西伯尔定律实际上是如何优化小弦的
looking at how small strings are

40
00:01:23,310 --> 00:01:25,090
看西伯尔定律实际上是如何优化小弦的
actually optimized by the sibyl's law

41
00:01:25,290 --> 00:01:27,219
标准库，因此它们最终不会像您想象的那样糟糕，而是首先
standard library so that they end up

42
00:01:27,420 --> 00:01:29,769
标准库，因此它们最终不会像您想象的那样糟糕，而是首先
being not as bad as you think but first

43
00:01:29,969 --> 00:01:31,000
我要感谢主持人赞助这部影片，如果你们有
I want to thank hosting out for

44
00:01:31,200 --> 00:01:32,738
我要感谢主持人赞助这部影片，如果你们有
sponsoring this video if you guys have

45
00:01:32,938 --> 00:01:34,959
没有听说过托管，它现在是一个快速，安全的网络托管平台
not heard of hosting out hosting it is a

46
00:01:35,159 --> 00:01:38,439
没有听说过托管，它现在是一个快速，安全的网络托管平台
fast and secure web hosting platform now

47
00:01:38,640 --> 00:01:40,179
我认为，如今，2020年，几乎每个人都应该拥有一个网站，如果
I think that nowadays in 2020 pretty

48
00:01:40,379 --> 00:01:42,128
我认为，如今，2020年，几乎每个人都应该拥有一个网站，如果
much everyone should have a website if

49
00:01:42,328 --> 00:01:43,509
您没有网站，并且正努力成为一名程序员
you don't have a website and you're

50
00:01:43,709 --> 00:01:46,480
您没有网站，并且正努力成为一名程序员
trying to become a programmer well you

51
00:01:46,680 --> 00:01:48,308
需要某种网站，以便您可以轻松地向人们展示您的身份
need some kind of website so that you

52
00:01:48,509 --> 00:01:50,500
需要某种网站，以便您可以轻松地向人们展示您的身份
can easily show people what it is you're

53
00:01:50,700 --> 00:01:51,849
工作和取得的成就，特别是如果您正在寻找工作
working on and what you've achieved

54
00:01:52,049 --> 00:01:53,259
工作和取得的成就，特别是如果您正在寻找工作
especially if you're looking for a job

55
00:01:53,459 --> 00:01:55,090
甚至没有让我着手做，而人们没有意识到的是
don't even get me started on that and

56
00:01:55,290 --> 00:01:56,558
甚至没有让我着手做，而人们没有意识到的是
what people don't realize is that the

57
00:01:56,759 --> 00:01:58,448
所有这一切的额外好处是您将知道如何建立一个网站
added benefit to all this is that you

58
00:01:58,649 --> 00:02:00,488
所有这一切的额外好处是您将知道如何建立一个网站
will know how to set up a website the

59
00:02:00,688 --> 00:02:01,750
您在互联网上立即拥有一个网站的事实意味着
fact that you've got a website on the

60
00:02:01,950 --> 00:02:04,209
您在互联网上立即拥有一个网站的事实意味着
Internet to me immediately means that

61
00:02:04,409 --> 00:02:05,918
好吧，你知道如何建立一个网站，你知道如何网络
well you know how to set up a website

62
00:02:06,118 --> 00:02:06,950
好吧，你知道如何建立一个网站，你知道如何网络
you know how web

63
00:02:07,150 --> 00:02:09,380
单一的口音本身就是一种优势，现在有很多值得一爱的地方
single accent that's a perp in and of

64
00:02:09,580 --> 00:02:10,850
单一的口音本身就是一种优势，现在有很多值得一爱的地方
itself now there's a lot to love about

65
00:02:11,050 --> 00:02:12,920
专门托管我们，除了真正非常快速，非常直观之外
hosting us specifically apart from being

66
00:02:13,120 --> 00:02:15,140
专门托管我们，除了真正非常快速，非常直观之外
really really fast really intuitive the

67
00:02:15,340 --> 00:02:16,820
他们内置的诊断程序可以帮助您进行某些操作
fact that they have built-in Diagnostics

68
00:02:17,020 --> 00:02:18,770
他们内置的诊断程序可以帮助您进行某些操作
to help you along the way of something

69
00:02:18,969 --> 00:02:20,990
就像您的DNS设置不正确一样，
like your DNS isn't set up correctly

70
00:02:21,189 --> 00:02:22,910
就像您的DNS设置不正确一样，
apart from all of that they're also

71
00:02:23,110 --> 00:02:25,219
真的非常实惠，我绝对喜欢，因为网络托管
really really affordable and I

72
00:02:25,419 --> 00:02:27,560
真的非常实惠，我绝对喜欢，因为网络托管
absolutely love that because web hosting

73
00:02:27,759 --> 00:02:29,600
应该让尽可能多的人访问，即使您没有很多
should be accessible to as many people

74
00:02:29,800 --> 00:02:32,300
应该让尽可能多的人访问，即使您没有很多
as possible even if you don't have a lot

75
00:02:32,500 --> 00:02:33,920
花在这上面并托管我们的钱我真的认为他们击中了
of money to spend on this and hosting

76
00:02:34,120 --> 00:02:35,719
花在这上面并托管我们的钱我真的认为他们击中了
our I really think they hit the ball on

77
00:02:35,919 --> 00:02:38,510
那是因为它们超赞而且价格合理
that because they are super amazing and

78
00:02:38,710 --> 00:02:39,800
那是因为它们超赞而且价格合理
affordable a lot of these kind of

79
00:02:40,000 --> 00:02:42,860
便宜的网络主机似乎并没有做得那么好，但是托管我们的主机
cheaper web hosts just don't seem to do

80
00:02:43,060 --> 00:02:45,170
便宜的网络主机似乎并没有做得那么好，但是托管我们的主机
that great of a job but hosting our is

81
00:02:45,370 --> 00:02:47,780
不是一些便宜的虚拟主机托管，它只是一个非常好的虚拟主机
not some cheap web host hosting it is

82
00:02:47,979 --> 00:02:50,150
不是一些便宜的虚拟主机托管，它只是一个非常好的虚拟主机
it's just a really good web hosting

83
00:02:50,349 --> 00:02:52,040
平台，而不考虑价格，因此，如果您还没有网站，并且您
platform regardless of the price so if

84
00:02:52,240 --> 00:02:53,480
平台，而不考虑价格，因此，如果您还没有网站，并且您
you don't have a website yet and you

85
00:02:53,680 --> 00:02:54,950
想上网，或者您和其他网络托管服务商在一起，而您不喜欢
want to get online or you're with some

86
00:02:55,150 --> 00:02:56,390
想上网，或者您和其他网络托管服务商在一起，而您不喜欢
other web host and you're not enjoying

87
00:02:56,590 --> 00:02:57,830
那经历，你想成为一个好的
that experience and you want to come

88
00:02:58,030 --> 00:02:59,300
那经历，你想成为一个好的
over to a good one

89
00:02:59,500 --> 00:03:00,980
然后一定要检查托管服务，并且说明中会有一个链接
then definitely check out hosting and

90
00:03:01,180 --> 00:03:01,820
然后一定要检查托管服务，并且说明中会有一个链接
there'll be a link in the description

91
00:03:02,020 --> 00:03:05,180
如果使用此代码chenna，您最多可以节省其托管计划91％的费用
you can save up to 91% off their hosting

92
00:03:05,379 --> 00:03:07,550
如果使用此代码chenna，您最多可以节省其托管计划91％的费用
plans if you use this code code chenna

93
00:03:07,750 --> 00:03:09,170
所以绝对要检查托管服务，如果你们需要任何类型的网络托管服务
so definitely check out hosting out if

94
00:03:09,370 --> 00:03:10,310
所以绝对要检查托管服务，如果你们需要任何类型的网络托管服务
you guys need any kind of web hosting

95
00:03:10,509 --> 00:03:12,950
下面的描述中的链接好字符串字符串字符串字符串字符串
link in the description below alright

96
00:03:13,150 --> 00:03:16,160
下面的描述中的链接好字符串字符串字符串字符串字符串
strings string string strings strings

97
00:03:16,360 --> 00:03:18,439
没有弦我们会在哪里，没有弦可能不在今天
strings where would we be without

98
00:03:18,639 --> 00:03:21,740
没有弦我们会在哪里，没有弦可能不在今天
strings probably not where we are today

99
00:03:21,939 --> 00:03:23,150
这是好事还是坏事，不知道其中的原因之一
whether that's a good thing or a bad

100
00:03:23,349 --> 00:03:25,009
这是好事还是坏事，不知道其中的原因之一
thing don't know one of the reasons that

101
00:03:25,209 --> 00:03:26,689
我们不喜欢字符串是因为它们倾向于分配内存和C ++堆
we don't like strings is because they

102
00:03:26,889 --> 00:03:31,039
我们不喜欢字符串是因为它们倾向于分配内存和C ++堆
tend to allocate memory and C++ heap

103
00:03:31,239 --> 00:03:34,100
分配不好，即使我把这些东西都分组在一起
allocations bad those things are just

104
00:03:34,300 --> 00:03:36,520
分配不好，即使我把这些东西都分组在一起
grouped together so much even though I

105
00:03:36,719 --> 00:03:39,920
意味着可以说内存分配可能还不错，但我最好不要
mean arguably speaking memory allocation

106
00:03:40,120 --> 00:03:42,200
意味着可以说内存分配可能还不错，但我最好不要
maybe isn't that bad anyway I better not

107
00:03:42,400 --> 00:03:45,350
之所以说什么，是因为我们知道创建标准字符串会导致
say anything cuz so because we know that

108
00:03:45,550 --> 00:03:47,330
之所以说什么，是因为我们知道创建标准字符串会导致
creating a standard string results in a

109
00:03:47,530 --> 00:03:50,000
内存分配很多人都尝试过，并避免他们尝试
memory allocation a lot of people try

110
00:03:50,199 --> 00:03:51,980
内存分配很多人都尝试过，并避免他们尝试
and kind of avoid that they'll try and

111
00:03:52,180 --> 00:03:53,539
减少他们的字符串使用量，否则他们会尝试并提出创意
reduce their kind of string usage or

112
00:03:53,739 --> 00:03:54,530
减少他们的字符串使用量，否则他们会尝试并提出创意
they'll try and come up with creative

113
00:03:54,729 --> 00:03:57,020
关于如何使用字符串的想法，或者在最坏的情况下，它们只会招来您的指责
ideas of how to use strings or at worst

114
00:03:57,219 --> 00:03:59,300
关于如何使用字符串的想法，或者在最坏的情况下，它们只会招来您的指责
case scenario they'll just berate you

115
00:03:59,500 --> 00:04:00,800
使用字符串的原因，因为为什么您总是一直分配这么多的内存
for using strings because why you

116
00:04:01,000 --> 00:04:02,810
使用字符串的原因，因为为什么您总是一直分配这么多的内存
allocating so much memory all the time

117
00:04:03,009 --> 00:04:05,210
你不知道那不好吗，但正如我所说，我在这里告诉你
don't you know that that is bad but as I

118
00:04:05,409 --> 00:04:06,530
你不知道那不好吗，但正如我所说，我在这里告诉你
mentioned I'm here to tell you that

119
00:04:06,729 --> 00:04:08,960
北方不一定好，因为C ++中心库可能
north it's not necessarily bad because

120
00:04:09,159 --> 00:04:11,750
北方不一定好，因为C ++中心库可能
the C++ center library has maybe

121
00:04:11,949 --> 00:04:13,969
对此有些出乎意料的想法，他们通过说来制止
somewhat surprisingly thought of this

122
00:04:14,169 --> 00:04:16,098
对此有些出乎意料的想法，他们通过说来制止
and they've put a stop to it by saying

123
00:04:16,298 --> 00:04:19,250
小字符串，即长度不长的字符串
that small strings that is strings that

124
00:04:19,449 --> 00:04:21,110
小字符串，即长度不长的字符串
are not exceedingly long

125
00:04:21,310 --> 00:04:23,720
他们不需要堆分配，我可以分配一些基于
they don't need to be heap-allocated I

126
00:04:23,920 --> 00:04:26,030
他们不需要堆分配，我可以分配一些基于
can just allocate a little stack based

127
00:04:26,230 --> 00:04:28,400
没有堆分配的缓冲区，它将是我的静态字符串
buffer that is not heap-allocated and

128
00:04:28,600 --> 00:04:30,560
没有堆分配的缓冲区，它将是我的静态字符串
that is gonna be my static string

129
00:04:30,759 --> 00:04:33,500
现在存储小于特定长度的字符串
storage for Strings that are below a

130
00:04:33,699 --> 00:04:35,960
现在存储小于特定长度的字符串
certain length now what that actual

131
00:04:36,160 --> 00:04:38,000
长度是定义一个小的字符串，可以根据盈余绝对变化
length is to define a small string can

132
00:04:38,199 --> 00:04:40,100
长度是定义一个小的字符串，可以根据盈余绝对变化
absolutely vary based on the surplus

133
00:04:40,300 --> 00:04:41,990
加上您正在使用的标准库，但在Visual Studio 2019中
plus standard library that you're using

134
00:04:42,189 --> 00:04:44,990
加上您正在使用的标准库，但在Visual Studio 2019中
but here in Visual Studio 2019 that

135
00:04:45,189 --> 00:04:47,629
如果您的字符串为15个字符，则长度似乎为15个字符，或者
length seems to be 15 characters if you

136
00:04:47,829 --> 00:04:49,400
如果您的字符串为15个字符，则长度似乎为15个字符，或者
have a string that is 15 characters or

137
00:04:49,600 --> 00:04:51,829
更少，它将不会在堆上分配内存，只会使用该堆栈缓冲区
less it will not allocate memory on the

138
00:04:52,029 --> 00:04:54,020
更少，它将不会在堆上分配内存，只会使用该堆栈缓冲区
heap it will just use that stack buffer

139
00:04:54,220 --> 00:04:56,990
正如我提到的，但是如果您的字符串确实是16个字符或以上
as I mentioned however if you do have a

140
00:04:57,189 --> 00:04:59,150
正如我提到的，但是如果您的字符串确实是16个字符或以上
string that is 16 characters or above

141
00:04:59,350 --> 00:05:01,790
那会击中malloc的，你知道堆Alec以及地球上的任何函数
that's gonna hit malloc and you know

142
00:05:01,990 --> 00:05:03,710
那会击中malloc的，你知道堆Alec以及地球上的任何函数
heap Alec and whatever on earth function

143
00:05:03,910 --> 00:05:05,870
用于在您正在使用的任何平台上分配内存，换句话说，如果
is used to allocate memory on whatever

144
00:05:06,069 --> 00:05:08,150
用于在您正在使用的任何平台上分配内存，换句话说，如果
platform you're on so in other words if

145
00:05:08,350 --> 00:05:10,490
您的字符串非常小，您不必担心使用
you have a string that's quite small you

146
00:05:10,689 --> 00:05:11,629
您的字符串非常小，您不必担心使用
don't have to worry about using a

147
00:05:11,829 --> 00:05:13,220
海螺壳指针或试图进行微管理或优化此类位
conch-shell pointer or trying to

148
00:05:13,420 --> 00:05:16,220
海螺壳指针或试图进行微管理或优化此类位
micromanage or optimize that kind of bit

149
00:05:16,420 --> 00:05:19,160
的代码，因为可能不会导致堆分配
of your code because likely that's not

150
00:05:19,360 --> 00:05:20,360
的代码，因为可能不会导致堆分配
going to result in a heap allocation

151
00:05:20,560 --> 00:05:22,579
无论如何，让我们不费吹灰之力地深入Visual Studio并实际采用
anyway so without further ado let's dive

152
00:05:22,779 --> 00:05:24,650
无论如何，让我们不费吹灰之力地深入Visual Studio并实际采用
into Visual Studio and actually take a

153
00:05:24,850 --> 00:05:26,629
看看这是如何在幕后工作的，并在实际操作中看到
look at how this works behind the scenes

154
00:05:26,829 --> 00:05:29,300
看看这是如何在幕后工作的，并在实际操作中看到
and see it in action I'm just gonna make

155
00:05:29,500 --> 00:05:31,520
我要称呼它为弦乐的弦，这是一个很小的弦，只有
a string I'm gonna call it churner now

156
00:05:31,720 --> 00:05:33,949
我要称呼它为弦乐的弦，这是一个很小的弦，只有
this is a really small string it's only

157
00:05:34,149 --> 00:05:35,840
六个字符会使很多人失望，因为如果您分配此字符
six characters which would throw a lot

158
00:05:36,040 --> 00:05:37,670
六个字符会使很多人失望，因为如果您分配此字符
of people off because if you assign this

159
00:05:37,870 --> 00:05:40,129
对于标准字符串，这意味着大量的堆分配
to a standard string this is kind of

160
00:05:40,329 --> 00:05:42,710
对于标准字符串，这意味着大量的堆分配
implication of a heap allocation a lot

161
00:05:42,910 --> 00:05:44,180
的人们可能选择实际做类似Const这样的事情
of people might choose to actually do

162
00:05:44,379 --> 00:05:45,770
的人们可能选择实际做类似Const这样的事情
something like this like a Const our

163
00:05:45,970 --> 00:05:47,810
之所以命名，是因为这显然是静态字符串，这意味着
name because this is something that is

164
00:05:48,009 --> 00:05:49,490
之所以命名，是因为这显然是静态字符串，这意味着
clearly a static string meaning we're

165
00:05:49,689 --> 00:05:51,110
不会附加任何内容，我们不会从文件中获取
not gonna be appending anything to it

166
00:05:51,310 --> 00:05:53,509
不会附加任何内容，我们不会从文件中获取
we're not taking it in from a file or

167
00:05:53,709 --> 00:05:55,189
从控制台或类似的东西开始，它只是一个简单的静态字符串，因此
from like the console or anything like

168
00:05:55,389 --> 00:05:58,129
从控制台或类似的东西开始，它只是一个简单的静态字符串，因此
that it's just a simple static string so

169
00:05:58,329 --> 00:06:00,710
那为什么我要用一个标准的字符串呢？
why then would I use a standard string

170
00:06:00,910 --> 00:06:02,480
那为什么我要用一个标准的字符串呢？
if that comes with that whole heap

171
00:06:02,680 --> 00:06:04,610
分配开销很好，这是堆分配所没有的
allocation overhead well here's a thing

172
00:06:04,810 --> 00:06:06,710
分配开销很好，这是堆分配所没有的
it doesn't come with a heap allocation

173
00:06:06,910 --> 00:06:08,930
开销，因为这符合C ++中小字符串的标准，这意味着它
overhead because this fits the criteria

174
00:06:09,129 --> 00:06:12,079
开销，因为这符合C ++中小字符串的标准，这意味着它
of a small string in C++ which means it

175
00:06:12,279 --> 00:06:13,639
实际上只会存储在静态分配的缓冲区中
will in fact just be stored in a

176
00:06:13,839 --> 00:06:15,560
实际上只会存储在静态分配的缓冲区中
statically allocated buffer that does

177
00:06:15,759 --> 00:06:17,720
根本不推堆，让我们来看一下我实际上是在做什么
not push the heap at all so let's take a

178
00:06:17,920 --> 00:06:19,009
根本不推堆，让我们来看一下我实际上是在做什么
look at this in action what I'm actually

179
00:06:19,209 --> 00:06:20,000
要做的是有些不同，而不是直截了当
gonna do is something a little bit

180
00:06:20,199 --> 00:06:21,470
要做的是有些不同，而不是直截了当
different instead of just straightaway

181
00:06:21,670 --> 00:06:23,840
跳到一个实际的例子，然后也许看看你知道的
jumping to a practical example and then

182
00:06:24,040 --> 00:06:25,160
跳到一个实际的例子，然后也许看看你知道的
maybe taking a look at you know

183
00:06:25,360 --> 00:06:26,870
压倒性的运营商知道，我认为我们最终将在此视频中做为
overriding operator knew which I think

184
00:06:27,069 --> 00:06:28,579
压倒性的运营商知道，我认为我们最终将在此视频中做为
we will do eventually in this video as

185
00:06:28,779 --> 00:06:30,439
好吧，我现在要去看看字符串的定义，现在让我们来看一下
well I'm just gonna go to the definition

186
00:06:30,639 --> 00:06:32,150
好吧，我现在要去看看字符串的定义，现在让我们来看一下
of string now let's take a look at this

187
00:06:32,350 --> 00:06:34,639
我将隐藏此解决方案资源管理器，因此我们将字符串作为基本字符串，现在有一个
I'll hide this solution Explorer so we

188
00:06:34,839 --> 00:06:37,520
我将隐藏此解决方案资源管理器，因此我们将字符串作为基本字符串，现在有一个
string as a basic string now there's a

189
00:06:37,720 --> 00:06:38,660
这里我们需要了解的几件事主要是元素类型
few things that we need to know it here

190
00:06:38,860 --> 00:06:40,579
这里我们需要了解的几件事主要是元素类型
mainly the fact that the element type

191
00:06:40,779 --> 00:06:43,040
这是一个字符，所以如果我们在这里上这堂课，我们来看看
here is a char so if we go to this class

192
00:06:43,240 --> 00:06:44,449
这是一个字符，所以如果我们在这里上这堂课，我们来看看
here and we take a look at it we have

193
00:06:44,649 --> 00:06:46,400
如果我们进入构造函数，则此元素类型在这里我们知道是char
this element type here which we know is

194
00:06:46,600 --> 00:06:49,129
如果我们进入构造函数，则此元素类型在这里我们知道是char
char if we go down to the constructor

195
00:06:49,329 --> 00:06:51,079
实际上吸收了某种形式的字符缓冲区
that actually takes in some kind of

196
00:06:51,279 --> 00:06:54,170
实际上吸收了某种形式的字符缓冲区
buffer of characters which looks like

197
00:06:54,370 --> 00:06:56,509
这是什么东西，所以我们有这种Const LM指针和LM
what this thing is here so we have this

198
00:06:56,709 --> 00:06:58,610
这是什么东西，所以我们有这种Const LM指针和LM
kind of Const LM pointer and LM of

199
00:06:58,810 --> 00:07:00,528
当然，在这种情况下，Const是我们的指针，现在这些东西适用于
course is a Const our pointer in this

200
00:07:00,728 --> 00:07:02,509
当然，在这种情况下，Const是我们的指针，现在这些东西适用于
case now some of this stuff applies to

201
00:07:02,709 --> 00:07:04,129
赢得调试模式，其他人回复发布模式，我们将采取
earn the debug mode and other stuff

202
00:07:04,329 --> 00:07:05,240
赢得调试模式，其他人回复发布模式，我们将采取
replies to release mode we'll take a

203
00:07:05,439 --> 00:07:06,439
稍后再看，但是这里的主要功能是一个标志
look at that a bit later but the main

204
00:07:06,639 --> 00:07:08,900
稍后再看，但是这里的主要功能是一个标志
function here is a sign which takes in

205
00:07:09,100 --> 00:07:11,270
实际的指针是那个const char指针，实际上是单词
the actual pointer being that const char

206
00:07:11,470 --> 00:07:13,759
实际的指针是那个const char指针，实际上是单词
pointer which is in fact the word

207
00:07:13,959 --> 00:07:16,069
切尔诺在这里，然后还说明在这种情况下将是6，因为切尔诺
Cherno here and then also account which

208
00:07:16,269 --> 00:07:17,870
切尔诺在这里，然后还说明在这种情况下将是6，因为切尔诺
in this case would be 6 because Cherno

209
00:07:18,069 --> 00:07:20,180
是6个字符，如果我们进入此分配函数，可能会有点
is 6 characters if we go into this

210
00:07:20,379 --> 00:07:21,590
是6个字符，如果我们进入此分配函数，可能会有点
assign function it might be a little bit

211
00:07:21,790 --> 00:07:23,540
很难找到合适的，但在这种情况下，它将是需要
difficult to find the right one but in

212
00:07:23,740 --> 00:07:25,340
很难找到合适的，但在这种情况下，它将是需要
this case it would be the one that takes

213
00:07:25,540 --> 00:07:27,860
在该元素中，当然可以重定向到其他内容并执行一些操作
in that element that of course redirects

214
00:07:28,060 --> 00:07:29,838
在该元素中，当然可以重定向到其他内容并执行一些操作
to something else and does some some

215
00:07:30,038 --> 00:07:32,060
额外的转换内容，但是如果我们进一步深入研究，我们会找到合适的内容
extra conversion stuff but if we drill

216
00:07:32,259 --> 00:07:34,460
额外的转换内容，但是如果我们进一步深入研究，我们会找到合适的内容
down even more and we find the right one

217
00:07:34,660 --> 00:07:36,259
这里实际上只是上面的一点，您可以看到这是一个
here which is actually just a little bit

218
00:07:36,459 --> 00:07:38,088
这里实际上只是上面的一点，您可以看到这是一个
above here you can see this is the one

219
00:07:38,288 --> 00:07:40,250
它接受那个指针，然后计算它是多少个字符
that takes in that pointer and then the

220
00:07:40,449 --> 00:07:41,870
它接受那个指针，然后计算它是多少个字符
count of how many characters it is

221
00:07:42,069 --> 00:07:43,819
这里有很少的if语句，基本上就是说，如果
there's this little if statement here

222
00:07:44,019 --> 00:07:45,620
这里有很少的if语句，基本上就是说，如果
and this basically says that if the

223
00:07:45,819 --> 00:07:47,540
count表示字符串的大小小于某个特定值
count meaning the size of the string is

224
00:07:47,740 --> 00:07:50,149
count表示字符串的大小小于某个特定值
below a certain value which is this kind

225
00:07:50,348 --> 00:07:52,100
保留大小，我们将在一分钟内查看，然后实际发生了什么
of reserved size which we'll check out

226
00:07:52,300 --> 00:07:54,259
保留大小，我们将在一分钟内查看，然后实际发生了什么
in a minute then actually what happens

227
00:07:54,459 --> 00:07:56,870
我们只是得到看起来像已经存在的内存缓冲区，
is we simply get what looks like to be

228
00:07:57,069 --> 00:07:59,480
我们只是得到看起来像已经存在的内存缓冲区，
an already existing buffer of memory and

229
00:07:59,680 --> 00:08:02,569
只需将我们的角色移动到该内存缓冲区中就可以了
simply move our characters over into

230
00:08:02,769 --> 00:08:04,939
只需将我们的角色移动到该内存缓冲区中就可以了
that buffer of memory and that's it and

231
00:08:05,139 --> 00:08:06,079
那么我们只返回它，所以根本没有分配，而在这里
then we just return it so there's no

232
00:08:06,279 --> 00:08:08,930
那么我们只返回它，所以根本没有分配，而在这里
allocation at all whereas here in this

233
00:08:09,129 --> 00:08:10,819
如果它没有通过if语句测试的情况，我们实际上称之为
case if it doesn't pass this if

234
00:08:11,019 --> 00:08:12,800
如果它没有通过if语句测试的情况，我们实际上称之为
statement test we actually call this

235
00:08:13,000 --> 00:08:15,290
重新分配功能，正如您最终可以在此处看到的那样实际调用
reallocate for function which as you can

236
00:08:15,490 --> 00:08:17,870
重新分配功能，正如您最终可以在此处看到的那样实际调用
see eventually over here actually calls

237
00:08:18,069 --> 00:08:19,939
一个分配函数，而该分配函数实际上会命中新的
an allocate function and that allocate

238
00:08:20,139 --> 00:08:22,040
一个分配函数，而该分配函数实际上会命中新的
function will in fact hit the new

239
00:08:22,240 --> 00:08:24,079
运算符，并会导致堆分配，让我们回头一点
operator and will cause a heap

240
00:08:24,279 --> 00:08:25,699
运算符，并会导致堆分配，让我们回头一点
allocation let's go back a little bit

241
00:08:25,899 --> 00:08:28,670
这是我的res变量是什么，它是为字符串保留的当前存储空间
here what is this my res variable it's

242
00:08:28,870 --> 00:08:31,069
这是我的res变量是什么，它是为字符串保留的当前存储空间
the current storage reserved for string

243
00:08:31,269 --> 00:08:32,809
换句话说，就是储备量，所以任何低于或等于
so in other words it's just that reserve

244
00:08:33,009 --> 00:08:35,328
换句话说，就是储备量，所以任何低于或等于
size so anything below this or equal to

245
00:08:35,528 --> 00:08:37,189
这将构成一个小字符串，如果我们看一下它在哪里
this will constitute a small string and

246
00:08:37,389 --> 00:08:38,689
这将构成一个小字符串，如果我们看一下它在哪里
if we take a look at where this is

247
00:08:38,889 --> 00:08:41,269
实际上在这里设置，您可以看到它被设置为缓冲区大小减去1
actually set inside here you can see

248
00:08:41,469 --> 00:08:43,789
实际上在这里设置，您可以看到它被设置为缓冲区大小减去1
that it gets set to buffer size minus 1

249
00:08:43,990 --> 00:08:47,329
对我们来说缓冲区的大小是这个可怕的Val buff的I
and buffer size for us is this scary Val

250
00:08:47,529 --> 00:08:48,549
对我们来说缓冲区的大小是这个可怕的Val buff的I
buff's I

251
00:08:48,750 --> 00:08:51,639
这实际上是一个常量表达式，在这种情况下，
which is in fact a constant expression

252
00:08:51,840 --> 00:08:53,500
这实际上是一个常量表达式，在这种情况下，
and in this case it's actually going to

253
00:08:53,700 --> 00:08:56,349
是16，我们最终减去1，所以我们有15，意思是15是最大值
be 16 and we eventually subtract one so

254
00:08:56,549 --> 00:08:58,449
是16，我们最终减去1，所以我们有15，意思是15是最大值
we have 15 meaning 15 is that maximum

255
00:08:58,649 --> 00:09:00,729
组成小字符串的字符数量，
amount of characters that constitutes a

256
00:09:00,929 --> 00:09:03,159
组成小字符串的字符数量，
small string so that's kind of all the

257
00:09:03,360 --> 00:09:04,809
只需一点点代码就可以查看此处的所有工作原理，
code in just a little bit kind of

258
00:09:05,009 --> 00:09:06,609
只需一点点代码就可以查看此处的所有工作原理，
looking at how everything works here and

259
00:09:06,809 --> 00:09:08,349
如果我回到那个赋值函数中的任何一个，那么你可以看到
if I go back to that assign function

260
00:09:08,549 --> 00:09:10,539
如果我回到那个赋值函数中的任何一个，那么你可以看到
whichever one it is then you can see

261
00:09:10,740 --> 00:09:11,859
该if语句的存在实际上不会导致或堆积
that the presence of this if statement

262
00:09:12,059 --> 00:09:14,289
该if语句的存在实际上不会导致或堆积
does in fact result in nor heap

263
00:09:14,490 --> 00:09:16,959
分配一个不超过15个字符的字符串，我们可以
allocations for a string that is 15

264
00:09:17,159 --> 00:09:19,120
分配一个不超过15个字符的字符串，我们可以
characters or smaller and we can of

265
00:09:19,320 --> 00:09:20,799
当然要测试一下，所以我想做的就是编写这个小运算符
course test this out so what I like

266
00:09:21,000 --> 00:09:22,509
当然要测试一下，所以我想做的就是编写这个小运算符
doing is writing this little operator

267
00:09:22,710 --> 00:09:25,149
新的，这里我们将采用此处的大小，这只是返回一个
new here we'll just take in the size

268
00:09:25,350 --> 00:09:26,709
新的，这里我们将采用此处的大小，这只是返回一个
here this will just simply return a

269
00:09:26,909 --> 00:09:29,139
大小合适的malloc在这里的好处是我们实际上可以放
malloc with the right size here the

270
00:09:29,340 --> 00:09:30,429
大小合适的malloc在这里的好处是我们实际上可以放
benefit here is that we can actually put

271
00:09:30,629 --> 00:09:32,229
一个断点，我还将在此处添加一些自定义代码以实际打印此代码
a breakpoint and I'll also add some

272
00:09:32,429 --> 00:09:34,329
一个断点，我还将在此处添加一些自定义代码以实际打印此代码
custom code here to actually print this

273
00:09:34,529 --> 00:09:37,059
所以我们只说分配，然后是大小，然后是字节，是的
so we'll just say allocating and then

274
00:09:37,259 --> 00:09:39,939
所以我们只说分配，然后是大小，然后是字节，是的
size and then bytes yeah

275
00:09:40,139 --> 00:09:41,889
好吧，让我们不必为此担心，我们将C放入get中，以便如果我们不单击
okay so let's not worry about this let's

276
00:09:42,090 --> 00:09:46,569
好吧，让我们不必为此担心，我们将C放入get中，以便如果我们不单击
put a C in get so that if we don't click

277
00:09:46,769 --> 00:09:47,889
关闭我们的控制台，然后让我们启动这个好吧，所以检查一下我们
close our console and then let's launch

278
00:09:48,090 --> 00:09:49,750
关闭我们的控制台，然后让我们启动这个好吧，所以检查一下我们
this alright so check this out we

279
00:09:49,950 --> 00:09:51,819
实际上分配了8个字节，即使我们的字符串明显低于15
actually allocate 8 bytes even though

280
00:09:52,019 --> 00:09:53,589
实际上分配了8个字节，即使我们的字符串明显低于15
our string clearly is below 15

281
00:09:53,789 --> 00:09:55,299
视觉地理字符串类的另一个小怪癖
characters that's another little quirk

282
00:09:55,500 --> 00:09:56,829
视觉地理字符串类的另一个小怪癖
with visual geo string class it's

283
00:09:57,029 --> 00:09:58,419
基本上只有在调试模式下才会发生，但是如果我们切换到发布
basically something that only happens in

284
00:09:58,620 --> 00:10:00,309
基本上只有在调试模式下才会发生，但是如果我们切换到发布
debug mode but if we switch to release

285
00:10:00,509 --> 00:10:02,379
模式，然后启动它，那么您将看到我们根本没有Noor分配，但是如果我
mode and we launch this then you'll see

286
00:10:02,580 --> 00:10:05,289
模式，然后启动它，那么您将看到我们根本没有Noor分配，但是如果我
we have Noor allocations at all but if I

287
00:10:05,490 --> 00:10:07,029
展开并使其比这6个字符略多一点，以便7 8 9 10
expand this and make this a little bit

288
00:10:07,230 --> 00:10:10,509
展开并使其比这6个字符略多一点，以便7 8 9 10
more than these 6 characters so 7 8 9 10

289
00:10:10,710 --> 00:10:16,269
11 12 13 14 15所以正好是15个字符让我们看一下
11 12 13 14 15 so that's exactly 15

290
00:10:16,470 --> 00:10:17,709
11 12 13 14 15所以正好是15个字符让我们看一下
characters let's take a look at that

291
00:10:17,909 --> 00:10:20,409
仍然没有任何分配，但是当我输入16个字符时，您可以
still nothing no allocations but the

292
00:10:20,610 --> 00:10:23,620
仍然没有任何分配，但是当我输入16个字符时，您可以
moment I go into 16 characters you can

293
00:10:23,820 --> 00:10:25,329
看到我们分配了32个字节，因此它立即捕捉到此预定义
see we allocate 32 bytes so it

294
00:10:25,529 --> 00:10:27,069
看到我们分配了32个字节，因此它立即捕捉到此预定义
immediately snaps up to this predefined

295
00:10:27,269 --> 00:10:29,469
这里的值只有一个字符少，我们绝对没有堆分配
value here with just one character less

296
00:10:29,669 --> 00:10:31,389
这里的值只有一个字符少，我们绝对没有堆分配
we have absolutely no heap allocations

297
00:10:31,589 --> 00:10:33,939
但是一旦我们将字符数限制为16个或更多，我们就会在堆上分配内存，
but once we tip over to 16 characters or

298
00:10:34,139 --> 00:10:36,909
但是一旦我们将字符数限制为16个或更多，我们就会在堆上分配内存，
more we allocate memory on the heap and

299
00:10:37,110 --> 00:10:38,740
如果您担心的话，这基本上就是本视频的全部内容
that's basically all there is to say for

300
00:10:38,940 --> 00:10:40,329
如果您担心的话，这基本上就是本视频的全部内容
this video if if you're in care about

301
00:10:40,529 --> 00:10:41,709
现在您几乎可以停止观看任何更多的详细信息，只需知道在C ++中
any more details you can pretty much

302
00:10:41,909 --> 00:10:44,169
现在您几乎可以停止观看任何更多的详细信息，只需知道在C ++中
stop watching now just know that in C++

303
00:10:44,370 --> 00:10:45,669
特别是在我实现的C ++标准库的实现中
and specifically in this implementation

304
00:10:45,870 --> 00:10:48,309
特别是在我实现的C ++标准库的实现中
of the C++ standard library which I'm

305
00:10:48,509 --> 00:10:50,740
在此处使用Visual Studio 2019的15个字符以内的任何字符串都不会
using Visual Studio 2019 here any string

306
00:10:50,940 --> 00:10:53,049
在此处使用Visual Studio 2019的15个字符以内的任何字符串都不会
that is 15 characters or less does not

307
00:10:53,250 --> 00:10:54,759
导致堆分配，从某种意义上说，这是一种优化，它的作用更大
cause a heap allocation and in that

308
00:10:54,960 --> 00:10:57,279
导致堆分配，从某种意义上说，这是一种优化，它的作用更大
sense it's an optimization it's more

309
00:10:57,480 --> 00:10:59,019
高效，它更快，现在让我们快速了解一下到底发生了什么
efficient it's faster now let's quickly

310
00:10:59,220 --> 00:11:00,789
高效，它更快，现在让我们快速了解一下到底发生了什么
take a look at what on earth happens in

311
00:11:00,990 --> 00:11:01,818
导致分配的调试模式，我们将放置一个断点
debug mode that caused

312
00:11:02,019 --> 00:11:03,438
导致分配的调试模式，我们将放置一个断点
an allocation we'll put a breakpoint

313
00:11:03,639 --> 00:11:05,659
在这里，我还将带回切尔诺，我们会检查一下
here and I'll also take this back to

314
00:11:05,860 --> 00:11:07,428
在这里，我还将带回切尔诺，我们会检查一下
Cherno and we'll check it out all right

315
00:11:07,629 --> 00:11:08,808
因此，此分配来自外部代码，让我们仅展示该外部代码
so this allocation comes from external

316
00:11:09,009 --> 00:11:10,788
因此，此分配来自外部代码，让我们仅展示该外部代码
code let's just show that external code

317
00:11:10,989 --> 00:11:13,399
让我们走到这里这可能会向我们展示我们想要的
and let's go up to here which is

318
00:11:13,600 --> 00:11:14,839
让我们走到这里这可能会向我们展示我们想要的
probably going to show us what we want

319
00:11:15,039 --> 00:11:16,639
看看我会在这里留出更多空间，其中包含一个代理指针
to see I'll just make some more room

320
00:11:16,839 --> 00:11:19,428
看看我会在这里留出更多空间，其中包含一个代理指针
here there it is contain a proxy pointer

321
00:11:19,629 --> 00:11:21,709
所以这就是导致分配的原因，现在您可以使用此容器代理指针
so this is what causes the allocation

322
00:11:21,909 --> 00:11:24,409
所以这就是导致分配的原因，现在您可以使用此容器代理指针
now this container proxy pointer you can

323
00:11:24,610 --> 00:11:27,168
看到等于此重新绑定的Alec T情况，它实际上是模板
see is equal to this rebind Alec T

324
00:11:27,369 --> 00:11:28,848
看到等于此重新绑定的Alec T情况，它实际上是模板
situation which is actually a template

325
00:11:29,048 --> 00:11:31,368
这个课程的论点，或者我什至不知道这是什么，所以
argument for this class or I don't even

326
00:11:31,568 --> 00:11:33,078
这个课程的论点，或者我什至不知道这是什么，所以
know what this is to be honest it's so

327
00:11:33,278 --> 00:11:34,818
复杂但基本上整个事情的重点是您可以看到
complicated but basically the point of

328
00:11:35,019 --> 00:11:36,288
复杂但基本上整个事情的重点是您可以看到
this whole thing is that you can see

329
00:11:36,489 --> 00:11:37,848
仅在迭代器调试级别不为零的情况下，这是因为
that this is only the case if the

330
00:11:38,048 --> 00:11:39,889
仅在迭代器调试级别不为零的情况下，这是因为
iterator debug level is not zero because

331
00:11:40,089 --> 00:11:42,378
在这种情况下，如果调试级别为
in that sense this part of the code gets

332
00:11:42,578 --> 00:11:44,568
在这种情况下，如果调试级别为
compiled however if the debug level is

333
00:11:44,769 --> 00:11:46,248
零，这是在发布模式下会看到的
zero which is what it would be in

334
00:11:46,448 --> 00:11:47,598
零，这是在发布模式下会看到的
release mode you can see that instead

335
00:11:47,798 --> 00:11:50,298
这个获取代理分配器是一个伪分配器，这个伪分配器可以
this get proxy allocator is a fake

336
00:11:50,499 --> 00:11:52,279
这个获取代理分配器是一个伪分配器，这个伪分配器可以
allocator and this fake allocator does

337
00:11:52,480 --> 00:11:53,808
绝对没有，所以这个容器代理12
absolutely nothing

338
00:11:54,009 --> 00:11:55,578
绝对没有，所以这个容器代理12
so this container proxy 12 which

339
00:11:55,778 --> 00:11:57,678
最终实际上导致了新分配的发生，因此事实证明
eventually actually causes that new

340
00:11:57,879 --> 00:11:59,928
最终实际上导致了新分配的发生，因此事实证明
allocation to take place so it turns out

341
00:12:00,129 --> 00:12:00,978
从某种意义上来说，这只是一点调试而已
that it's a little bit of a red herring

342
00:12:01,178 --> 00:12:03,258
从某种意义上来说，这只是一点调试而已
in the sense that it's a debug only

343
00:12:03,458 --> 00:12:05,478
分配，实际上不是小字符串优化无法正常工作
allocation and it's not in fact small

344
00:12:05,678 --> 00:12:07,308
分配，实际上不是小字符串优化无法正常工作
string optimization not working it's

345
00:12:07,509 --> 00:12:09,678
所有调试字符串都是这样，这是一个小的字符串优化
just the case for all debug strings okay

346
00:12:09,879 --> 00:12:12,438
所有调试字符串都是这样，这是一个小的字符串优化
so that's a small string optimization

347
00:12:12,639 --> 00:12:15,108
足够小的字符串不会导致任何堆分配，因此
strings that are sufficiently small will

348
00:12:15,308 --> 00:12:17,178
足够小的字符串不会导致任何堆分配，因此
not cause any heap allocations and thus

349
00:12:17,379 --> 00:12:18,618
会导致您的程序运行更快，这是一个可爱的小技巧
will result in your program running

350
00:12:18,818 --> 00:12:20,328
会导致您的程序运行更快，这是一个可爱的小技巧
faster it's a cute little trick they

351
00:12:20,528 --> 00:12:22,308
进入C ++标准库，使其变得更快一点
worked into the C++ standard library to

352
00:12:22,509 --> 00:12:24,649
进入C ++标准库，使其变得更快一点
make it that little bit faster how

353
00:12:24,850 --> 00:12:25,308
反正周到，希望你们喜欢这个视频
thoughtful

354
00:12:25,509 --> 00:12:26,389
反正周到，希望你们喜欢这个视频
anyway hope you guys enjoyed this video

355
00:12:26,589 --> 00:12:27,649
如果您这样做了，请点击“赞”按钮，然后在下方添加评论
if you did please hit that like button

356
00:12:27,850 --> 00:12:29,598
如果您这样做了，请点击“赞”按钮，然后在下方添加评论
drop a comment below with what you would

357
00:12:29,798 --> 00:12:31,639
想要看到下一个，我将要提供大量类似的数据
like to see next I'm about to come out

358
00:12:31,839 --> 00:12:33,048
想要看到下一个，我将要提供大量类似的数据
with a whole bunch of like data

359
00:12:33,249 --> 00:12:34,938
结构和模式以及所有这类的东西视频，这就是我
structures and patterns and all of that

360
00:12:35,139 --> 00:12:36,318
结构和模式以及所有这类的东西视频，这就是我
kind of stuff videos that's what I'm

361
00:12:36,519 --> 00:12:38,928
C ++系列或多或少地吸引人，别忘了
kind of more or less gravitating towards

362
00:12:39,129 --> 00:12:41,358
C ++系列或多或少地吸引人，别忘了
with the C++ series don't forget to

363
00:12:41,558 --> 00:12:43,039
签出托管高达91％的虚拟主机，我会看到你们
check out hosting up up to 91 percent

364
00:12:43,240 --> 00:12:45,168
签出托管高达91％的虚拟主机，我会看到你们
off web hosting and I will see you guys

365
00:12:45,369 --> 00:12:47,870
下次再见[音乐]
next time goodbye

366
00:12:48,070 --> 00:12:53,070
下次再见[音乐]
[Music]

