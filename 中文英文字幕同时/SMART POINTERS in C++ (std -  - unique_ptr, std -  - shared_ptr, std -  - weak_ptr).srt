﻿1
00:00:00,000 --> 00:00:01,389
嗨，大家好，我叫频道欢迎回到州老板
hey what's up guys my name is a channel

2
00:00:01,589 --> 00:00:02,619
嗨，大家好，我叫频道欢迎回到州老板
welcome back to my state boss last

3
00:00:02,819 --> 00:00:03,910
今天的系列文章我们将讨论C ++中的智能指针
series today we're gonna be talking all

4
00:00:04,110 --> 00:00:06,219
今天的系列文章我们将讨论C ++中的智能指针
about smart pointers in C++

5
00:00:06,419 --> 00:00:07,540
因此，由于我的指针是我的视频中最近出现的一个话题，
so as my pointers are a topic that has

6
00:00:07,740 --> 00:00:10,210
因此，由于我的指针是我的视频中最近出现的一个话题，
come up a lot recently in my videos and

7
00:00:10,410 --> 00:00:11,919
人们要求这样做，什么是智能指针？
people requesting this and what is a

8
00:00:12,119 --> 00:00:13,390
人们要求这样做，什么是智能指针？
smart pointer should I be using smart

9
00:00:13,589 --> 00:00:15,430
指针和今天所有的一切，我们只是讨论它们是什么，我们不是真的
pointers and all that today we just goes

10
00:00:15,630 --> 00:00:16,810
指针和今天所有的一切，我们只是讨论它们是什么，我们不是真的
over what they are we're not really

11
00:00:17,010 --> 00:00:18,490
深入了解为什么我认为您应该或不应该使用
going to get into depth about what why I

12
00:00:18,690 --> 00:00:20,320
深入了解为什么我认为您应该或不应该使用
think you should or shouldn't be using

13
00:00:20,519 --> 00:00:22,030
他们以及所有这些东西以及我对智能指针的一般看法
them and all that stuff and my opinions

14
00:00:22,230 --> 00:00:23,980
他们以及所有这些东西以及我对智能指针的一般看法
about smart pointers in general I'm

15
00:00:24,179 --> 00:00:25,359
今天将其保存为另一个视频，我们将专注于
gonna save that for another video today

16
00:00:25,559 --> 00:00:26,890
今天将其保存为另一个视频，我们将专注于
we're just gonna focus on what a smart

17
00:00:27,089 --> 00:00:28,810
指针是什么，它可以为您做什么？
pointer is and what it can do for you so

18
00:00:29,010 --> 00:00:29,949
指针是什么，它可以为您做什么？
earlier we talked about what new and

19
00:00:30,149 --> 00:00:31,659
delete确实会在堆上分配新的内存，并且需要删除才能删除该内存
delete does new allocates memory on the

20
00:00:31,859 --> 00:00:33,429
delete确实会在堆上分配新的内存，并且需要删除才能删除该内存
heap and delete is needed to delete that

21
00:00:33,630 --> 00:00:34,869
内存以释放该内存，因为它不会自动释放
memory to free that memory because it

22
00:00:35,070 --> 00:00:36,579
内存以释放该内存，因为它不会自动释放
won't be freed automatically smart

23
00:00:36,780 --> 00:00:38,858
指针是使该过程自动化的一种方法，这就是他们所有的明智之举
pointers are a way to automate that

24
00:00:39,058 --> 00:00:41,559
指针是使该过程自动化的一种方法，这就是他们所有的明智之举
process that's all they are right smart

25
00:00:41,759 --> 00:00:44,288
指针意味着当您调用new时，您不必调用delete，实际上
pointers mean that when you call new you

26
00:00:44,488 --> 00:00:46,448
指针意味着当您调用new时，您不必调用delete，实际上
don't have to call delete and in fact in

27
00:00:46,649 --> 00:00:47,829
很多情况下，有了智能指针，我们甚至不必调用新指针，所以很多人
many cases with smart pointers we don't

28
00:00:48,030 --> 00:00:50,378
很多情况下，有了智能指针，我们甚至不必调用新指针，所以很多人
even have to call new so a lot of people

29
00:00:50,579 --> 00:00:52,538
倾向于具有这种编程风格并且尽可能安全，他们从不
tend to have this kind of programming

30
00:00:52,738 --> 00:00:54,070
倾向于具有这种编程风格并且尽可能安全，他们从不
style and safe as possible they never

31
00:00:54,270 --> 00:00:56,649
曾经调用过new或delete，而智能指针是使其最佳的方式
ever call new or delete and smart

32
00:00:56,850 --> 00:00:58,838
曾经调用过new或delete，而智能指针是使其最佳的方式
pointers are best their way to make that

33
00:00:59,039 --> 00:01:00,549
发生，所以智能指针本质上是真正的原始指针的包装，当
happen so smart pointers are essentially

34
00:01:00,750 --> 00:01:03,489
发生，所以智能指针本质上是真正的原始指针的包装，当
a wrapper around a real raw pointer when

35
00:01:03,689 --> 00:01:05,230
您创建一个智能指针，并将其命名为new并分配您的
you create a smart pointer and you make

36
00:01:05,430 --> 00:01:07,329
您创建一个智能指针，并将其命名为new并分配您的
it it will call new and allocate your

37
00:01:07,530 --> 00:01:09,459
给您的内存，然后基于您使用该内存的指针
memory for you and then based on which

38
00:01:09,659 --> 00:01:11,890
给您的内存，然后基于您使用该内存的指针
my pointer you use that memory will at

39
00:01:12,090 --> 00:01:13,719
有些地方会自动免费，所以让我们来看看第一种
some point be automatically free so

40
00:01:13,920 --> 00:01:15,069
有些地方会自动免费，所以让我们来看看第一种
let's take a look at the first kind of

41
00:01:15,269 --> 00:01:16,509
最简单的智能边框，我们具有唯一的指针，所以唯一的指针是
an simplest smart border that we have

42
00:01:16,709 --> 00:01:18,668
最简单的智能边框，我们具有唯一的指针，所以唯一的指针是
unique pointers so a unique pointer is a

43
00:01:18,868 --> 00:01:20,769
脚本指针意味着当该指针超出范围时它将
script pointer meaning that when that

44
00:01:20,969 --> 00:01:23,590
脚本指针意味着当该指针超出范围时它将
pointer goes out of scope it will it

45
00:01:23,790 --> 00:01:24,879
将被销毁，它将称为删除，我们实际上谈到了
will get destroyed and it will call

46
00:01:25,079 --> 00:01:26,230
将被销毁，它将称为删除，我们实际上谈到了
delete we actually talked about how

47
00:01:26,430 --> 00:01:28,119
对象生命周期的工作原理以及如何利用堆栈分配的力量
object lifetimes work and how you can

48
00:01:28,319 --> 00:01:30,250
对象生命周期的工作原理以及如何利用堆栈分配的力量
leverage the power of stack allocation

49
00:01:30,450 --> 00:01:31,778
上一个视频中的所有内容，所以一定要检查一下是否还没有
all that in the last video so definitely

50
00:01:31,978 --> 00:01:32,738
上一个视频中的所有内容，所以一定要检查一下是否还没有
check that out if you haven't already

51
00:01:32,938 --> 00:01:34,778
但这基本上是我们的脚本指向的工作或唯一的指针，没有
but that is basically our script points

52
00:01:34,978 --> 00:01:36,579
但这基本上是我们的脚本指向的工作或唯一的指针，没有
work or unique pointers there isn't

53
00:01:36,780 --> 00:01:37,569
它们被称为唯一指针，因为它们必须是唯一的，你不能
they're called unique pointers it

54
00:01:37,769 --> 00:01:39,399
它们被称为唯一指针，因为它们必须是唯一的，你不能
because they have to be unique you can't

55
00:01:39,599 --> 00:01:41,679
复制一个唯一的指针，因为如果您复制一个唯一的指针
copy a unique pointer because if you

56
00:01:41,879 --> 00:01:43,929
复制一个唯一的指针，因为如果您复制一个唯一的指针
copy a unique pointer the memory that

57
00:01:44,129 --> 00:01:45,278
它指向他们基本上你将有两个指针，两个唯一
it's pointing to they'll basically

58
00:01:45,478 --> 00:01:46,840
它指向他们基本上你将有两个指针，两个唯一
you'll have two pointers two unique

59
00:01:47,040 --> 00:01:48,099
指向相同内存块的指针，当其中一个指针死亡时
pointers pointing to the same block of

60
00:01:48,299 --> 00:01:50,079
指向相同内存块的指针，当其中一个指针死亡时
memory and when one of them dies it will

61
00:01:50,280 --> 00:01:52,179
释放内存，这意味着您突然有了第二个唯一指针
free that memory meaning that suddenly

62
00:01:52,379 --> 00:01:53,649
释放内存，这意味着您突然有了第二个唯一指针
that second unique pointer you had

63
00:01:53,849 --> 00:01:55,119
指向同一本内存的书指向已释放的内存，因此
pointed to the same book of memory is

64
00:01:55,319 --> 00:01:57,219
指向同一本内存的书指向已释放的内存，因此
pointing to memory that's been freed so

65
00:01:57,420 --> 00:01:59,230
您无法复制唯一的指针，您需要的指针是您想要滑擦时需要的
you cannot copy unique pointers you need

66
00:01:59,430 --> 00:02:00,730
您无法复制唯一的指针，您需要的指针是您想要滑擦时需要的
pointers are for when you want a skrub

67
00:02:00,930 --> 00:02:03,159
指针，这是您实际上想要的对该指针的唯一引用，让我们
pointer and that is the only reference

68
00:02:03,359 --> 00:02:04,869
指针，这是您实际上想要的对该指针的唯一引用，让我们
to that pointer you actually want let's

69
00:02:05,069 --> 00:02:05,759
看一个例子，一个唯一的指针，所以第一件事
take a look at an example

70
00:02:05,959 --> 00:02:07,140
看一个例子，一个唯一的指针，所以第一件事
a unique pointer so the first thing

71
00:02:07,340 --> 00:02:08,368
您需要做的是访问所有这些智能指针，包括存储
you'll need to do to get access to all

72
00:02:08,568 --> 00:02:10,650
您需要做的是访问所有这些智能指针，包括存储
these smart pointers is include memory

73
00:02:10,849 --> 00:02:12,750
现在我们在这里有这个实体类，或者
now we have this entity class here or

74
00:02:12,949 --> 00:02:14,040
现在我们在这里有这个实体类，或者
what is is the constructor on a

75
00:02:14,240 --> 00:02:15,900
当我们创建实体R时（当我们销毁实体时）使用print的析构函数
destructor with print when we create the

76
00:02:16,099 --> 00:02:17,460
当我们创建实体R时（当我们销毁实体时）使用print的析构函数
entity R when we destroy the entity just

77
00:02:17,659 --> 00:02:18,600
这样我们就可以研究这些智能指针的行为
so that we can kind of look into the

78
00:02:18,800 --> 00:02:20,640
这样我们就可以研究这些智能指针的行为
behavior of these smart pointers so over

79
00:02:20,840 --> 00:02:21,930
在这里主要如果我想创建一个持续一定的唯一指针
here in main if I want to create a

80
00:02:22,129 --> 00:02:23,880
在这里主要如果我想创建一个持续一定的唯一指针
unique pointer which lasts in a certain

81
00:02:24,080 --> 00:02:25,618
作用域，所以我为您创建了一个新作用域，您将在其中进入一个空作用域
scope so I've made a new scope you an

82
00:02:25,818 --> 00:02:27,539
作用域，所以我为您创建了一个新作用域，您将在其中进入一个空作用域
empty scope inside here I'm going to

83
00:02:27,739 --> 00:02:29,490
使用唯一指针分配我的实体的方式是
allocate my entity using a unique

84
00:02:29,689 --> 00:02:31,080
使用唯一指针分配我的实体的方式是
pointer the way I'll do that is I'll

85
00:02:31,280 --> 00:02:34,259
输入SV唯一指针，我将为其提供实体的模板参数，然后
type in SV unique pointer I'll give it a

86
00:02:34,459 --> 00:02:36,810
输入SV唯一指针，我将为其提供实体的模板参数，然后
template argument of entity and then

87
00:02:37,009 --> 00:02:38,550
我给它起一个名字，例如entity，然后我有两个选择
I'll give it a name such as entity and

88
00:02:38,750 --> 00:02:39,810
我给它起一个名字，例如entity，然后我有两个选择
then I've kind of got two options I

89
00:02:40,009 --> 00:02:41,039
可以在这里调用构造函数，然后输入新的实体，例如此注释
could either call the constructor here

90
00:02:41,239 --> 00:02:43,950
可以在这里调用构造函数，然后输入新的实体，例如此注释
and type in new entity like this note

91
00:02:44,150 --> 00:02:45,660
您实际上将无法进行这种构造，因为如果您
that you actually won't be able to do

92
00:02:45,860 --> 00:02:49,259
您实际上将无法进行这种构造，因为如果您
this kind of construction because if you

93
00:02:49,459 --> 00:02:51,090
看一下唯一指针，构造函数实际上是显式的，意味着
look at unique pointer the constructor

94
00:02:51,289 --> 00:02:53,069
看一下唯一指针，构造函数实际上是显式的，意味着
is actually explicit meaning that you do

95
00:02:53,269 --> 00:02:54,689
必须显式调用构造函数，没有隐式的转换
have to call the constructor explicitly

96
00:02:54,889 --> 00:02:56,490
必须显式调用构造函数，没有隐式的转换
there's no implicit kind of conversion

97
00:02:56,689 --> 00:02:58,380
或转换构造函数，这是提出独特观点的一种方法，然后您
or converting constructor so that's one

98
00:02:58,580 --> 00:02:59,640
或转换构造函数，这是提出独特观点的一种方法，然后您
way to make unique point and then you

99
00:02:59,840 --> 00:03:01,230
如果我想打电话给我，可以像访问其他任何东西一样访问它
can kind of access it like you would

100
00:03:01,430 --> 00:03:03,150
如果我想打电话给我，可以像访问其他任何东西一样访问它
anything else if I wanted to call a

101
00:03:03,349 --> 00:03:04,500
这里的功能我们什至没有任何功能，但是如果我要调用一个
function here we don't even have any

102
00:03:04,699 --> 00:03:05,969
这里的功能我们什至没有任何功能，但是如果我要调用一个
functions but if I were to call a

103
00:03:06,169 --> 00:03:07,860
函数在这里，我只是通过箭头运算符访问它，
function here I would just access it

104
00:03:08,060 --> 00:03:09,090
函数在这里，我只是通过箭头运算符访问它，
through the arrow operator and

105
00:03:09,289 --> 00:03:10,890
一切都将完全相同，就好像这只是一个滚动指针一样
everything would be exactly the same as

106
00:03:11,090 --> 00:03:12,300
一切都将完全相同，就好像这只是一个滚动指针一样
if this was just a roll pointer the

107
00:03:12,500 --> 00:03:13,920
尽管构建此方法的首选方法实际上是将其分配给STD
preferred way though to construct this

108
00:03:14,120 --> 00:03:15,990
尽管构建此方法的首选方法实际上是将其分配给STD
would actually be to assign it to STD

109
00:03:16,189 --> 00:03:18,750
在那里使该实体具有独特性的主要原因是
make unique with that entity there the

110
00:03:18,949 --> 00:03:20,310
在那里使该实体具有独特性的主要原因是
primary reason that that's important for

111
00:03:20,509 --> 00:03:21,900
唯一指针实际上是由于异常安全性而引起的，我们将在后面讨论
unique pointers is actually due to

112
00:03:22,099 --> 00:03:23,430
唯一指针实际上是由于异常安全性而引起的，我们将在后面讨论
exception safety we'll talk about

113
00:03:23,629 --> 00:03:25,560
在本系列中的某些时候，我完全不喜欢例外，所以
exceptions at some point in this series

114
00:03:25,759 --> 00:03:28,080
在本系列中的某些时候，我完全不喜欢例外，所以
I don't like exceptions at all so that's

115
00:03:28,280 --> 00:03:29,370
每当发生时都会成为一个有趣的情节，但无论如何还是首选
gonna be an interesting episode whenever

116
00:03:29,569 --> 00:03:31,560
每当发生时都会成为一个有趣的情节，但无论如何还是首选
that happens but anyway the preferred

117
00:03:31,759 --> 00:03:33,480
实现此目的的方法是调用make unique，因为如果
way to make this is to call make unique

118
00:03:33,680 --> 00:03:36,120
实现此目的的方法是调用make unique，因为如果
because it is slightly safer if if the

119
00:03:36,319 --> 00:03:37,259
构造函数碰巧抛出一个异常，而您最终并没有
constructor happens to throw an

120
00:03:37,459 --> 00:03:39,509
构造函数碰巧抛出一个异常，而您最终并没有
exception you weren't end up having a

121
00:03:39,709 --> 00:03:41,340
没有引用的悬空指针，无论如何，这是内存泄漏
dangling pointer with no reference and

122
00:03:41,539 --> 00:03:43,590
没有引用的悬空指针，无论如何，这是内存泄漏
that's a memory leak anyway the idea is

123
00:03:43,789 --> 00:03:45,420
一旦我们创建了这个唯一的指针，我们就可以调用我们想要的任何方法，
that once we make this unique pointer we

124
00:03:45,620 --> 00:03:46,710
一旦我们创建了这个唯一的指针，我们就可以调用我们想要的任何方法，
can call whatever method we want and

125
00:03:46,909 --> 00:03:48,689
您会看到，如果我按f5键运行程序，则会在此处创建实体，
you'll see that if I hit f5 to run my

126
00:03:48,889 --> 00:03:50,969
您会看到，如果我按f5键运行程序，则会在此处创建实体，
program our entity gets created here and

127
00:03:51,169 --> 00:03:52,680
那么如果我按f10离开这个范围，我们的实体现在就被摧毁了
then if I hit f10 to get out of this

128
00:03:52,879 --> 00:03:54,750
那么如果我按f10离开这个范围，我们的实体现在就被摧毁了
scope our entity is destroyed now

129
00:03:54,949 --> 00:03:56,310
好的，所以当此代码结束时，我们的实体会自动被销毁
okay so automatically when this code

130
00:03:56,509 --> 00:03:58,349
好的，所以当此代码结束时，我们的实体会自动被销毁
ends our entity gets destroyed that's

131
00:03:58,549 --> 00:03:59,700
我拥有的最简单的指针非常有用，它的位置很低
the simplest my pointer that we have

132
00:03:59,900 --> 00:04:01,140
我拥有的最简单的指针非常有用，它的位置很低
it's very useful it's got a very low

133
00:04:01,340 --> 00:04:03,210
它实际上并没有开销，只是分配了一个堆栈
overhead it doesn't really even have an

134
00:04:03,409 --> 00:04:05,280
它实际上并没有开销，只是分配了一个堆栈
overhead it's just a stack allocated

135
00:04:05,479 --> 00:04:07,020
对象，当该堆栈分配对象的眼睛时，它将调用
object and when when that stack

136
00:04:07,219 --> 00:04:08,670
对象，当该堆栈分配对象的眼睛时，它将调用
allocated object eyes it will call

137
00:04:08,870 --> 00:04:10,650
删除您的指针并释放该内存，这是我的问题
delete on your pointer and free that

138
00:04:10,849 --> 00:04:12,240
删除您的指针并释放该内存，这是我的问题
memory the problem with this is as I

139
00:04:12,439 --> 00:04:12,510
意思是如果你想在那个时候复制
meant

140
00:04:12,710 --> 00:04:14,400
意思是如果你想在那个时候复制
if you want to copy that at that point

141
00:04:14,599 --> 00:04:15,780
如果您想分享这一点并将其传递给函数，或者
if you want to kind of share that point

142
00:04:15,979 --> 00:04:17,639
如果您想分享这一点并将其传递给函数，或者
and maybe pass it into a function or

143
00:04:17,839 --> 00:04:20,189
还有另一个课堂故事，你会遇到问题，因为你不能
have another class story you're gonna

144
00:04:20,389 --> 00:04:22,020
还有另一个课堂故事，你会遇到问题，因为你不能
run into a problem because you can't

145
00:04:22,220 --> 00:04:23,610
复制它，如果您要尝试制作另一个独特的东西，请看一下
copy it and if you take a look at this

146
00:04:23,810 --> 00:04:26,610
复制它，如果您要尝试制作另一个独特的东西，请看一下
if I was to try and make another unique

147
00:04:26,810 --> 00:04:29,250
此处的指针称为“简单行”或类似的符号，并将其分配给
pointer here called easy row or

148
00:04:29,449 --> 00:04:30,240
此处的指针称为“简单行”或类似的符号，并将其分配给
something like that and assign it to

149
00:04:30,439 --> 00:04:32,340
实体，我实际上做不到，您会在这里收到一条错误消息
entity I actually can't do that and

150
00:04:32,540 --> 00:04:33,569
实体，我实际上做不到，您会在这里收到一条错误消息
you'll get kind of an error message here

151
00:04:33,769 --> 00:04:35,220
如果您转到这个独特的定义点，而您看起来有点奇怪
which looks a bit weird if you go to

152
00:04:35,420 --> 00:04:36,780
如果您转到这个独特的定义点，而您看起来有点奇怪
this unique point of definition and you

153
00:04:36,980 --> 00:04:38,699
实际向下滚动一点，您会看到复制构造函数和复制
actually scroll down a bit you'll see

154
00:04:38,899 --> 00:04:40,170
实际向下滚动一点，您会看到复制构造函数和复制
that the copy constructor and the copy

155
00:04:40,370 --> 00:04:42,210
赋值运算符实际上已删除，这就是为什么如果
assignment operator are actually deleted

156
00:04:42,410 --> 00:04:43,980
赋值运算符实际上已删除，这就是为什么如果
which is why you get a compile error if

157
00:04:44,180 --> 00:04:45,540
你尝试做这样的事情，那就是专门为
you try and do something like this and

158
00:04:45,740 --> 00:04:47,040
你尝试做这样的事情，那就是专门为
that's that's there specifically to

159
00:04:47,240 --> 00:04:49,079
阻止您将自己挖入坟墓，因为您无法复制
prevent you from digging yourself into a

160
00:04:49,279 --> 00:04:50,490
阻止您将自己挖入坟墓，因为您无法复制
grave because you cannot copy this

161
00:04:50,689 --> 00:04:52,259
因为因为一旦这些独特指针之一死亡，它们都会
because because again as soon as one of

162
00:04:52,459 --> 00:04:53,850
因为因为一旦这些独特指针之一死亡，它们都会
these unique pointers dies they all

163
00:04:54,050 --> 00:04:55,740
本质上是一种死亡，因为该内存的基础内存
essentially kind of die because the

164
00:04:55,939 --> 00:04:57,389
本质上是一种死亡，因为该内存的基础内存
memory the underlying memory of that

165
00:04:57,589 --> 00:05:00,360
窥视门控对象被释放，因此，如果您喜欢共享，那就是共享指针
peepal gated object gets freed so if you

166
00:05:00,560 --> 00:05:02,400
窥视门控对象被释放，因此，如果您喜欢共享，那就是共享指针
like sharing that's where shared pointer

167
00:05:02,600 --> 00:05:04,290
进来并分享指针的工作方式有点不同
comes in and share pointer kind of works

168
00:05:04,490 --> 00:05:05,879
进来并分享指针的工作方式有点不同
a bit a bit differently it's a bit more

169
00:05:06,079 --> 00:05:07,530
如果您愿意的话，可以选择它，因为它会在引擎盖下做很多其他的事情
hardcore if you will because it does a

170
00:05:07,730 --> 00:05:09,300
如果您愿意的话，可以选择它，因为它会在引擎盖下做很多其他的事情
lot of other stuff under the hood the

171
00:05:09,500 --> 00:05:10,920
实现共享指针的方式实际上取决于编译器
way that a shared pointer is implemented

172
00:05:11,120 --> 00:05:12,150
实现共享指针的方式实际上取决于编译器
is actually kind of up to the compiler

173
00:05:12,350 --> 00:05:13,620
以及您在编译器中使用的标准库
and the standard library that you're

174
00:05:13,819 --> 00:05:15,900
以及您在编译器中使用的标准库
using with your compiler however in

175
00:05:16,100 --> 00:05:17,340
我见过的几乎所有系统都在使用一种称为
pretty much all systems that I've seen

176
00:05:17,540 --> 00:05:18,750
我见过的几乎所有系统都在使用一种称为
it's it's using something called

177
00:05:18,949 --> 00:05:20,400
参考计数我们将有一个有关参考计数的特定视频
reference counting we're gonna have a

178
00:05:20,600 --> 00:05:21,930
参考计数我们将有一个有关参考计数的特定视频
specific video about reference counting

179
00:05:22,129 --> 00:05:23,490
实际上，在标准库中，我们要做的很多事情
and in fact a lot of these things in the

180
00:05:23,689 --> 00:05:24,960
实际上，在标准库中，我们要做的很多事情
standard library we're actually gonna

181
00:05:25,160 --> 00:05:26,340
有视频，我们可以自己实现，因为它们是很好的例子
have videos where we implement them

182
00:05:26,540 --> 00:05:28,199
有视频，我们可以自己实现，因为它们是很好的例子
ourselves because they're great examples

183
00:05:28,399 --> 00:05:30,360
C ++的工作原理以及如何使用C ++，因此我们将肯定
of how C++ works and how we can kind of

184
00:05:30,560 --> 00:05:32,490
C ++的工作原理以及如何使用C ++，因此我们将肯定
use C++ so we're going to definitely

185
00:05:32,689 --> 00:05:34,560
写我们自己独特的奖金montoya共享点，所有这些东西
write our own unique bonus montoya

186
00:05:34,759 --> 00:05:36,150
写我们自己独特的奖金montoya共享点，所有这些东西
shared point all of that kind of stuff

187
00:05:36,350 --> 00:05:38,040
未来以及其他类型的卫星掩埋功能，如果您
in the future as well as other kind of

188
00:05:38,240 --> 00:05:40,439
未来以及其他类型的卫星掩埋功能，如果您
satellite bury features so if you if you

189
00:05:40,639 --> 00:05:42,000
希望那即将到来，但共享指针的工作方式是通过引用
want that that's coming but the way the

190
00:05:42,199 --> 00:05:43,920
希望那即将到来，但共享指针的工作方式是通过引用
shared pointer works is via reference

191
00:05:44,120 --> 00:05:45,210
计数和引用计数基本上是您保持的一种做法
counting and reference counting is

192
00:05:45,410 --> 00:05:46,530
计数和引用计数基本上是您保持的一种做法
basically a practice where you keep

193
00:05:46,730 --> 00:05:49,259
跟踪您的指针有多少个引用，并尽快
track of how many references you have to

194
00:05:49,459 --> 00:05:50,790
跟踪您的指针有多少个引用，并尽快
your pointer and as soon as that

195
00:05:50,990 --> 00:05:52,470
引用计数达到零即被删除，例如
reference count reaches zero that's when

196
00:05:52,670 --> 00:05:54,689
引用计数达到零即被删除，例如
it gets deleted so as an example I

197
00:05:54,889 --> 00:05:57,060
创建一个共享指针，然后创建另一个棚指针并复制
create one shared pointer I then create

198
00:05:57,259 --> 00:05:58,439
创建一个共享指针，然后创建另一个棚指针并复制
another shed pointer and copy that my

199
00:05:58,639 --> 00:06:00,000
ref计数现在是2，因此第一个为1，第二个为2
ref count is now two so one for the

200
00:06:00,199 --> 00:06:01,530
ref计数现在是2，因此第一个为1，第二个为2
first one once the second one that's two

201
00:06:01,730 --> 00:06:03,449
当第一个死亡时，我的参考计数下降了一个，所以我现在就开始计数
when the first one dies my reference

202
00:06:03,649 --> 00:06:05,100
当第一个死亡时，我的参考计数下降了一个，所以我现在就开始计数
count goes down one so I'm on one now

203
00:06:05,300 --> 00:06:06,810
然后当最后一个死亡时，我的参考计数回到零，
and then when the last one dies my

204
00:06:07,009 --> 00:06:08,280
然后当最后一个死亡时，我的参考计数回到零，
reference count goes back to zero and

205
00:06:08,480 --> 00:06:09,120
我死了，所以内存被释放了，可以使用
I'm dead

206
00:06:09,319 --> 00:06:11,220
我死了，所以内存被释放了，可以使用
so the memory gets freed so to use a

207
00:06:11,420 --> 00:06:13,530
您只需在STD共享点中键入共享指针，就可以摆脱
shared pointer you just type in STD

208
00:06:13,730 --> 00:06:15,090
您只需在STD共享点中键入共享指针，就可以摆脱
shared point I'll I'm just get rid of

209
00:06:15,290 --> 00:06:18,150
此编译错误将在此处执行NC作为模板参数
this compile error will do NC over here

210
00:06:18,350 --> 00:06:19,470
此编译错误将在此处执行NC作为模板参数
as the template parameter

211
00:06:19,670 --> 00:06:22,500
我将做为共享实体，并将其设置为等于STV make
I'll do shared entity as the

212
00:06:22,699 --> 00:06:24,660
我将做为共享实体，并将其设置为等于STV make
and I'll set this equal to STV make

213
00:06:24,860 --> 00:06:27,689
现在共享MT，在这种情况下，您也可以像这样新建一个实体
shared MT now in this case you could

214
00:06:27,889 --> 00:06:30,090
现在共享MT，在这种情况下，您也可以像这样新建一个实体
have also done a new entity like this

215
00:06:30,290 --> 00:06:31,860
然后您可以查看是否可以正常编译，除非您绝对不想这样做
and you can see if that compiles fine

216
00:06:32,060 --> 00:06:33,600
然后您可以查看是否可以正常编译，除非您绝对不想这样做
except you definitely don't want to do

217
00:06:33,800 --> 00:06:35,100
唯一的原因是共享指针具有唯一点
that with shared pointer with unique

218
00:06:35,300 --> 00:06:36,480
唯一的原因是共享指针具有唯一点
points are really the only reason not to

219
00:06:36,680 --> 00:06:38,160
直接调用new是因为使用共享构建了异常安全性
call new directly is because of

220
00:06:38,360 --> 00:06:39,540
直接调用new是因为使用共享构建了异常安全性
exception safety built with shared

221
00:06:39,740 --> 00:06:40,620
指针实际上会有所不同，因为共享指针必须
pointer there's actually going to be a

222
00:06:40,819 --> 00:06:42,600
指针实际上会有所不同，因为共享指针必须
difference because shared pointer has to

223
00:06:42,800 --> 00:06:44,069
分配另一个称为控制块的内存块，将其存储在其中
allocate another block of memory called

224
00:06:44,269 --> 00:06:45,720
分配另一个称为控制块的内存块，将其存储在其中
the control block where it stores that

225
00:06:45,920 --> 00:06:48,780
引用计数，以及是否创建（如果先创建新实体然后通过）
reference count and if you create if you

226
00:06:48,980 --> 00:06:50,879
引用计数，以及是否创建（如果先创建新实体然后通过）
first created a new entity and then pass

227
00:06:51,079 --> 00:06:52,800
它到共享指针构造函数中，它必须分配test2
it into the shared pointer constructor

228
00:06:53,000 --> 00:06:54,660
它到共享指针构造函数中，它必须分配test2
it has to allocate that's test2

229
00:06:54,860 --> 00:06:55,949
正确的分配，因为您先构造实体然后
allocation that's right because you

230
00:06:56,149 --> 00:06:57,750
正确的分配，因为您先构造实体然后
constructing the entity first and then

231
00:06:57,949 --> 00:06:59,490
被共享的指针已被控制器剪切，必须构造其控制块
be shared pointer has the controller cut

232
00:06:59,689 --> 00:07:00,900
被共享的指针已被控制器剪切，必须构造其控制块
it has to construct its control block

233
00:07:01,100 --> 00:07:02,550
而如果您进行共享，则实际上可以将它们共同构建在一起
whereas if you do make share it can

234
00:07:02,750 --> 00:07:04,590
而如果您进行共享，则实际上可以将它们共同构建在一起
actually construct them together which

235
00:07:04,790 --> 00:07:07,560
对于那些讨厌新人和新人的人来说，效率要高得多
is a lot more efficient and also for

236
00:07:07,759 --> 00:07:09,270
对于那些讨厌新人和新人的人来说，效率要高得多
those of you people who hate new and

237
00:07:09,470 --> 00:07:10,920
删除它显然会从您的代码库中删除new关键字，因为
delete this obviously gets rid of the

238
00:07:11,120 --> 00:07:12,480
删除它显然会从您的代码库中删除new关键字，因为
new keyword from your codebase because

239
00:07:12,680 --> 00:07:13,889
你只是在叫一个城市共享而不是新实体，所以我敢打赌
you're just calling a city make shared

240
00:07:14,089 --> 00:07:15,930
你只是在叫一个城市共享而不是新实体，所以我敢打赌
instead of new entity so I bet you guys

241
00:07:16,129 --> 00:07:16,710
喜欢它，所以当然可以使用共享指针
love that

242
00:07:16,910 --> 00:07:19,259
喜欢它，所以当然可以使用共享指针
so with shared pointer you can of course

243
00:07:19,459 --> 00:07:22,889
复制它，是的，我的意思是，如果我输入这样的代码，它将可以完美工作
copy it and yeah I mean if I type in

244
00:07:23,089 --> 00:07:24,300
复制它，是的，我的意思是，如果我输入这样的代码，它将可以完美工作
code like this it's gonna work perfectly

245
00:07:24,500 --> 00:07:26,129
好的，我也可以将其移到此处，然后将其移至此处
fine I could also move this outside to

246
00:07:26,329 --> 00:07:28,410
好的，我也可以将其移到此处，然后将其移至此处
here and just have this over here let's

247
00:07:28,610 --> 00:07:30,240
让我们再来一个有趣的范围，以便我可以演示一下它是如何工作的
let's make another scope for fun so I

248
00:07:30,439 --> 00:07:31,290
让我们再来一个有趣的范围，以便我可以演示一下它是如何工作的
can kind of demonstrate how this works

249
00:07:31,490 --> 00:07:33,810
然后将其拖到这里好，这样我又有了两个作用域
and you drag this over here alright so

250
00:07:34,009 --> 00:07:35,400
然后将其拖到这里好，这样我又有了两个作用域
I've got kind of two scopes again the

251
00:07:35,600 --> 00:07:37,530
我得到的第一个是0，然后是这个，我有棚子实体
first one I've got is 0 that's it and

252
00:07:37,730 --> 00:07:38,850
我得到的第一个是0，然后是这个，我有棚子实体
then in this one I have my shed entity

253
00:07:39,050 --> 00:07:41,340
我将通过棚屋实体更轻松地进行分配，我将全部注释掉
I'm going to assign easier with my shed

254
00:07:41,540 --> 00:07:43,980
我将通过棚屋实体更轻松地进行分配，我将全部注释掉
entity I'm just going to comment out all

255
00:07:44,180 --> 00:07:45,509
用唯一的指针摆脱所有其他代码，所以现在会发生什么
get rid of all that other code with the

256
00:07:45,709 --> 00:07:47,400
用唯一的指针摆脱所有其他代码，所以现在会发生什么
unique pointer so now what will happen

257
00:07:47,600 --> 00:07:49,770
如果我击中f5，那第一件事将会发生
is if I hit f5 the first thing that's

258
00:07:49,970 --> 00:07:51,030
如果我击中f5，那第一件事将会发生
gonna happen is I'm going to construct

259
00:07:51,230 --> 00:07:53,579
我的实体，这样就很好了，它创建后，我将在
my entity so that's done good it's

260
00:07:53,779 --> 00:07:55,800
我的实体，这样就很好了，它创建后，我将在
created I'm going to assign this when

261
00:07:56,000 --> 00:07:57,750
第一件大衣死了，她也死了，但是你可以看到它并没有
the first coat dies this challenge she

262
00:07:57,949 --> 00:08:00,329
第一件大衣死了，她也死了，但是你可以看到它并没有
dies however you can see that it hasn't

263
00:08:00,529 --> 00:08:02,040
销毁了我的HT，但并未删除它，因为0仍然在保存一个
destroyed my HT it hasn't deleted it

264
00:08:02,240 --> 00:08:04,379
销毁了我的HT，但并未删除它，因为0仍然在保存一个
because a 0 is still alive at holding a

265
00:08:04,579 --> 00:08:06,930
现在引用该实体时，当我有10个时，该实体死亡时
reference to that entity now when I have

266
00:08:07,129 --> 00:08:09,449
现在引用该实体时，当我有10个时，该实体死亡时
10 that's when it dies when all the

267
00:08:09,649 --> 00:08:11,639
当所有堆栈分配的对象种类
references are gone when all of the

268
00:08:11,839 --> 00:08:13,379
当所有堆栈分配的对象种类
stack allocated kind of objects that

269
00:08:13,579 --> 00:08:15,449
跟踪所有共享指针，当它们从读取时消失时，
keep track of all the shared pointers

270
00:08:15,649 --> 00:08:17,340
跟踪所有共享指针，当它们从读取时消失时，
when they die when they get read from

271
00:08:17,540 --> 00:08:19,410
记忆所有这些，这就是您的基础实体被删除的所有权利
memory all of them that's when your

272
00:08:19,610 --> 00:08:21,240
记忆所有这些，这就是您的基础实体被删除的所有权利
underlying entity gets deleted all right

273
00:08:21,439 --> 00:08:22,770
最后还有一些可以与共享点一起使用的东西是
and finally there's something else that

274
00:08:22,970 --> 00:08:23,970
最后还有一些可以与共享点一起使用的东西是
you can use with shared points are

275
00:08:24,170 --> 00:08:24,270
称为重量指针，您可以做什么
called

276
00:08:24,470 --> 00:08:26,250
称为重量指针，您可以做什么
a weight pointer and what you can do

277
00:08:26,449 --> 00:08:27,810
只是声明它就像什么
with that is just declare it like it was

278
00:08:28,009 --> 00:08:28,230
只是声明它就像什么
anything

279
00:08:28,430 --> 00:08:31,439
并给它一个股票密度的值，这是做什么的
and kind of give it the value of a share

280
00:08:31,639 --> 00:08:33,929
并给它一个股票密度的值，这是做什么的
density and what this does what this

281
00:08:34,129 --> 00:08:36,599
确实有点像您要复制该共享实体并增加
does is kind of the same as if you were

282
00:08:36,799 --> 00:08:38,099
确实有点像您要复制该共享实体并增加
to copy that share entity and increase

283
00:08:38,299 --> 00:08:39,269
引用计数，除非它不包括它不增加引用
the ref count except it doesn't

284
00:08:39,470 --> 00:08:40,559
引用计数，除非它不包括它不增加引用
including it doesn't increase the ref

285
00:08:40,759 --> 00:08:42,269
当您将一个共享指针分配给另一个共享指针从而计数时计数
count when you assign a shared pointer

286
00:08:42,470 --> 00:08:43,949
当您将一个共享指针分配给另一个共享指针从而计数时计数
to another shared pointer thus copying

287
00:08:44,149 --> 00:08:45,839
它会增加引用计数，但是当您将共享指针分配给
it it will increase the ref count but

288
00:08:46,039 --> 00:08:47,039
它会增加引用计数，但是当您将共享指针分配给
when you assign a shared pointer to a

289
00:08:47,240 --> 00:08:48,990
弱指针会使您的橙色增加裁判计数，因此这对于如果您
weak pointer your orange increase the

290
00:08:49,190 --> 00:08:50,699
弱指针会使您的橙色增加裁判计数，因此这对于如果您
ref count so this is great for if you

291
00:08:50,899 --> 00:08:52,349
有点不想获得实体的所有权，就像您可能正在存储
kind of don't want to take ownership of

292
00:08:52,549 --> 00:08:54,449
有点不想获得实体的所有权，就像您可能正在存储
the entity like you might be storing a

293
00:08:54,649 --> 00:08:55,679
实体列表，您并不在乎它们是否有效，但是您
list of entities and you don't really

294
00:08:55,879 --> 00:08:57,000
实体列表，您并不在乎它们是否有效，但是您
care if they're valid or not but you

295
00:08:57,200 --> 00:08:58,439
只是想像使用弱指针一样对它们的引用进行存储，您可以
just want to store like a reference to

296
00:08:58,639 --> 00:09:00,120
只是想像使用弱指针一样对它们的引用进行存储，您可以
them right with weak pointer you can

297
00:09:00,320 --> 00:09:02,219
有点问，嘿，这还活着吗，如果可以，你可以做
kind of ask it hey is this is this still

298
00:09:02,419 --> 00:09:03,750
有点问，嘿，这还活着吗，如果可以，你可以做
even alive and if it is you can do

299
00:09:03,950 --> 00:09:05,549
您需要执行的任何操作，但它不会使它保持活动状态，因为它不会
whatever you need to do but it won't

300
00:09:05,750 --> 00:09:07,289
您需要执行的任何操作，但它不会使它保持活动状态，因为它不会
keep it alive because it doesn't

301
00:09:07,490 --> 00:09:08,669
实际上增加了引用计数，这意味着如果我最糟糕的是实际替换
actually increase the ref count meaning

302
00:09:08,870 --> 00:09:10,500
实际上增加了引用计数，这意味着如果我最糟糕的是实际替换
that if I worst was to actually replace

303
00:09:10,700 --> 00:09:12,659
这棚，看看弱指针在哪里，然后基本上做了完全一样的
this shed and see where the weak pointer

304
00:09:12,860 --> 00:09:14,819
这棚，看看弱指针在哪里，然后基本上做了完全一样的
and then basically did the exact same

305
00:09:15,019 --> 00:09:16,979
我在那之前做过的事情，实体将在这里获得信誉
thing that I did before then the entity

306
00:09:17,179 --> 00:09:17,969
我在那之前做过的事情，实体将在这里获得信誉
will get credit here

307
00:09:18,169 --> 00:09:19,139
它会分配到棚屋，然后看，但是当我离开第一辆车时
it'll get assigned to the shed and see

308
00:09:19,340 --> 00:09:22,019
它会分配到棚屋，然后看，但是当我离开第一辆车时
but when I exit the first car that is

309
00:09:22,220 --> 00:09:24,000
它被破坏了什么，所以这个重量指针现在指向一个无效的
what it gets destroyed so this weight

310
00:09:24,200 --> 00:09:25,769
它被破坏了什么，所以这个重量指针现在指向一个无效的
pointer is now pointing to an invalid

311
00:09:25,970 --> 00:09:28,139
和T但是您可以问一个弱指针，您是否过期了
and T however you can ask a weak pointer

312
00:09:28,340 --> 00:09:29,159
和T但是您可以问一个弱指针，您是否过期了
are you expired

313
00:09:29,360 --> 00:09:30,449
您是否仍然有效，所以对于您而言，这现在是非常聪明的指针
are you still valid so that's pretty

314
00:09:30,649 --> 00:09:32,759
您是否仍然有效，所以对于您而言，这现在是非常聪明的指针
much smart pointers now as for when you

315
00:09:32,960 --> 00:09:34,379
应该使用它们，如果我正在使用的话，应该一直尝试使用它们
should use them you should probably try

316
00:09:34,580 --> 00:09:35,939
应该使用它们，如果我正在使用的话，应该一直尝试使用它们
and use them all the time if I'm being

317
00:09:36,139 --> 00:09:37,259
完全诚实，他们自动执行了您的内存管理，他们摆脱了
completely honest they automate your

318
00:09:37,460 --> 00:09:39,599
完全诚实，他们自动执行了您的内存管理，他们摆脱了
memory management they they they get rid

319
00:09:39,799 --> 00:09:40,859
他们所做的一切都是为了防止您意外地通过
of all they did they prevent you from

320
00:09:41,059 --> 00:09:42,509
他们所做的一切都是为了防止您意外地通过
accidentally leaking memory by

321
00:09:42,710 --> 00:09:44,339
忘记调用删除它们确实是非常有用的共享指针，特别是
forgetting to call delete they're really

322
00:09:44,539 --> 00:09:46,319
忘记调用删除它们确实是非常有用的共享指针，特别是
quite useful shared pointer specifically

323
00:09:46,519 --> 00:09:47,819
由于它的引用计数系统，所以有点开销，但是
has a bit of an overhead because of its

324
00:09:48,019 --> 00:09:49,529
由于它的引用计数系统，所以有点开销，但是
reference counting systems but then

325
00:09:49,730 --> 00:09:50,699
很多人很多人倾向于写自己的记忆
again a lot of people a lot of people

326
00:09:50,899 --> 00:09:52,229
很多人很多人倾向于写自己的记忆
who tend to write their own memory

327
00:09:52,429 --> 00:09:53,549
管理系统也往往会有一些开销，所以这有点
management systems tend to have a bit of

328
00:09:53,750 --> 00:09:55,979
管理系统也往往会有一些开销，所以这有点
an overhead as well so it's kind of it's

329
00:09:56,179 --> 00:09:57,929
一个非常微妙的话题，因为您现在拥有这种新型的C ++程序员
a very delicate topic because you have

330
00:09:58,129 --> 00:09:59,789
一个非常微妙的话题，因为您现在拥有这种新型的C ++程序员
this new breed of C++ programmers now

331
00:09:59,990 --> 00:10:01,649
他们只使用这些功能，然后您就拥有了像眼镜一样的人
who only use these kind of features and

332
00:10:01,850 --> 00:10:03,029
他们只使用这些功能，然后您就拥有了像眼镜一样的人
then you have like the optical people

333
00:10:03,230 --> 00:10:04,979
谁在使用您并删除我，两者兼而有之，因为有一段时间
who using you and delete I'm kind of a

334
00:10:05,179 --> 00:10:07,049
谁在使用您并删除我，两者兼而有之，因为有一段时间
bit of both because there is a time

335
00:10:07,250 --> 00:10:09,389
您想使用唯一指针或共享点两个的地方，但是
where you want to use unique pointer or

336
00:10:09,590 --> 00:10:11,309
您想使用唯一指针或共享点两个的地方，但是
shared point two perhaps but there's

337
00:10:11,509 --> 00:10:12,419
也是需要新的和删除的时间，所以不要以为这有
also a time where you need new and

338
00:10:12,620 --> 00:10:14,250
也是需要新的和删除的时间，所以不要以为这有
delete so don't think that this has

339
00:10:14,450 --> 00:10:15,719
我认为在删除中完全替换了您，它绝对没有替换
completely replaced you in delete in my

340
00:10:15,919 --> 00:10:17,639
我认为在删除中完全替换了您，它绝对没有替换
opinion it has absolutely not replaced

341
00:10:17,840 --> 00:10:19,229
新建并删除它只是您刚刚应该做的事情
new and delete it's just something that

342
00:10:19,429 --> 00:10:20,639
新建并删除它只是您刚刚应该做的事情
you should probably do when you just

343
00:10:20,840 --> 00:10:22,199
需要声明一个堆分配的对象，而您并不需要特别整理
need to declare a heap-allocated object

344
00:10:22,399 --> 00:10:23,609
需要声明一个堆分配的对象，而您并不需要特别整理
and you don't particularly want to tidy

345
00:10:23,809 --> 00:10:25,349
忙于自己，因为您以后不需要显式调用
up after yourself because you don't need

346
00:10:25,549 --> 00:10:27,179
忙于自己，因为您以后不需要显式调用
to kind of explicitly call to later

347
00:10:27,379 --> 00:10:29,099
在这种情况下，您应该使用智能型显式管理该内存
explicitly manage that memory in those

348
00:10:29,299 --> 00:10:30,779
在这种情况下，您应该使用智能型显式管理该内存
cases you should be using a smart

349
00:10:30,980 --> 00:10:32,429
指针通常会使用唯一的指针，因为它具有较低的
pointers usually use unique pointer

350
00:10:32,629 --> 00:10:33,929
指针通常会使用唯一的指针，因为它具有较低的
whenever you can because it has a lower

351
00:10:34,129 --> 00:10:36,719
但如果您绝对需要在对象之间共享，那么您就不能
overhead but if you absolutely need to

352
00:10:36,919 --> 00:10:38,579
但如果您绝对需要在对象之间共享，那么您就不能
share between objects so you just can't

353
00:10:38,779 --> 00:10:39,339
使用任何只是不能使用唯一指针然后使用
use any

354
00:10:39,539 --> 00:10:41,679
使用任何只是不能使用唯一指针然后使用
just can't use a unique pointer then use

355
00:10:41,879 --> 00:10:43,419
共享指针，但绝对要先执行该顺序，然后再考虑指针
a shared pointer but definitely going

356
00:10:43,620 --> 00:10:44,709
共享指针，但绝对要先执行该顺序，然后再考虑指针
that order first you think pointer first

357
00:10:44,909 --> 00:10:46,209
偏好分享点第二个偏好，希望你们喜欢
preference share point a second

358
00:10:46,409 --> 00:10:47,620
偏好分享点第二个偏好，希望你们喜欢
preference hope you guys enjoy this

359
00:10:47,820 --> 00:10:49,179
视频，即使您在YouTube上点击了“赞”按钮，也可以
video if you did even if that like

360
00:10:49,379 --> 00:10:50,919
视频，即使您在YouTube上点击了“赞”按钮，也可以
button on our YouTube check I said it

361
00:10:51,120 --> 00:10:53,109
就像上次说的那样，但是现在就像在中间
was like the said it was there last time

362
00:10:53,309 --> 00:10:54,549
就像上次说的那样，但是现在就像在中间
but now it's like in the middle it's

363
00:10:54,750 --> 00:10:56,469
喜欢这里还是我们想要的东西，您可以单击喜欢按钮
like here or something out in our anyway

364
00:10:56,669 --> 00:10:57,998
喜欢这里还是我们想要的东西，您可以单击喜欢按钮
you can click that like button if you

365
00:10:58,198 --> 00:10:59,919
喜欢这个视频，请留下您对以后的视频以及所有其他建议
enjoyed this video leave any suggestions

366
00:11:00,120 --> 00:11:01,748
喜欢这个视频，请留下您对以后的视频以及所有其他建议
you have for future videos and all that

367
00:11:01,948 --> 00:11:03,339
在下面的评论部分以及您可能遇到的任何问题
in the comment section below as well as

368
00:11:03,539 --> 00:11:04,599
在下面的评论部分以及您可能遇到的任何问题
any questions you may have

369
00:11:04,799 --> 00:11:06,188
我有一个不一致的服务器，您还可以在其中询问问题链接
I have a discord server where you can

370
00:11:06,389 --> 00:11:07,479
我有一个不一致的服务器，您还可以在其中询问问题链接
also ask questions link in the

371
00:11:07,679 --> 00:11:09,399
下面的说明，如果您真的喜欢这个视频，并且喜欢这个系列，
description below and if you really like

372
00:11:09,600 --> 00:11:10,868
下面的说明，如果您真的喜欢这个视频，并且喜欢这个系列，
this video and you like this series and

373
00:11:11,068 --> 00:11:11,919
您想要支持它，想要查看更多视频，则可以转到
you want to support it you want to see

374
00:11:12,120 --> 00:11:13,269
您想要支持它，想要查看更多视频，则可以转到
more videos then you can go to

375
00:11:13,470 --> 00:11:14,979
patreon.com/scishow Turner通过帮助支持这一点而获得了一些甜蜜的回报
patreon.com/scishow turner pick up some

376
00:11:15,179 --> 00:11:17,019
patreon.com/scishow Turner通过帮助支持这一点而获得了一些甜蜜的回报
sweet rewards by helping to support this

377
00:11:17,220 --> 00:11:18,849
系列我下次见
series I will see you guys next time

378
00:11:19,049 --> 00:11:20,199
系列我下次见
goodbye

379
00:11:20,399 --> 00:11:25,399
[音乐]
[Music]

