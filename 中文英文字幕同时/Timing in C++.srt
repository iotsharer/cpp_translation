﻿1
00:00:00,000 --> 00:00:01,449
嘿，大家好，我叫C ++系列，欢迎中国来参加，今天
hey what's up guys my name is a China

2
00:00:01,649 --> 00:00:04,389
嘿，大家好，我叫C ++系列，欢迎中国来参加，今天
welcome back to my c++ series so today

3
00:00:04,589 --> 00:00:05,828
我们将要讨论计时，那么我们如何计时需要多长时间
we're gonna be talking all about timing

4
00:00:06,028 --> 00:00:08,169
我们将要讨论计时，那么我们如何计时需要多长时间
so how do we time how long it takes for

5
00:00:08,369 --> 00:00:10,120
我们完成某些操作或执行某些代码
us to complete a certain operation or

6
00:00:10,320 --> 00:00:12,250
我们完成某些操作或执行某些代码
execute certain code that is what

7
00:00:12,449 --> 00:00:14,050
今天，我们将要成为现在，时机对很多事情都有用
today's we are going to be about now

8
00:00:14,250 --> 00:00:17,109
今天，我们将要成为现在，时机对很多事情都有用
timing is useful for so many things

9
00:00:17,309 --> 00:00:18,579
您是否想在某个时间发生某件事，或者您是否正在
whether or not you want something to

10
00:00:18,778 --> 00:00:20,890
您是否想在某个时间发生某件事，或者您是否正在
happen at a certain time or if you're

11
00:00:21,089 --> 00:00:22,030
只是评估您的效果或基准测试，并查看您的速度有多快
just kind of evaluating your performance

12
00:00:22,230 --> 00:00:24,100
只是评估您的效果或基准测试，并查看您的速度有多快
or benchmarking and seeing how fast your

13
00:00:24,300 --> 00:00:27,609
代码运行时，您需要知道在启动过程中经过了多少时间
code runs you need to know how much time

14
00:00:27,809 --> 00:00:29,470
代码运行时，您需要知道在启动过程中经过了多少时间
passes while you up while your

15
00:00:29,670 --> 00:00:31,720
应用程序实际上正在运行，有几种方法可以实现此目的，因为
application is actually running there

16
00:00:31,920 --> 00:00:33,669
应用程序实际上正在运行，有几种方法可以实现此目的，因为
are several ways to achieve this since

17
00:00:33,869 --> 00:00:36,009
C ++ 11中我们有一个叫做Chronos的东西，本质上说它本质上是
C++ 11 we have something called Chronos

18
00:00:36,210 --> 00:00:37,779
C ++ 11中我们有一个叫做Chronos的东西，本质上说它本质上是
essentially say it's essentially part of

19
00:00:37,979 --> 00:00:40,899
C ++附带的库，实际上不是我们拥有的库
the library that is becomes with C++ and

20
00:00:41,100 --> 00:00:42,369
C ++附带的库，实际上不是我们拥有的库
it's not something that we actually have

21
00:00:42,570 --> 00:00:44,349
如果您想获得较高的收益，请先进入操作系统库进行实际操作
to go to the operating system library to

22
00:00:44,549 --> 00:00:47,229
如果您想获得较高的收益，请先进入操作系统库进行实际操作
actually do before if you wanted a high

23
00:00:47,429 --> 00:00:48,909
解析时间表示您需要一个非常精确的计时器
resolution time on meaning you wanted a

24
00:00:49,109 --> 00:00:50,979
解析时间表示您需要一个非常精确的计时器
very very precise timer you need to

25
00:00:51,179 --> 00:00:52,779
实际上使用操作系统库，因此在Windows中，例如
actually use the operating systems

26
00:00:52,979 --> 00:00:54,878
实际上使用操作系统库，因此在Windows中，例如
library so in Windows for example we

27
00:00:55,079 --> 00:00:56,140
有一个称为查询性能计数器的东西，我们仍然可以使用该东西
have something called query performance

28
00:00:56,340 --> 00:00:58,628
有一个称为查询性能计数器的东西，我们仍然可以使用该东西
counter we can still use that stuff and

29
00:00:58,829 --> 00:01:00,038
实际上，如果您想更多地控制自己的访问方式
in fact if you want a little bit more

30
00:01:00,238 --> 00:01:02,739
实际上，如果您想更多地控制自己的访问方式
control of how you actually access that

31
00:01:02,939 --> 00:01:05,918
CPU的计时能力，那么您可能确实想使用该平台
timing capability of your CPU then you

32
00:01:06,118 --> 00:01:07,629
CPU的计时能力，那么您可能确实想使用该平台
probably do want to use the platform

33
00:01:07,829 --> 00:01:10,329
特定的库，但是今天我们只看一下
specific libraries however today we're

34
00:01:10,530 --> 00:01:11,829
特定的库，但是今天我们只看一下
just going to be taking a look at the

35
00:01:12,030 --> 00:01:14,230
一种与平台无关的c ++标准库，弄清楚如何
kind of platform-independent c++

36
00:01:14,430 --> 00:01:16,390
一种与平台无关的c ++标准库，弄清楚如何
standard library way of figuring out how

37
00:01:16,590 --> 00:01:19,659
执行代码行之间的时间间隔或时间长短，以及
long or how much time passes between our

38
00:01:19,859 --> 00:01:21,700
执行代码行之间的时间间隔或时间长短，以及
lines of code as they get executed and

39
00:01:21,900 --> 00:01:24,129
这是chrono库的一部分，现在我在说明中链接了一个链接
that is part of the chrono library now

40
00:01:24,329 --> 00:01:27,459
这是chrono库的一部分，现在我在说明中链接了一个链接
I've linked a link in the description

41
00:01:27,659 --> 00:01:30,308
下面是这种chrono类型的API的实际cpp参考
below to the actual cpp reference for

42
00:01:30,509 --> 00:01:33,819
下面是这种chrono类型的API的实际cpp参考
this chrono kind of API I'm going to

43
00:01:34,019 --> 00:01:35,200
向您展示了一种非常简单的方法来设置和开始使用
show you a very simple way of how we can

44
00:01:35,400 --> 00:01:36,909
向您展示了一种非常简单的方法来设置和开始使用
set it up and start using it here so

45
00:01:37,109 --> 00:01:38,289
我们可以弄清楚运行某些代码要花多长时间，我可能会
that we can figure out how long it takes

46
00:01:38,489 --> 00:01:40,808
我们可以弄清楚运行某些代码要花多长时间，我可能会
us to run certain code and I'll probably

47
00:01:41,009 --> 00:01:42,969
还向您展示了一种有用的方法，现在整个视频将
also show you one way in which it can be

48
00:01:43,170 --> 00:01:45,128
还向您展示了一种有用的方法，现在整个视频将
useful now this whole video is going to

49
00:01:45,328 --> 00:01:46,329
对于本系列的未来很重要，因为随着我们开始整合
be important for the future of this

50
00:01:46,530 --> 00:01:48,189
对于本系列的未来很重要，因为随着我们开始整合
series because as we start integrating

51
00:01:48,390 --> 00:01:50,619
更复杂的功能，我开始越来越多地谈论如何
more kind of complex features and I

52
00:01:50,819 --> 00:01:52,329
更复杂的功能，我开始越来越多地谈论如何
start talking more and more about how to

53
00:01:52,530 --> 00:01:55,390
正确执行操作以及如何编写将要使用的性能良好的代码
do things properly and how to write good

54
00:01:55,590 --> 00:01:58,149
正确执行操作以及如何编写将要使用的性能良好的代码
performing code we're going to be using

55
00:01:58,349 --> 00:02:01,719
是时候看到这种差异，所以如果我向您展示一种缓慢的方法
timing to see that difference so if I

56
00:02:01,920 --> 00:02:03,819
是时候看到这种差异，所以如果我向您展示一种缓慢的方法
show you a kind of slow way of doing

57
00:02:04,019 --> 00:02:05,829
事情，然后我说很好，实际上，如果我们这样做会更好
things and then I say well actually it

58
00:02:06,030 --> 00:02:07,000
事情，然后我说很好，实际上，如果我们这样做会更好
would be better if we did it this way

59
00:02:07,200 --> 00:02:09,429
您将可以实际看到花费了多长时间的差异
you'll be able to actually see the

60
00:02:09,628 --> 00:02:11,289
您将可以实际看到花费了多长时间的差异
difference in terms of how long it takes

61
00:02:11,489 --> 00:02:11,610
实际运行该代码，所以这非常
to

62
00:02:11,810 --> 00:02:13,618
实际运行该代码，所以这非常
actually run that code so this is very

63
00:02:13,818 --> 00:02:15,960
重要的是，这很可能会成为基准测试，我将详细介绍
important from this will probably branch

64
00:02:16,159 --> 00:02:17,520
重要的是，这很可能会成为基准测试，我将详细介绍
into benchmarking and I'll talk more

65
00:02:17,719 --> 00:02:19,500
关于在另一个视频中进行基准测试，我们可以设置某种API，以便
about benchmarking in another video and

66
00:02:19,699 --> 00:02:21,630
关于在另一个视频中进行基准测试，我们可以设置某种API，以便
we can set up some kind of API so that

67
00:02:21,830 --> 00:02:22,980
我们可以计时完成功能和任意范围所需的时间
we can time how long it takes to

68
00:02:23,180 --> 00:02:25,500
我们可以计时完成功能和任意范围所需的时间
complete functions and arbitrary scopes

69
00:02:25,699 --> 00:02:28,319
代码也可能如此，所以请注意此视频，让我们跳到正确的位置
code probably as well so yeah pay

70
00:02:28,519 --> 00:02:29,850
代码也可能如此，所以请注意此视频，让我们跳到正确的位置
attention to this video let's jump right

71
00:02:30,050 --> 00:02:31,590
并学习如何计时曲线，这是我要做的第一件事
into it and learn how we can time a

72
00:02:31,789 --> 00:02:32,939
并学习如何计时曲线，这是我要做的第一件事
curve so the first thing I'm going to do

73
00:02:33,139 --> 00:02:35,219
这是克朗（Krona），其中包含了我们需要的几乎所有东西，我也是
here is include Krona this contains

74
00:02:35,419 --> 00:02:36,630
这是克朗（Krona），其中包含了我们需要的几乎所有东西，我也是
pretty much everything we need I'm also

75
00:02:36,830 --> 00:02:37,980
将包含线程，因为我们将在一分钟内对此进行一些处理
going to include thread because we'll do

76
00:02:38,180 --> 00:02:39,270
将包含线程，因为我们将在一分钟内对此进行一些处理
some stuff with that in a minute as well

77
00:02:39,469 --> 00:02:42,090
因此从chrono实际使用该库并找出其非常非常简单
so from chrono it's very very simple to

78
00:02:42,289 --> 00:02:44,069
因此从chrono实际使用该库并找出其非常非常简单
actually use that library and figure out

79
00:02:44,269 --> 00:02:45,390
我们现在可以输入什么时间，只需输入STD chrono高分辨率时钟
what the current time is we can just

80
00:02:45,590 --> 00:02:48,420
我们现在可以输入什么时间，只需输入STD chrono高分辨率时钟
type in STD chrono high resolution clock

81
00:02:48,620 --> 00:02:51,390
然后，现在这就是如果您将鼠标悬停在
and then now this is what actually gives

82
00:02:51,590 --> 00:02:54,030
然后，现在这就是如果您将鼠标悬停在
us the current time if you hover over

83
00:02:54,229 --> 00:02:55,860
这可以看到它返回了SUV当前时间的时间点
this you can see it returns a time point

84
00:02:56,060 --> 00:02:57,390
这可以看到它返回了SUV当前时间的时间点
of SUV current sit o'clock it's quite a

85
00:02:57,590 --> 00:02:59,399
长类型，这就是为什么我要使用订单和这种标签的原因
long type which is why I'm just going to

86
00:02:59,598 --> 00:03:01,380
长类型，这就是为什么我要使用订单和这种标签的原因
use order and kind of label this as the

87
00:03:01,580 --> 00:03:03,180
开始的时候，我们要在这里执行某种代码
starting time we're then going to

88
00:03:03,379 --> 00:03:05,610
开始的时候，我们要在这里执行某种代码
execute some kind of code here what I'm

89
00:03:05,810 --> 00:03:06,930
现在要做，因为我实际上没有其他要执行的代码
going to do for now because I don't

90
00:03:07,129 --> 00:03:08,430
现在要做，因为我实际上没有其他要执行的代码
actually have any other code to execute

91
00:03:08,629 --> 00:03:10,590
一周和一次的时间中，我只是要为这个线程睡眠编写sed，然后
of the week and time I'm just going to

92
00:03:10,789 --> 00:03:14,280
一周和一次的时间中，我只是要为这个线程睡眠编写sed，然后
write sed this thread sleep for and then

93
00:03:14,479 --> 00:03:16,319
我们将只写1s一秒钟，要得到它实际上必须使用a
we'll just write 1s for one second and

94
00:03:16,519 --> 00:03:18,030
我们将只写1s一秒钟，要得到它实际上必须使用a
to get that will actually have to use a

95
00:03:18,229 --> 00:03:22,920
命名空间的STD文字和chrono文字使它变得很酷
namespace STD literals and chrono

96
00:03:23,120 --> 00:03:26,280
命名空间的STD文字和chrono文字使它变得很酷
literals to get that s there okay cool

97
00:03:26,479 --> 00:03:27,900
所以基本上我们现在要做的就是告诉这个线程当前
so basically what we're doing now is

98
00:03:28,099 --> 00:03:29,280
所以基本上我们现在要做的就是告诉这个线程当前
we're telling this thread this current

99
00:03:29,479 --> 00:03:31,140
线程睡眠一秒钟，所以我们说的只是一种暂停
thread to sleep for one second so we're

100
00:03:31,340 --> 00:03:32,520
线程睡眠一秒钟，所以我们说的只是一种暂停
saying just kind of pauses pause

101
00:03:32,719 --> 00:03:36,000
执行一秒钟，因此很明显，当我们开始时，通过编写此代码
execution for one second so obviously by

102
00:03:36,199 --> 00:03:37,890
执行一秒钟，因此很明显，当我们开始时，通过编写此代码
writing this code when we have our start

103
00:03:38,090 --> 00:03:40,259
我们的结束时间应该是一秒钟左右
and our end timing we should see it be

104
00:03:40,459 --> 00:03:42,210
我们的结束时间应该是一秒钟左右
around one second probably won't be

105
00:03:42,409 --> 00:03:44,280
正好一秒钟，因为首先不能保证所有线程睡眠
exactly one second because first of all

106
00:03:44,479 --> 00:03:46,618
正好一秒钟，因为首先不能保证所有线程睡眠
thread sleeps are not guaranteed to be

107
00:03:46,818 --> 00:03:48,900
确切地说，您告诉它要睡觉多少时间，但它们会从
exactly how much you tell it to sleep

108
00:03:49,099 --> 00:03:50,730
确切地说，您告诉它要睡觉多少时间，但它们会从
for but also they'll be overhead from

109
00:03:50,930 --> 00:03:53,400
实际的时间，所以如果我们回到这个位置，我会正确结束订单，这将
the actual timing so if we get back into

110
00:03:53,599 --> 00:03:55,710
实际的时间，所以如果我们回到这个位置，我会正确结束订单，这将
this I'll right order end and this will

111
00:03:55,909 --> 00:03:57,840
与这里的起始代码完全相同，最后我们要弄清楚
be exactly the same as this start code

112
00:03:58,039 --> 00:03:59,368
与这里的起始代码完全相同，最后我们要弄清楚
here and finally we want to figure out

113
00:03:59,568 --> 00:04:01,379
实际持续时间，所以我要写SVD计时持续时间，我们可以得到
the actual duration so I'll write SVD

114
00:04:01,579 --> 00:04:04,319
实际持续时间，所以我要写SVD计时持续时间，我们可以得到
chrono duration well we can just get

115
00:04:04,519 --> 00:04:05,849
这对我们来说足够高的分辨率，我称之为
this in floats that'll be high enough

116
00:04:06,049 --> 00:04:07,650
这对我们来说足够高的分辨率，我称之为
resolution for us I'll call this

117
00:04:07,849 --> 00:04:10,289
持续时间，就像我们本可以使用的那样，它只是结束减去开始
duration and it will just be end minus

118
00:04:10,489 --> 00:04:12,118
持续时间，就像我们本可以使用的那样，它只是结束减去开始
start just like that we could have used

119
00:04:12,318 --> 00:04:13,860
也可以在这里订购，但这种类型的期限不长，所以可以写下来
order here as well but this type isn't

120
00:04:14,060 --> 00:04:15,330
也可以在这里订购，但这种类型的期限不长，所以可以写下来
quite as long so it's fine to write down

121
00:04:15,530 --> 00:04:17,730
最后我只是要了解控制台方向
and finally I'm just going to see out

122
00:04:17,930 --> 00:04:19,319
最后我只是要了解控制台方向
that into the console direction don't

123
00:04:19,519 --> 00:04:21,598
数，这将在几秒钟内
count and

124
00:04:21,798 --> 00:04:23,670
数，这将在几秒钟内
and this will be in seconds I'll just

125
00:04:23,870 --> 00:04:26,309
在末尾加上s，这样很明显，如果我按f5键，您会看到我们的数字
add s to the end of that so that's clear

126
00:04:26,509 --> 00:04:28,800
在末尾加上s，这样很明显，如果我按f5键，您会看到我们的数字
if I hit f5 you can see the number we

127
00:04:29,000 --> 00:04:30,540
到达这里大约一秒钟左右，这就是我刚刚展示的
get here is pretty much around one

128
00:04:30,740 --> 00:04:32,520
到达这里大约一秒钟左右，这就是我刚刚展示的
second so that's it what I just showed

129
00:04:32,720 --> 00:04:34,319
您是一种独立于平台的方式，实际上可以计算出某事物有多长时间
you is a platform-independent way of

130
00:04:34,519 --> 00:04:35,759
您是一种独立于平台的方式，实际上可以计算出某事物有多长时间
actually figuring out how long something

131
00:04:35,959 --> 00:04:38,040
花费了多少时间或获得了当前时间以及所有这些
takes or how much time passes or getting

132
00:04:38,240 --> 00:04:39,569
花费了多少时间或获得了当前时间以及所有这些
the current time and all that this

133
00:04:39,769 --> 00:04:41,189
内核库非常棒，它具有很高的分辨率，并且可以在
kernel library is fantastic it's very

134
00:04:41,389 --> 00:04:42,990
内核库非常棒，它具有很高的分辨率，并且可以在
high resolution timing and it works on

135
00:04:43,189 --> 00:04:44,939
几乎所有平台，所以我真的建议您将其用于所有平台
pretty much all platforms so really I

136
00:04:45,139 --> 00:04:46,199
几乎所有平台，所以我真的建议您将其用于所有平台
recommend that you use this for all of

137
00:04:46,399 --> 00:04:47,550
您的时间需求，除非您专门进行某种
your timing needs unless you're

138
00:04:47,750 --> 00:04:48,689
您的时间需求，除非您专门进行某种
specifically doing some kind of

139
00:04:48,889 --> 00:04:50,009
低级的东西，所以如果您正在运行
low-level things so you want to reduce

140
00:04:50,209 --> 00:04:52,139
低级的东西，所以如果您正在运行
overhead even more if you're running a

141
00:04:52,339 --> 00:04:53,579
庞大的游戏引擎或某种很酷的库，您可能想使用
huge game engine or some kind of cool

142
00:04:53,779 --> 00:04:55,020
庞大的游戏引擎或某种很酷的库，您可能想使用
library you might want to use the

143
00:04:55,220 --> 00:04:56,910
平台无关，很抱歉，平台特定的库（例如
platform independent I'm sorry the

144
00:04:57,110 --> 00:04:59,369
平台无关，很抱歉，平台特定的库（例如
platform specific library such as the

145
00:04:59,569 --> 00:05:01,110
win32 api以及查询性能计数器和我们可能要研究的内容
win32 api with the query performance

146
00:05:01,310 --> 00:05:02,910
win32 api以及查询性能计数器和我们可能要研究的内容
counter and stuff we might look into

147
00:05:03,110 --> 00:05:04,740
将来，但实际上几乎不需要99％的案例
that in the future but really there's

148
00:05:04,939 --> 00:05:07,199
将来，但实际上几乎不需要99％的案例
pretty much no need for 99% of cases

149
00:05:07,399 --> 00:05:10,079
现在就使用STD kroehner，如果我们再回到这一点，我将向您展示
just use STD kroehner now if we dive

150
00:05:10,279 --> 00:05:11,579
现在就使用STD kroehner，如果我们再回到这一点，我将向您展示
back into this I'm going to show you a

151
00:05:11,779 --> 00:05:13,110
实际处理此问题的一种更聪明的方法，因为您可以看到
bit of a smarter way of actually dealing

152
00:05:13,310 --> 00:05:14,490
实际处理此问题的一种更聪明的方法，因为您可以看到
with this because you can see that this

153
00:05:14,689 --> 00:05:16,290
我们必须运行很多代码，因此支持假设我们有一个
is quite a lot of code that we have to

154
00:05:16,490 --> 00:05:18,420
我们必须运行很多代码，因此支持假设我们有一个
run so support suppose that we have a

155
00:05:18,620 --> 00:05:21,119
函数，我将其称为函数，它的功能类似于打印
function I'll call it function and it

156
00:05:21,319 --> 00:05:23,338
函数，我将其称为函数，它的功能类似于打印
does something like maybe just prints

157
00:05:23,538 --> 00:05:25,769
在控制台上打招呼很多次，所以我将其包装为
out in our hello to the console a bunch

158
00:05:25,968 --> 00:05:27,600
在控制台上打招呼很多次，所以我将其包装为
of times so I'll just wrap this in a for

159
00:05:27,800 --> 00:05:29,819
循环，我会说可能会执行约一百次或类似的操作
loop and I'll say maybe it does that

160
00:05:30,019 --> 00:05:32,100
循环，我会说可能会执行约一百次或类似的操作
about a hundred times or something like

161
00:05:32,300 --> 00:05:32,338
没错，所以我们有一个for循环，它运行一个
that

162
00:05:32,538 --> 00:05:34,230
没错，所以我们有一个for循环，它运行一个
right so we have a for loop it runs a

163
00:05:34,430 --> 00:05:35,759
我们要弄清楚一百次，这个看清楚操作要持续多久
hundred times we want to figure out well

164
00:05:35,959 --> 00:05:37,649
我们要弄清楚一百次，这个看清楚操作要持续多久
how long is this see out operation

165
00:05:37,848 --> 00:05:40,079
实际上，将C实际写到控制台需要花多长时间？
really how long does it take C out to

166
00:05:40,278 --> 00:05:41,879
实际上，将C实际写到控制台需要花多长时间？
actually write stuff out to the console

167
00:05:42,079 --> 00:05:43,800
我想为这个功能计时一百次，所以我可以做些什么
a hundred times I want to time this

168
00:05:44,000 --> 00:05:45,689
我想为这个功能计时一百次，所以我可以做些什么
function so what I could do at this

169
00:05:45,889 --> 00:05:47,639
关键是复制开始和结束并完成所有这些工作，我
point is copy start and end and do all

170
00:05:47,839 --> 00:05:49,410
关键是复制开始和结束并完成所有这些工作，我
of that stuff that's a lot of work I

171
00:05:49,610 --> 00:05:51,240
我真的不想这样做，实际上我将要建立一个非常非常
don't really want to do that instead I'm

172
00:05:51,439 --> 00:05:52,829
我真的不想这样做，实际上我将要建立一个非常非常
actually going to set up a very very

173
00:05:53,028 --> 00:05:54,809
基本的结构或类都没关系，只需将其设置为计时器并
basic struct or class doesn't matter

174
00:05:55,009 --> 00:05:57,180
基本的结构或类都没关系，只需将其设置为计时器并
just make this a struct called timer and

175
00:05:57,379 --> 00:05:58,889
这实际上将为我做一切，而我在这里要做的就是
this will actually do everything for me

176
00:05:59,089 --> 00:06:00,838
这实际上将为我做一切，而我在这里要做的就是
and what I'm going to do here is I'm

177
00:06:01,038 --> 00:06:02,910
将要使用整个对象生命周期的一种范式来实际实现
going to use the whole object lifetime

178
00:06:03,110 --> 00:06:06,389
将要使用整个对象生命周期的一种范式来实际实现
kind of paradigm to actually get it to

179
00:06:06,589 --> 00:06:08,218
如果你们还没有签出，基本上会自动为我计时
basically automatically time everything

180
00:06:08,418 --> 00:06:10,050
如果你们还没有签出，基本上会自动为我计时
for me if you guys haven't checked out

181
00:06:10,250 --> 00:06:11,999
有关对象生命周期和C ++的视频绝对可以做到这一点，这非常有用
the video on object lifetime and C++

182
00:06:12,199 --> 00:06:14,129
有关对象生命周期和C ++的视频绝对可以做到这一点，这非常有用
definitely do that it's very very useful

183
00:06:14,329 --> 00:06:16,350
实际上，这个例子我很确定我确实以
and in fact this example I'm pretty sure

184
00:06:16,550 --> 00:06:18,240
实际上，这个例子我很确定我确实以
I did actually give in that video as

185
00:06:18,439 --> 00:06:20,939
好，所以跳回到我们的代码中，构造函数实际上将
well and so jumping back into our code

186
00:06:21,139 --> 00:06:23,249
好，所以跳回到我们的代码中，构造函数实际上将
the constructor is going to actually

187
00:06:23,449 --> 00:06:25,110
启动计时器，很遗憾，我要抓住它并将其放在这里
start the timer so I'm going to grab

188
00:06:25,310 --> 00:06:27,149
启动计时器，很遗憾，我要抓住它并将其放在这里
this and put it in here unfortunately I

189
00:06:27,348 --> 00:06:28,740
实际使用命令，所以我必须弄清楚sed的类型是什么
actually use order so I'm gonna have to

190
00:06:28,939 --> 00:06:30,718
实际使用命令，所以我必须弄清楚sed的类型是什么
figure out what the type is which is sed

191
00:06:30,918 --> 00:06:32,550
亲注意时间点的任何角落城市布所以
Pro note time point s any corner City

192
00:06:32,750 --> 00:06:33,559
亲注意时间点的任何角落城市布所以
cloth so

193
00:06:33,759 --> 00:06:41,600
电视计时时间点的电视计时稳定时钟，所以这就是开始的方式
TV chrono time point s TV chrono steady

194
00:06:41,800 --> 00:06:43,879
电视计时时间点的电视计时稳定时钟，所以这就是开始的方式
clock so that's how kind of start just

195
00:06:44,079 --> 00:06:47,660
称这个起点并放在这里，这样我们就有起点和终点，我也
call this start and put it up here so we

196
00:06:47,860 --> 00:06:50,239
称这个起点并放在这里，这样我们就有起点和终点，我也
have our start and our end and I also

197
00:06:50,439 --> 00:06:52,399
从这里抓住一个方向，并将其放在那是持续时间的开始
grabbed this from here a direction and

198
00:06:52,598 --> 00:06:55,670
从这里抓住一个方向，并将其放在那是持续时间的开始
put this in that's the duration start

199
00:06:55,870 --> 00:06:57,619
我要在构造函数和析构函数中分配给我
I'm going to assign to this in the

200
00:06:57,819 --> 00:07:00,410
我要在构造函数和析构函数中分配给我
constructor and in the destructor when I

201
00:07:00,610 --> 00:07:02,600
将整个代码复制到析构函数中的这里，我实际上要做什么
copy this whole code here in the in the

202
00:07:02,800 --> 00:07:04,218
将整个代码复制到析构函数中的这里，我实际上要做什么
destructor what I'm actually going to do

203
00:07:04,418 --> 00:07:09,819
是写作，等于同一件事，然后终于策展，我的意思是你
is write and equals the same thing and

204
00:07:10,019 --> 00:07:12,439
是写作，等于同一件事，然后终于策展，我的意思是你
then finally curation and I mean you

205
00:07:12,639 --> 00:07:13,819
不必真正存储，就像您看到的那样，因为我们真的只是在
don't really have to store and as you

206
00:07:14,019 --> 00:07:15,259
不必真正存储，就像您看到的那样，因为我们真的只是在
can see because we're really just up at

207
00:07:15,459 --> 00:07:17,420
持续时间，但是如果人们想要访问，也许我想
the duration but in case people want to

208
00:07:17,620 --> 00:07:18,799
持续时间，但是如果人们想要访问，也许我想
access that maybe I want to kind of

209
00:07:18,999 --> 00:07:20,629
将其写入我的计时器结构中，以便生成等于和减去点的那个
write that into my timer struct so

210
00:07:20,829 --> 00:07:23,088
将其写入我的计时器结构中，以便生成等于和减去点的那个
generation equals and minus dot and that

211
00:07:23,288 --> 00:07:25,129
可以在几秒钟内为我们提供方向，这就是我真正需要的
will give us the direction in seconds of

212
00:07:25,329 --> 00:07:27,949
可以在几秒钟内为我们提供方向，这就是我真正需要的
course okay and that's really all I need

213
00:07:28,149 --> 00:07:29,420
让我们把它变得更简单，说我们实际上打印了持续时间，这样
let's just make it even easier and say

214
00:07:29,620 --> 00:07:31,519
让我们把它变得更简单，说我们实际上打印了持续时间，这样
that we actually print the duration so

215
00:07:31,718 --> 00:07:34,429
我们会说我花费的时间，然后我会写持续时间（秒），因为
we'll say time I took and then I'll

216
00:07:34,629 --> 00:07:37,759
我们会说我花费的时间，然后我会写持续时间（秒），因为
write in duration seconds now because

217
00:07:37,959 --> 00:07:39,410
这是一个如此快速的操作，我们实际上可能要打印
this is such a quick operation we

218
00:07:39,610 --> 00:07:40,489
这是一个如此快速的操作，我们实际上可能要打印
actually might want to print the

219
00:07:40,689 --> 00:07:43,790
而是用毫秒表示，调情M s等于计数时间乘以1000
milliseconds instead so I will say flirt

220
00:07:43,990 --> 00:07:48,290
而是用毫秒表示，调情M s等于计数时间乘以1000
M s equals duration of count times 1,000

221
00:07:48,490 --> 00:07:50,269
这将为我们提供以毫秒为单位的值，我将其更改为
which will give us the value in

222
00:07:50,468 --> 00:07:52,069
这将为我们提供以毫秒为单位的值，我将其更改为
milliseconds and I'll change this to be

223
00:07:52,269 --> 00:07:56,360
m / s，这很不错，对我来说，很酷的声音几乎就是全部
m/s and this to be MS okay cool sounds

224
00:07:56,560 --> 00:07:58,519
m / s，这很不错，对我来说，很酷的声音几乎就是全部
pretty good to me that's pretty much all

225
00:07:58,718 --> 00:08:00,468
我需要这里，现在我们有了一个基本的结构，基本上可以完成
I need here and now we have a basic

226
00:08:00,668 --> 00:08:01,999
我需要这里，现在我们有了一个基本的结构，基本上可以完成
structure that will basically do the

227
00:08:02,199 --> 00:08:03,410
时间，甚至自动为我们打印出来，它当然会
timing and even print it up for us

228
00:08:03,610 --> 00:08:05,269
时间，甚至自动为我们打印出来，它当然会
automatically and it will of course do

229
00:08:05,468 --> 00:08:07,009
销毁后，我们真正需要做的就是在
that upon destruction all we really need

230
00:08:07,209 --> 00:08:08,989
销毁后，我们真正需要做的就是在
to do is just create the object at the

231
00:08:09,189 --> 00:08:11,059
我们函数的开始，就是这样，在我的函数中
beginning of our function and that's

232
00:08:11,259 --> 00:08:13,429
我们函数的开始，就是这样，在我的函数中
that's it so over here in my function

233
00:08:13,629 --> 00:08:14,929
我要做的方式就是输入时间，就像那样
the way I'm going to do this is just

234
00:08:15,129 --> 00:08:16,790
我要做的方式就是输入时间，就像那样
type in time a time up like that that's

235
00:08:16,990 --> 00:08:18,139
我所要做的，我已经完成了整个范围，整个功能
all I have to do and I'm done

236
00:08:18,338 --> 00:08:19,999
我所要做的，我已经完成了整个范围，整个功能
this entire scope this entire function

237
00:08:20,199 --> 00:08:22,759
现在将计时，所以如果我返回并删除我所有的废话
will now be timed so if I go back and

238
00:08:22,959 --> 00:08:24,649
现在将计时，所以如果我返回并删除我所有的废话
delete all of this crap that I that I

239
00:08:24,848 --> 00:08:27,379
不在乎我只是要调用这样的函数，我们将按f5
don't care about I'm just going to call

240
00:08:27,579 --> 00:08:29,899
不在乎我只是要调用这样的函数，我们将按f5
function like that and we'll hit f5 and

241
00:08:30,098 --> 00:08:31,759
说会发生什么事，好酷，所以请检查一下，我们可以进行百次印刷
say what happens okay cool so check this

242
00:08:31,959 --> 00:08:33,498
说会发生什么事，好酷，所以请检查一下，我们可以进行百次印刷
out we get pelo printing a hundred times

243
00:08:33,698 --> 00:08:36,049
到控制台，最后我们说这花费了122毫秒
to the console and at the very end we

244
00:08:36,250 --> 00:08:39,078
到控制台，最后我们说这花费了122毫秒
say that this took 122 milliseconds cool

245
00:08:39,278 --> 00:08:40,609
现在我们知道我们的零件实际需要多长时间122毫秒
so we now know how long our part

246
00:08:40,809 --> 00:08:42,889
现在我们知道我们的零件实际需要多长时间122毫秒
actually takes 122 milliseconds very

247
00:08:43,089 --> 00:08:44,689
非常慢，让我们看看是否可以优化
very slow let's see if we might be able

248
00:08:44,889 --> 00:08:45,209
非常慢，让我们看看是否可以优化
to optimize

249
00:08:45,409 --> 00:08:47,159
有点，所以由于某些原因，CDN线路似乎速度较慢，所以如果我们
a little bit so a CDN line seems to be

250
00:08:47,360 --> 00:08:48,990
有点，所以由于某些原因，CDN线路似乎速度较慢，所以如果我们
quite slower for some reason so if we

251
00:08:49,190 --> 00:08:50,639
只是摆脱它，实际上在里面的我们的印刷品中添加一个反斜杠n
just get rid of that and actually put a

252
00:08:50,839 --> 00:08:52,828
只是摆脱它，实际上在里面的我们的印刷品中添加一个反斜杠n
backslash n into our print here inside

253
00:08:53,028 --> 00:08:54,659
我们的功能让我们看看这将有多快，您可以在这里看到
our function let's see how much faster

254
00:08:54,860 --> 00:08:56,339
我们的功能让我们看看这将有多快，您可以在这里看到
this will be and you can see over here

255
00:08:56,539 --> 00:08:58,229
我们已经花了大约三分之一的执行时间，显然您应该
that we've caught out about a third of

256
00:08:58,429 --> 00:08:59,969
我们已经花了大约三分之一的执行时间，显然您应该
our execution time obviously you should

257
00:09:00,169 --> 00:09:02,219
如果您确实在乎，请再运行几次并在发布模式下运行一次
run this a few more times and in release

258
00:09:02,419 --> 00:09:04,039
如果您确实在乎，请再运行几次并在发布模式下运行一次
mode if you actually care about

259
00:09:04,240 --> 00:09:06,269
基准测试和所有这些，但就目前而言，我们仍然可以看到差异
benchmarking and all of that but for now

260
00:09:06,470 --> 00:09:07,678
基准测试和所有这些，但就目前而言，我们仍然可以看到差异
we can still see the difference anyway

261
00:09:07,879 --> 00:09:10,049
这是C ++中计时的基本概述，以及如何计时功能
that is a basic overview of timing in

262
00:09:10,250 --> 00:09:11,938
这是C ++中计时的基本概述，以及如何计时功能
C++ and how you can time your functions

263
00:09:12,139 --> 00:09:13,109
看看执行某些代码需要多长时间，这显然是一个非常
and see how long it takes to execute

264
00:09:13,309 --> 00:09:15,269
看看执行某些代码需要多长时间，这显然是一个非常
certain code obviously this was a very

265
00:09:15,470 --> 00:09:16,799
一个非常粗糙的示例，我只是向您展示了如何实际使用该计时API
very rough example I was just showing

266
00:09:17,000 --> 00:09:18,979
一个非常粗糙的示例，我只是向您展示了如何实际使用该计时API
you how to actually use that timing API

267
00:09:19,179 --> 00:09:22,589
而不是如何实际执行基准测试，我们将有一个视频
not how to actually perform benchmarks

268
00:09:22,789 --> 00:09:23,878
而不是如何实际执行基准测试，我们将有一个视频
we're going to have a video all about

269
00:09:24,078 --> 00:09:26,248
将来您当然也可以使用Visual Studio分析工具
that in the future you can of course

270
00:09:26,448 --> 00:09:28,198
将来您当然也可以使用Visual Studio分析工具
also use visual studios profiling tools

271
00:09:28,399 --> 00:09:29,758
或您正在使用任何工具的任何想法说您正在使用许多工具
or whatever idea you're using whatever

272
00:09:29,958 --> 00:09:32,099
或您正在使用任何工具的任何想法说您正在使用许多工具
tool say you're using a lot of tools do

273
00:09:32,299 --> 00:09:34,109
有内置的分析工具，因此您实际上可以自动化很多
have profiling tools built in so you can

274
00:09:34,309 --> 00:09:35,938
有内置的分析工具，因此您实际上可以自动化很多
actually automate a lot of this this is

275
00:09:36,139 --> 00:09:36,928
或多或少地称为仪器
more or less something called

276
00:09:37,129 --> 00:09:38,459
或多或少地称为仪器
instrumentation where you actually

277
00:09:38,659 --> 00:09:41,219
修改您的源代码以包含诸如此类计时之类的性能分析工具
modify your source code to contain kind

278
00:09:41,419 --> 00:09:43,729
修改您的源代码以包含诸如此类计时之类的性能分析工具
of profiling tools such as this timing

279
00:09:43,929 --> 00:09:46,589
这可能非常有用，因为并非您使用的所有平台都可以使用
that can be very very useful because not

280
00:09:46,789 --> 00:09:48,839
这可能非常有用，因为并非您使用的所有平台都可以使用
all platforms you work with are going to

281
00:09:49,039 --> 00:09:50,878
具有足够的飞镖和高架轮廓，这是一种非常简单的方法
have good enough profile of the dart and

282
00:09:51,078 --> 00:09:52,469
具有足够的飞镖和高架轮廓，这是一种非常简单的方法
overhead and this is a really simple way

283
00:09:52,669 --> 00:09:54,539
要做到这一点，您实际上可以做很多事情，您可以
to do it there are so many things that

284
00:09:54,740 --> 00:09:56,609
要做到这一点，您实际上可以做很多事情，您可以
you can actually do from this you can

285
00:09:56,809 --> 00:09:58,889
基本上将数据收集到一个大文件中，然后输出并读取
basically collect the data into like one

286
00:09:59,089 --> 00:10:01,049
基本上将数据收集到一个大文件中，然后输出并读取
big file and output it and read it in

287
00:10:01,250 --> 00:10:02,938
另一个工具，这样您就可以看到图形或什至类似
another tool so that you can see graphs

288
00:10:03,139 --> 00:10:05,878
另一个工具，这样您就可以看到图形或什至类似
or even like kind of a diagram of how

289
00:10:06,078 --> 00:10:07,469
每个函数需要花费多长时间以及该函数调用什么函数，
long each function takes and what

290
00:10:07,669 --> 00:10:09,448
每个函数需要花费多长时间以及该函数调用什么函数，
function that function calls and there's

291
00:10:09,649 --> 00:10:11,219
如此之多，我们将在未来探索
so many things that will extend from

292
00:10:11,419 --> 00:10:13,229
如此之多，我们将在未来探索
this that we'll explore in the future so

293
00:10:13,429 --> 00:10:15,508
如果您开始做这个非常重要的事情，一定会感到非常兴奋
definitely get excited about that very

294
00:10:15,708 --> 00:10:16,799
如果您开始做这个非常重要的事情，一定会感到非常兴奋
important that you start doing this if

295
00:10:17,000 --> 00:10:18,779
您确实关心性能，因为我想很多人都在看这个
you do care about performance since I

296
00:10:18,980 --> 00:10:20,219
您确实关心性能，因为我想很多人都在看这个
imagine a lot of people watching this

297
00:10:20,419 --> 00:10:22,438
系列是游戏开发人员或想要编写游戏引擎的人
series are game developers or people who

298
00:10:22,639 --> 00:10:24,839
系列是游戏开发人员或想要编写游戏引擎的人
want to write game engines you need to

299
00:10:25,039 --> 00:10:26,519
一直在做这样的事情，以实际了解您的探测速度
be doing stuff like this all the time to

300
00:10:26,720 --> 00:10:28,529
一直在做这样的事情，以实际了解您的探测速度
actually see how fast your probe

301
00:10:28,730 --> 00:10:30,688
执行，并且由于您使用C ++作为一种语言，因此您可能正在使用C ++
performs and since you are using C++ as

302
00:10:30,889 --> 00:10:33,899
执行，并且由于您使用C ++作为一种语言，因此您可能正在使用C ++
a language you're using C++ probably

303
00:10:34,100 --> 00:10:35,578
因为您关心性能并且想要编写快速的代码，所以
because you care about performance and

304
00:10:35,778 --> 00:10:38,399
因为您关心性能并且想要编写快速的代码，所以
you want to write fast code so

305
00:10:38,600 --> 00:10:39,508
无论如何，我一定会习惯这种东西，我希望你们喜欢
definitely get used to this kind of

306
00:10:39,708 --> 00:10:40,919
无论如何，我一定会习惯这种东西，我希望你们喜欢
stuff anyway I hope you guys enjoyed

307
00:10:41,120 --> 00:10:42,419
如果您有这部影片，可以按“赞”按钮，也可以帮助支持
this video if you did you can hit the

308
00:10:42,620 --> 00:10:43,919
如果您有这部影片，可以按“赞”按钮，也可以帮助支持
like button you can also help support

309
00:10:44,120 --> 00:10:45,358
本系列通过前往patreon看4/2 churner，非常感谢
this series by going over to patreon

310
00:10:45,558 --> 00:10:47,729
本系列通过前往patreon看4/2 churner，非常感谢
look on 4/2 churner huge thank you as

311
00:10:47,929 --> 00:10:49,889
一直对我所有的顾客来说，如果没有你，这个系列就不会在这里
always to all of my patrons this series

312
00:10:50,089 --> 00:10:52,529
一直对我所有的顾客来说，如果没有你，这个系列就不会在这里
would not be here without you I can kind

313
00:10:52,730 --> 00:10:54,019
分的保证，所以他们他是如此之多，我会看到你们
of cent guarantee that so they

314
00:10:54,220 --> 00:10:55,909
分的保证，所以他们他是如此之多，我会看到你们
he's so much and I will see you guys in

315
00:10:56,110 --> 00:10:58,219
下一个视频再见[音乐]
the next video goodbye

316
00:10:58,419 --> 00:11:03,419
下一个视频再见[音乐]
[Music]

